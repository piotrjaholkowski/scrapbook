#pragma once

#define JATEST_GENERATE_TEST_FILE 0
#define JATEST_SUITE_NAME_LENGTH 40
#define JATEST_MAX_TEST_PER_SUITE 40
//#define JATEST_USE_WIN_QPC // Use windows query performance
#define JATEST_TIME_STAMP_NUM 64
#define JATEST_USE_TIMESTAMPS // If this is undefined then JATEST_TIMESTAMP(Operation) is defined as " "

#ifdef JA_SDL2_VERSION
	#include <SDL2/SDL.h>
#else
	#include <SDL/SDL.h>
#endif


#include <stdint.h>
#include <limits.h>

//typedef unsigned char uint8_t;
//typedef unsigned int  uint32_t;

enum jaTestTimeStamp
{
	JA_DEFAULT = 0,
	JA_COMPUTE_FORCES_TIME_STAMP = 1,
	JA_GENERATE_CONTACTS_TIME_STAMP = 2,
	JA_GENERATE_CONSTRAINT_ISLAND_TIME_STAMP = 3,
	JA_RESOLVE_CONSTRAINTS_TIME_STAMP = 4,
	JA_MAKE_ISLAND_SLEEP_TIME_STAMP = 5,
	JA_COLLISION_DETECTION_TIME_STAMP = 6,
	JA_INTEGRATION_TIME_STAMP = 7,
	JA_CLEAR_FORCES_TIME_STAMP = 8,
	JA_REMOVE_OLD_CONSTRAINTS_TIME_STAMP = 9
};

namespace ja
{
	namespace setup
	{
		//const int32_t ID_UNASSIGNED = 10000;

		const int32_t TAG_NAME_LENGTH  = 24;
		const int32_t FILE_NAME_LENGTH = 255;

		namespace profiler
		{
			const uint32_t timeStampNum = 1024;
		}

		namespace resources
		{
			const uint32_t ID_UNASSIGNED = UINT_MAX;
			const int32_t  RESOURCE_ALLOCATOR_SIZE = 65536; // 16 * 4KB
		}

		namespace animation
		{
			const uint16_t ID_UNASSIGNED = USHRT_MAX;
		}
		
		namespace entities
		{
			const int32_t ENTITY_ALLOCATOR_SIZE = 65536; // 16 * 4KB
			const int32_t MAX_ENTITIES = 1000;
			const int32_t MAX_COMPONENT_GROUPS = 25;
			//const int32_t ENTITY_TAG_NAME_LENGTH = 24;
			//const int32_t COMPONENT_TAG_NAME_LENGTH = 24;
		}
		
		namespace scripts
		{
			const int32_t MAX_SCRIPT_FUNCTION_NUM = 4; //Max number of function names per script
			const int32_t SCRIPT_FUNCION_LENGTH = 40; //Max length of function name characters
			//const int32_t TAG_NAME_LENGTH = 24;
		}

		namespace graphics
		{
			const uint16_t ID_UNASSIGNED = USHRT_MAX;
			const int32_t  LAYER_OBJECT_CREATOR_NUM = 15;
			const int32_t  POLYGON_ALLOCATOR_SIZE = 65536; // 16 * 4KB
			const int32_t  ANIMATION_ALLOCATOR_SIZE    = 786432;
			const int32_t  DRAW_ELEMENT_ALLOCATOR_SIZE = 786432;
			const int32_t  MAX_SKELETONS = 1000;
			const int32_t  EFFECT_TAG_LENGTH = 24;
			const int32_t  BONE_NAME_LENGTH = 16;
			const int32_t  MAX_LAYER_OBJECTS = 1000;
			const uint32_t TEXTURE_UNASSIGNED = 0xffffffff;
			const int32_t  PIXELS_PER_UNIT = 16;//32 // Pixels per unit
			const int32_t  MAX_TEXTURE_PROXIES = 2;
			const int32_t  SHADER_VARIABLES_NUM = 3; // Number of variables of shader to transfer data between game and shader in jaShader class
			//const int32_t  SHADER_SPECIAL_VARIABLES_NUM = 4; // Number of floats special variable which is set by each sprite entity with isSpecial = true
			const int32_t  SHADER_VARIABLE_NAME_LENGTH = 16;
			const int32_t  SHADER_PASSES_NUM = 3;
			const int32_t  TEMP_LAYER_OBJECTS_NUM = 500;
			const int32_t  MAX_OBJECTS_PER_RENDER_COMMAND = 64;
			const uint32_t MATERIAL_UNIFORM_BLOCK_BINDING_POINT = 1;
			const uint32_t SETTING_UNIFORM_BLOCK_BINDING_POINT = 2;
			const uint32_t SHADER_OBJECT_UNIFORM_BLOCK_BINDING_POINT = 3;
		}

		namespace physics
		{
			const uint16_t ID_UNASSIGNED = USHRT_MAX;
			const int32_t  PHYSICS_OBJECT_ALLOCATOR_SIZE = 65536; // 16 * 4KB
			const int32_t  MULTI_SHAPE_ALLOCATOR_SIZE = 65536; // 16 * 4 KB
			const int32_t  MAX_PHYSICS_CONSTRAINT_ALLOCATOR = 10 * 65536; // 16 * 4KB
			const int32_t  PHYSICS_ISLAND_ALLOCATOR_SIZE = 65536; // 16 * 4KB
			const int32_t  MAX_PHYSIC_OBJECT = 1000;
			const int32_t  MAX_CONSTRAINT_CREATOR = 15; // Number of constraint creators available
			const float    DEFAULT_GRAVITY = -0.060f;
			const float    CONTACT2D_ELASTIC_ACCELERATION_TRESHOLD = 0.070f; // The minimal relative acceleration of two bodies contact which is considered as elastic
			const float    CONTACT2D_RELAXATION_BIAS = 0.2f; // Correct 20% of rigid body contact each frame
			const float    CONTACT2D_SKIN_THICKNESS = 0.01f;// Value substracted from penetration depth to keep contact in cache
			const int32_t  CONTACT2D_FRAME_PERSISTENCE = 2;// If contact is not updated and rigid body is in collision with other body then keep not updated contact for two simulation steps
			const float    CONTACT2D_SLEEP_TRESHOLD = 0.0001f;// If sum of delta impulses is below that treshold then contact is considered as contact which needs to be put to sleep
			const float    LINEAR_VELOCITY_SLEEP_TRESHOLD = 0.01f;// If energy of constraint island is below that treshold then island goes to sleep
			const float    ANGULAR_VELOCITY_SLEEP_TRESHOLD = (2.0f / 180.0f * 3.14159265359f); // If energy of constraint island is below that treshold then island goes to sleep
			const int32_t  MAX_ISLAND_TO_SLEEP_CACHE = 64;// Size of hash table for keeping island sleep time between steps
			const float    ISLAND_SLEEP_TIME = 3.0f; // Time after which whole constraint island go to sleep if all constraints are relaxed
			const int32_t  MAX_CONTACT2D_NUM = 2;// Number of contacts in jaContact2dManifold
			const float    DISTANCE_JOINT2D_RELAXATION_BIAS = 0.2f; // Correct 20% of rigid body distance each position correction step
			const float    DISTANCE_JOINT2D_SLOPE = 0.005f; // Slope which determines position error of distance joint as acceptable solution
			const float    DISTANCE_JOINT2D_SLEEP_TRESHOLD = 0.0001f;
		}

		namespace sound
		{
			//Sound capture
			const int32_t CAPTURE_BUFFER_SIZE = 256;
			const int32_t CAPTURE_SAMPLE_RATE = 8192;
			//music streaming
			const int32_t STREAM_BUFFS_NUM = 3;
			//music streaming
		}

		namespace threads
		{
			const int32_t MAX_THREADS = 64; // Max number of threads available from thread manager
			const int32_t THREAD_TASK_NUM = 2000; // Average number of tasks allocated by one thread manager ture
		}

		namespace gilgamesh
		{
			const int32_t SCENE_ALLOCATOR_SIZE = 65536; // 16 * 4KB
			const float   XENO_ERROR_TOLERANCE = 1.0e-15f; // Tolerance of error in MPR intersectionXeno function
			const float   GOLDEN_RATIO = 1.618033987f; // Golden ratio fibonacci magic number
			const int32_t MAXIMUM_ITERATIONS = 100; // Maximum iteration number in MPR penetrationXeno function
			const float   EPA_TOLERANCE = 0.0001f;
			const float   MPR_TOLERANCE = 0.0001f;
			const int32_t VERTICES_NUM2D = 16; // Maximum number of vertices at convex polygon
			const int32_t PATH_VERTICES_NUM2D = 16; // Maximum number of vertices at path
			const int32_t CONTACT_HASH_SPACE = 512; // Hash space number multiplied by hash seed in contact->hash to give each contact unique number  
			const float   SIMPLEX_EPSILON2D = 0.01f; // u�ywanie w algorytmie mpr2d
			const int32_t MAX_PARTICLES = 100; // Max number of particles by one particle generator
			const int32_t MAX_CONTACT2D_PER_OBJECT = 2; // Max number of contacts returned per object
			const float   SECOND_CONTACT2D_TOLERANCE = 0.01f; // Diffrence on normal beetwen two collision points
			const int32_t MAX_SHAPES = 10; // Number of collison shapes processed
			const int32_t CACHE_LINE_SIZE = 64;

			// Hash grid configuration
			const int32_t  MAX_BUCKET_NUM = 128; // Max bucket number at hashGrid
			const int32_t  NODE_ALLOCATED_DATA_BLOCK = 1024; // How much hash nodes allocate at one block by pool allocator
			//WARNING !!! should not be more than 30000 due to size of test bit array which is ~53 Megabytes at 30000 objects
			const uint32_t MAX_OBJECTS = 1000; // // Defines maximum number of objects, used for creating bit object test array in hashGrid
			const int32_t  MAX_CONTACTS_PER_OBJECT = 16; // Used to calculate necessary memory size while using multithreading
		}

		namespace gui
		{
			const int32_t INFO_FADE_MAX_CHARS_NUM = 255;
			const int32_t LOG_FADE_MAX_CHARS_NUM = 500;
		}

		namespace fastAllocatorSTL
		{
			const int32_t memBlockSize = 1024 * 1024 * 4; // 4 Megabytes | (memBlock % 2) == 0
			const int32_t freeSpaceLookupTableSize = 20;              // Sizes: 8,16,32,64,128,256,512,...,2097152,4194304
			const int32_t biggestIndexedSearchIndex = 10;
			const int32_t lookupIndexSearchTableSize = 4096;
			const int32_t smallestAllocationSize = 8;
		}
	}
}

#ifdef GILUSEDOUBLE
#ifndef GILSSE
typedef double gilFloat;
#define GILMAXFLOAT DBL_MAX
#define GILMINFLOAT DBL_MIN
#define GILFLOAT double
#define GILACOS acos // acosf
#define GILSIN sin // sinf
#define GILCOS cos // cosf
#define GILSQRT sqrt // sqrtf
#define GILFABS fabs // fabsf
#define  GILTAN tan // tanf
#define	GILATAN atan2 // atan2f
#define GILPOW pow

#define GILEPS DBL_EPSILON
#define GIL_REAL(x)(x ## f)
#define GIL_ONE GIL_REAL(1.)
#define GIL_ZERO GIL_REAL(0.)
#else
typedef float gilFloat;
#define GILMAXFLOAT FLT_MAX
#define GILMINFLOAT FLT_MIN
#define GILFLOAT float
#define GILACOS acosf
#define GILSIN sinf
#define GILCOS cosf
#define GILSQRT sqrtf
#define GILFABS fabsf
#define  GILTAN tanf
#define	GILATAN atan2f
#define GILPOW powf

#define GILEPS FLT_EPSILON
#define GIL_REAL(x)(x ## f)
#define GIL_ONE GIL_REAL(1.)
#define GIL_ZERO GIL_REAL(0.)
#endif
#else
typedef float jaFloat;
#define GILMAXFLOAT FLT_MAX
#define GILMINFLOAT FLT_MIN
#define GILFLOAT float
#define GILACOS acosf
#define GILASIN asinf
#define	GILATAN atan2f
#define GILSIN sinf
#define GILCOS cosf
#define GILSQRT sqrtf
#define GILFABS fabs //ja::math::fastAbs ordinary abs is faster on haswell 
#define  GILTAN tanf
#define GILPOW powf

#define GILEPS FLT_EPSILON
#define GIL_REAL(x)(x ## f)
#define GIL_ONE GIL_REAL(1.)
#define GIL_ZERO GIL_REAL(0.)
#endif

//#define NO_SDL_GLEXT
//#define JA_SDL2_VERSION
#define JA_SDL_OPENGL // Use openGL API
#define JA_SDL_API // Use SDL API

#define JA_CYCLIC_FUNCTION_LOG_MESSAGE_SIZE 30
#define JA_INVALID_UINT_VALUE UINT_MAX

