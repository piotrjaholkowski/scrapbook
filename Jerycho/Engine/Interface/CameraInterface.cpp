#include "CameraInterface.hpp"

#include "../Math/VectorMath2d.hpp"
#include "../Gilgamesh/Mid/AABB2d.hpp"
#include "../Gilgamesh/Shapes/Box2d.hpp"
#include "../Manager/DisplayManager.hpp"

namespace ja
{

using namespace gil;

void CameraInterface::getBoundingBox(AABB2d* boundingBox)
{
	int32_t widthRes, heightRes;
	jaFloat camWidthRes, camHeightRes;
	DisplayManager::getResolution(&widthRes,&heightRes);
	const jaFloat pixelsPerUnit = (jaFloat)setup::graphics::PIXELS_PER_UNIT;
	camWidthRes  = ((jaFloat)widthRes / pixelsPerUnit);
	camHeightRes = ((jaFloat)heightRes / pixelsPerUnit);

	camWidthRes  /= scale.x;
	camHeightRes /= scale.y;

	jaTransform2 transform(position, angleRad);
	Box2d box = Box2d(camWidthRes,camHeightRes);
	box.initAABB2d(transform,*boundingBox);
}

}