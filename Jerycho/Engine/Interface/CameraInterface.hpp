#pragma once

#include "../Math/Math.hpp"
#include "../Math/VectorMath2d.hpp"

namespace gil
{
	class AABB2d;
}

namespace ja
{

	class CameraInterface
	{
	public:
		jaVector2 position;
		jaVector2 scale;
		jaVector2 velocity;
		jaFloat   angleRad;

		CameraInterface() : position(0.0f, 0.0f), scale(1.0f,1.0f), angleRad(0.0f), velocity(0.0f, 0.0f)
		{

		}

		CameraInterface(jaVector2& position, jaFloat radians = 0) : scale(1.0f, 1.0f), angleRad(radians), position(position), velocity(0.0f, 0.0f)
		{

		}

		CameraInterface(jaVector2& position, jaVector2& scale, jaFloat radians = 0) : position(position), scale(scale), angleRad(radians), velocity(0.0f, 0.0f)
		{

		}

		void getBoundingBox(gil::AABB2d* boundingBox);

	};

}

