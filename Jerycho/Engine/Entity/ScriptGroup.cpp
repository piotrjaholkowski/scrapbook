#include "ScriptGroup.hpp"
#include "../Manager/ResourceManager.hpp"

namespace ja
{
ScriptGroup* ScriptGroup::singleton = nullptr;

ScriptGroup::ScriptGroup(CacheLineAllocator* cacheLineAllocator) : EntityGroup(cacheLineAllocator), scripts(nullptr)
{
	L = ScriptManager::getSingleton()->getLuaState();
	singleton = this;

	Resource<Texture2d>* resource = ResourceManager::getSingleton()->loadEternalTexture2d("entity_icons/script.png");

	if (nullptr != resource)
	{
		this->textureImage = resource->resource;
	}

	std::cout << "ScriptComponent size: " << sizeof(ScriptComponent) << std::endl;
}

ScriptGroup::~ScriptGroup()
{

}

void ScriptGroup::update( float deltaTime )
{
	this->deltaTime = deltaTime;

	for(ScriptComponent* itScript = scripts; itScript != nullptr; itScript = (ScriptComponent*)(itScript->next))
	{
		updateComponent(itScript);
	}
}

void ScriptGroup::updateComponent(ScriptComponent* scriptComponent)
{
	if (scriptComponent->updateRef != 0)
	{
		lua_rawgeti(L, LUA_REGISTRYINDEX, scriptComponent->updateRef);
		lua_pushnumber(L, deltaTime);
		lua_pushlightuserdata(L, scriptComponent->getEntity());
		lua_pushlightuserdata(L, scriptComponent);
		lua_call(L, 3, 0);
	}
}

void ScriptGroup::updateScriptFunctionReference(int32_t oldFunctionReference, int32_t newFunctionReference)
{
	for (ScriptComponent* itScript = scripts; itScript != nullptr; itScript = (ScriptComponent*)(itScript->next))
	{
		if (itScript->updateRef == oldFunctionReference)
		{
			itScript->updateRef = newFunctionReference;
		}
	}
}

ScriptComponent* ScriptGroup::createComponent(Entity* entity)
{
	ScriptComponent* script = new (cacheLineAllocator->allocate(sizeof(ScriptComponent))) ScriptComponent(this);

	if(scripts == nullptr)
	{
		scripts = script;
	}
	else
	{
		script->next = scripts;
		scripts->previous = script;
		scripts = script;
	}

	entity->addComponent(script);

	return script;
}

void ScriptGroup::deleteComponent( EntityComponent* component )
{
	ScriptComponent* deletedComponent = (ScriptComponent*)component;

	if(deletedComponent == scripts)
	{
		scripts = (ScriptComponent*)(deletedComponent->next);
	}

	if(deletedComponent->previous != nullptr)
		deletedComponent->previous->next = deletedComponent->next;

	if(deletedComponent->next != nullptr)
		deletedComponent->next->previous = deletedComponent->previous;


	cacheLineAllocator->free(deletedComponent,sizeof(ScriptComponent));
}

ScriptComponent::ScriptComponent( EntityGroup* componentGroup ) : EntityComponent(componentGroup,(uint8_t)EntityGroupType::SCRIPT), updateRef(0), updateHash(0), updateFileHash(0) //script(nullptr), functionIndex(0)
{

}

void ScriptComponent::setUpdateFunction(const ScriptFunction& scriptFunction)
{
	this->updateRef      = scriptFunction.functionRef;
	this->updateHash     = scriptFunction.functionHash;
	this->updateFileHash = scriptFunction.fileHash;
}

void ScriptComponent::onUpdateOtherComponent( EntityComponent* component )
{

}

void ScriptComponent::onDeleteOtherComponent( EntityComponent* component )
{

}



}
