#pragma once

#include "Entity.hpp"

#include "../Data/DataModel.hpp"
#include "../Manager/ScriptManager.hpp"


namespace ja
{

	class DataComponent : public EntityComponent
	{
	private:
		DataModel* dataModel;
		void*      allocatedData;
		uint32_t   allocatedDataSize;
		int32_t    initDataFunctionRef;
	public:
		DataComponent(EntityGroup* componentGroup);

		void onUpdateOtherComponent(EntityComponent* component);
		void onDeleteOtherComponent(EntityComponent* component);
		void initData();
		void setDataModel(DataModel* dataModel);

		inline void setFloat(const DataFieldInfo* dataFieldInfo, const int32_t elementId, const float dataValue)
		{
			float* dataPtr = (float*)((uint8_t*)allocatedData + (dataFieldInfo->dataFieldOffset + (elementId * sizeof(float)) ) );
			*dataPtr = dataValue;

			if (0 != dataFieldInfo->accessDataFunctionRef)
			{
				lua_State* L = ScriptManager::getSingleton()->getLuaState();
				lua_rawgeti(L, LUA_REGISTRYINDEX, dataFieldInfo->accessDataFunctionRef);
				lua_pushlightuserdata(L, this);
				lua_pushlightuserdata(L, (void*)dataFieldInfo);
				lua_pushinteger(L, elementId);
				lua_call(L, 3, 0);
			}
		}

		inline void setInt32(const DataFieldInfo* dataFieldInfo, const int32_t elementId, const int32_t dataValue)
		{
			int32_t* dataPtr = (int32_t*)((uint8_t*)allocatedData + (dataFieldInfo->dataFieldOffset + (elementId * sizeof(int32_t))));
			*dataPtr = dataValue;

			if (0 != dataFieldInfo->accessDataFunctionRef)
			{
				lua_State* L = ScriptManager::getSingleton()->getLuaState();
				lua_rawgeti(L, LUA_REGISTRYINDEX, dataFieldInfo->accessDataFunctionRef);
				lua_pushlightuserdata(L, this);
				lua_pushlightuserdata(L, (void*)dataFieldInfo);
				lua_pushinteger(L, elementId);

				lua_call(L, 3, 0);
			}
		}

		inline void callDataAccess(const DataFieldInfo* dataFieldInfo, const int32_t elementId)
		{
			if (0 != dataFieldInfo->accessDataFunctionRef)
			{
				lua_State* L = ScriptManager::getSingleton()->getLuaState();
				lua_rawgeti(L, LUA_REGISTRYINDEX, dataFieldInfo->accessDataFunctionRef);
				lua_pushlightuserdata(L, this);
				lua_pushlightuserdata(L, (void*)dataFieldInfo);
				lua_pushinteger(L, elementId);
				lua_call(L, 3, 0);
			}
		}

		inline void zeroAllocatedMem()
		{
			memset(this->allocatedData, 0, this->allocatedDataSize);
		}

		inline DataModel* getDataModel()
		{
			return dataModel;
		}

		inline void* getAllocatedData()
		{
			return this->allocatedData;
		}

		friend class DataGroup;
	};

	class DataGroup : public EntityGroup
	{
	private:
		lua_State*     L;
		DataComponent* data;

		static DataGroup* singleton;

		ja::Texture2d* textureImage;
	public:
		DataGroup(CacheLineAllocator* cacheLineAllocator);
		~DataGroup();

		inline static DataGroup* getSingleton()
		{
			return singleton;
		}

		DataComponent* createComponent(Entity* entity);
		void deleteComponent(EntityComponent* component);
		void update(float deltaTime);
		void updateDataModel(DataModel* oldDataModel, DataModel* newDataModel);
		void updateScriptFunctionReference(int32_t oldReference, int32_t newReference);

		inline Texture2d* getTextureImage() const
		{
			return this->textureImage;
		}
	};

}
