#include "LayerObjectGroup.hpp"
#include "../Manager/ResourceManager.hpp"

namespace ja
{

LayerObjectGroup::LayerObjectGroup(CacheLineAllocator* cacheLineAllocator) : EntityGroup(cacheLineAllocator), layerObjectComponents(nullptr)
{
	layerObjectManager = LayerObjectManager::getSingleton();
	singleton = this;

	Resource<Texture2d>* resource = ResourceManager::getSingleton()->loadEternalTexture2d("entity_icons/layer_object.png");

	if (nullptr != resource)
	{
		this->textureImage = resource->resource;
	}

	std::cout << "LayerObjectComponent size: " << sizeof(LayerObjectComponent) << std::endl;
}

LayerObjectGroup::~LayerObjectGroup()
{

}

void LayerObjectGroup::update( float deltaTime )
{
	for(LayerObjectComponent* itLayerObject = layerObjectComponents; itLayerObject != nullptr; itLayerObject = (LayerObjectComponent*)(itLayerObject->next))
	{
		updateComponent(itLayerObject,deltaTime);
	}
}

void LayerObjectGroup::updateComponent(LayerObjectComponent* layerObjectComponent, float deltaTime)
{
	LayerObjectElement* layerObjectCollection = layerObjectComponent->getLayerObjectCollection();

	if ((layerObjectCollection != nullptr) && (layerObjectComponent->physicObject != nullptr))
	{
		gil::ObjectHandle2d* physicHandle = layerObjectComponent->physicObject->getShapeHandle();
		bool fixedRotation = layerObjectComponent->isFixedRotation();

		do
		{
			gil::ObjectHandle2d* layerObjectHandle = layerObjectCollection->layerObject->getHandle();
			layerObjectHandle->transform.position = physicHandle->transform.position - (layerObjectHandle->transform.rotationMatrix * layerObjectCollection->positionDiff);
			if (!fixedRotation)
				layerObjectHandle->transform.rotationMatrix = layerObjectCollection->rotDiff * physicHandle->transform.rotationMatrix;

			layerObjectCollection->layerObject->updateTransform();
			//layerObjectManager->updateLayerObject(layerObjectCollection->layerObject);

			layerObjectCollection = layerObjectCollection->next;
		} while (layerObjectCollection != nullptr);
	}
}

void LayerObjectGroup::updateScriptFunctionReference(int32_t oldFunctionReference, int32_t newFunctionReference)
{

}

LayerObjectComponent* LayerObjectGroup::createComponent(Entity* entity)
{
	LayerObjectComponent* layerObject = new (cacheLineAllocator->allocate(sizeof(LayerObjectComponent))) LayerObjectComponent(this);

	if(layerObjectComponents == nullptr)
	{
		layerObjectComponents = layerObject;
	}
	else
	{
		layerObject->next = layerObjectComponents;
		layerObjectComponents->previous = layerObject;
		layerObjectComponents = layerObject;
	}

	entity->addComponent(layerObject);

	return layerObject;
}

void LayerObjectGroup::deleteComponent( EntityComponent* component )
{
	if(component == this->layerObjectComponents)
	{
		this->layerObjectComponents = (LayerObjectComponent*)(component->next);
	}

	if(component->previous != nullptr)
		component->previous->next = component->next;

	if(component->next != nullptr)
		component->next->previous = component->previous;

	LayerObjectComponent* layerObjectComponent = (LayerObjectComponent*)(component);
	layerObjectComponent->clearLayerObjects();
	this->cacheLineAllocator->free(component,sizeof(LayerObjectComponent));
}

LayerObjectComponent::LayerObjectComponent( EntityGroup* componentGroup ) : EntityComponent(componentGroup,(uint8_t)EntityGroupType::LAYER_OBJECT), layerObjectCollection(nullptr), physicObject(nullptr)
{

}

void LayerObjectComponent::setPhysicComponent( PhysicComponent* component )
{
	physicObject = component->getPhysicObject();
	component->setReferenced(true);
	updateLocationData();
}

void LayerObjectComponent::onUpdateOtherComponent( EntityComponent* component )
{
	if(component->getComponentType() != (uint8_t)EntityGroupType::PHYSIC_OBJECT)
		return;

	PhysicComponent* physicComponent = (PhysicComponent*)(component);
	PhysicComponent* currentComponent = nullptr;

	if(physicObject != nullptr)
	{
		currentComponent = (PhysicComponent*)(physicObject->getComponentData());
	}

	if(physicComponent == currentComponent)
	{
		physicObject = physicComponent->getPhysicObject();
		updateLocationData();
	}
}

void LayerObjectComponent::onDeleteOtherComponent( EntityComponent* component )
{
	if(component->getComponentType() != (uint8_t)EntityGroupType::PHYSIC_OBJECT)
		return;

	PhysicComponent* physicComponent  = (PhysicComponent*)(component);
	PhysicComponent* currentComponent = nullptr;

	if(physicObject != nullptr)
	{
		currentComponent = (PhysicComponent*)(physicObject->getComponentData());
	}

	if(physicComponent == currentComponent)
	{
		physicObject = nullptr;
	}
}

void LayerObjectComponent::addLayerObject( LayerObject* layerObject)
{
	if(layerObject->componentData != nullptr)
		return;

	LayerObjectElement* layerObjectElement = new (componentGroup->cacheLineAllocator->allocate(sizeof(LayerObjectElement))) LayerObjectElement(this,layerObject);

	if(layerObjectCollection == nullptr)
	{
		layerObjectCollection = layerObjectElement;
	}
	else
	{
		LayerObjectElement* itLayerObjectCollection = layerObjectCollection;

		while(itLayerObjectCollection->next != nullptr)
		{
			itLayerObjectCollection = itLayerObjectCollection->next;
		}

		itLayerObjectCollection->next = layerObjectElement;
	}

	updateLocationData();
}

LayerObjectGroup* LayerObjectGroup::singleton = nullptr;

}
