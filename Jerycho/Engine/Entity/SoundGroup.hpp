#pragma once

#include "../Manager/SoundManager.hpp"
#include "PhysicGroup.hpp"
#include "Entity.hpp"

namespace ja
{

	enum class SoundComponentType
	{
		SOUND_EFFECT   = 0,
		SOUND_STREAMER = 1
	};

#define AL_END_OF_LOOP 678

	class SoundStreamer
	{
	public:
		SoundStream* soundStream;
		int32_t bitStream;
		ALint   state;
		ALuint  buffers[setup::sound::STREAM_BUFFS_NUM];
		bool    isLoop;
		int64_t currentSample;

		SoundStreamer() : soundStream(nullptr), bitStream(0)
		{
			state = AL_STOPPED;
			isLoop = false;
		}

		void setSample(int64_t samplePos)
		{
			//ov_pcm_seek_lap(&soundStream->oggFile, samplePos);
			currentSample = samplePos;
		}

		void saveSamplePosition()
		{
			currentSample = ov_pcm_tell(&soundStream->oggFile);
		}

		void loadSamplePosition()
		{
			ov_pcm_seek(&soundStream->oggFile, currentSample);
		}

		void setElapsedTime(float elapsedTime)
		{
			ov_time_seek(&soundStream->oggFile, elapsedTime);
			currentSample = ov_pcm_tell(&soundStream->oggFile);
		}

		void getElapsedTime(float& elapsedTime)
		{
			ov_pcm_seek(&soundStream->oggFile, currentSample);
			elapsedTime = static_cast<float>(ov_time_tell(&soundStream->oggFile));
		}
	};

	class SoundComponent : public EntityComponent
	{
	private:
		SoundEffect* soundEffect;
		SoundStreamer* soundStreamer;
		PhysicObject2d* physicObject;
		ALuint  soundEmitterId;
		uint8_t soundType;
	public:
		SoundComponent(EntityGroup* componentGroup);
		inline void setRelativeToListener(bool isRelative); // determines if the positions are relative to the listener
		inline void setReferenceDistance(float referenceDistance); //the distance under which the volume for the source would normally drop by half (before being influenced by rolloff factor or AL_MAX_DISTANCE)
		inline void setMaxDistance(float maxDistance); // max distance which after reaching sound won't be mixed used with the Inverse Clamped Distance Model
		inline void setRolloffFactor(float rollofFactor);
		inline void setPosition(float x, float y, float z);
		inline void setVelocity(float x, float y, float z);
		inline void setDirection(float x, float y, float z);
		inline void setPitch(float pitch); // speed of streaming sound
		inline void setGain(float gain); // volume multiplier
		inline void setMinGain(float gain); // lowest volume of sound
		inline void setMaxGain(float gain); // max volume of sound
		inline void setLoop(bool isLoop);
		inline bool isLoop();
		inline void stop();
		inline void setSound(SoundEffect* soundEffect);
		inline void setSoundStream(SoundStream* soundStream);
		inline void play();
		inline ALint getState();
		inline void pause();
		inline void setElapsedTime(float time);
		inline void getElapsedTime(float& elapsedTime);
		inline SoundEffect* getSoundEffect();
		inline SoundStream* getSoundStream();
		inline SoundStreamer* getSoundStreamer();
		inline uint8_t getSoundComponentType();

		void setPhysicComponent(PhysicComponent* component);

		inline PhysicObject2d* getPhysicObject()
		{
			return physicObject;
		}

		void onUpdateOtherComponent(EntityComponent* component);
		void onDeleteOtherComponent(EntityComponent* component);

		friend class SoundGroup;
	};

	class SoundGroup : public EntityGroup
	{
	private:
		SoundManager*   soundManager;
		SoundComponent* soundEmitters;
		int8_t*         soundStreamBuffer;
	public:
		SoundGroup(CacheLineAllocator* cacheLineAllocator);
		~SoundGroup();

		EntityComponent* createComponent();
		SoundStreamer*   createSoundStreamer();
		void deleteComponent(EntityComponent* component);
		void deleteSoundStreamer(SoundStreamer* soundStreamer);
		void update(float deltaTime);
		void updateSoundStreamer(SoundComponent* soundComponent);

		inline void updateComponent(SoundComponent* soundComponent)
		{
			if (soundComponent->physicObject != nullptr) {
				gil::ObjectHandle2d* objectHandle = soundComponent->physicObject->getShapeHandle();
				soundComponent->setPosition(objectHandle->transform.position.x, objectHandle->transform.position.y, 0.0);
			}

			if (soundComponent->getSoundComponentType() == (uint8_t)SoundComponentType::SOUND_STREAMER) {
				updateSoundStreamer(soundComponent);
			}
		}
	};

	void SoundComponent::setRelativeToListener(bool isRelative)
	{
		// default 1.0
		alSourcei(soundEmitterId, AL_SOURCE_RELATIVE, isRelative); // deafult AL_FALSE
	}

	void SoundComponent::setReferenceDistance(float referenceDistance)
	{
		alSourcef(soundEmitterId, AL_REFERENCE_DISTANCE, referenceDistance); // deafult 1.0
	}

	void SoundComponent::setRolloffFactor(float rollofFactor)
	{
		alSourcef(soundEmitterId, AL_ROLLOFF_FACTOR, rollofFactor); // deafult 1.0
	}

	void SoundComponent::setMaxDistance(float maxDistance)
	{
		alSourcef(soundEmitterId, AL_MAX_DISTANCE, maxDistance); // deafult maxFloat
	}

	void SoundComponent::setPosition(float x, float y, float z)
	{
		alSource3f(soundEmitterId, AL_POSITION, x, y, z); // default 0,0,0
	}

	void SoundComponent::setDirection(float x, float y, float z)
	{
		alSource3f(soundEmitterId, AL_DIRECTION, x, y, z); // if not 0,0,0 then source is directional
		//default (0.0f, 0.0f, 0.0f)
	}

	void SoundComponent::setVelocity(float x, float y, float z)
	{
		alSource3f(soundEmitterId, AL_VELOCITY, x, y, z); // default 0,0,0
	}

	void SoundComponent::setPitch(float pitch)
	{
		alSourcef(soundEmitterId, AL_PITCH, pitch); // default 1.0
	}

	void SoundComponent::setGain(float gain)
	{
		alSourcef(soundEmitterId, AL_GAIN, gain); // default 1.0 [0, any]
	}

	void SoundComponent::setMinGain(float gain)
	{
		alSourcef(soundEmitterId, AL_MIN_GAIN, gain); // default 0.0
	}

	void SoundComponent::setMaxGain(float gain)
	{
		alSourcef(soundEmitterId, AL_MAX_GAIN, gain); // default 1.0
	}

	void SoundComponent::setLoop(bool isLoop)
	{
		if (soundStreamer != nullptr) {
			soundStreamer->isLoop = isLoop;
		}
		else {
			alSourcei(soundEmitterId, AL_LOOPING, isLoop);
		}
	}

	bool SoundComponent::isLoop()
	{
		int state;
		alGetSourcei(soundEmitterId, AL_LOOPING, &state);

		if (state > 0)
			return true;
		return false;
	}

	void SoundComponent::stop()
	{
		alSourceStop(soundEmitterId);
		if (soundStreamer != nullptr) {
			soundStreamer->setSample(0);
			soundStreamer->state = AL_INITIAL;
		}
	}

	void SoundComponent::setSound(SoundEffect* soundEffect)
	{
		soundType = (uint8_t)SoundComponentType::SOUND_EFFECT;
		if (soundStreamer != nullptr) {
			stop();
			SoundGroup* soundGroup = (SoundGroup*)getComponentGroup();
			soundGroup->deleteSoundStreamer(soundStreamer);
			soundStreamer = nullptr;
		}
		this->soundEffect = soundEffect;
		alSourcei(soundEmitterId, AL_BUFFER, soundEffect->soundId);
	}

	void SoundComponent::setSoundStream(SoundStream* soundStream)
	{
		soundType = (uint8_t)SoundComponentType::SOUND_STREAMER;

		stop();
		SoundGroup*     soundGroup = (SoundGroup*)getComponentGroup();
		soundStreamer = soundGroup->createSoundStreamer();
		soundStreamer->soundStream = soundStream;
		soundStreamer->state = AL_INITIAL;
	}

	void SoundComponent::play()
	{
		int32_t state; // AL_INITIAL, AL_STOPPED, AL_PAUSED, AL_PLAYING
		alGetSourcei(soundEmitterId, AL_SOURCE_STATE, &state);
		if (state == AL_STOPPED)
			alSourceRewind(soundEmitterId);
		alSourcePlay(soundEmitterId);

		if (soundStreamer != nullptr) {
			soundStreamer->state = AL_PLAYING;
		}
	}

	ALint SoundComponent::getState()
	{
		ALint state;
		alGetSourcei(soundEmitterId, AL_SOURCE_STATE, &state);
		// state AL_PLAYING, AL_STOPPED, AL_PAUSED

		return state;
	}

	void SoundComponent::pause()
	{
		alSourcePause(soundEmitterId);
		if (soundStreamer != nullptr) {
			soundStreamer->state = AL_PAUSED;
		}
	}

	void SoundComponent::setElapsedTime(float time)
	{
		if (soundType == (uint8_t)SoundComponentType::SOUND_EFFECT)
			alSourcef(soundEmitterId, AL_SEC_OFFSET, time); // time in seconds
		else {
			if (soundStreamer == nullptr)
				return;
			soundStreamer->setElapsedTime(time);
		}
		//AL_SAMPLE_OFFSET 
		//AL_BYTE_OFFSET 
	}

	void SoundComponent::getElapsedTime(float& elapsedTime)
	{
		if (soundType == (uint8_t)SoundComponentType::SOUND_EFFECT)
			alGetSourcef(soundEmitterId, AL_SEC_OFFSET, &elapsedTime);
		else {
			if (soundStreamer == nullptr)
				return;
			soundStreamer->getElapsedTime(elapsedTime);
		}
	}

	SoundEffect* SoundComponent::getSoundEffect()
	{
		return soundEffect;
	}

	SoundStream* SoundComponent::getSoundStream()
	{
		if (soundStreamer != nullptr)
		{
			return soundStreamer->soundStream;
		}
		return nullptr;
	}

	SoundStreamer* SoundComponent::getSoundStreamer()
	{
		return soundStreamer;
	}

	uint8_t SoundComponent::getSoundComponentType()
	{
		return soundType;
	}

}

