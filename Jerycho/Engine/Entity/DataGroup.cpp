#include "DataGroup.hpp"
#include "../Manager/ResourceManager.hpp"
#include "../Manager/ScriptManager.hpp"

namespace ja
{
	DataGroup* DataGroup::singleton = nullptr;

	const char     DataTypeInfo::typeName[][24] = {"float", "int32", "bool"};
	const uint32_t DataTypeInfo::typeSize[] = { sizeof(float), sizeof(int32_t), sizeof(int32_t) };

	DataGroup::DataGroup(CacheLineAllocator* cacheLineAllocator) : EntityGroup(cacheLineAllocator), data(nullptr)
	{
		L         = ScriptManager::getSingleton()->getLuaState();
		singleton = this;

		Resource<Texture2d>* resource = ResourceManager::getSingleton()->loadEternalTexture2d("entity_icons/data.png");

		if (nullptr != resource)
		{
			this->textureImage = resource->resource;
		}

		std::cout << "DataComponent size: " << sizeof(DataComponent) << std::endl;
	}

	DataGroup::~DataGroup()
	{

	}

	void DataGroup::update(float deltaTime)
	{
		//this->deltaTime = deltaTime;
		//
		//for (ScriptComponent* itScript = scripts; itScript != nullptr; itScript = (ScriptComponent*)(itScript->next))
		//{
		//	updateComponent(itScript);
		//}
	}

	void DataGroup::updateDataModel(DataModel* oldDataModel, DataModel* newDataModel)
	{
		for (DataComponent* itData = data; itData != nullptr; itData = (DataComponent*)(itData->next))
		{
			DataModel* dataModel = itData->getDataModel();
			
			if (oldDataModel == dataModel)
			{
				itData->setDataModel(newDataModel); // it automaticlly copies old field names to new
			}
		}
	}

	void DataGroup::updateScriptFunctionReference(int32_t oldReference, int32_t newReference)
	{
		HashMap<ja::Resource<ja::DataModel>>* dataModels = ResourceManager::getSingleton()->getDataModels();

		//search all data models
		{
			const uint32_t dataModelsNum = dataModels->getSize();
			HashEntry<ja::Resource<ja::DataModel>>* pHashEntry = dataModels->getFirst();

			for (uint32_t i = 0; i < dataModelsNum; ++i)
			{
				DataModel* dataModel = pHashEntry[i].data->resource;

				dataModel->updateScriptFunctionReference(oldReference, newReference);
			}
		}
		
		ja::DataComponent* itData = this->data;
		
		while (nullptr != itData)
		{
			if (itData->initDataFunctionRef == oldReference)
				itData->initDataFunctionRef = newReference;

			itData = (ja::DataComponent*)itData->next;
		}
	}

	DataComponent* DataGroup::createComponent(Entity* entity)
	{
		DataComponent* newData = new (cacheLineAllocator->allocate(sizeof(DataComponent))) DataComponent(this);

		if (data == nullptr)
		{
			data = newData;
		}
		else
		{
			newData->next  = data;
			data->previous = newData;
			data           = newData;
		}

		entity->addComponent(newData);

		return newData;
	}

	void DataGroup::deleteComponent(EntityComponent* component)
	{
		DataComponent* deletedComponent = (DataComponent*)component;

		if (nullptr != deletedComponent->allocatedData)
			cacheLineAllocator->free(deletedComponent->allocatedData, deletedComponent->allocatedDataSize);

		if (deletedComponent == data)
		{
			data = (DataComponent*)(deletedComponent->next);
		}

		if (deletedComponent->previous != nullptr)
			deletedComponent->previous->next = deletedComponent->next;

		if (deletedComponent->next != nullptr)
			deletedComponent->next->previous = deletedComponent->previous;

		cacheLineAllocator->free(deletedComponent, sizeof(DataComponent));
	}

	DataComponent::DataComponent(EntityGroup* componentGroup) : EntityComponent(componentGroup, (uint8_t)EntityGroupType::DATA), dataModel(nullptr), allocatedData(nullptr), allocatedDataSize(0)
	{

	}

	void DataComponent::setDataModel(DataModel* dataModel)
	{
		if (this->dataModel == dataModel)
			return;
		
		uint32_t newDataSize      = dataModel->getDataSize();
		uint8_t* newAllocatedData = (uint8_t*)this->componentGroup->cacheLineAllocator->allocate(newDataSize);

		if (nullptr != dataModel)
		{
			/*const ScriptFunction* accessDataFunction = dataModel->getAccessDataFunction();

			if (nullptr != accessDataFunction)
			{
				this->dataAccessFunctionRef = accessDataFunction->functionRef;
			}
			else
			{
				this->dataAccessFunctionRef = 0;
			}*/

			const ScriptFunction* initDataFunction = dataModel->getInitDataFunction();

			if (nullptr != initDataFunction)
			{
				this->initDataFunctionRef = initDataFunction->functionRef;
			}
			else
			{
				this->initDataFunctionRef = 0;
			}
		}	

		if (nullptr != this->dataModel)
		{
			//Copy old data
			StackAllocator<DataFieldInfo>* newDataFieldStack = dataModel->getDataFields();
			const uint32_t                 newDataFieldsNum  = newDataFieldStack->getSize();
			DataFieldInfo*                 pNewDataField     = newDataFieldStack->getFirst();

			StackAllocator<DataFieldInfo>* oldDataFieldStack = this->dataModel->getDataFields();
			const uint32_t                 oldDataFieldsNum  = oldDataFieldStack->getSize();
			DataFieldInfo*                 pOldDataField     = oldDataFieldStack->getFirst();

			for (uint32_t i = 0; i < newDataFieldsNum; ++i)
			{
				for (uint32_t z = 0; z < oldDataFieldsNum; ++z)
				{
					if ( (pNewDataField[i].dataNameHash  == pOldDataField[z].dataNameHash) &&
						 (pNewDataField[i].dataType      == pOldDataField[z].dataType) &&
						 (pNewDataField[i].dataFieldSize == pOldDataField[z].dataFieldSize))
					{
						memcpy(newAllocatedData + pNewDataField[i].dataFieldOffset, (uint8_t*)allocatedData + pOldDataField[z].dataFieldOffset, pNewDataField[i].dataFieldSize);
					}
					else
					{
						memset(newAllocatedData + pNewDataField[i].dataFieldOffset, 0, pNewDataField[i].dataFieldSize);
					}
				}
			}

			this->componentGroup->cacheLineAllocator->free(this->allocatedData, this->allocatedDataSize);
		}		

		this->allocatedDataSize = newDataSize;
		this->allocatedData     = newAllocatedData;
		this->dataModel         = dataModel;
	}

	void DataComponent::initData()
	{
		if (0 != this->initDataFunctionRef)
		{
			if (this->initDataFunctionRef != 0)
			{
				lua_State* L = ScriptManager::getSingleton()->getLuaState();
				lua_rawgeti(L, LUA_REGISTRYINDEX, this->initDataFunctionRef);
				lua_pushlightuserdata(L, this);
				lua_call(L, 1, 0);
			}
		}
		else
		{
			zeroAllocatedMem();
		}
	}

	void DataComponent::onUpdateOtherComponent(EntityComponent* component)
	{

	}

	void DataComponent::onDeleteOtherComponent(EntityComponent* component)
	{

	}

}
