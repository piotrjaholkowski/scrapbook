#pragma once

#include "Entity.hpp"
#include "../Manager/ScriptManager.hpp"
#include "../Scripts/Script.hpp"

namespace ja
{

	class ScriptComponent : public EntityComponent
	{
	private:
		int32_t  updateRef;
		uint32_t updateHash;
		uint32_t updateFileHash;
	public:
		ScriptComponent(EntityGroup* componentGroup);

		void setUpdateFunction(const ScriptFunction& scriptFunction);

		inline uint32_t getUpdateFileHash() const
		{
			return this->updateFileHash;
		}

		inline uint32_t getUpdateHash() const
		{
			return this->updateHash;
		}

		void onUpdateOtherComponent(EntityComponent* component);
		void onDeleteOtherComponent(EntityComponent* component);

		friend class ScriptGroup;
	};

	class ScriptGroup : public EntityGroup
	{
	private:
		lua_State* L;
		float               deltaTime;
		ScriptComponent*    scripts;
		static ScriptGroup* singleton;

		ja::Texture2d* textureImage;
	public:
		ScriptGroup(CacheLineAllocator* cacheLineAllocator);
		~ScriptGroup();

		inline static ScriptGroup* getSingleton()
		{
			return singleton;
		}

		ScriptComponent* createComponent(Entity* entity);
		void deleteComponent(EntityComponent* component);
		void update(float deltaTime);
		void updateComponent(ScriptComponent* scriptComponent);
		void updateScriptFunctionReference(int32_t oldFunctionReference, int32_t newFunctionReference);

		inline Texture2d* getTextureImage() const
		{
			return this->textureImage;
		}
	};

}
