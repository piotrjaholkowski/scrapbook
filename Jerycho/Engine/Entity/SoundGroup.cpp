#include "SoundGroup.hpp"
#include "../Manager/SoundManager.hpp"
#include "../Manager/ResourceManager.hpp"

namespace ja
{

SoundGroup::SoundGroup(CacheLineAllocator* cacheLineAllocator) : EntityGroup(cacheLineAllocator), soundEmitters(nullptr)
{
	soundManager      = SoundManager::getSingleton();
	soundStreamBuffer = new int8_t[1024 * 1024 * 4]; // 4MB
}

SoundGroup::~SoundGroup()
{
	delete[] soundStreamBuffer;
}

void SoundGroup::update(float deltaTime)
{
	for(SoundComponent* itSoundEmitter = soundEmitters; itSoundEmitter != nullptr; itSoundEmitter = (SoundComponent*)itSoundEmitter->next)
	{
		updateComponent(itSoundEmitter);
	}
}

EntityComponent* SoundGroup::createComponent()
{
	SoundComponent* soundComponent = new (cacheLineAllocator->allocate(sizeof(SoundComponent))) SoundComponent(this);

	alGenSources(1, &soundComponent->soundEmitterId);

	soundComponent->setRelativeToListener(false);
	soundComponent->setPosition(0,0,0);
	soundComponent->setRolloffFactor(1);
	soundComponent->setDirection(0,0,0);
	soundComponent->setGain(1);
	soundComponent->setPitch(1);
	soundComponent->setMaxDistance(25.0);
	soundComponent->setReferenceDistance(15.0);
	soundComponent->setVelocity(0,0,0);
	
	if(soundEmitters == nullptr)
	{
		soundEmitters = soundComponent;
	}
	else
	{
		soundComponent->next = soundEmitters;
		soundEmitters->previous = soundComponent;
		soundEmitters = soundComponent;
	}

	return soundComponent;
}

SoundStreamer* SoundGroup::createSoundStreamer()
{
	SoundStreamer* soundStreamer = new (cacheLineAllocator->allocate(sizeof(SoundStreamer))) SoundStreamer();
	alGenBuffers(setup::sound::STREAM_BUFFS_NUM, soundStreamer->buffers);
	return soundStreamer;
}

void SoundGroup::deleteComponent( EntityComponent* component )
{
	if(component == soundEmitters)
	{
		soundEmitters = (SoundComponent*)(component->next);
	}

	if(component->previous != nullptr)
		component->previous->next = component->next;

	if(component->next != nullptr)
		component->next->previous = component->previous;

	SoundComponent* soundComponent = (SoundComponent*)(component);
	soundComponent->stop();
	alDeleteSources(1, &soundComponent->soundEmitterId);

	if (soundComponent->getSoundComponentType() == (uint8_t)SoundComponentType::SOUND_STREAMER) {
		deleteSoundStreamer(soundComponent->getSoundStreamer());
	}
	
	cacheLineAllocator->free(component,sizeof(SoundComponent));
}

void SoundGroup::deleteSoundStreamer(SoundStreamer* soundStreamer)
{
	alDeleteBuffers(setup::sound::STREAM_BUFFS_NUM, soundStreamer->buffers);
	cacheLineAllocator->free(soundStreamer, sizeof(SoundStreamer));
}

void SoundGroup::updateSoundStreamer(SoundComponent* soundComponent)
{
	SoundStream* soundStream = soundComponent->getSoundStream();
	SoundStreamer* soundStreamer = soundComponent->getSoundStreamer();

	if (soundStream == nullptr)
		return;

	if ((soundStreamer->state != AL_PLAYING) && (soundStreamer->state != AL_INITIAL) && (soundStreamer->state != AL_END_OF_LOOP))
		return;

	bool isEndOfStream;
	ALint processedBuffersNum;
	ALuint soundBuffers[setup::sound::STREAM_BUFFS_NUM];
	int bytesToRead = 64 * ((soundStream->freq * soundStream->channelsNum * soundStream->depth) / 1000); // 64 ms to read

	if (soundStreamer->state == AL_INITIAL) {
		ALint state;
		alGetSourcei(soundComponent->soundEmitterId, AL_SOURCE_STATE, &state);
		if (state != AL_PLAYING) {
			soundStreamer->bitStream = 0;
			soundStreamer->setSample(0);
			soundStreamer->loadSamplePosition();
			alSourceStop(soundComponent->soundEmitterId);
			alSourcei(soundComponent->soundEmitterId, AL_BUFFER, 0);
			processedBuffersNum = soundStream->getBufferDataFromStream(setup::sound::STREAM_BUFFS_NUM, (char*)soundStreamBuffer, soundStreamer->bitStream, bytesToRead, soundStreamer->buffers, isEndOfStream);
			alSourceQueueBuffers(soundComponent->soundEmitterId, processedBuffersNum, soundStreamer->buffers);
			soundStreamer->state = AL_STOPPED;
			soundStreamer->saveSamplePosition();
		}
		return;
	}

	if (soundStreamer->state == AL_END_OF_LOOP) {
		ALint state;
		alGetSourcei(soundComponent->soundEmitterId, AL_SOURCE_STATE, &state);
		if (state == AL_STOPPED) {
			alSourceStop(soundComponent->soundEmitterId);
			alSourcei(soundComponent->soundEmitterId, AL_BUFFER, 0);
			soundStreamer->setSample(0);
			soundStreamer->loadSamplePosition();
			processedBuffersNum = soundStream->getBufferDataFromStream(setup::sound::STREAM_BUFFS_NUM, (char*)soundStreamBuffer, soundStreamer->bitStream, bytesToRead, soundStreamer->buffers, isEndOfStream);
			alSourceQueueBuffers(soundComponent->soundEmitterId, processedBuffersNum, soundStreamer->buffers);
			soundStreamer->saveSamplePosition();
			soundStreamer->state = AL_PLAYING;
		}
		return;
	}

	ALint state;
	ALint readBuffersNum = 0;
	alGetSourcei(soundComponent->soundEmitterId, AL_SOURCE_STATE, &state);

	soundStreamer->loadSamplePosition();
	alGetSourcei(soundComponent->soundEmitterId, AL_BUFFERS_PROCESSED, &processedBuffersNum);
	if (processedBuffersNum > 0) {
		alSourceUnqueueBuffers(soundComponent->soundEmitterId, processedBuffersNum, soundBuffers);
		readBuffersNum = soundStream->getBufferDataFromStream(processedBuffersNum, (char*)soundStreamBuffer, soundStreamer->bitStream, bytesToRead, soundBuffers, isEndOfStream);
		if (isEndOfStream)
		{
			if (readBuffersNum != 0)
				alSourceQueueBuffers(soundComponent->soundEmitterId, readBuffersNum, soundBuffers);
			if (soundStreamer->isLoop) {
				soundStreamer->state = AL_END_OF_LOOP;
			} else {
				soundStreamer->state = AL_INITIAL;
			}
			soundStreamer->setSample(0); // stop and rewind
			return;
		}
		else {
			alSourceQueueBuffers(soundComponent->soundEmitterId, readBuffersNum, soundBuffers);
			soundStreamer->saveSamplePosition();
		}
	}

	if (state != AL_PLAYING)
		alSourcePlay(soundComponent->soundEmitterId);
}

SoundComponent::SoundComponent( EntityGroup* componentGroup ) : EntityComponent(componentGroup,(uint8_t)EntityGroupType::SOUND), soundEffect(nullptr), soundStreamer(nullptr), physicObject(nullptr)
{

}

void SoundComponent::onUpdateOtherComponent( EntityComponent* component )
{
	if(component->getComponentType() != (uint8_t)EntityGroupType::PHYSIC_OBJECT)
		return;

	PhysicComponent* physicComponent  = (PhysicComponent*)(component);
	PhysicComponent* currentComponent = nullptr;

	if(physicObject != nullptr)
	{
		currentComponent = (PhysicComponent*)(physicObject->getComponentData());
	}

	if(physicComponent == currentComponent)
	{
		physicObject = physicComponent->getPhysicObject();
	}
}

void SoundComponent::onDeleteOtherComponent( EntityComponent* component )
{
	if(component->getComponentType() != (uint8_t)EntityGroupType::PHYSIC_OBJECT)
		return;

	PhysicComponent* physicComponent  = (PhysicComponent*)(component);
	PhysicComponent* currentComponent = nullptr;

	if(physicObject != nullptr)
	{
		currentComponent = (PhysicComponent*)(physicObject->getComponentData());
	}

	if(physicComponent == currentComponent)
	{
		physicObject = nullptr;
	}
}

void SoundComponent::setPhysicComponent( PhysicComponent* physicComponent )
{
	physicComponent->setReferenced(true);
	physicObject = physicComponent->getPhysicObject();
}

}

