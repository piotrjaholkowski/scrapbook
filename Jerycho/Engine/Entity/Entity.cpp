#include "Entity.hpp"
#include "../Manager/ScriptManager.hpp"

namespace ja
{

HashMap<EntityHashTagName>    Entity::hashTagDictionary;
HashMap<ComponentHashTagName> EntityComponent::hashTagDictionary;

const char* Entity::getHashTagName(uint32_t hash)
{
	uint32_t                      hashNum  = hashTagDictionary.getSize();
	HashEntry<EntityHashTagName>* hashTags = hashTagDictionary.getFirst();

	for (uint32_t i = 0; i < hashNum; ++i)
	{
		if (hash == hashTags[i].hash)
		{
			return hashTags[i].data->getName();
		}
	}

	return nullptr;
}

uint32_t Entity::createHashTag(const char* name)
{
	EntityHashTagName hashTag;
	hashTag.setName(name);

	const uint32_t                hashNum  = hashTagDictionary.getSize();
	HashEntry<EntityHashTagName>* hashTags = hashTagDictionary.getFirst();

	for (uint32_t i = 0; i < hashNum; ++i)
	{
		if (hashTags[i].data->getHash() == hashTag.getHash())
		{
			return hashTag.getHash();
		}
	}

	hashTagDictionary.add(hashTag.getHash(), new EntityHashTagName(hashTag));

	return hashTag.getHash();
}

uint32_t Entity::rename(uint32_t oldHash, const char* newName)
{
	return createHashTag(newName);
}

void Entity::clearDictionary()
{
	uint32_t                      hashNum  = hashTagDictionary.getSize();
	HashEntry<EntityHashTagName>* hashTags = hashTagDictionary.getFirst();

	for (uint32_t i = 0; i < hashNum; ++i)
	{
		delete hashTags[i].data;
	}

	hashTagDictionary.clear();
}

const char* EntityComponent::getHashTagName(uint32_t hash)
{
	uint32_t                         hashNum  = hashTagDictionary.getSize();
	HashEntry<ComponentHashTagName>* hashTags = hashTagDictionary.getFirst();

	for (uint32_t i = 0; i < hashNum; ++i)
	{
		if (hash == hashTags[i].hash)
		{
			return hashTags[i].data->getName();
		}
	}

	return nullptr;
}

uint32_t EntityComponent::createHashTag(const char* name)
{
	ComponentHashTagName hashTag;
	hashTag.setName(name);

	const uint32_t                   hashNum  = hashTagDictionary.getSize();
	HashEntry<ComponentHashTagName>* hashTags = hashTagDictionary.getFirst();

	for (uint32_t i = 0; i < hashNum; ++i)
	{
		if (hashTags[i].data->getHash() == hashTag.getHash())
		{
			return hashTag.getHash();
		}
	}

	hashTagDictionary.add(hashTag.getHash(), new ComponentHashTagName(hashTag));

	return hashTag.getHash();
}

uint32_t EntityComponent::rename(uint32_t oldHash, const char* newName)
{
	return createHashTag(newName);
}

void EntityComponent::clearDictionary()
{
	uint32_t                         hashNum  = hashTagDictionary.getSize();
	HashEntry<ComponentHashTagName>* hashTags = hashTagDictionary.getFirst();

	for (uint32_t i = 0; i < hashNum; ++i)
	{
		delete hashTags[i].data;
	}

	hashTagDictionary.clear();
}

void Entity::setTagName(const char* tagName)
{
	this->hash = Entity::createHashTag(tagName);
}

const char* Entity::getTagName() const
{
	return Entity::getHashTagName(this->hash);
}

void EntityComponent::setTagName(const char* tagName)
{
	this->hash = EntityComponent::createHashTag(tagName);
}

const char* EntityComponent::getTagName() const
{
	return EntityComponent::getHashTagName(this->hash);
}

}