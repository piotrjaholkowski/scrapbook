#include "PhysicGroup.hpp"
#include "../Manager/PhysicsManager.hpp"
#include "../Manager/ResourceManager.hpp"

namespace ja
{ 

PhysicGroup* PhysicGroup::singleton;

PhysicGroup::PhysicGroup(CacheLineAllocator* cacheLineAllocator) : EntityGroup(cacheLineAllocator), physicObjects(nullptr)
{
	physicsManager = PhysicsManager::getSingleton();
	singleton = this;

	Resource<Texture2d>* resource = ResourceManager::getSingleton()->loadEternalTexture2d("entity_icons/physic_object.png");

	if (nullptr != resource)
	{
		this->textureImage = resource->resource;
	}

	std::cout << "PhysicComponent size: " << sizeof(PhysicComponent) << std::endl;
}

void PhysicGroup::update( float deltaTime )
{
	this->deltaTime = deltaTime;
}

PhysicComponent* PhysicGroup::createComponent(Entity* entity)
{
	PhysicComponent* physicObject = new (cacheLineAllocator->allocate(sizeof(PhysicComponent))) PhysicComponent(this);

	if(physicObjects == nullptr)
	{
		physicObjects = physicObject;
	}
	else
	{
		physicObject->next = physicObjects;
		physicObjects->previous = physicObject;
		physicObjects = physicObject;
	}

	entity->addComponent(physicObject);

	return physicObject;
}

void PhysicGroup::deleteComponent( EntityComponent* component )
{
	PhysicComponent* physicComponent = (PhysicComponent*)(component);
	if(component == physicObjects)
	{
		physicObjects = (PhysicComponent*)(component->next);
	}

	if(component->previous != nullptr)
		component->previous->next = component->next;

	if(component->next != nullptr)
		component->next->previous = component->previous;

	if(physicComponent->getPhysicObject() != nullptr)
	{
		if(physicComponent->autoDelete) // delete physicObject assigned to entity
		{
			if(component->getEntity()->getPhysicObject() == physicComponent->getPhysicObject()) // check if physic object is root
			{
				component->getEntity()->physicObject = nullptr;
			}
			// it is set to avoid slow delete time
			physicComponent->physicObject->componentData = nullptr;
			physicsManager->deletePhysicObjectImmediate(physicComponent->physicObject);
		}
		else
		{
			physicComponent->physicObject->componentData = nullptr; // make physic object avaliable to other entities
		}
	}
	
	cacheLineAllocator->free(component,sizeof(PhysicComponent));
}

void PhysicGroup::updateScriptFunctionReference(int32_t oldReference, int32_t newReference)
{
	for (PhysicComponent* itPhysic = physicObjects; itPhysic != nullptr; itPhysic = (PhysicComponent*)(itPhysic->next))
	{
		if (itPhysic->onBeginConstraintRef == oldReference)
		{
			itPhysic->onBeginConstraintRef = newReference;
		}

		if (itPhysic->onEndConstraintRef == oldReference)
		{
			itPhysic->onEndConstraintRef = newReference;
		}
	}
}

PhysicComponent::PhysicComponent( EntityGroup* componentGroup ) : EntityComponent(componentGroup,(uint8_t)EntityGroupType::PHYSIC_OBJECT), physicObject(nullptr), autoDelete(true),
onBeginConstraintRef(0), onBeginConstraintHash(0), onBeginConstraintFileHash(0),
onEndConstraintRef(0), onEndConstraintHash(0), onEndConstraintFileHash(0)
{

}

void PhysicComponent::onUpdateOtherComponent( EntityComponent* component )
{
	//There is no other components in this component
}

void PhysicComponent::onDeleteOtherComponent( EntityComponent* component )
{
	//There is no other components in this component
}

void PhysicComponent::callBeginConstraint(Constraint2d* constraint)
{
	if (0 != onBeginConstraintRef)
	{
		lua_State* L = ScriptManager::getSingleton()->getLuaState();
		lua_rawgeti(L, LUA_REGISTRYINDEX, onBeginConstraintRef);
		lua_pushlightuserdata(L, this);
		lua_pushlightuserdata(L, constraint);
		lua_call(L, 2, 0);	
	}
}

void PhysicComponent::callEndConstraint(Constraint2d* constraint)
{
	if (0 != onEndConstraintRef)
	{
		lua_State* L = ScriptManager::getSingleton()->getLuaState();
		lua_rawgeti(L, LUA_REGISTRYINDEX, onEndConstraintRef);
		lua_pushlightuserdata(L, this);
		lua_pushlightuserdata(L, constraint);
		lua_call(L, 2, 0);
	}
}

void PhysicComponent::setPhysicObject( PhysicObject2d* physicObject )
{
	PhysicObject2d* tempPhysic = nullptr;

	if(nullptr != this->physicObject)
		tempPhysic = this->physicObject;
	
	this->physicObject = physicObject;
	if(nullptr != physicObject)
		this->physicObject->componentData = this;

	updateRelations(); // Notifies other components which uses this component
	if(tempPhysic != nullptr)
		tempPhysic->componentData = nullptr;
}

void PhysicComponent::setRoot()
{
	if (nullptr != this->physicObject)
		this->getEntity()->physicObject = this->physicObject;
}

bool PhysicComponent::isRoot()
{
	if (nullptr == this->getEntity()->physicObject)
		return false;

	return (this->getEntity()->physicObject == this->physicObject);
}

void PhysicComponent::setBeginConstraintFunction(const ScriptFunction& scriptFunction)
{
	this->onBeginConstraintRef      = scriptFunction.functionRef;
	this->onBeginConstraintHash     = scriptFunction.functionHash;
	this->onBeginConstraintFileHash = scriptFunction.fileHash;
}

void PhysicComponent::setEndConstraintFunction(const ScriptFunction& scriptFunction)
{
	this->onEndConstraintRef      = scriptFunction.functionRef;
	this->onEndConstraintHash     = scriptFunction.functionHash;
	this->onEndConstraintFileHash = scriptFunction.fileHash;
}


}