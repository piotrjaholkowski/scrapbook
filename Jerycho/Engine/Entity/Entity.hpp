#pragma once

#include "../jaSetup.hpp"
#include "../Math/Math.hpp"
#include "../Physics/PhysicObject2d.hpp"
#include "../Graphics/Texture2d.hpp"
#include "../Allocators/HashMap.hpp"
#include "../Utility/NameTag.hpp"

namespace ja
{

enum class EntityGroupType
{
	PHYSIC_OBJECT   = 0,
	PARTICLES       = 1,
	SOUND           = 2,
	SCRIPT          = 3,
	LAYER_OBJECT    = 4,
	DATA            = 5
};

enum class EntityComponentFlag
{
	COMPONENT_REFERENCED = 1 // if component with this flag is referenced by other components
};

class Entity;
class EntityComponent;
class EntityGroup;

typedef NameTag<setup::TAG_NAME_LENGTH>    EntityHashTagName;
typedef NameTag<setup::TAG_NAME_LENGTH> ComponentHashTagName;

class EntityGroup
{
public:
	CacheLineAllocator* cacheLineAllocator;
	EntityGroup(CacheLineAllocator* cacheLineAllocator) : cacheLineAllocator(cacheLineAllocator)
	{

	}

	virtual ~EntityGroup()
	{

	}

	virtual EntityComponent* createComponent(Entity*) = 0;
	virtual void deleteComponent(EntityComponent* component) = 0;
	virtual void update(float deltaTime) = 0;
	virtual ja::Texture2d* getTextureImage() const = 0;
	virtual void updateScriptFunctionReference(int32_t oldFunctionReference, int32_t newFunctionReference) = 0;
};

class EntityComponent
{
private:
	static HashMap<ComponentHashTagName> hashTagDictionary;

	static void        clearDictionary();
	static const char* getHashTagName(uint32_t hash);
	static uint32_t    createHashTag(const char* name);
	static uint32_t    rename(uint32_t oldHash, const char* newName);
protected:
	EntityGroup* componentGroup;
public:
	EntityComponent* next; // next component of that same type
	EntityComponent* previous; // previous component of that same type

	EntityComponent* nextComponent; // next component of entity
	EntityComponent* previousComponent; // previous component of entity
protected:
	Entity*  entity;
	uint32_t hash;

	uint8_t componentType;
protected:
	uint8_t componentFlag;

public:

	EntityComponent(EntityGroup* componentGroup, uint8_t componentType) : componentGroup(componentGroup), componentType(componentType), next(nullptr), previous(nullptr), nextComponent(nullptr), previousComponent(nullptr), entity(nullptr), componentFlag(0), hash(0)
	{

	}

	virtual ~EntityComponent()
	{

	}

	inline EntityGroup* getComponentGroup() const
	{
		return componentGroup;
	}

	inline uint8_t getComponentType() const
	{
		return componentType;
	}

	inline Entity* getEntity() const
	{
		return this->entity;
	}

	inline EntityComponent* getNext() const
	{
		return next;
	}

	inline EntityComponent* getPrevious() const
	{
		return previous;
	}

	inline EntityComponent* getNextComponent() const
	{
		return nextComponent;
	}

	inline EntityComponent* getPreviousComponent() const
	{
		return previousComponent;
	}

	void        setTagName(const char* tagName);
	const char* getTagName() const;

	inline uint32_t getHash() const
	{
		return hash;
	}

	//Method called by Entity to which component belongs to.
	//To udpate relation with sent component
	virtual void onUpdateOtherComponent(EntityComponent* component) = 0;
	//Method called by Entity to which component belongs to.
	//To remove relation with sent component
	virtual void onDeleteOtherComponent(EntityComponent* component) = 0;

	inline bool isReferenced() const
	{
		if (componentFlag & (uint8_t)EntityComponentFlag::COMPONENT_REFERENCED)
			return true;
		return false;
	}

	inline void setReferenced(bool state)
	{
		if (state)
			componentFlag |= (uint8_t)EntityComponentFlag::COMPONENT_REFERENCED;
		else
			componentFlag ^= (uint8_t)EntityComponentFlag::COMPONENT_REFERENCED;
	}

	//Updates relation of this component with any other components
	inline void updateRelations();

	//Remove relation of this component with any other components
	inline void removeRelations();

	//This method is called when entity is deleted
	inline void deleteComponent();

	friend class Entity;
	friend class EntityManager;
};

class Entity
{
private:
	static HashMap<EntityHashTagName> hashTagDictionary;

	static void        clearDictionary();
	static const char* getHashTagName(uint32_t hash);
	static uint32_t    createHashTag(const char* name);
	static uint32_t    rename(uint32_t oldHash, const char* newName);

	EntityComponent* components;
	PhysicObject2d*  physicObject;
	uint32_t         hash;

	uint16_t id;
	
public:
	Entity() : components(nullptr), physicObject(nullptr)
	{
		hash = 0;
	}

	void        setTagName(const char* tagName);
	const char* getTagName() const;

	inline uint16_t getId() const
	{
		return id;
	}

	inline uint32_t getHash() const
	{
		return hash;
	}

	inline PhysicObject2d* getPhysicObject() const
	{
		return physicObject;
	}

	inline EntityComponent* getComponent(uint8_t componentType, const char* hashTagName) const
	{
		uint32_t hash = ja::math::convertToHash(hashTagName);

		for (EntityComponent* itComp = components; itComp != nullptr; itComp = itComp->nextComponent)
		{
			if (itComp->componentType == componentType)
			{
				if (itComp->hash == hash)
					return itComp;
			}
		}

		return nullptr;
	}

private:
	inline void addComponent(EntityComponent* component)
	{
		if (components == nullptr)
		{
			components = component;
		}
		else
		{
			EntityComponent* itComp;

			for (itComp = components; itComp->nextComponent != nullptr; itComp = itComp->nextComponent)
			{
			}

			itComp->nextComponent = component;
			component->previousComponent = itComp;
		}

		component->entity = this;
	}
public:

	inline EntityComponent* getComponent(uint8_t componentType) const
	{
		for (EntityComponent* itComp = components; itComp != nullptr; itComp = itComp->nextComponent)
		{
			if (itComp->componentType == componentType)
			{
				return itComp;
			}
		}

		return nullptr;
	}

	//It returns number of components in array of pointers
	inline uint8_t getComponents(uint8_t componentType, EntityComponent** arrayPointer, uint8_t arraySize) const
	{
		uint8_t componentNum = 0;
		for (EntityComponent* itComp = components; itComp != nullptr; itComp = itComp->nextComponent)
		{
			if (itComp->componentType == componentType)
			{
				arrayPointer[componentNum] = itComp;
				++componentNum;
				if (componentNum == arraySize)
				{
					break;
				}
			}
		}

		return componentNum;
	}

	inline EntityComponent* getSubComponent(uint8_t componentType, uint8_t id) const
	{
		uint8_t tempId = 0;
		for (EntityComponent* itComp = components; itComp != nullptr; itComp = itComp->nextComponent)
		{
			if (itComp->componentType == componentType)
			{
				if (tempId == id)
					return itComp;
				++tempId;
			}
		}

		return nullptr;
	}

	inline void deleteComponents()
	{
		for (EntityComponent* itComp = components; itComp != nullptr; itComp = itComp->nextComponent)
		{
			itComp->componentGroup->deleteComponent(itComp);
		}
	}

	inline EntityComponent* getFirstComponent() const
	{
		return components;
	}

	
	friend class PhysicGroup;
	friend class PhysicComponent;
	friend class LayerObjectGroup;
	friend class ScriptGroup;
	friend class DataGroup;
	friend class DataComponent;
	friend class EntityManager;
	friend class EntityComponent;
};

void EntityComponent::updateRelations()
{
	for (EntityComponent* itComp = entity->components; itComp != nullptr; itComp = itComp->nextComponent)
	{
		itComp->onUpdateOtherComponent(this);
	}
}

void EntityComponent::removeRelations()
{
	for (EntityComponent* itComp = entity->components; itComp != nullptr; itComp = itComp->nextComponent)
	{
		itComp->onDeleteOtherComponent(this);
	}
}

void EntityComponent::deleteComponent()
{
	if (isReferenced())
		removeRelations();

	componentGroup->deleteComponent(this);

	if (entity->components == this)
	{
		entity->components = nextComponent;
	}

	if (nextComponent != nullptr)
	{
		nextComponent->previousComponent = previousComponent;
	}

	if (previousComponent != nullptr)
	{
		previousComponent->nextComponent = nextComponent;
	}
}

}