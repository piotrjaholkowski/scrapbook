#pragma once

#include "Entity.hpp"
#include "../Manager/ScriptManager.hpp"
#include "../Scripts/Script.hpp"

namespace ja
{

	class PhysicComponent : public EntityComponent
	{
	private:
		PhysicObject2d* physicObject;

		int32_t  onBeginConstraintRef;
		uint32_t onBeginConstraintHash;
		uint32_t onBeginConstraintFileHash;

		int32_t  onEndConstraintRef;
		uint32_t onEndConstraintHash;
		uint32_t onEndConstraintFileHash;
	public:
		bool autoDelete;
		PhysicComponent(EntityGroup* componentGroup);
		void setPhysicObject(PhysicObject2d* physicObject);
		void setRoot();
		bool isRoot();

		inline PhysicObject2d* getPhysicObject() const
		{
			return physicObject;
		}

		void onUpdateOtherComponent(EntityComponent* component);
		void onDeleteOtherComponent(EntityComponent* component);
		
		inline uint32_t getOnBeginConstraintHash() const
		{
			return onBeginConstraintHash;
		}

		inline uint32_t getOnBeginConstraintFileHash() const
		{
			return onBeginConstraintFileHash;
		}

		inline uint32_t getOnEndConstraintHash() const
		{
			return onEndConstraintHash;
		}

		inline uint32_t getOnEndConstraintFileHash() const
		{
			return onEndConstraintFileHash;
		}

		void setBeginConstraintFunction(const ScriptFunction& scriptFunction);
		void setEndConstraintFunction(const ScriptFunction& scriptFunction);

		void callBeginConstraint(Constraint2d* constraint);
		void callEndConstraint(Constraint2d* constraint);

		friend class PhysicGroup;
	};

	class PhysicGroup : public EntityGroup
	{
	private:
		float deltaTime;
		PhysicComponent* physicObjects;
		PhysicsManager*  physicsManager;
		static PhysicGroup* singleton;

		Texture2d* textureImage;
	public:

		PhysicGroup(CacheLineAllocator* cacheLineAllocator);

		inline static PhysicGroup* getSingleton()
		{
			return singleton;
		}

		PhysicComponent* createComponent(Entity* entity);
		void deleteComponent(EntityComponent* component);
		void update(float deltaTime);
		void updateComponent(EntityComponent* component);
		void updateScriptFunctionReference(int32_t oldReference, int32_t newReference);

		inline Texture2d* getTextureImage() const
		{
			return textureImage;
		}
	};

}