#pragma once

#include "Entity.hpp"
#include "PhysicGroup.hpp"
#include "../Physics/PhysicObject2d.hpp"
#include "../Manager/LayerObjectManager.hpp"

namespace ja
{

	enum class LayerObjectComponentFlag
	{
		AUTO_DELETE    = 1,
		FIXED_ROTATION = 2
	};

	class LayerObjectComponent;

	class LayerObjectElement
	{
	private:
		LayerObjectElement* next;
	public:
		LayerObject* layerObject;

		jaVector2 positionDiff;
		jaMatrix2 rotDiff;

		LayerObjectElement(LayerObjectComponent* layerObjectComponent, LayerObject* layerObject) : next(nullptr), layerObject(layerObject)
		{
			layerObject->componentData = layerObjectComponent;
		}

		inline LayerObjectElement* getNext() const
		{
			return next;
		}

		friend class LayerObjectComponent;
		friend class LayerObjectGroup;
	};

	class LayerObjectComponent : public EntityComponent
	{
	private:
		PhysicObject2d*     physicObject;
		LayerObjectElement* layerObjectCollection;
	public:
		uint16_t layerObjectFlag;

		inline void updateLocationData()
		{
			if ((layerObjectCollection != nullptr) && (physicObject != nullptr))
			{
				LayerObjectElement*  itLayerObjectCollection = layerObjectCollection;
				gil::ObjectHandle2d* physicHandle = physicObject->getShapeHandle();

				while (itLayerObjectCollection != nullptr)
				{
					gil::ObjectHandle2d* layerObjectHandle = itLayerObjectCollection->layerObject->getHandle();
					itLayerObjectCollection->positionDiff  = physicHandle->transform.position - layerObjectHandle->transform.position;
					itLayerObjectCollection->rotDiff       = physicHandle->transform.rotationMatrix * jaVectormath2::inverse(layerObjectHandle->transform.rotationMatrix);
					itLayerObjectCollection                = itLayerObjectCollection->next;
				}
			}
		}
	public:
		LayerObjectComponent(EntityGroup* componentGroup);

		inline LayerObjectElement* getLayerObjectCollection() const
		{
			return layerObjectCollection;
		}

		inline void setAutoDelete(bool state)
		{
			if (state)
			{
				layerObjectFlag |= (uint16_t)LayerObjectComponentFlag::AUTO_DELETE;
			}
			else
			{
				layerObjectFlag |= (uint16_t)LayerObjectComponentFlag::AUTO_DELETE;
				layerObjectFlag ^= (uint16_t)LayerObjectComponentFlag::AUTO_DELETE;
			}
		}

		inline void setFixedRotation(bool state)
		{
			if (state)
			{
				layerObjectFlag |= (uint16_t)LayerObjectComponentFlag::FIXED_ROTATION;
			}
			else
			{
				layerObjectFlag |= (uint16_t)LayerObjectComponentFlag::FIXED_ROTATION;
				layerObjectFlag ^= (uint16_t)LayerObjectComponentFlag::FIXED_ROTATION;
			}
		}

		inline bool isAutoDelete() const
		{
			if (layerObjectFlag & (uint16_t)LayerObjectComponentFlag::AUTO_DELETE)
				return true;
			return false;
		}

		inline bool isFixedRotation() const
		{
			if (layerObjectFlag & (uint16_t)LayerObjectComponentFlag::FIXED_ROTATION)
				return true;
			return false;
		}

		inline PhysicObject2d* getPhysicObject() const
		{
			return physicObject;
		}

		void addLayerObject(LayerObject* layerObject);

		inline void clearLayerObjects()
		{
			CacheLineAllocator* cacheLineAllocator   = componentGroup->cacheLineAllocator;
			LayerObjectElement* itLayerObjectElement = layerObjectCollection;
			LayerObjectElement* deleteLayerObjectElement;

			while (itLayerObjectElement != nullptr)
			{
				itLayerObjectElement->layerObject->componentData = nullptr;
				deleteLayerObjectElement = itLayerObjectElement;
				itLayerObjectElement = itLayerObjectElement->next;
				cacheLineAllocator->free(deleteLayerObjectElement, sizeof(LayerObjectElement));
			}

			layerObjectCollection = nullptr;
		}

		void setPhysicComponent(PhysicComponent* component);
		void onUpdateOtherComponent(EntityComponent* component);
		void onDeleteOtherComponent(EntityComponent* component);

		friend class LayerObjectGroup;
	};

	class LayerObjectGroup : public EntityGroup
	{
	private:
		LayerObjectManager*      layerObjectManager;
		LayerObjectComponent*    layerObjectComponents;
		static LayerObjectGroup* singleton;
		Texture2d*               textureImage;
	public:
		LayerObjectGroup(CacheLineAllocator* cacheLineAllocator);
		~LayerObjectGroup();

		inline static LayerObjectGroup* getSingleton()
		{
			return singleton;
		}

		LayerObjectComponent* createComponent(Entity* entity);
		void deleteComponent(EntityComponent* component);
		void update(float deltaTime);

		void updateComponent(LayerObjectComponent* layerObjectComponent, float deltaTime);

		void updateScriptFunctionReference(int32_t oldFunctionReference, int32_t newFunctionReference);
		

		inline Texture2d* getTextureImage() const
		{
			return this->textureImage;
		}
	};

}