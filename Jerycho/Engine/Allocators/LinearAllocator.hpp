#pragma once

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

class LinearBlock
{
private:
	LinearBlock* previous;
public:
	int8_t*  dataPointer;
	uint32_t blockSize;
	
	LinearBlock(int8_t* dataPointer, uint32_t blockSize, LinearBlock* previous = nullptr) : dataPointer(dataPointer), blockSize(blockSize), previous(previous)
	{

	}

	~LinearBlock()
	{
		if(nullptr != this->dataPointer)
		{
			free(this->dataPointer);
			this->dataPointer = nullptr;
			this->blockSize = 0;
		}
	}

	inline LinearBlock* getPrevious()
	{
		return this->previous;
	}
};

//It's for allocating objects which won't be released
//dynamically but all at one time
class LinearAllocator
{
private:
	int8_t*      currentBlockData;
	int8_t*      currentBlockDataAligned;
	LinearBlock* activeBlock;
	uint32_t     blockSize;
	uint32_t     spaceLeft;
	uint32_t     alignTo;
public:

	LinearAllocator(uint32_t blockSize, uint32_t alignTo) : blockSize(blockSize), alignTo(alignTo)
	{
		this->currentBlockData        = (int8_t*)malloc(this->blockSize + this->alignTo);
		this->currentBlockDataAligned = (int8_t*)((uintptr_t)this->currentBlockData + ((uintptr_t)this->alignTo - ((uintptr_t)this->currentBlockData % this->alignTo)));

		this->spaceLeft   = this->blockSize;
		this->activeBlock = new LinearBlock(this->currentBlockData, this->blockSize);
	}

	~LinearAllocator()
	{
		LinearBlock* tempBlock = this->activeBlock;

		while(nullptr != tempBlock->getPrevious())
		{
			this->activeBlock = tempBlock;
			tempBlock         = this->activeBlock->getPrevious();
			delete this->activeBlock;
		}

		delete tempBlock;
		activeBlock = nullptr;
	}

	//Clear but don't free allocated memory
	inline void clear()
	{
		if(this->activeBlock == nullptr)
			return;

		LinearBlock* tempBlock = this->activeBlock;

		uint32_t blocksNum = 1;
		while(nullptr != tempBlock->getPrevious())
		{
			this->activeBlock = tempBlock;
			tempBlock         = tempBlock->getPrevious();
			delete this->activeBlock;
			++blocksNum;
		}

		if(blocksNum > 1) 
		{
			delete tempBlock;
			spaceLeft         = 0;
			this->blockSize   = blocksNum * this->blockSize; // resize block
			this->activeBlock = nullptr;
		}
		else 
		{
			spaceLeft = blockSize;
			activeBlock = tempBlock;
		}
	}

	inline void* allocate(uint32_t allocationSize)
	{
		if (0 == allocationSize)
			return nullptr;

		if(allocationSize > this->blockSize)
		{
			this->blockSize = allocationSize;
		}

		if(this->spaceLeft >= allocationSize)
		{
			uint32_t temp    = this->blockSize - this->spaceLeft;
			this->spaceLeft -= allocationSize;			
			return (void*)(this->currentBlockDataAligned + temp);
		}
		else
		{
			this->currentBlockData        = (int8_t*)malloc(this->blockSize + this->alignTo);
			this->currentBlockDataAligned = (int8_t*)((uintptr_t)this->currentBlockData + ((uintptr_t)this->alignTo - ((uintptr_t)this->currentBlockData % this->alignTo)));
			this->activeBlock             = new LinearBlock(this->currentBlockData,this->blockSize,this->activeBlock);
			this->spaceLeft               = this->blockSize - allocationSize;
			return this->currentBlockDataAligned;
		}
	}

	uint32_t getAllocatedMemorySize()
	{
		uint32_t allocatedMemory = 0;

		if(nullptr != this->activeBlock)
		{
			LinearBlock* tempBlock = this->activeBlock;
			allocatedMemory = tempBlock->blockSize;

			while(nullptr != tempBlock->getPrevious())
			{
				allocatedMemory += tempBlock->blockSize;
				tempBlock        = tempBlock->getPrevious();
			}
		}

		return allocatedMemory;
	}

	uint32_t getAllocatedBlocksNum()
	{
		if(nullptr != activeBlock)
		{
			LinearBlock* tempBlock = activeBlock;
			uint32_t     blocksNum = 1;

			while(nullptr != tempBlock->getPrevious())
			{
				++blocksNum;
				tempBlock = tempBlock->getPrevious();
			}

			return blocksNum;
		}

		return 0;
	}

	uint32_t getFreeSpaceAtCurrentBlock()
	{
		return spaceLeft;
	}
};

