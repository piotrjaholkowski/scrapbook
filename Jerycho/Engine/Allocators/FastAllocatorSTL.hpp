#pragma once

#include "../jaSetup.hpp"

#include <stdint.h>
#include <iostream>

namespace ja
{

	class FastAllocatorSTLMemBlock
	{
	public:
		static FastAllocatorSTLMemBlock* activeMemBlock;
	private:
		FastAllocatorSTLMemBlock* previousMemBlock;
		uint8_t*                  freeDataOffset;
		int32_t                   freeSpaceLeft;

		static void**  freeSpaceLookupTable[setup::fastAllocatorSTL::freeSpaceLookupTableSize];
		uint8_t        memBlockData[setup::fastAllocatorSTL::memBlockSize];

		inline FastAllocatorSTLMemBlock(FastAllocatorSTLMemBlock* previousMemBlock) : previousMemBlock(previousMemBlock)
		{
			this->freeDataOffset = memBlockData;
			this->freeSpaceLeft = setup::fastAllocatorSTL::memBlockSize;
		}

		static inline int8_t findLookupTableIndexForFreeSpace(const int32_t memorySize, int32_t& actualMemorySize)
		{
			actualMemorySize = setup::fastAllocatorSTL::smallestAllocationSize;

			if (memorySize <= actualMemorySize)
				return 0;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 1;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 2;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 3;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 4;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 5;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 6;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 7;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 8;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 9;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 10;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 11;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 12;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 13;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 14;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 15;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 16;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 17;
			actualMemorySize = actualMemorySize << 1;

			if (memorySize <= actualMemorySize)
				return 18;
			actualMemorySize = actualMemorySize << 1;

			return 19;
		}

		static inline int8_t findLookupTableIndexForSpaceLeft(const int32_t spaceLeft)
		{
			int32_t actualMemorySize;

			int8_t freeIndex = findLookupTableIndexForFreeSpace(spaceLeft, actualMemorySize);

			if (spaceLeft == actualMemorySize)
				return freeIndex;

			--freeIndex;
			return freeIndex;
		}

	public:

		inline void* allocate(const int32_t memorySize)
		{
			if (0 == memorySize)
				return nullptr;

			int32_t      actualMemorySize;
			const int8_t lookupIndex = FastAllocatorSTLMemBlock::findLookupTableIndexForFreeSpace(memorySize, actualMemorySize);

			if (FastAllocatorSTLMemBlock::freeSpaceLookupTable[lookupIndex] != nullptr)
			{
				void* freedSpacePtr = (void*)FastAllocatorSTLMemBlock::freeSpaceLookupTable[lookupIndex];
				FastAllocatorSTLMemBlock::freeSpaceLookupTable[lookupIndex] = (void**)(*FastAllocatorSTLMemBlock::freeSpaceLookupTable[lookupIndex]);
				return freedSpacePtr;
			}

			if (this->freeSpaceLeft >= actualMemorySize)
			{
				this->freeSpaceLeft -= actualMemorySize;
				uint8_t* allocatedMem = freeDataOffset;
				this->freeDataOffset = this->freeDataOffset + actualMemorySize;

				return allocatedMem;
			}
			else
			{
				int8_t freedLookupIndex = findLookupTableIndexForSpaceLeft(freeSpaceLeft);

				if (freedLookupIndex >= 0)
				{

					*((void**)freeDataOffset) = FastAllocatorSTLMemBlock::freeSpaceLookupTable[freedLookupIndex];
					FastAllocatorSTLMemBlock::freeSpaceLookupTable[freedLookupIndex] = (void**)freeDataOffset;
				}

				FastAllocatorSTLMemBlock::activeMemBlock = new FastAllocatorSTLMemBlock(FastAllocatorSTLMemBlock::activeMemBlock);
				return FastAllocatorSTLMemBlock::activeMemBlock->allocate(actualMemorySize);
			}

			return nullptr;
		}

		inline void deallocate(void* pointer, int32_t memorySize)
		{
			if (0 == memorySize)
				return;

			int32_t actualMemorySize;
			int32_t freedLookupIndex = FastAllocatorSTLMemBlock::findLookupTableIndexForFreeSpace(memorySize, actualMemorySize);

			*((void**)pointer) = FastAllocatorSTLMemBlock::freeSpaceLookupTable[freedLookupIndex];
			FastAllocatorSTLMemBlock::freeSpaceLookupTable[freedLookupIndex] = (void**)pointer;
		}

		friend class FastAllocatorSTLInitializer;
	};

	

	class FastAllocatorSTLInitializer
	{
	public:
		FastAllocatorSTLInitializer()
		{
			FastAllocatorSTLMemBlock::activeMemBlock = new FastAllocatorSTLMemBlock(nullptr);

			memset(FastAllocatorSTLMemBlock::freeSpaceLookupTable, 0, sizeof(FastAllocatorSTLMemBlock::freeSpaceLookupTable));
		}

		~FastAllocatorSTLInitializer()
		{
		}
	};

	extern FastAllocatorSTLInitializer FastAllocatorInit;
	

	template<typename _Tp> class FastAllocatorSTL
	{
	public:
		typedef _Tp               value_type;
		typedef value_type*       pointer;
		typedef const value_type* const_pointer;
		typedef value_type&       reference;
		typedef const value_type& const_reference;

		typedef std::size_t     size_type;
		typedef std::ptrdiff_t  difference_type;

		inline FastAllocatorSTL() {}

		inline ~FastAllocatorSTL() {}


		inline FastAllocatorSTL(FastAllocatorSTL const&) {}

		template<typename U>
		inline FastAllocatorSTL(FastAllocatorSTL<U> const&) {}

		template<typename U>
		class rebind { 
		public:
			typedef FastAllocatorSTL<U> other; 
		};

		pointer address(reference r) { return &r; }
		const_pointer address(const_reference r) { return &r; }

		pointer allocate(size_type count, const void* = 0)
		{
			return (pointer)FastAllocatorSTLMemBlock::activeMemBlock->allocate(count * sizeof(_Tp));
		}

		void deallocate(pointer p, size_type n)
		{
			FastAllocatorSTLMemBlock::activeMemBlock->deallocate((uint8_t*)p, n * sizeof(_Tp));
		}

		size_type max_size() const
		{
			return (size_type)(-1);
		}

		void construct(pointer p, const value_type& x)
		{
			new(p)value_type(x);
		}

		void destroy(pointer p)
		{
			p->~value_type();
		}
	};

}

template <typename _Tp>
inline bool operator==(const ja::FastAllocatorSTL<_Tp>& a, const ja::FastAllocatorSTL<_Tp>& b)
{
	if (&a == &b)
	{
		return true;
	}

	return false;
}

template <typename _Tp>
inline bool operator!=(const ja::FastAllocatorSTL<_Tp>& a, const ja::FastAllocatorSTL<_Tp>& b)
{
	if (&a != &b)
	{
		return true;
	}

	return false;
}