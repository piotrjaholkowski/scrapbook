#include "FastAllocatorSTL.hpp"

namespace ja{

	//FastAllocatorSTLMemBlock initialization
	void**                    FastAllocatorSTLMemBlock::freeSpaceLookupTable[setup::fastAllocatorSTL::freeSpaceLookupTableSize];
	FastAllocatorSTLMemBlock* FastAllocatorSTLMemBlock::activeMemBlock;

	FastAllocatorSTLInitializer FastAllocatorInit;
}
