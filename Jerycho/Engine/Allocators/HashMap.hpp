#pragma once

#include "../Math/Math.hpp"

#include <stdint.h>
#include <string>

template <typename T>
class HashEntry
{
public:
	uint32_t hash;
	T*       data;
};



template <typename T>
class HashMap
{
private:
	HashEntry<T>* hashEntries;
	uint32_t      size;
	uint32_t      allocatedNum;
public:

	HashMap()
	{
		this->size = 0;
		this->allocatedNum = 8;
		hashEntries = (HashEntry<T>*)malloc(sizeof(HashEntry<T>) * this->allocatedNum);		
	}

	~HashMap()
	{
		free(this->hashEntries);
	}

	inline T* find(uint32_t hash) const
	{
		for (uint32_t i = 0; i < size; ++i)
		{
			if (hashEntries[i].hash == hash)
			{
				return hashEntries[i].data;
			}
		}

		return nullptr;
	}

	inline T* find(const char* name) const
	{
		uint32_t hash = ja::math::convertToHash(name);

		for (uint32_t i = 0; i < size; ++i)
		{
			if (hashEntries[i].hash == hash)
			{
				return hashEntries[i].data;
			}
		}

		return nullptr;
	}

	static uint32_t getHash(const char* name) 
	{
		return ja::math::convertToHash(name);
	}

	inline void add(uint32_t hash, T* element)
	{
		if (this->size == this->allocatedNum)
		{
			this->allocatedNum = 2 * this->allocatedNum;

			HashEntry<T>* temp = (HashEntry<T>*)malloc(sizeof(HashEntry<T>) * this->allocatedNum);
			memcpy(temp, this->hashEntries, sizeof(HashEntry<T>) * this->size);
			free(this->hashEntries);
			this->hashEntries = temp;
		}

		this->hashEntries[this->size].hash = hash;
		this->hashEntries[this->size].data = element;

		this->size++;
	}

	inline uint32_t add(const char* name, T* elelment)
	{
		uint32_t hash = ja::math::convertToHash(name);

		if (this->size == this->allocatedNum)
		{
			this->allocatedNum = 2 * this->allocatedNum;

			HashEntry<T>* temp = (HashEntry<T>*)malloc(sizeof(HashEntry<T>) * this->allocatedNum);
			memcpy(temp, this->hashEntries, sizeof(HashEntry<T> * this->size));
			free(this->hashEntries);
			this->hashEntries = temp;
		}
		
		this->hashEntries[this->size].hash = hash;
		this->hashEntries[this->size].data = element;

		this->size++

		return hash;
	}

	inline void remove(uint32_t hash)
	{
		uint32_t i = 0;

		for (i; i < size; ++i)
		{
			if (this->hashEntries[i].hash == hash)
				break;
		}

		if (i == size) // Element not found
			return;
		
		uint32_t elementId = ((uintptr_t)(&this->hashEntries[i]) - (uintptr_t)(this->hashEntries)) / sizeof(HashEntry<T>);

		if (size > 1)
			memcpy(&this->hashEntries[elementId], &this->hashEntries[elementId + 1], (size - elementId - 1) * sizeof(HashEntry<T>));


		--size;
	}

	inline uint32_t getSize() const
	{
		return size;
	}

	inline void clear()
	{
		size = 0;
	}

	HashEntry<T>* getFirst() const
	{
		return this->hashEntries;
	}

};