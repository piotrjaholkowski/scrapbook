#pragma once

#include <iostream>
#include <cstring>
#include <assert.h>
#include <stdint.h>

#include "CacheLineAllocator.hpp"

template <typename T>
class StackPoolAllocator
{
private:
	T*       stackObjects = nullptr;
	uint32_t size;
	uint32_t capacity;
	void*    allocatedMem = nullptr;
	uint32_t alignTo;

	CacheLineAllocator* cacheLineAllocator;
public:
	StackPoolAllocator(CacheLineAllocator* pCacheLineAllocator);
	StackPoolAllocator(int32_t capacity, int32_t alignTo, CacheLineAllocator* pCacheLineAllocator);
	~StackPoolAllocator();
	inline void push(const T& element);
	inline T*   push();
	inline T*   pushArray(uint32_t objectsNum);
	inline T pop();
	inline uint32_t getSize() const;
	inline void setSize(uint32_t size);
	inline uint32_t getCapacity();
	inline void clear();
	inline T* getFirst() const;
	inline T* getLast() const;
	inline void resizeMem(uint32_t memSize);
	inline void resizeCapacity(uint32_t capacity);
	inline void erase(T* pointer);

	inline CacheLineAllocator* getMemoryAllocator();
};

template <typename T>
void StackPoolAllocator<T>::erase(T* pointer)
{
	uint32_t elementId = ((uintptr_t)(pointer)-(uintptr_t)(this->stackObjects)) / sizeof(T);

	if (this->size > 1)
		memcpy(&this->stackObjects[elementId], &this->stackObjects[elementId + 1], (this->size - elementId - 1) * sizeof(T));

	--size;
}

template <typename T>
StackPoolAllocator<T>::StackPoolAllocator(CacheLineAllocator* pCacheLineAllocator) : size(0), capacity(8), alignTo(32), cacheLineAllocator(pCacheLineAllocator)
{
	this->allocatedMem = this->cacheLineAllocator->allocate((this->capacity * sizeof(T)) + this->alignTo);
	this->stackObjects = (T*)((uintptr_t)this->allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
}

template <typename T>
StackPoolAllocator<T>::StackPoolAllocator(int32_t capacity, int32_t alignTo, CacheLineAllocator* pCacheLineAllocator) : size(0), capacity(capacity), alignTo(alignTo), cacheLineAllocator(pCacheLineAllocator)
{
	this->allocatedMem = this->cacheLineAllocator->allocate((this->capacity * sizeof(T)) + this->alignTo);
	this->stackObjects = (T*)((uintptr_t)this->allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
}

template <typename T>
StackPoolAllocator<T>::~StackPoolAllocator()
{
	if (this->allocatedMem != nullptr)
	{
		this->cacheLineAllocator->free(this->allocatedMem, (this->capacity * sizeof(T)) + this->alignTo);
		this->allocatedMem = nullptr;
		this->stackObjects = nullptr;
	}
}

template <typename T>
void StackPoolAllocator<T>::push(const T& element)
{
	if (this->size == this->capacity)
	{
		void* oldAllocatedMem = this->allocatedMem;
		T*    oldStackObjects = this->stackObjects;

		//this->capacity *= 2;
		const uint32_t newCapacity = this->capacity * 2;

		this->allocatedMem = this->cacheLineAllocator.allocate((newCapacity * sizeof(T)) + this->alignTo);
		this->stackObjects = (T*)((uintptr_t)this->allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
		std::memcpy(this->stackObjects, oldStackObjects, this->size * sizeof(T));

		this->cacheLineAllocator.free(oldAllocatedMem, (this->capacity * sizeof(T)) + this->alignTo);

		this->capacity = newCapacity;
	}

	stackObjects[this->size] = element;
	++this->size;
}

template <typename T>
T* StackPoolAllocator<T>::push()
{
	if (this->size == this->capacity)
	{
		void* oldAllocatedMem = this->allocatedMem;
		T*    oldStackObjects = this->stackObjects;

		//this->capacity *= 2;
		uint32_t newCapacity = this->capacity * 2;

		this->allocatedMem = this->cacheLineAllocator->allocate((newCapacity * sizeof(T)) + this->alignTo);
		this->stackObjects = (T*)((uintptr_t)this->allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
		std::memcpy(this->stackObjects, oldStackObjects, this->size * sizeof(T));

		this->cacheLineAllocator->free(oldAllocatedMem, (this->capacity * sizeof(T)) + this->alignTo);
		this->capacity = newCapacity;
	}

	return &this->stackObjects[this->size++];
}

template <typename T>
T StackPoolAllocator<T>::pop()
{
	--this->size;
	return this->stackObjects[this->size];
}

template <typename T>
void StackPoolAllocator<T>::resizeMem(uint32_t memSize)
{

	if (memSize > (sizeof(T) * capacity))
	{
		void* oldAllocatedMem = allocatedMem;
		T* oldStackObjects = stackObjects;

		//capacity = memSize / sizeof(T);
		uint32_t newCapacity = memSize / sizeof(T);

		allocatedMem = this->cacheLineAllocator->allocate((newCapacity * sizeof(T)) + alignTo);
		stackObjects = (T*)((uintptr_t)allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
		std::memcpy(stackObjects, oldStackObjects, size * sizeof(T));

		this->cacheLineAllocator->free(oldAllocatedMem, (this->capacity * sizeof(T)) + this->alignTo);
		this->capacity = newCapacity;
	}
}

template <typename T>
void StackPoolAllocator<T>::resizeCapacity(uint32_t newCapacity)
{
	if (newCapacity > this->capacity)
	{
		void* oldAllocatedMem = allocatedMem;
		T*    oldStackObjects = stackObjects;

		allocatedMem = this->cacheLineAllocator->allocate((newCapacity * sizeof(T)) + alignTo);
		stackObjects = (T*)((uintptr_t)allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
		std::memcpy(stackObjects, oldStackObjects, size * sizeof(T));

		this->cacheLineAllocator->free(oldAllocatedMem, (this->capacity * sizeof(T)) + this->alignTo);
		this->capacity = newCapacity;
	}
}

template <typename T>
T* StackPoolAllocator<T>::pushArray(uint32_t objectsNum)
{
	const uint32_t arraySize     = objectsNum * sizeof(T);
	const uint32_t allocatedSize = this->capacity * sizeof(T);
	const uint32_t actualSize    = this->size * sizeof(T);
	const uint32_t newSize       = actualSize + arraySize;

	if (newSize > allocatedSize)
	{
		void* oldAllocatedMem = this->allocatedMem;
		T*    oldStackObjects = this->stackObjects;

		//this->capacity = newSize / sizeof(T);
		uint32_t newCapacity = newSize / sizeof(T);

		this->allocatedMem = this->cacheLineAllocator->allocate((newCapacity * sizeof(T)) + this->alignTo);
		this->stackObjects = (T*)((uintptr_t)this->allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
		std::memcpy(this->stackObjects, oldStackObjects, this->size * sizeof(T));

		this->cacheLineAllocator->free(oldAllocatedMem, (this->capacity * sizeof(T)) + this->alignTo);
		this->capacity = newCapacity;
	}

	uint32_t arrayStart = this->size;
	this->size += objectsNum;

	return &this->stackObjects[arrayStart];
}

template <typename T>
inline T* StackPoolAllocator<T>::getFirst() const
{
	return this->stackObjects;
}

template <typename T>
inline T* StackPoolAllocator<T>::getLast() const
{
	return &this->stackObjects[size - 1];
}

template <typename T>
uint32_t StackPoolAllocator<T>::getSize() const
{
	return this->size;
}

template <typename T>
void StackPoolAllocator<T>::setSize(uint32_t size)
{
	this->size = size;
}

template <typename T>
uint32_t StackPoolAllocator<T>::getCapacity()
{
	return this->capacity;
}

template <typename T>
void StackPoolAllocator<T>::clear()
{
	this->size = 0;
}

template <typename T>
CacheLineAllocator* StackPoolAllocator<T>::getMemoryAllocator()
{
	return this->cacheLineAllocator;
}
