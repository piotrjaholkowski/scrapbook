#pragma once

#include <iostream>
#include <cstring>
#include <assert.h>
#include <stdint.h>

template <typename T>
class StackAllocator
{
private:
	T*       stackObjects = nullptr;
	uint32_t size;
	uint32_t capacity;
	void*    allocatedMem = nullptr;
	uint32_t alignTo;
public:
	StackAllocator();
	StackAllocator(int32_t capacity, int32_t alignTo);
	~StackAllocator();
	inline void push(const T& element);
	inline T*   push();
	inline T*   pushArray(uint32_t objectsNum);
	inline T pop();
	inline uint32_t getSize() const;
	inline void setSize(uint32_t size);
	inline uint32_t getCapacity();
	inline void clear();
	inline T* getFirst() const;
	inline T* getLast() const;
	inline void resize(uint32_t memSize);
	inline void erase(T* pointer);
};

template <typename T>
void StackAllocator<T>::erase(T* pointer)
{
	uint32_t elementId = ((uintptr_t)(pointer) - (uintptr_t)(this->stackObjects)) / sizeof(T);

	if (this->size > 1)
		memcpy(&this->stackObjects[elementId], &this->stackObjects[elementId + 1], (this->size - elementId - 1) * sizeof(T));

	--size;
}

template <typename T>
StackAllocator<T>::StackAllocator() : size(0), capacity(8), alignTo(32)
{
	this->allocatedMem = malloc( (this->capacity * sizeof(T)) + this->alignTo);
	this->stackObjects = (T*)((uintptr_t)this->allocatedMem + ( (uintptr_t)this->alignTo - ( (uintptr_t)this->allocatedMem % this->alignTo) ) );
}

template <typename T>
StackAllocator<T>::StackAllocator(int32_t capacity, int32_t alignTo) : size(0), capacity(capacity), alignTo(alignTo)
{
	this->allocatedMem = malloc((this->capacity * sizeof(T)) + this->alignTo);
	this->stackObjects = (T*)((uintptr_t)this->allocatedMem + ( (uintptr_t)this->alignTo - ( (uintptr_t)this->allocatedMem % this->alignTo) ) );
}

template <typename T>
StackAllocator<T>::~StackAllocator()
{
	if (this->allocatedMem != nullptr)
	{
		free(this->allocatedMem);
		this->allocatedMem = nullptr;
		this->stackObjects = nullptr;
	}
}

template <typename T>
void StackAllocator<T>::push(const T& element)
{
	if (this->size == this->capacity)
	{
		void* oldAllocatedMem = this->allocatedMem;
		T*    oldStackObjects = this->stackObjects;

		this->capacity *= 2;

		this->allocatedMem = malloc((this->capacity * sizeof(T)) + this->alignTo);
		this->stackObjects = (T*)((uintptr_t)this->allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
		std::memcpy(this->stackObjects, oldStackObjects, this->size * sizeof(T));

		free(oldAllocatedMem);
	}

	stackObjects[this->size] = element;
	++this->size;
}

template <typename T>
T* StackAllocator<T>::push()
{
	if (this->size == this->capacity)
	{
		void* oldAllocatedMem = this->allocatedMem;
		T*    oldStackObjects = this->stackObjects;

		this->capacity *= 2;

		this->allocatedMem = malloc((this->capacity * sizeof(T)) + this->alignTo);
		this->stackObjects = (T*)((uintptr_t)this->allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
		std::memcpy(this->stackObjects, oldStackObjects, this->size * sizeof(T));

		free(oldAllocatedMem);
	}

	return &this->stackObjects[this->size++];
}

template <typename T>
T StackAllocator<T>::pop()
{
	--this->size;
	return this->stackObjects[this->size];
}

template <typename T>
void StackAllocator<T>::resize(uint32_t memSize)
{

	if ( memSize > (sizeof(T) * capacity) )
	{
		void* oldAllocatedMem = allocatedMem;
		T* oldStackObjects = stackObjects;

		capacity = memSize / sizeof(T);

		allocatedMem = malloc((capacity * sizeof(T)) + alignTo);
		stackObjects = (T*)((uintptr_t)allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
		std::memcpy(stackObjects, oldStackObjects, size * sizeof(T));

		free(oldAllocatedMem);
	}
}

template <typename T>
T* StackAllocator<T>::pushArray(uint32_t objectsNum)
{
	const uint32_t arraySize     = objectsNum * sizeof(T);
	const uint32_t allocatedSize = this->capacity * sizeof(T);
	const uint32_t actualSize    = this->size * sizeof(T);
	const uint32_t newSize       = actualSize + arraySize;

	if (newSize > allocatedSize)
	{
		void* oldAllocatedMem = this->allocatedMem;
		T*    oldStackObjects = this->stackObjects;

		this->capacity = newSize / sizeof(T);

		this->allocatedMem = malloc((this->capacity * sizeof(T)) + this->alignTo);
		this->stackObjects = (T*)((uintptr_t)this->allocatedMem + ((uintptr_t)this->alignTo - ((uintptr_t)this->allocatedMem % this->alignTo)));
		std::memcpy(this->stackObjects, oldStackObjects, this->size * sizeof(T));

		free(oldAllocatedMem);
	}

	uint32_t arrayStart  = this->size;
	this->size          += objectsNum;

	return &this->stackObjects[arrayStart];
}

template <typename T>
inline T* StackAllocator<T>::getFirst() const
{
	return this->stackObjects;
}

template <typename T>
inline T* StackAllocator<T>::getLast() const
{
	return &this->stackObjects[size - 1];
}

template <typename T>
uint32_t StackAllocator<T>::getSize() const
{
	return this->size;
}

template <typename T>
void StackAllocator<T>::setSize(uint32_t size)
{
	this->size = size;
}

template <typename T>
uint32_t StackAllocator<T>::getCapacity()
{
	return this->capacity;
}

template <typename T>
void StackAllocator<T>::clear()
{
	this->size = 0;
}
