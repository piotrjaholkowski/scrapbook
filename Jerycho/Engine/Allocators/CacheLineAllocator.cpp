#include "CacheLineAllocator.hpp"

#include "../Manager/LogManager.hpp"

void* CacheLineAllocator::allocate(uint32_t objectSize)
{
	if (0 == objectSize)
		return nullptr;

	void* pAllocatedData;


	if (objectSize <= largestPoolAllocationSize)
	{
		//look in released mem
		int32_t freeListId = (objectSize / this->cacheLineSize) + (((objectSize % this->cacheLineSize) != 0) ? 1 : 0);

		if (freeList[freeListId].freeChunks != nullptr)
		{
			pAllocatedData = (void*)freeList[freeListId].freeChunks;
			freeList[freeListId].freeChunks = (void**)(*freeList[freeListId].freeChunks);
			return pAllocatedData;
		}

		pAllocatedData = activeBlock->allocate(freeList[freeListId].size);
		if (nullptr == pAllocatedData)
		{
			uint32_t size;
			pAllocatedData = activeBlock->getFreeSpace(&size);
			free(pAllocatedData, size);
			activeBlock = new CacheLineBlock(this->blockSize, this->cacheLineSize, activeBlock);
			pAllocatedData = activeBlock->allocate(objectSize);
		}

		/*if (log)
		{
			ja::LogManager::getSingleton()->addLogMessage(ja::LogType::INFO_LOG, "allocation size %d %p", objectSize, pAllocatedData);
		}*/

		return pAllocatedData;
	}

	void* pChunkInfo;
	pChunkInfo = this->allocate(sizeof(CacheLineLargeChunk)); // allocate pointer
	uint8_t* pLargeChunkData = (uint8_t*)malloc(objectSize + this->cacheLineSize);
	pAllocatedData = pLargeChunkData + (this->cacheLineSize - (uint32_t)((uintptr_t)pLargeChunkData % this->cacheLineSize));
	largeTypeList = new (pChunkInfo)CacheLineLargeChunk(pLargeChunkData, pAllocatedData, largeTypeList);
	return pAllocatedData;
}

void CacheLineAllocator::clear()
{
	if (activeBlock == nullptr)
		return;

	//Release large allocated types
	for (CacheLineLargeChunk* itLargeChunk = largeTypeList; itLargeChunk != nullptr; itLargeChunk = itLargeChunk->previous)
	{
		::free(itLargeChunk->allocatedChunkData);
	}

	this->largeTypeList = nullptr;

	CacheLineBlock* tempBlock = activeBlock;

	while (tempBlock->getPrevious() != nullptr)
	{
		activeBlock = tempBlock;
		tempBlock = tempBlock->getPrevious();
		delete activeBlock;
	}

	activeBlock = tempBlock;
	activeBlock->clear(); // clear but don't free memory

	for (uint32_t i = 0; i<CACHE_LINE_ALLOCATOR_LARGEST_MULTIPLIER; ++i)
	{
		freeList[i].freeChunks = nullptr;
	}

	/*if (log)
	{
		ja::LogManager::getSingleton()->addLogMessage(ja::LogType::INFO_LOG, "CLEAR MEM");
	}*/
}

void CacheLineAllocator::free(void* pointer, uint32_t objectSize)
{
	if (0 == objectSize)
		return;

	/*if (log)
	{
		ja::LogManager::getSingleton()->addLogMessage(ja::LogType::INFO_LOG, "free size %d %p", objectSize, pointer);
	}*/

	if (objectSize <= largestPoolAllocationSize)
	{
		int32_t freeListId = (objectSize / this->cacheLineSize) + (((objectSize % this->cacheLineSize) != 0) ? 1 : 0);
		*((void**)pointer) = freeList[freeListId].freeChunks; // Write to address at pointer position of previous free chunk
		freeList[freeListId].freeChunks = (void**)pointer; // Set free chunk pointer at address of pointer
		return;
	}
	else
	{
		for (CacheLineLargeChunk* itLargeChunk = largeTypeList; itLargeChunk != nullptr; itLargeChunk = itLargeChunk->previous)
		{
			if (itLargeChunk->alignedData == pointer)
			{
				::free(itLargeChunk->allocatedChunkData);

				if (nullptr != itLargeChunk->previous)
				{
					itLargeChunk->previous->next = itLargeChunk->next;
				}

				if (nullptr != itLargeChunk->next)
				{
					itLargeChunk->next->previous = itLargeChunk->previous;
				}
				else
				{
					largeTypeList = itLargeChunk->previous;
				}

				this->free(itLargeChunk, sizeof(CacheLineLargeChunk));
				return;
			}
		}
	}
}