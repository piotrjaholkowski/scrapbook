#pragma once
#include <assert.h>
#include <iostream>

template <class T>
class PoolBlock
{
private:
	int8_t*  data;
	T*       nextChunk; // next free memory chunk
	uint32_t allocatedObjectsNum;
	uint32_t blockSize;
	uint32_t allocatedMem;
	uint32_t offset; // where allocation of data begins
	PoolBlock<T>* previous; // used by higher order allocator
public:
	PoolBlock(uint32_t blockSize, uint32_t alignedTo, PoolBlock<T>* previous = nullptr);
	inline T* allocate(); // return null if there is no more space
	inline PoolBlock<T>* getPrevious();
	void* getSegment(uint32_t& offset);
	void clear();
	~PoolBlock();
};

template <class T>
PoolBlock<T>* PoolBlock<T>::getPrevious()
{
	return previous;
}

template <class T>
void* PoolBlock<T>::getSegment(uint32_t& offset)
{
	offset = this->offset;
	return data;
}

template <class T>
void PoolBlock<T>::clear()
{
	this->allocatedMem     = 0;
	int8_t* alignedAddress = (int8_t*)((uintptr_t)(this->data) + this->offset);
	nextChunk = (T*)alignedAddress;
}

template <class T>
PoolBlock<T>::PoolBlock(uint32_t blockSize, uint32_t alignedTo, PoolBlock<T>* previous = nullptr) : previous(previous)
{
	this->blockSize = blockSize;
	this->data      = (int8_t*)malloc(this->blockSize + alignedTo);
	this->offset    = (alignedTo - (uint32_t)((uintptr_t)this->data % alignedTo));
	
	this->allocatedMem     = 0;
	int8_t* alignedAddress = (int8_t*)( (uintptr_t)(this->data) + this->offset );

	this->nextChunk = (T*)alignedAddress;
}

template <class T>
T* PoolBlock<T>::allocate()
{
	allocatedMem += sizeof(T);
	if (allocatedMem > blockSize)
		return nullptr;

	return nextChunk++;
}

template <class T>
PoolBlock<T>::~PoolBlock()
{
	if (data != nullptr)
	{
		free(data);
		data = nullptr;
	}
}

template<class T>
class PoolAllocator
{
private:
	uint32_t blockSize;
	uint32_t alignedTo;
	void**   freeChunk; // pointer to last free chunk of data

	PoolBlock<T>* activeMemBlock;
public:
	PoolAllocator(uint32_t blockSize, uint32_t alignedTo);
	~PoolAllocator();

	T* allocate();
	void free(T* pointer);
	void clear();
	void* getFirstSegment(uint32_t& offset);
};

template <class T>
PoolAllocator<T>::PoolAllocator(uint32_t blockSize, uint32_t alignedTo)
{
	assert(sizeof(T) >= sizeof(void*));
	this->alignedTo = alignedTo;
	this->blockSize = blockSize - (blockSize % sizeof(T));

	activeMemBlock = new PoolBlock<T>(blockSize, alignedTo, nullptr);
	freeChunk = nullptr;
}

template <class T>
T* PoolAllocator<T>::allocate()
{
	T* chunk;

	if (nullptr != freeChunk)
	{
		chunk = (T*)freeChunk;
		freeChunk = (void**)(*freeChunk);
	}
	else
	{
		chunk = activeMemBlock->allocate();
		if (nullptr == chunk)
		{
			activeMemBlock = new PoolBlock<T>(blockSize, alignedTo, activeMemBlock);
			chunk = activeMemBlock->allocate();
		}
	}

	return chunk;
}

template <class T>
void PoolAllocator<T>::free(T* pointer)
{
	*((void**)pointer) = freeChunk; // Write to address at pointer position of previous free chunk
	freeChunk = (void**)pointer;    // Set free chunk pointer at address of pointer
}

template <class T>
PoolAllocator<T>::~PoolAllocator()
{
	if (activeMemBlock == nullptr)
		return;

	PoolBlock<T>* tempBlock = activeMemBlock;

	while (tempBlock->getPrevious() != nullptr)
	{
		activeMemBlock = tempBlock;
		tempBlock = tempBlock->getPrevious();
		delete activeMemBlock;
	}

	delete tempBlock;
	activeMemBlock = nullptr;
}

template <class T>
void PoolAllocator<T>::clear()
{
	if (activeMemBlock == nullptr)
		return;

	PoolBlock<T>* tempBlock = activeMemBlock;

	while (tempBlock->getPrevious() != nullptr)
	{
		activeMemBlock = tempBlock;
		tempBlock = tempBlock->getPrevious();
		delete activeMemBlock;
	}

	activeMemBlock = tempBlock;
	activeMemBlock->clear();
}
