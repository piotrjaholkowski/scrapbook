#pragma once

#include <algorithm> 
#include <vector>
#include <limits.h>
#include <assert.h>
#include <stdint.h>
#include <iostream>

#define CACHE_LINE_ALLOCATOR_LARGEST_MULTIPLIER 640

class CacheLineBlock
{
private:
	uint32_t        allocatedData;
	int8_t*         nextChunk;
	int8_t*         alignedData;
	int8_t*         data;
	
	uint32_t        blockSize;
	
	CacheLineBlock* previous;
	

public:
	CacheLineBlock(uint32_t blockSize, uint32_t cacheLineSize, CacheLineBlock* previous = nullptr) : allocatedData(0), previous(previous), blockSize(blockSize)
	{
		data          = (int8_t*)malloc(blockSize + cacheLineSize);
		alignedData   = data + (cacheLineSize - (uint32_t)((uintptr_t)this->data % cacheLineSize));
		allocatedData = 0;
		nextChunk     = alignedData;
	}

	//Reset that block but don't free memory
	inline void clear()
	{
		allocatedData = 0;
		nextChunk     = alignedData;
	}

	inline void* allocate(uint32_t allocationSize)
	{
		void* p;
		allocatedData += allocationSize;
		if (allocatedData > blockSize)
		{
			allocatedData -= allocationSize;
			return nullptr;
		}
		p = nextChunk;
		nextChunk += allocationSize;
		return p;
	}

	void* getFreeSpace(uint32_t* size)
	{
		*size = blockSize - allocatedData;
		return nextChunk;
	}

	inline CacheLineBlock* getPrevious()
	{
		return previous;
	}

	~CacheLineBlock()
	{
		if (data != nullptr)
		{
			free(data);
			data = nullptr;
		}
	}
};

struct CacheLineChunkInfo
{
	uint32_t size;
	void**   freeChunks;
};

class CacheLineLargeChunk
{
public:
	void*                alignedData;
	void*                allocatedChunkData;
	CacheLineLargeChunk* previous;
	CacheLineLargeChunk* next;

	CacheLineLargeChunk(void* allocatedChunkData, void* alignedData, CacheLineLargeChunk* previous) : allocatedChunkData(allocatedChunkData),
		alignedData(alignedData), previous(previous), next(nullptr)
	{
		if (this->previous != nullptr)
		{
			this->previous->next = this;
		}
	}

};

class CacheLineAllocator
{
private:
	uint32_t        blockSize;
	CacheLineBlock* activeBlock;

	CacheLineChunkInfo   freeList[CACHE_LINE_ALLOCATOR_LARGEST_MULTIPLIER];
	CacheLineLargeChunk* largeTypeList;

	const uint32_t largestPoolAllocationSize;
	const uint32_t cacheLineSize;
public:
	//bool  log = false;

	CacheLineAllocator(uint32_t blockSize, uint32_t cacheLineSize) :
		largeTypeList(nullptr),
		blockSize(blockSize),
		cacheLineSize(cacheLineSize),
		largestPoolAllocationSize(cacheLineSize * CACHE_LINE_ALLOCATOR_LARGEST_MULTIPLIER)
	{
		activeBlock = new CacheLineBlock(blockSize,cacheLineSize);

		for (uint32_t i = 0; i< CACHE_LINE_ALLOCATOR_LARGEST_MULTIPLIER; ++i)
		{
			freeList[i].size       = i * this->cacheLineSize;
			freeList[i].freeChunks = nullptr;
		}
	}

	inline uint32_t getLargestPoolAllocationSize()
	{
		return this->largestPoolAllocationSize;
	}

	inline uint32_t getCacheLineSize()
	{
		return this->cacheLineSize;
	}

	void* allocate(uint32_t objectSize);

	inline uint32_t getBlockSize()
	{
		return blockSize;
	}

	//clear but don't free allocated memory
	void clear();

	void free(void* pointer, uint32_t objectSize);

	~CacheLineAllocator()
	{
		if (activeBlock == nullptr)
			return;

		for (CacheLineLargeChunk* itLargeChunk = largeTypeList; itLargeChunk != nullptr; itLargeChunk = itLargeChunk->previous)
		{
			::free(itLargeChunk->allocatedChunkData);
		}

		this->largeTypeList = nullptr;

		CacheLineBlock* tempBlock = activeBlock;

		while (tempBlock->getPrevious() != nullptr)
		{
			activeBlock = tempBlock;
			tempBlock = tempBlock->getPrevious();
			delete activeBlock;
		}

		delete tempBlock;
		activeBlock = nullptr;
	}
};