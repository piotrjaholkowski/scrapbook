#pragma once

#include "../Math/Math.hpp"
#include "Shapes/Shape2d.hpp"
#include "Mid/AABB2d.hpp"

#include "emmintrin.h" // SSE 2
#include "smmintrin.h" // SSE 4.1

class PhysicsTests;

namespace gil
{

enum ObjectFlag {
	GIL_OBJECTTYPE = 1,
	GIL_FLAGED     = 2,
	GIL_IS_CHILD   = 3
};

class ObjectHandle2d;

class HashNode
{
public:
	ObjectHandle2d* handle;
	//This field is used for keeping information about next hashNode in hashBucket
	HashNode* next;
	//This field is used for keeping information about previous hashNode in hashBucket
	HashNode* prev;
	//This field is used by hashProxy of each body to keep information about next hashNode in hashProxy list
	HashNode* nextProxyNode;
	int16_t   bucketId;

	HashNode(ObjectHandle2d* handle, int16_t bucketId) : handle(handle), bucketId(bucketId), next(nullptr), prev(nullptr), nextProxyNode(nullptr)
	{

	}
};

class CellCoordinate
{
public:
	int16_t x;
	int16_t y;
};

class HashCodes
{
public:
	uint32_t hashCodes[4];

	inline void clear()
	{
		hashCodes[0] = 0;
		hashCodes[1] = 0;
		hashCodes[2] = 0;
		hashCodes[3] = 0;
	}

	inline void addHashCode(uint16_t hashCode)
	{
		uint16_t arrayId   = hashCode / 32;
		uint8_t  shiftsNum = hashCode % 32;
		hashCodes[arrayId] |= (1 << shiftsNum);
	}

	inline bool isHashCodeAdded(uint16_t hashCode)
	{
		uint16_t arrayId   = hashCode / 32;
		uint8_t  shiftsNum = hashCode % 32;
		return (0 != (hashCodes[arrayId] & (1 << shiftsNum)));
	}

	static inline bool testPair(const __m128i& hashCodes0, const __m128i& hashCodes1, const __m128i& bucketMask)
	{
		__m128i and128 = _mm_and_si128(hashCodes0, hashCodes1);
		return (1 == _mm_test_all_zeros(and128, bucketMask));
	}

	static inline bool testPair(HashCodes& hashCodes0, HashCodes& hashCodes1, HashCodes& bucketMask)
	{
		const __m128i bucketMask_128 = _mm_lddqu_si128((__m128i const*)bucketMask.hashCodes);
		const __m128i hashCodes0_128 = _mm_lddqu_si128((__m128i const*)hashCodes0.hashCodes);
		const __m128i hashCodes1_128 = _mm_lddqu_si128((__m128i const*)hashCodes1.hashCodes);

		return testPair(hashCodes0_128, hashCodes1_128, bucketMask_128);
	}
};

//It is used for keeping all hashNodes of body together
class HashProxy
{
public:
	HashNode* firstNode; // first added node

	CellCoordinate cellCoordinate[2];
	HashCodes      hashCodes;

	HashProxy() : firstNode(nullptr)
	{
		hashCodes.clear();
	}

	inline bool checkIfCoordinatesEqual(int16_t x0, int16_t y0, int16_t x1, int16_t y1)
	{
		const bool isEqual0 = (x0 == cellCoordinate[0].x);
		const bool isEqual1 = (y0 == cellCoordinate[0].y);
		const bool isEqual2 = (x1 == cellCoordinate[1].x);
		const bool isEqual3 = (y1 == cellCoordinate[1].y);
		return (isEqual0 && isEqual1 && isEqual2 && isEqual3);
	}

	// This function doesn't check if object has already node with equal hash
	inline void pushBack(HashNode* node)
	{
		if (firstNode == nullptr)
		{
			firstNode = node;
		}
		else
		{
			node->nextProxyNode = firstNode;
			firstNode = node;
		}
	}

	void printNodes()
	{
		HashNode* itProxyNode = firstNode;
		while (itProxyNode != nullptr)
		{
			std::cout << "\tBucket ID " << itProxyNode->bucketId << std::endl;
			itProxyNode = itProxyNode->nextProxyNode;
		}
	}
};

class ObjectHandle2d
{
public:
	AABB2d          volume;
	jaTransform2    transform;
	Shape2d*        shape;
	ObjectHandle2d* next;
	ObjectHandle2d* prev;
private:
	void* userData;
public:
	HashProxy hashProxy;
	uint16_t  id;

	uint16_t flag;

	//filter
	uint16_t categoryBits;
	uint16_t maskBits;
	int8_t   groupIndex;
	//filter

	ObjectHandle2d(uint16_t id, Shape2d* shape, const jaTransform2& transform) : transform(transform), next(nullptr), prev(nullptr), flag(0), categoryBits(0x0001), maskBits(0xFFFF), groupIndex(1)
	{
		this->id = id;
		this->shape = shape;
		this->shape->initAABB2d(transform, volume);
		this->shape->setParent(this);
	}

	ObjectHandle2d(uint16_t id, const Shape2d* shapeToCopy, const jaTransform2& transform, CacheLineAllocator* shapeAllocator) : transform(transform), next(nullptr), prev(nullptr), flag(0), categoryBits(0x0001), maskBits(0xFFFF), groupIndex(1)
	{
		Shape2d* shapeCopy = (Shape2d*)(shapeAllocator->allocate(shapeToCopy->sizeOf())); // allocates shape after object handle
		shapeToCopy->clone(shapeCopy, shapeAllocator);

		this->id = id;
		this->shape = shapeCopy;
		this->shape->initAABB2d(transform, volume);
		this->shape->setParent(this);
	}

	inline void setDynamic(bool isDynamic)
	{
		if (isDynamic)
		{
			flag |= GIL_OBJECTTYPE;
		}
		else
		{
			flag |= GIL_OBJECTTYPE;
			flag ^= GIL_OBJECTTYPE;
		}
	}

	inline void setFlag()
	{
		flag |= GIL_FLAGED;
	}

	inline void setUnflag()
	{
		flag ^= GIL_FLAGED;
	}

	inline void setCategoryMask(uint16_t& categoryMask)
	{
		this->categoryBits = categoryMask;
	}

	inline uint16_t getCategoryMask()
	{
		return categoryBits;
	}

	inline void setMaskBits(uint16_t& maskBits)
	{
		this->maskBits = maskBits;
	}

	inline uint16_t getMaskBits()
	{
		return maskBits;
	}

	inline void setGroupIndex(int8_t groupIndex)
	{
		this->groupIndex = groupIndex;
	}

	inline int8_t getGroupIndex()
	{
		return groupIndex;
	}

#pragma warning( disable : 4800 ) 
	inline bool isFlaged()
	{
		return (flag & GIL_FLAGED);
	}

	inline bool isDynamic()
	{
		return (flag & GIL_OBJECTTYPE);
	}
#pragma warning( default : 4800 )

	AABB2d getAABB2d()
	{
		return volume;
	}

	void setTransform(jaTransform2& transform)
	{
		this->transform = transform;
	}

	jaTransform2 getTransform()
	{
		return transform;
	}

	jaTransform2* getTransformP()
	{
		return &transform;
	}

	Shape2d* getShape() const
	{
		return shape;
	}

	void initAABB2d()
	{
		shape->initAABB2d(transform, volume);
	}

	void updateAABB2d()
	{
		shape->updateAABB2d(transform, volume);
	}

	inline uint16_t getId() const
	{
		return id;
	}

	inline void* getUserData()
	{
		return userData;
	}

	void setUserData(void* userData)
	{
		this->userData = userData;
	}

	friend class ::PhysicsTests;
};

}