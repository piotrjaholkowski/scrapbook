#pragma once

namespace gil
{

enum class ShapeType
{
	Box           = 0,
	Circle        = 1,
	ConvexPolygon = 2,
	Particles     = 3,
	MultiShape    = 4,
	TriangleMesh  = 5,
	Point         = 6
};

}

