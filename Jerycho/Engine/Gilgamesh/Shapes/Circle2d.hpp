#pragma once

#include "../../jaSetup.hpp"
#include "../Shapes/Shape2d.hpp"
#include "../Shapes/Shape2dTypes.hpp"

namespace gil
{

	class Circle2d : public Shape2d
	{
	private:

	public:
		jaFloat radius;
		jaFloat density;
		jaFloat restitution;
		jaFloat friction;

		inline Circle2d(jaFloat radius) : Shape2d((uint8_t)ShapeType::Circle), radius(radius)
		{

		}

		inline Circle2d() : Shape2d((uint8_t)ShapeType::Circle), radius(0.5)
		{

		}

		inline jaVector2 findSupportPoint(const jaVector2& searchDirection) const
		{
			return (searchDirection * radius);
		}

		inline void getVertex(uint16_t vertexIndex, jaVector2& vertex) const
		{

		}

		//Support points in counter-clockwise order
		uint16_t findSupportPointIndex(const jaVector2& searchDirection) const
		{
			return 0;
		}
		//Support triangle in counter-clockwise order
		void getSupportTriangle(uint16_t vertexIndex, jaVector2* triangle) const
		{
			return;
		}

		inline jaFloat getRadius() const
		{
			return radius;
		}

		bool isPointInShape(const jaTransform2& transform, const jaVector2& point)
		{
			jaVector2 diffrence = point - transform.position;
			if (jaVectormath2::length(diffrence) > radius) return false;
			return true;
		}

		void initAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			aabb.min.x = transform.position.x - radius;
			aabb.min.y = transform.position.y - radius;
			aabb.max.x = transform.position.x + radius;
			aabb.max.y = transform.position.y + radius;
		}

		void updateAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			aabb.min.x = transform.position.x - radius;
			aabb.min.y = transform.position.y - radius;
			aabb.max.x = transform.position.x + radius;
			aabb.max.y = transform.position.y + radius;
		}

		inline uint32_t sizeOf() const
		{
			return sizeof(Circle2d);
		}

		void clone(void* destination) const
		{
			memcpy(destination, this, sizeof(Circle2d));
		}

		void clone(void* destination, CacheLineAllocator* allocator) const
		{
			memcpy(destination, this, sizeof(Circle2d));
		}

#pragma warning( disable : 4244)
		inline jaFloat getArea() const
		{
			return (ja::math::PI * radius * radius);
		}
#pragma warning( default : 4244)

		void getMassData(MassData2d& massData)
		{
			massData.mass = density * getArea();
			massData.centerOfMass.x = 0.0f;
			massData.centerOfMass.y = 0.0f;
			massData.interia = massData.mass * (GIL_REAL(0.5) * radius * radius);
		}

		jaFloat getSizeX()
		{
			return radius;
		}

		jaFloat getSizeY()
		{
			return radius;
		}

		void setSizeX(jaFloat size)
		{
			if (size < GIL_ZERO)
				size = -size;

			radius = size;
		}

		void setSizeY(jaFloat size)
		{
			if (size < GIL_ZERO)
				size = -size;

			radius = size;
		}

		inline void getInnerRandomPoint(jaVector2& point)
		{
			point.x = fmodf((float)rand(), 2 * radius);
			point.y = fmodf((float)rand(), 2 * radius);
			point.x -= radius;
			point.y -= radius;
		}

		uint16_t getVerticesNum() const
		{
			return 0;
		}

		jaFloat getDensity()
		{
			return density;
		}

		jaFloat getRestitution()
		{
			return restitution;
		}

		jaFloat getFriction()
		{
			return friction;
		}

		void setDensity(jaFloat density)
		{
			this->density = density;
		}

		void setRestitution(jaFloat restitution)
		{
			this->restitution = restitution;
		}

		void setFriction(jaFloat friction)
		{
			this->friction = friction;
		}

		void setParent(ObjectHandle2d* parent)
		{

		}

		const char* getName() const
		{
			return "Circle";
		}
	};

}
