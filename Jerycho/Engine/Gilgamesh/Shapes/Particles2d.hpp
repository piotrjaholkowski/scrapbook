#pragma once


#include "../../jaSetup.hpp"
#include "../Shapes/Shape2d.hpp"
#include "../Shapes/Shape2dTypes.hpp"

namespace gil
{

	struct Particle2d
	{
	public:
		jaVector2 position;
		jaFloat   angle;
		jaVector2 linearVelocity;
		jaFloat   angluarVelocity;
		jaFloat   scale;
		jaFloat   alpha;
		jaFloat   life;
	};

	
	class Particles2d : public Shape2d
	{
	public:
		Particle2d* particles; // array of particles position

		uint16_t emitterId;

		uint16_t particleNum;
		uint16_t activeParticles;
		bool collide;

		jaFloat density;
		jaFloat restitution;
		jaFloat friction;

		inline Particles2d(uint16_t particleNum, Particle2d* particles, bool collide, uint16_t emitterId) : Shape2d((uint8_t)ShapeType::Particles), particleNum(particleNum), collide(collide)
		{
			this->particles = particles;
			this->emitterId = emitterId; 
			activeParticles = 0;
		}

		inline void getVertex(uint16_t vertexIndex, jaVector2& vertex) const
		{

		}

		//Support points in counter-clockwise order
		uint16_t findSupportPointIndex(const jaVector2& searchDirection) const
		{
			uint16_t particleIndex = 0;
			jaFloat  maxDot = GILMINFLOAT;
			jaFloat  dot;

			for (uint16_t i = 0; i < particleNum; i++)
			{
				if (particles[i].life < 0)
					continue;

				dot = jaVectormath2::projection(particles[i].position, searchDirection);
				if (dot > maxDot)
				{
					maxDot = dot;
					particleIndex = i;
				}
			}

			return particleIndex;
		}

		inline jaVector2 findSupportPoint(const jaVector2& searchDirection) const
		{
			uint16_t particleIndex = findSupportPointIndex(searchDirection);
			return particles[particleIndex].position;
		}

		//Support triangle in counter-clockwise order
		void getSupportTriangle(uint16_t vertexIndex, jaVector2* triangle) const
		{
			return;
		}

		void initAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{

			aabb.min = transform.position;
			aabb.max = transform.position;

		}

		void updateAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			jaVector2 center = transform.position;

			aabb.min = center;
			aabb.max = center;
			activeParticles = particleNum;

			if (collide)
			{
				

			}
			
			for (uint16_t i = 0; i < particleNum; i++)
			{
				if (particles[i].life < 0)
				{
					activeParticles--;
					continue;
				}
		
				//right
				if (aabb.max.x < particles[i].position.x)
					aabb.max.x = particles[i].position.x;
		
				//left
				if (aabb.min.x > particles[i].position.x)
					aabb.min.x = particles[i].position.x;
		
				//top
				if (aabb.max.y < particles[i].position.y)
					aabb.max.y = particles[i].position.y;
	
				//down
				if (aabb.min.y > particles[i].position.y)
					aabb.min.y = particles[i].position.y;
			}

		}


		bool isPointInShape(const jaTransform2& transform, const jaVector2& point)
		{
			return false;
		}

		inline uint32_t sizeOf() const
		{
			return sizeof(Particles2d);
		}

		void clone(void* destination) const
		{
			memcpy(destination, this, sizeof(Particles2d));
		}

		void clone(void* destination, CacheLineAllocator* allocator) const
		{
			memcpy(destination, this, sizeof(Particles2d));
		}

		jaFloat getArea() const
		{
			return 0;
		}

		void getMassData(MassData2d& massData)
		{

		}

		jaFloat getSizeX()
		{
			return 0;
		}

		jaFloat getSizeY()
		{
			return 0;
		}

		void setSizeX(jaFloat size)
		{

		}

		void setSizeY(jaFloat size)
		{

		}

		inline void getInnerRandomPoint(jaVector2& point)
		{

		}

		uint16_t getVerticesNum() const
		{
			return particleNum;
		}

		jaFloat getDensity()
		{
			return density;
		}

		jaFloat getRestitution()
		{
			return restitution;
		}

		jaFloat getFriction()
		{
			return friction;
		}

		void setDensity(jaFloat density)
		{
			this->density = density;
		}

		void setRestitution(jaFloat restitution)
		{
			this->restitution = restitution;
		}

		void setFriction(jaFloat friction)
		{
			this->friction = friction;
		}

		void setParent(ObjectHandle2d* parent)
		{

		}

		const char* getName() const
		{
			return "Particles";
		}
	};

}
