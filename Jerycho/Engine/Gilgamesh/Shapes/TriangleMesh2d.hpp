#pragma once

#include "../../jaSetup.hpp"
#include "../Shapes/Shape2d.hpp"
#include "../Shapes/Shape2dTypes.hpp"

#include <stdint.h>

namespace gil
{

	class TriangleMesh2d : public Shape2d
	{
	private:
		jaVector2* vertices;
		uint16_t*  triangleIndices;
		uint8_t*   edgesToNotTest;
		uint16_t   verticesNum;
		uint16_t   trianglesNum;
		//jaVector2  scale;
		jaFloat    density;
		jaFloat    restitution;
		jaFloat    friction;

		CacheLineAllocator* shapeAllocator;

	public:
		TriangleMesh2d(CacheLineAllocator* shapeAllocator) : Shape2d((uint8_t)ShapeType::TriangleMesh), shapeAllocator(shapeAllocator), vertices(nullptr), verticesNum(0), triangleIndices(nullptr), trianglesNum(0), edgesToNotTest(nullptr)
		{

		}

		const jaVector2* getVertices() const
		{
			return this->vertices;
		}

		uint16_t getVerticesNum() const
		{
			return this->verticesNum;
		}

		const uint16_t* getTriangleIndices() const
		{
			return this->triangleIndices;
		}

		const uint8_t* getEdgesToNotTest() const
		{
			return this->edgesToNotTest;
		}

		uint16_t getTrianglesNum() const
		{
			return this->trianglesNum;
		}

		void setEdgesToNotTest()
		{
			memset(this->edgesToNotTest, 0, trianglesNum * sizeof(uint8_t));

			for (uint16_t triangleIt = 0; triangleIt < this->trianglesNum; ++triangleIt)
			{
				uint16_t triangleI  = triangleIt * 3;
				uint16_t triangleI0 = this->triangleIndices[triangleI];
				uint16_t triangleI1 = this->triangleIndices[triangleI + 1];
				uint16_t triangleI2 = this->triangleIndices[triangleI + 2];

				for (uint16_t triangleZt = triangleIt + 1; triangleZt < this->trianglesNum; ++triangleZt)
				{
					uint16_t triangleZ  = triangleZt * 3;
					uint16_t triangleZ0 = this->triangleIndices[triangleZ];
					uint16_t triangleZ1 = this->triangleIndices[triangleZ + 1];
					uint16_t triangleZ2 = this->triangleIndices[triangleZ + 2];

					bool testsIZ[9];

					testsIZ[0] = (triangleI0 == triangleZ0);
					testsIZ[1] = (triangleI0 == triangleZ1);
					testsIZ[2] = (triangleI0 == triangleZ2);

					testsIZ[3] = (triangleI1 == triangleZ0);
					testsIZ[4] = (triangleI1 == triangleZ1);
					testsIZ[5] = (triangleI1 == triangleZ2);

					testsIZ[6] = (triangleI2 == triangleZ0);
					testsIZ[7] = (triangleI2 == triangleZ1);
					testsIZ[8] = (triangleI2 == triangleZ2);

					bool testsI[3];

					testsI[0] = (testsIZ[0] || testsIZ[1] || testsIZ[2]);
					testsI[1] = (testsIZ[3] || testsIZ[4] || testsIZ[5]);
					testsI[2] = (testsIZ[6] || testsIZ[7] || testsIZ[8]);

					uint8_t commonVerticesNum = (uint8_t)(testsI[0]) + (uint8_t)(testsI[1]) + (uint8_t)(testsI[2]);
					
					if (commonVerticesNum >= 2)
					{
						bool testsZ[3];
						testsZ[0] = (testsIZ[0] || testsIZ[3] || testsIZ[6]);
						testsZ[1] = (testsIZ[1] || testsIZ[4] || testsIZ[7]);
						testsZ[2] = (testsIZ[2] || testsIZ[5] || testsIZ[8]);

						this->edgesToNotTest[triangleIt] |= (testsI[0] && testsI[1]) ? 1 : 0; // b001
						this->edgesToNotTest[triangleIt] |= (testsI[1] && testsI[2]) ? 2 : 0; // b010
						this->edgesToNotTest[triangleIt] |= (testsI[2] && testsI[0]) ? 4 : 0; // b100
						this->edgesToNotTest[triangleZt] |= (testsZ[0] && testsZ[1]) ? 1 : 0; // b001
						this->edgesToNotTest[triangleZt] |= (testsZ[1] && testsZ[2]) ? 2 : 0; // b010
						this->edgesToNotTest[triangleZt] |= (testsZ[2] && testsZ[0]) ? 4 : 0; // b100				
					}
				}
			}
		}

		static uint32_t getTriangleDataAllocationSize(uint16_t verticesNum, uint16_t trianglesNum)
		{
			return ((verticesNum * sizeof(jaVector2)) + (trianglesNum * ((sizeof(uint16_t) * 3) + sizeof(uint8_t))));;
		}

		void setTriangleMesh(const jaVector2* vertices, uint16_t verticesNum, const uint16_t* triangleIndices, uint16_t trianglesNum , jaVector2& translateOffset, uint8_t* edgesToTest = nullptr)
		{
			if (this->vertices != nullptr)
			{
				const uint32_t oldAllocationSize = getTriangleDataAllocationSize(this->verticesNum, this->trianglesNum);
				this->shapeAllocator->free(this->vertices, oldAllocationSize);	
			}

			const uint32_t newAllocationSize = getTriangleDataAllocationSize(verticesNum, trianglesNum);
			this->vertices        = (jaVector2*)this->shapeAllocator->allocate(newAllocationSize);
			this->triangleIndices = (uint16_t*)(this->vertices + verticesNum);
			this->edgesToNotTest  = (uint8_t*)(this->triangleIndices + (3 * trianglesNum));

			this->verticesNum  = verticesNum;
			this->trianglesNum = trianglesNum;
			translateOffset.setZero();

			for (uint16_t i = 0; i < verticesNum; ++i)
			{
				translateOffset.x += vertices[i].x;
				translateOffset.y += vertices[i].y;
			}

			float fVerticesNum = (float)verticesNum;
			translateOffset.x /= fVerticesNum;
			translateOffset.y /= fVerticesNum;

			for (uint16_t i = 0; i < verticesNum; ++i)
			{
				this->vertices[i].x = vertices[i].x - translateOffset.x;
				this->vertices[i].y = vertices[i].y - translateOffset.y;
			}

			memcpy(this->triangleIndices, triangleIndices, trianglesNum * 3 * sizeof(uint16_t));
			
			if (nullptr != edgesToTest)
			{
				memcpy(this->edgesToNotTest, edgesToTest, trianglesNum * sizeof(uint8_t));
			}
			else
			{
				setEdgesToNotTest();
			}
		}

		void setTriangleMesh(const jaVector2* vertices, uint16_t verticesNum, const uint16_t* triangleIndices, uint16_t trianglesNum, uint8_t* edgesToTest = nullptr)
		{
			if (this->vertices != nullptr)
			{
				const uint32_t oldAllocationSize = getTriangleDataAllocationSize(this->verticesNum, this->trianglesNum);
				this->shapeAllocator->free(this->vertices, oldAllocationSize);
			}

			const uint32_t newAllocationSize = getTriangleDataAllocationSize(verticesNum, trianglesNum);
			this->vertices = (jaVector2*)this->shapeAllocator->allocate(newAllocationSize);
			this->triangleIndices = (uint16_t*)(this->vertices + verticesNum);
			this->edgesToNotTest = (uint8_t*)(this->triangleIndices + (3 * trianglesNum));

			this->verticesNum = verticesNum;
			this->trianglesNum = trianglesNum;

			memcpy(this->vertices, vertices, verticesNum * sizeof(jaVector2));
			memcpy(this->triangleIndices, triangleIndices, trianglesNum * 3 * sizeof(uint16_t));

			if (edgesToTest != nullptr)
				memcpy(this->edgesToNotTest, edgesToTest, trianglesNum * sizeof(uint8_t));
			else
				setEdgesToNotTest();
		}

		static void getShapeCenter(jaVector2* const vertices, const uint16_t* triangleIndices, uint16_t trianglesNum, jaVector2& shapeCenter)
		{
			shapeCenter.setZero();

			for (uint16_t i = 0; i < trianglesNum; ++i)
			{
				uint16_t triangleI = i * 3;
				const uint16_t triangleI0 = triangleI + 0;
				const uint16_t triangleI1 = triangleI + 1;
				const uint16_t triangleI2 = triangleI + 2;

				jaVector2 triangleCenter = (vertices[triangleI0] + vertices[triangleI1] + vertices[triangleI2]) / 3.0f;

				shapeCenter += triangleCenter;
			}

			shapeCenter = shapeCenter / (float)(trianglesNum);
		}

		inline jaVector2 findSupportPoint(const jaVector2& searchDirection) const
		{
			jaFloat  maxDot = jaVectormath2::projection(searchDirection, vertices[0]);
			jaFloat  dot;
			uint16_t maxIndex = 0;

			for (uint16_t i = 1; i<this->verticesNum; ++i)
			{
				dot = jaVectormath2::projection(searchDirection, vertices[i]);
				if (dot > maxDot)
				{
					maxDot = dot;
					maxIndex = i;
				}
			}

			return vertices[maxIndex];
		}

		inline void findSupportPoints(const jaVector2& searchDirection, jaVector2& supportPoint, jaVector2& oppositeSupportPoint) const
		{
			jaFloat  maxDot   = jaVectormath2::projection(searchDirection, vertices[0]);
			jaFloat  minDot   = maxDot;
			uint16_t maxIndex = 0;
			uint16_t minIndex = 0;

			for (uint16_t i = 1; i<this->verticesNum; ++i)
			{
				const jaFloat dot = jaVectormath2::projection(searchDirection, vertices[i]);
				
				if (dot > maxDot)
				{
					maxDot   = dot;
					maxIndex = i;
				}

				if (dot < minDot)
				{
					minDot   = dot;
					minIndex = i;
				}
			}

			supportPoint         = vertices[maxIndex];
			oppositeSupportPoint = vertices[minIndex];
		}

		inline void getVertex(uint16_t vertexIndex, jaVector2* vertex) const
		{

		}

		inline uint16_t findSupportPointIndex(const jaVector2& searchDirection) const
		{
			jaFloat  maxDot = jaVectormath2::projection(searchDirection, vertices[0]);
			jaFloat  dot;
			uint16_t maxIndex = 0;

			for (uint16_t i = 1; i<verticesNum; ++i)
			{
				dot = jaVectormath2::projection(searchDirection, vertices[i]);
				if (dot > maxDot)
				{
					maxDot = dot;
					maxIndex = i;
				}
			}

			return maxIndex;
		}

		inline void getVertex(uint16_t vertexIndex, jaVector2& vertex) const
		{
			vertex.x = vertices[vertexIndex].x;
			vertex.y = vertices[vertexIndex].y;
		}

		inline void getSupportTriangle(uint16_t vertexIndex, jaVector2* triangle) const
		{
			triangle = nullptr;
		}

		static uint8_t findSupportPointIndex(const jaVector2* triangleVertices, const jaVector2& supportDir)
		{
			const float proj0 = jaVectormath2::projection(triangleVertices[0], supportDir);
			const float proj1 = jaVectormath2::projection(triangleVertices[1], supportDir);
			const float proj2 = jaVectormath2::projection(triangleVertices[2], supportDir);

			if (proj0 > proj1)
			{
				if (proj0 > proj2)
					return 0;
				else
					return 2;
			}
			else
			{
				if (proj1 > proj2)
					return 1;
				else
					return 2;
			}
		}

		static void getSupportTriangle(const jaVector2* triangleVertices, uint16_t vertexIndex, jaVector2* supportTriangle)
		{
			uint16_t lowerIndex;
			uint16_t higherIndex = (vertexIndex + 1) % 3;

			lowerIndex = (vertexIndex == 0) ? 2 : (vertexIndex - 1);

			supportTriangle[0] = triangleVertices[lowerIndex];
			supportTriangle[1] = triangleVertices[vertexIndex];
			supportTriangle[2] = triangleVertices[higherIndex];
		}

		bool isPointInShape(const jaTransform2& transform, const jaVector2& point)
		{
			jaVector2 positionDiff = point - transform.position;
			jaMatrix2 invRotMat    = jaVectormath2::inverse(transform.rotationMatrix);
			jaVector2 pointLocal   = invRotMat * positionDiff;

			for (uint16_t triangleIt = 0; triangleIt < this->trianglesNum; ++triangleIt)
			{
				uint16_t triangleI = triangleIt * 3;
				const uint16_t index0 = triangleI + 0;
				const uint16_t index1 = triangleI + 1;
				const uint16_t index2 = triangleI + 2;

				const jaVector2 triangleVertices[3] = { this->vertices[index0], this->vertices[index1], this->vertices[index2] };
				jaVector2 normals[3];
				//Make normals from support trangles edges
				normals[0] = jaVectormath2::perpendicular(triangleVertices[1] - triangleVertices[0]); //normal on the left of vector 
				normals[1] = jaVectormath2::perpendicular(triangleVertices[2] - triangleVertices[1]); //normal on the left of vector
				normals[2] = jaVectormath2::perpendicular(triangleVertices[0] - triangleVertices[2]); //normal on the left of vector

				for (uint16_t normalI = 0; normalI < 3; ++normalI)
				{
					jaFloat projTriangle = jaVectormath2::projection(normals[0], triangleVertices[0]);
					jaFloat projPoint = jaVectormath2::projection(normals[0], pointLocal);

					jaFloat depth = projTriangle - projPoint;
					if (depth < GIL_ZERO)
						goto NO_TRIANGLE_INTERSECTION;
				}
				
				return true;

			NO_TRIANGLE_INTERSECTION:
				continue;
			}

			return false;
		}

		void initAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			jaMatrix2 invRot = jaVectormath2::inverse(transform.rotationMatrix);
			jaVector2 left, top, right, down;

			findSupportPoints(invRot * jaVector2(-1.0f, 0.0f), left, right);
			findSupportPoints(invRot * jaVector2(0.0f, 1.0f), top, down);

			left  = transform.rotationMatrix * left;
			top   = transform.rotationMatrix * top;
			right = transform.rotationMatrix * right;
			down  = transform.rotationMatrix * down;

			aabb.max = jaVector2(right.x, top.y);
			aabb.min = jaVector2(left.x, down.y);

			aabb.max += transform.position;
			aabb.min += transform.position;
		}

		void updateAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			jaMatrix2 invRot = jaVectormath2::inverse(transform.rotationMatrix);
			jaVector2 left, top, right, down;

			findSupportPoints(invRot * jaVector2(-1.0f, 0.0f), left, right);
			findSupportPoints(invRot * jaVector2(0.0f, 1.0f), top, down);

			left  = transform.rotationMatrix * left;
			top   = transform.rotationMatrix * top;
			right = transform.rotationMatrix * right;
			down  = transform.rotationMatrix * down;

			aabb.max = jaVector2(right.x, top.y);
			aabb.min = jaVector2(left.x, down.y);

			aabb.max += transform.position;
			aabb.min += transform.position;
		}

		inline uint32_t sizeOf() const
		{
			return sizeof(TriangleMesh2d);
		}

		void clone(void* destination) const
		{
			TriangleMesh2d* newTriangleMesh = (TriangleMesh2d*)destination;
			memcpy(destination, this, sizeof(TriangleMesh2d));

			newTriangleMesh->shapeAllocator = this->shapeAllocator;
			const uint32_t copySize = getTriangleDataAllocationSize(this->verticesNum, this->trianglesNum);

			newTriangleMesh->vertices = (jaVector2*)newTriangleMesh->shapeAllocator->allocate(copySize);
			memcpy(newTriangleMesh->vertices, this->vertices, copySize);
			
			newTriangleMesh->triangleIndices = (uint16_t*)(newTriangleMesh->vertices + verticesNum);
			newTriangleMesh->edgesToNotTest  = (uint8_t*)(newTriangleMesh->triangleIndices + (3 * trianglesNum));
		}

		void clone(void* destination, CacheLineAllocator* allocator) const
		{
			TriangleMesh2d* newTriangleMesh = (TriangleMesh2d*)destination;
			memcpy(destination, this, sizeof(TriangleMesh2d));

			newTriangleMesh->shapeAllocator = allocator;
			const uint32_t copySize = getTriangleDataAllocationSize(this->verticesNum, this->trianglesNum);

			newTriangleMesh->vertices = (jaVector2*)newTriangleMesh->shapeAllocator->allocate(copySize);
			memcpy(newTriangleMesh->vertices, this->vertices, copySize);

			newTriangleMesh->triangleIndices = (uint16_t*)(newTriangleMesh->vertices + verticesNum);
			newTriangleMesh->edgesToNotTest  = (uint8_t*)(newTriangleMesh->triangleIndices + (3 * trianglesNum));
		}

		void copy(TriangleMesh2d* triangleMeshDst)
		{
			CacheLineAllocator* allocatorDst = triangleMeshDst->shapeAllocator;
			
			const uint32_t oldDataSize = getTriangleDataAllocationSize(triangleMeshDst->verticesNum, triangleMeshDst->trianglesNum);
			allocatorDst->free(triangleMeshDst->vertices, oldDataSize);
			
			const uint32_t newDataSize = getTriangleDataAllocationSize(this->verticesNum, this->trianglesNum);
			triangleMeshDst->vertices  = (jaVector2*)allocatorDst->allocate(newDataSize);
			memcpy(triangleMeshDst->vertices, this->vertices, newDataSize);

			triangleMeshDst->verticesNum  = verticesNum;
			triangleMeshDst->trianglesNum = trianglesNum;

			triangleMeshDst->triangleIndices = (uint16_t*)(triangleMeshDst->vertices + triangleMeshDst->verticesNum);
			triangleMeshDst->edgesToNotTest  = (uint8_t*)(triangleMeshDst->triangleIndices + (3 * triangleMeshDst->trianglesNum));
		}

		jaFloat getArea() const
		{
			jaFloat area = 0.0f;

			for (uint16_t triangleIt = 0; triangleIt < trianglesNum; ++triangleIt)
			{
				uint16_t triangleI = triangleIt * 3;
				const uint16_t index0 = triangleI + 0;
				const uint16_t index1 = triangleI + 1;
				const uint16_t index2 = triangleI + 2;

				const jaVector2 triangleVertices[3] = { this->vertices[index0], this->vertices[index1], this->vertices[index2] };

				const jaVector2 e1 = triangleVertices[2] - triangleVertices[1];
				const jaVector2 e2 = triangleVertices[0] - triangleVertices[1];

				const jaFloat D = jaVectormath2::det(e1, e2);

				const jaFloat triangleArea = 0.5f * D;
				area += triangleArea;
			}

			return area;
		}

		void getMassData(MassData2d& massData)
		{
			massData.centerOfMass.setZero();

			const jaFloat k_inv3 = 1.0f / 3.0f;
			jaFloat       area   = 0.0f;
			jaFloat       I      = 0.0f;

			jaVector2 centroid(0.0f,0.0f);

			for (uint16_t triangleIt = 0; triangleIt < this->trianglesNum; ++triangleIt)
			{
				uint16_t triangleI = triangleIt * 3;
				const uint16_t index0 = triangleI + 0;
				const uint16_t index1 = triangleI + 1;
				const uint16_t index2 = triangleI + 2;

				const jaVector2 triangleVertices[3] = { this->vertices[index0], this->vertices[index1], this->vertices[index2] };

				const jaVector2 e1 = triangleVertices[2] - triangleVertices[1];
				const jaVector2 e2 = triangleVertices[0] - triangleVertices[1];

				const jaFloat D = jaVectormath2::det(e1, e2);

				const jaFloat triangleArea = 0.5f * D;
				area += triangleArea;

				//center of mass
				const jaVector2 triangleCenter = (triangleVertices[0] + triangleVertices[1] + triangleVertices[2]) / 3.0f;
				centroid += ((triangleArea * density) * triangleCenter);				

				const jaFloat ex1 = e1.x, ey1 = e1.y;
				const jaFloat ex2 = e2.x, ey2 = e2.y;

				const jaFloat intx2 = ex1*ex1 + ex2*ex1 + ex2*ex2;
				const jaFloat inty2 = ey1*ey1 + ey2*ey1 + ey2*ey2;

				I += (0.25f * k_inv3 * D) * (intx2 + inty2);
			}

			massData.mass = area * density;
			massData.interia = density * I;
			massData.centerOfMass = centroid / massData.mass;
		}

		jaFloat getSizeX()
		{
			return 1.0f;
		}

		jaFloat getSizeY()
		{
			return 1.0f;
		}

		void setSizeX(jaFloat size)
		{
			
		}

		void setSizeY(jaFloat size)
		{
			
		}

		inline void getInnerRandomPoint(jaVector2& point)
		{
			point.x = 0.0f;
			point.y = 0.0f;
		}

		jaFloat getDensity()
		{
			return density;
		}

		jaFloat getRestitution()
		{
			return restitution;
		}

		jaFloat getFriction()
		{
			return friction;
		}

		void setDensity(jaFloat density)
		{
			this->density = density;
		}

		void setRestitution(jaFloat restitution)
		{
			this->restitution = restitution;
		}

		void setFriction(jaFloat friction)
		{
			this->friction = friction;
		}

		void setParent(ObjectHandle2d* parent)
		{
		}

		const char* getName() const
		{
			return "TriangleMesh";
		}
	};

}