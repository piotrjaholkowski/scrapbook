#pragma once

#include "../../jaSetup.hpp"
#include "../ObjectHandle2d.hpp"
#include "../Shapes/Shape2d.hpp"
#include "../Shapes/Shape2dTypes.hpp"
#include "../../Allocators/CacheLineAllocator.hpp"

namespace gil
{
	
	class MultiShape2d : public Shape2d
	{
	public:
		ObjectHandle2d*     parent;
		ObjectHandle2d*     shapes;
		CacheLineAllocator* shapeAllocator;

		inline MultiShape2d(CacheLineAllocator* shapeAllocator) : Shape2d((uint8_t)ShapeType::MultiShape), shapes(nullptr), shapeAllocator(shapeAllocator), parent(nullptr)
		{

		}

		inline void deleteShapes()
		{
			ObjectHandle2d* shapeIt = this->shapes;

			while (shapeIt != nullptr)
			{
				ObjectHandle2d* shapeTemp = shapeIt;
				shapeIt = shapeIt->next;
				shapeAllocator->free(shapeTemp, shapeTemp->shape->sizeOf() + sizeof(ObjectHandle2d));
			}

			this->shapes = nullptr;
		}

		~MultiShape2d()
		{
			deleteShapes();
		}

		void setParent(ObjectHandle2d* parent)
		{
			this->parent = parent;

			uint16_t parentId = parent->id;
			void*    userData = parent->getUserData();

			for (ObjectHandle2d* shapeProxyIt = shapes; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
			{
				shapeProxyIt->id = parentId;
				shapeProxyIt->setUserData(userData);
			}
		}

		//transform is in local coordinates
		inline ObjectHandle2d* addShape(const jaTransform2& transform, Shape2d* shape)
		{
			ObjectHandle2d* shapeProxy = (ObjectHandle2d*)shapeAllocator->allocate(sizeof(ObjectHandle2d) + shape->sizeOf());
			shapeProxy->shape = (Shape2d*)((uint8_t*)shapeProxy + sizeof(ObjectHandle2d));
			shape->clone(shapeProxy->shape, this->shapeAllocator);
			shapeProxy->transform = transform;

			shapeProxy->categoryBits = 0x0001;
			shapeProxy->maskBits     = 0xFFFF;
			shapeProxy->groupIndex   = 1;
			shapeProxy->flag         = GIL_IS_CHILD;

			if (parent != nullptr)
			{
				shapeProxy->id = parent->id;
				shapeProxy->setUserData(parent->getUserData());
			}

			if (shapes == nullptr) {
				shapeProxy->next = nullptr;
				shapes = shapeProxy;
			}
			else {
				shapeProxy->next = shapes;
				shapes = shapeProxy;
			}

			return shapeProxy;
		}

	private:
		inline void addShape(ObjectHandle2d* shapeProxy)
		{
			ObjectHandle2d* newShapeProxy = (ObjectHandle2d*)shapeAllocator->allocate( sizeof(ObjectHandle2d) + shapeProxy->shape->sizeOf());
			newShapeProxy->shape = (Shape2d*)((uint8_t*)newShapeProxy + sizeof(ObjectHandle2d));
			shapeProxy->shape->clone(newShapeProxy->shape, this->shapeAllocator);
			newShapeProxy->transform = shapeProxy->transform;

			if (parent != nullptr)
			{
				shapeProxy->id = parent->id;
			}

			if (shapes == nullptr) {
				newShapeProxy->next = nullptr;
				shapes = newShapeProxy;
			}
			else {
				newShapeProxy->next = shapes;
				shapes = newShapeProxy;
			}
		}
	public:

		inline ObjectHandle2d* getShapeHandle() const
		{
			return shapes;
		}

		inline jaVector2 findSupportPoint(const jaVector2& searchDirection) const
		{
			//There is no point in implementation of this method because shape can be concave
			//so collision detection algorithms won't work this kind of shape needs special treatment
			//return jaVector2(0.0f, 0.0f);

			ObjectHandle2d* shapeProxyIt = shapes;
			jaTransform2    shapeTransformWorld;
			jaVector2       bestPoint(0.0f, 0.0f);
			jaVector2       tempPoint(0.0f, 0.0f);
		
			float         dotBest = 0.0f;
			float         dotTemp = 0.0f;

			for (shapeProxyIt; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
			{
				jaMatrix2 invRotShapeMat = jaVectormath2::inverse(shapeProxyIt->transform.rotationMatrix);

				tempPoint  = shapeProxyIt->shape->findSupportPoint(invRotShapeMat * searchDirection);
				tempPoint  = shapeProxyIt->transform.rotationMatrix * tempPoint;
				tempPoint += shapeProxyIt->transform.position;

				float dotTemp = jaVectormath2::projection(tempPoint, searchDirection);

				if (dotTemp > dotBest)
				{
					dotBest   = dotTemp;
					bestPoint = tempPoint;
				}
			}

			return bestPoint;
		}

		inline uint16_t findSupportPointIndex(const jaVector2& searchDirection) const
		{
			//There is no point in implementation of this method because shape can be concave
			//so collision detection algorithms won't work this kind of shape needs special treatment
			return 0;
		}

		inline void getVertex(uint16_t vertexIndex, jaVector2& vertex) const
		{
			//There is no point in implementation of this method
			
		}

		inline void getSupportTriangle(uint16_t vertexIndex, jaVector2* triangle) const
		{
			//There is no point in implementation of this method because shape can be concave
			//so collision detection algorithms won't work this kind of shape needs special treatment
			triangle = nullptr;
		}

		bool isPointInShape(const jaTransform2& transform, const jaVector2& point)
		{
			ObjectHandle2d* shapeProxyIt = shapes;

			jaTransform2 shapeTransformWorld;

			for (shapeProxyIt; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
			{
				if ( shapeProxyIt->volume.isPointInside(point) ) 
				{
					shapeTransformWorld.rotationMatrix = transform.rotationMatrix * shapeProxyIt->transform.rotationMatrix;
					shapeTransformWorld.position       = transform.position + (transform.rotationMatrix * shapeProxyIt->transform.position);

					if (shapeProxyIt->shape->isPointInShape(shapeTransformWorld, point))
					{
						return true;
					}
				}
			}

			return false;
		}

		void initAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			ObjectHandle2d* shapeProxyIt = shapes;

			AABB2d sum;
			jaTransform2 shapeTransformWorld;

			if (shapeProxyIt != nullptr)
			{
				shapeTransformWorld.rotationMatrix = transform.rotationMatrix * shapeProxyIt->transform.rotationMatrix;
				shapeTransformWorld.position = transform.position + (transform.rotationMatrix * shapeProxyIt->transform.position);

				shapeProxyIt->shape->initAABB2d(shapeTransformWorld, shapeProxyIt->volume);
				aabb = shapeProxyIt->volume;
				shapeProxyIt = shapeProxyIt->next;
			}
			else {
				aabb.min.x = transform.position.x - 0.5f;
				aabb.min.y = transform.position.y - 0.5f;
				aabb.max.x = transform.position.x + 0.5f;
				aabb.max.y = transform.position.y + 0.5f;
			}

			for (shapeProxyIt; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
			{
				shapeTransformWorld.rotationMatrix = transform.rotationMatrix * shapeProxyIt->transform.rotationMatrix;
				shapeTransformWorld.position = transform.position + (transform.rotationMatrix * shapeProxyIt->transform.position);

				shapeProxyIt->shape->initAABB2d(shapeTransformWorld, shapeProxyIt->volume);
				AABB2d::getSum(shapeProxyIt->volume, aabb, sum);
				aabb = sum;
			}
		}

		void updateAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			ObjectHandle2d* shapeProxyIt = shapes;

			AABB2d       sum;
			jaTransform2 shapeTransformWorld;

			if (shapeProxyIt != nullptr)
			{
				shapeTransformWorld.rotationMatrix = transform.rotationMatrix * shapeProxyIt->transform.rotationMatrix;
				shapeTransformWorld.position = transform.position + (transform.rotationMatrix * shapeProxyIt->transform.position);

				shapeProxyIt->shape->updateAABB2d(shapeTransformWorld, shapeProxyIt->volume);
				aabb = shapeProxyIt->volume;
				shapeProxyIt = shapeProxyIt->next;
			}
			else {
				aabb.min.x = transform.position.x - 0.5f;
				aabb.min.y = transform.position.y - 0.5f;
				aabb.max.x = transform.position.x + 0.5f;
				aabb.max.y = transform.position.y + 0.5f;
			}

			for (shapeProxyIt; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
			{
				shapeTransformWorld.rotationMatrix = transform.rotationMatrix * shapeProxyIt->transform.rotationMatrix;
				shapeTransformWorld.position       = transform.position + (transform.rotationMatrix * shapeProxyIt->transform.position);

				shapeProxyIt->shape->updateAABB2d(shapeTransformWorld, shapeProxyIt->volume);
				AABB2d::getSum(shapeProxyIt->volume, aabb, sum);
				aabb = sum;
			}
		}

		inline uint32_t sizeOf() const
		{
			return sizeof(MultiShape2d);
		}

		void clone(void* destination) const
		{
			MultiShape2d* newMultiShape = (MultiShape2d*)destination;
			memcpy(destination, this, sizeof(MultiShape2d));

			newMultiShape->shapeAllocator = this->shapeAllocator;
			newMultiShape->shapes         = nullptr;
			
			ObjectHandle2d* shapeProxyIt  = this->shapes;

			for (shapeProxyIt; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
			{
				newMultiShape->addShape(shapeProxyIt);
			}
		}

		void clone(void* destination, CacheLineAllocator* allocator) const
		{
			MultiShape2d* newMultiShape = (MultiShape2d*)destination;

			memcpy(destination, this, sizeof(MultiShape2d));
			newMultiShape->shapeAllocator = allocator;
			newMultiShape->shapes         = nullptr;
			
			ObjectHandle2d* shapeProxyIt = this->shapes;

			for (shapeProxyIt; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
			{
				newMultiShape->addShape(shapeProxyIt);
			}
		}

		void copy(MultiShape2d* multiShapeDst)
		{
			multiShapeDst->deleteShapes();

			ObjectHandle2d* shapeProxyIt = this->shapes;

			for (shapeProxyIt; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
			{
				multiShapeDst->addShape(shapeProxyIt);
			}
		}

		jaFloat getArea() const
		{
			jaFloat area = 0.0f;
			ObjectHandle2d* shapeProxy = shapes;

			while (shapeProxy != nullptr) {
				area += shapeProxy->shape->getArea();
				shapeProxy = shapeProxy->next;
			}
			return area;
		}

		void getMassData(MassData2d& massData)
		{
			ObjectHandle2d* shapeProxyIt = shapes;

			massData.mass    = GIL_ZERO;
			massData.interia = GIL_ZERO;
			massData.centerOfMass.setZero();

			while (shapeProxyIt != nullptr) {
				MassData2d proxyMassData;
				Shape2d*   shape = shapeProxyIt->shape;

				shape->getMassData(proxyMassData);

				jaFloat proxyMass = proxyMassData.mass;
				jaFloat massSum   = massData.mass + proxyMass;

				proxyMassData.centerOfMass = shapeProxyIt->transform.position + (shapeProxyIt->transform.rotationMatrix * proxyMassData.centerOfMass);
				jaVector2 comDistance      = massData.centerOfMass - proxyMassData.centerOfMass;

				massData.interia     += (proxyMass * proxyMassData.interia) + jaVectormath2::projection(comDistance, comDistance) * (proxyMass * massData.mass / massSum);
				massData.centerOfMass = jaVectormath2::lerp(massData.centerOfMass, proxyMassData.centerOfMass ,proxyMass / massSum);
				massData.mass         = massSum;
				
				shapeProxyIt = shapeProxyIt->next;
			}
	
		}

		jaFloat getSizeX()
		{
			return 0.0f;
		}

		jaFloat getSizeY()
		{
			return 0.0f;
		}

		void setSizeX(jaFloat size)
		{

		}

		void setSizeY(jaFloat size)
		{

		}

		inline void getInnerRandomPoint(jaVector2& point)
		{

		}

		uint16_t getVerticesNum() const
		{
			return 0;
		}

		jaFloat getDensity()
		{
			return 0.0f;
		}

		jaFloat getRestitution()
		{
			return 0.0f;
		}

		jaFloat getFriction()
		{
			return 0.0f;
		}

		void setDensity(jaFloat density)
		{
		
		}

		void setRestitution(jaFloat restitution)
		{
			
		}

		void setFriction(jaFloat friction)
		{
			
		}

		const char* getName() const
		{
			return "MultiShape";
		}
	};

}
