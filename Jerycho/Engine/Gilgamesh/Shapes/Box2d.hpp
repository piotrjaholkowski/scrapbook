#pragma once

#include "../../jaSetup.hpp"
#include "Shape2d.hpp"
#include "Shape2dTypes.hpp"

namespace gil
{

	class Box2d : public Shape2d
	{
	private:
		jaFloat halfWidth, halfHeight;
		jaFloat density;
		jaFloat restitution;
		jaFloat friction;

	public:

		inline Box2d(jaFloat width, jaFloat height) : Shape2d((uint8_t)ShapeType::Box), halfWidth(width / GIL_REAL(2.0)), halfHeight(height / GIL_REAL(2.0))
		{
			if (halfWidth < GIL_ZERO)
				halfWidth = -halfWidth;
			if (halfHeight < GIL_ZERO)
				halfHeight = -halfHeight;
		}

		inline Box2d() : Shape2d((uint8_t)ShapeType::Box), halfWidth(GIL_REAL(1.0)), halfHeight(GIL_REAL(1.0))
		{

		}

		inline void setBoxSize(jaFloat halfWidth, jaFloat halfHeight)
		{
			if (halfWidth < GIL_ZERO)
				this->halfWidth = -halfWidth;
			if (halfHeight < GIL_ZERO)
				this->halfHeight = -halfHeight;
		}

		inline jaVector2 findSupportPoint(const jaVector2& searchDirection) const
		{
			if (searchDirection.x < 0)
			{
				if (searchDirection.y < 0)
				{
					return jaVector2(-halfWidth, -halfHeight);
				}
				else
				{
					return jaVector2(-halfWidth, halfHeight);
				}
			}
			else
			{
				if (searchDirection.y < 0)
				{
					return jaVector2(halfWidth, -halfHeight);
				}
				else
				{
					return jaVector2(halfWidth, halfHeight);
				}
			}
		}

		//Points in counter-clockwise order
		//
		// 3---2
		// |   |
		// 0---1
		//
		inline uint16_t findSupportPointIndex(const jaVector2& searchDirection) const
		{
			if (searchDirection.x < 0)
			{
				if (searchDirection.y < 0)
				{
					return 0;
				}
				else
				{
					return  3;
				}
			}
			else
			{
				if (searchDirection.y < 0)
				{
					return 1;
				}
				else
				{
					return 2;
				}
			}
		}

		//Points in counter-clockwise order
		//
		// 3---2
		// |   |
		// 0---1
		//
		inline void getVertex(uint16_t vertexIndex, jaVector2& vertex) const
		{
			switch (vertexIndex)
			{
			case 0:
				vertex.x = -halfWidth;
				vertex.y = -halfHeight;
				break;
			case 1:
				vertex.x = halfWidth;
				vertex.y = -halfHeight;
				break;
			case 2:
				vertex.x = halfWidth;
				vertex.y = halfHeight;
				break;
			case 3:
				vertex.x = -halfWidth;
				vertex.y = halfHeight;
				break;
			}
		}

		//Points in counter-clockwise order
		//
		// 3---2
		// |   |
		// 0---1
		//
		inline void getSupportTriangle(uint16_t vertexIndex, jaVector2* triangle) const
		{
			uint16_t lowerIndex;
			uint16_t higherIndex;

			if (vertexIndex == 0)
			{
				lowerIndex = 3;
				higherIndex = 1;
			}
			else
			{
				if (vertexIndex == 3)
				{
					lowerIndex = 2;
					higherIndex = 0;
				}
				else
				{
					lowerIndex = vertexIndex - 1;
					higherIndex = vertexIndex + 1;
				}
			}

			getVertex(lowerIndex,  triangle[0]);
			getVertex(vertexIndex, triangle[1]);
			getVertex(higherIndex, triangle[2]);
		}

		inline jaFloat getHalfWidth()
		{
			return halfWidth;
		}

		inline jaFloat getHalfHeight()
		{
			return halfHeight;
		}

		inline jaFloat getWidth()
		{
			return 2 * halfWidth;
		}

		inline jaFloat getHeight()
		{
			return 2 * halfHeight;
		}

		inline void setWidth(jaFloat width)
		{
			this->halfWidth = width * 0.5f;
		}

		inline void setHeight(jaFloat height)
		{
			this->halfHeight = height * 0.5f;
		}

		bool isPointInShape(const jaTransform2& transform, const jaVector2& point)
		{
			jaVector2 diffrence = jaVectormath2::inverse(transform.rotationMatrix) *  (point - transform.position);
			if (diffrence.x > halfWidth)   return false;
			if (diffrence.x < -halfWidth)  return false;
			if (diffrence.y > halfHeight)  return false;
			if (diffrence.y < -halfHeight) return false;
			return true;
		}

		void initAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			jaMatrix2 invRot = jaVectormath2::inverse(transform.rotationMatrix);
			jaVector2 v1 = findSupportPoint(invRot * jaVector2(1.0f, 0.0f));
			jaVector2 v2 = findSupportPoint(invRot * jaVector2(0.0f, 1.0f));

			v1 = transform.rotationMatrix * v1;
			v2 = transform.rotationMatrix * v2;

			aabb.max = jaVector2( v1.x,  v2.y);
			aabb.min = jaVector2(-v1.x, -v2.y);

			aabb.max += transform.position;
			aabb.min += transform.position;
		}

		void updateAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			jaMatrix2 invRot = jaVectormath2::inverse(transform.rotationMatrix);
			jaVector2 v1 = findSupportPoint(invRot * jaVector2(1.0f, 0.0f));
			jaVector2 v2 = findSupportPoint(invRot * jaVector2(0.0f, 1.0f));

			v1 = transform.rotationMatrix * v1;
			v2 = transform.rotationMatrix * v2;

			aabb.max = jaVector2( v1.x,  v2.y);
			aabb.min = jaVector2(-v1.x, -v2.y);

			aabb.max += transform.position;
			aabb.min += transform.position;
		}

		inline uint32_t sizeOf() const
		{
			return sizeof(Box2d);
		}

		void clone(void* destination) const
		{
			memcpy(destination, this, sizeof(Box2d));
		}

		void clone(void* destination, CacheLineAllocator* allocator) const
		{
			memcpy(destination, this, sizeof(Box2d));
		}

		jaFloat getArea() const
		{
			return (2 * halfWidth * 2 * halfHeight);
		}

		void getMassData(MassData2d& massData)
		{
			jaFloat width  = 2 * halfWidth;
			jaFloat height = 2 * halfHeight;
			
			massData.mass           = width * height * density;
			massData.centerOfMass.x = 0.0f;
			massData.centerOfMass.y = 0.0f;
			massData.interia = massData.mass * ((width * width + height * height) / GIL_REAL(12.0));
		}

		jaFloat getSizeX()
		{
			return 2 * halfWidth;
		}

		jaFloat getSizeY()
		{
			return 2 * halfHeight;
		}

		void setSizeX(jaFloat size)
		{
			if (size < 0)
				size = -size;

			halfWidth = size * 0.5f;
		}

		void setSizeY(jaFloat size)
		{
			if (size < 0)
				size = -size;

			halfHeight = size * 0.5f;
		}

		inline void getInnerRandomPoint(jaVector2& point)
		{
			point.x = fmodf((float)(rand()), 2 * halfWidth);
			point.y = fmodf((float)(rand()), 2 * halfHeight);
			point.x -= halfWidth;
			point.y -= halfHeight;
		}

		uint16_t getVerticesNum() const
		{
			return 4;
		}

		jaFloat getDensity()
		{
			return density;
		}

		jaFloat getRestitution()
		{
			return restitution;
		}

		jaFloat getFriction()
		{
			return friction;
		}

		void setDensity(jaFloat density)
		{
			this->density = density;
		}
		
		void setRestitution(jaFloat restitution)
		{
			this->restitution = restitution;
		}

		void setFriction(jaFloat friction)
		{
			this->friction = friction;
		}

		void setParent(ObjectHandle2d* parent)
		{

		}

		const char* getName() const
		{
			return "Box";
		}
	};

}
