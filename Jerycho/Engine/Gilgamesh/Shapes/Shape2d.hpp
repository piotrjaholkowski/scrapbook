#pragma once

#include "../../jaSetup.hpp"
#include "Shape2dTypes.hpp"
#include "../../Math/VectorMath2d.hpp"
#include "../Mid/AABB2d.hpp"
#include "../../Allocators/CacheLineAllocator.hpp"

namespace gil
{
	class ObjectHandle2d;

	enum Shape2dFlag
	{
		GIL_SHAPE_SENSOR = 1
	};

	class MassData2d
	{
	public:
		jaFloat   mass;
		jaVector2 centerOfMass;
		jaFloat   interia;
	};

	class Shape2d
	{
	private:
		uint8_t shapeType;
		uint8_t shapeFlag;
	
	public:

		Shape2d(uint8_t shapeType);
		virtual void getVertex(uint16_t vertexIndex, jaVector2& vertex) const = 0;
		virtual jaVector2 findSupportPoint(const jaVector2& searchDirection) const = 0;
		//Support points in counter-clockwise order
		virtual uint16_t findSupportPointIndex(const jaVector2& searchDirection) const = 0;
		//Support triangle in counter-clockwise order
		virtual void getSupportTriangle(uint16_t vertexIndex, jaVector2* triangle) const = 0;
		virtual void updateAABB2d(const jaTransform2& transform, AABB2d& volume) = 0;
		virtual bool isPointInShape(const jaTransform2& transform, const jaVector2& point) = 0;
		virtual void initAABB2d(const jaTransform2& transform, AABB2d& volume) = 0;
		virtual void getInnerRandomPoint(jaVector2& point) = 0;
		virtual uint32_t sizeOf() const = 0;
		virtual void clone(void* destination) const = 0;
		virtual void clone(void* destination, CacheLineAllocator* allocator) const = 0;
		virtual jaFloat getArea() const = 0; // two - dimensional surface
		virtual void getMassData(MassData2d& massData) = 0;
		virtual jaFloat getSizeX() = 0;
		virtual jaFloat getSizeY() = 0;
		virtual void setSizeX(jaFloat size) = 0;
		virtual void setSizeY(jaFloat size) = 0;
		virtual uint16_t getVerticesNum() const = 0;

		virtual jaFloat getDensity()     = 0;
		virtual jaFloat getRestitution() = 0;
		virtual jaFloat getFriction()    = 0;

		virtual void setParent(ObjectHandle2d* handle) = 0;

		virtual void setDensity(jaFloat density)          = 0;
		virtual void setRestitution(jaFloat restitution)  = 0;
		virtual void setFriction(jaFloat friction)        = 0;

		inline uint8_t getType() const;

		inline void setSensor(bool state);
		inline bool isSensor();

		virtual const char* getName() const = 0;

		virtual ~Shape2d() = 0;
	};

	inline Shape2d::Shape2d(uint8_t shapeType) : shapeType(shapeType)
	{

	}

	inline Shape2d::~Shape2d()
	{

	}

	inline uint8_t Shape2d::getType() const
	{
		return this->shapeType;
	}

	inline void Shape2d::setSensor(bool state)
	{
		if (state)
		{
			shapeFlag |= GIL_SHAPE_SENSOR;
		}
		else
		{
			shapeFlag |= GIL_SHAPE_SENSOR;
			shapeFlag ^= GIL_SHAPE_SENSOR;
		}
	}

	inline bool Shape2d::isSensor()
	{
		return (shapeFlag & GIL_SHAPE_SENSOR);
	}

}
