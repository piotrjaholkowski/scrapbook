#pragma once

#include "../../jaSetup.hpp"
#include "../Shapes/Shape2d.hpp"
#include "../Shapes/Shape2dTypes.hpp"

namespace gil
{

	class ConvexPolygon2d : public Shape2d
	{
	public:
		jaVector2 vertices[ja::setup::gilgamesh::VERTICES_NUM2D];
		jaVector2 scale;
		uint16_t  verticesNum;
		jaFloat   density;
		jaFloat   restitution;
		jaFloat   friction;

		ConvexPolygon2d() : Shape2d((uint8_t)ShapeType::ConvexPolygon), verticesNum(3)
		{

		}

		void setVertices(const jaVector2* vertices, uint16_t verticesNum, jaVector2& translateOffset)
		{
			scale.x = 1.0f;
			scale.y = 1.0f;

			this->verticesNum = verticesNum;
			translateOffset.setZero(); 

			for (uint16_t i = 0; i < verticesNum; ++i)
			{
				translateOffset.x += vertices[i].x;
				translateOffset.y += vertices[i].y;
			}

			float fVerticesNum = (float)verticesNum;
			translateOffset.x /= fVerticesNum;
			translateOffset.y /= fVerticesNum;

			for (uint16_t i = 0; i < verticesNum; ++i)
			{
				this->vertices[i].x = vertices[i].x - translateOffset.x;
				this->vertices[i].y = vertices[i].y - translateOffset.y;
			}
		}

		void setVertices(const jaVector2* vertices, uint16_t verticesNum)
		{
			scale.x = 1.0f;
			scale.y = 1.0f;
			this->verticesNum = verticesNum;

			memcpy(this->vertices, vertices, sizeof(jaVector2) * verticesNum);
		}

		static void getShapeCenter(jaVector2* const vertices, const uint16_t verticesNum, jaVector2& shapeCenter)
		{
			shapeCenter.setZero();

			for (uint16_t i = 0; i < verticesNum; ++i)
			{
				shapeCenter.x += vertices[i].x;
				shapeCenter.y += vertices[i].y;
			}

			float fVerticesNum = (float)verticesNum;
			shapeCenter.x /= fVerticesNum;
			shapeCenter.y /= fVerticesNum;
		}

		jaVector2 const* getVertices() const
		{
			return this->vertices;
		}

		inline void getVertex(uint16_t vertexIndex, jaVector2& vertex) const
		{
			vertex = this->scale * this->vertices[vertexIndex];
		}

		inline jaVector2 findSupportPoint(const jaVector2& searchDirection) const
		{
			jaFloat maxDot = jaVectormath2::projection(searchDirection, jaVector2(scale.x * vertices[0].x, scale.y * vertices[0].y));
			jaFloat dot;
			uint16_t maxIndex = 0;

			for (uint16_t i = 1; i<verticesNum; ++i)
			{
				dot = jaVectormath2::projection(searchDirection, jaVector2(scale.x * vertices[i].x, scale.y * vertices[i].y));
				if (dot > maxDot)
				{
					maxDot = dot;
					maxIndex = i;
				}
			}

			return jaVector2(scale.x * vertices[maxIndex].x, scale.y * vertices[maxIndex].y); 
		}

		inline uint16_t findSupportPointIndex(const jaVector2& searchDirection) const
		{
			jaFloat  maxDot = jaVectormath2::projection(searchDirection, jaVector2(scale.x * vertices[0].x, scale.y * vertices[0].y));
			jaFloat  dot;
			uint16_t maxIndex = 0;
	
			for (uint16_t i = 1; i<verticesNum; ++i)
			{
				dot = jaVectormath2::projection(searchDirection, jaVector2(scale.x * vertices[i].x, scale.y * vertices[i].y));
				if (dot > maxDot)
				{
					maxDot = dot;
					maxIndex = i;
				}
			}

			return maxIndex;
		}

		inline void getSupportTriangle(uint16_t vertexIndex, jaVector2* triangle) const
		{
			uint16_t lowerIndex;
			uint16_t higherIndex = (vertexIndex + 1) % verticesNum;

			if (vertexIndex == 0)
				lowerIndex = verticesNum - 1;
			else
				lowerIndex = vertexIndex - 1;

			getVertex(lowerIndex , triangle[0]);
			getVertex(vertexIndex, triangle[1]);
			getVertex(higherIndex, triangle[2]);
		}

		bool isPointInShape(const jaTransform2& transform, const jaVector2& point)
		{
			jaMatrix2 invRot       = jaVectormath2::inverse(transform.rotationMatrix);
			jaVector2 positionDiff = point - transform.position;
			jaVector2 searchDir    = invRot * positionDiff;

			uint16_t supportIndex  = findSupportPointIndex(searchDir);
			uint16_t nextIndex     = (supportIndex + 1) % verticesNum;
			uint16_t previousIndex = supportIndex - 1;
			
			if (supportIndex == 0)
				previousIndex = verticesNum - 1;

			jaVector2 supportVec;
			jaVector2 nextVec;
			jaVector2 prevVec;

			getVertex(supportIndex , supportVec);
			getVertex(nextIndex    , nextVec);
			getVertex(previousIndex, prevVec);
			jaVector2 e0 = prevVec - supportVec;
			jaVector2 e1 = supportVec - nextVec;
			
			jaVector2 e0n(-e0.y, e0.x);
			jaVector2 e1n(-e1.y, e1.x);

			jaVector2 worldSupportVertex = transform * supportVec; 
			jaVector2 castPoint          = point - worldSupportVertex;

			if (0.0f < jaVectormath2::projection(e0n, castPoint))
				return false;
			
			if (0.0f < jaVectormath2::projection(e1n, castPoint))
				return false;

			return true;
		}

		void initAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{ 
			jaMatrix2 invRot = jaVectormath2::inverse(transform.rotationMatrix);
			jaVector2 left  = findSupportPoint(invRot * jaVector2(-1.0f,  0.0f));
			jaVector2 top   = findSupportPoint(invRot * jaVector2( 0.0f,  1.0f));
			jaVector2 right = findSupportPoint(invRot * jaVector2( 1.0f,  0.0f));
			jaVector2 down  = findSupportPoint(invRot * jaVector2( 0.0f, -1.0f));

			left  = transform.rotationMatrix * left;
			top   = transform.rotationMatrix * top;
			right = transform.rotationMatrix * right;
			down  = transform.rotationMatrix * down;

			aabb.max = jaVector2(right.x, top.y );
			aabb.min = jaVector2(left.x , down.y);

			aabb.max += transform.position;
			aabb.min += transform.position;
		}

		void updateAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			jaMatrix2 invRot = jaVectormath2::inverse(transform.rotationMatrix);
			jaVector2 left  = findSupportPoint(invRot * jaVector2(-1.0f,  0.0f));
			jaVector2 top   = findSupportPoint(invRot * jaVector2( 0.0f,  1.0f));
			jaVector2 right = findSupportPoint(invRot * jaVector2( 1.0f,  0.0f));
			jaVector2 down  = findSupportPoint(invRot * jaVector2( 0.0f, -1.0f));

			left  = transform.rotationMatrix * left;
			top   = transform.rotationMatrix * top;
			right = transform.rotationMatrix * right;
			down  = transform.rotationMatrix * down;

			aabb.max = jaVector2(right.x, top.y);
			aabb.min = jaVector2(left.x , down.y);

			aabb.max += transform.position;
			aabb.min += transform.position;
		}

		inline uint32_t sizeOf() const
		{
			return sizeof(ConvexPolygon2d);
		}

		void clone(void* destination) const
		{
			memcpy(destination, this, sizeof(ConvexPolygon2d));
		}

		void clone(void* destination, CacheLineAllocator* allocator) const
		{
			memcpy(destination, this, sizeof(ConvexPolygon2d));
		}

		jaFloat getArea() const
		{
			float posDet = 0.0f;
			float negDet = 0.0f;
			
			uint16_t nextIndex = 0;

			jaVector2 vertex;
			jaVector2 nextVertex;

			for (uint16_t i = 0; i < (verticesNum - 1); ++i)
			{
				++nextIndex;
				getVertex(nextIndex, nextVertex);
				posDet += (vertex.x * nextVertex.y);
				negDet += (vertex.y * nextVertex.x);
				vertex = nextVertex;
			}

			return ((posDet - negDet) * 0.5f);
		}

		void getMassData(MassData2d& massData)
		{
			massData.centerOfMass.x = 0.0f;
			massData.centerOfMass.y = 0.0f;

			const jaFloat k_inv3 = 1.0f / 3.0f;
			jaFloat       area   = 0.0f;
			jaFloat       I      = 0.0f;

			for (uint16_t i = 0; i < verticesNum; ++i)
			{
				jaVector2 e1;
				jaVector2 e2;
				getVertex(i, e1);

				if ((i + 1) < verticesNum)
					getVertex(i + 1, e2);
				else
					getVertex(0    , e2);

				jaFloat D = jaVectormath2::det(e1, e2);
			
				jaFloat triangleArea = 0.5f * D;
				area += triangleArea;

				jaFloat ex1 = e1.x, ey1 = e1.y;
				jaFloat ex2 = e2.x, ey2 = e2.y;

				jaFloat intx2 = ex1*ex1 + ex2*ex1 + ex2*ex2;
				jaFloat inty2 = ey1*ey1 + ey2*ey1 + ey2*ey2;

				I += (0.25f * k_inv3 * D) * (intx2 + inty2);
			}

			massData.mass    = area * density;
			massData.interia = density * I;
		}

		jaFloat getSizeX()
		{
			jaFloat minProjection = 0.0f;
			jaFloat maxProjection = 0.0f;

			jaVector2 supportDir(1.0f, 0.0f);
			jaVector2 vertex;

			for (uint16_t i = 0; i < verticesNum; ++i)
			{
				getVertex(i, vertex);
				jaFloat projectedPoint = jaVectormath2::projection(supportDir, vertex);

				if (projectedPoint < minProjection)
					minProjection = projectedPoint;

				if (projectedPoint > maxProjection)
					maxProjection = projectedPoint;
			}

			return fabs(maxProjection - minProjection);
		}

		jaFloat getSizeY()
		{
			jaFloat minProjection = 0.0f;
			jaFloat maxProjection = 0.0f;

			jaVector2 supportDir(0.0f, 1.0f);
			jaVector2 vertex;

			for (uint16_t i = 0; i < verticesNum; ++i)
			{
				getVertex(i, vertex);
				jaFloat projectedPoint = jaVectormath2::projection(supportDir, vertex);

				if (projectedPoint < minProjection)
					minProjection = projectedPoint;

				if (projectedPoint > maxProjection)
					maxProjection = projectedPoint;
			}

			return fabs(maxProjection - minProjection);
		}

		void setSizeX(jaFloat size)
		{
			jaFloat minProjection = 0.0f;
			jaFloat maxProjection = 0.0f;

			jaVector2 supportDir(1.0f, 0.0f);

			for (int16_t i = 0; i < verticesNum; ++i)
			{
				jaFloat projectedPoint = jaVectormath2::projection(supportDir, vertices[i]);

				if (projectedPoint < minProjection)
					minProjection = projectedPoint;

				if (projectedPoint > maxProjection)
					maxProjection = projectedPoint;
			}

			float sizeX = fabs(maxProjection - minProjection);

			scale.x = fabs(size) / sizeX;
		}

		void setSizeY(jaFloat size)
		{
			jaFloat minProjection = 0.0f;
			jaFloat maxProjection = 0.0f;

			jaVector2 supportDir(0.0f, 1.0f);

			for (int16_t i = 0; i < verticesNum; ++i)
			{
				jaFloat projectedPoint = jaVectormath2::projection(supportDir, vertices[i]);

				if (projectedPoint < minProjection)
					minProjection = projectedPoint;

				if (projectedPoint > maxProjection)
					maxProjection = projectedPoint;
			}

			float sizeY = fabs(maxProjection - minProjection);

			scale.y = fabs(size) / sizeY;
		}

		uint16_t getVerticesNum() const
		{
			return verticesNum;
		}

		inline void getInnerRandomPoint(jaVector2& point)
		{
			point.x = 0.0f;
			point.y = 0.0f;
		}

		jaFloat getDensity()
		{
			return density;
		}

		jaFloat getRestitution()
		{
			return restitution;
		}

		jaFloat getFriction()
		{
			return friction;
		}

		void setDensity(jaFloat density)
		{
			this->density = density;
		}

		void setRestitution(jaFloat restitution)
		{
			this->restitution = restitution;
		}

		void setFriction(jaFloat friction)
		{
			this->friction = friction;
		}

		void setParent(ObjectHandle2d* parent)
		{
		}

		const char* getName() const
		{
			return "ConvexPolygon";
		}
	};

}
