#pragma once

#include "Shape2d.hpp"
#include "Shape2dTypes.hpp"

#include "../../Allocators/CacheLineAllocator.hpp"

namespace gil
{
	class Point2d : public Shape2d
	{
	private:
		jaFloat density;
		jaFloat restitution;
		jaFloat friction;

	public:
		inline Point2d() : Shape2d((uint8_t)ShapeType::Point)
		{

		}

		inline ~Point2d()
		{

		}

		inline void getVertex(uint16_t vertexIndex, jaVector2& vertex) const
		{
			vertex.x = 0.0f;
			vertex.y = 0.0f;
		}

		inline void getSupportTriangle(uint16_t vertexIndex, jaVector2* triangle) const
		{
		}

		bool isPointInShape(const jaTransform2& transform, const jaVector2& point)
		{
			if ((transform.position.x == point.x) && (transform.position.y == point.y))
				return true;

			return false;
		}

		inline jaVector2 findSupportPoint(const jaVector2& searchDirection) const
		{
			return jaVector2(0.0f, 0.0f);
		}

		void clone(void* destination) const
		{
			memcpy(destination, this, sizeof(Point2d));
		}

		void clone(void* destination, CacheLineAllocator* allocator) const
		{
			memcpy(destination, this, sizeof(Point2d));
		}

		uint16_t getVerticesNum() const
		{
			return 1;
		}

		inline uint16_t findSupportPointIndex(const jaVector2& searchDirection) const
		{
			return 0;
		}

		void initAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			aabb.max = transform.position;
			aabb.min = transform.position;
		}

		void updateAABB2d(const jaTransform2& transform, AABB2d& aabb)
		{
			aabb.max += transform.position;
			aabb.min += transform.position;
		}

		inline void getInnerRandomPoint(jaVector2& point)
		{
			point.x = 0.0f;
			point.y = 0.0f;
		}

		jaFloat getArea() const
		{
			return 0.0f;
		}

		void getMassData(MassData2d& massData)
		{
			massData.mass = density;
			massData.centerOfMass.x = 0.0f;
			massData.centerOfMass.y = 0.0f;
			massData.interia = density;
		}

		jaFloat getSizeX()
		{
			return 0.0f;
		}

		jaFloat getSizeY()
		{
			return 0.0f;
		}

		void setSizeX(jaFloat size)
		{

		}

		void setSizeY(jaFloat size)
		{
	
		}

		inline uint32_t sizeOf() const
		{
			return sizeof(Point2d);
		}

		jaFloat getDensity()
		{
			return density;
		}

		jaFloat getRestitution()
		{
			return restitution;
		}

		jaFloat getFriction()
		{
			return friction;
		}

		void setDensity(jaFloat density)
		{
			this->density = density;
		}

		void setRestitution(jaFloat restitution)
		{
			this->restitution = restitution;
		}

		void setFriction(jaFloat friction)
		{
			this->friction = friction;
		}

		void setParent(ObjectHandle2d* parent)
		{

		}

		const char* getName() const
		{
			return "Point";
		}
	};
}