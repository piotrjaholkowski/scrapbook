#include "Scene2d.hpp"

namespace gil
{

	Scene2d::Scene2d(CacheLineAllocator* commonAllocator /*=NULL*/)
	{
		idGenerator = 0;
		freeId.reserve(64);
		this->commonAllocator = commonAllocator;
	}

	ObjectHandle2d* Scene2d::createObject(const Shape2d* shape, const jaTransform2& transform)
	{
		uint16_t id;
		if (freeId.size() != 0)
		{
			id = freeId.back();
			freeId.pop_back();
		}
		else
		{
			id = idGenerator++;
		}

		return new (commonAllocator->allocate(sizeof(ObjectHandle2d))) ObjectHandle2d(id, shape, transform, this->commonAllocator);
	}

	void Scene2d::changeObjectShape(ObjectHandle2d* handle, Shape2d* newShape)
	{
		Shape2d* oldShape = handle->getShape();
		uint32_t oldShapeSize = oldShape->sizeOf();
		oldShape->~Shape2d();
		commonAllocator->free(oldShape, oldShapeSize);

		Shape2d* shapeCopy = (Shape2d*)commonAllocator->allocate(newShape->sizeOf());

		newShape->clone(shapeCopy, commonAllocator);
		handle->shape = shapeCopy;
		handle->initAABB2d();
	}

	void Scene2d::deleteObject(ObjectHandle2d* handle)
	{
		freeId.push_back(handle->getId());
		Shape2d* shape = handle->getShape();
		uint32_t shapeSize = shape->sizeOf();
		shape->~Shape2d();
		
		commonAllocator->free(handle, sizeof(ObjectHandle2d));
		commonAllocator->free(shape, shapeSize);
	}

	void Scene2d::setNarrowConfiguration(Narrow2dConfiguration configuration)
	{
		conf = configuration;
	}

	void Scene2d::clearIdGenerator()
	{
		idGenerator = 0;
		freeId.clear();
		freeId.reserve(64);
	}

}