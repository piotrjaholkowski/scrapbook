#include "HashGrid.hpp"
#include "../../Manager/ThreadManager.hpp"
#include "../../Manager/RecycledMemoryManager.hpp"
#include "../Threads/Threads.hpp"
#include "../../Utility/String.hpp"

#include <assert.h>

using namespace gil;
using namespace ja;

void generateBucketMasks(HashCodes* bucketMasks)
{
	bucketMasks[0].clear();
	bucketMasks[0].addHashCode(0);

	for (uint32_t i = 1; i < 128; ++i)
	{
		bucketMasks[i].hashCodes[0] = bucketMasks[i - 1].hashCodes[0];
		bucketMasks[i].hashCodes[1] = bucketMasks[i - 1].hashCodes[1];
		bucketMasks[i].hashCodes[2] = bucketMasks[i - 1].hashCodes[2];
		bucketMasks[i].hashCodes[3] = bucketMasks[i - 1].hashCodes[3];

		bucketMasks[i].addHashCode(i);
	}

	for (uint32_t i = 0; i < 128; ++i)
	{
		bucketMasks[i].hashCodes[0] = ~bucketMasks[i].hashCodes[0];
		bucketMasks[i].hashCodes[1] = ~bucketMasks[i].hashCodes[1];
		bucketMasks[i].hashCodes[2] = ~bucketMasks[i].hashCodes[2];
		bucketMasks[i].hashCodes[3] = ~bucketMasks[i].hashCodes[3];
	}

}

HashGrid::HashGrid(int16_t bucketWidth, int16_t bucketHeight, jaFloat cellSize, CacheLineAllocator* allocator) : Scene2d(allocator), cellSize(cellSize), bucketWidth(bucketWidth), bucketHeight(bucketHeight)
{
	this->bucketNum = bucketWidth * bucketHeight;
	assert(bucketNum <= setup::gilgamesh::MAX_BUCKET_NUM);

	uint32_t nodeMemory = setup::gilgamesh::NODE_ALLOCATED_DATA_BLOCK * sizeof(HashNode);

	nodeAllocator = new PoolAllocator<HashNode>(nodeMemory,16);
	
	hashBucket        = new HashCell[2 * bucketNum];
	hashBucketStatic  = &hashBucket[0];
	hashBucketDynamic = &hashBucket[bucketNum];

	collisionPairCache = new StackAllocator<HashCollisionPair>(100,16);
	conversionFactor   = GIL_REAL(1.0) / cellSize;

	lastStaticObject  = nullptr;
	lastDynamicObject = nullptr;

	testPairBitarray = nullptr;

	const uint32_t alignTo = 16;
	this->bucketMasksMem = new uint8_t[((sizeof(HashCodes) * 128) + alignTo)];
	this->bucketMasks    = (HashCodes*)(bucketMasksMem + ((uintptr_t)(this->bucketMasksMem) % alignTo));
	generateBucketMasks(this->bucketMasks);
}

HashGrid::~HashGrid()
{
	delete nodeAllocator;
	delete collisionPairCache;
	delete[] bucketMasksMem;
	delete[] hashBucket;
}

void HashGrid::rebuild(int16_t bucketWidth, int16_t bucketHeight, jaFloat cellSize) {
	if ((bucketWidth == this->bucketWidth) && (bucketHeight == this->bucketHeight) && (cellSize == this->cellSize))
		return;

	this->cellSize     = cellSize;
	this->bucketWidth  = bucketWidth;
	this->bucketHeight = bucketHeight;
	this->bucketNum    = bucketWidth * bucketHeight;
	assert(bucketNum <= ja::setup::gilgamesh::MAX_BUCKET_NUM);

	nodeAllocator->clear();

	delete[] hashBucket;
	hashBucket        = new HashCell[2 * bucketNum];
	hashBucketStatic  = &hashBucket[0];
	hashBucketDynamic = &hashBucket[bucketNum];

	conversionFactor = GIL_REAL(1.0) / cellSize;

	ObjectHandle2d* itDynamicObject = lastDynamicObject;
	for (itDynamicObject; itDynamicObject != nullptr; itDynamicObject = itDynamicObject->prev) {
		itDynamicObject->hashProxy.firstNode = nullptr;
		insertDynamicObject(itDynamicObject);
	}

	ObjectHandle2d* itStaticObject = lastStaticObject;
	for (itStaticObject; itStaticObject != nullptr; itStaticObject = itStaticObject->prev) {
		itStaticObject->hashProxy.firstNode = nullptr;
		insertStaticObject(itStaticObject);
	}
}

ObjectHandle2d* HashGrid::createStaticObject( const Shape2d* shape, const jaTransform2& transform)
{
	ObjectHandle2d* handle = this->createObject(shape,transform);
	
	if(lastStaticObject==nullptr)
		lastStaticObject = handle;
	else {
		lastStaticObject->next = handle;
		handle->prev =lastStaticObject;
		lastStaticObject = handle;
	}

	handle->setDynamic(false);
	insertStaticObject(handle);

	return handle;
}

ObjectHandle2d* HashGrid::createDynamicObject( const Shape2d* shape, const jaTransform2& transform)
{
	ObjectHandle2d* handle = createObject(shape,transform);

	if(lastDynamicObject==nullptr)
		lastDynamicObject = handle;
	else {
		lastDynamicObject->next = handle;
		handle->prev =lastDynamicObject;
		lastDynamicObject = handle;
	}

	handle->setDynamic(true);

	insertDynamicObject(handle);
	
	return handle;
}

void HashGrid::changeObjectShape(ObjectHandle2d* handle, Shape2d* newShape)
{
	Scene2d::changeObjectShape(handle, newShape);
	handle->initAABB2d();
	
	if(handle->isDynamic())
	{
		updateDynamicObject(handle);
	}
	else
	{
		updateStaticObject(handle);
	}
}

void HashGrid::insertStaticObject( ObjectHandle2d* handle )
{
	AABB2d     aabb  = handle->getAABB2d();
	HashProxy* proxy = &handle->hashProxy;

	int16_t x0 = (int16_t)(aabb.min.x * conversionFactor);
	int16_t y0 = (int16_t)(aabb.min.y * conversionFactor);
	int16_t x1 = (int16_t)(aabb.max.x * conversionFactor);
	int16_t y1 = (int16_t)(aabb.max.y * conversionFactor);

	{
		proxy->cellCoordinate[0].x = x0;
		proxy->cellCoordinate[0].y = y0;
		proxy->cellCoordinate[1].x = x1;
		proxy->cellCoordinate[1].y = y1;

		int16_t dx = (x1 - x0) + x0;
		int16_t dy = (y1 - y0) + y0;

		proxy->hashCodes.clear();

		for (int16_t i = x0; i <= dx; ++i)
		{
			for (int16_t z = y0; z <= dy; ++z)
			{
				int16_t hash = getCellHash(i, z);
				
				if (false == proxy->hashCodes.isHashCodeAdded(hash))
				{
					proxy->hashCodes.addHashCode(hash);
					HashNode* hashNode = createStaticNode(hash, handle);
					proxy->pushBack(hashNode);
				}
			}
		}
	}
}

void HashGrid::insertDynamicObject( ObjectHandle2d* handle )
{
	AABB2d     aabb  = handle->getAABB2d();
	HashProxy* proxy = &handle->hashProxy;

	int16_t x0 = (int16_t)(aabb.min.x * conversionFactor); 
	int16_t y0 = (int16_t)(aabb.min.y * conversionFactor);
	int16_t x1 = (int16_t)(aabb.max.x * conversionFactor);
	int16_t y1 = (int16_t)(aabb.max.y * conversionFactor);

	{
		proxy->cellCoordinate[0].x = x0;
		proxy->cellCoordinate[0].y = y0;
		proxy->cellCoordinate[1].x = x1;
		proxy->cellCoordinate[1].y = y1;

		int16_t dx = (x1 - x0) + x0;
		int16_t dy = (y1 - y0) + y0;

		proxy->hashCodes.clear();

		for (int16_t i = x0; i <= dx; ++i)
		{
			for (int16_t z = y0; z <= dy; ++z)
			{
				int16_t hash = getCellHash(i, z);

				if (false == proxy->hashCodes.isHashCodeAdded(hash))
				{
					proxy->hashCodes.addHashCode(hash);
					HashNode* hashNode = createDynamicNode(hash, handle);
					proxy->pushBack(hashNode);
				}
			}
		}
	}
}

void HashGrid::removeStaticProxyNodes( HashProxy* proxy)
{
	HashNode* nodeIt = proxy->firstNode;

	while(nodeIt != nullptr)
	{
		if (nodeIt->prev!=nullptr)
		{
			//make link beetwen nodes which surround deleted node of hashProxy
			nodeIt->prev->next = nodeIt->next;
			if(nodeIt->next != nullptr)
			{
				nodeIt->next->prev = nodeIt->prev;
			}
		}
		else
		{
			//if nodeIt-prev is null that means deleted node is first at hushBucket
			if(nodeIt->next!=nullptr)
			{
				hashBucketStatic[nodeIt->bucketId].firsNode = nodeIt->next;
				nodeIt->next->prev = nullptr;
			}
			else
			{
				//There is only one object at hashBucket so null the firstNode
				hashBucketStatic[nodeIt->bucketId].firsNode = nullptr;
			}
		}

		nodeAllocator->free(nodeIt);
		nodeIt = nodeIt->nextProxyNode;
	}

	//it will be probably deleted anyway but i'm nulling it
	proxy->firstNode = nullptr;
}

void HashGrid::removeDynamicProxyNodes( HashProxy* proxy)
{
	HashNode* nodeIt = proxy->firstNode;

	while(nodeIt != nullptr)
	{
		if (nodeIt->prev != nullptr)
		{
			nodeIt->prev->next = nodeIt->next;
			if(nodeIt->next != nullptr)
			{
				nodeIt->next->prev = nodeIt->prev;
			}
		}
		else
		{
			if(nodeIt->next != nullptr)
			{
				hashBucketDynamic[nodeIt->bucketId].firsNode = nodeIt->next;
				nodeIt->next->prev = nullptr;
			}
			else
			{
				hashBucketDynamic[nodeIt->bucketId].firsNode = nullptr;
			}
		}

		nodeAllocator->free(nodeIt);
		nodeIt = nodeIt->nextProxyNode;
	}
	
	proxy->firstNode = nullptr;
}

void HashGrid::updateDynamicObject( ObjectHandle2d* handle, bool updateVolume)
{
	HashProxy* proxy = &handle->hashProxy;

	if(updateVolume)
	{
		handle->updateAABB2d();
	} 

	AABB2d  aabb = handle->getAABB2d();
	int16_t x0   = (int16_t)(aabb.min.x * conversionFactor); 
	int16_t y0   = (int16_t)(aabb.min.y * conversionFactor);
	int16_t x1   = (int16_t)(aabb.max.x * conversionFactor);
	int16_t y1   = (int16_t)(aabb.max.y * conversionFactor);

	if (!proxy->checkIfCoordinatesEqual(x0, y0, x1, y1)) // cell positions changed
	{
		proxy->cellCoordinate[0].x = x0;
		proxy->cellCoordinate[0].y = y0;
		proxy->cellCoordinate[1].x = x1;
		proxy->cellCoordinate[1].y = y1;

		int16_t dx = (x1 - x0) + x0;
		int16_t dy = (y1 - y0) + y0;

		HashCodes oldHashCodes = proxy->hashCodes;
		proxy->hashCodes.clear();

		for (int16_t i = x0; i <= dx; ++i)
		{
			for (int16_t z = y0; z <= dy; ++z)
			{
				int16_t hash = getCellHash(i, z);

				if ((false == oldHashCodes.isHashCodeAdded(hash)) && (false == proxy->hashCodes.isHashCodeAdded(hash)))
				{				
					HashNode* hashNode = createDynamicNode(hash, handle);
					proxy->pushBack(hashNode);
				}

				proxy->hashCodes.addHashCode(hash);
			}
		}

		removeOldDynamicHashes(proxy);
	}
}

void HashGrid::updateStaticObject(ObjectHandle2d* handle)
{
	HashProxy* proxy = &handle->hashProxy;
	handle->updateAABB2d(); 
	AABB2d aabb = handle->getAABB2d();

	int16_t x0  = (int16_t)(aabb.min.x * conversionFactor); 
	int16_t y0  = (int16_t)(aabb.min.y * conversionFactor);
	int16_t x1  = (int16_t)(aabb.max.x * conversionFactor);
	int16_t y1  = (int16_t)(aabb.max.y * conversionFactor);

	if (!proxy->checkIfCoordinatesEqual(x0, y0, x1, y1)) // cell positions changed
	{
		proxy->cellCoordinate[0].x = x0;
		proxy->cellCoordinate[0].y = y0;
		proxy->cellCoordinate[1].x = x1;
		proxy->cellCoordinate[1].y = y1;

		int16_t dx = (x1 - x0) + x0;
		int16_t dy = (y1 - y0) + y0;

		HashCodes oldHashCodes = proxy->hashCodes;
		proxy->hashCodes.clear();

		for (int16_t i = x0; i <= dx; ++i)
		{
			for (int16_t z = y0; z <= dy; ++z)
			{
				int16_t hash = getCellHash(i, z);

				if ((false == oldHashCodes.isHashCodeAdded(hash)) && (false == proxy->hashCodes.isHashCodeAdded(hash)))
				{			
					HashNode* hashNode = createStaticNode(hash, handle);
					proxy->pushBack(hashNode);
				}

				proxy->hashCodes.addHashCode(hash);
			}
		}

		removeOldStaticHashes(proxy);
	}
}

void HashGrid::removeObject(ObjectHandle2d* handle)
{
	HashProxy* proxy = &handle->hashProxy;

	if(handle->isDynamic())
	{
		removeDynamicProxyNodes(proxy);
		if(handle->prev == nullptr) // first dynamic object
		{
			if(handle->next == nullptr)
			{
				lastDynamicObject = nullptr; // first and last dynamic object
			}
			else
			{
				handle->next->prev = nullptr; // first dynamic object
			}
		}
		else
		{
			if(handle->next == nullptr)
			{
				lastDynamicObject = handle->prev; // last dynamic object
				lastDynamicObject->next = nullptr;
			}
			else
			{
				handle->next->prev = handle->prev; // in the middle
				handle->prev->next = handle->next;
			}
		}
	}
	else 
	{
		removeStaticProxyNodes(proxy);
		if(handle->prev == nullptr) // first dynamic object
		{
			if(handle->next == nullptr)
			{
				lastStaticObject = nullptr; // first and last dynamic object
			}
			else
			{
				handle->next->prev = nullptr; // first dynamic object
			}
		}
		else
		{
			if(handle->next == nullptr)
			{
				lastStaticObject = handle->prev; // last dynamic object
				lastStaticObject->next = nullptr;
			}
			else
			{
				handle->next->prev = handle->prev; // in the middle
				handle->prev->next = handle->next;
			}
		}
	}

	deleteObject(handle);
}

void HashGrid::setObjectType(ObjectHandle2d* handle, bool isDynamic)
{
	HashProxy* proxy = &handle->hashProxy;
	if(handle->isDynamic())
	{
		removeDynamicProxyNodes(proxy);

		if(handle->prev == nullptr) // first dynamic object
		{
			if(handle->next == nullptr)
			{
				lastDynamicObject = nullptr; // first and last dynamic object
			}
			else
			{
				handle->next->prev = nullptr; // first dynamic object
			}
		}
		else
		{
			if(handle->next == nullptr)
			{
				lastDynamicObject = handle->prev; // last dynamic object
				lastDynamicObject->next = nullptr;
			}
			else
			{
				handle->next->prev = handle->prev; // in the middle
				handle->prev->next = handle->next;
			}
		}
	}
	else 
	{
		removeStaticProxyNodes(proxy);

		if(handle->prev == nullptr) // first dynamic object
		{
			if(handle->next == nullptr)
			{
				lastStaticObject = nullptr; // first and last dynamic object
			}
			else
			{
				handle->next->prev = nullptr; // first dynamic object
			}
		}
		else
		{
			if(handle->next == nullptr)
			{
				lastStaticObject = handle->prev; // last dynamic object
				lastStaticObject->next = nullptr;
			}
			else
			{
				handle->next->prev = handle->prev; // in the middle
				handle->prev->next = handle->next;
			}
		}
	}

	handle->setDynamic(isDynamic);
	
	if(isDynamic)
	{
		insertDynamicObject(handle);

		if(lastDynamicObject == nullptr)
		{
			lastDynamicObject = handle;
			handle->next = nullptr;
			handle->prev = nullptr;
		}
		else
		{
			lastDynamicObject->next = handle;
			handle->next = nullptr;
			handle->prev = lastDynamicObject;
			lastDynamicObject = handle;
		}
	}
	else
	{
		insertStaticObject(handle);

		if(lastStaticObject == nullptr)
		{
			lastStaticObject = handle;
			handle->next = nullptr;
			handle->prev = nullptr;
		}
		else
		{
			lastStaticObject->next = handle;
			handle->next = nullptr;
			handle->prev = lastStaticObject;
			lastStaticObject = handle;
		}
	}
}

uint32_t HashGrid::generateCollisionsMultiThread(CollisionDataPairMultithread* dataPairCache, void* memoryContactCache, int32_t memoryContactCacheSize)
{
	ObjectHandle2d* itObject;
	HashProxy*      hashProxy;
	HashNode*       itNode;
	HashNode*       itProxy;

	ThreadManager* threadManager = ThreadManager::getSingleton();

	itObject         = lastDynamicObject;
	testPairBitarray = (int32_t*)(RecycledMemoryManager::getSingleton()->getMemoryBlock(GIL_TEST_PAIR_ARRAY_SIZE));
	clearTestPairArray(testPairBitarray);

	uint32_t collisionDataIt = 0 ;

	while (itObject != nullptr) // iterating through dynamic bodies
	{
		hashProxy = &itObject->hashProxy;
		itProxy   = hashProxy->firstNode;
		while (itProxy != nullptr) // iterating through nodes of proxy of current body
		{
			itNode = itProxy->next; // next node in the same bucket
			while (itNode != nullptr) // check in dynamic nodes
			{
				if (collisionFilter(itProxy->handle, itNode->handle)) {
					if (AABB2d::intersect(itProxy->handle->volume, itNode->handle->volume))
					{
						if (itProxy->handle->getId() != itNode->handle->getId())
						{
							if (testPair(itProxy->handle->getId(), itNode->handle->getId())) // Test if that pair was already checked
							{
								dataPairCache[collisionDataIt].handleA = itProxy->handle;
								dataPairCache[collisionDataIt].handleB = itNode->handle;
								++collisionDataIt;
							}
						}
					}
				}
				itNode = itNode->next; // next node in the same bucket
			}

			itNode = hashBucketStatic[itProxy->bucketId].firsNode;
			while (itNode != nullptr) // check in static nodes
			{
				if (collisionFilter(itProxy->handle, itNode->handle)) {
					if (AABB2d::intersect(itProxy->handle->volume, itNode->handle->volume))
					{
						if (testPair(itProxy->handle->getId(), itNode->handle->getId())) // Test if that pair was already checked
						{
							dataPairCache[collisionDataIt].handleA = itProxy->handle;
							dataPairCache[collisionDataIt].handleB = itNode->handle;
							++collisionDataIt;
						}
					}
				}
				itNode = itNode->next;
			}
			itProxy = itProxy->nextProxyNode;
		}
		itObject = itObject->prev;
	}

	threadManager->createTaskArray(dataPairCache, collisionDataIt, memoryContactCache, memoryContactCacheSize , Thread::getCollision, this);
	threadManager->waitUntilFinish();
	RecycledMemoryManager::getSingleton()->releaseMemoryBlock(testPairBitarray);
	return collisionDataIt;
}


void HashGrid::generateCollisions(StackAllocator<Contact2d>* contactList)
{
	ObjectHandle2d* itObject;
	HashNode*       itNode;
	HashNode*       itProxy;

	itObject = lastDynamicObject;

	while (itObject != nullptr) // iterating through dynamic bodies
	{
		HashProxy* hashProxy    = &itObject->hashProxy;
		itProxy                 = hashProxy->firstNode;	

		while (itProxy != nullptr) // iterating through nodes of proxy of current body
		{
			const uint16_t        bucketId    = itProxy->bucketId;
			const ObjectHandle2d* proxyHandle = itProxy->handle;

			__m128i bucketMask_128 = _mm_lddqu_si128((__m128i const*)bucketMasks[bucketId].hashCodes);
			__m128i hashCodes0_128 = _mm_lddqu_si128((__m128i const*)proxyHandle->hashProxy.hashCodes.hashCodes);

			itNode = itProxy->next; // next node in the same bucket
			while (itNode != nullptr) // check in dynamic nodes
			{
				const ObjectHandle2d* nodeHandle = itNode->handle;
				__m128i hashCodes1_128 = _mm_lddqu_si128((__m128i const*)nodeHandle->hashProxy.hashCodes.hashCodes);

				if (HashCodes::testPair(hashCodes0_128, hashCodes1_128, bucketMask_128))
				{
					if (collisionFilter(proxyHandle, nodeHandle)) {
						if (AABB2d::intersect(proxyHandle->volume, nodeHandle->volume))
						{
							//It's better to check if it is the same object outside testPair function
							if (proxyHandle->getId() != nodeHandle->getId())
							{
								collisionPairCache->push(HashCollisionPair(proxyHandle, nodeHandle));
							}
						}
					}
				}

				itNode = itNode->next; // next node in the same bucket
			}

			itNode = hashBucketStatic[bucketId].firsNode;
			while (itNode != nullptr) // check in static nodes
			{
				const ObjectHandle2d* nodeHandle = itNode->handle;
				__m128i hashCodes1_128 = _mm_lddqu_si128((__m128i const*)nodeHandle->hashProxy.hashCodes.hashCodes);

				if (HashCodes::testPair(hashCodes0_128, hashCodes1_128, bucketMask_128))
				{
					if (collisionFilter(proxyHandle, nodeHandle)) {
						if (AABB2d::intersect(proxyHandle->volume, nodeHandle->volume))
						{
							collisionPairCache->push(HashCollisionPair(proxyHandle, nodeHandle));
						}
					}
				}

				itNode = itNode->next;
			}

			itProxy = itProxy->nextProxyNode;
		}

		itObject = itObject->prev;
	}

	int32_t testNum = collisionPairCache->getSize();
	HashCollisionPair* collisionPair = collisionPairCache->getFirst();

	void* memoryBlock = RecycledMemoryManager::getSingleton()->getMemoryBlock(setup::gilgamesh::MAX_OBJECTS  * (uint32_t)(setup::gilgamesh::MAX_CONTACTS_PER_OBJECT) * (uint32_t)(sizeof(Contact2d)));
	Contact2d* contacts = (Contact2d*)(memoryBlock);

	for (int32_t i = 0; i<testNum; ++i)
	{
		uint32_t collisionNum = getCollision(collisionPair[i].handleA, collisionPair[i].handleB, contacts);
		contacts += collisionNum;
	}

	uint32_t allContactsNum = ((int8_t*)(contacts)-(int8_t*)(memoryBlock)) / sizeof(Contact2d);

	contactList->resize(allContactsNum * sizeof(Contact2d));
	std::memcpy(contactList->getFirst(), memoryBlock, allContactsNum * sizeof(Contact2d));
	contactList->setSize(allContactsNum);

	RecycledMemoryManager::getSingleton()->releaseMemoryBlock(memoryBlock);

	collisionPairCache->clear();
}

void HashGrid::printObjectHashBuckets()
{
	ObjectHandle2d* itObject = lastStaticObject;
	std::cout << "Static objects:" << std::endl;
	
	while(itObject!=nullptr)
	{
		std::cout << "Object ID " << itObject->getId() << std::endl;
		itObject->hashProxy.printNodes();
		itObject = itObject->prev;
	}

	std::cout << "Dynamic objects:" << std::endl;

	itObject = lastDynamicObject;
	while(itObject!=nullptr)
	{
		std::cout << "Object ID " << itObject->getId() << std::endl;
		itObject->hashProxy.printNodes();
		itObject = itObject->prev;
	}
}


void HashGrid::clearTestPairArrayMultithreated(int32_t* testPairBitarray)
{
	ThreadManager* threadManager = ThreadManager::getSingleton();
	threadManager->createTaskArray(testPairBitarray, GIL_TEST_PAIR_ARRAY_SIZE, nullptr, 0, Thread::clearTestBitArray, nullptr);
	threadManager->waitUntilFinish();
}

void HashGrid::clearTestPairArray(int* testPairBitarray)
{
	memset(testPairBitarray, 0, GIL_TEST_PAIR_ARRAY_SIZE);
}

jaFloat HashGrid::getCellSize()
{
	return cellSize;
}

int16_t HashGrid::getCellWidth()
{
	return bucketWidth;
}

int16_t HashGrid::getCellHeight()
{
	return bucketHeight;
}

int16_t HashGrid::getBucketNum()
{
	return bucketNum;
}

void HashGrid::updateDynamicObjectsMultiThread()
{
	ThreadManager* threadManager = ThreadManager::getSingleton(); 
	
	uint32_t      hashUpdatesSize   = (uint32_t)(setup::gilgamesh::MAX_OBJECTS * sizeof(HashUpdateMultithread));
	const int32_t maxCellsPerObject = 16;
	uint32_t      hashesSize        = ( threadManager->getThreadsNum() *  setup::gilgamesh::MAX_OBJECTS  * sizeof(int16_t) * maxCellsPerObject);
	
	void* hashUpdatesMem = RecycledMemoryManager::getSingleton()->getMemoryBlock(hashUpdatesSize);
	void* hashesMem      = RecycledMemoryManager::getSingleton()->getMemoryBlock(hashesSize);
	HashUpdateMultithread* hashUpdates = (HashUpdateMultithread*)( (char*)(hashUpdatesMem) + ((unsigned int)(hashUpdatesMem) % setup::gilgamesh::CACHE_LINE_SIZE) );
	
	threadManager->createTaskArray(hashUpdates, 0, hashesMem, hashesSize, Thread::getHashes, this);
	threadManager->waitUntilFinish();

	uint32_t i = 0;
	ObjectHandle2d* itObject = lastDynamicObject;
	while (itObject != nullptr)
	{
		if (hashUpdates[i].hashesNum != 0) {

			HashProxy* proxy = &itObject->hashProxy;

			removeDynamicProxyNodes(proxy);
			int16_t* hashes = hashUpdates[i].hashes;
			for (int32_t z = 0; z < hashUpdates[i].hashesNum; ++z)
			{
				HashNode* node = createDynamicNode(*hashes, itObject);
				proxy->pushBack(node);// It doesn't check if there is node with equal hash
				++hashes;
			}
		}

		++i;
		itObject = itObject->prev;
	}

	RecycledMemoryManager::getSingleton()->releaseMemoryBlock(hashesMem);
	RecycledMemoryManager::getSingleton()->releaseMemoryBlock(hashUpdatesMem);
}

void HashGrid::updateDynamicObjects()
{
	ObjectHandle2d* itObject = lastDynamicObject;

	//It is faster to firs update all bounding rectangles
	while(itObject != nullptr) // iterating through dynamic bodies
	{
		itObject->updateAABB2d();
		itObject = itObject->prev;
	}

	itObject = lastDynamicObject;
	while(itObject != nullptr) // iterating through dynamic bodies
	{
		updateDynamicObject(itObject,false);
		itObject = itObject->prev;
	}
}

void HashGrid::getObjectsAtPoint( StackAllocator<ObjectHandle2d*>* objectList, const jaVector2& point)
{
	int16_t x    = (int16_t)(point.x * conversionFactor); 
	int16_t y    = (int16_t)(point.y * conversionFactor);
	int16_t hash = getCellHash(x,y);

	HashNode* itNode = hashBucketStatic[hash].firsNode;
	while(itNode != nullptr)
	{
		if(AABB2d::intersect(point,itNode->handle->volume))
		{
			if(itNode->handle->getShape()->isPointInShape(itNode->handle->transform, point))
			{
				objectList->push(itNode->handle);
			}
		}
		itNode = itNode->next;
	}

	itNode = hashBucketDynamic[hash].firsNode;
	while(itNode != nullptr)
	{
		if(AABB2d::intersect(point,itNode->handle->volume))
		{
			if(itNode->handle->getShape()->isPointInShape(itNode->handle->transform, point))
			{
				objectList->push(itNode->handle);
			}
		}
		itNode = itNode->next;
	}
}

void HashGrid::getObjectsAtRectangle(StackAllocator<ObjectHandle2d*>* objectList, const AABB2d& aabb)
{
	int16_t hash;
	int16_t x  = (int16_t)(aabb.min.x * conversionFactor); 
	int16_t y  = (int16_t)(aabb.min.y * conversionFactor);
	int16_t x1 = (int16_t)(aabb.max.x * conversionFactor);
	int16_t y1 = (int16_t)(aabb.max.y * conversionFactor);

	HashNode* staticNode;
	HashNode* dynamicNode;

	uint32_t objectListStart = objectList->getSize();

	if((x==x1) && (y==y1)) // only one hashBucket
	{
		hash = getCellHash(x,y);
		staticNode  = hashBucketStatic[hash].firsNode;
		dynamicNode = hashBucketDynamic[hash].firsNode;

		while(staticNode != nullptr)
		{
			if(AABB2d::intersect(staticNode->handle->volume,aabb))
			{
				objectList->push(staticNode->handle);			
			}
				
			staticNode = staticNode->next;
		}

		while(dynamicNode != nullptr)
		{
			if(AABB2d::intersect(dynamicNode->handle->volume,aabb))
			{
				objectList->push(dynamicNode->handle);
			}

			dynamicNode = dynamicNode->next;
		}

		return;
	}
	else // many hashBuckets
	{
		int16_t dx = (x1 - x) + x;
		int16_t dy = (y1 - y) + y;

		bool* testBuckets = (bool*)( RecycledMemoryManager::getSingleton()->getMemoryBlock(bucketNum * sizeof(bool)) );
		memset(testBuckets,0, sizeof(bool) * bucketNum);

		for(int16_t i=x; i<=dx; ++i)
		{
			for(int16_t z=y; z<=dy; ++z)
			{
				hash = getCellHash(i,z);
				testBuckets[hash] = true; // To avoid checking one bucket more than once
			}
		}

		for(int16_t i=0; i<bucketNum; ++i)
		{
			if(testBuckets[i])
			{
				staticNode = hashBucketStatic[i].firsNode;
				dynamicNode = hashBucketDynamic[i].firsNode;

				while(staticNode != nullptr)
				{
					if(AABB2d::intersect(staticNode->handle->volume,aabb))
					{
						if(!staticNode->handle->isFlaged()) // avoid adding two handle of this same object to the list
						{
							objectList->push(staticNode->handle);
							staticNode->handle->setFlag();
						}				
					}

					staticNode = staticNode->next;
				}

				while(dynamicNode != nullptr)
				{
					if(AABB2d::intersect(dynamicNode->handle->volume,aabb))
					{
						if(!dynamicNode->handle->isFlaged()) // avoid adding two handle of this same object to the list
						{
							objectList->push(dynamicNode->handle);
							dynamicNode->handle->setFlag();
						}	
					}

					dynamicNode = dynamicNode->next;
				}
			}
		}	

		ObjectHandle2d** objects   = objectList->getFirst();
		uint32_t         objectNum = objectList->getSize();

		for (uint32_t i = objectListStart; i < objectNum; ++i)
		{
			objects[i]->setUnflag();
		}

		RecycledMemoryManager::getSingleton()->releaseMemoryBlock(testBuckets);
	}	
}

void HashGrid::getObjectsAtShape(StackAllocator<ObjectHandle2d*>* objectList, const jaTransform2& transform, Shape2d* shape)
{
	int16_t        hash;
	ObjectHandle2d shapeHandle(0, shape, transform);
	Contact2d      contacts[16];
	
	int16_t x  = (int16_t)(shapeHandle.volume.min.x * conversionFactor);
	int16_t y  = (int16_t)(shapeHandle.volume.min.y * conversionFactor);
	int16_t x1 = (int16_t)(shapeHandle.volume.max.x * conversionFactor);
	int16_t y1 = (int16_t)(shapeHandle.volume.max.y * conversionFactor);

	HashNode* staticNode;
	HashNode* dynamicNode;

	uint32_t objectListStart = objectList->getSize();

	if ((x == x1) && (y == y1)) // only one hashBucket
	{
		hash        = getCellHash(x, y);
		staticNode  = hashBucketStatic[hash].firsNode;
		dynamicNode = hashBucketDynamic[hash].firsNode;

		while (staticNode != nullptr)
		{
			if (AABB2d::intersect(staticNode->handle->volume, shapeHandle.volume))
			{
				if (0 != getCollision(&shapeHandle, staticNode->handle, contacts)) {
					objectList->push(staticNode->handle);
				}
			}

			staticNode = staticNode->next;
		}

		while (dynamicNode != nullptr)
		{
			if (AABB2d::intersect(dynamicNode->handle->volume, shapeHandle.volume))
			{
				if (0 != getCollision(&shapeHandle, dynamicNode->handle, contacts)) {
					objectList->push(dynamicNode->handle);
				}
			}

			dynamicNode = dynamicNode->next;
		}

		return;
	}
	else // many hashBuckets
	{
		int16_t dx = (x1 - x) + x;
		int16_t dy = (y1 - y) + y;

		bool* testBuckets = (bool*)(RecycledMemoryManager::getSingleton()->getMemoryBlock(sizeof(bool) * bucketNum));
		memset(testBuckets, 0, sizeof(bool)* bucketNum);

		for (int16_t i = x; i <= dx; ++i)
		{
			for (int16_t z = y; z <= dy; ++z)
			{
				hash = getCellHash(i, z);
				testBuckets[hash] = true; // To avoid checking one bucket more than once
			}
		}

		for (int16_t i = 0; i<bucketNum; ++i)
		{
			if (testBuckets[i])
			{
				staticNode = hashBucketStatic[i].firsNode;
				dynamicNode = hashBucketDynamic[i].firsNode;

				while (staticNode != nullptr)
				{
					if (AABB2d::intersect(staticNode->handle->volume, shapeHandle.volume))
					{
						if (!staticNode->handle->isFlaged()) // avoid adding two handle of this same object to the list
						{
							if (0 != getCollision(&shapeHandle, staticNode->handle, contacts)) {
								objectList->push(staticNode->handle);
								staticNode->handle->setFlag();
							}
						}
					}

					staticNode = staticNode->next;
				}

				while (dynamicNode != nullptr)
				{
					if (AABB2d::intersect(dynamicNode->handle->volume, shapeHandle.volume))
					{
						if (!dynamicNode->handle->isFlaged()) // avoid adding two handle of this same object to the list
						{
							if (0 != getCollision(&shapeHandle, dynamicNode->handle, contacts)) {
								objectList->push(dynamicNode->handle);
								dynamicNode->handle->setFlag();
							}
						}
					}

					dynamicNode = dynamicNode->next;
				}
			}
		}

		ObjectHandle2d** objects   = objectList->getFirst();
		uint32_t         objectNum = objectList->getSize();
		for (uint32_t i = objectListStart; i < objectNum; ++i)
		{
			objects[i]->setUnflag();
		}

		RecycledMemoryManager::getSingleton()->releaseMemoryBlock(testBuckets);
	}
}

void HashGrid::rayIntersection( jaVector2& ray0, jaVector2& ray1, StackAllocator<RayContact2d>* contactList)
{
	jaVector2 p0;
	jaVector2 p1;

	if(ray0.x < ray1.x)
	{
		p0.x = ray0.x;
		p0.y = ray0.y;
		p1.x = ray1.x;
		p1.y = ray1.y;
	}
	else
	{
		p0.x = ray1.x;
		p0.y = ray1.y;
		p1.x = ray0.x;
		p1.y = ray0.y;
	}

	int16_t x  = (int16_t)(p0.x * conversionFactor); 
	int16_t y  = (int16_t)(p0.y * conversionFactor);
	int16_t x2 = (int16_t)(p1.x * conversionFactor);
	int16_t addY;

	if(p0.y < p1.y) // if raising
		addY = 1;
	else
		addY = -1;

	jaVector2 ray = p1 - p0;
	int16_t widthCells  = abs((int16_t)(ray.x * conversionFactor)); // width of cells holding ray
	int16_t heightCells = abs((int16_t)(ray.y * conversionFactor)); // height of cells holding ray
	++widthCells;  // to avoid 0
	++heightCells; // to avoid 0

	jaFloat raiseYFactor;

	if(widthCells > heightCells)
	{
		raiseYFactor = (jaFloat)heightCells / (jaFloat)widthCells;
	}
	else
	{
		raiseYFactor = (jaFloat)widthCells / (jaFloat)heightCells;
	}
		
	jaFloat deltaHorizontal = 0;
	int16_t hash;

	bool* testBuckets = (bool*)(RecycledMemoryManager::getSingleton()->getMemoryBlock(bucketNum * sizeof(bool)));
	memset(testBuckets,0,sizeof(bool) * bucketNum);

	if(widthCells > heightCells)
	{
		while(x<=x2)
		{
			hash = getCellHash(x,y);
			testBuckets[hash] = true; // set true for testing this bucket
			deltaHorizontal += raiseYFactor;

			if(deltaHorizontal >= GIL_ONE)
			{
				deltaHorizontal -= GIL_ONE;
				if(deltaHorizontal == GIL_ZERO)
				{
					y+=addY; // depenging on ray is raising or declining id adds or subs
					++x;
					continue;;
				}
				++x;
				continue;
			}
			++x;
		}
	}
	else
	{
		while(x<=x2)
		{
			hash = getCellHash(x,y);
			testBuckets[hash] = true; // set true for testing this bucket
			deltaHorizontal += raiseYFactor;

			if(deltaHorizontal >= GIL_ONE)
			{
				deltaHorizontal -= GIL_ONE;
				if(deltaHorizontal == GIL_ZERO)
				{
					y+=addY; // depenging on ray is raising or declining id adds or subs
					++x;
					continue;;
				}
				++x;
				continue;
			}
			y+=addY;
		}
	}

	for(int16_t i=0; i<bucketNum; ++i)
	{
		if(testBuckets[bucketNum])
		{

		}
	}

	RecycledMemoryManager::getSingleton()->releaseMemoryBlock(testBuckets);
}
