#pragma once

#include "../../jaSetup.hpp"
#include "../Scene2d.hpp"
#include "../Mid/AABB2d.hpp"
#include "../../Math/VectorMath2d.hpp"
#include "../ObjectHandle2d.hpp"
#include "../../Allocators/PoolAllocator.hpp"
#include "../../Allocators/StackAllocator.hpp"

class PhysicsTests;

namespace gil
{

	class HashUpdateMultithread {
	public:
		int16_t* hashes;
		int16_t  hashesNum;
		char     padding[58];
	};

	class HashCollisionPair
	{
	public:
		const ObjectHandle2d* handleA;
		const ObjectHandle2d* handleB;

		HashCollisionPair(const ObjectHandle2d* handleA, const ObjectHandle2d* handleB) : handleA(handleA), handleB(handleB)
		{

		}
	};

	//It is for keeping hashBucket
	class HashCell
	{
	public:
		//First node of hash bucket
		HashNode* firsNode;
		HashCell() : firsNode(nullptr)
		{

		}
		void insertNode();
	};

	class HashGrid : public Scene2d
	{
	private:
		jaFloat cellSize;
		jaFloat conversionFactor;

		int16_t bucketWidth;
		int16_t bucketHeight;
		int16_t bucketNum;

		///////////////////////
		//It is allocated with nodeAllocator before offset block
		HashCell* hashBucket;        // container which allocates all next buckets
		HashCell* hashBucketStatic;  // container of bucketNum size;
		HashCell* hashBucketDynamic; // container of bucketNum size
		///////////////////////
		StackAllocator<HashCollisionPair>* collisionPairCache;
		PoolAllocator<HashNode>*           nodeAllocator;

	public:
		static const uint32_t GIL_MAX_OBJECT_PAIRS     = ja::setup::gilgamesh::MAX_OBJECTS * (ja::setup::gilgamesh::MAX_OBJECTS - 1) / 2;
		static const uint32_t GIL_TEST_PAIR_ARRAY_SIZE = ((GIL_MAX_OBJECT_PAIRS + 31) / 32) * sizeof(uint32_t);
		
	private:
		int32_t*   testPairBitarray;
		uint8_t*   bucketMasksMem;
		HashCodes* bucketMasks;

		void clearTestPairArrayMultithreated(int32_t* testPairBitarray);
		void clearTestPairArray(int32_t* testPairBitarray);
		
		//Function for faster and smarter allocation of new nodes if hash proxy
		//already have some hash then new node is not allocated
		//It checks if hashProxy has node with equal ID to avoid unecessary memory allocation
		inline void pushBackDynamicHash(ObjectHandle2d* handle, HashProxy* hashProxy, int16_t hash)
		{
			if (hashProxy->firstNode == nullptr)
			{
				hashProxy->firstNode = createDynamicNode(hash, handle);
			}
			else
			{
				//It's one directioned list so you have to iterate
				//through all nodes to get to the last which you can
				//use to link
				HashNode* itProxyNode = hashProxy->firstNode;

				while (itProxyNode->nextProxyNode != nullptr)
				{
					if (itProxyNode->bucketId == hash)
						return; // duplicated hash return

					itProxyNode = itProxyNode->nextProxyNode;
				}

				if (itProxyNode->bucketId == hash)
					return; // duplicated hash return

				itProxyNode->nextProxyNode = createDynamicNode(hash, handle);
			}
		}

		inline void removeOldDynamicHashes(HashProxy* hashProxy)
		{		
			HashNode* itProxyNode       = hashProxy->firstNode;
			HashNode* previousProxyNode = nullptr;

			while (itProxyNode != nullptr)
			{
				if (false == hashProxy->hashCodes.isHashCodeAdded(itProxyNode->bucketId))
				{
					//hashCode not added remove

					if (itProxyNode->prev != nullptr)
					{
						itProxyNode->prev->next = itProxyNode->next;
						if (itProxyNode->next != nullptr)
						{
							itProxyNode->next->prev = itProxyNode->prev;
						}
					}
					else
					{
						if (itProxyNode->next != nullptr)
						{
							hashBucketDynamic[itProxyNode->bucketId].firsNode = itProxyNode->next;
							itProxyNode->next->prev = nullptr;
						}
						else
						{
							hashBucketDynamic[itProxyNode->bucketId].firsNode = nullptr;
						}
					}

					if (itProxyNode == hashProxy->firstNode)
					{
						hashProxy->firstNode = itProxyNode->nextProxyNode;
					}

					nodeAllocator->free(itProxyNode);
					
					if (nullptr != previousProxyNode)
						previousProxyNode->nextProxyNode = itProxyNode->nextProxyNode;
				}
				else
				{
					previousProxyNode = itProxyNode;
				}

				itProxyNode = itProxyNode->nextProxyNode;
			}
		}

		inline void removeOldStaticHashes(HashProxy* hashProxy)
		{
			HashNode* itProxyNode       = hashProxy->firstNode;
			HashNode* previousProxyNode = nullptr;

			while (itProxyNode != nullptr)
			{
				if (false == hashProxy->hashCodes.isHashCodeAdded(itProxyNode->bucketId))
				{
					//hashCode not added remove

					if (itProxyNode->prev != nullptr)
					{
						itProxyNode->prev->next = itProxyNode->next;
						if (itProxyNode->next != nullptr)
						{
							itProxyNode->next->prev = itProxyNode->prev;
						}
					}
					else
					{
						if (itProxyNode->next != nullptr)
						{
							hashBucketStatic[itProxyNode->bucketId].firsNode = itProxyNode->next;
							itProxyNode->next->prev = nullptr;
						}
						else
						{
							hashBucketStatic[itProxyNode->bucketId].firsNode = nullptr;
						}
					}

					if (itProxyNode == hashProxy->firstNode)
					{
						hashProxy->firstNode = itProxyNode->nextProxyNode;
					}

					nodeAllocator->free(itProxyNode);

					if (nullptr != previousProxyNode)
						previousProxyNode->nextProxyNode = itProxyNode->nextProxyNode;
				}
				else
				{
					previousProxyNode = itProxyNode;
				}

				itProxyNode = itProxyNode->nextProxyNode;
			}
		}

		//Function for faster and smarter allocation of new nodes if hash proxy
		//already have some hash then new node is not allocated
		//It checks if hashProxy has node with equal ID to avoid unecessary memory allocation
		inline void pushBackStaticHash(ObjectHandle2d* handle, HashProxy* hashProxy, int16_t hash)
		{
			if (hashProxy->firstNode == nullptr)
			{
				hashProxy->firstNode = createStaticNode(hash, handle);
			}
			else
			{
				//It's one directioned list so you have to iterate
				//through all nodes to get to the last which you can
				//use to link
				HashNode* itProxyNode = hashProxy->firstNode;

				while (itProxyNode->nextProxyNode != nullptr)
				{
					if (itProxyNode->bucketId == hash)
						return; // duplicated hash return

					itProxyNode = itProxyNode->nextProxyNode;
				}

				if (itProxyNode->bucketId == hash)
					return; // duplicated hash return

				itProxyNode->nextProxyNode = createStaticNode(hash, handle);
			}
		}

		void insertStaticObject(ObjectHandle2d* handle);
		void insertDynamicObject(ObjectHandle2d* handle);
		void removeStaticProxyNodes(HashProxy* proxy);
		void removeDynamicProxyNodes(HashProxy* proxy);
		inline HashNode* createStaticNode(int16_t hash, ObjectHandle2d* handle);
		inline HashNode* createDynamicNode(int16_t hash, ObjectHandle2d* handle);

		inline bool testPair(uint32_t id0, uint32_t id1)
		{
			uint32_t bitindex;

			if (id0 < id1)
			{
				bitindex = id0 * (2 * ja::setup::gilgamesh::MAX_OBJECTS - id0 - 3) / 2 + id1 - 1;
			}
			else
			{
				bitindex = id1 * (2 * ja::setup::gilgamesh::MAX_OBJECTS - id1 - 3) / 2 + id0 - 1;
			}

			uint32_t mask = 1L << (bitindex & 31);
			if ((testPairBitarray[bitindex >> 5] & mask) == 0) {
				// Bit not set, so pair has not already been processed;
				// process object pair for intersection now

				// Finally mark object pair as processed
				testPairBitarray[bitindex >> 5] |= mask;
				return true;
			}
			return false;
		}
	public:

		HashGrid(int16_t bucketWidth, int16_t bucketHeight, jaFloat cellSize, CacheLineAllocator* allocator);
	
		void rebuild(int16_t bucketWidth, int16_t bucketHeight, jaFloat cellSize);
		ObjectHandle2d* createStaticObject(const Shape2d* shape, const jaTransform2& transform);
		ObjectHandle2d* createDynamicObject(const Shape2d* shape, const jaTransform2& transform);
		uint32_t generateCollisionsMultiThread(CollisionDataPairMultithread* dataPairCache, void* memoryContactCache, int32_t memoryContactCacheSize);
		void generateCollisions(StackAllocator<Contact2d>* contactList);
		void rayIntersection(jaVector2& p0, jaVector2& p1, StackAllocator<RayContact2d>* contactList);
		jaFloat getCellSize();
		int16_t getCellWidth();
		int16_t getCellHeight();
		int16_t getBucketNum();
		void getObjectsAtPoint(StackAllocator<ObjectHandle2d*>* objectList, const jaVector2& point);
		void getObjectsAtRectangle(StackAllocator<ObjectHandle2d*>* objectList, const AABB2d& aabb);
		void getObjectsAtShape(StackAllocator<ObjectHandle2d*>* objectList, const jaTransform2& transform, Shape2d* shape);
		void removeObject(ObjectHandle2d* handle);
		inline int16_t getCellHash(int16_t x, int16_t y);
		void updateDynamicObject(ObjectHandle2d* handle, bool updateVolume = true);
		void updateStaticObject(ObjectHandle2d* handle);
		void updateDynamicObjectsMultiThread();
		void updateDynamicObjects();
		void changeObjectShape(ObjectHandle2d* handle, Shape2d* newShape);
		void setObjectType(ObjectHandle2d* handle, bool isDynamic);

		static inline bool collisionFilter(const ObjectHandle2d* handleA, const ObjectHandle2d* handleB);

		void printObjectHashBuckets();

		~HashGrid();

		friend class ThreadWorkers;
		friend class ::PhysicsTests;
	};

	inline bool HashGrid::collisionFilter(const ObjectHandle2d* handleA, const ObjectHandle2d* handleB) {

		int8_t groupFlag = handleA->groupIndex & handleB->groupIndex;
		if (handleA->groupIndex != handleB->groupIndex)
		{
			if ((handleA->categoryBits & handleB->maskBits) && (handleB->categoryBits & handleA->maskBits))
				return true;
			else
				return false;
		}
		else
		{
			if (handleA->groupIndex < 0)
				return false;
		}

		return true;
	}

	inline int16_t HashGrid::getCellHash(int16_t x, int16_t y)
	{
		return ((abs(y) % this->bucketHeight) * this->bucketHeight) + (abs(x) % this->bucketWidth);
	}

	inline HashNode* HashGrid::createStaticNode(short hash, ObjectHandle2d* handle)
	{
		HashNode* node = new(nodeAllocator->allocate()) HashNode(handle, hash);
		if (hashBucketStatic[hash].firsNode == nullptr)
		{
			hashBucketStatic[hash].firsNode = node;
		}
		else
		{
			hashBucketStatic[hash].firsNode->prev = node;
			node->next = hashBucketStatic[hash].firsNode;
			hashBucketStatic[hash].firsNode = node;
		}

		return node;
	}

	inline HashNode* HashGrid::createDynamicNode(int16_t hash, ObjectHandle2d* handle)
	{
		HashNode* node = new(nodeAllocator->allocate()) HashNode(handle, hash);
		if (hashBucketDynamic[hash].firsNode == nullptr)
		{
			hashBucketDynamic[hash].firsNode = node;
		}
		else
		{
			hashBucketDynamic[hash].firsNode->prev = node;
			node->next = hashBucketDynamic[hash].firsNode;
			hashBucketDynamic[hash].firsNode = node;
		}

		return node;
	}

}