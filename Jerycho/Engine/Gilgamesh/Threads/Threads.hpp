#pragma once

#include "../../Manager/ThreadManager.hpp"
#include "../ObjectHandle2d.hpp"

namespace gil
{

	class ThreadsInitializer {
	public:
		ThreadsInitializer();
	};

	namespace Thread {
		extern void(*clearTestBitArray)(ja::WorkingThread* workingThread);
		extern void(*generateCollisions)(ja::WorkingThread* workingThread);
		extern void(*updateDynamicBodies)(ja::WorkingThread* workingThread);
		extern void(*getCollision)(ja::WorkingThread* workingThread);
		extern void(*getHashes)(ja::WorkingThread* workingThread);
		extern ThreadsInitializer initFunctions; // This initializes thread function pointers at start of program
	};

	class ThreadWorkers {
	private:
	public:
		static void clearTestBitArray(ja::WorkingThread* workingThread);
		static void generateCollisionsCellIterator(ja::WorkingThread* workingThread); // Each thread generates contacts iterating through cells of hash grid
		static void generateCollisionsBodyIterator(ja::WorkingThread* workingThread); // Each thread generates contacts iterating through dynamic bodies of hash grid
		static void getCollision(ja::WorkingThread* workingThread);
		static void getHashes(ja::WorkingThread* workingThread);
	};

}