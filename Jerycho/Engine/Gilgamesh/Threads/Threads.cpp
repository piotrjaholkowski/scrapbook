#include "Threads.hpp"
#include "../Broad/HashGrid.hpp"

using namespace gil;

void (*Thread::clearTestBitArray)(ja::WorkingThread*) = nullptr;
void (*Thread::generateCollisions)(ja::WorkingThread*) = nullptr;
void (*Thread::getCollision)(ja::WorkingThread*) = nullptr;
void (*Thread::getHashes)(ja::WorkingThread*) = nullptr;
ThreadsInitializer Thread::initFunctions;

ThreadsInitializer::ThreadsInitializer()
{
	Thread::clearTestBitArray  = ThreadWorkers::clearTestBitArray;
	Thread::generateCollisions = ThreadWorkers::generateCollisionsBodyIterator;
	Thread::getCollision       = ThreadWorkers::getCollision;
	Thread::getHashes          = ThreadWorkers::getHashes;
}

void ThreadWorkers::clearTestBitArray(ja::WorkingThread* workingThread)
{
	uint32_t threadId = workingThread->threadId;
	uint32_t threadsNum = workingThread->threadsNum;
	char* testPairBitarray = (char*)(workingThread->taskArrayData);
	uint32_t memoryToClear = (workingThread->taskNum / threadsNum) + (workingThread->taskNum % threadsNum);

	memset(testPairBitarray + (threadId * memoryToClear), 0, memoryToClear);
}

void ThreadWorkers::generateCollisionsCellIterator(ja::WorkingThread* workingThread)
{
	uint32_t  threadId = workingThread->threadId;
	uint32_t  threadsNum = workingThread->threadsNum;
	uint32_t  taskNum = workingThread->taskNum;
	HashCell* hashBucketStatic = (HashCell*)(workingThread->taskArrayData);
	HashCell* hashBucketDynamic = hashBucketStatic + taskNum;
	HashGrid* hashGrid  = (HashGrid*)(workingThread->taskData);
	uint32_t  itCounter = 0;
	uint32_t  taskId    = threadId;
	Contact2d* contacts = (Contact2d*)((char*)(workingThread->threadAllocatedMem) + sizeof(uint32_t) );
	
	for (itCounter; taskId < taskNum; ++itCounter)
	{
		taskId = threadId + (threadsNum * itCounter);
		HashCell* itHashBucketDynamic = hashBucketDynamic + taskId;
		HashCell* itHashBucketStatic = hashBucketStatic + taskId;
		HashNode* itDynamicNode = itHashBucketDynamic->firsNode;

		while (itDynamicNode != nullptr) {
			ObjectHandle2d* handleA = itDynamicNode->handle;
			ObjectHandle2d* handleB = nullptr;

			HashNode* itNextDynamicNode = itDynamicNode->next;

			while (itNextDynamicNode != nullptr) {
				handleB = itNextDynamicNode->handle;
				if (hashGrid->collisionFilter(handleA, handleB)) {
					if (AABB2d::intersect(handleA->volume, handleB->volume))
					{
						//threadManager->lockGate0();
						//bool neverTested = hashGrid->testPair(handleA->getId(), handleB->getId());
						//threadManager->unlockGate0();

						if ( hashGrid->testPair( handleA->getId(), handleB->getId() ) ) {
							unsigned int contactsNum = hashGrid->getCollision(handleA, handleB, contacts);
							contacts += contactsNum;
						}
					}
				}
				itNextDynamicNode = itNextDynamicNode->next;
			}

			HashNode* itNextStaticNode = itHashBucketStatic->firsNode;

			while (itNextStaticNode != nullptr) {
				handleB = itNextStaticNode->handle;
				if (hashGrid->collisionFilter(handleA, handleB)) {
					if (AABB2d::intersect(handleA->volume, handleB->volume))
					{
						//threadManager->lockGate0();
						//bool neverTested = hashGrid->testPair(handleA->getId(), handleB->getId());
						//threadManager->unlockGate0();

						if (hashGrid->testPair(handleA->getId(), handleB->getId())) {
							uint32_t contactsNum = hashGrid->getCollision(handleA, handleB, contacts);
							contacts += contactsNum;
						}
					}
				}
				itNextStaticNode = itNextStaticNode->next;
			}

			itDynamicNode = itDynamicNode->next;
		}	
	}

	uint32_t* contactsNum = (uint32_t*)(workingThread->threadAllocatedMem);
	workingThread->memoryAllocatedByThread = (char*)(contacts) - (char*)(workingThread->threadAllocatedMem);
	*contactsNum = (workingThread->memoryAllocatedByThread - sizeof(uint32_t)) / sizeof(Contact2d);
}

void ThreadWorkers::generateCollisionsBodyIterator(ja::WorkingThread* workingThread)
{
	uint32_t threadId = workingThread->threadId;
	uint32_t threadsNum = workingThread->threadsNum;
	uint32_t taskNum = workingThread->taskNum;
	HashCell* hashBucketStatic  = (HashCell*)(workingThread->taskArrayData);
	HashCell* hashBucketDynamic = hashBucketStatic + taskNum;
	HashGrid* hashGrid = (HashGrid*)(workingThread->taskData);
	uint32_t  itCounter = 0;
	//unsigned int taskId = threadId;
	Contact2d* contacts = (Contact2d*)((char*)(workingThread->threadAllocatedMem) + sizeof(uint32_t));

	ObjectHandle2d* itObject;
	HashNode* itNode;
	HashNode* itProxy;
	uint8_t   taskSkip = threadId;

	for (itObject = hashGrid->getLastDynamicObject(); itObject != nullptr; itObject = itObject->prev) {
		if (taskSkip > 0) {
			--taskSkip;
			continue;
		}

		HashProxy* hashProxy = &itObject->hashProxy;
		itProxy = hashProxy->firstNode;
		while (itProxy != nullptr) // iterating through nodes of proxy of current body
		{
			itNode = itProxy->next; // next node in the same bucket
			while (itNode != nullptr) // check in dynamic nodes
			{
				if (hashGrid->collisionFilter(itProxy->handle, itNode->handle)) {
					if (AABB2d::intersect(itProxy->handle->volume, itNode->handle->volume))
					{
						//It's better to check if it is the same object outside testPair function
						if (itProxy->handle->getId() != itNode->handle->getId())
						{
							if (hashGrid->testPair(itProxy->handle->getId(), itNode->handle->getId())) // Test if that pair was already checked
							{
								uint32_t collisionNum = hashGrid->getCollision(itProxy->handle, itNode->handle, contacts);
								contacts += collisionNum;
							}
						}
					}
				}

				itNode = itNode->next; // next node in the same bucket
			}

			itNode = hashBucketStatic[itProxy->bucketId].firsNode;
			while (itNode != nullptr) // check in static nodes
			{
				if (hashGrid->collisionFilter(itProxy->handle, itNode->handle)) {
					if (AABB2d::intersect(itProxy->handle->volume, itNode->handle->volume))
					{
						if (hashGrid->testPair(itProxy->handle->getId(), itNode->handle->getId())) // Test if that pair was already checked
						{
							uint32_t collisionNum = hashGrid->getCollision(itProxy->handle, itNode->handle, contacts);
							contacts += collisionNum;
						}
					}
				}

				itNode = itNode->next;
			}

			itProxy = itProxy->nextProxyNode;
		}

		taskSkip = threadsNum - 1;
	}
	
	uint32_t* contactsNum = (uint32_t*)(workingThread->threadAllocatedMem);
	workingThread->memoryAllocatedByThread = (char*)(contacts)-(char*)(workingThread->threadAllocatedMem);
	*contactsNum = (workingThread->memoryAllocatedByThread - sizeof(uint32_t)) / sizeof(Contact2d);
}

void ThreadWorkers::getHashes(ja::WorkingThread* workingThread)
{
	uint32_t threadId   = workingThread->threadId;
	uint32_t threadsNum = workingThread->threadsNum;
	HashGrid* hashGrid  = (HashGrid*)(workingThread->taskData);
	HashUpdateMultithread* hashUpdates = (HashUpdateMultithread*)( workingThread->taskArrayData );
	int16_t* hashes = (int16_t*)( workingThread->threadAllocatedMem );
	
	int16_t  bucketNum = hashGrid->getBucketNum();
	uint32_t taskId    = threadId;
	jaFloat  conversionFactor = hashGrid->conversionFactor;
	uint8_t  taskSkip = threadId;
	uint32_t updateI = threadId;

	
	for (ObjectHandle2d* itObject = hashGrid->getLastDynamicObject(); itObject != nullptr; itObject = itObject->prev)
	{
		if (taskSkip > 0) {
			--taskSkip;
			continue;
		}
		itObject->updateAABB2d();

		//HashProxy* proxy = (HashProxy*)(itObject->broadProxy);
		HashProxy* proxy = &itObject->hashProxy;
		AABB2d aabb      = itObject->getAABB2d();
	

		int16_t x0  = (int16_t)(aabb.min.x * conversionFactor);
		int16_t y0  = (int16_t)(aabb.min.y * conversionFactor);
		int16_t x1  = (int16_t)(aabb.max.x * conversionFactor);
		int16_t y1  = (int16_t)(aabb.max.y * conversionFactor);

		if (proxy->checkIfCoordinatesEqual(x0, y0, x1, y1))
		{
			hashUpdates[updateI].hashesNum = 0;
		}
		else
		{
			proxy->cellCoordinate[0].x = x0;
			proxy->cellCoordinate[0].y = y0;
			proxy->cellCoordinate[1].x = x1;
			proxy->cellCoordinate[1].y = y1;

			int16_t dx = (x1 - x0) + x0;
			int16_t dy = (y1 - y0) + y0;

			hashUpdates[updateI].hashes = hashes;
			int16_t hashesNum = 0;

			for (int16_t i = x0; i <= dx; ++i)
			{
				for (int16_t z = y0; z <= dy; ++z)
				{
					hashes[hashesNum] = hashGrid->getCellHash(i, z);

					for (int16_t p = 0; p < hashesNum; ++p) { // To avoid inserting the same hash twice
						if (hashes[hashesNum] == hashes[p]) {
							--hashesNum;
							break;
						}
					}
					++hashesNum;
				}
			}

			hashUpdates[updateI].hashesNum = hashesNum;
			hashes                         = hashes + hashesNum;
		}

		/*if ((x == x1) && (y == y1)) // fits in one cell
		{
			hash = hashGrid->getCellHash(x, y);
			if (hash != proxy->cacheHash)
			{
				proxy->cacheHash = hash;
				hashUpdates[updateI].hashesNum = 1;
				*hashes = hash;
				hashUpdates[updateI].hashes = hashes;
				++hashes;
			}
			else {
				hashUpdates[updateI].hashesNum = 0;
			}
		}
		else
		{
			int16_t dx = (x1 - x) + x;
			int16_t dy = (y1 - y) + y;
			proxy->cacheHash = bucketNum + 1;

			hashUpdates[updateI].hashes = hashes;
			int16_t hashesNum = 0;

			for (int16_t i = x; i <= dx; ++i)
			{
				for (int16_t z = y; z <= dy; ++z)
				{
					hashes[hashesNum] = hashGrid->getCellHash(i, z); 

					for (int16_t p = 0; p < hashesNum; ++p) { // To avoid inserting the same hash twice
						if (hashes[hashesNum] == hashes[p]) {
							--hashesNum;
							break;
						}
					}
					++hashesNum;
				}
			}
			hashUpdates[updateI].hashesNum = hashesNum;
			hashes = hashes + hashesNum;
		}*/

		updateI += threadsNum;
		taskSkip = threadsNum - 1;
	}
}

void ThreadWorkers::getCollision(ja::WorkingThread* workingThread)
{
	uint32_t threadId   = workingThread->threadId;
	uint32_t threadsNum = workingThread->threadsNum;
	uint32_t taskNum    = workingThread->taskNum;
	HashGrid* hashGrid  = (HashGrid*)(workingThread->taskData);
	CollisionDataPairMultithread* collisionPairs = (CollisionDataPairMultithread*)(workingThread->taskArrayData);
	Contact2d*                    contacts       = (Contact2d*)(workingThread->threadAllocatedMem);
	
	CollisionDataPairMultithread* collisionPair;
	for (uint32_t i = threadId; i < taskNum; i += threadsNum)
	{
		collisionPair = &collisionPairs[i];
		collisionPair->contacsNum = hashGrid->getCollision( collisionPair->handleA, collisionPair->handleB, contacts);
		collisionPair->contacts = contacts;
		contacts += collisionPair->contacsNum;
	}
}