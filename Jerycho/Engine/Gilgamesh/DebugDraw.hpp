#pragma once

#include "../../Engine/Manager/DisplayManager.hpp"
#include "../../Engine/Manager/ResourceManager.hpp"
#include "Broad/HashGrid.hpp"
#include "Shapes/Box2d.hpp"
#include "Shapes/ConvexPolygon2d.hpp"
#include "Shapes/TriangleMesh2d.hpp"
#include "Shapes/Circle2d.hpp"
#include "Shapes/Particles2d.hpp"
#include "Shapes/MultiShape2d.hpp"
#include "../Gui/Margin.hpp"
#include "../Math/VectorMath2d.hpp"

namespace gil
{

	namespace Debug
	{
		class DebugInit
		{
		public:
			DebugInit();
		};

		extern void(*drawShapeFunc[10])(const Shape2d*, const jaTransform2&);
		extern DebugInit initFunctions;

#pragma warning( disable : 4244)

		inline void drawAABB2d(const AABB2d& aabb)
		{
			const jaVector2& min = aabb.min;
			const jaVector2& max = aabb.max;

			glBegin(GL_LINES);
			glVertex2f(min.x, min.y);
			glVertex2f(max.x, min.y);
			glVertex2f(max.x, min.y);
			glVertex2f(max.x, max.y);
			glVertex2f(max.x, max.y);
			glVertex2f(min.x, max.y);
			glVertex2f(min.x, max.y);
			glVertex2f(min.x, min.y);
			glEnd();
		}

		static void drawBox2d(const Shape2d* shape, const jaTransform2& transform)
		{
			Box2d*    box        = (Box2d*)(shape);
			jaFloat   halfWidth  = box->getHalfWidth();
			jaFloat   halfHeight = box->getHalfHeight();

			glPushMatrix();
			jaMatrix44 modelMatrix;

			jaVectormath2::setTranslateRotScaleMatrix44(transform.position, transform.rotationMatrix, jaVector2(1.0f, 1.0f), modelMatrix);
			glMultMatrixf(modelMatrix.element);

			glBegin(GL_LINES);
			glVertex2f(-halfWidth, -halfHeight);
			glVertex2f(halfWidth, -halfHeight);
			glVertex2f(halfWidth, -halfHeight);
			glVertex2f(halfWidth, halfHeight);
			glVertex2f(halfWidth, halfHeight);
			glVertex2f(-halfWidth, halfHeight);
			glVertex2f(-halfWidth, halfHeight);
			glVertex2f(-halfWidth, -halfHeight);
			glEnd();

			glPopMatrix();
		}

		static void drawConvexPolygon2d(const Shape2d* shape, const jaTransform2& transformMatrix)
		{
			ConvexPolygon2d* convexPolygon = (ConvexPolygon2d*)(shape);
			uint16_t verticesNum = convexPolygon->getVerticesNum();

			jaVector2 position = transformMatrix.getPosition();

			ja::DisplayManager* display = ja::DisplayManager::getSingleton();

			display->pushWorldMatrix();
			display->translateWorldMatrix(position.x, position.y, 0);
			display->rotateWorldMatrix(transformMatrix.rotationMatrix);

			uint16_t  i;
			uint16_t  nextIndex;
			jaVector2 iVector;
			jaVector2 nVector;

			convexPolygon->getVertex(0, iVector);

			for (i = 0; i < verticesNum - 1; ++i)
			{
				nextIndex = i + 1;
				
				convexPolygon->getVertex(nextIndex, nVector);
				display->drawLine(iVector.x, iVector.y, nVector.x, nVector.y);
				iVector = nVector;
			}

			convexPolygon->getVertex(0, nVector);
			display->drawLine(iVector.x, iVector.y, nVector.x, nVector.y);

			display->popWorldMatrix();
		}

		static void drawTriangleMesh2d(const Shape2d* shape, const jaTransform2& transform)
		{
			TriangleMesh2d* triangleMesh = (TriangleMesh2d*)shape;

			glPushMatrix();
			jaMatrix44 modelMatrix;
			jaVectormath2::setTranslateRotScaleMatrix44(transform.position, transform.rotationMatrix, jaVector2(1.0f, 1.0f), modelMatrix);
			glMultMatrixf(modelMatrix.element);

			{
				const jaVector2* pVertices    = triangleMesh->getVertices();
				const uint16_t*  pIndices     = triangleMesh->getTriangleIndices();
				const uint16_t   trianglesNum = triangleMesh->getTrianglesNum();
				const uint16_t   indicesNum   = 3 * trianglesNum;

				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

				glEnableClientState(GL_VERTEX_ARRAY);
				//glEnableClientState(GL_COLOR_ARRAY);

				glVertexPointer(2, GL_FLOAT, sizeof(jaVector2), pVertices);
				//glColorPointer(4, GL_FLOAT, sizeof(ColorVertex), ((uint8_t*)pVertices + sizeof(jaVector2)));

				glDrawElements(GL_TRIANGLES, indicesNum, GL_UNSIGNED_SHORT, pIndices);

				glDisableClientState(GL_VERTEX_ARRAY);
				//glDisableClientState(GL_COLOR_ARRAY);

				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				
				{
					//Draw edges not to test
					const uint8_t* edgesToNotTest = triangleMesh->getEdgesToNotTest();
					glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
					glBegin(GL_LINES);
					for (uint16_t triangleIt = 0; triangleIt < trianglesNum; ++triangleIt)
					{
						uint16_t triangleI = triangleIt * 3;

						const uint16_t index0 = pIndices[triangleI];
						const uint16_t index1 = pIndices[triangleI + 1];
						const uint16_t index2 = pIndices[triangleI + 2];

						if (edgesToNotTest[triangleIt] & 1)
						{
							glVertex2f(pVertices[index0].x, pVertices[index0].y);
							glVertex2f(pVertices[index1].x, pVertices[index1].y);
						}

						if (edgesToNotTest[triangleIt] & 2)
						{
							glVertex2f(pVertices[index1].x, pVertices[index1].y);
							glVertex2f(pVertices[index2].x, pVertices[index2].y);
						}

						if (edgesToNotTest[triangleIt] & 4)
						{
							glVertex2f(pVertices[index2].x, pVertices[index2].y);
							glVertex2f(pVertices[index0].x, pVertices[index0].y);
						}
					}
					glEnd();
				}
			}

			glPopMatrix();
		}

		static void drawCircle2d(const Shape2d* shape, const jaTransform2& transform)
		{
			Circle2d* circle   = (Circle2d*)(shape);
		
			jaVector2 point    = jaVector2(0.0f, circle->radius);
			jaVector2 point2;
			jaMatrix2 rot = jaMatrix2(jaVectormath2::degreesToRadians(360.0f / 32.0f));

			glPushMatrix();
			glTranslatef(transform.position.x, transform.position.y, 0.0f);

			glBegin(GL_LINES);
			for (uint32_t i = 0; i < 32; i++)
			{
				point2 = rot * point;
				glVertex2f(point.x, point.y);
				glVertex2f(point2.x, point2.y);
				point = point2;
			}
			glEnd();

			jaMatrix44 rotMatrix;
			jaVectormath2::setRotMatrix44(transform.rotationMatrix, rotMatrix);
			glMultMatrixf(rotMatrix.element);
			
			glBegin(GL_LINES);
			glVertex2f(0.0f,           0.0f);
			glVertex2f(circle->radius, 0.0f);
			glEnd();

			glPopMatrix();
		}

		static void drawMultiShape2d(const Shape2d* shape, const jaTransform2& transformMatrix)
		{
			MultiShape2d*       multiShape = (MultiShape2d*)(shape);
			jaVector2           position   = transformMatrix.getPosition();
			ja::DisplayManager* display    = ja::DisplayManager::getSingleton();

			display->pushWorldMatrix();
			display->translateWorldMatrix(position.x, position.y, 0);
			display->rotateWorldMatrix(transformMatrix.rotationMatrix);

			ObjectHandle2d* shapeProxyIt = multiShape->shapes;
			jaTransform2    shapeTransform;

			while (shapeProxyIt != nullptr) {
				shapeTransform.rotationMatrix = transformMatrix.rotationMatrix * shapeProxyIt->transform.rotationMatrix;
				shapeTransform.position       = transformMatrix.position + (transformMatrix.rotationMatrix * shapeProxyIt->transform.position);

				drawShapeFunc[shapeProxyIt->shape->getType()](shapeProxyIt->shape, shapeProxyIt->transform);
				shapeProxyIt = shapeProxyIt->next;
			}

			display->popWorldMatrix();
		}

		static void drawPoint2d(const Shape2d* shape, const jaTransform2& transformMatrix)
		{
			jaVector2 position = transformMatrix.getPosition();
		
			glPushMatrix();

			glPointSize(2.0f);

			glBegin(GL_POINTS);
	
			glVertex2f(position.x, position.y);

			glEnd();

			glPopMatrix();
		}

		inline void drawObjectHandle2d(ObjectHandle2d* handle)
		{
			Shape2d* shape = handle->getShape();
			drawShapeFunc[shape->getType()](shape, *handle->getTransformP());

			uint32_t id;
			int32_t x, y;
			char idT[10];
			ja::DisplayManager::getSingleton()->setColorUb(255, 0, 0);

			jaVector2 position = handle->getTransform().getPosition();
			id = handle->getId();
#pragma warning(disable : 4996)
			sprintf(idT, "%u", id);
#pragma warning(default : 4996)

			ja::DisplayManager::getSingleton()->getWindowPosition(position.x, position.y, 0, &x, &y);
			ja::DisplayManager::getSingleton()->RenderText(ja::ResourceManager::getSingleton()->getEternalFont("lucon.ttf",12)->resource, x, y, idT, (uint32_t)ja::Margin::CENTER, 0, 255, 0, 255);
		}

		inline void drawObjectHandle2d(const ObjectHandle2d* handle, const jaTransform2& transform)
		{
			Shape2d* shape = handle->getShape();
			drawShapeFunc[shape->getType()](shape, transform);
		}

		inline void drawHashGrid(jaVector2& position, jaFloat cellSize, jaFloat halfWidth, jaFloat halfHeight)
		{
			jaVector2 begin = jaVector2(position.x - halfWidth, position.y - halfHeight);
			jaVector2 end = jaVector2(position.x + halfWidth, position.y + halfHeight);
			int32_t countX = (2 * halfWidth) / cellSize;
			int32_t countY = (2 * halfHeight) / cellSize;

			countX++;
			countY++;

			jaFloat interval = begin.x;

			for (int32_t i = 0; i < countX; i++) 
			{
				ja::DisplayManager::getSingleton()->drawLine(interval, begin.y, interval, end.y);
				interval += cellSize;
			}

			interval = begin.y;
			for (int32_t i = 0; i < countY; i++) 
			{
				ja::DisplayManager::getSingleton()->drawLine(begin.x, interval, end.x, interval);
				interval += cellSize;
			}

		}

		inline void drawHashGridNumbers(jaVector2& position, HashGrid* hashGrid, jaFloat halfWidth, jaFloat halfHeight)
		{
			uint32_t hash;
			int32_t  x, y;

			char hashT[10];
			jaFloat         cellSize = hashGrid->getCellSize();
			jaFloat conversionFactor = GIL_REAL(1.0) / cellSize;
			jaVector2          begin = jaVector2(position.x - halfWidth, position.y - halfHeight);
			jaVector2            end = jaVector2(position.x + halfWidth, position.y + halfHeight);
			int32_t           countX = (2 * halfWidth) / cellSize;
			int32_t           countY = (2 * halfHeight) / cellSize;
			ja::Font*     fontRender = ja::ResourceManager::getSingleton()->getEternalFont("lucon.ttf",12)->resource;
			float scale = ja::DisplayManager::getHeightMultiplier();

			countX++;
			countY++;

			// draw hash numbers
			jaFloat intervalX = begin.x;
			jaFloat intervalY = begin.y;
			for (int32_t i = 0; i < countX; i++)
			{
				for (int32_t j = 0; j < countY; j++)
				{
					x = (int32_t)(intervalX * conversionFactor); 
					y = (int32_t)(intervalY * conversionFactor);
					hash = hashGrid->getCellHash(x, y);

#pragma warning(disable : 4996)
					sprintf(hashT, "%u", hash);
#pragma warning(default : 4996)
					ja::DisplayManager::RenderTextWorld(fontRender, intervalX, intervalY - (scale * 32), scale, hashT, (uint32_t)ja::Margin::LEFT, 15, 70, 0, 255);

					intervalY += cellSize;
				}
				intervalX += cellSize;
				intervalY = begin.y;
			}
		}

		inline void drawHashGrid(ja::CameraInterface* camera, HashGrid* hashGrid, bool drawCellNumbers)
		{
			jaVector2 position = camera->position;
			jaVector2 scale = camera->scale;
			ja::DisplayManager* display = ja::DisplayManager::getSingleton();
			float halfWidth, halfHeight;
			display->getWorldResolution(&halfWidth, &halfHeight);
	
			halfWidth = halfWidth / scale.x;
			halfHeight = halfHeight / scale.y;
			jaFloat  cellSize  = hashGrid->getCellSize();
			uint32_t bucketNum = hashGrid->getBucketNum();

			float   invCellSize = 1.0f / cellSize;
			int32_t countX = (halfWidth)  * invCellSize;
			int32_t countY = (halfHeight) * invCellSize;
			halfWidth *= 0.5f;
			halfHeight *= 0.5f;

			jaVector2 begin = jaVector2(position.x - halfWidth, position.y - halfHeight);
			jaVector2 end = jaVector2(position.x + halfWidth, position.y + halfHeight);

			begin.x = ceil(begin.x * invCellSize) * cellSize;
			begin.y = ceil(begin.y * invCellSize) * cellSize;

			begin.x -= cellSize;
			begin.y -= cellSize;

			countX += 2;
			countY += 2;

			float interval = begin.x;

			for (int32_t i = 0; i < countX; i++) // pionowe
			{
				ja::DisplayManager::drawLine(interval, begin.y, interval, end.y);
				interval += cellSize;
			}

			interval = begin.y;
			for (int32_t i = 0; i < countY; i++) // poziome
			{
				ja::DisplayManager::drawLine(begin.x, interval, end.x, interval);
				interval += cellSize;
			}

			if (drawCellNumbers)
				drawHashGridNumbers(position, hashGrid, halfWidth, halfHeight);
		}

#pragma warning( default : 4244)
	}

}



