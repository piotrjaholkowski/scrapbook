#pragma once

#include "Narrow/Narrow2d.hpp"
#include "Narrow/Sat2d.hpp"
#include "Narrow/Mpr2d.hpp"
#include "Narrow/Points2d.hpp"
#include "Shapes/Shape2dTypes.hpp"

namespace gil
{
	typedef uint32_t(*CollisionFuncPtr)(const Shape2d*, const jaTransform2&, const Shape2d*, const jaTransform2&, Contact2d*, uint16_t&, const Narrow2dConfiguration&);
	class Narrow2dConfiguration
	{
	public:
		uint8_t collisionShapesNum;
		uint32_t(*fnCollision[ja::setup::gilgamesh::MAX_SHAPES][ja::setup::gilgamesh::MAX_SHAPES])(const Shape2d*, const jaTransform2&, const Shape2d*, const jaTransform2&, Contact2d*, uint16_t&, const Narrow2dConfiguration&);

	public:

		Narrow2dConfiguration()
		{

		}

		void addIntersection(uint8_t typeA, uint8_t typeB, CollisionFuncPtr fnPointer)
		{
			fnCollision[typeA][typeB] = fnPointer;
			fnCollision[typeB][typeA] = fnPointer;
		}

		void addIntersection(uint8_t typeA, uint8_t typeB, CollisionFuncPtr fnPointerA, CollisionFuncPtr fnPointerB)
		{
			fnCollision[typeA][typeB] = fnPointerA;
			fnCollision[typeB][typeA] = fnPointerB;
		}

		inline uint32_t getCollision(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* mtd, uint16_t& hashSeed, const Narrow2dConfiguration& conf) const
		{
			return fnCollision[a->getType()][b->getType()](a, transformA, b, transformB, mtd, hashSeed, conf);
		}
	};

	class gilDefault2dConfiguration : public Narrow2dConfiguration
	{
	private:

	public:
		gilDefault2dConfiguration() : Narrow2dConfiguration()
		{

			//addIntersection(gilBox2dShape,gilBox2dShape,gilSat2d::penetrationBoxBox);
			addIntersection((uint8_t)ShapeType::Box, (uint8_t)ShapeType::Box          , Sat2d::penetrationConvexPolygonConvexPolygon);
			//addIntersection((uint8_t)ShapeType::Box, (uint8_t)ShapeType::Circle       , Sat2d::penetrationBoxCircle);
			addIntersection((uint8_t)ShapeType::Box, (uint8_t)ShapeType::Circle       ,Sat2d::penetrationConvexPolygonCircle);
			//addIntersection((uint8_t)ShapeType::Box, (uint8_t)ShapeType::Particles    , Points2d::intersectionParticlesShape);
			addIntersection((uint8_t)ShapeType::Box, (uint8_t)ShapeType::ConvexPolygon, Sat2d::penetrationConvexPolygonConvexPolygon);
			addIntersection((uint8_t)ShapeType::Box, (uint8_t)ShapeType::TriangleMesh,  Sat2d::penetrationTriangleMeshConvexPolygon);
			addIntersection((uint8_t)ShapeType::Box, (uint8_t)ShapeType::Point,         Sat2d::penetrationConvexPolygonPoint);

			//addIntersection(gilCircle2dShape   , gilConvexPolygon2dShape, Sat2d::penetrationPolygonCircle);
			addIntersection((uint8_t)ShapeType::Circle, (uint8_t)ShapeType::Circle   ,        Sat2d::penetrationCircleCircle);
			addIntersection((uint8_t)ShapeType::Circle, (uint8_t)ShapeType::ConvexPolygon, Sat2d::penetrationConvexPolygonCircle);
			//addIntersection((uint8_t)ShapeType::Circle, (uint8_t)ShapeType::Particles,     Points2d::intersectionParticlesShape);
			addIntersection((uint8_t)ShapeType::Circle, (uint8_t)ShapeType::TriangleMesh, Sat2d::penetraionTriangleMeshCircle);
			addIntersection((uint8_t)ShapeType::Circle, (uint8_t)ShapeType::Point,        Sat2d::penetrationCirclePoint);
			//addIntersection((uint8_t)ShapeType::Particles, (uint8_t)ShapeType::Particles,     Points2d::intersectionParticlesParticles);

			//addIntersection((uint8_t)ShapeType::ConvexSlope, (uint8_t)ShapeType::ConvexSlope  , Mpr2d::penetrationShapeShape);
			//addIntersection((uint8_t)ShapeType::ConvexSlope, (uint8_t)ShapeType::Box          , Mpr2d::penetrationShapeShape);
			//addIntersection((uint8_t)ShapeType::ConvexSlope, (uint8_t)ShapeType::Circle       , Mpr2d::penetrationShapeShape);
			//addIntersection((uint8_t)ShapeType::ConvexSlope, (uint8_t)ShapeType::Capsule      , Mpr2d::penetrationShapeShape);
			//addIntersection((uint8_t)ShapeType::ConvexSlope, (uint8_t)ShapeType::ConvexPolygon, Mpr2d::penetrationShapeShape);
			//addIntersection((uint8_t)ShapeType::ConvexSlope, (uint8_t)ShapeType::Particles    , Points2d::intersectionParticlesShape);

			//addIntersection((uint8_t)ShapeType::Capsule, (uint8_t)ShapeType::Capsule      , Mpr2d::penetrationShapeShape);
			//addIntersection((uint8_t)ShapeType::Capsule, (uint8_t)ShapeType::Box          , Mpr2d::penetrationShapeShape);
			//addIntersection((uint8_t)ShapeType::Capsule, (uint8_t)ShapeType::Circle       , Mpr2d::penetrationShapeShape);
			//addIntersection((uint8_t)ShapeType::Capsule, (uint8_t)ShapeType::ConvexSlope  , Mpr2d::penetrationShapeShape);
			//addIntersection((uint8_t)ShapeType::Capsule, (uint8_t)ShapeType::ConvexPolygon, Mpr2d::penetrationShapeShape);
			//addIntersection((uint8_t)ShapeType::Capsule, (uint8_t)ShapeType::Particles    , Points2d::intersectionParticlesShape);

			addIntersection((uint8_t)ShapeType::Box          , (uint8_t)ShapeType::MultiShape,    Sat2d::penetrationMultiShapeShape);
			addIntersection((uint8_t)ShapeType::Circle       , (uint8_t)ShapeType::MultiShape,    Sat2d::penetrationMultiShapeShape);
			addIntersection((uint8_t)ShapeType::ConvexPolygon, (uint8_t)ShapeType::MultiShape,    Sat2d::penetrationMultiShapeShape);
			//addIntersection((uint8_t)ShapeType::Path         , (uint8_t)ShapeType::MultiShape,    Sat2d::penetrationMultiShapeShape);
			addIntersection((uint8_t)ShapeType::MultiShape   , (uint8_t)ShapeType::MultiShape,    Sat2d::penetrationMultiShapeMultiShape);

			//addIntersection((uint8_t)ShapeType::Path, (uint8_t)ShapeType::Box   , Sat2d::penetrationPathShape);
			//addIntersection((uint8_t)ShapeType::Path, (uint8_t)ShapeType::Circle, Sat2d::penetrationPathShape);

			addIntersection((uint8_t)ShapeType::TriangleMesh, (uint8_t)ShapeType::TriangleMesh, Sat2d::penetrationTriangleMeshTriangleMesh);
			addIntersection((uint8_t)ShapeType::TriangleMesh, (uint8_t)ShapeType::ConvexPolygon, Sat2d::penetrationTriangleMeshConvexPolygon);

			addIntersection((uint8_t)ShapeType::ConvexPolygon, (uint8_t)ShapeType::ConvexPolygon, Sat2d::penetrationConvexPolygonConvexPolygon);
			addIntersection((uint8_t)ShapeType::ConvexPolygon, (uint8_t)ShapeType::Point,         Sat2d::penetrationConvexPolygonPoint);

		}

	};

}