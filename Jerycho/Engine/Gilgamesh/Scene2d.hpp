#pragma once

#include "../Math/Math.hpp"
#include "Narrow2dConfiguration.hpp"
#include "../Allocators/CacheLineAllocator.hpp"
#include "../Allocators/StackAllocator.hpp"
#include "Shapes/Shape2d.hpp"
#include "ObjectHandle2d.hpp"
#include <list>

namespace gil
{

	class Scene2d
	{
	protected:
		Narrow2dConfiguration conf;
		ObjectHandle2d* lastStaticObject;
		ObjectHandle2d* lastDynamicObject;
		uint16_t        idGenerator;
		std::vector<uint16_t> freeId;
		CacheLineAllocator* commonAllocator;
		void deleteObject(ObjectHandle2d* handle);

	public:
		Scene2d(CacheLineAllocator* commonAllocator);
		ObjectHandle2d* createObject(const Shape2d* shape, const jaTransform2& transform);
		void changeObjectShape(ObjectHandle2d* handle, Shape2d* newShape);
		inline uint32_t getCollision(const ObjectHandle2d* handleA, const ObjectHandle2d* handleB, Contact2d* contact);
		void setNarrowConfiguration(Narrow2dConfiguration configuration);
		void clearIdGenerator();
		inline uint16_t getHighestGeneratedId();
		virtual void removeObject(ObjectHandle2d* handle) = 0;
		virtual void updateStaticObject(ObjectHandle2d* handle) = 0;
		virtual void updateDynamicObject(ObjectHandle2d* handle, bool updateVolume) = 0;
		virtual void getObjectsAtRectangle(StackAllocator<ObjectHandle2d*>* objectList, const AABB2d& rectangle) = 0;
		virtual void getObjectsAtPoint(StackAllocator<ObjectHandle2d*>* objectList, const jaVector2& point) = 0;
		virtual void getObjectsAtShape(StackAllocator<ObjectHandle2d*>* objectList, const jaTransform2& transform, Shape2d* shape) = 0;
		virtual ObjectHandle2d* createDynamicObject(const Shape2d* shape, const jaTransform2& transform) = 0;
		virtual ObjectHandle2d* createStaticObject(const Shape2d* shape, const jaTransform2& transform) = 0;
		virtual void setObjectType(ObjectHandle2d* handle, bool isDynamic) = 0;
		virtual uint32_t generateCollisionsMultiThread(CollisionDataPairMultithread* dataPairCache, void* memoryContactCache, int32_t memoryContactCacheSize) = 0;
		virtual void generateCollisions(StackAllocator<Contact2d>* contactList) = 0;
		virtual void rayIntersection(jaVector2& ray0, jaVector2& ray1, StackAllocator<RayContact2d>* contactList) = 0;
		virtual void updateDynamicObjectsMultiThread() = 0;
		virtual void updateDynamicObjects() = 0;
		virtual ~Scene2d() = 0;
		inline ObjectHandle2d* getLastStaticObject();
		inline ObjectHandle2d* getLastDynamicObject();
	};

	inline Scene2d::~Scene2d()
	{

	}

	inline uint16_t Scene2d::getHighestGeneratedId()
	{
		return idGenerator;
	}

	inline ObjectHandle2d* Scene2d::getLastStaticObject()
	{
		return lastStaticObject;
	}

	inline ObjectHandle2d* Scene2d::getLastDynamicObject()
	{
		return lastDynamicObject;
	}

	inline uint32_t Scene2d::getCollision(const ObjectHandle2d* handleA, const ObjectHandle2d* handleB, Contact2d* contact)
	{
		uint16_t hashSeed = 0;

		contact->rA[0].setZero();
		contact->rA[1].setZero();
		contact->rB[0].setZero();
		contact->rB[1].setZero();

		if (handleA->id < handleB->id)
		{
			contact->objectA = (ObjectHandle2d*)handleA;
			contact->objectB = (ObjectHandle2d*)handleB;
			return conf.getCollision(handleA->shape, handleA->transform, handleB->shape, handleB->transform, contact, hashSeed, conf);
		}
		else
		{
			contact->objectA = (ObjectHandle2d*)handleB;
			contact->objectB = (ObjectHandle2d*)handleA;
			return conf.getCollision(handleB->shape, handleB->transform, handleA->shape, handleA->transform, contact, hashSeed, conf);
		}	
	}

}
