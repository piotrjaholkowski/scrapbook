#include "Points2d.hpp"
#include "../Shapes/Shape2d.hpp"
#include "../Shapes/Particles2d.hpp"

using namespace gil;
uint16_t particlesFilter[ja::setup::gilgamesh::MAX_PARTICLES];

uint32_t Points2d::intersectionParticlesShape(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	Particles2d*    particlesA;
	Shape2d*        shapeB;
	ObjectHandle2d* particlesHandle;
	ObjectHandle2d* shapeHandle;
	jaTransform2*   particlesTransform;
	jaTransform2*   shapeTransform;

	if (a->getType() != (uint8_t)ShapeType::Particles) // Swap A=Particles B=Shape
	{
		particlesA = (Particles2d*)a;
		shapeB     = (Shape2d*)b;
		particlesHandle = contact->objectA;
		shapeHandle     = contact->objectB;
		particlesTransform = (jaTransform2*)&transformA;
		shapeTransform     = (jaTransform2*)&transformB;
	}
	else {
		particlesA = (Particles2d*)b;
		shapeB     = (Shape2d*)a;
		particlesHandle = contact->objectB;
		shapeHandle     = contact->objectA;
		particlesTransform = (jaTransform2*)&transformB;
		shapeTransform     = (jaTransform2*)&transformA;
	}

	if(particlesA->collide == false)
		return 0;

	if(particlesA->emitterId == shapeHandle->id)
		return 0;
	
	AABB2d aabbA = particlesHandle->getAABB2d();
	AABB2d aabbB = shapeHandle->getAABB2d();
	AABB2d unionAB;

	AABB2d::getUnion(aabbA,aabbB,unionAB);
	if(unionAB.min.x > unionAB.max.x)
		return 0;
	if(unionAB.min.y > unionAB.max.y)
		return 0;

	memset(particlesFilter, 0, sizeof(particlesFilter));

	/*uint16_t* sortX = particlesA->particlesSortX;
	uint16_t* sortY = particlesA->particlesSortY;
	Particle2d* particles = particlesA->particles;

	if(particlesTransform->position.x > unionAB.min.x)
	{
		if (particlesTransform->position.x > unionAB.max.x)
		{
			//check sortXLeft only
			uint16_t begin;
			uint16_t end;
			for(begin=0; begin<particlesA->sortXLeftNum; ++begin)
			{
				if(particles[sortX[begin]].position.x >= unionAB.min.x)
					break;
			}

			for(end = begin; end<particlesA->sortXLeftNum; ++end)
			{
				if(particles[sortX[end]].position.x > unionAB.max.x)
					break;
				particlesFilter[sortX[end]] += 1;
			}
		}
		else
		{
			//check sortXLeft and sortXRight
			uint16_t begin;
			uint16_t end;
			
			//sortXLeft
			for(begin=0; begin<particlesA->sortXLeftNum; ++begin)
			{
				if(particles[sortX[begin]].position.x >= unionAB.min.x)
					break;
			}

			for(end = begin; end<particlesA->sortXLeftNum; ++end)
			{
				particlesFilter[sortX[end]] += 1;
			}
			//sortXRight
			for (end = ja::setup::gilgamesh::MAX_PARTICLES - particlesA->sortXRightNum; end<ja::setup::gilgamesh::MAX_PARTICLES; ++end)
			{
				if(particles[sortX[end]].position.x > unionAB.max.x)
					break;
				particlesFilter[sortX[end]] += 1;
			}
		}
	}
	else 
	{
		//check sortXRight only
		uint16_t begin;
		uint16_t end;
		for (begin = ja::setup::gilgamesh::MAX_PARTICLES - particlesA->sortXRightNum; begin<ja::setup::gilgamesh::MAX_PARTICLES; ++begin)
		{
			if(particles[sortX[begin]].position.x >= unionAB.min.x)
				break;
		}

		for (end = begin; end<ja::setup::gilgamesh::MAX_PARTICLES; ++end)
		{
			if(particles[sortX[end]].position.x > unionAB.max.x)
				break;
			particlesFilter[sortX[end]] += 1;
		}
	}

	if (particlesTransform->position.y > unionAB.min.y)
	{
		if (particlesTransform->position.y > unionAB.max.y)
		{
			//check sortYDown only
			uint16_t begin;
			uint16_t end;
			for(begin=0; begin<particlesA->sortYDownNum; ++begin)
			{
				if(particles[sortY[begin]].position.y >= unionAB.min.y)
					break;
			}

			for(end = begin; end<particlesA->sortYDownNum; ++end)
			{
				if(particles[sortY[end]].position.y > unionAB.max.y)
					break;
				particlesFilter[sortY[end]] += 1;
			}
		}
		else
		{
			//check sortYDown and sortYTop
			uint16_t begin;
			uint16_t end;

			//sortYDown
			for(begin=0; begin<particlesA->sortYDownNum; ++begin)
			{
				if(particles[sortY[begin]].position.y >= unionAB.min.y)
					break;
			}

			for(end = begin; end<particlesA->sortYDownNum; ++end)
			{
				particlesFilter[sortY[end]] += 1;
			}
			//sortYTop
			for (end = ja::setup::gilgamesh::MAX_PARTICLES - particlesA->sortYTopNum; end<ja::setup::gilgamesh::MAX_PARTICLES; ++end)
			{
				if(particles[sortY[end]].position.y > unionAB.max.y)
					break;
				particlesFilter[sortY[end]] += 1;
			}
		}
	}
	else 
	{
		//check sortYTop only
		uint16_t begin;
		uint16_t end;
		for (begin = ja::setup::gilgamesh::MAX_PARTICLES - particlesA->sortYTopNum; begin<ja::setup::gilgamesh::MAX_PARTICLES; ++begin)
		{
			if(particles[sortY[begin]].position.y >= unionAB.min.y)
				break;
		}

		for (end = begin; end<ja::setup::gilgamesh::MAX_PARTICLES; ++end)
		{
			if(particles[sortY[end]].position.y > unionAB.max.y)
				break;
			particlesFilter[sortY[end]] += 1;
		}
	}

	for(uint16_t i=0; i<particlesA->particleNum; i++)
	{
		if(particlesFilter[i] == 2)
		{
			//test particle
			if(shapeB->isPointInShape(*shapeTransform,particles[i].position))
			{
				// this is place for service response
				particles[i].life = -1;
			}
		}
	}*/


	return 0;
}

/*bool gilPoints2d::intersectionShapeParticles(gilObjectHandle2d& B, gilObjectHandle2d& A, gilContact2d* contact)
{
	gilParticles2d* particlesA = (gilParticles2d*)A.getShape();
	gilShape2d* shapeB = (gilShape2d*)B.getShape();

	if(particlesA->collide == false)
		return false;

	if(particlesA->emitterId == B.id)
		return false;

	gilAABB2d aabbA = A.getAABB2d();
	gilAABB2d aabbB = B.getAABB2d();
	gilAABB2d unionAB;

	gilAABB2d::getUnion(aabbA,aabbB,unionAB);
	if(unionAB.min.x > unionAB.max.x)
		return false;
	if(unionAB.min.y > unionAB.max.y)
		return false;

	//clear particles filter
	unsigned char size = particlesA->particleNum;
	for(unsigned char i=0; i<size; i++)
		particlesFilter[i] = 0;

	unsigned char* sortX = particlesA->particlesSortX;
	unsigned char* sortY = particlesA->particlesSortY;
	gilParticle2d* particles = particlesA->particles;

	if(A.transform.position.x > unionAB.min.x)
	{
		if(A.transform.position.x > unionAB.max.x)
		{
			//check sortXLeft only
			unsigned char begin;
			unsigned char end;
			for(begin=0; begin<particlesA->sortXLeftNum; ++begin)
			{
				if(particles[sortX[begin]].position.x >= unionAB.min.x)
					break;
			}

			for(end = begin; end<particlesA->sortXLeftNum; ++end)
			{
				if(particles[sortX[end]].position.x > unionAB.max.x)
					break;
				particlesFilter[sortX[end]] += 1;
			}
		}
		else
		{
			//check sortXLeft and sortXRight
			unsigned char begin;
			unsigned char end;

			//sortXLeft
			for(begin=0; begin<particlesA->sortXLeftNum; ++begin)
			{
				if(particles[sortX[begin]].position.x >= unionAB.min.x)
					break;
			}

			for(end = begin; end<particlesA->sortXLeftNum; ++end)
			{
				particlesFilter[sortX[end]] += 1;
			}
			//sortXRight
			for(end = GILMAXPARTICLES - particlesA->sortXRightNum; end<GILMAXPARTICLES; ++end)
			{
				if(particles[sortX[end]].position.x > unionAB.max.x)
					break;
				particlesFilter[sortX[end]] += 1;
			}
		}
	}
	else 
	{
		//check sortXRight only
		unsigned char begin;
		unsigned char end;
		for(begin=GILMAXPARTICLES - particlesA->sortXRightNum; begin<GILMAXPARTICLES; ++begin)
		{
			if(particles[sortX[begin]].position.x >= unionAB.min.x)
				break;
		}

		for(end = begin; end<GILMAXPARTICLES; ++end)
		{
			if(particles[sortX[end]].position.x > unionAB.max.x)
				break;
			particlesFilter[sortX[end]] += 1;
		}
	}

	if(A.transform.position.y > unionAB.min.y)
	{
		if(A.transform.position.y > unionAB.max.y)
		{
			//check sortYDown only
			unsigned char begin;
			unsigned char end;
			for(begin=0; begin<particlesA->sortYDownNum; ++begin)
			{
				if(particles[sortY[begin]].position.y >= unionAB.min.y)
					break;
			}

			for(end = begin; end<particlesA->sortYDownNum; ++end)
			{
				if(particles[sortY[end]].position.y > unionAB.max.y)
					break;
				particlesFilter[sortY[end]] += 1;
			}
		}
		else
		{
			//check sortYDown and sortYTop
			unsigned char begin;
			unsigned char end;

			//sortYDown
			for(begin=0; begin<particlesA->sortYDownNum; ++begin)
			{
				if(particles[sortY[begin]].position.y >= unionAB.min.y)
					break;
			}

			for(end = begin; end<particlesA->sortYDownNum; ++end)
			{
				particlesFilter[sortY[end]] += 1;
			}
			//sortYTop
			for(end = GILMAXPARTICLES - particlesA->sortYTopNum; end<GILMAXPARTICLES; ++end)
			{
				if(particles[sortY[end]].position.y > unionAB.max.y)
					break;
				particlesFilter[sortY[end]] += 1;
			}
		}
	}
	else 
	{
		//check sortYTop only
		unsigned char begin;
		unsigned char end;
		for(begin=GILMAXPARTICLES - particlesA->sortYTopNum; begin<GILMAXPARTICLES; ++begin)
		{
			if(particles[sortY[begin]].position.y >= unionAB.min.y)
				break;
		}

		for(end = begin; end<GILMAXPARTICLES; ++end)
		{
			if(particles[sortY[end]].position.y > unionAB.max.y)
				break;
			particlesFilter[sortY[end]] += 1;
		}
	}

	for(unsigned char i=0; i<particlesA->particleNum; i++)
	{
		if(particlesFilter[i] == 2)
		{
			//test particle
			if(shapeB->isPointInShape(B.transform,particles[i].position))
			{
				// this is place for service response
				particles[i].life = -1;
			}
		}
	}

	return false;
}*/

uint32_t Points2d::intersectionParticlesParticles(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	return 0;
}