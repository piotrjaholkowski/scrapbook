#include "Sat2d.hpp"

#include "../../jaSetup.hpp"
#include "../Shapes/Box2d.hpp"
#include "../Shapes/TriangleMesh2d.hpp"
#include "../Shapes/Circle2d.hpp"
#include "../Shapes/MultiShape2d.hpp"
//#include "../Shapes/Path2d.hpp"
#include "../Shapes/Point2d.hpp"

#include "../Narrow2dConfiguration.hpp"

using namespace gil;

//            normal
//              /\
//              | 
//  clip normal |
//   <------------------
//              |      |
//              |      |
//
void Sat2d::clipPoint(jaVector2& clipPoint, jaVector2& clipNormal, jaVector2* point)
{
	jaVector2 distClipPoint(point->x - clipPoint.x, point->y - clipPoint.y);

	jaFloat projection = jaVectormath2::projection(distClipPoint,clipNormal);
	if(projection > 0) // point is outside clipping plane
	{
		point->x = point->x - (projection * clipNormal.x);
		point->y = point->y - (projection * clipNormal.y);
	}
}

bool Sat2d::intersectionSegment(const jaVector2& a, const jaVector2& b, const jaVector2& c, const jaVector2& d, jaVector2& intersectionPoint)
{
	jaFloat denominator = (a.x-b.x)*(c.y-d.y) - (a.y-b.y)*(c.x-d.x);
	if (denominator == GIL_ZERO)
		return false;
	
	jaFloat temp0 = (a.x*b.y-a.y*b.x);
	jaFloat temp1 = (c.x*d.y-c.y*d.x);

	intersectionPoint.x = ((c.x-d.x)*temp0-(a.x-b.x)*temp1)/denominator;

	intersectionPoint.y = ((c.y-d.y)*temp0-(a.y-b.y)*temp1)/denominator;

	if (intersectionPoint.x < jaVectormath2::min(a.x,b.x))
		return false;
	if (intersectionPoint.x > jaVectormath2::max(a.x, b.x))
		return false;
	if (intersectionPoint.x < jaVectormath2::min(c.x, d.x))
		return false;
	if (intersectionPoint.x > jaVectormath2::max(c.x, d.x))
		return false;
	return true;
}

void Sat2d::getSupportTriangle(const Shape2d* shape, const jaVector2& supportDir, const jaVector2& searchPoint, jaVector2* supportTriangle, uint16_t& supportPointIndex)
{
	uint16_t firstSupportPointIndex = shape->findSupportPointIndex(supportDir);
	shape->getVertex(firstSupportPointIndex, supportTriangle[0]);

	jaVector2 newSearchDir            = jaVectormath2::normalize(searchPoint - supportTriangle[0]);
	uint16_t  secondSupportPointIndex = shape->findSupportPointIndex(newSearchDir);

	if (firstSupportPointIndex == secondSupportPointIndex)
	{
		supportPointIndex = firstSupportPointIndex;
		shape->getSupportTriangle(supportPointIndex, supportTriangle);
		return;
	}

	shape->getVertex(secondSupportPointIndex, supportTriangle[1]);

	newSearchDir = jaVectormath2::normalize(jaVectormath2::perpendicular(supportTriangle[0] - supportTriangle[1]));

	float normalProjection = jaVectormath2::projection(newSearchDir, searchPoint);
	if (normalProjection < 0.0f)
		newSearchDir = -newSearchDir;

	supportPointIndex = shape->findSupportPointIndex(newSearchDir);

	shape->getSupportTriangle(supportPointIndex, supportTriangle);
}

void Sat2d::getSupportTriangle(const jaVector2* triangleVertices, const jaVector2& supportDir, const jaVector2& searchPoint, jaVector2* supportTriangle, uint8_t& supportPointIndex)
{
	uint8_t firstSupportPointIndex = TriangleMesh2d::findSupportPointIndex(triangleVertices, supportDir);

	jaVector2 newSearchDir = jaVectormath2::normalize(searchPoint - triangleVertices[firstSupportPointIndex]);
	
	uint8_t secondSupportPointIndex = TriangleMesh2d::findSupportPointIndex(triangleVertices ,newSearchDir);

	if (firstSupportPointIndex == secondSupportPointIndex)
	{
		supportPointIndex = firstSupportPointIndex;
		TriangleMesh2d::getSupportTriangle(triangleVertices, supportPointIndex, supportTriangle);
		return;
	}

	newSearchDir = jaVectormath2::normalize(jaVectormath2::perpendicular(triangleVertices[firstSupportPointIndex] - triangleVertices[secondSupportPointIndex]));

	float normalProjection = jaVectormath2::projection(newSearchDir, searchPoint);
	if (normalProjection < 0.0f)
		newSearchDir = -newSearchDir;

	supportPointIndex = TriangleMesh2d::findSupportPointIndex(triangleVertices, newSearchDir);

	TriangleMesh2d::getSupportTriangle(triangleVertices, supportPointIndex, supportTriangle);
}

/*bool Sat2d::penetrationBoxBoxBullet(ObjectHandle2d* A, ObjectHandle2d* B, Contact2d*, jaVector2& relVel)
{
	return true;
}*/

/*uint32_t Sat2d::penetrationBoxBox(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	const Box2d* boxA = (Box2d*)a;
	const Box2d* boxB = (Box2d*)b;
	const jaTransform2& trA = transformA;
	const jaTransform2& trB = transformB;

	const jaMatrix2 invRotA = jaVectormath2::inverse(trA.rotationMatrix);
	const jaMatrix2 invRotB = jaVectormath2::inverse(trB.rotationMatrix);

	jaVector2 searchDir;
	jaVector2 normal;
	jaVector2 temp;

	jaFloat minA,maxA,minB,maxB;
	jaFloat overlapMin = GILMAXFLOAT, overlap;

	const jaTransform2 relTransform = jaTransform2(invRotA * (trB.position - trA.position), trB.rotationMatrix * invRotA);
	const jaMatrix2    relInvRot    = jaVectormath2::inverse(relTransform.rotationMatrix);

	for(uint8_t i=0; i < 2; i++)
	{
		normal = Sat2d::axis[i];
		searchDir = normal;

		temp = boxA->findSupportPoint(searchDir);
		maxA = jaVectormath2::projection(temp, normal);

		minA = jaVectormath2::projection(-temp, normal);

		if(minA > maxA)
		{
			Sat2d::SwitchValues(minA,maxA);
		}

		searchDir = relInvRot * normal;
		temp = boxB->findSupportPoint(searchDir);
		maxB = jaVectormath2::projection(relTransform * temp, normal);

		minB = jaVectormath2::projection(relTransform * -temp, normal);

		if(minB > maxB)
		{
			Sat2d::SwitchValues(minB,maxB);
		} 

		overlap = Sat2d::CalculateInterval(minA,minB,maxA,maxB);
		if(overlap < 0) return 0;

		if(overlap < overlapMin)
		{
			overlapMin = overlap;
			contact->normal = trA.rotationMatrix * Sat2d::axis[i];
		}
	}

	for(uint8_t i =0; i < 2; i++)
	{
		normal = relTransform.rotationMatrix * Sat2d::axis[i];
		searchDir = normal;

		temp = boxA->findSupportPoint(searchDir);
		maxA = jaVectormath2::projection(temp, normal);

		minA = jaVectormath2::projection(-temp, normal);

		if(minA > maxA)
		{
			Sat2d::SwitchValues(minA,maxA);
		} 

		searchDir = relInvRot * normal;
		temp = boxB->findSupportPoint(searchDir);
		maxB = jaVectormath2::projection(relTransform * temp, normal);

		minB = jaVectormath2::projection(relTransform * -temp, normal);

		if(minB > maxB)
		{
			Sat2d::SwitchValues(minB,maxB);
		}
		overlap = Sat2d::CalculateInterval(minA,minB,maxA,maxB);
		if(overlap < 0) return 0;
		if(overlap < overlapMin)
		{
			overlapMin = overlap;
			contact->normal = trB.rotationMatrix * Sat2d::axis[i];
		}
	}

	if (jaVectormath2::projection(trA.position - trB.position, contact->normal) < GIL_REAL(0.0))
	{
		contact->normal = -contact->normal;
	}

	contact->depth = overlapMin;
	contact->pointNum = 1;
	contact->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + 0; // need to update this part to contact hash be unique

	jaVector2 vA[3];
	jaVector2 vB[3];

	vA[0] = trA * boxA->findSupportPoint(invRotA * -contact->normal);
	vB[0] = trB * boxB->findSupportPoint(invRotB * contact->normal);

	jaVector2 edge = vB[0] - vA[0];
	searchDir = jaVector2(-edge.y,edge.x);
	searchDir.normalize();
	
	temp = boxA->findSupportPoint(invRotA * searchDir); // mirror point
	vA[1] = trA * temp;
	if(jaVectormath2::equals(vA[0], vA[1])) // obiektA jest odcinkiem obiektB tr�jk�tem
	{
		contact->rA[0] = vA[0] - trA.position;
		contact->rB[0] = vA[0] + (contact->normal * contact->depth) - trB.position;
	}
	else
	{
		vA[2] = trA * -temp; // lustrzane odbicie;
		if (jaVectormath2::equals(vA[0], vA[2])) // obiektA jest odcinkiem obiektB tr�jk�tem
		{
			contact->rA[0] = vA[0] - trA.position;	
			contact->rB[0] = vA[0] + (contact->normal * contact->depth) - trB.position;
		}
		else // obiektA jest tr�jk�tem 
		{
			temp = boxB->findSupportPoint(invRotB * searchDir);

			vB[1] = trB * temp;
			if (jaVectormath2::equals(vB[0], vB[1])) // obiektA jest tr�jk�tem obiektB odcinkiem
			{
				contact->rB[0] = vB[0] - trB.position;
				contact->rA[0] = vB[0] - (contact->normal * contact->depth) - trA.position;
			}
			else 
			{
				vB[2] = trB * -temp;
				if (jaVectormath2::equals(vB[0], vB[2])) // obiektA jest tr�jk�tem obiektB odcinkiem 
				{
					contact->rB[0] = vB[0] - trB.position;
					contact->rA[0] = vB[0] - (contact->normal * contact->depth) - trA.position;
				}
				else // obiekt a i b jest tr�jk�tem
				{
					contact->rA[0] = vA[0] - trA.position;
					contact->rB[0] = vA[0] + (contact->normal * contact->depth) - trB.position;
				}
			}		
		}
	}

	return 1;
}*/

// Genenrated contact hash
// if normal is on A hash = indexOfSupportPoint on B with minimal penetration
// if normal is on B hash = indexOfSupportPoint on A with minimal penetration + num of B points
// if there is two collision points then hash = num of B points + num of A points
uint32_t Sat2d::penetrationConvexPolygonConvexPolygon(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	const jaTransform2& aT = transformA;
	const jaTransform2& bT = transformB;
	jaMatrix2 invRotA = jaVectormath2::inverse(aT.rotationMatrix);
	jaMatrix2 invRotB = jaVectormath2::inverse(bT.rotationMatrix);

	jaVector2 ABdiff(bT.position.x - aT.position.x, bT.position.y - aT.position.y); 
	const jaVector2 supportDir = jaVectormath2::normalize(ABdiff);
	//uint16_t indexA = a->findSupportPointIndex(invRotA * ABdiff);
	//uint16_t indexB = b->findSupportPointIndex(invRotB * -ABdiff);
	uint16_t indexA;
	uint16_t indexB;

	jaVector2 vA[3]; // 1 - index of support point
	jaVector2 vB[3]; // 1 - index of support point

	//Get support triangle in counter clockwise order
	//a->getSupportTriangle(indexA,vA);
	getSupportTriangle(a, invRotA * supportDir, invRotA * ABdiff, vA, indexA);
	//b->getSupportTriangle(indexB,vB);
	getSupportTriangle(b, invRotB * -supportDir, invRotB * (-ABdiff), vB, indexB);

	//Transform triangles to world space
	for(uint8_t i=0; i<3; ++i)
	{
		vA[i] = transformA * vA[i];
		vB[i] = transformB * vB[i];
	}

	jaVector2 normals[4];
	//Make normals from support trangles edges
	normals[0] = jaVectormath2::perpendicular(vA[0] - vA[1]); //normal on the left of vector 
	normals[1] = jaVectormath2::perpendicular(vA[1] - vA[2]); //normal on the left of vector
	normals[2] = jaVectormath2::perpendicular(vB[0] - vB[1]); //normal on the left of vector
	normals[3] = jaVectormath2::perpendicular(vB[1] - vB[2]); //normal on the left of vector

	uint8_t bestNormalIndex = 0; // index of normal with minimal projected penetration
	uint8_t aPointIndex = 1; // Index of point from triangle
	uint8_t bPointIndex = 1; // Index of point from triangle
	jaFloat minDepth = GILMAXFLOAT; // mininmal projected penetration on normal
	jaFloat c1ProjB  = GIL_ZERO; // projected penetration of first contact
	jaFloat c1ProjA  = GIL_ZERO;

	//Find best normal
	for(uint8_t i=0; i<2; ++i) //A trinagle normals
	{
		normals[i].normalize(); // Can be deleted ?

		jaFloat aProj = jaVectormath2::projection(normals[i], vA[1]);
		jaFloat bProj = GILMAXFLOAT;

		uint8_t pointIndex = 1;
		for(uint8_t z=0; z<3; ++z) // Project all three points from B and pick best
		{
			jaFloat tempBProj = jaVectormath2::projection(normals[i], vB[z]);
			if(tempBProj < bProj)
			{
				bProj = tempBProj;
				pointIndex = z;
			}
		}

		jaFloat depth = aProj - bProj;

		if(depth < GIL_ZERO)
			return 0;

		if(depth < minDepth)
		{
			minDepth = depth;
			bestNormalIndex = i;
			c1ProjB = bProj; // Normal on A object so penetration point is on B
			c1ProjA = aProj;
			bPointIndex = pointIndex;
		}
	}

	for(uint8_t i=2; i<4; ++i) //B triangle normals
	{
		normals[i].normalize(); // Sqrt

		jaFloat aProj = GILMAXFLOAT;
		jaFloat bProj = jaVectormath2::projection(normals[i], vB[1]);

		uint8_t pointIndex = 1;
		for(uint8_t z=0; z<3; ++z) // Project all three points from B and pick best
		{
			jaFloat tempAProj = jaVectormath2::projection(normals[i], vA[z]);
			if(tempAProj < aProj)
			{
				aProj = tempAProj;
				pointIndex = z;
			}
		}
	
		jaFloat depth = bProj - aProj;

		if(depth < GIL_ZERO)
			return 0;

		if(depth < minDepth)
		{
			minDepth = depth;
			bestNormalIndex = i;
			c1ProjA = aProj; // Normal on B object so penetration point is on A
			c1ProjB = bProj;
			aPointIndex = pointIndex;
		}
	}

	contact->depth  = minDepth;
	contact->normal = normals[bestNormalIndex];

	if(bestNormalIndex < 2) //Normal from object A
	{
		jaVector2 clipLeftNormal = jaVectormath2::perpendicular(normals[bestNormalIndex]);
		if(bestNormalIndex == 0)
		{
			Sat2d::clipPoint(vA[0],-clipLeftNormal,&vB[bPointIndex]);
			Sat2d::clipPoint(vA[1], clipLeftNormal,&vB[bPointIndex]);
		}
		else // bestNormalIndex = 1
		{
			Sat2d::clipPoint(vA[1],-clipLeftNormal,&vB[bPointIndex]);
			Sat2d::clipPoint(vA[2], clipLeftNormal,&vB[bPointIndex]);
		}
		
		contact->hash  = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + indexB;
		contact->rB[0] = vB[bPointIndex] - bT.position;
		contact->rA[0] = vB[bPointIndex] + normals[bestNormalIndex] * minDepth - aT.position;
		contact->pointNum = 1;

		for(uint8_t i=0; i<3; ++i)
		{
			if(i == bPointIndex) // Avoid procecting one point two times
				continue;

			jaFloat c2ProjB = jaVectormath2::projection(normals[bestNormalIndex], vB[i]);
			
			if(c2ProjB > c1ProjA)
				continue;
			
			jaFloat diffProj = abs(c2ProjB - c1ProjB);
			if(diffProj < ja::setup::gilgamesh::SECOND_CONTACT2D_TOLERANCE)
			{
				contact->pointNum = 2;
				contact->hash     = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + a->getVerticesNum() + b->getVerticesNum();
				
				//Clip point
				if(bestNormalIndex == 0)
				{
					Sat2d::clipPoint(vA[0],-clipLeftNormal, &vB[i]);
					Sat2d::clipPoint(vA[1], clipLeftNormal, &vB[i]);
				}
				else // bestNormalIndex == 1
				{
					Sat2d::clipPoint(vA[1],-clipLeftNormal, &vB[i]);
					Sat2d::clipPoint(vA[2], clipLeftNormal, &vB[i]);
				}

				contact->rB[1] = vB[i] - bT.position;
				contact->rA[1] = vB[i] + normals[bestNormalIndex] * contact->depth - aT.position;
				break;
			}
		}
	}
	else // Normal from object B
	{
		jaVector2 clipLeftNormal = jaVectormath2::perpendicular(normals[bestNormalIndex]);
		if(bestNormalIndex == 2)
		{
			Sat2d::clipPoint(vB[0],-clipLeftNormal,&vA[aPointIndex]);
			Sat2d::clipPoint(vB[1],clipLeftNormal,&vA[aPointIndex]);
		}
		else // bestNormalIndex = 3
		{
			Sat2d::clipPoint(vB[1],-clipLeftNormal,&vA[aPointIndex]);
			Sat2d::clipPoint(vB[2],clipLeftNormal,&vA[aPointIndex]);
		}

		contact->hash  = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + indexA + b->getVerticesNum();
		contact->rA[0] = vA[aPointIndex] - aT.position;
		contact->rB[0] = vA[aPointIndex] + normals[bestNormalIndex] * minDepth - bT.position;
		contact->pointNum = 1;

		for(uint8_t i=0; i<3; ++i)
		{
			if(i == aPointIndex) // Avoid projecting one point two times
				continue;

			jaFloat c2ProjA = jaVectormath2::projection(normals[bestNormalIndex], vA[i]);

			if(c2ProjA > c1ProjB)
				continue;

			jaFloat diffProj = abs(c2ProjA - c1ProjA);
			if(diffProj < ja::setup::gilgamesh::SECOND_CONTACT2D_TOLERANCE)
			{
				contact->pointNum = 2;
				contact->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + a->getVerticesNum() + b->getVerticesNum();

				//Find slice vector
				if(bestNormalIndex == 2)
				{
					Sat2d::clipPoint(vB[0],-clipLeftNormal, &vA[i]);
					Sat2d::clipPoint(vB[1], clipLeftNormal, &vA[i]);
				}
				else // bestNormalIndex == 3
				{
					Sat2d::clipPoint(vB[1],-clipLeftNormal, &vA[i]);
					Sat2d::clipPoint(vB[2], clipLeftNormal, &vA[i]);
				}

				contact->rA[1] = vA[i] - aT.position;
				contact->rB[1] = vA[i] + normals[bestNormalIndex] * contact->depth - bT.position;
				break;
			}
		}
	}

	if (jaVectormath2::projection(ABdiff, contact->normal) < GIL_REAL(0.0))
	{
		contact->normal = -contact->normal;
	}

	return 1;
}

uint32_t Sat2d::penetrationCircleCircle(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	Circle2d* circleA = (Circle2d*)a;
	Circle2d* circleB = (Circle2d*)b;
	
	contact->normal = transformB.position - transformA.position;
	jaFloat distance = contact->normal.normalize();
	contact->depth = distance - circleB->radius - circleA->radius;
	if(contact->depth > 0)
		return 0;

	contact->rA[0] = circleA->findSupportPoint(contact->normal);
	contact->rB[0] = circleB->findSupportPoint(-contact->normal);
	contact->pointNum = 1;
	contact->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + 0;

	return 1;
}

uint32_t Sat2d::penetrationConvexPolygonPoint(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	Shape2d*  convexPolygon;
	Point2d*  point;
	const jaTransform2* convexPolygonTransform;
	const jaTransform2* pointTransform;

	bool switched = false;
	if (a->getType() == (uint8_t)ShapeType::Point) // swap handles A=ConvexPolygon B=Circle
	{
		convexPolygon  = (Shape2d*)b;
		pointTransform = &transformA;
		convexPolygonTransform = &transformB;
		switched = true;
	}
	else {
		convexPolygon = (Shape2d*)a;
		point         = (Point2d*)b;
		convexPolygonTransform = &transformA;
		pointTransform = &transformB;
	}

	jaVector2 ABdiff      = pointTransform->position - convexPolygonTransform->position; // circle center in convex polygon space
	const jaVector2 pointCenter = jaVectormath2::inverse(convexPolygonTransform->rotationMatrix) * ABdiff;
	const jaVector2 supportDir  = jaVectormath2::normalize(pointCenter);

	jaVector2 convexTriangle[3];

	//getSupportTriangle(convexPolygon, supportDir, circleCenter, convexTriangle);
	uint16_t supportPoint = convexPolygon->findSupportPointIndex(supportDir);
	convexPolygon->getSupportTriangle(supportPoint, convexTriangle);

	jaVector2 normals[3];
	normals[0] = jaVectormath2::perpendicular(convexTriangle[0] - convexTriangle[1]); //normal on the left of vector 
	normals[1] = jaVectormath2::perpendicular(convexTriangle[1] - convexTriangle[2]); //normal on the left of vector

	uint8_t bestNormalIndex = 0; // index of normal with minimal projected penetration
	jaFloat minDepth        = GILMAXFLOAT; // mininmal projected penetration on normal

	//Find best normal
	for (uint8_t i = 0; i<2; ++i) //A trinagle normals
	{
		normals[i].normalize(); // Can be deleted ?

		jaFloat convexProj = jaVectormath2::projection(normals[i], convexTriangle[1]);
		jaFloat pointProj = jaVectormath2::projection(normals[i], pointCenter);

		jaFloat depth = convexProj - pointProj;

		if (depth < GIL_ZERO)
			return 0;

		if (depth < minDepth)
		{
			minDepth = depth;
			bestNormalIndex = i;
		}
	}

	contact->pointNum = 1;
	contact->depth = minDepth;
	contact->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + 0;
	contact->normal = convexPolygonTransform->rotationMatrix * normals[bestNormalIndex];

	{
		contact->rA[0] = (convexPolygonTransform->rotationMatrix * pointCenter) + (contact->normal * minDepth);//(convexPolygonTransform->rotationMatrix * circleTransformedPoint) + (contact->normal * minDepth);
		contact->rB[0] = jaVector2(0.0f, 0.0f);//circle->findSupportPoint(-contact->normal);
		
		if (switched)
		{
			ABdiff = -ABdiff;

			jaVector2 tempA = contact->rA[0];
			contact->rA[0]  = contact->rB[0];
			contact->rB[0]  = tempA;
		}
	}

	if (jaVectormath2::projection(ABdiff, contact->normal) < GIL_REAL(0.0))
	{
		contact->normal = -contact->normal;
	}

	return 1;
}

uint32_t Sat2d::penetrationCirclePoint(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	Shape2d*  circle;
	Point2d*  point;
	const jaTransform2* circleTransform;
	const jaTransform2* pointTransform;

	bool switched = false;
	if (a->getType() == (uint8_t)ShapeType::Point) // swap handles A=ConvexPolygon B=Circle
	{
		circle          = (Circle2d*)b;
		pointTransform  = &transformA;
		circleTransform = &transformB;
		switched = true;
	}
	else {
		circle          = (Circle2d*)a;
		point           = (Point2d*)b;
		circleTransform = &transformA;
		pointTransform  = &transformB;
	}

	jaVector2 ABdiff            = pointTransform->position - circleTransform->position; // circle center in convex polygon space
	const jaVector2 pointCenter = ABdiff;
	const jaVector2 supportDir  = jaVectormath2::normalize(ABdiff);

	const jaVector2 circleSupportPoint = circle->findSupportPoint(supportDir);

	jaFloat minDepth;

	//Find best normal
	{
		jaFloat circleProj = jaVectormath2::projection(supportDir, circleSupportPoint);
		jaFloat pointProj  = jaVectormath2::projection(supportDir, pointCenter);

		minDepth = circleProj - pointProj;

		if (minDepth < GIL_ZERO)
			return 0;
	}

	contact->pointNum = 1;
	contact->depth    = minDepth;
	contact->hash     = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + 0;
	contact->normal   = supportDir; //convexPolygonTransform->rotationMatrix * normals[bestNormalIndex];

	{
		contact->rA[0] = pointCenter + (contact->normal * minDepth);
		contact->rB[0] = jaVector2(0.0f, 0.0f);

		if (switched)
		{
			ABdiff = -ABdiff;

			jaVector2 tempA = contact->rA[0];
			contact->rA[0]  = contact->rB[0];
			contact->rB[0]  = tempA;
		}
	}

	if (jaVectormath2::projection(ABdiff, contact->normal) < GIL_REAL(0.0))
	{
		contact->normal = -contact->normal;
	}

	return 1;
}

uint32_t Sat2d::penetrationConvexPolygonCircle(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	Shape2d*  convexPolygon;
	Circle2d* circle;
	const jaTransform2* convexPolygonTransform;
	const jaTransform2* circleTransform;

	bool switched = false;
	if (a->getType() == (uint8_t)ShapeType::Circle) // swap handles A=ConvexPolygon B=Circle
	{
		circle        = (Circle2d*)a;
		convexPolygon = (Shape2d*)b;
		circleTransform        = &transformA;
		convexPolygonTransform = &transformB;
		switched = true;
	}
	else {
		convexPolygon = (Shape2d*)a;
		circle        = (Circle2d*)b;
		convexPolygonTransform = &transformA;
		circleTransform        = &transformB;
	}

	jaVector2 ABdiff       = circleTransform->position - convexPolygonTransform->position; // circle center in convex polygon space
	jaVector2 circleCenter = jaVectormath2::inverse(convexPolygonTransform->rotationMatrix) * ABdiff;
	jaVector2 supportDir   = jaVectormath2::normalize(circleCenter);

	jaVector2 convexTriangle[3];

	//uint16_t convexSupportPoint = convexPolygon->findSupportPointIndex(supportDir);
	//convexPolygon->getSupportTriangle(convexSupportPoint, convexTriangle);
	uint16_t supportPointIndex;
	getSupportTriangle(convexPolygon, supportDir, circleCenter, convexTriangle, supportPointIndex);
 
	jaVector2 normals[3];
	normals[0] = jaVectormath2::perpendicular(convexTriangle[0] - convexTriangle[1]); //normal on the left of vector 
	normals[1] = jaVectormath2::perpendicular(convexTriangle[1] - convexTriangle[2]); //normal on the left of vector
	normals[2] = circleCenter - convexTriangle[1];

	uint8_t bestNormalIndex       = 0; // index of normal with minimal projected penetration
	jaFloat minDepth              = GILMAXFLOAT; // mininmal projected penetration on normal

	//Find best normal
	for (uint8_t i = 0; i<2; ++i) //A trinagle normals
	{
		normals[i].normalize(); // Can be deleted ?

		jaFloat convexProj = jaVectormath2::projection(normals[i], convexTriangle[1]);

		const jaVector2 circlePoint            = circle->findSupportPoint(-normals[i]);
		const jaVector2 circleTransformedPoint = circlePoint + circleCenter; // Position in box space
		jaFloat         circleProj             = jaVectormath2::projection(normals[i], circleTransformedPoint);

		jaFloat depth = convexProj - circleProj;

		if (depth < GIL_ZERO)
			return 0;

		if (depth < minDepth)
		{
			minDepth              = depth;
			bestNormalIndex       = i;
		}
	}

	//check which normal to use
	jaFloat edge0Proj = jaVectormath2::projection(convexTriangle[0] - convexTriangle[1], normals[2]); // circleCenter - convexTriangle[1];
	jaFloat edge1Proj = jaVectormath2::projection(convexTriangle[2] - convexTriangle[1], normals[2]);

	bool testCircleNormal = ((edge0Proj < 0.0f) && (edge1Proj < 0.0f));

	if (testCircleNormal)
	{
		normals[2].normalize();
		//Circle normal
		jaFloat convexProj = jaVectormath2::projection(normals[2], convexTriangle[1]);

		const jaVector2 circlePoint = circle->findSupportPoint(-normals[2]);
		const jaVector2 circleTransformedPoint = circlePoint + circleCenter; // Position in box space
		jaFloat         circleProj = jaVectormath2::projection(normals[2], circleTransformedPoint);

		jaFloat depth = convexProj - circleProj;

		if (depth < GIL_ZERO)
			return 0;

		if (depth < minDepth)
		{
			minDepth              = depth;
			bestNormalIndex       = 2;
		}
	}

	contact->pointNum = 1;
	contact->depth    = minDepth;
	contact->hash     = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + 0;
	contact->normal   = convexPolygonTransform->rotationMatrix * normals[bestNormalIndex];


	{
		if (2 == bestNormalIndex) // normal from circle
		{
			contact->rA[0] = convexPolygonTransform->rotationMatrix * convexTriangle[1];
			contact->rB[0] = circle->findSupportPoint(-contact->normal);
		}
		else // normal from convex polygon
		{
			const jaVector2 circlePoint            = circle->findSupportPoint(-normals[bestNormalIndex]);
			jaVector2       circleTransformedPoint = circlePoint + circleCenter;

			jaVector2 clipLeftNormal = jaVectormath2::perpendicular(normals[bestNormalIndex]);
			if (bestNormalIndex == 0)
			{
				Sat2d::clipPoint(convexTriangle[0], -clipLeftNormal, &circleTransformedPoint);
				Sat2d::clipPoint(convexTriangle[1], clipLeftNormal,  &circleTransformedPoint);
			}
			else // bestNormalIndex = 1
			{
				Sat2d::clipPoint(convexTriangle[1], -clipLeftNormal, &circleTransformedPoint);
				Sat2d::clipPoint(convexTriangle[2], clipLeftNormal,  &circleTransformedPoint);
			}

			contact->rA[0] = (convexPolygonTransform->rotationMatrix * circleTransformedPoint) + (contact->normal * minDepth);
			contact->rB[0] = circle->findSupportPoint(-contact->normal);
		}

	
		if (switched)
		{
			ABdiff = -ABdiff;
		
			jaVector2 tempA = contact->rA[0];
			contact->rA[0]  = contact->rB[0];
			contact->rB[0]  = tempA;
		}
	}

	if (jaVectormath2::projection(ABdiff, contact->normal) < GIL_REAL(0.0))
	{
		contact->normal = -contact->normal;
	}

	return 1;
}

uint32_t Sat2d::penetraionTriangleMeshCircle(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	TriangleMesh2d*     triangleMesh;
	Circle2d*           circle;
	const jaTransform2* triangleMeshTransform;
	const jaTransform2* circleTransform;

	bool switched = false;
	if (a->getType() == (uint8_t)ShapeType::Circle) // swap handles A=Box B=Circle
	{
		circle                = (Circle2d*)a;
		triangleMesh          = (TriangleMesh2d*)b;
		circleTransform       = &transformA;
		triangleMeshTransform = &transformB;
		switched = true;
	}
	else {
		triangleMesh          = (TriangleMesh2d*)a;
		circle                = (Circle2d*)b;
		triangleMeshTransform = &transformA;
		circleTransform       = &transformB;
	}

	uint16_t         trianglesNum    = triangleMesh->getTrianglesNum();
	const jaVector2* pVertices       = triangleMesh->getVertices();
	const uint16_t*  pIndices        = triangleMesh->getTriangleIndices();
	const uint8_t*   pEdgesToNotTest = triangleMesh->getEdgesToNotTest();

	uint16_t collisionsNum = 0;

	jaVector2 ABdiff             = circleTransform->position - triangleMeshTransform->position; // circle center in triangle space
	const jaVector2 circleCenter = jaVectormath2::inverse(triangleMeshTransform->rotationMatrix) * ABdiff;

	ABdiff = (switched) ? -ABdiff : ABdiff;;

	const uint8_t normalToNotTestLUT[3][2] = { { 4, 1 }, { 1, 2 }, { 2, 4 } };

	for (uint16_t triangleIt = 0; triangleIt < trianglesNum; ++triangleIt)
	{
		const uint8_t edgesToNotTestFlag = pEdgesToNotTest[triangleIt];
		if ((1 | 2 | 4) == edgesToNotTestFlag) // it is inner triangle
			continue;

		const uint16_t triangleI = triangleIt * 3;

		const uint16_t index[3] = { pIndices[triangleI], pIndices[triangleI + 1], pIndices[triangleI + 2] };
	
		const jaVector2 triangleVertices[3] = { pVertices[index[0]], pVertices[index[1]], pVertices[index[2]] };
		const jaVector2 triangleCenter      = ((triangleVertices[0] + triangleVertices[1] + triangleVertices[2]) / 3.0f);
		
		const jaVector2 circleTriangleDiff = circleCenter - triangleCenter;
		const jaVector2 supportDir = jaVectormath2::normalize(circleTriangleDiff);

		jaVector2 convexTriangle[3];

		uint8_t triangleSupportPoint = 0;
		getSupportTriangle(triangleVertices, supportDir, circleCenter, convexTriangle, triangleSupportPoint);

		uint8_t normalToNotTest[3] = { normalToNotTestLUT[triangleSupportPoint][0] & edgesToNotTestFlag, normalToNotTestLUT[triangleSupportPoint][1] & edgesToNotTestFlag, 0};

		if ((0 != normalToNotTest[0]) && (0 != normalToNotTest[1]))
			goto NO_TRIANGLE_CIRCLE_INTERSECTION; // all normals come from internal edges

		jaVector2 normals[3];
		normals[0] = jaVectormath2::perpendicular(convexTriangle[0] - convexTriangle[1]); //normal on the left of vector 
		normals[1] = jaVectormath2::perpendicular(convexTriangle[1] - convexTriangle[2]); //normal on the left of vector
		normals[2] = circleCenter - convexTriangle[1];

		uint8_t bestNormalIndex = 0; // index of normal with minimal projected penetration
		jaFloat minDepth        = GILMAXFLOAT; // mininmal projected penetration on normal

		float depth[2];
		//Find best normal
		for (uint8_t i = 0; i<2; ++i) //A trinagle normals
		{
			normals[i].normalize(); // Can be deleted ?

			//jaFloat convexProj = jaVectormath2::projection(normals[i], convexTriangle[1]);

			const jaVector2 circlePoint = circle->findSupportPoint(-normals[i]);
			//const jaVector2 circleTransformedPoint = circlePoint + circleCenter; // Position in box space
			const jaVector2 circleTransformedPoint = (circlePoint + circleCenter) - convexTriangle[1]; 
			jaFloat         circleProj = jaVectormath2::projection(normals[i], circleTransformedPoint);

			//depth[i] = convexProj - circleProj;
			depth[i] = -circleProj;

			if (depth[i] < GIL_ZERO)
				goto NO_TRIANGLE_CIRCLE_INTERSECTION;

			if (depth[i] < minDepth)
			{
				minDepth        = depth[i];
				bestNormalIndex = i;
			}
		}

		bool wasInternalEdgeBestNormal = false;

		if (normalToNotTest[bestNormalIndex])
		{
			bestNormalIndex = (bestNormalIndex == 0) ? 1 : 0;
			minDepth = depth[bestNormalIndex];
			wasInternalEdgeBestNormal = true;
			//continue;
		}

		//check which normal to use
		jaFloat edge0Proj = jaVectormath2::projection(convexTriangle[0] - convexTriangle[1], normals[2]); // circleCenter - convexTriangle[1];
		jaFloat edge1Proj = jaVectormath2::projection(convexTriangle[2] - convexTriangle[1], normals[2]);

		bool testCircleNormal = ((edge0Proj < 0.0f) && (edge1Proj < 0.0f)) && (!wasInternalEdgeBestNormal);

		if (testCircleNormal)
		{
			normals[2].normalize();
			//Circle normal
			jaFloat convexProj = jaVectormath2::projection(normals[2], convexTriangle[1]);

			const jaVector2 circlePoint            = circle->findSupportPoint(-normals[2]);
			const jaVector2 circleTransformedPoint = circlePoint + circleCenter; // Position in box space
			jaFloat         circleProj             = jaVectormath2::projection(normals[2], circleTransformedPoint);

			jaFloat depth = convexProj - circleProj;

			if (depth < GIL_ZERO)
				goto NO_TRIANGLE_CIRCLE_INTERSECTION;

			if (depth < minDepth)
			{
				minDepth = depth;
				bestNormalIndex = 2;
			}
		}

		Contact2d* contactIt = &contact[collisionsNum];

		contactIt->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + triangleIt;
		
		for (uint8_t i = 0; i < collisionsNum; ++i)
		{
			//if ((contact[i].hash & triangleIdHash) == (contactIt->hash & triangleIdHash)) // This same triangle point
			uint16_t triangleId = contact[i].hash;

			bool triangleConnected = index[triangleSupportPoint] == pIndices[triangleId * 3];
			triangleConnected     |= index[triangleSupportPoint] == pIndices[(triangleId * 3) + 1];
			triangleConnected     |= index[triangleSupportPoint] == pIndices[(triangleId * 3) + 2];

			if (triangleConnected)
			{
				if (minDepth < contact[i].depth)
				{
					contactIt = &contact[i];
					--collisionsNum; // Will be increased anyway
					break;
				}

				goto NO_TRIANGLE_CIRCLE_INTERSECTION; // contact duplicated
			}
		}

		contactIt->depth    = minDepth;
		contactIt->pointNum = 1;
		contactIt->normal   = triangleMeshTransform->rotationMatrix * normals[bestNormalIndex];
		contactIt->objectA  = contact->objectA;
		contactIt->objectB  = contact->objectB;

		{
			if (2 == bestNormalIndex) // normal from circle
			{
				contactIt->rA[0] = triangleMeshTransform->rotationMatrix * convexTriangle[1];
				contactIt->rB[0] = circle->findSupportPoint(-contactIt->normal);
			}
			else // normal from convex polygon
			{
				const jaVector2 circlePoint = circle->findSupportPoint(-normals[bestNormalIndex]);
				jaVector2       circleTransformedPoint = circlePoint + circleCenter;

				jaVector2 clipLeftNormal = jaVectormath2::perpendicular(normals[bestNormalIndex]);
				if (bestNormalIndex == 0)
				{
					Sat2d::clipPoint(convexTriangle[0], -clipLeftNormal, &circleTransformedPoint);
					Sat2d::clipPoint(convexTriangle[1], clipLeftNormal, &circleTransformedPoint);
				}
				else // bestNormalIndex = 1
				{
					Sat2d::clipPoint(convexTriangle[1], -clipLeftNormal, &circleTransformedPoint);
					Sat2d::clipPoint(convexTriangle[2], clipLeftNormal, &circleTransformedPoint);
				}

				contactIt->rA[0] = (triangleMeshTransform->rotationMatrix * circleTransformedPoint) + (contactIt->normal * minDepth);
				contactIt->rB[0] = circle->findSupportPoint(-contactIt->normal);
			}

			if (switched)
			{
				jaVector2 tempA  = contactIt->rA[0];
				contactIt->rA[0] = contactIt->rB[0];
				contactIt->rB[0] = tempA;
			}
		}

		if (jaVectormath2::projection(ABdiff, contactIt->normal) < GIL_REAL(0.0))
		{
			contactIt->normal = -contactIt->normal;
		}

		++collisionsNum;

	NO_TRIANGLE_CIRCLE_INTERSECTION:
		continue;
	}

	return collisionsNum;
}

/*uint32_t Sat2d::penetrationBoxCircle(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	Box2d*    box;
	Circle2d* circle;
	const jaTransform2* boxTransform;
	const jaTransform2* circleTransform;

	bool switched = false;
	if(a->getType() == (uint8_t)ShapeType::Circle) // swap handles A=Box B=Circle
	{
		circle = (Circle2d*)a;
		box    = (Box2d*)b;
		circleTransform = &transformA;
		boxTransform    = &transformB;
		switched = true;
	} else {	
		box    = (Box2d*)a;
		circle = (Circle2d*)b;
		boxTransform    = &transformA;
		circleTransform = &transformB;
	}

	jaVector2 ABdiff       = circleTransform->position - boxTransform->position;
	jaVector2 circleCenter = jaVectormath2::inverse(boxTransform->rotationMatrix) * ABdiff;

	jaVector2 min(-box->getHalfWidth(),-box->getHalfHeight());
	jaVector2 max(box->getHalfWidth(),box->getHalfHeight());
	jaVector2 boxPoint;
	jaVector2 circlePoint;

	uint8_t t = 0;

	if(circleCenter.x < min.x)
	{
		boxPoint.x = min.x;
		circlePoint.x = circle->getRadius();
	}
	else if(circleCenter.x > max.x)
	{
		boxPoint.x = max.x;
		circlePoint.x = -circle->getRadius();
	}
	else
	{
		boxPoint.x = circleCenter.x;
		circlePoint.x = GIL_ZERO;
		t++;
	}

	if(circleCenter.y < min.y)
	{
		boxPoint.y = min.y;
		circlePoint.y = circle->getRadius();
	}
	else if(circleCenter.y > max.y)
	{
		boxPoint.y = max.y; 
		circlePoint.y = -circle->getRadius();
	}
	else
	{
		boxPoint.y = circleCenter.y;
		circlePoint.y = GIL_ZERO;
		t++;
	}

	if (jaVectormath2::length(circleCenter - boxPoint) > circle->radius)
		return 0;
	
	//calculation of contact point
	if(t == 2) // middle of circle inside box
	{
		jaFloat apX, apY;

		if(circleCenter.x > 0) apX = max.x - circleCenter.x;
		else apX = min.x - circleCenter.x;

		if(circleCenter.y > 0) apY = max.y - circleCenter.y;
		else apY = min.y - circleCenter.y;

		if(abs(apX) < abs(apY)) // on y axis
		{
			if(apX < GIL_ZERO) boxPoint.x = min.x;
			else boxPoint.x = max.x;

			if(apX < GIL_ZERO) circlePoint.x = circle->getRadius();
			else circlePoint.x = -circle->getRadius();
		}
		else // on x axis
		{
			if(apY < GIL_ZERO) boxPoint.y = min.y;
			else boxPoint.y = max.y;

			if(apY < GIL_ZERO) circlePoint.y = circle->getRadius();
			else circlePoint.y = -circle->getRadius();
		}
	}

	if(t == 0)
	{
		circlePoint = -circle->getRadius() * jaVectormath2::normalize(circleCenter - boxPoint);
	}

	if (switched)
	{
		ABdiff.x = -ABdiff.x;
		ABdiff.y = -ABdiff.y;

		contact->rA[0] = boxTransform->rotationMatrix * circlePoint;
		contact->rB[0] = boxTransform->rotationMatrix * boxPoint;
	}
	else
	{
		contact->rA[0] = boxTransform->rotationMatrix * boxPoint;
		contact->rB[0] = boxTransform->rotationMatrix * circlePoint;
	}
	contact->normal = ABdiff + contact->rB[0] - contact->rA[0];

	contact->pointNum = 1;
	contact->depth    = contact->normal.normalize();
	contact->hash     = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + 0;

	if (jaVectormath2::projection(ABdiff, contact->normal) < GIL_REAL(0.0))
	{
		contact->normal = -contact->normal;
	}

	return 1;
}*/

uint32_t Sat2d::penetrationTriangleMeshTriangleMesh(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	return 0;
}

uint32_t Sat2d::penetrationTriangleMeshConvexPolygon(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	TriangleMesh2d*     triangleMesh;
	Shape2d*            convexPolygon;
	const jaTransform2* triangleMeshTransform;
	const jaTransform2* convexPolygonTransform;	

	//A TriangleMesh
	//B ConvexPolygon

	bool switched = false;
	if (a->getType() == (uint8_t)ShapeType::TriangleMesh) // swap handles A=Box B=Triangle mesh
	{
		triangleMesh           = (TriangleMesh2d*)a;
		convexPolygon          = (Shape2d*)b;
		triangleMeshTransform  = &transformA;
		convexPolygonTransform = &transformB;
	}
	else 
	{
		convexPolygon          = (Shape2d*)a;
		triangleMesh           = (TriangleMesh2d*)b;
		convexPolygonTransform = &transformA;
		triangleMeshTransform  = &transformB;
		switched = true;
	}
	
	uint16_t         trianglesNum    = triangleMesh->getTrianglesNum();
	const jaVector2* pVertices       = triangleMesh->getVertices();
	const uint16_t*  pIndices        = triangleMesh->getTriangleIndices();
	const uint8_t*   pEdgesToNotTest = triangleMesh->getEdgesToNotTest();

	uint32_t collisionsNum = 0;

	jaVector2 ABdiff                    = convexPolygonTransform->position - triangleMeshTransform->position; // convex polygon in triangle space
	const jaMatrix2 invRot              = jaVectormath2::inverse(triangleMeshTransform->rotationMatrix);
	const jaVector2 convexPolygonCenter = invRot * ABdiff;
	jaTransform2 transformConvexPolygonToTriangleMeshSpace(convexPolygonCenter, invRot);

	ABdiff = (switched) ? -ABdiff : ABdiff;;

	const uint8_t normalToNotTestLUT[3][2] = { { 4, 1 }, { 1, 2 }, { 2, 4 } };

	for (uint16_t triangleIt = 0; triangleIt < trianglesNum; ++triangleIt)
	{
		const uint8_t edgesToNotTestFlag = pEdgesToNotTest[triangleIt];
		if ((1 | 2 | 4) == edgesToNotTestFlag) // it is inner triangle
			continue;

		const uint16_t triangleI = triangleIt * 3;

		const uint16_t index[3] = { pIndices[triangleI], pIndices[triangleI + 1], pIndices[triangleI + 2] };

		const jaVector2 triangleVertices[3] = { pVertices[index[0]], pVertices[index[1]], pVertices[index[2]] };
		const jaVector2 triangleCenter      = ((triangleVertices[0] + triangleVertices[1] + triangleVertices[2]) / 3.0f);

		const jaVector2 convexPolygonTriangleDiff = convexPolygonCenter - triangleCenter;
		const jaVector2 supportDir                = jaVectormath2::normalize(convexPolygonTriangleDiff);

		jaVector2 vA[3];

		uint8_t supportAIndex = TriangleMesh2d::findSupportPointIndex(triangleVertices, supportDir);
		TriangleMesh2d::getSupportTriangle(triangleVertices, supportAIndex, vA);

		uint8_t normalToNotTest[4] = { normalToNotTestLUT[supportAIndex][0] & edgesToNotTestFlag, normalToNotTestLUT[supportAIndex][1] & edgesToNotTestFlag, 0, 0};

		jaVector2 vB[3];
		uint16_t  supportBIndex = convexPolygon->findSupportPointIndex(-supportDir);
		convexPolygon->getSupportTriangle(supportBIndex, vB);

		vB[0] = transformConvexPolygonToTriangleMeshSpace * vB[0];
		vB[1] = transformConvexPolygonToTriangleMeshSpace * vB[1];
		vB[2] = transformConvexPolygonToTriangleMeshSpace * vB[2];

		jaVector2 normals[4];
		//Make normals from support trangles edges
		normals[0] = jaVectormath2::perpendicular(vA[0] - vA[1]); //normal on the left of vector 
		normals[1] = jaVectormath2::perpendicular(vA[1] - vA[2]); //normal on the left of vector
		normals[2] = jaVectormath2::perpendicular(vB[0] - vB[1]); //normal on the left of vector
		normals[3] = jaVectormath2::perpendicular(vB[1] - vB[2]); //normal on the left of vector

		uint8_t bestNormalIndex = 0; // index of normal with minimal projected penetration
		uint8_t aPointIndex     = 1; // Index of point from triangle
		uint8_t bPointIndex     = 1; // Index of point from triangle
		jaFloat minDepth        = GILMAXFLOAT; // mininmal projected penetration on normal
		jaFloat c1ProjB         = GIL_ZERO; // projected penetration of first contact
		jaFloat c1ProjA         = GIL_ZERO;

		//Find best normal
		for (uint8_t i = 0; i<2; ++i) //A trinagle normals
		{
			normals[i].normalize(); // Can be deleted ?

			jaFloat aProj = jaVectormath2::projection(normals[i], vA[1]);
			jaFloat bProj = GILMAXFLOAT;

			uint8_t pointIndex = 1;
			for (uint8_t z = 0; z<3; ++z) // Project all three points from B and pick best
			{
				jaFloat tempBProj = jaVectormath2::projection(normals[i], vB[z]);
				if (tempBProj < bProj)
				{
					bProj      = tempBProj;
					pointIndex = z;
				}
			}

			jaFloat depth = aProj - bProj;

			if (depth < GIL_ZERO)
				goto NO_TRIANGLE_BOX_INTERSECTION;

			if (normalToNotTest[i])
				continue;

			if (depth < minDepth)
			{
				minDepth = depth;
				bestNormalIndex = i;
				c1ProjB = bProj; // Normal on A object so penetration point is on B
				c1ProjA = aProj;
				bPointIndex = pointIndex;
			}
		}

		for (uint8_t i = 2; i<4; ++i) //B triangle normals
		{
			normals[i].normalize(); 

			jaFloat aProj = GILMAXFLOAT;
			jaFloat bProj = jaVectormath2::projection(normals[i], vB[1]);

			uint8_t pointIndex = 1;
			for (uint8_t z = 0; z<3; ++z) // Project all three points from B and pick best
			{
				jaFloat tempAProj = jaVectormath2::projection(normals[i], vA[z]);
				if (tempAProj < aProj)
				{
					aProj = tempAProj;
					pointIndex = z;
				}
			}

			jaFloat depth = bProj - aProj;

			if (depth < GIL_ZERO)
				goto NO_TRIANGLE_BOX_INTERSECTION;

			if (normalToNotTest[i])
				continue;

			if (depth < minDepth)
			{
				minDepth = depth;
				bestNormalIndex = i;
				c1ProjA = aProj; // Normal on B object so penetration point is on A
				c1ProjB = bProj;
				aPointIndex = pointIndex;
			}
		}

		if (normalToNotTest[bestNormalIndex])
			continue;

		Contact2d* contactIt = &contact[collisionsNum];

		contactIt->depth   = minDepth;
		contactIt->normal  = triangleMeshTransform->rotationMatrix * normals[bestNormalIndex];
		contactIt->objectA = contact->objectA;
		contactIt->objectB = contact->objectB;

		if (bestNormalIndex < 2) //Normal from object A
		{
			jaVector2 clipLeftNormal = jaVectormath2::perpendicular(normals[bestNormalIndex]);
			if (bestNormalIndex == 0)
			{
				Sat2d::clipPoint(vA[0], -clipLeftNormal, &vB[bPointIndex]);
				Sat2d::clipPoint(vA[1], clipLeftNormal, &vB[bPointIndex]);
			}
			else // bestNormalIndex = 1
			{
				Sat2d::clipPoint(vA[1], -clipLeftNormal, &vB[bPointIndex]);
				Sat2d::clipPoint(vA[2], clipLeftNormal, &vB[bPointIndex]);
			}

			contactIt->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + supportBIndex; // TO POPRAWIC ZEBY HASH BYL UNIKALNY
			//contact[collisionsNum].rB[0] = vB[bPointIndex] - bT.position;
			contactIt->rB[0] = triangleMeshTransform->rotationMatrix * (vB[bPointIndex] - transformConvexPolygonToTriangleMeshSpace.position);
			//contact[collisionsNum].rA[0] = vB[bPointIndex] + normals[bestNormalIndex] * minDepth - aT.position;
			contactIt->rA[0] = triangleMeshTransform->rotationMatrix * (vB[bPointIndex] + (normals[bestNormalIndex] * minDepth));
			contactIt->pointNum = 1;

			for (uint8_t i = 0; i<3; ++i)
			{
				if (i == bPointIndex) // Avoid projecting one point two times
					continue;

				jaFloat c2ProjB = jaVectormath2::projection(normals[bestNormalIndex], vB[i]);

				if (c2ProjB > c1ProjA)
					continue;

				jaFloat diffProj = abs(c2ProjB - c1ProjB);
				if (diffProj < ja::setup::gilgamesh::SECOND_CONTACT2D_TOLERANCE)
				{
					contactIt->pointNum = 2;
					contactIt->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + a->getVerticesNum() + b->getVerticesNum(); // TO POPRAWIC ZEBY HASH BYL UNIKALNY

					//Clip point
					if (bestNormalIndex == 0)
					{
						Sat2d::clipPoint(vA[0], -clipLeftNormal, &vB[i]);
						Sat2d::clipPoint(vA[1], clipLeftNormal, &vB[i]);
					}
					else // bestNormalIndex == 1
					{
						Sat2d::clipPoint(vA[1], -clipLeftNormal, &vB[i]);
						Sat2d::clipPoint(vA[2], clipLeftNormal, &vB[i]);
					}

					//contact[collisionsNum].rB[1] = vB[i] - bT.position;
					contactIt->rB[1] = triangleMeshTransform->rotationMatrix * (vB[i] - transformConvexPolygonToTriangleMeshSpace.position);
					//contact[collisionsNum].rA[1] = vB[i] + normals[bestNormalIndex] * contact[collisionsNum].depth - aT.position;
					contactIt->rA[1] = triangleMeshTransform->rotationMatrix * (vB[i] + (normals[bestNormalIndex] * minDepth));
					break;
				}
			}
		}
		else // Normal from object B
		{
			jaVector2 clipLeftNormal = jaVectormath2::perpendicular(normals[bestNormalIndex]);
			if (bestNormalIndex == 2)
			{
				Sat2d::clipPoint(vB[0], -clipLeftNormal, &vA[aPointIndex]);
				Sat2d::clipPoint(vB[1], clipLeftNormal, &vA[aPointIndex]);
			}
			else // bestNormalIndex = 3
			{
				Sat2d::clipPoint(vB[1], -clipLeftNormal, &vA[aPointIndex]);
				Sat2d::clipPoint(vB[2], clipLeftNormal, &vA[aPointIndex]);
			}

			contactIt->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + supportAIndex + b->getVerticesNum(); // TO POPRAWIC ZEBY HASH BYL UNIKALNY
			//contact[collisionsNum].rA[0] = vA[aPointIndex] - aT.position;
			contactIt->rA[0] = triangleMeshTransform->rotationMatrix * vA[aPointIndex];
			//contact[collisionsNum].rB[0] = vA[aPointIndex] + normals[bestNormalIndex] * minDepth - bT.position;
			contactIt->rB[0] = triangleMeshTransform->rotationMatrix * (vA[aPointIndex] + (normals[bestNormalIndex] * minDepth) - transformConvexPolygonToTriangleMeshSpace.position);
			contactIt->pointNum = 1;

			for (uint8_t i = 0; i<3; ++i)
			{
				if (i == aPointIndex) // Avoid projecting one point two times
					continue;

				jaFloat c2ProjA = jaVectormath2::projection(normals[bestNormalIndex], vA[i]);

				if (c2ProjA > c1ProjB)
					continue;

				jaFloat diffProj = abs(c2ProjA - c1ProjA);
				if (diffProj < ja::setup::gilgamesh::SECOND_CONTACT2D_TOLERANCE)
				{
					contactIt->pointNum = 2;
					contactIt->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + a->getVerticesNum() + b->getVerticesNum();

					//Find slice vector
					if (bestNormalIndex == 2)
					{
						Sat2d::clipPoint(vB[0], -clipLeftNormal, &vA[i]);
						Sat2d::clipPoint(vB[1], clipLeftNormal, &vA[i]);
					}
					else // bestNormalIndex == 3
					{
						Sat2d::clipPoint(vB[1], -clipLeftNormal, &vA[i]);
						Sat2d::clipPoint(vB[2], clipLeftNormal, &vA[i]);
					}

					//contact[collisionsNum].rA[1] = vA[i] - aT.position;
					contactIt->rA[1] = triangleMeshTransform->rotationMatrix * vA[i];
					//contact[collisionsNum].rB[1] = vA[i] + normals[bestNormalIndex] * contact[collisionsNum].depth - bT.position;
					contactIt->rB[1] = triangleMeshTransform->rotationMatrix * (vA[i] + (normals[bestNormalIndex] * minDepth) - transformConvexPolygonToTriangleMeshSpace.position);
					break;
				}
			}
		}

		if (jaVectormath2::projection(ABdiff, contactIt->normal) < GIL_REAL(0.0))
		{
			contactIt->normal = -contactIt->normal;
		}

		++collisionsNum;

	NO_TRIANGLE_BOX_INTERSECTION:
		continue;
	}

	return collisionsNum;
}

uint32_t Sat2d::penetrationMultiShapeShape(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	MultiShape2d* multiShape;
	Shape2d*      shape;

	bool swapped = false;
	if (a->getType() == (uint8_t)ShapeType::MultiShape) // swap handles A=Box B=Circle
	{
		multiShape = (MultiShape2d*)a;
		shape      = (Shape2d*)b;
	}
	else {
		multiShape = (MultiShape2d*)b;
		shape      = (Shape2d*)a;
		swapped = true;
	}

	ObjectHandle2d* shapeProxyIt = multiShape->getShapeHandle();
	ObjectHandle2d* handleA = contact->objectA;
	ObjectHandle2d* handleB = contact->objectB;
	jaTransform2    multiShapeTransform;
	jaVector2       shapeProxyPosition;
	uint32_t        collisionNum = 0;
	uint32_t        collisionIt;

	if (swapped)
	{
		for (shapeProxyIt; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
		{
			multiShapeTransform.rotationMatrix = transformB.rotationMatrix * shapeProxyIt->transform.rotationMatrix;
			shapeProxyPosition                 = transformB.rotationMatrix * shapeProxyIt->transform.position;
			multiShapeTransform.position       = transformB.position + shapeProxyPosition;

			contact[collisionNum].objectA = handleA;
			contact[collisionNum].objectB = handleB;

			collisionIt   = collisionNum;
			collisionNum += conf.getCollision(shape, transformA, shapeProxyIt->shape, multiShapeTransform, &contact[collisionNum], hashSeed, conf);

			for (collisionIt; collisionIt < collisionNum; ++collisionIt)
			{
				contact[collisionIt].rB[0]   += shapeProxyPosition;
				contact[collisionIt].rB[1]   += shapeProxyPosition;
				contact[collisionIt].objectB  = shapeProxyIt;
			}
		}
	}
	else {
		for (shapeProxyIt; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
		{
			multiShapeTransform.rotationMatrix = transformA.rotationMatrix * shapeProxyIt->transform.rotationMatrix;

			shapeProxyPosition = transformA.rotationMatrix * shapeProxyIt->transform.position;

			multiShapeTransform.position = transformA.position + shapeProxyPosition;

			contact[collisionNum].objectA = handleA;
			contact[collisionNum].objectB = handleB;

			collisionIt   = collisionNum;
			collisionNum += conf.getCollision(shapeProxyIt->shape, multiShapeTransform, shape, transformB, &contact[collisionNum], hashSeed, conf);

			for (collisionIt; collisionIt < collisionNum; ++collisionIt)
			{
				contact[collisionIt].rA[0]   += shapeProxyPosition;
				contact[collisionIt].rA[1]   += shapeProxyPosition;
				contact[collisionIt].objectA  = shapeProxyIt;
			}
		}
	}

	return collisionNum;
}

uint32_t Sat2d::penetrationMultiShapeMultiShape(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	const MultiShape2d* multiShapeA = (MultiShape2d*)a;
	const MultiShape2d* multiShapeB = (MultiShape2d*)b;

	ObjectHandle2d* shapeProxyAIt = multiShapeA->getShapeHandle();
	ObjectHandle2d* shapeProxyBIt = multiShapeB->getShapeHandle();

	ObjectHandle2d* handleA = contact->objectA;
	ObjectHandle2d* handleB = contact->objectB;

	jaTransform2 multiShapeTransformA;
	jaTransform2 multiShapeTransformB;
	jaVector2    shapeProxyPositionA;
	jaVector2    shapeProxyPositionB;
	uint32_t     collisionNum = 0;
	uint32_t     collisionIt;

	for (shapeProxyAIt; shapeProxyAIt != nullptr; shapeProxyAIt = shapeProxyAIt->next)
	{
		for (shapeProxyBIt = multiShapeB->getShapeHandle(); shapeProxyBIt != nullptr; shapeProxyBIt = shapeProxyBIt->next)
		{
			contact[collisionNum].objectA = handleA;
			contact[collisionNum].objectB = handleB;

			multiShapeTransformA.rotationMatrix = transformA.rotationMatrix * shapeProxyAIt->transform.rotationMatrix;
			shapeProxyPositionA = transformA.rotationMatrix * shapeProxyAIt->transform.position;
			multiShapeTransformA.position = transformA.position + shapeProxyPositionA;

			multiShapeTransformB.rotationMatrix = transformB.rotationMatrix * shapeProxyBIt->transform.rotationMatrix;
			shapeProxyPositionB = transformB.rotationMatrix * shapeProxyBIt->transform.position;
			multiShapeTransformB.position = transformB.position + shapeProxyPositionB;

			collisionIt   = collisionNum;
			collisionNum += conf.getCollision(shapeProxyAIt->shape, multiShapeTransformA, shapeProxyBIt->shape, multiShapeTransformB, &contact[collisionNum], hashSeed, conf);

			for (collisionIt; collisionIt < collisionNum; ++collisionIt)
			{
				contact[collisionIt].rA[0]   += shapeProxyPositionA;
				contact[collisionIt].rA[1]   += shapeProxyPositionA;
				contact[collisionIt].objectA  = shapeProxyAIt;
				contact[collisionIt].rB[0]   += shapeProxyPositionB;
				contact[collisionIt].rB[1]   += shapeProxyPositionB;
				contact[collisionIt].objectB  = shapeProxyBIt;
			}
		}
	}

	return collisionNum;
}

/*uint32_t Sat2d::penetrationPathShape(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	Path2d*       path;
	Shape2d*      shape;
	jaTransform2* pathTransform;
	jaTransform2* shapeTransform;

	bool switched = false;
	if (a->getType() == (uint8_t)ShapeType::Path) // swap handles A=Path2d B=Shape
	{
		path  = (Path2d*)a;
		shape = (Shape2d*)b;
		pathTransform  = (jaTransform2*)&transformA;
		shapeTransform = (jaTransform2*)&transformB;	
	}
	else
	{
		shape = (Shape2d*)a;
		path  = (Path2d*)b;
		shapeTransform = (jaTransform2*)&transformA;
		pathTransform  = (jaTransform2*)&transformB;
		switched = true;
	}
	
	uint16_t pathVerticesNum = (uint16_t)path->getVerticesNum();
	uint16_t lineSegmentsNum = (uint16_t)pathVerticesNum - 1;
	
	//uint8_t searchIndex;

	jaVector2 triangleWorldPoint;
	jaVector2 segmentWorldPoints[2];
	jaVector2 lineSegment;
	jaVector2 middleWorldPoint;
	jaVector2 searchDirection;
	jaVector2 invSearchDirection;
	jaVector2 normal;
	jaVector2 triangleSide0;
	jaVector2 triangleSide1;
	//jaVector2 normalTriangleSide0;
	//jaVector2 normalTriangleSide1;
	jaVector2 foundTriangle[3];
	jaVector2 foundWorldTriangle[3];
	jaMatrix2 invRotShape = jaVectormath2::inverse(shapeTransform->rotationMatrix);

	path->getVertex(0, segmentWorldPoints[0]);
	segmentWorldPoints[0] = *pathTransform * segmentWorldPoints[0];

	uint32_t collisionNum = 0;

	for (uint16_t i = 1; i < pathVerticesNum; ++i)
	{
		path->getVertex(i, segmentWorldPoints[1]);
		segmentWorldPoints[1] = *pathTransform * segmentWorldPoints[1];

		lineSegment = segmentWorldPoints[1] - segmentWorldPoints[0];

		middleWorldPoint.x = (lineSegment.x * 0.5f) + segmentWorldPoints[0].x;
		middleWorldPoint.y = (lineSegment.y * 0.5f) + segmentWorldPoints[0].y;

		searchDirection.x = middleWorldPoint.x - shapeTransform->position.x;
		searchDirection.y = middleWorldPoint.y - shapeTransform->position.y;

		searchDirection.normalize();
		invSearchDirection = invRotShape * searchDirection;

		uint16_t searchIndex = shape->findSupportPointIndex(invSearchDirection);
		shape->getSupportTriangle(searchIndex, foundTriangle);

		foundWorldTriangle[0] = *shapeTransform * foundTriangle[0];
		foundWorldTriangle[1] = *shapeTransform * foundTriangle[1];
		foundWorldTriangle[2] = *shapeTransform * foundTriangle[2];

		//first check if shape is in intersection region
		{
			jaFloat segmentA  = jaVectormath2::projection(lineSegment, segmentWorldPoints[0]);
			jaFloat segmentB  = jaVectormath2::projection(lineSegment, segmentWorldPoints[1]);

			jaFloat pointProj0 = jaVectormath2::projection(lineSegment, foundWorldTriangle[0]);
			jaFloat pointProj1 = jaVectormath2::projection(lineSegment, foundWorldTriangle[1]);
			jaFloat pointProj2 = jaVectormath2::projection(lineSegment, foundWorldTriangle[2]);

			//jaFloat temp1 = pointProj - segmentA;
			//jaFloat temp2 = pointProj - segmentB;
			
			jaFloat lSegment;
			jaFloat hSegment;

			if (segmentA < segmentB)
			{
				lSegment = segmentA;
				hSegment = segmentB;
			}

			jaFloat lPointProj;
			jaFloat hPointProj;

			if (pointProj1 < pointProj2)
			{
				if (pointProj0 < pointProj1)
				{
					lPointProj = pointProj0;
					hPointProj = pointProj2;
				}
				else
				{
					lPointProj = pointProj1;
					if (pointProj2 > pointProj0)
						hPointProj = pointProj2;
					else
						hPointProj = pointProj0;
				}
			}
			else
			{
				if (pointProj2 < pointProj1)
				{
					lPointProj = pointProj2;
					if (pointProj1 > pointProj0)
						hPointProj = pointProj1;
					else
						hPointProj = pointProj0;
				}
				else
				{
					lPointProj = pointProj1;
					hPointProj = pointProj2;
				}
			}

			//if (((pointProj - segmentA) * (segmentB - pointProj)) > 0.0f)
			{
				// inside segment

				normal = jaVectormath2::perpendicular(lineSegment);
				normal.normalize();

				

				jaFloat normalSign = jaVectormath2::projection(searchDirection, normal);

				jaFloat worldPointProj = jaVectormath2::projection(normal, foundWorldTriangle[1] - middleWorldPoint);
				//jaFloat worldSegmentProjA = jaVectormath2::projection(normal, segmentWorldPoints[0]);
				//jaFloat worldSegmentProjB = jaVectormath2::projection(normal, segmentWorldPoints[1]);

				if ((normalSign * worldPointProj) > 0.0f)
				{
					++collisionNum;
					//There is collision

					//Choose best minimum translation distance
				}

				//normalTriangleSide0 = foundTriangle[1] - foundTriangle[0];
				//normalTriangleSide1 = foundTriangle[2] - foundTriangle[1];
			}
		}

		segmentWorldPoints[0] = segmentWorldPoints[1];
	}
	
	
	//if (switched)
	//{
	//	ABdiff.x = -ABdiff.x;
	//	ABdiff.y = -ABdiff.y;
	//
	//	contact->rA[0] = boxTransform->rotationMatrix * circlePoint;
	//	contact->rB[0] = boxTransform->rotationMatrix * boxPoint;
	//}
	//else
	//{
	//	contact->rA[0] = boxTransform->rotationMatrix * boxPoint;
	//	contact->rB[0] = boxTransform->rotationMatrix * circlePoint;
	//}
	//contact->normal = ABdiff + contact->rB[0] - contact->rA[0];
	//
	//contact->pointNum = 1;
	//contact->depth = contact->normal.normalize();
	//contact->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + 0;
	//
	//if (jaVectormath2::projection(ABdiff, contact->normal) < GIL_REAL(0.0))
	//{
	//	contact->normal = -contact->normal;
	//}

	return collisionNum;
}*/