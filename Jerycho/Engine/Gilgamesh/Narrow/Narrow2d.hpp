#pragma  once

#include "../../jaSetup.hpp"
#include "../ObjectHandle2d.hpp"
#include "../../Math/VectorMath2d.hpp"

namespace gil
{
	typedef uint16_t HashType;

	class Contact2d
	{
	public:
		ObjectHandle2d* objectA; // object A is handle with lower id
		ObjectHandle2d* objectB; // object B is handle with higher id
		jaVector2 rA[2]; // vector from center of mass of bodyA to collision point on body a 
		jaVector2 rB[2]; // vector from center of mass of bodyB to collision point on body b 

		jaVector2 normal; // Normal penetration vector from objectA to objectB
		jaFloat   depth;

		HashType hash;
		uint8_t  pointNum;



		uint16_t getLowerIdHandle()
		{
			return ((objectA->id < objectB->id) ? objectA->id : objectB->id);
		}

		inline bool isSensor()
		{
			return false;
		}

		void getHandleId(uint16_t& lowerId, uint16_t& higherId)
		{
			lowerId  = objectA->id;
			higherId = objectB->id;
		}
	};

	class RayContact2d
	{
		ObjectHandle2d* object;
		jaVector2       contactPoint;
	};

	class CollisionDataPairMultithread
	{
	public:
		ObjectHandle2d* handleA;
		ObjectHandle2d* handleB;
		Contact2d* contacts;
		uint32_t   contacsNum;

		int8_t padding[48];
		//gilContact2d contact;
	};

}
