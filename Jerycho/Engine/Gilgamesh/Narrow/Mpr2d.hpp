#pragma once

#include "../../Math/VectorMath2d.hpp"
#include "../ObjectHandle2d.hpp"
#include "../Narrow/Narrow2d.hpp"

namespace gil
{
	class Narrow2dConfiguration;
	namespace Mpr2d
	{
		jaVector2 outsidePortal(jaVector2& v1, jaVector2& v2);
		jaVector2 insidePortal(jaVector2& v1, jaVector2& v2);
		bool      originInTriangle(jaVector2& a, jaVector2& b, jaVector2& c);
		bool      intersectPortal(jaVector2& b, jaVector2& c, jaVector2& d);
		uint32_t  penetrationShapeShape(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
	}

}
