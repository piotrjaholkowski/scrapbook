#pragma once

#include "../Narrow/Narrow2d.hpp"

namespace gil
{
	class Narrow2dConfiguration;

	namespace Points2d
	{
		uint32_t intersectionParticlesShape(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t intersectionParticlesParticles(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
	}

}
