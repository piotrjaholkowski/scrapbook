#pragma once

#include "../Narrow/Narrow2d.hpp"

namespace gil
{
	class Narrow2dConfiguration;

	namespace Sat2d {
		const jaVector2 axis[2] = { jaVector2(1, 0), jaVector2(0, 1) }; // for penetrationBoxBox normal axis

		inline void switchValues(jaFloat& a, jaFloat& b)
		{
			jaFloat temp = a;
			a = b;
			b = temp;
		}

		inline jaFloat calculateInterval(jaFloat minA, jaFloat minB, jaFloat maxA, jaFloat maxB)
		{
			if (minA<minB)
			{
				if (maxA>minB)
				{
					return (maxA - minB);
				}
				else
				{
					return -1.0f;
				}
			}
			else
			{
				if (maxB > minA)
				{
					return (maxB - minA);
				}
				else
				{
					return -1.0f;
				}
			}
		}


		static void clipPoint(jaVector2& clipPoint, jaVector2& clipNormal, jaVector2* point);
		static bool intersectionSegment(const jaVector2& a, const jaVector2& b, const jaVector2& c, const jaVector2& d, jaVector2& intersectionPoint);
		static void getSupportTriangle(const Shape2d* shape, const jaVector2& supportDir, const jaVector2& searchPoint, jaVector2* supportTriangle, uint16_t& supportPointIndex);
		static void getSupportTriangle(const jaVector2* triangleVertices, const jaVector2& supportDir, const jaVector2& searchPoint, jaVector2* supportTriangle, uint8_t& supportPointIndex);

		uint32_t penetrationConvexPolygonConvexPolygon(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t penetrationCircleCircle(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t penetrationConvexPolygonPoint(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t penetrationCirclePoint(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t penetrationConvexPolygonCircle(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t penetraionTriangleMeshCircle(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t penetrationTriangleMeshTriangleMesh(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t penetrationTriangleMeshConvexPolygon(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t penetrationMultiShapeShape(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t penetrationMultiShapeMultiShape(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
		uint32_t penetrationPathShape(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf);
	}

}
