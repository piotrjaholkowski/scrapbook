#include "Mpr2d.hpp"
#include "../../Math/VectorMath2d.hpp"

using namespace gil;

jaVector2 Mpr2d::outsidePortal(jaVector2& v1, jaVector2& v2)
{
	jaFloat dir = jaVectormath2::det(v1,v2);

	// -y,x | y,-x
	if (dir < GILEPS) return jaVector2(-v1.y + v2.y, v1.x - v2.x);
	else return jaVector2(v1.y - v2.y, -v1.x + v2.x);
}

jaVector2 Mpr2d::insidePortal(jaVector2& v1, jaVector2& v2)
{
	float dir = jaVectormath2::det(v1,v2);

	if (dir > GILEPS) return jaVector2(-v1.y + v2.y, v1.x - v2.x);
	else return jaVector2(v1.y - v2.y, -v1.x + v2.x);
}

bool Mpr2d::originInTriangle(jaVector2& a, jaVector2& b, jaVector2& c)
{
	jaVector2 ab = b - a;
	jaVector2 bc = c - b;
	jaVector2 ca = a - c;

	jaFloat pab = jaVectormath2::det(-a,ab);
	jaFloat pbc = jaVectormath2::det(-b,bc);
	bool sameSign = (((pab > 0) - (pab < 0)) == ((pbc > 0) - (pbc < 0)));
	if (!sameSign) return false;

	jaFloat pca = jaVectormath2::det(-c,ca);
	sameSign = (((pab > 0) - (pab < 0)) == ((pca > 0) - (pca < 0)));
	if (!sameSign) return false;

	return true;
}

bool Mpr2d::intersectPortal(jaVector2& b, jaVector2& c, jaVector2& d)
{

	jaFloat a1 = (-d.x) * (b.y - d.y) - (-d.y) * (b.x - d.x);
	jaFloat a2 = (-c.x) * (b.y - c.y) - (-c.y) * (b.x - c.x);

	if (a1 != 0.0f && a2 != 0.0f && a1 * a2 < 0.0f) {
		jaFloat a3 = (c.x) * (d.y) - (c.y ) * (d.x);
		jaFloat a4 = a3 + a2 - a1;
		if (a3 != 0.0f && a4 != 0.0f && a3 * a4 < 0.0f) return true;
	}

	// Segments not intersecting (or collinear)
	return false;
}

uint32_t Mpr2d::penetrationShapeShape(const Shape2d* a, const jaTransform2& transformA, const Shape2d* b, const jaTransform2& transformB, Contact2d* contact, uint16_t& hashSeed, const Narrow2dConfiguration& conf)
{
	hashSeed++;
	const Shape2d* shapeA = a;
	const Shape2d* shapeB = b;

	const jaMatrix2& invRotA = jaVectormath2::inverse(transformA.rotationMatrix);
	const jaMatrix2& invRotB = jaVectormath2::inverse(transformB.rotationMatrix);
	const jaTransform2 relTransform = jaTransform2(invRotA * transformA.position - transformA.position, transformB.rotationMatrix * invRotA);
	const jaMatrix2 relInvRot = jaVectormath2::inverse(relTransform.rotationMatrix);

	jaVector2 v0 = transformB.position - transformA.position;
	if (v0.equalsZero()) v0.x = GIL_REAL(0.00001);

	jaVector2 supportDir = jaVectormath2::normalize(-v0);
	jaVector2 v11 = transformA * shapeA->findSupportPoint(invRotA * -supportDir);
	jaVector2 v12 = transformB * shapeB->findSupportPoint(invRotB * supportDir);
	jaVector2 v1 = v12 - v11;

	if (jaVectormath2::projection(v1, supportDir) <= GIL_ZERO)
		return 0;

	supportDir = outsidePortal(v1, v0);
	supportDir.normalize();
	jaVector2 v21 = transformA * shapeA->findSupportPoint(invRotA * -supportDir);
	jaVector2 v22 = transformB * shapeB->findSupportPoint(invRotB * supportDir);
	jaVector2 v2 = v22 - v21;

	if (jaVectormath2::projection(v2, supportDir) <= GIL_ZERO)
		return 0;

	jaVector2 v31,v32,v3;
	jaVector2 normal,ab;
	jaFloat   t,s,denom;
	jaVector2 point1,point2;
	int32_t   maxIterations = 0;
	while (1) {
		// Find normal direction
		if (!intersectPortal(v0, v2, v1)) {
			// Origin lies inside the portal
			supportDir = insidePortal(v2, v1);
		} else {
			// Origin lies outside the portal
			supportDir = outsidePortal(v2, v1);
		}

		// Obtain the next support point
		supportDir.normalize();
		v31 = transformA * shapeA->findSupportPoint(invRotA * -supportDir);
		v32 = transformB * shapeB->findSupportPoint(invRotB * supportDir);
		v3  = v32 - v31;
		
		if (jaVectormath2::projection(v3, supportDir) <= 0)
			return 0;

		// Finished searching	
		if (jaVectormath2::projection(v3 - v2, supportDir) <= GILEPS || maxIterations++ > 10) {
			ab = v2 - v1;
			t = -jaVectormath2::projection(v1, ab);

			if (t <= GIL_ZERO) {
				t = GIL_ZERO;
				normal = v1;
			} else {
				denom = jaVectormath2::projection(ab, ab);
				if (t >= denom) {
					normal = v2;
					t = GIL_ONE;
				} else {
					t /= denom;
					normal = v1 + t * ab;
				}
			}

			s = GIL_ONE - t;

			point1 = s * v11 + t * v21;
			point2 = s * v12 + t * v22;

			contact->depth = normal.normalize();
			contact->normal = normal;
			contact->rA[0] = point1 - transformA.position;
			contact->rB[0] = point2 - transformB.position;
			contact->hash = (hashSeed * ja::setup::gilgamesh::CONTACT_HASH_SPACE) + 0;
			contact->pointNum = 1;

			return 1;
		}

		// If origin is inside (v1,v0,v3), We have a hit!
		if (originInTriangle(v0, v1, v3)) {
			v2  = v3;
			v21 = v31;
			v22 = v32;
			continue;
		}
		// If origin is inside (v3,v0,v2), we have a hit!
		else if (originInTriangle(v0, v2, v3)) {
			v1  = v3;
			v11 = v31;
			v12 = v32;
			continue;
		}

		return 0;
	}

}