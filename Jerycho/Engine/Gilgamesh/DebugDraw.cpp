#include "DebugDraw.hpp"

namespace gil
{

//gilDebug::drawShapeCallback gilDebug::drawShapeFunc = { gilDebug::drawBox2d, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };
void (*Debug::drawShapeFunc[10])(const Shape2d*, const jaTransform2&) = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
Debug::DebugInit initFunctions;

Debug::DebugInit::DebugInit()
{
	Debug::drawShapeFunc[(uint8_t)ShapeType::Box]           = Debug::drawBox2d;
	Debug::drawShapeFunc[(uint8_t)ShapeType::Circle]        = Debug::drawCircle2d;
	Debug::drawShapeFunc[(uint8_t)ShapeType::ConvexPolygon] = Debug::drawConvexPolygon2d;
	Debug::drawShapeFunc[(uint8_t)ShapeType::Point]         = Debug::drawPoint2d;
	Debug::drawShapeFunc[(uint8_t)ShapeType::MultiShape]    = Debug::drawMultiShape2d;
	//Debug::drawShapeFunc[(uint8_t)ShapeType::Particles]     = Debug::drawParticles2d;
	//Debug::drawShapeFunc[(uint8_t)ShapeType::Path]          = Debug::drawPath2d;
	Debug::drawShapeFunc[(uint8_t)ShapeType::TriangleMesh]  = Debug::drawTriangleMesh2d;
}

}