#pragma once

#include "../../Math/VectorMath2d.hpp"

namespace gil
{

	class AABB2d// : public gilBoundingVolume2d
	{
	public:
		jaVector2 min;
		jaVector2 max;

		inline AABB2d();
		inline AABB2d(jaVector2& min, jaVector2& max);
		inline jaVector2 getCenter() const;
		inline void getSize(float& width, float& height) const;

		static inline AABB2d make(jaVector2& p0, jaVector2& p1);
		static inline bool   intersect(const AABB2d& a, const AABB2d& b);
		static inline bool   intersect(const jaVector2& p, const AABB2d& aabb);
		static inline void   getUnion(const AABB2d& A, const AABB2d& B, AABB2d& unionAB);
		static inline void   getSum(const AABB2d& A, const AABB2d& B, AABB2d& AB);
	
		inline bool isPointInside(const jaVector2& point) const;
	};

	AABB2d::AABB2d()
	{

	}

	AABB2d::AABB2d(jaVector2 &min, jaVector2 &max)
	{
		this->min = min;
		this->max = max;
	}

	jaVector2 AABB2d::getCenter() const
	{
		return this->min + ((this->max - this->min) * 0.5f);
	}

	void AABB2d::getSize(float& width, float& height) const
	{
		width  = this->max.x - this->min.x;
		height = this->max.y - this->min.y;
	}

	AABB2d AABB2d::make(jaVector2& p0, jaVector2& p1)
	{
		AABB2d aabb;

		if (p1.x < p0.x)
		{
			aabb.min.x = p1.x;
			aabb.max.x = p0.x;
		}
		else
		{
			aabb.min.x = p0.x;
			aabb.max.x = p1.x;
		}

		if (p1.y < p0.y)
		{
			aabb.min.y = p1.y;
			aabb.max.y = p0.y;
		}
		else
		{
			aabb.min.y = p1.y;
			aabb.max.y = p0.y;
		}

		return aabb;
	}

	bool AABB2d::intersect(const AABB2d& a, const AABB2d& b)
	{
		if (a.max.x < b.min.x) return false;
		if (a.min.x > b.max.x) return false;
		if (a.max.y < b.min.y) return false;
		if (a.min.y > b.max.y) return false;
		return true;
	}

	bool AABB2d::intersect(const jaVector2& point, const AABB2d& aabb)
	{
		if (aabb.max.x < point.x) return false;
		if (aabb.min.x > point.x) return false;
		if (aabb.max.y < point.y) return false;
		if (aabb.min.y > point.y) return false;
		return true;
	}

	void AABB2d::getSum(const AABB2d& A, const AABB2d& B, AABB2d& AB)
	{
		if (A.min.x < B.min.x)
			AB.min.x = A.min.x;
		else
			AB.min.x = B.min.x;

		if (A.min.y < B.min.y)
			AB.min.y = A.min.y;
		else
			AB.min.y = B.min.y;

		if (A.max.x > B.max.x)
			AB.max.x = A.max.x;
		else
			AB.max.x = B.max.x;

		if (A.max.y > B.max.y)
			AB.max.y = A.max.y;
		else
			AB.max.y = B.max.y;
	}

	void AABB2d::getUnion(const AABB2d& A, const AABB2d& B, AABB2d& unionAB)
	{
		if (A.min.x > B.min.x)
			unionAB.min.x = A.min.x;
		else
			unionAB.min.x = B.min.x;

		if (A.min.y > B.min.y)
			unionAB.min.y = A.min.y;
		else
			unionAB.min.y = B.min.y;

		if (A.max.x < B.max.x)
			unionAB.max.x = A.max.x;
		else
			unionAB.max.x = B.max.x;

		if (A.max.y < B.max.y)
			unionAB.max.y = A.max.y;
		else
			unionAB.max.y = B.max.y;
	}

	bool AABB2d::isPointInside(const jaVector2& point) const
	{
		if (point.x < this->min.x)
			return false;
		if (point.x > this->max.x)
			return false;
		if (point.y < this->min.y)
			return false;
		if (point.y > this->max.y)
			return false;
		return true;
	}

}
