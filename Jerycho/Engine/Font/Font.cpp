#include "Font.hpp"

#include "FontPoint.hpp"
#include "FontContour.hpp"
#include "../Graphics/Triangulator.hpp"
#include "../Utility/Exception.hpp"
#include "../Manager/LogManager.hpp"
#include "../Manager/DisplayManager.hpp"

namespace ja
{ 

uint32_t next_po2( uint32_t a)
{
	uint32_t rval=2;
	while (rval<a)
		rval<<=1;
	return rval;
}

bool FontLoader::loadFont(const char* fontFile, uint32_t fontSize, FontEncode encode, Font* font)
{
	FT_Face    face;
	FT_Library library;

	font->encode               = encode;
	font->highestPosition      = 0;
	font->lowestPosition       = 0;
	font->widthestCharacerSize = 0;

	
	font->texture = new GLuint[(uint32_t)encode];
	font->letter  = new Letter[(uint32_t)encode];

	if(font->texture==nullptr || font->letter==nullptr)
	{
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't load %s out of memory", fontFile);
		return false;
	} 

	if(FT_Init_FreeType(&library)!=0)
	{
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't load %s FT_Init_FreeType fail", fontFile);
		return false;
	}
	
	if(FT_New_Face( library, fontFile, 0, &face)!=0)
	{
		FT_Done_FreeType(library);
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't load %s FT_New_Face fail", fontFile);
		return false;
	}

	if(FT_Set_Char_Size(face, fontSize*64, fontSize*64, 96, 96)!=0)
	{
		FT_Done_Face(face);
		FT_Done_FreeType(library);
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't load %s FT_Set_Char_Size fail", fontFile);
		return false;
	}

	glGenTextures((uint32_t)encode, font->texture);
	font->list_base = glGenLists((uint32_t)encode);
	if(font->list_base==GL_OUT_OF_MEMORY || *font->texture==GL_OUT_OF_MEMORY)
	{
		FT_Done_Face(face);
		FT_Done_FreeType(library);
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't load %s glGenTextures fail", fontFile);
		return false;
	}

	for (uint16_t z = 0; z<(uint16_t)encode; z++)
	{
		createCharacter(face,z,font->texture[z],font->letter[z],encode);
		if((font->letter[z].top + (int32_t)font->letter[z].bh) > font->highestPosition)
			font->highestPosition = font->letter[z].top + font->letter[z].bh;
		if(font->letter[z].top < font->lowestPosition)
			font->lowestPosition = font->letter[z].top;
		
		if(font->letter[z].advanceX > font->widthestCharacerSize)
			font->widthestCharacerSize = font->letter[z].advanceX;

		glNewList(font->list_base+z,GL_COMPILE);
	
		glBindTexture(GL_TEXTURE_2D,font->texture[z]);
		glBegin(GL_QUADS);
		glTexCoord2f(0,0);
		glVertex2i(font->letter[z].left,font->letter[z].top + font->letter[z].bh);
		glTexCoord2f(0,font->letter[z].ty);
		glVertex2i(font->letter[z].left,font->letter[z].top);
		glTexCoord2f(font->letter[z].tx,font->letter[z].ty);
		glVertex2i(font->letter[z].left + font->letter[z].bw,font->letter[z].top);
		glTexCoord2f(font->letter[z].tx,0);
		glVertex2i(font->letter[z].left + font->letter[z].bw,font->letter[z].top + font->letter[z].bh);
		glEnd();
		
		glTranslatef(font->letter[z].advanceX,0.0f,0.0f);

		glEndList();
	}

	font->highestCharacterSize = -font->lowestPosition + font->highestPosition;
	FT_Done_Face(face);
	FT_Done_FreeType(library);

	return true;
}

bool FontLoader::loadFontPolygon(const char* fontFile, FontEncode encode, FontPolygon* font)
{
	FT_Face    face;
	FT_Library library;

	font->encode                = encode;
	font->highestPosition       = 0;
	font->lowestPosition        = 0;
	font->widthestCharacterSize = 0;

	font->letter = new LetterPolygon[(uint32_t)encode];


	if (FT_Init_FreeType(&library) != 0)
	{
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't load %s FT_Init_FreeType fail", fontFile);
		return false;
	}

	if (FT_New_Face(library, fontFile, 0, &face) != 0)
	{
		FT_Done_FreeType(library);
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't load %s FT_New_Face fail", fontFile);
		return false;
	}

	for (uint16_t z = 0; z<(uint16_t)encode; z++)
	{
		createCharacterPolygon(face, z, font->letter[z], encode);
		
		if (font->letter[z].top > font->highestPosition)
			font->highestPosition = font->letter[z].top;
		
		if (font->letter[z].bottom < font->lowestPosition)
			font->lowestPosition = font->letter[z].bottom;

		if (font->letter[z].advanceX > font->widthestCharacterSize)
			font->widthestCharacterSize = font->letter[z].advanceX;
	}

	font->highestCharacterSize = -font->lowestPosition + font->highestPosition;
	FT_Done_Face(face);
	FT_Done_FreeType(library);

	return true;
}

void FontLoader::createCharacter(FT_Face face,uint16_t z, uint32_t& texture, Letter& letter,FontEncode encode)
{
	if(encode==FontEncode::EXTENDED_ASCII_STANDARD)
	{
		switch(z)
		{
		case 211:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x00d3), FT_LOAD_NO_HINTING);
				break;
			}
		case 243:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x00f3), FT_LOAD_NO_HINTING);
				break;
			}
		case 175:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x017b), FT_LOAD_NO_HINTING);
				break;
			}
		case 191:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x017c), FT_LOAD_NO_HINTING);
				break;
			}
		case 143:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0179), FT_LOAD_NO_HINTING);
				break;
			}
		case 159:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x017A), FT_LOAD_NO_HINTING);
				break;
			}
		case 209:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0143), FT_LOAD_NO_HINTING);
				break;
			}
		case 241:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0144), FT_LOAD_NO_HINTING);
				break;
			}
		case 163:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0141), FT_LOAD_NO_HINTING);
				break;
			}
		case 179:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0142), FT_LOAD_NO_HINTING);
				break;
			}
		case 198:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0106), FT_LOAD_NO_HINTING);
				break;
			}
		case 230:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0107), FT_LOAD_NO_HINTING);
				break;
			}
		case 202:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0118), FT_LOAD_NO_HINTING);
				break;
			}
		case 234:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0119), FT_LOAD_NO_HINTING);
				break;
			}
		case 165:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0104), FT_LOAD_NO_HINTING);
				break;
			}
		case 185:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0105), FT_LOAD_NO_HINTING);
				break;
			}
		case 140:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x015a), FT_LOAD_NO_HINTING);
				break;
			}
		case 156:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x015b), FT_LOAD_NO_HINTING);
				break;
			}
		default:
			{
				FT_Load_Glyph(face, FT_Get_Char_Index(face, z), FT_LOAD_NO_HINTING);
				break;	
			}
		}
	}
	else
	{
		//FT_Load_Glyph( face, FT_Get_Char_Index(face, z), 0); // It leaves artifacts 
		//FT_Load_Glyph( face, FT_Get_Char_Index(face, z), FT_LOAD_NO_SCALE); 
		FT_Load_Glyph( face, FT_Get_Char_Index(face, z), FT_LOAD_NO_HINTING); 
	}
	FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL); //LIGHT, FT_RENDER_MODE_NORMAL

	uint32_t h=next_po2(face->glyph->bitmap.rows);
	uint32_t w=next_po2(face->glyph->bitmap.width);

	letter.tx = (float)(face->glyph->bitmap.width) / (float)(w);
	letter.ty = (float)(face->glyph->bitmap.rows)  / (float)(h);

	letter.bh       = face->glyph->bitmap.rows;
	letter.bw       = face->glyph->bitmap.width;
	letter.advanceX = face->glyph->advance.x >> 6;
	letter.advanceY = face->glyph->advance.y >> 6;
	letter.top      =-(face->glyph->bitmap.rows - face->glyph->bitmap_top);
	letter.left     =face->glyph->bitmap_left;
	
	uint8_t* expanded_data = new uint8_t[h*w];
	uint8_t* bmp=face->glyph->bitmap.buffer;

	for( uint32_t j=0; j < h; j++ ) 
	{
		for( uint32_t i=0; i < w; i++) 
		{
			expanded_data[i+(j*w)] =  (i>=letter.bw || j>=letter.bh) ? 0 : bmp[i+(letter.bw*j)];
		}		
	}

	glBindTexture(GL_TEXTURE_2D, texture);
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);	
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST); // it repairs seams
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, w, h, 0, GL_ALPHA, GL_UNSIGNED_BYTE, expanded_data);
	delete [] expanded_data;
}

void FontLoader::createCharacterPolygon(FT_Face face, uint16_t z, LetterPolygon& letter, FontEncode encode)
{
	if (encode == FontEncode::EXTENDED_ASCII_STANDARD)
	{
		switch (z)
		{
		case 211:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x00d3), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 243:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x00f3), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 175:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x017b), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 191:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x017c), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 143:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0179), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 159:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x017A), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 209:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0143), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 241:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0144), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 163:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0141), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 179:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0142), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 198:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0106), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 230:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0107), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 202:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0118), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 234:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0119), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 165:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0104), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 185:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x0105), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 140:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x015a), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		case 156:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, 0x015b), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		default:
		{
			FT_Load_Glyph(face, FT_Get_Char_Index(face, z), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
			break;
		}
		}
	}
	else
	{
		FT_Load_Glyph(face, FT_Get_Char_Index(face, z), FT_LOAD_NO_SCALE | FT_LOAD_LINEAR_DESIGN);
	}

	const float unitsPerEmF = (float)face->units_per_EM;
	face->glyph->linearHoriAdvance;

	letter.advanceX = ((float)face->glyph->advance.x) / unitsPerEmF; 
	letter.advanceY = ((float)face->glyph->advance.y) / unitsPerEmF;

	FT_Outline*   pOutline    = &face->glyph->outline;
	const int16_t contoursNum = pOutline->n_contours;
	int16_t       startIndex  = 0;
	int16_t       endIndex    = 0;

	FontContour** pContourList = new FontContour*[contoursNum];

	for (int16_t i = 0; i < contoursNum; ++i)
	{
		FT_Vector* pointList = &pOutline->points[startIndex];
		char*      tagList   = &pOutline->tags[startIndex];

		endIndex = pOutline->contours[i];
		int16_t contourLength = (endIndex - startIndex) + 1;

		FontContour* contour = new FontContour(pointList, tagList, contourLength, unitsPerEmF);

		pContourList[i] = contour;

		startIndex = endIndex + 1;
	}


	for (int16_t i = 0; i < contoursNum; i++)
	{
		FontContour *c1 = pContourList[i];

		FontPoint leftMost(65536.0, 0.0);

		int16_t    c1PointsNum = c1->points.getSize();
		FontPoint* c1Points    = c1->points.getFirst();
		for (int16_t n = 0; n < c1PointsNum; n++)
		{
			FontPoint p = c1Points[n];

			if (p.x < leftMost.x)
				leftMost = p;
		}


		// 2. Count how many other contours we cross when going further to
		// the left.
		int32_t parity = 0;

		for (int16_t j = 0; j < contoursNum; j++)
		{
			if (j == i)
			{
				continue;
			}

			FontContour *c2          = pContourList[j];
			int16_t      c2PointsNum = c2->points.getSize();
			FontPoint*   c2Points    = c2->points.getFirst();

			for (int16_t n = 0; n < c2PointsNum; n++)
			{
				FontPoint p1 = c2Points[n];
				FontPoint p2 = c2Points[((n + 1) % c2PointsNum)];

				/* FIXME: combinations of >= > <= and < do not seem stable */
				if ((p1.y < leftMost.y && p2.y < leftMost.y)
					|| (p1.y >= leftMost.y && p2.y >= leftMost.y)
					|| (p1.x > leftMost.x && p2.x > leftMost.x))
				{
					continue;
				}
				else if (p1.x < leftMost.x && p2.x < leftMost.x)
				{
					parity++;
				}
				else
				{
					FontPoint a = p1 - leftMost;
					FontPoint b = p2 - leftMost;
					if (b.x * a.y > b.y * a.x)
					{
						parity++;
					}
				}
			}
		}

		// 3. Make sure the glyph has the proper parity.
		c1->setParity(parity);
	}
	
	{
		const double outsetSize = 0.0;

		letter.top    = -FLT_MAX;
		letter.bottom = FLT_MAX;
		letter.left   = FLT_MAX;
		letter.right  = -FLT_MANT_DIG;

		Triangulator triangulator;

		// no need to triangulate empty shape
		if (contoursNum>0)
		{		
			int32_t contourFlag = face->glyph->outline.flags;

			if (contourFlag & ft_outline_even_odd_fill) // ft_outline_reverse_fill
			{
				triangulator.setWindingRule(TessWindingRule::WINDING_ODD);
			}
			else
			{
				triangulator.setWindingRule(TessWindingRule::WINDING_NONZERO);
			}

			triangulator.beginTriangulation();

			// add each contour to triangulator helper
			for (int16_t i = 0; i<contoursNum; i++)
			{
				pContourList[i]->buildFrontOutset(outsetSize);

				const uint32_t   pointsNum = pContourList[i]->frontPoints.getSize();
				const FontPoint* pPoints   = pContourList[i]->frontPoints.getFirst();

				if (pointsNum >= 3)
				{
					triangulator.beginContour();
					
					for (uint32_t j = 0; j<pointsNum; ++j)
					{
						const double dX = (double)pPoints[j].x; 
						const double dY = (double)pPoints[j].y; 
						const float  fX = (float)dX;
						const float  fY = (float)dY;
						triangulator.addVertex( jaVector2(dX, dY) );

						if (fX < letter.left)
							letter.left = fX;

						if (fX > letter.right)
							letter.right = fX;

						if (fY > letter.top)
							letter.top = fY;

						if (fY < letter.bottom)
							letter.bottom = fY;
					}
			
					triangulator.endContour();
				}
			}
			
			triangulator.endTriangulation();
		}

		StackAllocator<jaVector2>* meshVertices = triangulator.getVertices();
		StackAllocator<uint32_t>*  meshIndices  = triangulator.getIndices();
	
		letter.verticesNum = meshVertices->getSize();
		letter.indicesNum  = meshIndices->getSize();
		letter.vertices    = new jaVector2[letter.verticesNum];
		letter.indices     = new uint32_t[letter.indicesNum];

		memcpy(letter.vertices, meshVertices->getFirst(), meshVertices->getSize() * sizeof(jaVector2));
		memcpy(letter.indices,  meshIndices->getFirst(),  meshIndices->getSize() * sizeof(uint32_t));
	}

	for (int16_t i = 0; i < contoursNum; ++i)
	{
		delete pContourList[i];
	}

	delete[] pContourList;
}

Font::Font(uint32_t resourceId) : resourceId(resourceId)
{
	this->texture = nullptr;
	this->letter  = nullptr;
}

FontPolygon::FontPolygon(uint32_t resourceId) : resourceId(resourceId)
{
	this->letter = nullptr;
}

Font::~Font()
{
	clear();
}

FontPolygon::~FontPolygon()
{
	clear();
}

void Font::clear()
{
	glDeleteTextures((uint32_t)encode, texture);
	glDeleteLists(list_base, (uint32_t)encode);

	if (nullptr != texture)
		delete[] texture;

	if (nullptr != letter)
		delete[] letter;

	texture = nullptr;
	letter  = nullptr;
}

void FontPolygon::clear()
{
	if (nullptr != letter)
		delete[] letter;

	letter = nullptr;
}

void Font::setColor(uint8_t r, uint8_t g, uint8_t b)
{
	color.r=r;
	color.g=g;
	color.b=b;
}

void Font::getTextSize(int32_t& width, int32_t& height, wchar_t* text) const
{
	int32_t text_width  = 0;
	int32_t text_height = 0;
	width  = 0;
	height = 0;

	for(const wchar_t *c=text;*c;c++)
	{
		if(*c=='\n')
		{
			text_height++;
			if(text_width > width)
				width = text_width;
			text_width = 0;
		}
		else
		{
			text_width += letter[*c].advanceX;
		}
	}

	if(text_width > width)
		width = text_width;

	height = this->highestCharacterSize + text_height*this->highestCharacterSize;
}

void Font::getTextSize(int32_t& width, int32_t& height, ja::string& text) const
{
	int32_t text_width = 0;
	int32_t text_height = 0;
	width  = 0;
	height = 0;

	for(const char* c=text.c_str();*c;c++)
	{
		if(*c=='\n')
		{
			text_height++;
			if(text_width > width)
				width = text_width;
			text_width = 0;
		}
		else
		{
			text_width += letter[*c].advanceX;
		}
	}

	if(text_width > width)
		width = text_width;

	height = this->highestCharacterSize + text_height*this->highestCharacterSize;
}

void Font::getLetterIndex(ja::string& text, int32_t cursorPositionX, int32_t& index) const
{
	int32_t width = 0;
	index = 0;

	for(const char* c=text.c_str();*c;c++)
	{
		width += letter[*c].advanceX;
		++index;
		if(width > cursorPositionX)
			break;	
	}
}

}
