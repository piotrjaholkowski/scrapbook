#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H

namespace ja
{

class FontPoint
{
public:

	inline FontPoint() : x(0), y(0), z(0)
	{
		
	}

	inline FontPoint(const double x, const double y) : x(x), y(y), z(0)
	{
	
	}

	inline FontPoint(const FT_Vector& ft_vector, float unitsPerEmF)
	{
		this->x = ((float)ft_vector.x / unitsPerEmF);
		this->y = ((float)ft_vector.y / unitsPerEmF);
	}

	FontPoint Normalise()
	{
		double norm = sqrt(this->x * this->x + this->y * this->y);

		if (norm == 0.0)
		{
			return *this;
		}

		return FontPoint(this->x / norm, this->y / norm);
	}

	inline FontPoint& operator += (const FontPoint& point)
	{
		this->x += point.x;
		this->y += point.y;

		return *this;
	}

	inline FontPoint operator + (const FontPoint& point) const
	{
		return FontPoint(this->x + point.x, this->y + point.y);
	}

	inline FontPoint& operator -= (const FontPoint& point)
	{
		this->x -= point.x;
		this->y -= point.y;

		return *this;
	}

	inline FontPoint operator - (const FontPoint& point) const
	{
		return FontPoint(this->x - point.x, this->y - point.y);
	}

	inline FontPoint operator * (double multiplier) const
	{
		return FontPoint(this->x * multiplier, this->y * multiplier);
	}

	inline friend FontPoint operator * (double multiplier, FontPoint& point)
	{
		return point * multiplier;
	}

	inline friend double operator * (FontPoint &a, FontPoint& b)
	{
		return a.x * b.x + a.y * b.y;
	}

	friend bool operator == (const FontPoint &a, const FontPoint &b);
	friend bool operator != (const FontPoint &a, const FontPoint &b);

public:
	
	double x,y,z;
};

inline bool operator == (const FontPoint &a, const FontPoint &b)
{
	return ( (a.x == b.x) && (a.y == b.y) );
}


}