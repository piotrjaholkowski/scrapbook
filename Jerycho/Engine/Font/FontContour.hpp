#pragma once

#include <ft2build.h>
#include FT_FREETYPE_H

#include "../Allocators/StackAllocator.hpp"
#include "FontPoint.hpp"

#include <stdint.h>

namespace ja
{

class FontContour
{

public:
	bool clockwise;

public:

	FontContour(FT_Vector* contour, char* pointTags, int16_t numberOfPoints, float unitsPerEmF);

	void addPoint(FontPoint point);

	FontPoint computeOutsetPoint(const FontPoint& A, const FontPoint& B, const FontPoint& C);

	void evaluateCubicCurve(FontPoint A, FontPoint B, FontPoint C, FontPoint D);
	void evaluateQuadraticCurve(FontPoint A, FontPoint B, FontPoint C);

	void setParity(int32_t parity);

	void buildFrontOutset(double outsetSize);

	StackAllocator<FontPoint> points;
	StackAllocator<FontPoint> outsetPoints;
	StackAllocator<FontPoint> frontPoints;
};

}