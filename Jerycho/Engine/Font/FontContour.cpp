#include "FontContour.hpp"
#include "../Math/VectorMath2d.hpp"

namespace ja
{

static const uint32_t BEZIER_STEPS = 5;

void FontContour::addPoint(FontPoint point)
{
	if (points.getSize() > 0)
	{
		if ( (point == *points.getLast()) || (point == *points.getFirst()) )
			return;
	}

	points.push(point);
	
}

void FontContour::evaluateCubicCurve(FontPoint A, FontPoint B, FontPoint C, FontPoint D)
{
	for (uint32_t i = 0; i < BEZIER_STEPS; i++)
	{
		double t = (double)(i) / (double)BEZIER_STEPS;

		FontPoint U = (1.0 - t) * A + t * B;
		FontPoint V = (1.0 - t) * B + t * C;
		FontPoint W = (1.0 - t) * C + t * D;

		FontPoint M = (1.0 - t) * U + t * V;
		FontPoint N = (1.0 - t) * V + t * W;

		addPoint((1.0 - t) * M + t * N);
	}
}

void FontContour::evaluateQuadraticCurve(FontPoint A, FontPoint B, FontPoint C)
{
	for (uint32_t i = 1; i < BEZIER_STEPS; i++)
	{
		double t = (double)(i) / (double)BEZIER_STEPS;

		FontPoint U = (1.0 - t) * A + t * B;
		FontPoint V = (1.0 - t) * B + t * C;

		addPoint((1.0 - t) * U + t * V);
	}
}

FontContour::FontContour(FT_Vector* contour, char* tags, int16_t n, float unitsPerEmF)
{
	FontPoint prev, cur(contour[(n - 1) % n],unitsPerEmF), next(contour[0],unitsPerEmF);
	FontPoint a, b = next - cur;
	double olddir, dir = atan2((next - cur).y, (next - cur).x);
	double angle = 0.0;

	// See http://freetype.sourceforge.net/freetype2/docs/glyphs/glyphs-6.html
	// for a full description of FreeType tags.
	for (int16_t i = 0; i < n; i++)
	{
		prev   = cur;
		cur    = next;
		next   = FontPoint(contour[(i + 1) % n],unitsPerEmF); 
		olddir = dir;
		dir    = atan2((next - cur).y, (next - cur).x);

		// Compute our path's new direction.
		double t = dir - olddir;
		if (t < -M_PI)
			t += 2 * M_PI;
		if (t > M_PI)
			t -= 2 * M_PI;
		angle += t;

		// Only process point tags we know.
		if (n < 2 || FT_CURVE_TAG(tags[i]) == FT_Curve_Tag_On)
		{
			addPoint(cur);
		}
		else if (FT_CURVE_TAG(tags[i]) == FT_Curve_Tag_Conic)
		{
			FontPoint prev2 = prev, next2 = next;

			// Previous point is either the real previous point (an "on"
			// point), or the midpoint between the current one and the
			// previous "conic off" point.
			if (FT_CURVE_TAG(tags[(i - 1 + n) % n]) == FT_Curve_Tag_Conic)
			{
				prev2 = (cur + prev) * 0.5;
				addPoint(prev2);
			}

			// Next point is either the real next point or the midpoint.
			if (FT_CURVE_TAG(tags[(i + 1) % n]) == FT_Curve_Tag_Conic)
			{
				next2 = (cur + next) * 0.5;
			}

			evaluateQuadraticCurve(prev2, cur, next2);
		}
		else if (FT_CURVE_TAG(tags[i]) == FT_Curve_Tag_Cubic && FT_CURVE_TAG(tags[(i + 1) % n]) == FT_Curve_Tag_Cubic)
		{
			evaluateCubicCurve(prev, cur, next, FontPoint(contour[(i + 2) % n],unitsPerEmF)); //evaluateCubicCurve(prev, cur, next, FontPoint(contour[(i + 2) % n]));
		}
	}

	// If final angle is positive (+2PI), it's an anti-clockwise contour,
	// otherwise (-2PI) it's clockwise.
	clockwise = (angle < 0.0);
}

// This function is a bit tricky. Given a path ABC, it returns the
// coordinates of the outset point facing B on the left at a distance
// of 64.0.
//                                         M
//                            - - - - - - X
//                             ^         / '
//                             | 64.0   /   '
//  X---->-----X     ==>    X--v-------X     '
// A          B \          A          B \   .>'
//               \                       \<'  64.0
//                \                       \                  .
//                 \                       \                 .
//                C X                     C X
//
FontPoint FontContour::computeOutsetPoint(const FontPoint& A, const FontPoint& B, const FontPoint& C)
{
	/* Build the rotation matrix from 'ba' vector */
	FontPoint ba = (A - B).Normalise();
	FontPoint bc = C - B;

	/* Rotate bc to the left */
	FontPoint tmp(bc.x * -ba.x + bc.y * -ba.y,
		bc.x * ba.y + bc.y * -ba.x);

	/* Compute the vector bisecting 'abc' */
	double norm = sqrt(tmp.x * tmp.x + tmp.y * tmp.y);
	double dist = 64.0 * sqrt((norm - tmp.x) / (norm + tmp.x));
	tmp.x = tmp.y < 0.0 ? dist : -dist;
	tmp.y = 64.0;

	/* Rotate the new bc to the right */
	return FontPoint(tmp.x * -ba.x + tmp.y * ba.y,
		tmp.x * -ba.y + tmp.y * -ba.x);
}

void FontContour::setParity(int32_t parity)
{
	uint32_t size = points.getSize();
	FontPoint vOutset;

	FontPoint* pPointList = points.getFirst();

	if (((parity & 1) && clockwise) || (!(parity & 1) && !clockwise))
	{
		// Contour orientation is wrong! We must reverse all points.
		// FIXME: could it be worth writing FTVector::reverse() for this?
		for (uint32_t i = 0; i < size / 2; i++)
		{
			FontPoint tmp = pPointList[i];
			pPointList[i] = pPointList[size - 1 - i];
			pPointList[size - 1 - i] = tmp;
		}

		clockwise = !clockwise;
	}
	
	for (uint32_t i = 0; i < size; i++)
	{
		uint32_t prev, cur, next;

		prev = (i + size - 1) % size;
		cur  = i;
		next = (i + size + 1) % size;

		vOutset = computeOutsetPoint(pPointList[prev], pPointList[cur], pPointList[next]);
		outsetPoints.push(vOutset);
	}
}

void FontContour::buildFrontOutset(double outsetSize)
{
	uint16_t   pointsNum = (uint16_t)points.getSize();
	FontPoint* pPoints  = points.getFirst();
	FontPoint* pOutsets = outsetPoints.getFirst();
	FontPoint* pFronts  = frontPoints.pushArray(pointsNum);

	for (uint16_t i = 0; i < pointsNum; ++i)
	{
		pFronts[i] = pPoints[i] + (pOutsets[i] * outsetSize);
	}
}

}