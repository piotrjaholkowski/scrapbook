#pragma once

#pragma warning(disable: 4786)
#pragma warning( disable : 4244)

#include "../jaSetup.hpp"

#include "../Math/VectorMath2d.hpp"
#include "../Utility/Color.hpp"
#include "../Utility/String.hpp"

#include <ft2build.h>
#include FT_FREETYPE_H

#include <stdint.h>

namespace ja
{

	enum class FontEncode{
		UNICODE_STANDARD        = 400,
		ASCII_STANDARD          = 128,
		EXTENDED_ASCII_STANDARD = 256
	};

	template <typename CharT, typename WidthT>
	class CharLine
	{
	public:
		CharLine(){};
		const CharT* start;
		uint32_t     charsNum;
		WidthT       width;
	};

	class Letter
	{
	public:
		float    tx, ty;   // texture coordinates
		uint32_t bh, bw;   // bitmap size
		int32_t  advanceX; // translation of cursor x
		int32_t  advanceY; // translation of cursor y
		int32_t  top;      // translation of glyph y
		int32_t  left;     // translation of glyph x
	};

	class LetterPolygon
	{
	public:
		jaVector2* vertices;
		uint32_t*  indices;

		uint32_t verticesNum;
		uint32_t indicesNum;

		float letterHeight;
		float letterWidth;
		float advanceX; // translation of cursor x
		float advanceY; // translation of cursor y
		float left;
		float right;
		float top;
		float bottom;

	public:

		LetterPolygon() : vertices(nullptr), indices(nullptr), verticesNum(0), indicesNum(0)
		{

		}

		~LetterPolygon()
		{
			if (this->vertices)
				delete[] this->vertices;

			if (this->indices)
				delete[] this->indices;
		}
	};

	

	class FontPolygon
	{
	private:
		uint32_t   resourceId;
		Color      color;
		float      highestCharacterSize;  // height of font
		float      widthestCharacterSize; // widest character
		float      lowestPosition;
		float      highestPosition;
		FontEncode encode;
	public:

		LetterPolygon* letter;

		FontPolygon(uint32_t resourceId);
		~FontPolygon();

		void clear();
		void getLetterIndex(ja::string& text, int32_t cursorPositionX, int32_t& index) const;

		inline float getHighestCharacterSize() const
		{
			//return height;
			return this->highestCharacterSize;
		}

		inline float getWidthestCharacterSize() const
		{
			return this->widthestCharacterSize;
		}

		inline float getHighestPosition() const
		{
			return this->highestPosition;
		}

		inline float getLowestPosition() const
		{
			return this->lowestPosition;
		}

		inline Color getColor() const
		{
			return color;
		}

		inline void getLetterSize(uint16_t letter, float& width, float& height) const
		{
			width  = this->letter[letter].advanceX;
			height = this->highestCharacterSize;
		}

		friend class FontLoader;
	};

	class Font
	{
	private:
		uint32_t   resourceId;
		uint32_t*  texture;
		uint32_t   list_base;
		Color      color;
		int32_t    highestCharacterSize; // height of font
		int32_t    widthestCharacerSize; // widest character
		int32_t    lowestPosition;
		int32_t    highestPosition;
		FontEncode encode;
	public:
		Letter* letter;
		
		Font(uint32_t resourceId);
		~Font();

		void clear();
		void setColor(uint8_t r, uint8_t g, uint8_t b);
		void getTextSize(int32_t& width, int32_t& height, wchar_t* text) const;
		void getTextSize(int32_t& width, int32_t& height, ja::string& text) const;

		//index is index of letter pointed by cursor
		void getLetterIndex(ja::string& text, int32_t cursorPositionX, int32_t& index) const;

		inline int32_t getHighestCharacterSize()
		{
			return this->highestCharacterSize;
		}

		inline int32_t getWidthestCharacterSize() const
		{
			return this->widthestCharacerSize;
		}

		inline int32_t getHighestPosition() const
		{
			return this->highestPosition;
		}

		inline int32_t getLowest() const
		{
			return this->lowestPosition;
		}

		inline Color getColor() const
		{
			return color;
		}

		inline void getLetterSize(uint16_t letter, int32_t& width, int32_t& height) const
		{
			width  = this->letter[letter].advanceX;
			height = this->highestCharacterSize;
		}

		inline uint32_t getListBase()
		{
			return list_base;
		}

		friend class FontLoader;
	};

	class FontLoader
	{
	private:
		static void createCharacter(FT_Face face, uint16_t z, uint32_t& texture, Letter& letter, FontEncode encode);
		static void createCharacterPolygon(FT_Face face, uint16_t z, LetterPolygon& letter, FontEncode encode);
	public:
		static bool loadFont(const char* fontFile, uint32_t fontSize, FontEncode encode, Font* font);
		static bool loadFontPolygon(const char* fontFile, FontEncode encode, FontPolygon* font); 
	};

}