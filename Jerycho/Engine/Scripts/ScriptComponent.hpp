#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Entity/Entity.hpp"
#include "../Entity/PhysicGroup.hpp"

namespace jas
{

	class ScriptComponent
	{
	private:
		static const char className[];

		static int32_t create(lua_State* L);
		static int32_t setFunction(lua_State* L);

	public:

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, create, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setFunction, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

}
