#include "LevelManager.hpp"
#include "ResourceManager.hpp"
#include "../Manager/LogManager.hpp"

namespace jas
{

const char LevelManager::className[] = "LevelManager";

int32_t LevelManager::saveLevel(lua_State* L)
{
	int32_t n = lua_gettop(L);
	if (n != 1)
		return 0;
	const char* levelName = lua_tostring(L, 1);
	//ja::LevelManager::getSingleton()->saveLevel(levelName);

	return 0;
}

int32_t LevelManager::loadLevel(lua_State* L)
{
	int32_t n = lua_gettop(L);
	if (n != 1)
		return 0;
	const char* levelName = lua_tostring(L, 1);
	//ja::LevelManager::getSingleton()->loadLevelSeparateThread(levelName);

	return 0;
}

int32_t LevelManager::reloadLevel(lua_State* L)
{
	ja::LevelManager::getSingleton()->reloadLevel();

	return 0;
}

int32_t LevelManager::reloadLevelSafe(lua_State* L)
{
	//ja::LevelManager::getSingleton()->reloadLevel();

	return 0;
}

int32_t LevelManager::loadShader(lua_State* L)
{
	ja::LevelManager*    levelManager    = ja::LevelManager::getSingleton();
	ja::ResourceManager* resourceManager = ja::ResourceManager::getSingleton();
	ja::LogManager*      logManager      = ja::LogManager::getSingleton();

	const char*    shaderFile = lua_tostring(L, 1);
	const uint32_t shaderType = (uint32_t)lua_tointeger(L, 2);

	char textBuffer[512];
	sprintf(textBuffer, "Loading %s", shaderFile);
	levelManager->printCurrentTask(textBuffer);
	logManager->addLogMessage(ja::LogType::INFO_LOG, textBuffer);

	ja::Resource<ja::Shader>* resource = resourceManager->loadShader(shaderFile, (ja::ShaderType)shaderType);

	if (nullptr == resource)
	{
		sprintf(textBuffer, "Can't load %s", shaderFile);
		levelManager->printFailureTask(textBuffer);
		logManager->addLogMessage(ja::LogType::ERROR_LOG, textBuffer);

		lua_pushnil(L);
	}
	else
	{
		sprintf(textBuffer, "%s loaded", shaderFile);
		levelManager->printFinishedTask(textBuffer);
		logManager->addLogMessage(ja::LogType::SUCCESS_LOG, textBuffer);

		lua_pushlightuserdata(L, resource->resource);
	}

	return 1;
}

int32_t LevelManager::createShaderBinder(lua_State* L)
{
	ja::LevelManager*    levelManager    = ja::LevelManager::getSingleton();
	ja::ResourceManager* resourceManager = ja::ResourceManager::getSingleton();
	ja::LogManager*      logManager      = ja::LogManager::getSingleton();

	const char* shaderBinderName   = lua_tostring(L, 1);

	char textBuffer[512];
	sprintf(textBuffer, "Creating %s shader binder", shaderBinderName);
	levelManager->printCurrentTask(textBuffer);
	logManager->addLogMessage(ja::LogType::INFO_LOG, textBuffer);
	
	ja::Resource<ja::ShaderBinder>* resource = resourceManager->getShaderBinder(shaderBinderName);
	
	if (nullptr == resource)
	{
		ResourceManager::createShaderBinder(L);

		ja::ShaderBinder* shaderBinder = (ja::ShaderBinder*)lua_touserdata(L, -1);

		if (nullptr == shaderBinder)
		{
			sprintf(textBuffer, "Can't create shader binder %s", shaderBinderName);
			levelManager->printFailureTask(textBuffer);
			logManager->addLogMessage(ja::LogType::ERROR_LOG, textBuffer);
		}
		else
		{
			sprintf(textBuffer, "Shader binder %s created", shaderBinderName);
			levelManager->printFinishedTask(textBuffer);
			logManager->addLogMessage(ja::LogType::SUCCESS_LOG, textBuffer);
		}
	}
	else
	{
		sprintf(textBuffer, "Shader binder %s already exists", shaderBinderName);
		levelManager->printFailureTask(textBuffer);
		logManager->addLogMessage(ja::LogType::ERROR_LOG, textBuffer);

		lua_pushlightuserdata(L, nullptr);
	}

	return 1;
}

int32_t LevelManager::createMaterial(lua_State* L)
{
	ja::LevelManager*    levelManager    = ja::LevelManager::getSingleton();
	ja::ResourceManager* resourceManager = ja::ResourceManager::getSingleton();
	ja::LogManager*      logManager      = ja::LogManager::getSingleton();

	const char* materialName     = lua_tostring(L, 1);
	const char* materialCoreName = lua_tostring(L, 2);

	char textBuffer[512];
	sprintf(textBuffer, "Creating %s material", materialName);
	levelManager->printCurrentTask(textBuffer);
	logManager->addLogMessage(ja::LogType::INFO_LOG, textBuffer);

	ja::Resource<ja::Material>* materialResource = resourceManager->getMaterial(materialName);

	if (nullptr == materialResource)
	{
		ResourceManager::addMaterial(L);

		 ja::Material* material = (ja::Material*)lua_touserdata(L, -1);

		 if (nullptr == material)
		 {
			 sprintf(textBuffer, "Can't create material %s", materialName);
			 levelManager->printFailureTask(textBuffer);
			 logManager->addLogMessage(ja::LogType::ERROR_LOG, textBuffer);
		 }
		 else
		 {
			 sprintf(textBuffer, "Material %s created", materialName);
			 levelManager->printFinishedTask(textBuffer);
			 logManager->addLogMessage(ja::LogType::SUCCESS_LOG, textBuffer);
		 }
	}
	else
	{
		sprintf(textBuffer, "Material %s already exists", materialCoreName);
		levelManager->printFailureTask(textBuffer);
		logManager->addLogMessage(ja::LogType::ERROR_LOG, textBuffer);

		lua_pushlightuserdata(L, nullptr);
	}

	return 1;
}

int32_t LevelManager::printCurrentTask(lua_State* L)
{
	ja::LevelManager* levelManager = ja::LevelManager::getSingleton();

	const char* task = lua_tostring(L, 1);
	levelManager->printCurrentTask(task);

	return 0;
}

int32_t LevelManager::printFinishedTask(lua_State* L)
{
	ja::LevelManager* levelManager = ja::LevelManager::getSingleton();

	const char* task = lua_tostring(L, 1);
	levelManager->printFinishedTask(task);

	return 0;
}

int32_t LevelManager::printFailureTask(lua_State* L)
{
	ja::LevelManager* levelManager = ja::LevelManager::getSingleton();

	const char* task = lua_tostring(L, 1);
	levelManager->printFailureTask(task);

	return 0;
}

}