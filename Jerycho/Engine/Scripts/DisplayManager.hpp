#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Manager/DisplayManager.hpp"

namespace jas 
{
	class DisplayManager
	{
	private:
		static const char className[];

		//void setBackgroundColor(int r, int g, int b)
		static int32_t setBackgroundColor(lua_State* L)
		{
			int32_t r = (int32_t)luaL_checkinteger(L, 1);
			int32_t g = (int32_t)luaL_checkinteger(L, 2);
			int32_t b = (int32_t)luaL_checkinteger(L, 3);

			ja::DisplayManager::getSingleton()->setBackgroundColor(r,g,b);
			return 0;
		}


	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,setBackgroundColor,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char DisplayManager::className[] = "DisplayManager";
}
