#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Manager/ThreadManager.hpp"

namespace jas
{
	class ThreadManager
	{
	private:
		static const char className[];

		static int32_t setThreadsNum(lua_State* L)
		{
			uint8_t threadsNum = (uint8_t)lua_tointeger(L, 1);
			ja::ThreadManager::getSingleton()->setThreadsNum(threadsNum);

			return 0;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, setThreadsNum, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char ThreadManager::className[] = "ThreadManager";
}

