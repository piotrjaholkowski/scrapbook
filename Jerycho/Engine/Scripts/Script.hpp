#pragma once

#include <string.h>
#include "../jaSetup.hpp"

#include "../Math/Math.hpp"
#include "../Allocators/StackAllocator.hpp"

extern "C" {
#include <lua/lua.h>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
}

namespace ja
{
	enum class ScriptFunctionType
	{
		INIT_ENTITY         = 0,
		UPDATE_ENTITY       = 1,
		CONSTRAINT_NOTIFIER = 2,
		INIT_DATA           = 3,
		ACCESS_DATA         = 4
		//RENDER_LAYER = 2
	};

	class ScriptFunction
	{
	public:
		int32_t  functionRef;
		uint32_t functionHash;
		uint32_t fileHash;
		uint8_t  functionType;
		char     functionName[setup::scripts::SCRIPT_FUNCION_LENGTH];
		char     fileName[setup::FILE_NAME_LENGTH];
		//lua_CFunction functionPtr;
		//int32_t       functionRef;

		static inline uint32_t getHash(const char* name)
		{
			return ja::math::convertToHash(name);
		}
	};

	class Script
	{
	public:
		uint32_t  resourceId;
		//char      functionName[setup::scripts::MAX_SCRIPT_FUNCTION_NUM][setup::scripts::SCRIPT_FUNCION_LENGTH];
		//uint16_t  functionNum;
		//StackAllocator<ScriptFunction> exportedFunctions;

		
		/*Script(uint32_t resourceId, const char* functionName)
		{
			this->resourceId = resourceId;
#pragma warning(disable:4996)
			strncpy(this->functionName[0], functionName, setup::scripts::MAX_SCRIPT_FUNCION_LENGTH);
#pragma warning(default:4996)
			functionNum = 1;
		}*/

		Script(uint32_t resourceId)
		{
			//this->functionNum = 0;
		}

		/*void addFunction(const char* functionName)
		{
			if (functionNum >= setup::scripts::MAX_SCRIPT_FUNCTION_NUM)
				return;
#pragma warning(disable:4996)
			strncpy(this->functionName[functionNum], functionName, setup::scripts::MAX_SCRIPT_FUNCION_LENGTH);
#pragma warning(default:4996)
			functionNum++;
		}*/

		~Script()
		{

		}
	};

}