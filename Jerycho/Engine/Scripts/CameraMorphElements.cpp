#include "CameraMorphElements.hpp"
#include "DrawElements.hpp"

namespace jas
{

	const char CameraMorphElements::className[] = "CameraMorphElements";



	void CameraMorphElements::save(FILE* pFile, const char* morphElementsVar, const ja::CameraMorphElements& cameraMorphElements)
	{
		const uint16_t morphsNum = cameraMorphElements.getMorphsNum();

		const char* cameraMorphVar  = "cameraMorph";
		const char* drawElementsVar = "drawElements";

		ja::MorphEntry* morphEntry = cameraMorphElements.getMorphEntries();

		for (uint16_t i = 0; i < morphsNum; ++i)
		{
			ja::CameraMorph* cameraMorph = (ja::CameraMorph*)morphEntry[i].layerObjectMorph;

			//local polygonMorph = MorphElements.createMorph(morphElements);
			fprintf(pFile, "local %s = MorphElements.createMorph(%s,'%s');\n", cameraMorphVar, morphElementsVar, cameraMorph->getName());

			const jaVector2 translate = cameraMorph->transform.position;
			const jaVector2 scale     = cameraMorph->scale;
			const float     rotation  = cameraMorph->transform.rotationMatrix.getRadians();

			fprintf(pFile, "CameraMorph.setPosition(%s,Vector2.create(%f,%f));\n", cameraMorphVar, translate.x, translate.y);
			fprintf(pFile, "CameraMorph.setRotation(%s, %f);\n", cameraMorphVar, rotation);
			fprintf(pFile, "CameraMorph.setScale(%s, Vector2.create(%f,%f));\n", cameraMorphVar, scale.x, scale.y);
			fprintf(pFile, "CameraMorph.setActiveFeatures(%s, %x);\n", cameraMorphVar, cameraMorph->getActiveFeatures());
		}
	}


}