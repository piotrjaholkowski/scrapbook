#include "PhysicComponent.hpp"

#include "../Entity/PhysicGroup.hpp"

namespace jas
{

const char PhysicComponent::className[] = "PhysicComponent";

int32_t PhysicComponent::create(lua_State* L)
{
	ja::Entity* entity = (ja::Entity*)lua_touserdata(L, 1);

	ja::PhysicComponent* physicComponent = ja::PhysicGroup::getSingleton()->createComponent(entity);
	//entity->addComponent(physicComponent); 

	lua_pushlightuserdata(L, physicComponent);

	return 1;
}

int32_t PhysicComponent::setRoot(lua_State* L)
{
	ja::PhysicComponent* physicComponent = (ja::PhysicComponent*)lua_touserdata(L, 1);
	
	physicComponent->setRoot();

	return 0;
}

int32_t PhysicComponent::setAutoDelete(lua_State* L)
{
	ja::PhysicComponent* physicComponent = (ja::PhysicComponent*)lua_touserdata(L, 1);
	bool isAutoDelete = (0 != lua_toboolean(L, 2));

	physicComponent->autoDelete = isAutoDelete;

	return 0;
}

int32_t PhysicComponent::setPhysicObject(lua_State* L)
{
	ja::PhysicComponent* component    = (ja::PhysicComponent*)(lua_touserdata(L, 1));
	ja::PhysicObject2d*  physicObject = (ja::PhysicObject2d*)(lua_touserdata(L, 2));

	component->setPhysicObject(physicObject);
	
	return 0;
}

int32_t PhysicComponent::getPhysicObject(lua_State* L)
{
	ja::PhysicComponent* component = (ja::PhysicComponent*)(lua_touserdata(L, 1));

	ja::PhysicObject2d* physicObject = component->getPhysicObject();
	if (physicObject == nullptr)
		lua_pushnil(L);
	else
		lua_pushlightuserdata(L, physicObject);

	return 1;
}

}