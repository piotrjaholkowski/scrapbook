#pragma once

/*
extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include "../Manager/SpriteManager.hpp"
#include "SpriteDef.hpp"
#include "Sprite.hpp"

namespace jas
{
	class SpriteManager
	{
	private:
		static const char className[];

		//jaSprite* createSpriteDef()
		static int createSpriteDef(lua_State* L)
		{
			ja::Sprite* sprite = ja::SpriteManager::getSingleton()->createSpriteDef();
			lua_pushlightuserdata(L,sprite);
			return 1;
		}

		//jaSpriteDef* getSpriteDef()
		static int getSpriteDef(lua_State* L)
		{
			SpriteDef::pushOnStack(L,&ja::SpriteManager::getSingleton()->spriteDef);

			return 1;
		}

		static int deleteSpriteImmediate(lua_State* L)
		{
			ja::Sprite* sprite = (ja::Sprite*)lua_touserdata(L,1);
			ja::SpriteManager::getSingleton()->deleteSpriteImmediate(sprite);
			return 0;
		}

		//jaSprite* getSprite(unsigned short spriteId)
		static int getSprite(lua_State* L)
		{
			unsigned short spriteId = luaL_checkunsigned(L,1); 
			ja::Sprite* sprite = ja::SpriteManager::getSingleton()->getSprite(spriteId);

			if(sprite == nullptr)
			{
				lua_pushnil(L);
			}
			else
			{
				//jaScriptSprite::pushOnStack(L,sprite);
				lua_pushlightuserdata(L,sprite);
			}

			return 1;
		}


	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,createSpriteDef,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getSpriteDef,table);
			JAS_ADD_SCRIPT_FUNCTION(L,deleteSpriteImmediate,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getSprite,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char SpriteManager::className[] = "SpriteManager";
}*/
