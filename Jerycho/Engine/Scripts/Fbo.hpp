#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Graphics/Fbo.hpp"

namespace jas
{
	class Fbo
	{
	private:
		static const char className[];

		static int32_t bind(lua_State* L)
		{
			ja::Fbo* fbo = static_cast<ja::Fbo*>(lua_touserdata(L, 1));
			fbo->bind();

			return 0;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, bind, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char Fbo::className[] = "Fbo";
}