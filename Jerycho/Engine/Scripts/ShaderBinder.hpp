#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Graphics/ShaderBinder.hpp"
#include "ScriptFunctions.hpp"

namespace jas
{
	class ShaderBinder
	{
	private:
		static const char className[];

		static int32_t bind(lua_State* L);
		static int32_t unbind(lua_State* L);

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, bind, table);
			JAS_ADD_SCRIPT_FUNCTION(L, unbind, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	
}