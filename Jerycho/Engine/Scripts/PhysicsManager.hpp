#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Manager/PhysicsManager.hpp"
#include "../Physics/RigidBody2d.hpp"

namespace jas
{
	class PhysicsManager
	{
	private:
		static const char className[];

		static int32_t setDrawAABB(lua_State* L)
		{
			if(lua_toboolean(L,1))
				ja::PhysicsManager::getSingleton()->drawAABB = true;
			else
				ja::PhysicsManager::getSingleton()->drawAABB = false;

			return 0;
		}

		static int32_t setDrawShape(lua_State* L)
		{
			if(lua_toboolean(L,1))
				ja::PhysicsManager::getSingleton()->drawShapes = true;
			else
				ja::PhysicsManager::getSingleton()->drawShapes = false;

			return 0;
		}

		static int32_t getPhysicObject(lua_State* L)
		{
			uint16_t physicId = (uint16_t)luaL_checkinteger(L,1);
			ja::PhysicObject2d* physicObject = ja::PhysicsManager::getSingleton()->getPhysicObject(physicId);
			
			if(physicObject == nullptr)
				lua_pushnil(L);
			else
				lua_pushlightuserdata(L, physicObject);
	
			return 1;
		}

		static int32_t getLastAwakePhysicObject(lua_State* L)
		{
			ja::PhysicObject2d* lastAwake = ja::PhysicsManager::getSingleton()->getLastAwakePhysicObject();

			if(lastAwake == nullptr)
				lua_pushnil(L);
			else
				lua_pushlightuserdata(L,lastAwake);

			return 1;
		}

		static int32_t getConstraints(lua_State* L)
		{
			ja::PhysicObject2d* physicObject = (ja::PhysicObject2d*)(lua_touserdata(L,1));

			ja::Constraint2d* constraint = ja::PhysicsManager::getSingleton()->getConstraints(physicObject);
			
			if(constraint == nullptr)
				lua_pushnil(L);
			else
				lua_pushlightuserdata(L,constraint);

			return 1;
		}

		static int32_t getPhysicObjectDef(lua_State* L)
		{
			lua_pushlightuserdata(L, &ja::PhysicsManager::getSingleton()->physicObjectDef);

			return 1;
		}

		static int32_t createBodyDef(lua_State* L)
		{
			ja::RigidBody2d* rigidBody;

			if (lua_gettop(L) == 1)
			{
				uint16_t bodyId = (uint16_t)lua_tointeger(L, 1);
				rigidBody = ja::PhysicsManager::getSingleton()->createBodyDef(bodyId);
			}
			else
			{
				rigidBody = ja::PhysicsManager::getSingleton()->createBodyDef();
			}
			
			lua_pushlightuserdata(L, rigidBody);

			return 1;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,setDrawAABB,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setDrawShape,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getPhysicObject,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getLastAwakePhysicObject,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getConstraints,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getPhysicObjectDef,table);
			JAS_ADD_SCRIPT_FUNCTION(L,createBodyDef,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char PhysicsManager::className[] = "PhysicsManager";
}