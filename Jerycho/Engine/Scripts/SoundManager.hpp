#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Manager/SoundManager.hpp"

namespace jas
{
	class SoundManager
	{
	private:
		static const char className[];

		static int32_t getFrequency(lua_State* L)
		{
			lua_pushnumber(L,ja::SoundManager::getSingleton()->fftData.frequency);
			return 1;
		}

		static int32_t getBiggestIndex(lua_State* L)
		{
			lua_pushinteger(L, ja::SoundManager::getSingleton()->fftData.biggestIndex);
			return 1;
		}
	
	public:

		static void registerClass(lua_State* L)
		{	
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,getFrequency,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getBiggestIndex,table);
			lua_setglobal(L, className); // setglobal remove table from stack*/
		}
	};

	const char SoundManager::className[] = "SoundManager";
}
