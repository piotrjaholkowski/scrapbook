#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Graphics/LayerObjectDef.hpp"

namespace jas
{
	class LayerObjectDef
	{
	private:
		static const char className[];

		static int32_t setPosition(lua_State* L);
		static int32_t setRotation(lua_State* L);
		static int32_t setScale(lua_State* L);
		static int32_t setLayerObjectType(lua_State* L);
		static int32_t setDepth(lua_State* L);
		static int32_t setLayerId(lua_State* L);
		static int32_t getDrawElements(lua_State* L);
		static int32_t getPolygonMorphElements(lua_State* L);
		static int32_t getCameraMorphElements(lua_State* L);
		static int32_t setCameraName(lua_State* L);

	public:

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, setPosition, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setRotation, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setScale,    table);
			JAS_ADD_SCRIPT_FUNCTION(L, setLayerObjectType, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setDepth, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setLayerId, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getDrawElements, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getPolygonMorphElements, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getCameraMorphElements, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setCameraName, table);

			//JAS_ADD_SCRIPT_FUNCTION(L, isSleeping, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, getPrevious, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

}