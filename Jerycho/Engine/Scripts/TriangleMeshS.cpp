#include "TriangleMesh.hpp"
#include "../Manager/RecycledMemoryManager.hpp"

namespace jas
{
const char TriangleMesh::className[] = "TriangleMesh";

int32_t TriangleMesh::setTriangleMesh(lua_State* L)
{
	gil::TriangleMesh2d* triangleMesh = (gil::TriangleMesh2d*)lua_touserdata(L, 1);

	//StackAllocator<float>    verticesArray;
	//StackAllocator<uint16_t> indicesArray;

	//uint16_t verticesNum = 0;
	//uint16_t trianglesNum = 0;
	//uint16_t indicesNum = 0;	

	luaL_checktype(L, 2, LUA_TTABLE);
	luaL_checktype(L, 3, LUA_TTABLE);

#ifdef LUA53
	const int32_t verticesArraySize = (int32_t)luaL_len(L, 2);
	const int32_t indicesArraySize = (int32_t)luaL_len(L, 3);
#else
	const int32_t verticesArraySize = lua_objlen(L, 2);
	const int32_t indicesArraySize  = lua_objlen(L, 3);
#endif

	RecycledMemoryManager* memoryManager = RecycledMemoryManager::getSingleton();
	void* memoryBlock = memoryManager->getMemoryBlock(ja::setup::physics::MULTI_SHAPE_ALLOCATOR_SIZE);

	
	//setting vertices


	float* verticesData = ((float*)memoryBlock + 1);

	//verticesArray.resize(arraySize * sizeof(float));
	//float* verticesData = verticesArray.getFirst() - 1;
	const uint16_t verticesNum = (uint16_t)(verticesArraySize / 2);

	//Faster method
	for (int32_t i = 1; i <= verticesArraySize; i++) {
		lua_rawgeti(L, 2, i);
		verticesData[i] = (float)lua_tonumber(L, -1);
		lua_pop(L, 1);
	}

	verticesData += 1;


	/*lua_pushnil(L);

	while (lua_next(L, -3) != 0)
	{
	verticesData[i++] = (float)lua_tonumber(L, -1);
	lua_pop(L, 1);
	}*/
	
	//setting indices

	//indicesArray.resize(indicesNum * sizeof(uint16_t));
	//uint16_t* indicesData = indicesArray.getFirst() - 1;

	uint16_t* indicesData = ((uint16_t*)(verticesData + verticesArraySize)) - 1;

	const uint16_t trianglesNum = (uint16_t)(indicesArraySize / 3);

	//Faster method
	for (int32_t i = 1; i <= indicesArraySize; i++) {
		lua_rawgeti(L, 3, i);
		indicesData[i] = (uint16_t)lua_tointeger(L, -1);
		lua_pop(L, 1);
	}

	indicesData += 1;

	/*lua_pushnil(L);

	while (lua_next(L, -2) != 0)
	{
	indicesData[i++] = (uint16_t)lua_tointeger(L, -1);
	lua_pop(L, 1);
	}*/

	const jaVector2* vertices = (jaVector2*)verticesData;
	const uint16_t*  indices = indicesData;

	/*for (uint16_t i = 0; i < verticesNum; ++i)
	{
	printf("%f, %f\n", vertices[i].x, vertices[i].y);
	}

	for (uint16_t i = 0; i < indicesNum; ++i)
	{
	std::cout << indices[i] << std::endl;
	}*/

	assert(false);
	triangleMesh->setTriangleMesh(vertices, verticesNum, indices, trianglesNum);

	memoryManager->releaseMemoryBlock(memoryBlock);

	return 0;
}

}