#pragma once

/*
extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include "../Graphics/SpriteDef.hpp"
#include "../Manager/ResourceManager.hpp"

namespace jas
{
	class SpriteDef
	{
	private:
		static const char className[];

		//scripter interface
		//jaSpriteDef* new()
		static int create(lua_State* L)
		{
			ja::SpriteDef *spriteDef = (ja::SpriteDef*)lua_newuserdata(L, sizeof(ja::SpriteDef));
			luaL_getmetatable(L, className);
			lua_setmetatable(L, -2);

			return 1;
		}

		static int setPosition(lua_State* L)
		{
			ja::SpriteDef* spriteDef = (ja::SpriteDef*)lua_touserdata(L,1);
			spriteDef->position.x = static_cast<jaFloat>(lua_tonumber(L,2));
			spriteDef->position.y =  static_cast<jaFloat>(lua_tonumber(L,3));

			return 0;
		}

		static int setPosition1(lua_State* L)
		{
			ja::SpriteDef* spriteDef = (ja::SpriteDef*)lua_touserdata(L,1);
			//spriteDef->position2.x = static_cast<gilFloat>(lua_tonumber(L,2));
			//spriteDef->position2.y =  static_cast<gilFloat>(lua_tonumber(L,3));

			return 0;
		}

		static int setLayerId(lua_State* L)
		{
			ja::SpriteDef* spriteDef = (ja::SpriteDef*)lua_touserdata(L,1);
			spriteDef->layerId = luaL_checkunsigned(L,2);

			return 0;
		}

		static int setTexture(lua_State* L)
		{
			ja::SpriteDef* spriteDef = (ja::SpriteDef*)lua_touserdata(L,1);
			unsigned int textureId = lua_tounsigned(L,2);

			spriteDef->texture = ResourceManager::getSingleton()->getLevelTexture2dProxy(textureId); 

			return 0;
		}

		//void setSpriteType(unsigned int spriteType)
		static int setSpriteType(lua_State* L)
		{
			ja::SpriteDef* spriteDef = (ja::SpriteDef*)lua_touserdata(L,1);
			unsigned int spriteType = lua_tounsigned(L,2);

			spriteDef->spriteType = (ja::SpriteType)spriteType;
			return 0;
		}

		static int setSequenceId(lua_State* L)
		{
			ja::SpriteDef* spriteDef = (ja::SpriteDef*)lua_touserdata(L,1);

			spriteDef->sequenceId = lua_tounsigned(L,2);
			return 0;
		}

		static int setColor(lua_State* L)
		{
			ja::SpriteDef* spriteDef = (ja::SpriteDef*)lua_touserdata(L,1);

			unsigned char r = lua_tounsigned(L,2);
			unsigned char g = lua_tounsigned(L,3);
			unsigned char b = lua_tounsigned(L,4);

			spriteDef->r = r;
			spriteDef->g = g;
			spriteDef->b = b;

			return 0;
		}

		
		//static int index(lua_State* L)
		//{
		//	jaSpriteDef* spriteDef = (jaSpriteDef*)lua_touserdata(L,1);
		//	const char* field =  lua_tostring(L, 2);
		//	int size = strlen(field); // function or field name
		//
		//	luaL_getmetatable(L,className);
		//	lua_pushlstring(L,field,size);
		//	lua_gettable(L,3); // function on stack
		//	return 1;
		//}
		//
		//static int toString(lua_State* L)
		//{
		//	jaEntity2dDef* entity2dDef = (jaEntity2dDef*)lua_touserdata(L,1);
		//
		//	lua_pushfstring(L, "group=%hhu subgroup=%hhu", entity2dDef->entityGroupId, entity2dDef->subgroupId);
		//	return 1;
		//}

		//scripter interface

	public:

		static void pushOnStack(lua_State* L, ja::SpriteDef* v)
		{
			lua_pushlightuserdata(L,(void*)v);
			
			//luaL_getmetatable(L, className);
			//lua_setmetatable(L, -2);
		}

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,create,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setTexture,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setLayerId,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setSpriteType,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setPosition,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setColor,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setPosition1,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setSequenceId,table);


			//lua_pushliteral(L,"__index");
			//lua_pushcfunction(L,index);
			//lua_settable(L,table);
			//
			//lua_pushliteral(L,"__tostring");
			//lua_pushcfunction(L,toString);
			//lua_settable(L,table);

			lua_setglobal(L,className);
		}

	};

	const char SpriteDef::className[] = "SpriteDef";
}*/
