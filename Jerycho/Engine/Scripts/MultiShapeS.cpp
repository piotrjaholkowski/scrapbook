#include "MultiShape.hpp"
#include "../Manager/PhysicsManager.hpp"

namespace jas
{

const char MultiShape::className[] = "MultiShape";
const char MultiShape::metaClassName[] = "MultiShapeMeta";

int32_t MultiShape::create(lua_State* L)
{
	CacheLineAllocator* temporaryAllocator = ja::PhysicsManager::getSingleton()->getTemporaryShapeAllocator();

	gil::MultiShape2d* multiShape = new (lua_newuserdata(L, sizeof(gil::MultiShape2d))) gil::MultiShape2d(temporaryAllocator);

	luaL_getmetatable(L, metaClassName);
	lua_setmetatable(L, -2);

	return 1;
}

int32_t MultiShape::gc(lua_State* L)
{
	gil::MultiShape2d* multiShape = (gil::MultiShape2d*)(lua_touserdata(L, 1));
	multiShape->~MultiShape2d();
	return 0;
}

/*int32_t MultiShape::__add(lua_State* L)
{
	std::cout << "It works" << std::endl;

	return 0;
}*/

int32_t MultiShape::addShape(lua_State* L)
{
	gil::MultiShape2d* multiShape = (gil::MultiShape2d*)(lua_touserdata(L,1));
	jaTransform2*       transform = (jaTransform2*)(lua_touserdata(L,2));
	gil::Shape2d*           shape = (gil::Shape2d*)(lua_touserdata(L,3));

	multiShape->addShape(*transform, shape);
	
	return 0;
}

int32_t MultiShape::deleteShapes(lua_State* L)
{
	gil::MultiShape2d* multiShape = (gil::MultiShape2d*)(lua_touserdata(L, 1));

	multiShape->deleteShapes();

	return 0;
}

}