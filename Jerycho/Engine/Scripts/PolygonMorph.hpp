#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"

#include <stdint.h>

namespace jas
{
	class PolygonMorph
	{
	private:
		static const char className[];

		static int32_t getDrawElements(lua_State* L);
		static int32_t setPosition(lua_State* L);
		static int32_t setRotation(lua_State* L);
		static int32_t setScale(lua_State* L);
		static int32_t setActiveFeatures(lua_State* L);

	public:
		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, getDrawElements, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setPosition, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setRotation, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setScale, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setActiveFeatures, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, getPrevious, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, setTimeFactor, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};
}