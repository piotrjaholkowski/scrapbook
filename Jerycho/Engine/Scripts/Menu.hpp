#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

//#include "../Manager/jaUIManager.h"
#include "../Gui/Menu.hpp"
#include "../Gui/Widget.hpp"

namespace jas
{
	class Menu
	{
	private:
		static const char className[];

		static int32_t getWidget(lua_State* L)
		{
			ja::Menu* menu = (ja::Menu*)lua_touserdata(L, 1);
			uint32_t widgetId = (uint32_t)lua_tointeger(L,2);

			ja::Widget* widget = menu->getWidget(widgetId);

			if(widget)
				lua_pushlightuserdata(L,widget);
			else
				lua_pushnil(L);

			return 1;
		}

		static int32_t addWidget(lua_State* L)
		{
			ja::Menu* menu = (ja::Menu*)lua_touserdata(L, 1);
			ja::Widget* widget = (ja::Widget*)lua_touserdata(L,2);

			menu->addWidget(widget);

			return 0;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,addWidget,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getWidget,table);


			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char Menu::className[] = "Menu";
}
