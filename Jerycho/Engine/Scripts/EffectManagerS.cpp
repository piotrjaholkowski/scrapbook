#include "EffectManager.hpp"

namespace jas
{

const char EffectManager::className[] = "EffectManager";

int32_t EffectManager::createLayer(lua_State* L)
{
	const char* layerName = lua_tostring(L, 1);

	uint8_t layerId = ja::EffectManager::getSingleton()->createLayer(layerName);

	lua_pushinteger(L,layerId);
	return 1;
}

int32_t EffectManager::createParallax(lua_State* L)
{
	const char* parallaxName   = lua_tostring(L, 1);
	ja::Camera* camera         = (ja::Camera*)lua_touserdata(L, 2);
	bool        isActiveCamera = (0 != lua_toboolean(L, 3));

	uint8_t parallaxId = ja::EffectManager::getSingleton()->createParallax(parallaxName, camera, isActiveCamera);

	lua_pushinteger(L, parallaxId);
	return 1;
}

}