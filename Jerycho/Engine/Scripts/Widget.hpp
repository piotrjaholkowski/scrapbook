#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Gui/Widget.hpp"

namespace jas
{
	class Widget
	{
	private:
		static const char className[];

		static int32_t getWidgetType(lua_State* L)
		{
			ja::Widget* widget = (ja::Widget*)lua_touserdata(L,1);

			ja::WidgetType widgetType = widget->getWidgetType();
			lua_pushinteger(L,(uint32_t)widgetType);

			return 1;
		}

		static int32_t setRender(lua_State* L)
		{
			ja::Widget* widget = (ja::Widget*)lua_touserdata(L,1);

			if(lua_toboolean(L,2))
				widget->setRender(true);
			else
				widget->setRender(false);

			return 0;
		}

		static int32_t setActive(lua_State* L)
		{
			ja::Widget* widget = (ja::Widget*)lua_touserdata(L,1);

			if(lua_toboolean(L,2))
				widget->setActive(true);
			else
				widget->setActive(false);

			return 0;
		}

		static int32_t isActive(lua_State* L)
		{
			ja::Widget* widget = (ja::Widget*)lua_touserdata(L,1);

			lua_pushboolean(L,widget->isActive());

			return 1;
		}

		static int32_t isRendered(lua_State* L)
		{
			ja::Widget* widget = (ja::Widget*)lua_touserdata(L,1);
			lua_pushboolean(L,widget->isRendered());

			return 1;
		}

		static int32_t getId(lua_State* L)
		{
			ja::Widget* widget = (ja::Widget*)lua_touserdata(L,1);
			lua_pushinteger(L,widget->getId());

			return 1;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,getWidgetType,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setRender,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setActive,table);
			JAS_ADD_SCRIPT_FUNCTION(L,isActive,table);
			JAS_ADD_SCRIPT_FUNCTION(L,isRendered,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getId,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char Widget::className[] = "Widget";
}
