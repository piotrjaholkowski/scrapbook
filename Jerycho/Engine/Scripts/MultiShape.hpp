#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Gilgamesh/Shapes/Multishape2d.hpp"

namespace jas
{
	class MultiShape
	{
	private:
		static const char className[];
		static const char metaClassName[];

		static int32_t create(lua_State* L);
		static int32_t addShape(lua_State* L);
		static int32_t deleteShapes(lua_State* L);

		static int32_t gc(lua_State* L);
		//static int32_t __add(lua_State* L);

	public:
		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);
			
			JAS_ADD_SCRIPT_FUNCTION(L, create, table);
			JAS_ADD_SCRIPT_FUNCTION(L, addShape, table);
			JAS_ADD_SCRIPT_FUNCTION(L, deleteShapes, table);
			
			lua_setglobal(L, className); // setglobal remove table from stack

			///////////////////////////////////////////////////////////////////////

			luaL_newmetatable(L,metaClassName);
			int32_t metatable = lua_gettop(L);

			//JAS_ADD_SCRIPT_FUNCTION(L, __add, metatable);
			//JAS_ADD_SCRIPT_FUNCTION(L, __gc, metatable);
			
			lua_pushliteral(L,"__gc");
			lua_pushcfunction(L,gc);
			lua_settable(L, metatable);
			//lua_settable(L, -3);
			lua_setglobal(L, metaClassName); // setglobal remove table from stack
			
		}
	};

}