#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Gilgamesh/Shapes/Circle2d.hpp"

namespace jas
{
	class Circle
	{
	private:
		static const char className[];

		static int32_t setRadius(lua_State* L);

	public:
		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, setRadius, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

	
}