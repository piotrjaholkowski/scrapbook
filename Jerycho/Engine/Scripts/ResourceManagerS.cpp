#include "ResourceManager.hpp"
#include "../Manager/LogManager.hpp"
#include "../Manager/ResourceManager.hpp"
#include "../Entity/DataGroup.hpp"

namespace jas
{

const char ResourceManager::className[] = "ResourceManager";

int32_t ResourceManager::loadShader(lua_State* L)
{
	const char*    fileName   = lua_tostring(L, 1);
	const uint32_t shaderType = lua_tointeger(L, 2);

	ja::Resource<ja::Shader>* resource = ja::ResourceManager::getSingleton()->loadShader(fileName, (ja::ShaderType)shaderType);

	if (nullptr == resource)
	{
		lua_pushnil(L);
	}
	else
	{
		lua_pushlightuserdata(L, resource->resource);
	}

	return 1;
}

int32_t ResourceManager::getShader(lua_State* L)
{
	const char* fileName = lua_tostring(L, 1);

	ja::Resource<ja::Shader>* resource = ja::ResourceManager::getSingleton()->getShader(fileName);

	if (nullptr == resource)
	{
		lua_pushnil(L);
	}
	else
	{
		lua_pushlightuserdata(L, resource->resource);
	}

	return 1;
}



int32_t ResourceManager::addDataModel(lua_State* L)
{
	const uint32_t argsNum          = lua_gettop(L);
	const char*    dataModelName    = lua_tostring(L, 1);

	if (LUA_TTABLE == lua_type(L, 2))
	{
		lua_Debug debugInfo;

		if (1 != lua_getstack(L, 1, &debugInfo))
		{
			return 0;
		}

		if (0 == lua_getinfo(L, "S", &debugInfo))
		{
			return 0;
		}

		if (debugInfo.source[0] == '@')
		{
			ja::ScriptManager*   scriptManager   = ja::ScriptManager::getSingleton();
			ja::ResourceManager* resourceManager = ja::ResourceManager::getSingleton();

			ja::string fileInScriptPath = debugInfo.source;
			bool       isInScriptPath   = false;

			resourceManager->getInScriptPath(fileInScriptPath, isInScriptPath);

			if (false == isInScriptPath)
			{
				ja::LogManager::getSingleton()->addLogMessage(ja::LogType::ERROR_LOG, "File %s needs to be in %s", fileInScriptPath.c_str(), ja::ResourceManager::getSingleton()->getScriptsPath());
				return 0;
			}

			ja::ScriptFunction* initDataFunction   = nullptr;

			if (argsNum > 2)
			{
				if(LUA_TTABLE == lua_type(L, 3))
				{
					lua_rawgeti(L, 3, 1);
					const char*    initFunctionName = lua_tostring(L, -1);
					const uint32_t initFunctionHash = ja::ScriptFunction::getHash(initFunctionName);
					lua_pop(L, 1);

					lua_rawgeti(L, 3, 2);
					const char*    initFileName = lua_tostring(L, -1);
					const uint32_t initFileHash = ja::ScriptFunction::getHash(initFileName);
					lua_pop(L, 1);	

					initDataFunction = (ja::ScriptFunction*)scriptManager->getScriptFunction(ja::ScriptFunctionType::INIT_DATA, initFileHash, initFunctionHash);
				}
				
			}

			//check if exists data model with that name
			ja::Resource<ja::DataModel>* existingDataModel = resourceManager->getDataModel(dataModelName);

			ja::DataModel*                     dataModel  = new ja::DataModel(dataModelName, fileInScriptPath.c_str(), initDataFunction);
			StackAllocator<ja::DataFieldInfo>* dataFields = dataModel->getDataFields();

			uint32_t fieldOffset = 0;
			uint32_t nameOffset  = 0;

			lua_pushnil(L);                // put a nil key on stack
			while (lua_next(L, 2) != 0) { // key(-1) is replaced by the next key(-1) in table(-2)

				ja::DataFieldInfo dataFieldInfo;

				const char*    fieldName       = lua_tostring(L, -2);  // Get key(-2) name
				const uint32_t fieldNameLength = strlen(fieldName);

				//const int32_t fieldHash = ja::DataFieldInfo::getHash(fieldName);

				lua_rawgeti(L, -1, 1);
				const uint32_t fieldElements = lua_tointeger(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				const ja::DataFieldInfo* pDataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, -1);
				lua_pop(L, 1);

				memcpy(&dataFieldInfo,              pDataFieldInfo, sizeof(ja::DataFieldInfo));
				//memcpy(dataFieldInfo.dataFieldName, fieldName,      fieldNameLength);
				dataFieldInfo.dataFieldName.setName(fieldName);
				

				dataFieldInfo.dataFieldSize   = (fieldElements * ja::DataTypeInfo::typeSize[(int32_t)dataFieldInfo.dataType]);
				dataFieldInfo.dataFieldOffset = fieldOffset;
				dataFieldInfo.dataNameHash    = dataFieldInfo.dataFieldName.getHash();

				lua_pop(L, 1);               // remove value(-1), now key on top at(-1)


				dataFields->push(dataFieldInfo);

				fieldOffset += dataFieldInfo.dataFieldSize;
			}

			dataModel->dataSize = fieldOffset;


			//replace old data model with new if exists model name with that same name
			if (nullptr != existingDataModel)
			{
				ja::LogManager::getSingleton()->addLogMessage(ja::LogType::WARNNING_LOG, "Material with name %s already exists", dataModelName);

				ja::DataGroup::getSingleton()->updateDataModel(existingDataModel->resource, dataModel);
				delete existingDataModel->resource;
				existingDataModel->resource = dataModel;
			}
			else
			{
				resourceManager->addDataModel(dataModelName, dataModel);
			}

			resourceManager->addScript(fileInScriptPath.c_str());		
			
			
			{
				//Add data model to data model table
				lua_getglobal(L, "DataModel");
				lua_pushstring(L, dataModelName);
				lua_pushlightuserdata(L, dataModel);
				lua_settable(L, -3);
			}

			dataModel->createModelTable();
		}
		else
		{
			ja::LogManager::getSingleton()->addLogMessage(ja::LogType::ERROR_LOG, "Data model %s can't be added to data models it is not in a file", dataModelName);
		}
	}

	return 0;
}

int32_t ResourceManager::addMaterial(lua_State* L)
{
	ja::LogManager*      logManager      = ja::LogManager::getSingleton();
	ja::ResourceManager* resourceManager = ja::ResourceManager::getSingleton();

	const uint32_t argsNum               = lua_gettop(L);
	const char*    materialName          = lua_tostring(L, 1);
	const char*    materialShaderDefName = lua_tostring(L, 2);
	
	ja::Resource<ja::Material>* existingMaterial = resourceManager->getMaterial(materialName);

	if (existingMaterial != nullptr)
	{
		logManager->addLogMessage(ja::LogType::ERROR_LOG, "Material %s already exists", materialName);

		lua_pushlightuserdata(L, existingMaterial->resource);
		return 1;
	}

	ja::Resource<ja::ShaderStructureDef>* materialShaderDefResource = resourceManager->getMaterialShaderDef(materialShaderDefName);

	if (nullptr == materialShaderDefResource)
	{
		logManager->addLogMessage(ja::LogType::ERROR_LOG, "Material structure %s does not exist", materialShaderDefName);

		lua_pushlightuserdata(L, nullptr);
		return 1;
	}

	ja::Resource<ja::Material>* materialResource = resourceManager->createMaterial(materialName, materialShaderDefResource->resource);
			
	{
		//Add material to material table table
		lua_getglobal(L, "Materials");
		lua_pushstring(L, materialName);
		lua_pushlightuserdata(L, materialResource->resource);
		lua_settable(L, -3);
	}

	lua_pushlightuserdata(L, materialResource->resource);

	return 1;
}

int32_t ResourceManager::addMaterialShaderDef(lua_State* L)
{
	const uint32_t argsNum               = lua_gettop(L);
	const char*    materialShaderDefName = lua_tostring(L, 1);
	const char*    shaderBinderName      = lua_tostring(L, 2);

	ja::LogManager*      logManager      = ja::LogManager::getSingleton();
	ja::ResourceManager* resourceManager = ja::ResourceManager::getSingleton();
	
	ja::Resource<ja::ShaderBinder>* shaderBinderResource = resourceManager->getShaderBinder(shaderBinderName);
	
	if (nullptr == shaderBinderResource)
	{
		logManager->addLogMessage(ja::LogType::ERROR_LOG, "Can't find %s shader binder", shaderBinderName);
		return 0;
	}

	ja::Resource<ja::ShaderStructureDef>* materialShaderDefResource = resourceManager->getMaterialShaderDef(materialShaderDefName);

	if (nullptr != materialShaderDefResource)
	{
		logManager->addLogMessage(ja::LogType::ERROR_LOG, "Material core %s already exists", materialShaderDefName);
		return 0;
	}

	if (LUA_TTABLE == lua_type(L, 3))
	{
		lua_Debug debugInfo;

		if (1 != lua_getstack(L, 1, &debugInfo))
		{
			return 0;
		}

		if (0 == lua_getinfo(L, "S", &debugInfo))
		{
			return 0;
		}

		if (debugInfo.source[0] == '@')
		{
			
			ja::ScriptManager* scriptManager = ja::ScriptManager::getSingleton();
			
			ja::string fileInScriptPath = debugInfo.source;
			bool       isInScriptPath = false;

			resourceManager->getInScriptPath(fileInScriptPath, isInScriptPath);

			if (false == isInScriptPath)
			{
				logManager->addLogMessage(ja::LogType::ERROR_LOG, "File %s needs to be in %s", fileInScriptPath.c_str(), ja::ResourceManager::getSingleton()->getScriptsPath());
				return 0;
			}

			const ja::UniformBlockInfo* materialInfo      = shaderBinderResource->resource->getMaterialUniformBlock();
			ja::ShaderBinder*           shaderBinder      = shaderBinderResource->resource;
			ja::ShaderStructureDef*     materialShaderDef = new ja::ShaderStructureDef(materialShaderDefName, fileInScriptPath.c_str(), shaderBinder, shaderBinder->getMaterialSize());
			
			StackAllocator<ja::DataFieldInfo>* dataFields = &materialShaderDef->dataFields;

			lua_pushnil(L);                // put a nil key on stack
			while (lua_next(L, 3) != 0) { // key(-1) is replaced by the next key(-1) in table(-2)

				ja::DataFieldInfo dataFieldInfo;

				const char*    fieldName = lua_tostring(L, -2);  // Get key(-2) name

				lua_rawgeti(L, -1, 1);
				const uint32_t fieldElements = lua_tointeger(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				const ja::DataFieldInfo* pDataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, -1);
				lua_pop(L, 1);

				memcpy(&dataFieldInfo, pDataFieldInfo, sizeof(ja::DataFieldInfo));
				dataFieldInfo.dataFieldName.setName(fieldName);

				dataFieldInfo.dataFieldSize   = (fieldElements * ja::DataTypeInfo::typeSize[(int32_t)dataFieldInfo.dataType]);
				dataFieldInfo.dataNameHash    = dataFieldInfo.dataFieldName.getHash();

				lua_pop(L, 1);               // remove value(-1), now key on top at(-1)

				const ja::UniformField* pUniformField = materialInfo->getUniformField(dataFieldInfo.dataFieldName.getHash());

				if (nullptr != pUniformField)
				{
					dataFieldInfo.dataFieldOffset = pUniformField->fieldOffset;
					dataFields->push(dataFieldInfo);
				}
			}

			resourceManager->addScript(fileInScriptPath.c_str());
			resourceManager->addMaterialShaderDef(materialShaderDefName, materialShaderDef);

			{
				//Add data model to data model table
				lua_getglobal(L, "MaterialDefs");
				lua_pushstring(L, materialShaderDefName);
				lua_pushlightuserdata(L, materialShaderDef);
				lua_settable(L, -3);
			}

			{
				//Create material data table
				lua_getglobal(L, "MaterialDef");
				lua_pushstring(L, materialShaderDefName);
				lua_newtable(L);

				StackAllocator<ja::DataFieldInfo>* dataFields = &materialShaderDef->dataFields;

				const uint32_t     dataFieldsNum = dataFields->getSize();
				ja::DataFieldInfo* pDataField    = dataFields->getFirst();

				for (uint32_t i = 0; i < dataFieldsNum; ++i)
				{
					lua_pushstring(L, pDataField[i].dataFieldName.getName());
					lua_pushlightuserdata(L, &pDataField[i]);
					lua_settable(L, -3);
				}

				lua_settable(L, -3);
			}
			
		}
		else
		{
			ja::LogManager::getSingleton()->addLogMessage(ja::LogType::ERROR_LOG, "Material def %s can't be added it is not in a file", materialShaderDefName);
		}
	}

	return 1;
}

int32_t ResourceManager::addShaderObjectDef(lua_State* L)
{
	const uint32_t argsNum          = lua_gettop(L);
	const char*    shaderBinderName = lua_tostring(L, 1);

	ja::LogManager*      logManager      = ja::LogManager::getSingleton();
	ja::ResourceManager* resourceManager = ja::ResourceManager::getSingleton();

	ja::Resource<ja::ShaderBinder>* shaderBinderResource = resourceManager->getShaderBinder(shaderBinderName);

	if (nullptr == shaderBinderResource)
	{
		logManager->addLogMessage(ja::LogType::ERROR_LOG, "Can't find %s shader binder", shaderBinderName);
		return 0;
	}

	ja::Resource<ja::ShaderStructureDef>* shaderObjectDefResource = resourceManager->getShaderObjectDef(shaderBinderName);

	if (nullptr != shaderObjectDefResource)
	{
		logManager->addLogMessage(ja::LogType::ERROR_LOG, "Shader object def %s already exists", shaderBinderName);
		return 0;
	}

	if (LUA_TTABLE == lua_type(L, 2))
	{
		lua_Debug debugInfo;

		if (1 != lua_getstack(L, 1, &debugInfo))
		{
			return 0;
		}

		if (0 == lua_getinfo(L, "S", &debugInfo))
		{
			return 0;
		}

		if (debugInfo.source[0] == '@')
		{

			ja::ScriptManager* scriptManager = ja::ScriptManager::getSingleton();

			ja::string fileInScriptPath = debugInfo.source;
			bool       isInScriptPath = false;

			resourceManager->getInScriptPath(fileInScriptPath, isInScriptPath);

			if (false == isInScriptPath)
			{
				logManager->addLogMessage(ja::LogType::ERROR_LOG, "File %s needs to be in %s", fileInScriptPath.c_str(), ja::ResourceManager::getSingleton()->getScriptsPath());
				return 0;
			}

			const ja::UniformBlockInfo* shaderObjectInfo = shaderBinderResource->resource->getShaderObjectUniformBlock();
			ja::ShaderBinder*               shaderBinder = shaderBinderResource->resource;
			ja::ShaderStructureDef*     shaderObjectDef  = new ja::ShaderStructureDef(shaderBinderName, fileInScriptPath.c_str(), shaderBinder, shaderBinder->getShaderObjectSize());

			StackAllocator<ja::DataFieldInfo>* dataFields = &shaderObjectDef->dataFields;

			lua_pushnil(L);                // put a nil key on stack
			while (lua_next(L, 2) != 0) { // key(-1) is replaced by the next key(-1) in table(-2)

				ja::DataFieldInfo dataFieldInfo;

				const char*    fieldName = lua_tostring(L, -2);  // Get key(-2) name

				lua_rawgeti(L, -1, 1);
				const uint32_t fieldElements = lua_tointeger(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				const ja::DataFieldInfo* pDataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, -1);
				lua_pop(L, 1);

				memcpy(&dataFieldInfo, pDataFieldInfo, sizeof(ja::DataFieldInfo));
				dataFieldInfo.dataFieldName.setName(fieldName);

				dataFieldInfo.dataFieldSize = (fieldElements * ja::DataTypeInfo::typeSize[(int32_t)dataFieldInfo.dataType]);
				dataFieldInfo.dataNameHash = dataFieldInfo.dataFieldName.getHash();

				lua_pop(L, 1);               // remove value(-1), now key on top at(-1)

				const ja::UniformField* pUniformField = shaderObjectInfo->getUniformField(dataFieldInfo.dataFieldName.getHash());

				if (nullptr != pUniformField)
				{
					dataFieldInfo.dataFieldOffset = pUniformField->fieldOffset;
					dataFields->push(dataFieldInfo);
				}
			}

			resourceManager->addScript(fileInScriptPath.c_str());
			resourceManager->addShaderObjectDef(shaderBinderName, shaderObjectDef);

			{
				//Add data model to data model table
				lua_getglobal(L, "ShaderObjectDefs");
				lua_pushstring(L, shaderBinderName);
				lua_pushlightuserdata(L, shaderObjectDef);
				lua_settable(L, -3);
			}

			{
				//Create material data table
				lua_getglobal(L, "ShaderObjectDef");
				lua_pushstring(L, shaderBinderName);
				lua_newtable(L);

				StackAllocator<ja::DataFieldInfo>* dataFields = &shaderObjectDef->dataFields;

				const uint32_t     dataFieldsNum = dataFields->getSize();
				ja::DataFieldInfo* pDataField    = dataFields->getFirst();

				for (uint32_t i = 0; i < dataFieldsNum; ++i)
				{
					lua_pushstring(L, pDataField[i].dataFieldName.getName());
					lua_pushlightuserdata(L, &pDataField[i]);
					lua_settable(L, -3);
				}

				lua_settable(L, -3);
			}
		}
		else
		{
			ja::LogManager::getSingleton()->addLogMessage(ja::LogType::ERROR_LOG, "Shader object def %s can't be added it is not in a file", shaderBinderName);
		}
	}

	return 1;
}

int32_t ResourceManager::createShaderBinder(lua_State* L)
{
	ja::ResourceManager* resourceManager = ja::ResourceManager::getSingleton();
	ja::LogManager*      logManager      = ja::LogManager::getSingleton();

	const char* shaderBinderName   = lua_tostring(L, 1);
	const char* vertexShaderName   = lua_tostring(L, 2);
	const char* fragmentShaderName = lua_tostring(L, 3);
	const char* geometryShaderName = "";

	if (4 == lua_gettop(L))
	{
		geometryShaderName = lua_tostring(L, 4);
	}

	ja::Resource<ja::ShaderBinder>* shaderBinderResource = resourceManager->createShaderBinder(shaderBinderName, vertexShaderName, fragmentShaderName, geometryShaderName);

	if (nullptr != shaderBinderResource)
	{
		lua_pushlightuserdata(L, shaderBinderResource->resource);
	}
	else
	{
		lua_pushlightuserdata(L, nullptr);
	}

	return 1;
}


}