#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Math/Transform2.hpp"

#include <stdint.h>

namespace jas
{
	class Transform2
	{
	private:
		static const char className[];

		static int32_t create(lua_State* L)
		{
			jaVector2* position    = (jaVector2*)lua_touserdata(L, 1);
			float      rotationRad = (float)lua_tonumber(L, 2);

			jaTransform2* transform = (jaTransform2*)lua_newuserdata(L, sizeof(jaTransform2));
			transform->position = *position;
			transform->rotationMatrix.setRadians(rotationRad);
		
			return 1;
		}

	public:
		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, create, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, isSleeping, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, getPrevious, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

	const char Transform2::className[] = "Transform2";
}