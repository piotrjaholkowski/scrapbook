#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Gilgamesh/Shapes/Box2d.hpp"

namespace jas
{
	class Box
	{
	private:
		static const char className[];

		static int32_t setWidth(lua_State* L);
		static int32_t setHeight(lua_State* L);

	public:
		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, setWidth,  table);
			JAS_ADD_SCRIPT_FUNCTION(L, setHeight, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

}