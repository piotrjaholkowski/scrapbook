#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Graphics/DrawElements.hpp"

#include <stdint.h>

namespace jas
{
	class DrawElements
	{
	private:
		static const char className[];

		static int32_t setVerticesColors(lua_State* L);
		static int32_t setVerticesPositions(lua_State* L);
		static int32_t setTriangleIndices(lua_State* L);

	public:

		static void save(FILE* pFile, const char* drawElementsVar, const ja::DrawElements& drawElements);

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, setVerticesPositions, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setVerticesColors, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setTriangleIndices, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};
}