#include "Box.hpp"

namespace jas
{

const char Box::className[] = "Box";

int32_t Box::setWidth(lua_State* L)
{
	gil::Box2d* box = (gil::Box2d*)(lua_touserdata(L, 1));
	const float width = (float)lua_tonumber(L, 2);

	box->setWidth(width);

	return 0;
}

int32_t Box::setHeight(lua_State* L)
{
	gil::Box2d* box = (gil::Box2d*)(lua_touserdata(L, 1));
	const float height = (float)lua_tonumber(L, 2);

	box->setWidth(height);

	return 0;
}

}