#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Gui/Widget.hpp"
#include "../Gui/Editbox.hpp"

namespace jas
{
	class Editbox
	{
	private:
		static const char className[];

		static int getText(lua_State* L)
		{
			ja::Editbox* editbox = (ja::Editbox*)lua_touserdata(L, 1);

			lua_pushstring(L,editbox->getText().c_str());
			return 1;
		}

		static int32_t getAsInt(lua_State* L)
		{
			ja::Editbox* editbox = (ja::Editbox*)lua_touserdata(L, 1);

			int intValue;
			editbox->getAsInt(&intValue);

			lua_pushinteger(L,intValue);
			return 1;
		}

		static int32_t setOnClick(lua_State* L)
		{
			ja::Editbox* editbox = (ja::Editbox*)lua_touserdata(L, 1);
			ja::ScriptDelegate* scriptDelegate = (ja::ScriptDelegate*)lua_touserdata(L,2);

			editbox->setOnClickEvent(scriptDelegate);

			return 0;
		}

		static int32_t setOnMouseOver(lua_State* L)
		{
			ja::Editbox* editbox = (ja::Editbox*)lua_touserdata(L, 1);
			ja::ScriptDelegate* scriptDelegate = (ja::ScriptDelegate*)lua_touserdata(L,2);

			editbox->setOnMouseOverEvent(scriptDelegate);

			return 0;
		}

		static int32_t setOnMouseLeave(lua_State* L)
		{
			ja::Editbox* editbox = (ja::Editbox*)lua_touserdata(L, 1);
			ja::ScriptDelegate* scriptDelegate = (ja::ScriptDelegate*)lua_touserdata(L,2);

			editbox->setOnMouseLeaveEvent(scriptDelegate);

			return 0;
		}

		static int32_t setColor(lua_State* L)
		{
			ja::Editbox* editbox = (ja::Editbox*)lua_touserdata(L, 1);

			uint8_t r = (uint8_t)lua_tointeger(L,2);
			uint8_t g = (uint8_t)lua_tointeger(L,3);
			uint8_t b = (uint8_t)lua_tointeger(L,4);
			uint8_t a = (uint8_t)lua_tointeger(L,5);

			editbox->setColor(r,g,b,a);

			return 0;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,getText,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getAsInt,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setOnClick,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setOnMouseOver,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setOnMouseLeave,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setColor,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char Editbox::className[] = "Editbox";
}
