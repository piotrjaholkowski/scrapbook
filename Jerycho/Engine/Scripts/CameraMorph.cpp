#include "CameraMorph.hpp"

#include "../Graphics/Camera.hpp"

namespace jas
{

	const char CameraMorph::className[] = "CameraMorph";

	int32_t CameraMorph::setPosition(lua_State* L)
	{
		ja::CameraMorph*  cameraMorph = (ja::CameraMorph*)lua_touserdata(L, 1);
		jaVector2*        position    = (jaVector2*)lua_touserdata(L, 2);

		cameraMorph->transform.position = *position;

		return 0;
	}

	int32_t CameraMorph::setRotation(lua_State* L)
	{
		ja::CameraMorph* cameraMorph = (ja::CameraMorph*)lua_touserdata(L, 1);
		float            angleRad    = (float)lua_tonumber(L, 2);

		cameraMorph->transform.rotationMatrix.setRadians(angleRad);

		return 0;
	}

	int32_t CameraMorph::setScale(lua_State* L)
	{
		ja::CameraMorph* cameraMorph = (ja::CameraMorph*)lua_touserdata(L, 1);
		jaVector2*       scale       = (jaVector2*)lua_touserdata(L, 2);

		cameraMorph->scale = *scale;

		return 0;
	}

	int32_t CameraMorph::setActiveFeatures(lua_State* L)
	{
		ja::CameraMorph*  cameraMorph = (ja::CameraMorph*)lua_touserdata(L, 1);
		uint32_t activeFeatures = (uint32_t)lua_tonumber(L, 2);

		cameraMorph->setActiveFeatures(activeFeatures);

		return 0;
	}

}
