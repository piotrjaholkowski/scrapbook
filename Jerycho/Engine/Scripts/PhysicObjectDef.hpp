#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Physics/PhysicObjectDef2d.hpp"

namespace jas
{
	class PhysicObjectDef
	{
	private:
		static const char className[];

		static int32_t setPosition(lua_State* L);
		static int32_t setRotation(lua_State* L);
		static int32_t setBodyType(lua_State* L);
		static int32_t setLinearVelocity(lua_State* L);
		static int32_t setGravity(lua_State* L);
		static int32_t setAngluarVelocity(lua_State* L);
		static int32_t setLinearDamping(lua_State* L);
		static int32_t setAngluarDamping(lua_State* L);
		static int32_t setFixedRotation(lua_State* L);
		static int32_t setAllowSleep(lua_State* L);
		static int32_t setSleep(lua_State* L);
		static int32_t setGroupIndex(lua_State* L);
		static int32_t setCategoryMask(lua_State* L);
		static int32_t setMaskBits(lua_State* L);
		static int32_t setShapeType(lua_State* L);
		static int32_t setTimeFactor(lua_State* L);
		static int32_t getShape(lua_State* L);

	public:

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, setPosition,        table);
			JAS_ADD_SCRIPT_FUNCTION(L, setRotation,        table);
			JAS_ADD_SCRIPT_FUNCTION(L, setBodyType,        table);
			JAS_ADD_SCRIPT_FUNCTION(L, setLinearVelocity,  table);
			JAS_ADD_SCRIPT_FUNCTION(L, setAngluarVelocity, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setLinearDamping,   table);
			JAS_ADD_SCRIPT_FUNCTION(L, setAngluarDamping,  table);
			JAS_ADD_SCRIPT_FUNCTION(L, setFixedRotation,   table);
			JAS_ADD_SCRIPT_FUNCTION(L, setAllowSleep,      table);
			JAS_ADD_SCRIPT_FUNCTION(L, setSleep,           table);
			JAS_ADD_SCRIPT_FUNCTION(L, setGravity,         table);
			JAS_ADD_SCRIPT_FUNCTION(L, setGroupIndex,      table);
			JAS_ADD_SCRIPT_FUNCTION(L, setCategoryMask,    table);
			JAS_ADD_SCRIPT_FUNCTION(L, setMaskBits,        table);
			JAS_ADD_SCRIPT_FUNCTION(L, setShapeType,       table);
			JAS_ADD_SCRIPT_FUNCTION(L, setTimeFactor,      table);
			JAS_ADD_SCRIPT_FUNCTION(L, getShape,           table);

			//JAS_ADD_SCRIPT_FUNCTION(L, isSleeping, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, getPrevious, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

	
}