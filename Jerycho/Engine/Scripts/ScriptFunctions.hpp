#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Manager/GameManager.hpp"
#include "../Manager/LevelManager.hpp"
#include <string>



#define JAS_ADD_SCRIPT_FUNCTION(L, FUNCTIONNAME, TABLE) lua_pushliteral(L,#FUNCTIONNAME); lua_pushcfunction(L,FUNCTIONNAME); lua_settable(L,TABLE);

namespace ja
{

/*
//Graphics
static int32_t setCamera(lua_State* L); // setCamera(string cameraName);

//Level manager
static int32_t saveLevel(lua_State* L); // saveLevel(string levelName);
static int32_t loadLevel(lua_State* L); // loadLevel(string levelName);
static int32_t reloadLevel(lua_State* L); // reloadLevel

//Graphics
static int32_t setCamera(lua_State *L)
{

	int32_t n = lua_gettop(L);
	if(n!=1) return 0;
	const char* cameraName = lua_tostring(L,1);
	GameManager::getSingleton()->sendMessage(SET_ACTIVE_CAMERA2D,cameraName);

	return 0;
}

//Level manager
static int32_t saveLevel(lua_State* L)
{
	int32_t n = lua_gettop(L);
	if(n!=1) return 0;
	const char* levelName = lua_tostring(L,1);
	//LevelManager::getSingleton()->saveLevel(levelName);

	return 0;
}

static int32_t loadLevel(lua_State* L)
{
	int32_t n = lua_gettop(L);
	if(n!=1) return 0;
	const char* levelName = lua_tostring(L,1);
	//LevelManager::getSingleton()->loadLevelSeparateThread(levelName);

	return 0;
}

static int32_t reloadLevel(lua_State* L)
{
	//LevelManager::getSingleton()->reloadLevel();

	return 0;
}*/

}