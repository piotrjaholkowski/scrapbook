#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Graphics/Polygon.hpp"

namespace jas
{

class PolygonMorphElements
{
private:
	static const char className[];

public:

	static void save(FILE* pFile, const char* morphElementsVar, const ja::PolygonMorphElements& polygonMorphElements);


	static void registerClass(lua_State* L)
	{
		lua_newtable(L);
		int32_t table = lua_gettop(L);

		//JAS_ADD_SCRIPT_FUNCTION(L, getDrawElements, table);

		lua_setglobal(L, className); // setglobal remove table from stack
	}
};

}