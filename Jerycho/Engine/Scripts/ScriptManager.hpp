#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Manager/ScriptManager.hpp"

namespace jas
{
	class ScriptManager
	{
	private:
		static const char className[];

		static int32_t addFunction(lua_State* L);

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, addFunction, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	
}
