#include "ScriptManager.hpp"
#include "../Manager/LogManager.hpp"
#include "../Manager/ResourceManager.hpp"

namespace jas
{

const char ScriptManager::className[] = "ScriptManager";

int32_t ScriptManager::addFunction(lua_State* L)
{
	lua_Debug debugInfo;

	if (1 != lua_getstack(L, 1, &debugInfo))
	{
		return 0;
	}

	if (0 == lua_getinfo(L, "S", &debugInfo))
	{
		return 0;
	}

	if (debugInfo.source[0] == '@')
	{
		ja::ScriptManager* scriptManager = ja::ScriptManager::getSingleton();

		uint8_t     functionType = (uint8_t)lua_tointeger(L, 1);
		const char* functionName = lua_tostring(L, 2);
		int32_t     functionRef  = luaL_ref(L, LUA_REGISTRYINDEX);

		ja::string fileInScriptPath = debugInfo.source;
		bool       isInScriptPath   = false;

		ja::ResourceManager::getSingleton()->getInScriptPath(fileInScriptPath, isInScriptPath);


		if (false == isInScriptPath)
		{
			//ja::LogManager::getSingleton()->addLogLine("File %s needs to be in %s", fileInScriptPath.c_str(), ja::ResourceManager::getSingleton()->getScriptsPath());
			ja::LogManager::getSingleton()->addLogMessage(ja::LogType::ERROR_LOG, "File %s needs to be in %s", fileInScriptPath.c_str(), ja::ResourceManager::getSingleton()->getScriptsPath());
			return 0;
		}

		ja::ScriptFunction scriptFunction;
 
		strcpy(scriptFunction.functionName, functionName);
		scriptFunction.functionHash = ja::ScriptFunction::getHash(scriptFunction.functionName);
		strcpy(scriptFunction.fileName, fileInScriptPath.c_str());
		scriptFunction.fileHash     = ja::ScriptFunction::getHash(scriptFunction.fileName);
		scriptFunction.functionRef  = functionRef;
		scriptFunction.functionType = functionType;

		//std::cout << "File: " << scriptFunction.fileName << std::endl;
		//std::cout << "Function: " <<  scriptFunction.functionName << std::endl;
		//std::cout << "Function type: " << scriptFunction.functionType << std::endl;
		//std::cout << "Function ref: " << scriptFunction.functionRef << std::endl;

		scriptManager->registerFunction(scriptFunction);
	}
	else
	{
		const char* functionName = lua_tostring(L, 2);

		//ja::LogManager::getSingleton()->addLogLine("Function %s can't be added to script functions it is not in a file", functionName);
		ja::LogManager::getSingleton()->addLogMessage(ja::LogType::ERROR_LOG, "Function %s can't be added to script functions it is not in a file", functionName);
	}

	return 0;
}


}