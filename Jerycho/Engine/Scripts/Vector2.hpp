#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

namespace jas
{
	class Vector2
	{
	private:
		static const char className[];
	public:
		static jaVector2* push(lua_State* L)
		{
			jaVector2 *vector2d = (jaVector2*)lua_newuserdata(L, sizeof(jaVector2));
			//luaL_getmetatable(L, className);
			//lua_setmetatable(L, -2);
			return vector2d;
		}

		static int32_t create(lua_State* L)
		{
			float x = (float)(lua_tonumber(L,1));
			float y = (float)(lua_tonumber(L,2));

			//jaVector2 *vector2d = (jaVector2*)lua_newuserdata(L, sizeof(jaVector2));
			//vector2d->x = x;
			//vector2d->y = y;
			jaVector2* vector2d = new (lua_newuserdata(L, sizeof(jaVector2))) jaVector2(x, y);

			//luaL_getmetatable(L, className);
			//lua_setmetatable(L, -2);

			return 1;
		}

		static int32_t getX(lua_State* L)
		{
			jaVector2* vectorA = (jaVector2*)lua_touserdata(L,1);
			lua_pushnumber(L,vectorA->x);

			return 1;
		}

		static int32_t getY(lua_State* L)
		{
			jaVector2* vectorA = (jaVector2*)lua_touserdata(L,1);
			lua_pushnumber(L,vectorA->y);

			return 1;
		}

		static int32_t setX(lua_State* L)
		{
			jaVector2* vectorA = (jaVector2*)lua_touserdata(L,1);
			vectorA->x = (float)(lua_tonumber(L,2));

			return 0;
		}

		static int32_t setY(lua_State* L)
		{
			jaVector2* vectorA = (jaVector2*)lua_touserdata(L,1);
			vectorA->y = (float)(lua_tonumber(L,2));

			return 0;
		}

		static int32_t length(lua_State* L)
		{
			jaVector2& vectorA = *(jaVector2*)(lua_touserdata(L,1));
			lua_pushnumber(L,jaVectormath2::length(vectorA));

			return 1;
		}

		static int32_t normalize(lua_State* L)
		{
			jaVector2& vectorA = *(jaVector2*)(lua_touserdata(L,1));
			lua_pushnumber(L,vectorA.normalize());

			return 1;
		}

		static int32_t unmVectors(lua_State* L)
		{
			jaVector2* vectorA  = (jaVector2*)(lua_touserdata(L,1));
			jaVector2* vector2d = (jaVector2*)(lua_newuserdata(L, sizeof(jaVector2)));
			vector2d->x = -vectorA->x;
			vector2d->y = -vectorA->y;
			luaL_getmetatable(L, className);
			lua_setmetatable(L, -2);

			return 1;
		}

		static int32_t index(lua_State* L)
		{
			jaVector2* vectorA = (jaVector2*)lua_touserdata(L,1);
			const char* field =  lua_tostring(L, 2);
			int32_t size = strlen(field);

			if(size == 1)
			{
				if(field[0] == 'x')
				{
					lua_pushnumber(L,vectorA->x);
					return 1;
				}

				if(field[0] == 'y')
				{
					lua_pushnumber(L,vectorA->y);
					return 1;
				}
			}

			luaL_getmetatable(L,className);
			lua_pushlstring(L,field,size);
			lua_gettable(L,3); // function on stack

			return 1;
		}

		static int32_t add(lua_State* L)
		{
			jaVector2* vectorA = (jaVector2*)lua_touserdata(L,1);
			jaVector2* vectorB = (jaVector2*)lua_touserdata(L,2);

			jaVector2* vector2d = (jaVector2*)lua_newuserdata(L, sizeof(jaVector2));
			*vector2d = *vectorA + *vectorB;
			luaL_getmetatable(L, className);
			lua_setmetatable(L, -2);

			return 1;
		}

		static int32_t sub(lua_State* L)
		{
			jaVector2& vectorA = *(jaVector2*)lua_touserdata(L,1);
			jaVector2& vectorB = *(jaVector2*)lua_touserdata(L,2);

			jaVector2& vector2d = *(jaVector2*)lua_newuserdata(L, sizeof(jaVector2));
			vector2d.x = vectorA.x - vectorB.x;
			vector2d.y = vectorA.y - vectorB.y; 
			luaL_getmetatable(L, className);
			lua_setmetatable(L, -2);

			return 1;
		}

		static int32_t toString(lua_State* L)
		{
			jaVector2* vector2d = (jaVector2*)lua_touserdata(L,1);

			lua_pushfstring(L, "x=%f y=%f", vector2d->x, vector2d->y);
			return 1;
		}
		//scripter interface

		//static void pushOnStack(lua_State* L, jaVector2* v)
		//{
		//	lua_pushlightuserdata(L,(void*)v);
		//	luaL_getmetatable(L, className);
		//	lua_setmetatable(L, -2);
		//}

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			//luaL_newmetatable(L, className);
			//int32_t metatable   = lua_gettop(L);
			
			JAS_ADD_SCRIPT_FUNCTION(L, create, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getX, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getY, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setX, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setY, table);
			//JAS_ADD_SCRIPT_FUNCTION(L,create,metatable);
			//JAS_ADD_SCRIPT_FUNCTION(L,add,metatable);
			//JAS_ADD_SCRIPT_FUNCTION(L,sub,metatable);
			//JAS_ADD_SCRIPT_FUNCTION(L,x,metatable);
			//JAS_ADD_SCRIPT_FUNCTION(L,y,metatable);
			//JAS_ADD_SCRIPT_FUNCTION(L,setX,metatable);
			//JAS_ADD_SCRIPT_FUNCTION(L,setY,metatable);
			//JAS_ADD_SCRIPT_FUNCTION(L,length,metatable);
			//JAS_ADD_SCRIPT_FUNCTION(L,normalize,metatable);

			//lua_pushliteral(L,"__index");
			//lua_pushcfunction(L,index);
			//lua_settable(L,metatable);
			//
			//lua_pushliteral(L,"__unm");
			//lua_pushcfunction(L,unmVectors);
			//lua_settable(L, metatable);
			//
			//lua_pushliteral(L,"__add");
			//lua_pushcfunction(L,add);
			//lua_settable(L, metatable); 
			//
			//lua_pushliteral(L,"__sub");
			//lua_pushcfunction(L,sub);
			//lua_settable(L, metatable); 
			//
			//lua_pushliteral(L,"__tostring");
			//lua_pushcfunction(L,toString);
			//lua_settable(L,metatable);

			lua_setglobal(L,className);
		}

	};

	const char Vector2::className[] = "Vector2";
}
