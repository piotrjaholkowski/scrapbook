#pragma once

#include "Script.hpp"
#include "../Allocators/LinearAllocator.hpp"
#include "../Utility/String.hpp"

namespace ja
{

	class ScriptLoader
	{
	private:
		static ScriptLoader* singleton;
		
		//static const char pExportKeyword[];
		//static const char pInitEntityFuncKeyword[];
		//static const char pUpdateEntityFuncKeyword[];

		ScriptLoader();

		//bool getFunctionType(const char* keyword, ScriptFunctionType& functionType);
	public:
		static void init();
		static ScriptLoader* getSingleton();
		//static Script* loadScript(const ja::string& fileName, uint32_t resourceId);
		//static void loadScript(const ja::string& fileName, Script* script);
		static void loadScript(const ja::string& fileName);
		~ScriptLoader();
		static void cleanUp();
	};

}
