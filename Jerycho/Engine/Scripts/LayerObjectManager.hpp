#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Manager/LayerObjectManager.hpp"
#include "../Graphics/LayerObject.hpp"

namespace jas
{
	class LayerObjectManager
	{
	private:
		static const char className[];

		static int32_t getLayerObject(lua_State* L)
		{
			uint16_t         layerObjectId = (uint16_t)luaL_checkinteger(L, 1);
			ja::LayerObject* layerObject   = ja::LayerObjectManager::getSingleton()->getLayerObject(layerObjectId);

			if (layerObject == nullptr)
				lua_pushnil(L);
			else
				lua_pushlightuserdata(L, layerObject);

			return 1;
		}

		static int32_t getLayerObjectDef(lua_State* L)
		{
			lua_pushlightuserdata(L, &ja::LayerObjectManager::getSingleton()->layerObjectDef);

			return 1;
		}

		static int32_t createLayerObjectDef(lua_State* L)
		{
			ja::LayerObject* layerObject;

			int32_t d = lua_gettop(L);

			if (lua_gettop(L) == 0)
			{
				layerObject = ja::LayerObjectManager::getSingleton()->createLayerObjectDef();	
			}
			else
			{
				uint16_t layerObjectId = (uint16_t)lua_tointeger(L, 1);
				layerObject = ja::LayerObjectManager::getSingleton()->createLayerObjectDef(layerObjectId);
			}

			lua_pushlightuserdata(L, layerObject);

			return 1;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, getLayerObject,   table);
			JAS_ADD_SCRIPT_FUNCTION(L, getLayerObjectDef, table);
			JAS_ADD_SCRIPT_FUNCTION(L, createLayerObjectDef, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char LayerObjectManager::className[] = "LayerObjectManager";
}