#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "Vector2.hpp"

namespace jas
{
	class Matrix2
	{
	private:
		static const char className[];

	public:
		static jaMatrix2* push(lua_State* L)
		{
			jaMatrix2 *matrix2d = (jaMatrix2*)lua_newuserdata(L, sizeof(jaMatrix2));
			//luaL_getmetatable(L, className);
			//lua_setmetatable(L, -2);
			return matrix2d;
		}

		static int32_t create(lua_State* L)
		{
			float x = (float)(lua_tonumber(L,1));
			float y = (float)(lua_tonumber(L,2));

			jaVector2 *vector2d = (jaVector2*)lua_newuserdata(L, sizeof(jaVector2));
			vector2d->x = x;
			vector2d->y = y;
			//luaL_getmetatable(L, className);
			//lua_setmetatable(L, -2);

			return 1;
		}

		static int32_t rotateVector(lua_State* L)
		{
			jaMatrix2& matrix2 = *(jaMatrix2*)(lua_touserdata(L,1));
			jaVector2& vector2 = *(jaVector2*)(lua_touserdata(L,2));

			jaVector2& outVector = *Vector2::push(L);
			outVector = matrix2 * vector2;

			return 1;
		}

		static void pushOnStack(lua_State* L, jaVector2* v)
		{
			lua_pushlightuserdata(L,(void*)v);
			//luaL_getmetatable(L, className);
			//lua_setmetatable(L, -2);
		}

		static void registerClass(lua_State* L) {

			//luaL_newmetatable(L, className);
			//int metatable   = lua_gettop(L);
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			//JAS_ADD_SCRIPT_FUNCTION(L,rotateVector,metatable);
			JAS_ADD_SCRIPT_FUNCTION(L, create,       table);
			JAS_ADD_SCRIPT_FUNCTION(L, rotateVector, table);

			lua_setglobal(L,className);
		}

	};

	const char Matrix2::className[] = "Matrix2";
}