#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Entity/Entity.hpp"

namespace jas
{
	class Component
	{
	private:
		static const char className[];

		/*static int convertToHashTag(lua_State* L)
		{
			const char* hashTagName = lua_tostring(L,1);

			unsigned short hashTag = jaEntityComponent2d::convertToHashTag(hashTagName);
			lua_pushunsigned(L,hashTag);

			return 1;
		}*/

		static int32_t setTagName(lua_State* L);
		static int32_t getNext(lua_State* L);
		static int32_t getPrevious(lua_State* L);
		static int32_t getNextComponent(lua_State* L);
		static int32_t getPreviousComponent(lua_State* L);
		static int32_t getComponentType(lua_State* L);

	public:

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, setTagName, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getNext, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getPrevious, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getNextComponent, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getPreviousComponent, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getComponentType, table);
			//JAS_ADD_SCRIPT_FUNCTION(L,convertToHashTag,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

}