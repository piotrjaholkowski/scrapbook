#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Gui/Scrollbar.hpp"

namespace jas
{
	class Scrollbar
	{
	private:
		static const char className[];

		static int32_t setColor(lua_State* L)
		{
			ja::Scrollbar* scrollbar = (ja::Scrollbar*)lua_touserdata(L,1);

			uint8_t r = (uint8_t)lua_tointeger(L,2);
			uint8_t g = (uint8_t)lua_tointeger(L,3);
			uint8_t b = (uint8_t)lua_tointeger(L,4);
			uint8_t a = (uint8_t)lua_tointeger(L,5);

			scrollbar->setColor(r,g,b,a);

			return 0;
		}

		static int32_t getValue(lua_State* L)
		{
			ja::Scrollbar* scrollbar = (ja::Scrollbar*)lua_touserdata(L,1);
			lua_pushnumber(L,scrollbar->getValue());

			return 1;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,setColor,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getValue,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char Scrollbar::className[] = "Scrollbar";
}
