#include "LayerObjectComponent.hpp"

#include "../Entity/LayerObjectGroup.hpp"

namespace jas
{

	const char LayerObjectComponent::className[] = "LayerObjectComponent";

	int32_t LayerObjectComponent::create(lua_State* L)
	{
		ja::Entity* entity = (ja::Entity*)lua_touserdata(L, 1);

		ja::LayerObjectComponent* layerObjectComponent = ja::LayerObjectGroup::getSingleton()->createComponent(entity);
		//entity->addComponent(layerObjectComponent);

		//layerObjectComponent->

		lua_pushlightuserdata(L, layerObjectComponent);

		return 1;
	}

	int32_t LayerObjectComponent::setAutoDelete(lua_State* L)
	{
		ja::LayerObjectComponent* layerObjectComponent = (ja::LayerObjectComponent*)lua_touserdata(L, 1);
		bool isAutoDelete = (0 != lua_toboolean(L, 2));
		
		layerObjectComponent->setAutoDelete(isAutoDelete);

		return 0;
	}

	int32_t LayerObjectComponent::setFixedRotation(lua_State* L)
	{
		ja::LayerObjectComponent* layerObjectComponent = (ja::LayerObjectComponent*)lua_touserdata(L, 1);
		bool isFixedRotation = (0 != lua_toboolean(L, 2));

		layerObjectComponent->setFixedRotation(isFixedRotation); 

		return 0;
	}

	int32_t LayerObjectComponent::addLayerObject(lua_State* L)
	{
		ja::LayerObjectComponent* layerObjectComponent = (ja::LayerObjectComponent*)lua_touserdata(L, 1);
		ja::LayerObject*          layerObject          = (ja::LayerObject*)lua_touserdata(L, 2);

		layerObjectComponent->addLayerObject(layerObject);

		return 0;
	}

	int32_t LayerObjectComponent::setPhysicComponent(lua_State* L)
	{
		ja::LayerObjectComponent* layerObjectComponent = (ja::LayerObjectComponent*)lua_touserdata(L, 1);
		ja::PhysicComponent*      physicComponent      = (ja::PhysicComponent*)lua_touserdata(L, 2);

		layerObjectComponent->setPhysicComponent(physicComponent);

		return 0;
	}

}