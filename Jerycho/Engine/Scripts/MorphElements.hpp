#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Scripts/ScriptFunctions.hpp"

#include <stdint.h>

namespace jas
{

class MorphElements
{
private:
	static const char className[];
	

	static int32_t clear(lua_State* L);
	static int32_t createMorph(lua_State* L);

public:



static void registerClass(lua_State* L)
{
	lua_newtable(L);
	int32_t table = lua_gettop(L);

	JAS_ADD_SCRIPT_FUNCTION(L, clear, table);
	JAS_ADD_SCRIPT_FUNCTION(L, createMorph, table);

	lua_setglobal(L, className); // setglobal remove table from stack
}

};


}