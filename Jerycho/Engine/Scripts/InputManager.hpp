#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Manager/InputManager.hpp"

namespace jas
{
	class InputManager
	{
	private:
		static const char className[];

		static int32_t isKeyPressed(lua_State* L)
		{
			int32_t key = (int32_t)luaL_checkinteger(L,1);

			lua_pushboolean(L, ja::InputManager::isKeyPressed(key));
			return 1;
		}

		static int32_t isKeyPress(lua_State* L)
		{
			int32_t key = (int32_t)luaL_checkinteger(L, 1);

			lua_pushboolean(L, ja::InputManager::isKeyPress(key));
			return 1;
		}

		static int32_t isKeyReleased(lua_State* L)
		{
			int32_t key = (int32_t)luaL_checkinteger(L, 1);

			lua_pushboolean(L, ja::InputManager::isKeyReleased(key));
			return 1;
		}

		static int32_t isMouseButtonPressed(lua_State* L)
		{
			int32_t button = (int32_t)luaL_checkinteger(L, 1);

			lua_pushboolean(L, ja::InputManager::isMouseButtonPressed(button));
			return 1;
		}

		static int32_t isMouseButtonReleased(lua_State* L)
		{
			int32_t button = (int32_t)luaL_checkinteger(L, 1);

			lua_pushboolean(L, ja::InputManager::isMouseButtonReleased(button));
			return 1;
		}

		static int32_t isMouseButtonPress(lua_State* L)
		{
			int32_t button = (int32_t)luaL_checkinteger(L, 1);

			lua_pushboolean(L, ja::InputManager::isMouseButtonPress(button));
			return 1;
		}

		static int32_t getMousePositionXY(lua_State* L)
		{
			lua_pushinteger(L, ja::InputManager::getMousePositionX());
			lua_pushinteger(L, ja::InputManager::getMousePositionY());

			return 2;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,isKeyPressed,table);
			JAS_ADD_SCRIPT_FUNCTION(L,isKeyReleased,table);
			JAS_ADD_SCRIPT_FUNCTION(L,isKeyPress,table);
			JAS_ADD_SCRIPT_FUNCTION(L,isMouseButtonPressed,table);
			JAS_ADD_SCRIPT_FUNCTION(L,isMouseButtonReleased,table);
			JAS_ADD_SCRIPT_FUNCTION(L,isMouseButtonPress,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getMousePositionXY,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char InputManager::className[] = "InputManager";
}
