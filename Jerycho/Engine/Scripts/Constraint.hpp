#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Physics/Constraints2d.hpp"

namespace jas
{
	class Constraint
	{
	private:
		static const char className[];

	public:

		static int32_t getConstraintType(lua_State* L)
		{
			ja::Constraint2d* constraint = (ja::Constraint2d*)(lua_touserdata(L,1));
		
			lua_pushinteger(L, constraint->getConstraintType());
			return 1;
		}

		static int32_t getNext(lua_State* L)
		{
			ja::Constraint2d*   constraint   = (ja::Constraint2d*)(lua_touserdata(L,1));
			ja::PhysicObject2d* physicObject = (ja::PhysicObject2d*)(lua_touserdata(L,2));
			
			lua_pushlightuserdata(L,constraint->getNext(physicObject));

			return 1;
		}

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,getConstraintType,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getNext,table)

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char Constraint::className[] = "Constraint";
}