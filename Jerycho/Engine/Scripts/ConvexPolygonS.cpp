#include "ConvexPolygon.hpp"
#include "../Manager/RecycledMemoryManager.hpp"

namespace jas
{

const char ConvexPolygon::className[] = "ConvexPolygon";

int32_t ConvexPolygon::setVertices(lua_State* L)
{
	gil::ConvexPolygon2d* convexPolygon = (gil::ConvexPolygon2d*)lua_touserdata(L, 1);

	luaL_checktype(L, 2, LUA_TTABLE);

#ifdef LUA53
	const int32_t arraySize = luaL_len(L, 2);
#else
	const int32_t arraySize = lua_objlen(L, 2);
#endif

	RecycledMemoryManager* memoryManager = RecycledMemoryManager::getSingleton();
	void* memoryBlock = memoryManager->getMemoryBlock(ja::setup::physics::MULTI_SHAPE_ALLOCATOR_SIZE);

	//StackAllocator<float> verticesArray;
	//verticesArray.resize(arraySize * sizeof(float));

	//float* verticesData = verticesArray.getFirst() - 1;
	float* verticesData = ((float*)memoryBlock) - 1;

	int32_t            i = 0;
	uint16_t verticesNum = arraySize / 2;

	//Faster method
	for (int32_t i = 1; i <= arraySize; i++) {
		lua_rawgeti(L, 2, i);
		verticesData[i] = (float)lua_tonumber(L, -1);
		lua_pop(L, 1);
	}

	/*lua_pushnil(L);

	while (lua_next(L, -2) != 0)
	{
		verticesData[i++] = (float)lua_tonumber(L, -1);
		lua_pop(L, 1);
	}*/

	const jaVector2* vertices = (jaVector2*)(verticesData + 1);

	convexPolygon->setVertices(vertices, verticesNum);

	memoryManager->releaseMemoryBlock(memoryBlock);

	return 0;
}

}

