#pragma once

#include "../Gilgamesh/Shapes/Shape2dTypes.hpp"
#include "../Graphics/LayerObject.hpp"
#include "../Scripts/Script.hpp"
#include "../Graphics/Shader.hpp"
#include "../Data/DataModel.hpp"

using namespace ja;
using namespace gil;

namespace jas
{
	class EnumTypes
	{
	public:
		static void registerClass(lua_State* L)
		{
			{
				//ShapeType
				lua_newtable(L);
				int32_t table = lua_gettop(L);

				lua_pushliteral(L, "Box");
				lua_pushinteger(L, (int32_t)ShapeType::Box);
				lua_settable(L, table);

				lua_pushliteral(L, "Circle");
				lua_pushinteger(L, (int32_t)ShapeType::Circle);
				lua_settable(L, table);

				lua_pushliteral(L, "ConvexPolygon");
				lua_pushinteger(L, (int32_t)ShapeType::ConvexPolygon);
				lua_settable(L, table);

				lua_pushliteral(L, "MultiShape");
				lua_pushinteger(L, (int32_t)ShapeType::MultiShape);
				lua_settable(L, table);

				lua_pushliteral(L, "Particles");
				lua_pushinteger(L, (int32_t)ShapeType::Particles);
				lua_settable(L, table);

				lua_pushliteral(L, "Point");
				lua_pushinteger(L, (int32_t)ShapeType::Point);
				lua_settable(L, table);

				lua_pushliteral(L, "TriangleMesh");
				lua_pushinteger(L, (int32_t)ShapeType::TriangleMesh);
				lua_settable(L, table);

				lua_setglobal(L, "ShapeType"); // setglobal remove table from stack
			}

			{
				//Layer Object Type
				
				lua_newtable(L);
				int32_t table = lua_gettop(L);

				lua_pushliteral(L, "Polygon");
				lua_pushinteger(L, (int32_t)LayerObjectType::POLYGON);
				lua_settable(L, table);

				lua_pushliteral(L, "Camera");
				lua_pushinteger(L, (int32_t)LayerObjectType::CAMERA);
				lua_settable(L, table);

				lua_setglobal(L, "LayerObjectType"); // setglobal remove table from stack
			}

			{
				//Script Function Type
				lua_newtable(L);
				int32_t table = lua_gettop(L);

				lua_pushliteral(L, "UpdateEntity");
				lua_pushinteger(L, (int32_t)ScriptFunctionType::UPDATE_ENTITY);
				lua_settable(L, table);

				lua_pushliteral(L, "InitEntity");
				lua_pushinteger(L, (int32_t)ScriptFunctionType::INIT_ENTITY);
				lua_settable(L, table);

				lua_pushliteral(L, "ConstraintNotifier");
				lua_pushinteger(L, (int32_t)ScriptFunctionType::CONSTRAINT_NOTIFIER);
				lua_settable(L, table);

				lua_pushliteral(L, "InitData");
				lua_pushinteger(L, (int32_t)ScriptFunctionType::INIT_DATA);
				lua_settable(L, table);

				lua_pushliteral(L, "AccessData");
				lua_pushinteger(L, (int32_t)ScriptFunctionType::ACCESS_DATA);
				lua_settable(L, table);

				lua_setglobal(L, "ScriptFunctionType"); // setglobal remove table from stack
			}

			{
				//Shader Type
				lua_newtable(L);
				int32_t table = lua_gettop(L);

				lua_pushliteral(L, "FragmentShader");
				lua_pushinteger(L, (int32_t)ShaderType::FRAGMENT_SHADER);
				lua_settable(L, table);

				lua_pushliteral(L, "VertexShader");
				lua_pushinteger(L, (int32_t)ShaderType::VERTEX_SHADER);
				lua_settable(L, table);

				lua_pushliteral(L, "GeometryShader");
				lua_pushinteger(L, (int32_t)ShaderType::GEOMETRY_SHADER);
				lua_settable(L, table);

				lua_setglobal(L, "ShaderType"); // setglobal remove table from stack
			}

			{
				//Body Type
				lua_newtable(L);
				int32_t table = lua_gettop(L);

				lua_pushliteral(L, "Static");
				lua_pushinteger(L, (int32_t)BodyType::STATIC_BODY);
				lua_settable(L, table);

				lua_pushliteral(L, "Kinematic");
				lua_pushinteger(L, (int32_t)BodyType::KINEMATIC_BODY);
				lua_settable(L, table);

				lua_pushliteral(L, "Dynamic");
				lua_pushinteger(L, (int32_t)BodyType::DYNAMIC_BODY);
				lua_settable(L, table);

				lua_setglobal(L, "BodyType"); // setglobal remove table from stack
			}

			{
				//Data Type
				lua_newtable(L);
				int32_t table = lua_gettop(L);

				lua_pushliteral(L, "float");
				lua_pushinteger(L, (int32_t)DataType::Float);
				lua_settable(L, table);

				lua_pushliteral(L, "int32");
				lua_pushinteger(L, (int32_t)DataType::Int32);
				lua_settable(L, table);

				lua_pushliteral(L, "bool");
				lua_pushinteger(L, (int32_t)DataType::Bool);
				lua_settable(L, table);

				lua_setglobal(L, "DataType"); // setglobal remove table from stack
			}

			{
				lua_newtable(L);
				int32_t table = lua_gettop(L);

				lua_pushliteral(L, "Label");
				lua_pushinteger(L, (int32_t)DataWidget::LABEL);
				lua_settable(L, table);

				lua_pushliteral(L, "Scrollbar");
				lua_pushinteger(L, (int32_t)DataWidget::SCROLLBAR);
				lua_settable(L, table);

				lua_pushliteral(L, "Textfield");
				lua_pushinteger(L, (int32_t)DataWidget::TEXTFIELD);
				lua_settable(L, table);

				lua_pushliteral(L, "Checkbox");
				lua_pushinteger(L, (int32_t)DataWidget::CHECKBOX);
				lua_settable(L, table);

				lua_setglobal(L, "DataWidget"); // setglobal remove table from stack
			}

			{
				lua_newtable(L);
				lua_setglobal(L, "DataModel");
			}

			{
				lua_newtable(L);
				lua_setglobal(L, "MaterialDefs");
			}

			{
				lua_newtable(L);
				lua_setglobal(L, "MaterialDef");
			}

			{
				lua_newtable(L);
				lua_setglobal(L, "Materials");
			}

			{
				lua_newtable(L);
				lua_setglobal(L, "ShaderObjectDefs");
			}

			{
				lua_newtable(L);
				lua_setglobal(L, "ShaderObjectDef");
			}
		}
	};
}