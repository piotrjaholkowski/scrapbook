#include "ScriptLoader.hpp"
#include "../Utility/Exception.hpp"
#include "../Manager/ScriptManager.hpp"
#include <iostream>
#include <fstream>

namespace ja
{

ScriptLoader* ScriptLoader::singleton = nullptr;


ScriptLoader::ScriptLoader()
{

}

ScriptLoader::~ScriptLoader()
{

}

ScriptLoader* ScriptLoader::getSingleton()
{
	return singleton;
}

void ScriptLoader::init()
{
	if(singleton==nullptr)
	{
		singleton = new ScriptLoader();
	}
}

void ScriptLoader::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void ScriptLoader::loadScript(const ja::string& fileName)
{

	if (!ScriptManager::getSingleton()->runScriptFile(fileName, true))
	{
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_SCRIPT, fileName);
	}

}

}