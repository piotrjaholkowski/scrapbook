#include "Component.hpp"

namespace jas
{

const char Component::className[] = "Component";

int32_t Component::setTagName(lua_State* L)
{
	ja::EntityComponent* entityComponent = (ja::EntityComponent*)(lua_touserdata(L, 1));
	const char* tagName = lua_tostring(L, 2);

	entityComponent->setTagName(tagName);

	return 0;
}

int32_t Component::getNext(lua_State* L)
{
	ja::EntityComponent* component = (ja::EntityComponent*)(lua_touserdata(L, 1));

	ja::EntityComponent* tempComponent = component->getNext();
	if (tempComponent == nullptr)
		lua_pushnil(L);
	else
		lua_pushlightuserdata(L, tempComponent);

	return 1;
}

int32_t Component::getPrevious(lua_State* L)
{
	ja::EntityComponent* component = (ja::EntityComponent*)(lua_touserdata(L, 1));

	ja::EntityComponent* tempComponent = component->getPrevious();
	if (tempComponent == nullptr)
		lua_pushnil(L);
	else
		lua_pushlightuserdata(L, tempComponent);

	return 1;
}

int32_t Component::getNextComponent(lua_State* L)
{
	ja::EntityComponent* component = (ja::EntityComponent*)(lua_touserdata(L, 1));

	ja::EntityComponent* tempComponent = component->getNextComponent();
	if (tempComponent == nullptr)
		lua_pushnil(L);
	else
		lua_pushlightuserdata(L, tempComponent);

	return 1;
}

int32_t Component::getPreviousComponent(lua_State* L)
{
	ja::EntityComponent* component = (ja::EntityComponent*)(lua_touserdata(L, 1));

	ja::EntityComponent* tempComponent = component->getPreviousComponent();
	if (tempComponent == nullptr)
		lua_pushnil(L);
	else
		lua_pushlightuserdata(L, tempComponent);

	return 1;
}

int32_t Component::getComponentType(lua_State* L)
{
	ja::EntityComponent* component = (ja::EntityComponent*)(lua_touserdata(L, 1));

	ja::EntityComponent* tempComponent = component->getNext();
	if (tempComponent == nullptr)
		lua_pushnil(L);
	else
		lua_pushlightuserdata(L, tempComponent);

	return 1;
}

}