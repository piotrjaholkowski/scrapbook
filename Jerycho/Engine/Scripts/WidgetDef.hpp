#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

//#include "../Manager/jaUIManager.h"
#include "../Gui/WidgetDef.hpp"

namespace jas
{
	class WidgetDef
	{
	private:
		static const char className[];

		static int32_t setPosition(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);
			widgetDef->positionX = (int32_t)lua_tointeger(L, 2);
			widgetDef->positionY = (int32_t)lua_tointeger(L, 3);

			return 0;
		}

		static int32_t setSize(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);
			widgetDef->width = (int32_t)lua_tointeger(L, 2);
			widgetDef->height = (int32_t)lua_tointeger(L, 3);

			return 0;
		}

		static int32_t setId(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);
			widgetDef->widgetId = (uint32_t)lua_tointeger(L,2); 

			return 0;
		}

		static int32_t setText(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);
			widgetDef->text = lua_tostring(L,2);

			return 0;
		}

		static int32_t setParent(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);
			widgetDef->parent = (ja::Widget*)lua_touserdata(L, 2);

			return 0;
		}

		static int32_t setFont(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);
			widgetDef->fontName = lua_tostring(L,2);

			return 0;
		}

		static int32_t setWidgetType(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);
			widgetDef->widgetType = static_cast<ja::WidgetType>(lua_tointeger(L,2));

			return 0;
		}

		static int32_t setColor(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);

			widgetDef->r = (uint8_t)lua_tointeger(L,2);
			widgetDef->g = (uint8_t)lua_tointeger(L,3);
			widgetDef->b = (uint8_t)lua_tointeger(L,4);
			widgetDef->a = (uint8_t)lua_tointeger(L,5);

			return 0;
		}

		static int32_t setGridSize(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);

			widgetDef->blocksX = (int32_t)lua_tointeger(L,2);
			widgetDef->blocksY = (int32_t)lua_tointeger(L,3);

			return 0;
		}

		static int32_t setFontColor(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);

			widgetDef->fontR = (uint8_t)lua_tointeger(L,2);
			widgetDef->fontG = (uint8_t)lua_tointeger(L,3);
			widgetDef->fontB = (uint8_t)lua_tointeger(L,4);
			widgetDef->fontA = (uint8_t)lua_tointeger(L,5);

			return 0;
		}

		static int32_t setRender(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);

			if(lua_toboolean(L,2))
				widgetDef->isRendered = true;
			else
				widgetDef->isRendered = false;

			return 0;
		}

		static int32_t setActive(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);

			if(lua_toboolean(L,2))
				widgetDef->isActive = true;
			else
				widgetDef->isActive = false;

			return 0;
		}

		static int32_t setBorderColor(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);

			widgetDef->borderR = (uint8_t)lua_tointeger(L,2);
			widgetDef->borderG = (uint8_t)lua_tointeger(L,3);
			widgetDef->borderB = (uint8_t)lua_tointeger(L,4);
			widgetDef->borderA = (uint8_t)lua_tointeger(L,5);

			return 0;
		}

		static int32_t setRenderBorder(lua_State* L)
		{
			ja::WidgetDef* widgetDef = (ja::WidgetDef*)lua_touserdata(L, 1);

			if(lua_toboolean(L,2))
				widgetDef->isBorderRendered = true;
			else
				widgetDef->isBorderRendered = false;

			return 0;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,setPosition,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setSize,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setGridSize,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setPosition,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setText,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setParent,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setFont,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setColor,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setBorderColor,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setFontColor,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setRender,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setActive,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setRenderBorder,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setId,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setWidgetType,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char WidgetDef::className[] = "WidgetDef";
}
