#include "Material.hpp"

#include "../Graphics/Material.hpp"
#include "../Manager/ScriptManager.hpp"
#include "../Manager/LogManager.hpp"
#include "../Manager/EffectManager.hpp"

namespace jas
{

const char Material::className[] = "Material";

int32_t Material::setBlendEquation(lua_State* L)
{
	ja::Material* material = (ja::Material*)lua_touserdata(L, 1);
	
	material->blendEquation = (uint32_t)lua_tointeger(L, 2);

	return 0;
}

int32_t Material::setSourceFactor(lua_State* L)
{
	ja::Material* material = (ja::Material*)lua_touserdata(L, 1);

	material->sourceFactor = (uint32_t)lua_tointeger(L, 2);

	return 0;
}

int32_t Material::setDestinationFactor(lua_State* L)
{
	ja::Material* material = (ja::Material*)lua_touserdata(L, 1);

	material->destinationFactor = (uint32_t)lua_tointeger(L, 2);

	return 0;
}

int32_t Material::setFloat(lua_State* L)
{
	ja::Material*                 material = (ja::Material*)lua_touserdata(L, 1);
	const ja::DataFieldInfo* dataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, 2);

	if (lua_isnumber(L, 3))
	{
		const float dataValue = (float)lua_tonumber(L, 3);
		material->setFloat(dataFieldInfo, 0, dataValue);

		return 0;
	}

	if (lua_istable(L, 3))
	{

	}

	return 0;
}

int32_t Material::setBool(lua_State* L)
{
	ja::Material*                 material = (ja::Material*)lua_touserdata(L, 1);
	const ja::DataFieldInfo* dataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, 2);

	if (lua_isboolean(L, 3))
	{
		const int32_t dataValue = lua_toboolean(L, 3);
		material->setBool(dataFieldInfo, 0, dataValue);

		return 0;
	}

	if (lua_istable(L, 3))
	{

	}

	return 0;
}


}