#include "DataComponent.hpp"

namespace jas
{
	const char DataComponent::className[] = "DataComponent";

	int32_t DataComponent::create(lua_State* L)
	{
		ja::Entity* entity = (ja::Entity*)lua_touserdata(L, 1);

		ja::DataComponent* dataComponent = ja::DataGroup::getSingleton()->createComponent(entity);

		lua_pushlightuserdata(L, dataComponent);

		return 1;
	}

	/*int32_t DataComponent::setFloat(lua_State* L)
	{
		ja::DataComponent* dataComponent = (ja::DataComponent*)lua_touserdata(L, 1);
		const int32_t      dataOffset    = lua_tointeger(L, 2);

		if (lua_isnumber(L, 3))
		{
			const float dataValue = (float)lua_tonumber(L, 3);
			dataComponent->setFloat(dataOffset, dataValue);

			return 0;
		}

		if (lua_istable(L, 3))
		{

		}	

		return 0;
	}*/

	int32_t DataComponent::setFloat(lua_State* L)
	{
		ja::DataComponent*       dataComponent = (ja::DataComponent*)lua_touserdata(L, 1);
		const ja::DataFieldInfo* dataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, 2);

		if (lua_isnumber(L, 3))
		{
			const float dataValue = (float)lua_tonumber(L, 3);
			dataComponent->setFloat(dataFieldInfo, 0, dataValue);

			return 0;
		}

		if (lua_istable(L, 3))
		{

		}

		return 0;
	}

	int32_t DataComponent::setFloatElement(lua_State* L)
	{
		return 0;
	}

	/*int32_t DataComponent::setInt32(lua_State* L)
	{
		ja::DataComponent* dataComponent = (ja::DataComponent*)lua_touserdata(L, 1);
		const int32_t      dataOffset    = lua_tointeger(L, 2);

		if (lua_isnumber(L, 3))
		{
			const int32_t dataValue = lua_tointeger(L, 3);
			dataComponent->setInt32(dataOffset, dataValue);

			return 0;
		}

		if (LUA_TTABLE == lua_type(L, 3))
		{
		}

		return 0;
	}*/

	int32_t DataComponent::setInt32(lua_State* L)
	{
		ja::DataComponent* dataComponent = (ja::DataComponent*)lua_touserdata(L, 1);
		ja::DataFieldInfo* dataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, 2);

		if (lua_isnumber(L, 3))
		{
			const int32_t dataValue = lua_tointeger(L, 3);
			dataComponent->setInt32(dataFieldInfo, 0, dataValue);

			return 0;
		}

		if (LUA_TTABLE == lua_type(L, 3))
		{
		}

		return 0;
	}

	/*int32_t DataComponent::setInt32Element(lua_State* L)
	{
		ja::DataComponent* dataComponent = (ja::DataComponent*)lua_touserdata(L, 1);
		const int32_t      dataOffset    = lua_tointeger(L, 2);
		const int32_t      elementId     = lua_tointeger(L, 3);

		if (lua_isnumber(L, 4))
		{
			const int32_t dataValue = lua_tointeger(L, 4);
			dataComponent->setInt32(dataOffset + (elementId * sizeof(int32_t)), dataValue);

			return 0;
		}

		if (LUA_TTABLE == lua_type(L, 4))
		{
		}

		return 0;
	}*/

	int32_t DataComponent::setInt32Element(lua_State* L)
	{
		ja::DataComponent*       dataComponent = (ja::DataComponent*)lua_touserdata(L, 1);
		const ja::DataFieldInfo* dataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, 2);
		const int32_t            elementId     = lua_tointeger(L, 3);

		if (lua_isnumber(L, 4))
		{
			const int32_t dataValue = lua_tointeger(L, 4);
			dataComponent->setInt32(dataFieldInfo, elementId, dataValue);

			return 0;
		}

		if (LUA_TTABLE == lua_type(L, 4))
		{
		}

		return 0;
	}

	

}