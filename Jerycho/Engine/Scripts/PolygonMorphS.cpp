#include "PolygonMorph.hpp"

namespace jas
{

const char PolygonMorph::className[] = "PolygonMorph";

int32_t PolygonMorph::getDrawElements(lua_State* L)
{
	ja::PolygonMorph* polygonMorph = (ja::PolygonMorph*)lua_touserdata(L, 1);

	lua_pushlightuserdata(L, &polygonMorph->drawElements);

	return 1;
}

int32_t PolygonMorph::setPosition(lua_State* L)
{
	ja::PolygonMorph* polygonMorph = (ja::PolygonMorph*)lua_touserdata(L, 1);
	jaVector2*        position     = (jaVector2*)lua_touserdata(L, 2);

	polygonMorph->transform.position = *position;

	return 0;
}

int32_t PolygonMorph::setRotation(lua_State* L)
{
	ja::PolygonMorph* polygonMorph = (ja::PolygonMorph*)lua_touserdata(L, 1);
	float             angleRad     = (float)lua_tonumber(L, 2);

	polygonMorph->transform.rotationMatrix.setRadians(angleRad);

	return 0;
}

int32_t PolygonMorph::setScale(lua_State* L)
{
	ja::PolygonMorph* polygonMorph = (ja::PolygonMorph*)lua_touserdata(L, 1);
	jaVector2*        scale        = (jaVector2*)lua_touserdata(L, 2);

	polygonMorph->scale = *scale;

	return 0;
}

int32_t PolygonMorph::setActiveFeatures(lua_State* L)
{
	ja::PolygonMorph*  polygonMorph   = (ja::PolygonMorph*)lua_touserdata(L, 1);
	uint32_t activeFeatures = (uint32_t)lua_tonumber(L, 2); 

	polygonMorph->setActiveFeatures(activeFeatures);

	return 0;
}

}
