#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"

//#include "../Gui/GuiLoader.hpp"

namespace jas
{

class ResourceManager
{
private:

	static const char className[];
	
/*static int32_t loadGui(lua_State* L)
{
	int32_t n = lua_gettop(L);
	if(n!=1)
		return 0;

	ja::string fileName = lua_tostring(L,1);

	//ja::ResourceManager::getSingleton()->loadGui(fileName);
	return 0;
}*/

/*static int32_t saveGui(lua_State* L)
{
	int32_t n = lua_gettop(L);
	if(n!=2)
		return 0;

	ja::string interfaceName = lua_tostring(L,1);
	ja::string fileName = lua_tostring(L,2);

	//ja::ResourceManager::getSingleton()->saveGui(interfaceName,fileName);
	return 0;
}*/

/*static int32_t getLevelCustomData(lua_State* L)
{
	int32_t n = lua_gettop(L);
	if(n!=1)
		return 0;

	uint32_t resourceId = (uint32_t)lua_tointeger(L,1);
	///ja::CustomData* customData = ja::ResourceManager::getSingleton()->getLevelCustomData(resourceId);
	///lua_pushlightuserdata(L,customData);
	///return 1;
	return 0;
}*/

	static int32_t loadShader(lua_State* L);
	static int32_t getShader(lua_State* L);
	static int32_t addDataModel(lua_State* L);
	static int32_t addMaterial(lua_State* L);
	static int32_t addMaterialShaderDef(lua_State* L);
	static int32_t addShaderObjectDef(lua_State* L);
	static int32_t createShaderBinder(lua_State* L);

public:

static void registerClass(lua_State* L) {

	lua_newtable(L);
	int32_t table = lua_gettop(L);

	//JAS_ADD_SCRIPT_FUNCTION(L,loadGui,table);
	//JAS_ADD_SCRIPT_FUNCTION(L,saveGui,table);
	//JAS_ADD_SCRIPT_FUNCTION(L,getLevelCustomData,table);
	JAS_ADD_SCRIPT_FUNCTION(L, loadShader, table);
	JAS_ADD_SCRIPT_FUNCTION(L, getShader, table);
	JAS_ADD_SCRIPT_FUNCTION(L, addDataModel, table);
	JAS_ADD_SCRIPT_FUNCTION(L, addMaterial, table);
	JAS_ADD_SCRIPT_FUNCTION(L, addMaterialShaderDef, table);
	JAS_ADD_SCRIPT_FUNCTION(L, addShaderObjectDef, table);
	JAS_ADD_SCRIPT_FUNCTION(L, createShaderBinder, table);

	lua_setglobal(L, className); // setglobal remove table from stack
}

friend class LevelManager;

};
	
}
