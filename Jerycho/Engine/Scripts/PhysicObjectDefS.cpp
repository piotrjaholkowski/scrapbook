#include "PhysicObjectDef.hpp"

namespace jas
{

const char PhysicObjectDef::className[] = "PhysicObjectDef";

int32_t PhysicObjectDef::setPosition(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	jaVector2*             position = (jaVector2*)lua_touserdata(L, 2);

	physicObjectDef->transform.position = *position;

	return 0;
}

int32_t PhysicObjectDef::setRotation(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	float                  rotationRad = (float)lua_tonumber(L, 2);

	physicObjectDef->transform.rotationMatrix.setRadians(rotationRad);

	return 0;
}

int32_t PhysicObjectDef::setBodyType(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	uint16_t               bodyType = (uint16_t)lua_tointeger(L, 2);

	physicObjectDef->bodyType = (ja::BodyType)bodyType;

	return 0;
}

int32_t PhysicObjectDef::setLinearVelocity(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	jaVector2*             velocity = (jaVector2*)lua_touserdata(L, 2);

	physicObjectDef->linearVelocity = *velocity;

	return 0;
}

int32_t PhysicObjectDef::setGravity(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	jaVector2*             gravity = (jaVector2*)lua_touserdata(L, 2);

	physicObjectDef->gravity = *gravity;

	return 0;
}

int32_t PhysicObjectDef::setAngluarVelocity(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	float                  angluarVelocity = (float)lua_tonumber(L, 2);

	physicObjectDef->angluarVelocity = angluarVelocity;

	return 0;
}

int32_t PhysicObjectDef::setLinearDamping(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	float                  linearDamping = (float)lua_tonumber(L, 2);

	physicObjectDef->linearDamping = linearDamping;

	return 0;
}

int32_t PhysicObjectDef::setAngluarDamping(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	float                  angluarDamping = (float)lua_tonumber(L, 2);

	physicObjectDef->angluarDamping = angluarDamping;

	return 0;
}

int32_t PhysicObjectDef::setFixedRotation(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	int32_t                isFixedRotation = lua_tointeger(L, 2);

	physicObjectDef->fixedRotation = (0 != isFixedRotation);

	return 0;
}

int32_t PhysicObjectDef::setAllowSleep(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	int32_t                isSleepingAllowed = lua_tointeger(L, 2);

	physicObjectDef->allowSleep = (0 != isSleepingAllowed);

	return 0;
}

int32_t PhysicObjectDef::setSleep(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	int32_t                isSleeping = lua_tointeger(L, 2);

	physicObjectDef->isSleeping = (0 != isSleeping);

	return 0;
}

int32_t PhysicObjectDef::setGroupIndex(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	int8_t                 groupIndex = (int8_t)lua_tointeger(L, 2);

	physicObjectDef->groupIndex = groupIndex;

	return 0;
}

int32_t PhysicObjectDef::setCategoryMask(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	int16_t                categoryMask = (int16_t)lua_tointeger(L, 2);

	physicObjectDef->categoryMask = categoryMask;

	return 0;
}

int32_t PhysicObjectDef::setMaskBits(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	int16_t                maskBits = (int16_t)lua_tointeger(L, 2);

	physicObjectDef->maskBits = maskBits;

	return 0;
}

int32_t PhysicObjectDef::setShapeType(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)lua_touserdata(L, 1);
	int8_t                 shapeType = (int8_t)lua_tointeger(L, 2);

	physicObjectDef->setShapeType(shapeType);

	return 0;
}

int32_t PhysicObjectDef::setTimeFactor(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)(lua_touserdata(L, 1));
	float                  timeFactor = (float)lua_tonumber(L, 2);

	physicObjectDef->timeFactor = timeFactor;

	return 0;
}

int32_t PhysicObjectDef::getShape(lua_State* L)
{
	ja::PhysicObjectDef2d* physicObjectDef = (ja::PhysicObjectDef2d*)(lua_touserdata(L, 1));


	lua_pushlightuserdata(L, physicObjectDef->getShape());

	return 1;
}

}