#include "MorphElements.hpp"

namespace jas
{
	
const char MorphElements::className[] = "MorphElements";

int32_t MorphElements::clear(lua_State* L)
{
	ja::MorphElements* morphElements = (ja::MorphElements*)lua_touserdata(L, 1);
	morphElements->clear();

	return 0;
}

int32_t MorphElements::createMorph(lua_State* L)
{
	ja::MorphElements* morphElements = (ja::MorphElements*)lua_touserdata(L, 1);
	const char*        morphName     = lua_tostring(L, 2);

	ja::LayerObjectMorph* layerObjectMorph = morphElements->createMorph(morphName);
	lua_pushlightuserdata(L, layerObjectMorph);

	return 1;
}

}