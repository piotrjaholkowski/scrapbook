#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Interface/CameraInterface.hpp"
#include "ScriptFunctions.hpp"

namespace jas
{
	class CameraInterface
	{
	private:
		static const char className[];

		static int32_t setPosition(lua_State* L)
		{
			ja::CameraInterface* camera = (ja::CameraInterface*)(lua_touserdata(L,1));
			jaVector2& position = *(jaVector2*)(lua_touserdata(L,2));

			camera->position = position;
			return 0;
		}

		static int32_t setScale(lua_State* L)
		{
			ja::CameraInterface* camera = (ja::CameraInterface*)(lua_touserdata(L,1));
			jaVector2& scale = *(jaVector2*)(lua_touserdata(L,2));

			camera->scale = scale;
			return 0;
		}

	public:

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,setPosition,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setScale,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char CameraInterface::className[] = "CameraInterface";
}