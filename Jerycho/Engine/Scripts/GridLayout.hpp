#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

//#include "../Manager/jaUIManager.h"
#include "../Gui/GridLayout.hpp"

namespace jas
{
	class GridLayout
	{
	private:
		static const char className[];


		static int32_t addWidget(lua_State* L)
		{
			ja::GridLayout* gridLayout = (ja::GridLayout*)lua_touserdata(L, 1);
			ja::Widget* widget = (ja::Widget*)lua_touserdata(L,2);
			int32_t x = (int32_t)lua_tointeger(L,3);
			int32_t y = (int32_t)lua_tointeger(L,4);

			gridLayout->addWidget(widget,x,y);

			return 0;
		}

		static int32_t getWidget(lua_State* L)
		{
			ja::GridLayout* grid = (ja::GridLayout*)lua_touserdata(L, 1);
			uint32_t widgetId = (uint32_t)lua_tointeger(L,2);

			ja::Widget* widget = grid->getWidget(widgetId);

			if(widget)
				lua_pushlightuserdata(L,widget);
			else
				lua_pushnil(L);

			return 1;
		}


	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			lua_pushliteral(L,"addWidget");
			lua_pushcfunction(L,addWidget);
			lua_settable(L,table);

			lua_pushliteral(L,"getWidget");
			lua_pushcfunction(L,getWidget);
			lua_settable(L,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char GridLayout::className[] = "GridLayout";
}