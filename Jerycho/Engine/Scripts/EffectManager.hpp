#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Manager/EffectManager.hpp"
#include "ScriptFunctions.hpp"

namespace jas
{
	class EffectManager
	{
	private:
		static const char className[];

		//static int32_t renderFbo(lua_State* L);
		//static int32_t bindEffectFbo(lua_State* L);
		static int32_t createLayer(lua_State* L);
		static int32_t createParallax(lua_State* L);

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			//JAS_ADD_SCRIPT_FUNCTION(L, renderFbo, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, bindEffectFbo, table);
			JAS_ADD_SCRIPT_FUNCTION(L, createLayer, table);
			JAS_ADD_SCRIPT_FUNCTION(L, createParallax, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	
}
