#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Manager/EntityManager.hpp"

namespace jas
{
	class EntityManager
	{
	private:
		static const char className[];

		static int32_t createEntity(lua_State* L);
		static int32_t deleteEntityImmediate(lua_State* L);
		static int32_t getEntity(lua_State* L);

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, createEntity, table);
			JAS_ADD_SCRIPT_FUNCTION(L, deleteEntityImmediate, table);
			JAS_ADD_SCRIPT_FUNCTION(L, getEntity, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

	
}