#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "Matrix2.hpp"
#include "../Physics/PhysicObject2d.hpp"

namespace jas
{
	class RigidBody
	{
	private:
		static const char className[];

		static int32_t applyForceToCenter(lua_State* L)
		{
			ja::RigidBody2d* rigidBody = (ja::RigidBody2d*)(lua_touserdata(L, 1));
			jaVector2& force = *(jaVector2*)(lua_touserdata(L,2));

			rigidBody->applyForceToCenter(force);

			return 0;
		}

		static int32_t getAngularVelocity(lua_State* L)
		{
			ja::RigidBody2d* rigidBody = (ja::RigidBody2d*)(lua_touserdata(L, 1));
			
			lua_pushnumber(L,rigidBody->getAngularVelocity());

			return 1;
		}

		static int32_t setAngularVelocity(lua_State* L)
		{
			ja::RigidBody2d* rigidBody = (ja::RigidBody2d*)(lua_touserdata(L, 1));
			float angularVelocity = (float)(lua_tonumber(L,2));

			rigidBody->setAngularVelocity(angularVelocity);

			return 0;
		}

		static int32_t setLinearVelocity(lua_State* L)
		{
			ja::RigidBody2d* rigidBody = (ja::RigidBody2d*)(lua_touserdata(L, 1));
			jaVector2& linVel = *(jaVector2*)(lua_touserdata(L,2));

			rigidBody->setLinearVelocity(linVel);

			return 0;
		}

		static int32_t getLinearVelocity(lua_State* L)
		{
			ja::RigidBody2d* rigidBody = (ja::RigidBody2d*)(lua_touserdata(L, 1));
			jaVector2& linVel = *Vector2::push(L);
			linVel = rigidBody->getLinearVelocity();

			return 1;
		}

		static int32_t isSleeping(lua_State* L)
		{
			ja::PhysicObject2d* physicObject = (ja::PhysicObject2d*)(lua_touserdata(L,1));
			lua_pushboolean(L,physicObject->isSleeping());

			return 1;
		}

		static int32_t getRotationMatrix(lua_State* L)
		{
			ja::RigidBody2d* rigidBody = (ja::RigidBody2d*)(lua_touserdata(L, 1));
			jaMatrix2& rotMatrix = *Matrix2::push(L);
			rotMatrix = rigidBody->getRotMatrix();

			return 1;
		}

		static int32_t getPosition(lua_State* L)
		{
			ja::RigidBody2d* rigidBody = (ja::RigidBody2d*)(lua_touserdata(L, 1));
			jaVector2& position = *Vector2::push(L);
			position = rigidBody->getShapeHandle()->transform.position;

			return 1;
		}

	public:
		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,isSleeping,table);
			JAS_ADD_SCRIPT_FUNCTION(L,applyForceToCenter,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setAngularVelocity,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getAngularVelocity,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setLinearVelocity,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getLinearVelocity,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getRotationMatrix,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getPosition,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

	const char RigidBody::className[] = "RigidBody";
}