#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Gilgamesh/Shapes/Shape2d.hpp"

namespace jas
{
	class Shape
	{
	private:
		static const char className[];

		static int32_t setFriction(lua_State* L);
		static int32_t setDensity(lua_State* L);
		static int32_t setRestitution(lua_State* L);

	public:

		static void save(FILE* pFile, const char* physicObjectDefVar, const char* shapeVariable, gil::Shape2d* shape, uint32_t reccurenceIt = 0);

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, setFriction, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setDensity, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setRestitution, table);
			

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

	
}