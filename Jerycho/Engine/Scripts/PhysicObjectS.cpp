#include "PhysicObject.hpp"
#include "Shape.hpp"

namespace jas
{

const char PhysicObject::className[] = "PhysicObject";

int32_t PhysicObject::isSleeping(lua_State* L)
{
	ja::PhysicObject2d* physicObject = (ja::PhysicObject2d*)(lua_touserdata(L, 1));
	lua_pushboolean(L, physicObject->isSleeping());

	return 1;
}

int32_t PhysicObject::getPrevious(lua_State* L)
{
	ja::PhysicObject2d* physicObject = (ja::PhysicObject2d*)(lua_touserdata(L, 1));

	ja::PhysicObject2d* previousObject = physicObject->getPrevious();

	if (previousObject == nullptr)
		lua_pushnil(L);
	else
		lua_pushlightuserdata(L, previousObject);

	return 1;
}

int32_t PhysicObject::setTimeFactor(lua_State* L)
{
	ja::PhysicObject2d* physicObject = (ja::PhysicObject2d*)(lua_touserdata(L, 1));
	float               timeFactor = (float)lua_tonumber(L, 2);

	physicObject->setTimeFactor(timeFactor);

	return 0;
}

void PhysicObject::save(bool exportMode, FILE* pFile, const char* physicObjectDefVar, const jaVector2& worldPosition, const ja::PhysicObject2d* physicObject)
{
	gil::ObjectHandle2d* shapeHandle = physicObject->getShapeHandle();
	const jaVector2 relativePosition = shapeHandle->transform.position - worldPosition;
	Shape2d* shape = physicObject->getShape();

	fprintf(pFile, "PhysicObjectDef.setPosition(%s,Vector2.create(x + %f,y + %f));\n", physicObjectDefVar, relativePosition.x, relativePosition.y);
	fprintf(pFile, "PhysicObjectDef.setRotation(%s,%f);\n", physicObjectDefVar, shapeHandle->transform.rotationMatrix.getRadians());
	fprintf(pFile, "PhysicObjectDef.setBodyType(%s,%s);\n", physicObjectDefVar, physicObject->getBodyTypeName());
	fprintf(pFile, "PhysicObjectDef.setTimeFactor(%s,%f);\n", physicObjectDefVar, physicObject->getTimeFactor());

	switch (physicObject->getObjectType())
	{
	case (uint8_t)ja::PhysicObjectType::RIGID_BODY:
	{
		ja::RigidBody2d* rigidBody = (ja::RigidBody2d*)physicObject;
		jaVector2 linearVelocity = rigidBody->getLinearVelocity();
		float     angularVelocity = rigidBody->getAngularVelocity();
		jaVector2 gravity = rigidBody->getGravity();

		fprintf(pFile, "PhysicObjectDef.setLinearVelocity(%s,Vector2.create(%f,%f));\n", physicObjectDefVar, linearVelocity.x, linearVelocity.y);
		fprintf(pFile, "PhysicObjectDef.setAngluarVelocity(%s,%f);\n", physicObjectDefVar, angularVelocity);
		fprintf(pFile, "PhysicObjectDef.setLinearDamping(%s,%f);\n", physicObjectDefVar, rigidBody->getLinearDamping());
		fprintf(pFile, "PhysicObjectDef.setAngluarDamping(%s,%f);\n", physicObjectDefVar, rigidBody->getAngularDamping());
		fprintf(pFile, "PhysicObjectDef.setFixedRotation(%s,%d);\n", physicObjectDefVar, (int32_t)rigidBody->isFixedRotation());
		fprintf(pFile, "PhysicObjectDef.setAllowSleep(%s,%d);\n", physicObjectDefVar, (int32_t)rigidBody->isSleepingAllowed());
		fprintf(pFile, "PhysicObjectDef.setSleep(%s,%d);\n", physicObjectDefVar, (int32_t)rigidBody->isSleeping());
		fprintf(pFile, "PhysicObjectDef.setGravity(%s,Vector2.create(%f,%f));\n", physicObjectDefVar, gravity.x, gravity.y);
		fprintf(pFile, "PhysicObjectDef.setGroupIndex(%s,%d);\n", physicObjectDefVar, (int32_t)rigidBody->getGroupIndex());
		fprintf(pFile, "PhysicObjectDef.setCategoryMask(%s,%d);\n", physicObjectDefVar, (int32_t)rigidBody->getCategoryMask());
		fprintf(pFile, "PhysicObjectDef.setMaskBits(%s,%d);\n", physicObjectDefVar, (int32_t)rigidBody->getMaskBits());
	}
	break;

	}

	const char* shapeVariable = "shape";
	jas::Shape::save(pFile, physicObjectDefVar, shapeVariable, shape);

	if (exportMode)
	{
		fprintf(pFile, "physicObjects[%d] = PhysicsManager.createBodyDef();\n", physicObject->getId());
	}
	else
	{
		fprintf(pFile, "physicObjects[%d] = PhysicsManager.createBodyDef(%d);\n", physicObject->getId(), physicObject->getId());
	}
}

}
