#include "LayerObject.hpp"
#include "../Graphics/Camera.hpp"
#include "DrawElements.hpp"
#include "PolygonMorphElements.hpp"
#include "CameraMorphElements.hpp"

namespace jas
{

	const char LayerObject::className[] = "LayerObject";

	void LayerObject::save(bool exportMode, FILE* pFile, const char* layerObjectDefVar, const jaVector2& worldPosition, const ja::LayerObject* layerObject)
	{
		gil::ObjectHandle2d* layerObjectHandle = layerObject->getHandle();
		const jaVector2      relativePosition = layerObjectHandle->transform.position - worldPosition;
		const jaVector2      scale = layerObject->getScale();
		Shape2d*             shape = layerObjectHandle->getShape();

		fprintf(pFile, "LayerObjectDef.setPosition(%s,Vector2.create(x + %f,y + %f));\n", layerObjectDefVar, relativePosition.x, relativePosition.y);
		fprintf(pFile, "LayerObjectDef.setRotation(%s,%f);\n", layerObjectDefVar, layerObjectHandle->transform.rotationMatrix.getRadians());
		fprintf(pFile, "LayerObjectDef.setScale(%s,Vector2.create(%f,%f));\n", layerObjectDefVar, scale.x, scale.y);

		fprintf(pFile, "LayerObjectDef.setLayerId(%s,%d);\n", layerObjectDefVar, (int32_t)layerObject->layerId);
		fprintf(pFile, "LayerObjectDef.setDepth(%s,%hu);\n", layerObjectDefVar, layerObject->depth);
		fprintf(pFile, "LayerObjectDef.setLayerObjectType(%s,LayerObjectType.%s);\n", layerObjectDefVar, layerObject->getName());


		switch (layerObject->getObjectType())
		{
			case (uint8_t)ja::LayerObjectType::POLYGON:
			{
				ja::Polygon* polygon = (ja::Polygon*)layerObject;

				const char* drawElementsVar = "drawElements";

				fprintf(pFile, "local %s = LayerObjectDef.getDrawElements(%s);\n", drawElementsVar, layerObjectDefVar);

				jas::DrawElements::save(pFile, drawElementsVar, polygon->drawElements);

				const char* morphElementsVar = "morphElements";

				if (polygon->morphElements.getMorphsNum() > 0)
				{
					fprintf(pFile, "local %s = LayerObjectDef.getPolygonMorphElements(%s);\n", morphElementsVar, layerObjectDefVar);
					fprintf(pFile, "MorphElements.clear(%s);\n", morphElementsVar);
					jas::PolygonMorphElements::save(pFile, morphElementsVar, polygon->morphElements);
				}		
			}
			break;
			case (uint8_t)ja::LayerObjectType::CAMERA:
			{
				ja::Camera* camera = (ja::Camera*)layerObject;

				fprintf(pFile, "LayerObjectDef.setCameraName(%s,'%s');\n", layerObjectDefVar, camera->getCameraName());

				const char* morphElementsVar = "morphElements";

				if (camera->morphElements.getMorphsNum() > 0)
				{
					fprintf(pFile, "local %s = LayerObjectDef.getCameraMorphElements(%s);\n", morphElementsVar, layerObjectDefVar);
					fprintf(pFile, "MorphElements.clear(%s);\n", morphElementsVar);
					jas::CameraMorphElements::save(pFile, morphElementsVar, camera->morphElements);
				}
			}
			break;
		}

		if (exportMode)
		{
			fprintf(pFile, "layerObjects[%d] = LayerObjectManager.createLayerObjectDef();\n", layerObject->getId());
		}
		else
		{
			fprintf(pFile, "layerObjects[%d] = LayerObjectManager.createLayerObjectDef(%d);\n", layerObject->getId(), layerObject->getId());
		}
	}

}