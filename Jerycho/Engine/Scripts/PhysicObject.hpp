#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Physics//PhysicObject2d.hpp"

namespace jas
{
	class PhysicObject
	{
	private:
		static const char className[];

		static int32_t isSleeping(lua_State* L);
		static int32_t getPrevious(lua_State* L);
		static int32_t setTimeFactor(lua_State* L);

	public:

		static void save(bool exportMode, FILE* pFile, const char* physicObjectDefVar, const jaVector2& worldPosition, const ja::PhysicObject2d* physicObject);

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, isSleeping,    table);
			JAS_ADD_SCRIPT_FUNCTION(L, getPrevious,   table);
			JAS_ADD_SCRIPT_FUNCTION(L, setTimeFactor, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

	
}