#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Entity/Entity.hpp"
#include "../Entity/DataGroup.hpp"

namespace jas
{

class DataComponent
{
private:
	static const char className[];

	static int32_t create(lua_State* L);
	static int32_t setFloat(lua_State* L);
	static int32_t setFloatElement(lua_State* L);
	static int32_t setInt32(lua_State* L);
	static int32_t setInt32Element(lua_State* L);
	//static int32_t set(lua_State* L);
	//static int32_t setFunction(lua_State* L);

public:

	static void registerClass(lua_State* L)
	{
		lua_newtable(L);
		int32_t table = lua_gettop(L);

		JAS_ADD_SCRIPT_FUNCTION(L, create, table);
		JAS_ADD_SCRIPT_FUNCTION(L, setFloat, table);
		JAS_ADD_SCRIPT_FUNCTION(L, setFloatElement, table);
		JAS_ADD_SCRIPT_FUNCTION(L, setInt32, table);
		JAS_ADD_SCRIPT_FUNCTION(L, setInt32Element, table);
		//JAS_ADD_SCRIPT_FUNCTION(L, set, table);
		//JAS_ADD_SCRIPT_FUNCTION(L, setFunction, table);

		lua_setglobal(L, className); // setglobal remove table from stack
	}

};

}