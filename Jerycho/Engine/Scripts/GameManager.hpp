#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Manager/InputManager.hpp"

namespace jas
{
	class GameManager
	{
	private:
		static const char className[];

		static int32_t getActiveCamera(lua_State* L)
		{
			lua_pushlightuserdata(L, ja::GameManager::getSingleton()->getActiveCamera());
			return 1;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,getActiveCamera,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char GameManager::className[] = "GameManager";
}
