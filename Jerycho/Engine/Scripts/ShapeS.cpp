#include "Shape.hpp"

namespace jas
{

const char Shape::className[] = "Shape";

void Shape::save(FILE* pFile, const char* physicObjectDefVar, const char* shapeVariable, gil::Shape2d* shape, uint32_t reccurenceIt)
{
	if (shape->getType() != (uint8_t)(ShapeType::MultiShape))
	{
		fprintf(pFile, "PhysicObjectDef.setShapeType(%s,ShapeType.%s);\n", physicObjectDefVar, shape->getName());
		fprintf(pFile, "local %s = PhysicObjectDef.getShape(%s);\n", shapeVariable, physicObjectDefVar);
		fprintf(pFile, "Shape.setFriction(%s,%f);\n", shapeVariable, shape->getFriction());
		fprintf(pFile, "Shape.setDensity(%s,%f);\n", shapeVariable, shape->getDensity());
		fprintf(pFile, "Shape.setRestitution(%s,%f);\n", shapeVariable, shape->getRestitution());
	}

	switch (shape->getType())
	{
	case (uint8_t)ShapeType::Box:
	{
		gil::Box2d* box = (gil::Box2d*)shape;
		fprintf(pFile, "Box.setWidth(%s,%f);\n", shapeVariable, box->getWidth());
		fprintf(pFile, "Box.setHeight(%s,%f);\n", shapeVariable, box->getHeight());
	}
	break;
	case (uint8_t)ShapeType::Circle:
	{
		gil::Circle2d* circle = (gil::Circle2d*)shape;
		fprintf(pFile, "Circle.setRadius(%s,%f);\n", shapeVariable, circle->radius);
	}
	break;
	case (uint8_t)ShapeType::ConvexPolygon:
	{
		gil::ConvexPolygon2d* convexPolygon = (gil::ConvexPolygon2d*)shape;

		const jaVector2* vertices    = convexPolygon->getVertices();
		const uint16_t   verticesNum = convexPolygon->getVerticesNum();

		fprintf(pFile, "ConvexPolygon.setVertices(shape,{");

		for (uint16_t i = 0; i < (verticesNum - 1); ++i)
		{
			fprintf(pFile, "%f, %f,\n", vertices[i].x, vertices[i].y);
		}
		fprintf(pFile, "%f,%f});\n", vertices[verticesNum - 1].x, vertices[verticesNum - 1].y);
	}
	break;
	case (uint8_t)ShapeType::Point:
	{

	}
	break;
	case (uint8_t)ShapeType::TriangleMesh:
	{
		gil::TriangleMesh2d* triangleMesh = (gil::TriangleMesh2d*)shape;

		//setting vertices
		{
			const jaVector2* vertices = triangleMesh->getVertices();
			const uint16_t   verticesNum = triangleMesh->getVerticesNum();

			fprintf(pFile, "TriangleMesh.setTriangleMesh(shape,{");

			for (uint16_t i = 0; i < (verticesNum - 1); ++i)
			{
				fprintf(pFile, "%f, %f,", vertices[i].x, vertices[i].y);
			}
			fprintf(pFile, "%f,%f},{", vertices[verticesNum - 1].x, vertices[verticesNum - 1].y);
		}

		//setting indices
		{
			const uint16_t* indices    = triangleMesh->getTriangleIndices();
			const uint16_t  indicesNum = triangleMesh->getTrianglesNum() * 3;

			for (uint16_t i = 0; i < (indicesNum - 1); ++i)
			{
				fprintf(pFile, "%d,", indices[i]);
			}
			fprintf(pFile, "%d});\n", indices[indicesNum - 1]);
		}
	}
	break;
	case (uint8_t)ShapeType::MultiShape:
	{
		gil::MultiShape2d* multiShape = (gil::MultiShape2d*)shape;

		ObjectHandle2d* shapeIt = multiShape->shapes;

		if (0 == reccurenceIt)
		{
			fprintf(pFile, "PhysicObjectDef.setShapeType(%s,ShapeType.%s);\n", physicObjectDefVar, shape->getName());
			fprintf(pFile, "local %s = PhysicObjectDef.getShape(%s);\n", shapeVariable, physicObjectDefVar);
			fprintf(pFile, "MultiShape.deleteShapes(%s);\n", shapeVariable);

			for (shapeIt; shapeIt != nullptr; shapeIt = shapeIt->next)
			{
				Shape2d* partShape = shapeIt->getShape();

				if ((uint8_t)ShapeType::MultiShape == partShape->getType())
				{
					char multiShapeVariable[20];
					sprintf(multiShapeVariable, "multiShape[%d]", (reccurenceIt + 1));
					save(pFile, physicObjectDefVar, multiShapeVariable, partShape, reccurenceIt + 1);
					const jaVector2 position = shapeIt->transform.position;
					const jaFloat   rotRad   = shapeIt->transform.rotationMatrix.getRadians();
					fprintf(pFile, "MultiShape.addShape(%s,Transform2.create(Vector2.create(%f,%f),%f),%s);\n", shapeVariable, position.x, position.y, rotRad, multiShapeVariable);
				}
				else
				{
					save(pFile, physicObjectDefVar, "partShape", partShape, reccurenceIt + 1);
					const jaVector2 position = shapeIt->transform.position;
					const jaFloat   rotRad = shapeIt->transform.rotationMatrix.getRadians();
					fprintf(pFile, "MultiShape.addShape(%s,Transform2.create(Vector2.create(%f,%f),%f),partShape);\n",shapeVariable, position.x, position.y, rotRad);
				}
			}

			fprintf(pFile, "PhysicObjectDef.setShapeType(%s,ShapeType.%s);\n", physicObjectDefVar, shape->getName());
		}
		else
		{
			fprintf(pFile, "%s = MultiShape.create();\n", shapeVariable);

			for (shapeIt; shapeIt != nullptr; shapeIt = shapeIt->next)
			{
				Shape2d* partShape = shapeIt->getShape();

				if ((uint8_t)ShapeType::MultiShape == partShape->getType())
				{
					char multiShapeVariable[20];
					sprintf(multiShapeVariable, "multiShape[%d]", (reccurenceIt + 1));
					save(pFile, physicObjectDefVar, multiShapeVariable, partShape, reccurenceIt + 1);
					const jaVector2 position = shapeIt->transform.position;
					const jaFloat   rotRad = shapeIt->transform.rotationMatrix.getRadians();
					fprintf(pFile, "MultiShape.addShape(%s,Transform2.create(Vector2.create(%f,%f),%f),%s);\n", shapeVariable, position.x, position.y, rotRad, multiShapeVariable);
				}
				else
				{
					save(pFile, physicObjectDefVar, "partShape", partShape, reccurenceIt + 1);
					const jaVector2 position = shapeIt->transform.position;
					const jaFloat   rotRad = shapeIt->transform.rotationMatrix.getRadians();
					fprintf(pFile, "MultiShape.addShape(%s,Transform2.create(Vector2.create(%f,%f),%f),partShape);\n", shapeVariable, position.x, position.y, rotRad);
				}
			}
		}
		
	}
	break;
	}
}

int32_t Shape::setFriction(lua_State* L)
{
	gil::Shape2d* shape = (gil::Shape2d*)(lua_touserdata(L, 1));
	const float friction = (float)lua_tonumber(L, 2);

	shape->setFriction(friction);

	return 0;
}

int32_t Shape::setDensity(lua_State* L)
{
	gil::Shape2d* shape = (gil::Shape2d*)(lua_touserdata(L, 1));
	const float density = (float)lua_tonumber(L, 2);

	shape->setDensity(density);

	return 0;
}

int32_t Shape::setRestitution(lua_State* L)
{
	gil::Shape2d* shape = (gil::Shape2d*)(lua_touserdata(L, 1));
	const float restitution = (float)lua_tonumber(L, 2);

	shape->setRestitution(restitution);

	return 0;
}

}