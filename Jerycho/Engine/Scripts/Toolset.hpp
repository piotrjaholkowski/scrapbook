#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "Vector2.hpp"
#include "../../Jerycho/Gui/Toolset.hpp"

namespace jas
{
	class Toolset
	{
	private:
		static const char className[];

		static int32_t getDrawPoint(lua_State* L)
		{
			jaVector2* drawPoint = Vector2::push(L);

			*drawPoint = tr::Toolset::getSingleton()->getDrawPoint();
			//lua_pushlightuserdata(L, drawPoint);

			return 1;
		}

	public:
		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, getDrawPoint, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, isSleeping, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, getPrevious, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

	const char Toolset::className[] = "Toolset";
}