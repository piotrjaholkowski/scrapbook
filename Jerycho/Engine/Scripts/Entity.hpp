#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Entity/Entity.hpp"

namespace jas
{

class Entity
{
private:
	static const char className[];

	static int32_t getPhysicObject(lua_State* L);

	/*static int32_t getTagName(lua_State* L)
	{
		jaEntity2d* entity = static_cast<jaEntity2d*>(lua_touserdata(L,1));

		if(entity->tagName[0] == '\0')
			lua_pushnil(L);
		else
			lua_pushstring(L,entity->tagName);

		return 1;
	}*/

	static int32_t getComponent(lua_State* L);
	static int32_t setTagName(lua_State* L);

public:

/*static void pushOnStack(lua_State* L, jaEntity2d* entity)
{
	lua_pushlightuserdata(L,(void*)entity);
	luaL_getmetatable(L, className);
	lua_setmetatable(L, -2);
}*/
	static void save(bool exportMode, FILE* pFile, const char* entityVar, const jaVector2& worldPosition, const ja::Entity* entity);


	static void registerClass(lua_State* L)
	{
		lua_newtable(L);
		int32_t table = lua_gettop(L);

		JAS_ADD_SCRIPT_FUNCTION(L, getPhysicObject, table);
		JAS_ADD_SCRIPT_FUNCTION(L, getComponent,    table);
		JAS_ADD_SCRIPT_FUNCTION(L, setTagName,      table);
		//JAS_ADD_SCRIPT_FUNCTION(L,getTagName,table);

		lua_setglobal(L, className); // setglobal remove table from stack
	}
};



}