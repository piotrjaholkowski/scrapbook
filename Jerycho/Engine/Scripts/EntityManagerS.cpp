#include "EntityManager.hpp"

namespace jas
{

const char EntityManager::className[] = "EntityManager";

int32_t EntityManager::createEntity(lua_State* L)
{
	ja::Entity* entity;

	if (lua_gettop(L) == 1)
	{
		uint16_t entityId = (uint16_t)lua_tointeger(L, 1);
		entity = ja::EntityManager::getSingleton()->createEntity(entityId);
	}
	else
	{
		entity = ja::EntityManager::getSingleton()->createEntity();
	}

	lua_pushlightuserdata(L, entity);

	return 1;
}

int32_t EntityManager::deleteEntityImmediate(lua_State* L)
{
	ja::Entity* entity = (ja::Entity*)lua_touserdata(L, 1);

	ja::EntityManager::getSingleton()->deleteEntityImmediate(entity);
	return 0;
}


int32_t EntityManager::getEntity(lua_State* L)
{
	uint16_t entityId  = (uint16_t)luaL_checkinteger(L, 1);
	ja::Entity* entity = ja::EntityManager::getSingleton()->getEntity(entityId);

	if (entity == nullptr)
	{
		lua_pushnil(L);
	}
	else
	{
		lua_pushlightuserdata(L, entity);
	}

	return 1;
}

}