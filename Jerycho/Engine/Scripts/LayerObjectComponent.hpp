#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Entity/Entity.hpp"
#include "../Entity/LayerObjectGroup.hpp"

namespace jas
{

class LayerObjectComponent
{
private:
	static const char className[];

	static int32_t create(lua_State* L);
	//static int32_t setRoot(lua_State* L);
	static int32_t setAutoDelete(lua_State* L);
	static int32_t setFixedRotation(lua_State* L);
	static int32_t addLayerObject(lua_State* L);
	static int32_t setPhysicComponent(lua_State* L);

	//static int32_t setPhysicObject(lua_State* L);
	//static int32_t getPhysicObject(lua_State* L);

public:

	static void registerClass(lua_State* L)
	{
		lua_newtable(L);
		int32_t table = lua_gettop(L);

		JAS_ADD_SCRIPT_FUNCTION(L, create, table);
		//JAS_ADD_SCRIPT_FUNCTION(L, setRoot, table);
		JAS_ADD_SCRIPT_FUNCTION(L, setAutoDelete, table);
		JAS_ADD_SCRIPT_FUNCTION(L, setFixedRotation, table);
		JAS_ADD_SCRIPT_FUNCTION(L, addLayerObject, table);
		JAS_ADD_SCRIPT_FUNCTION(L, setPhysicComponent, table);
		//JAS_ADD_SCRIPT_FUNCTION(L, setPhysicObject, table);
		//JAS_ADD_SCRIPT_FUNCTION(L, getPhysicObject, table);

		lua_setglobal(L, className); // setglobal remove table from stack
	}

};

}
