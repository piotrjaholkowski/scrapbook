#include "ShaderBinder.hpp"

namespace jas
{

const char ShaderBinder::className[] = "ShaderBinder";



int32_t ShaderBinder::bind(lua_State* L)
{
	ja::ShaderBinder* shaderBinder = (ja::ShaderBinder*)(lua_touserdata(L, 1));
	shaderBinder->bind();
	
	return 0;
}

int32_t ShaderBinder::unbind(lua_State* L)
{
	ja::ShaderBinder::unbind();

	return 0;
}


}