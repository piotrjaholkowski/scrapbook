#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Manager/UIManager.hpp"

namespace jas
{
	class UIManager
	{
	private:
		static const char className[];

		//bool isMouseOverGui()
		static int32_t isMouseOverGui(lua_State* L)
		{
			lua_pushboolean(L,ja::UIManager::getSingleton()->isMouseOverGui());

			return 1;
		}

		static int32_t isGuiFocused(lua_State* L)
		{
			if (ja::UIManager::getSingleton()->getLastSelected() == nullptr)
				lua_pushboolean(L,0);
			else
				lua_pushboolean(L,1);

			return 1;
		}

		static int32_t createWidgetDef(lua_State* L)
		{
			ja::Widget* widget = ja::UIManager::getSingleton()->createWidgetDef();
			lua_pushlightuserdata(L,widget);

			return 1;
		}

		static int32_t getWidgetDef(lua_State* L)
		{
			ja::WidgetDef* widgetDef = &ja::UIManager::getSingleton()->widgetDef;
			lua_pushlightuserdata(L,widgetDef);

			return 1;
		}

		static int32_t addInterface(lua_State* L)
		{
			ja::string name = lua_tostring(L,1);
			ja::Interface* newInterface = (ja::Interface*)lua_touserdata(L,2);

			ja::UIManager::getSingleton()->addInterface(name, newInterface);
			return 0;
		}

		static int32_t getInterface(lua_State* L)
		{
			ja::string name = lua_tostring(L,1);
			ja::Interface* scriptInterface = ja::UIManager::getSingleton()->getInterface(name);

			lua_pushlightuserdata(L,scriptInterface);
			return 1;
		}

		static int32_t registerDelegate(lua_State* L)
		{
			ja::string name = lua_tostring(L,1);
			ja::ScriptDelegate* scriptDelegate = ja::UIManager::getSingleton()->registerDelegate(name);

			lua_pushlightuserdata(L,scriptDelegate);
			return 1;
		}

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,isMouseOverGui,table);
			JAS_ADD_SCRIPT_FUNCTION(L,isGuiFocused,table);
			JAS_ADD_SCRIPT_FUNCTION(L,createWidgetDef,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getWidgetDef,table);
			JAS_ADD_SCRIPT_FUNCTION(L,addInterface,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getInterface,table);
			JAS_ADD_SCRIPT_FUNCTION(L,registerDelegate,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	const char UIManager::className[] = "UIManager";
}
