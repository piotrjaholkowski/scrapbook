#include "ScriptComponent.hpp"

#include "../Entity/ScriptGroup.hpp"

namespace jas
{

	const char ScriptComponent::className[] = "ScriptComponent";

	int32_t ScriptComponent::setFunction(lua_State* L)
	{
		ja::ScriptComponent* scriptComponent = (ja::ScriptComponent*)lua_touserdata(L, 1);
		const char*          functionName    = lua_tostring(L, 2);
		const char*          fileName        = nullptr;
		

		if (lua_gettop(L) == 3)
		{
			fileName = lua_tostring(L, 3);
		}

		int32_t newFunctionHash     = ja::ScriptFunction::getHash(functionName);
		int32_t newFunctionFileHash = scriptComponent->getUpdateFileHash();

		if (nullptr != fileName)
			newFunctionFileHash = ja::ScriptFunction::getHash(fileName);

		const ja::ScriptFunction* newScriptFunction = ja::ScriptManager::getSingleton()->getScriptFunction(ja::ScriptFunctionType::UPDATE_ENTITY, newFunctionFileHash, newFunctionHash);

		if (nullptr != newScriptFunction)
		{
			scriptComponent->setUpdateFunction(*newScriptFunction);
		}

		return 0;
	}

	int32_t ScriptComponent::create(lua_State* L)
	{
		ja::Entity* entity = (ja::Entity*)lua_touserdata(L, 1);

		ja::ScriptComponent* scriptComponent = ja::ScriptGroup::getSingleton()->createComponent(entity); 

		lua_pushlightuserdata(L, scriptComponent);

		return 1;
	}

}