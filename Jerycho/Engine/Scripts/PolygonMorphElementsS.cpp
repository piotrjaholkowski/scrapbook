#include "PolygonMorphElements.hpp"
#include "DrawElements.hpp"

namespace jas
{
	
const char PolygonMorphElements::className[] = "PolygonMorphElements";



void PolygonMorphElements::save(FILE* pFile, const char* morphElementsVar, const ja::PolygonMorphElements& polygonMorphElements)
{
	const uint16_t morphsNum = polygonMorphElements.getMorphsNum();

	const char* polygonMorphVar = "polygonMorph";
	const char* drawElementsVar = "drawElements";

	ja::MorphEntry* morphEntry = polygonMorphElements.getMorphEntries();

	for (uint16_t i = 0; i < morphsNum; ++i)
	{
		ja::PolygonMorph* polygonMorph = (ja::PolygonMorph*)morphEntry[i].layerObjectMorph;

		//local polygonMorph = MorphElements.createMorph(morphElements);
		fprintf(pFile, "local %s = MorphElements.createMorph(%s,'%s');\n", polygonMorphVar, morphElementsVar, polygonMorph->getName());
		fprintf(pFile, "local %s = PolygonMorph.getDrawElements(%s);\n", drawElementsVar, polygonMorphVar);
		DrawElements::save(pFile, drawElementsVar, polygonMorph->drawElements);

		const jaVector2 translate = polygonMorph->transform.position;
		const jaVector2 scale     = polygonMorph->scale;
		const float     rotation  = polygonMorph->transform.rotationMatrix.getRadians();

		fprintf(pFile, "PolygonMorph.setPosition(%s,Vector2.create(%f,%f));\n", polygonMorphVar, translate.x, translate.y);
		fprintf(pFile, "PolygonMorph.setRotation(%s, %f);\n", polygonMorphVar, rotation);
		fprintf(pFile, "PolygonMorph.setScale(%s, Vector2.create(%f,%f));\n", polygonMorphVar, scale.x, scale.y);
		fprintf(pFile, "PolygonMorph.setActiveFeatures(%s, %x);\n", polygonMorphVar, polygonMorph->getActiveFeatures());
	}
}


}