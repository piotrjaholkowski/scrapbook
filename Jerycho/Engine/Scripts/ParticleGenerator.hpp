#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Physics/ParticleGenerator2d.hpp"

namespace jas
{
	class ParticleGenerator
	{
	private:
		static const char className[];

		static int32_t isSleeping(lua_State* L)
		{
			ja::PhysicObject2d* physicObject = (ja::PhysicObject2d*)(lua_touserdata(L,1));
			lua_pushboolean(L,physicObject->isSleeping());

			return 1;
		}

		static int32_t setSpawnDelay(lua_State* L)
		{
			ja::ParticleGenerator2d* particleGenerator = (ja::ParticleGenerator2d*)(lua_touserdata(L,1));
			jaFloat spawnDelay = (float)(lua_tonumber(L,2));

			particleGenerator->nextSpawnTime = spawnDelay;

			return 0;
		}

		static int32_t getSpawnDelay(lua_State* L)
		{
			ja::ParticleGenerator2d* particleGenerator = (ja::ParticleGenerator2d*)(lua_touserdata(L,1));

			lua_pushnumber(L,particleGenerator->nextSpawnTime);

			return 1;
		}

	public:
		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,isSleeping,table);
			JAS_ADD_SCRIPT_FUNCTION(L,setSpawnDelay,table);
			JAS_ADD_SCRIPT_FUNCTION(L,getSpawnDelay,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

	const char ParticleGenerator::className[] = "ParticleGenerator";
}
