#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Graphics/LayerRenderer.hpp"

namespace jas
{
	class LayerRenderer
	{
	private:
		static const char className[];

		static int32_t renderLayer(lua_State* L)
		{
			ja::LayerRenderer* layerRender = (ja::LayerRenderer*)( lua_touserdata(L,1) );
			uint8_t layerId = (uint8_t)lua_tointeger(L,2);

			layerRender->renderLayer(layerId);

			return 0;
		}

	public:

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L, renderLayer, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};

	const char LayerRenderer::className[] = "LayerRenderer";
}