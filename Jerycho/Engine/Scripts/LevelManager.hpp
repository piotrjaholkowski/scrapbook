#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../Manager/LevelManager.hpp"
#include "ScriptFunctions.hpp"

namespace jas
{
	class LevelManager
	{
	private:
		static const char className[];
		static int32_t saveLevel(lua_State* L);
		static int32_t loadLevel(lua_State* L);
		static int32_t reloadLevel(lua_State* L);
		static int32_t reloadLevelSafe(lua_State* L);
		static int32_t loadShader(lua_State* L);
		static int32_t createShaderBinder(lua_State* L);
		static int32_t createMaterial(lua_State* L);
		static int32_t printCurrentTask(lua_State* L);
		static int32_t printFinishedTask(lua_State* L);
		static int32_t printFailureTask(lua_State* L);

	public:

		static void registerClass(lua_State* L) {

			lua_newtable(L);
			int32_t table = lua_gettop(L);

			JAS_ADD_SCRIPT_FUNCTION(L,saveLevel,table);
			JAS_ADD_SCRIPT_FUNCTION(L,loadLevel,table);
			JAS_ADD_SCRIPT_FUNCTION(L,reloadLevel,table);
			JAS_ADD_SCRIPT_FUNCTION(L,reloadLevelSafe,table);
			JAS_ADD_SCRIPT_FUNCTION(L,loadShader,table);
			JAS_ADD_SCRIPT_FUNCTION(L,createShaderBinder,table);
			JAS_ADD_SCRIPT_FUNCTION(L,createMaterial,table);
			JAS_ADD_SCRIPT_FUNCTION(L,printCurrentTask,table);
			JAS_ADD_SCRIPT_FUNCTION(L,printFinishedTask,table);
			JAS_ADD_SCRIPT_FUNCTION(L,printFailureTask,table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}

	};

	
}