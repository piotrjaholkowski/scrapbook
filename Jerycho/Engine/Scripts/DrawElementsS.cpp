#include "DrawElements.hpp"
#include "../Manager/RecycledMemoryManager.hpp"

namespace jas
{

const char DrawElements::className[] = "DrawElements";

int32_t DrawElements::setVerticesPositions(lua_State* L)
{
	ja::DrawElements* drawElements = (ja::DrawElements*)lua_touserdata(L, 1);

	luaL_checktype(L, 2, LUA_TTABLE);

#ifdef LUA53
	const uint32_t arraySize = (uint32_t)luaL_len(L, 2);
#else
	const uint32_t arraySize = lua_objlen(L, 2);
#endif

	RecycledMemoryManager* memoryManager = RecycledMemoryManager::getSingleton();
	void* memoryBlock = memoryManager->getMemoryBlock(ja::setup::graphics::POLYGON_ALLOCATOR_SIZE);

	//StackAllocator<float> colorVerticesArray;
	//colorVerticesArray.resize(arraySize * sizeof(float));

	//float* colorVerticesData = colorVerticesArray.getFirst() - 1;
	float* positionVerticesData = ((float*)memoryBlock) - 1;

	//int32_t                  i = 0;
	const uint32_t verticesNum = arraySize / 2;

	/*lua_pushnil(L);

	while (lua_next(L, -2) != 0)
	{
	colorVerticesData[i++] = (float)lua_tonumber(L, -1);
	lua_pop(L, 1);
	}*/

	//Faster method
	for (uint32_t i = 1; i <= arraySize; i++) {
		lua_rawgeti(L, 2, i);
		positionVerticesData[i] = (float)lua_tonumber(L, -1);
		lua_pop(L, 1);
	}

	positionVerticesData += 1;

	drawElements->setVerticesPositions((jaVector2*)positionVerticesData, verticesNum);

	memoryManager->releaseMemoryBlock(memoryBlock);

	return 0;
}

int32_t DrawElements::setVerticesColors(lua_State* L)
{
	ja::DrawElements* drawElements = (ja::DrawElements*)lua_touserdata(L, 1);

	luaL_checktype(L, 2, LUA_TTABLE);

#ifdef LUA53
	const uint32_t arraySize = (uint32_t)luaL_len(L, 2);
#else
	const uint32_t arraySize = lua_objlen(L, 2);
#endif

	RecycledMemoryManager* memoryManager = RecycledMemoryManager::getSingleton();
	void* memoryBlock = memoryManager->getMemoryBlock(ja::setup::graphics::POLYGON_ALLOCATOR_SIZE);

	//StackAllocator<float> colorVerticesArray;
	//colorVerticesArray.resize(arraySize * sizeof(float));

	//float* colorVerticesData = colorVerticesArray.getFirst() - 1;
	float* colorVerticesData = ((float*)memoryBlock) - 1;

	//int32_t                  i = 0;
	const uint32_t verticesNum = arraySize / 4;

	/*lua_pushnil(L);

	while (lua_next(L, -2) != 0)
	{
		colorVerticesData[i++] = (float)lua_tonumber(L, -1);
		lua_pop(L, 1);
	}*/

	//Faster method
	for (uint32_t i = 1; i <= arraySize; i++) {
		lua_rawgeti(L, 2, i);
		colorVerticesData[i] = (float)lua_tonumber(L, -1);
		lua_pop(L, 1);
	}

	colorVerticesData += 1;

	drawElements->setVerticesColors((ja::Color4f*)colorVerticesData, verticesNum);

	memoryManager->releaseMemoryBlock(memoryBlock);

	return 0;
}



int32_t DrawElements::setTriangleIndices(lua_State* L)
{
	ja::DrawElements* drawElements = (ja::DrawElements*)lua_touserdata(L, 1);
	
	luaL_checktype(L, 2, LUA_TTABLE);
	//const int32_t arraySize = (int32_t)luaL_len(L, 2);
	const uint32_t arraySize = lua_objlen(L, 2);

	//StackAllocator<uint16_t> indicesArray;
	//indicesArray.resize(arraySize * sizeof(uint16_t));

	RecycledMemoryManager* memoryManager = RecycledMemoryManager::getSingleton();

	void* memoryBlock = memoryManager->getMemoryBlock(ja::setup::graphics::POLYGON_ALLOCATOR_SIZE);

	//uint16_t* indicesData = indicesArray.getFirst() - 1;
	uint32_t* indicesData = ((uint32_t*)memoryBlock) - 1;

	/*int32_t i = 0;

	lua_pushnil(L);

	while (lua_next(L, -2) != 0)
	{
		indicesData[i++] = (uint16_t)lua_tointeger(L, -1);
		lua_pop(L, 1);
	}*/

	for (uint32_t i = 1; i <= arraySize; i++) {
		lua_rawgeti(L, 2, i);
		indicesData[i] = (uint32_t)lua_tointeger(L, -1);
		lua_pop(L, 1);
	}

	indicesData += 1;

	drawElements->setTriangleIndices(indicesData, arraySize);

	memoryManager->releaseMemoryBlock(memoryBlock);

	return 0;
}

void DrawElements::save(FILE* pFile, const char* drawElementsVar, const ja::DrawElements& drawElements)
{
	if (drawElements.getVerticesNum() > 0)
	{
		fprintf(pFile, "DrawElements.setVerticesPositions(%s, {", drawElementsVar);
		uint32_t               verticesNum = drawElements.getVerticesNum();

		const jaVector2* vertices = drawElements.getVerticesPositions();
		for (uint32_t i = 0; i < verticesNum - 1; ++i)
		{
			fprintf(pFile, "%f, %f,\n", vertices[i].x, vertices[i].y);
		}

		uint32_t z = verticesNum - 1;
		fprintf(pFile, "%f, %f});\n", vertices[z].x, vertices[z].y);
	}

	if (drawElements.getVerticesNum() > 0)
	{
		fprintf(pFile, "DrawElements.setVerticesColors(%s, {", drawElementsVar);
		uint32_t               verticesNum = drawElements.getVerticesNum();

		const ja::Color4f* colorVertices = drawElements.getVerticesColors();
		for (uint32_t i = 0; i < verticesNum - 1; ++i)
		{
			fprintf(pFile, "%f, %f, %f, %f\n", colorVertices[i].r, colorVertices[i].g, colorVertices[i].b, colorVertices[i].a);
		}

		uint32_t z = verticesNum - 1;
		fprintf(pFile, "%f, %f, %f, %f});\n", colorVertices[z].r, colorVertices[z].g, colorVertices[z].b, colorVertices[z].a);
	}
	
	if (drawElements.getTriangleIndicesNum() > 0)
	{
		fprintf(pFile, "DrawElements.setTriangleIndices(%s,{", drawElementsVar);
		uint32_t        triangleIndicesNum = drawElements.getTriangleIndicesNum();
		const uint32_t* pIndices = drawElements.getTriangleIndices();
		for (uint32_t i = 0; i < triangleIndicesNum - 1; ++i)
		{
			fprintf(pFile, "%d,", pIndices[i]);
		}
		fprintf(pFile, "%d});\n", pIndices[triangleIndicesNum - 1]);
	}
	
}

}