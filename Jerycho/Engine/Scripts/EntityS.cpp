#include "Entity.hpp"
#include "../Entity/LayerObjectGroup.hpp"
#include "../Entity/ScriptGroup.hpp"

namespace jas
{

const char Entity::className[] = "Entity";

int32_t Entity::getPhysicObject(lua_State* L)
{
	ja::Entity* entity = (ja::Entity*)lua_touserdata(L, 1);

	ja::PhysicObject2d* physicObject = entity->getPhysicObject();

	if (physicObject == nullptr)
		lua_pushnil(L);
	else
		lua_pushlightuserdata(L, physicObject);

	return 1;
}

int32_t Entity::getComponent(lua_State* L)
{
	ja::Entity* entity = (ja::Entity*)(lua_touserdata(L, 1));
	uint8_t     componentType = (uint8_t)(lua_tointeger(L, 2));
	int32_t     isString = lua_isstring(L, 3);

	ja::EntityComponent* component = nullptr;
	if (isString)
	{
		const char* hashTagName = lua_tostring(L, 3);
		component = entity->getComponent(componentType, hashTagName);
	}
	else
		component = entity->getComponent(componentType);

	if (component == nullptr)
		lua_pushnil(L);
	else
		lua_pushlightuserdata(L, component);

	return 1;
}

int32_t Entity::setTagName(lua_State* L)
{
	ja::Entity* entity  = (ja::Entity*)(lua_touserdata(L, 1)); 
	const char* tagName = lua_tostring(L, 2);

	entity->setTagName(tagName);

	return 0;
}

void Entity::save(bool exportMode, FILE* pFile, const char* entityVar, const jaVector2& worldPosition, const ja::Entity* entity)
{
	fprintf(pFile, "local %s = EntityManager.createEntity();\n", entityVar);

	const char* tagName = entity->getTagName();

	if (nullptr != tagName)
		fprintf(pFile, "Entity.setTagName(%s,'%s');\n", entityVar, entity->getTagName());

	ja::EntityComponent* itComponent = entity->getFirstComponent();

	const char* physicComponentVar      = "physicComponent";
	const char* layerObjectComponentVar = "layerObjectComponent";
	const char* scriptComponentVar      = "scriptComponent";
	const char* dataComponentVar        = "dataComponent";

	for (itComponent; nullptr != itComponent; itComponent = itComponent->getNextComponent())
	{
		switch (itComponent->getComponentType())
		{
			case (uint8_t)ja::EntityGroupType::PHYSIC_OBJECT:
			{
				ja::PhysicComponent* physicComponent = (ja::PhysicComponent*)itComponent;
				
				fprintf(pFile, "local %s = PhysicComponent.create(%s);\n", physicComponentVar, entityVar);
				
				const char* tagName = itComponent->getTagName();
				if (nullptr != tagName)
				{
					fprintf(pFile, "Component.setTagName(%s,'%s');\n", physicComponentVar, tagName);
				}
				
				ja::PhysicObject2d* physicObject = physicComponent->getPhysicObject();
				if (nullptr != physicObject)
				{
					fprintf(pFile, "PhysicComponent.setPhysicObject(%s,physicObjects[%d]);\n", physicComponentVar, physicObject->getId());
				}
				
				fprintf(pFile, "PhysicComponent.setAutoDelete(%s,%s);\n", physicComponentVar, ja::getBoolString(physicComponent->autoDelete));
				
				if (physicComponent->isRoot())
					fprintf(pFile, "PhysicComponent.setRoot(%s);\n", physicComponentVar);

			}
				break;

			case (uint8_t)ja::EntityGroupType::LAYER_OBJECT:
			{
				ja::LayerObjectComponent* layerObjectComponent = (ja::LayerObjectComponent*)itComponent;

				fprintf(pFile, "local %s = LayerObjectComponent.create(%s);\n", layerObjectComponentVar, entityVar);

				const char* tagName = itComponent->getTagName();
				if (nullptr != tagName)
				{
					fprintf(pFile, "Component.setTagName(%s,'%s');\n", layerObjectComponentVar, tagName);
				}

				{
					//save layer objects
					ja::LayerObjectElement* layerObjectElement = layerObjectComponent->getLayerObjectCollection();

					for (layerObjectElement; layerObjectElement != nullptr; layerObjectElement = layerObjectElement->getNext())
					{
						fprintf(pFile, "LayerObjectComponent.addLayerObject(layerObject[%d]);\n", layerObjectElement->layerObject->getId());
					}
				}

				ja::PhysicObject2d* physicObject = (ja::PhysicObject2d*)layerObjectComponent->getPhysicObject();

				if (nullptr != physicObject)
				{
					fprintf(pFile, "local %s = PhysicObject.getComponentData(physicObjects[%d]);\n", physicComponentVar, physicObject->getId());
					fprintf(pFile, "LayerObjectComponent.setPhysicComponent(%s,%s);\n", layerObjectComponentVar, physicComponentVar);
				}
				

				fprintf(pFile, "LayerObjectComponent.setAutoDelete(%s,%s);\n", layerObjectComponentVar, ja::getBoolString(layerObjectComponent->isAutoDelete()) );
			}
				break;

			case (uint8_t)ja::EntityGroupType::SCRIPT:
			{
				ja::ScriptComponent* scriptComponent = (ja::ScriptComponent*)itComponent;

				fprintf(pFile, "local %s = ScriptComponent.create(%s);\n", scriptComponentVar, entityVar);

				const char* tagName = itComponent->getTagName();
				if (nullptr != tagName)
				{
					fprintf(pFile, "Component.setTagName(%s,'%s');\n", layerObjectComponentVar, tagName);
				}

				if (0 != scriptComponent->getUpdateHash())
				{
					const ja::ScriptFunction* scriptFunction = ja::ScriptManager::getSingleton()->getScriptFunction(ja::ScriptFunctionType::UPDATE_ENTITY, scriptComponent->getUpdateFileHash(), scriptComponent->getUpdateHash());

					fprintf(pFile, "ScriptComponent.setFunction(%s,'%s','%s');\n", scriptComponentVar, scriptFunction->functionName, scriptFunction->fileName);
				}
				
			}
				break;

			case (uint8_t)ja::EntityGroupType::DATA:
			{
				ja::DataComponent* dataComponent = (ja::DataComponent*)itComponent;

				fprintf(pFile, "local %s = DataComponent.create(%s);\n", dataComponentVar, entityVar);

				ja::DataModel* dataModel = dataComponent->getDataModel();
				if (nullptr != dataModel)
				{
					fprintf(pFile, "DataComponent.setDataModel(%s,DataModel.%s);\n", dataComponentVar, dataModel->getName());

					StackAllocator<ja::DataFieldInfo>* dataFields = dataModel->getDataFields();

					const uint32_t     dataFieldsNum = dataFields->getSize();
					ja::DataFieldInfo* pDataField = dataFields->getFirst();

					for (uint32_t i = 0; i < dataFieldsNum; ++i)
					{
						switch (pDataField[i].dataType)
						{
						case ja::DataType::Int32:
						{
							pDataField[i].save<int32_t>(pFile, dataComponentVar, dataModel->getName(), dataComponent->getAllocatedData());
						}
						break;
						case ja::DataType::Float:
						{
							pDataField[i].save<float>(pFile, dataComponentVar, dataModel->getName(), dataComponent->getAllocatedData());
						}
						break;
						}
					}
				}
			}
			break;
		}

	}
}

}