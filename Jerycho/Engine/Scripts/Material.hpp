#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}



#include "../Scripts/ScriptFunctions.hpp"

#include <stdint.h>

namespace jas
{

	class Material
	{
	private:
		static const char className[];
	public:

		//static int32_t create(lua_State* L);
		static int32_t setBlendEquation(lua_State* L);
		static int32_t setSourceFactor(lua_State* L);
		static int32_t setDestinationFactor(lua_State* L);
		static int32_t setFloat(lua_State* L);
		static int32_t setBool(lua_State* L);

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			//JAS_ADD_SCRIPT_FUNCTION(L, create, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setBlendEquation, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setSourceFactor, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setDestinationFactor, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setFloat, table);
			JAS_ADD_SCRIPT_FUNCTION(L, setBool, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, setRange, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, setDataAccess, table);

			lua_setglobal(L, className);
		}

	};



}