#include "DataFieldInfo.hpp"
#include "../Manager/ScriptManager.hpp"

namespace jas
{

const char DataFieldInfo::className[] = "DataFieldInfo";

int32_t DataFieldInfo::create(lua_State* L)
{
	ja::DataFieldInfo* dataField = new (lua_newuserdata(L, sizeof(ja::DataFieldInfo))) ja::DataFieldInfo();

	return 1;
}

int32_t DataFieldInfo::setDataType(lua_State* L)
{
	 ja::DataFieldInfo* dataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, 1);
	 int32_t            dataType      = lua_tointeger(L, 2);

	 dataFieldInfo->dataType = (ja::DataType)dataType;

	 return 0;
}

int32_t DataFieldInfo::setDataWidget(lua_State* L)
{
	ja::DataFieldInfo* dataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, 1);
	int32_t            dataWidget    = lua_tointeger(L, 2);

	dataFieldInfo->dataWidget = (ja::DataWidget)dataWidget;

	return 0;
}

int32_t DataFieldInfo::setRange(lua_State* L)
{
	ja::DataFieldInfo* dataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, 1);
	float              rangeMin      = (float)lua_tonumber(L, 2);
	float              rangeMax      = (float)lua_tonumber(L, 3);

	dataFieldInfo->rangeMin = rangeMin;
	dataFieldInfo->rangeMax = rangeMax;

	return 0;
}

int32_t DataFieldInfo::setDataAccess(lua_State* L)
{
	ja::DataFieldInfo* dataFieldInfo = (ja::DataFieldInfo*)lua_touserdata(L, 1);

	if (LUA_TTABLE == lua_type(L, 2))
	{
		lua_rawgeti(L, 2, 1);
		const char*    accessFunctionName = lua_tostring(L, -1);
		const uint32_t accessFunctionHash = ja::ScriptFunction::getHash(accessFunctionName);
		lua_pop(L, 1);

		lua_rawgeti(L, 2, 2);
		const char*    accessFileName = lua_tostring(L, -1);
		const uint32_t accessFileHash = ja::ScriptFunction::getHash(accessFileName);
		lua_pop(L, 1);

		ja::ScriptFunction* accessDataFunction = (ja::ScriptFunction*)ja::ScriptManager::getSingleton()->getScriptFunction(ja::ScriptFunctionType::ACCESS_DATA, accessFileHash, accessFunctionHash);
		dataFieldInfo->setAccessDataFunction(accessDataFunction);
	}

	return 0;
}

}