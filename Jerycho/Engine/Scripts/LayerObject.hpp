#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "ScriptFunctions.hpp"
#include "../Graphics/LayerObject.hpp"

namespace jas
{
	class LayerObject
	{
	private:
		static const char className[];

	public:

		static void save(bool exportMode, FILE* pFile, const char* layerObjectDefVar, const jaVector2& worldPosition, const ja::LayerObject* layerObject);

		static void registerClass(lua_State* L)
		{
			lua_newtable(L);
			int32_t table = lua_gettop(L);

			//JAS_ADD_SCRIPT_FUNCTION(L, isSleeping, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, getPrevious, table);
			//JAS_ADD_SCRIPT_FUNCTION(L, setTimeFactor, table);

			lua_setglobal(L, className); // setglobal remove table from stack
		}
	};


}