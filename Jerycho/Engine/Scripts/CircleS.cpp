#include "Circle.hpp"

namespace jas
{

const char Circle::className[] = "Circle";

int32_t Circle::setRadius(lua_State* L)
{
	gil::Circle2d* circle = (gil::Circle2d*)(lua_touserdata(L, 1));
	const float radius = (float)lua_tonumber(L, 2);

	circle->radius = radius;

	return 0;
}

}
