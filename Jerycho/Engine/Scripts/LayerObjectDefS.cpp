#include "LayerObjectDef.hpp"

namespace jas
{
	const char LayerObjectDef::className[] = "LayerObjectDef";

	int32_t LayerObjectDef::setPosition(lua_State* L)
	{
		ja::LayerObjectDef* layerObjectDef = (ja::LayerObjectDef*)lua_touserdata(L, 1);
		jaVector2*          position = (jaVector2*)lua_touserdata(L, 2);

		layerObjectDef->transform.position = *position;

		return 0;
	}

	int32_t LayerObjectDef::setRotation(lua_State* L)
	{
		ja::LayerObjectDef* layerObjectDef = (ja::LayerObjectDef*)lua_touserdata(L, 1);
		float               rotationRad = (float)lua_tonumber(L, 2);

		layerObjectDef->transform.rotationMatrix.setRadians(rotationRad);

		return 0;
	}

	int32_t LayerObjectDef::setScale(lua_State* L)
	{
		ja::LayerObjectDef* layerObjectDef = (ja::LayerObjectDef*)lua_touserdata(L, 1);
		jaVector2*          scale = (jaVector2*)lua_touserdata(L, 2);

		layerObjectDef->scale = *scale;

		return 0;
	}

	int32_t LayerObjectDef::setLayerObjectType(lua_State* L)
	{
		ja::LayerObjectDef* layerObjectDef = (ja::LayerObjectDef*)lua_touserdata(L, 1);
		uint8_t             layerObjectType = (uint8_t)lua_tointeger(L, 2);

		layerObjectDef->layerObjectType = layerObjectType;

		return 0;
	}

	int32_t LayerObjectDef::setDepth(lua_State* L)
	{
		ja::LayerObjectDef* layerObjectDef = (ja::LayerObjectDef*)lua_touserdata(L, 1);
		uint16_t            depth = (uint16_t)lua_tonumber(L, 2);

		layerObjectDef->depth = depth;

		return 0;
	}

	int32_t LayerObjectDef::setLayerId(lua_State* L)
	{
		ja::LayerObjectDef* layerObjectDef = (ja::LayerObjectDef*)lua_touserdata(L, 1);
		uint8_t             layerId = (uint8_t)lua_tointeger(L, 2);

		layerObjectDef->layerId = layerId;

		return 0;
	}

	int32_t LayerObjectDef::getDrawElements(lua_State* L)
	{
		ja::LayerObjectDef* layerObjectDef = (ja::LayerObjectDef*)lua_touserdata(L, 1);

		lua_pushlightuserdata(L, layerObjectDef->drawElements);

		return 1;
	}

	int32_t LayerObjectDef::getPolygonMorphElements(lua_State* L)
	{
		ja::LayerObjectDef* layerObjectDef = (ja::LayerObjectDef*)lua_touserdata(L, 1);

		lua_pushlightuserdata(L, layerObjectDef->polygonMorphElements);

		return 1;
	}

	int32_t LayerObjectDef::getCameraMorphElements(lua_State* L)
	{
		ja::LayerObjectDef* layerObjectDef = (ja::LayerObjectDef*)lua_touserdata(L, 1);

		lua_pushlightuserdata(L, layerObjectDef->cameraMorphElements);

		return 1;
	}

	int32_t LayerObjectDef::setCameraName(lua_State* L)
	{
		ja::LayerObjectDef* layerObjectDef = (ja::LayerObjectDef*)lua_touserdata(L, 1);
		const char*         cameraName     = lua_tostring(L, 2);

		layerObjectDef->cameraName.setName(cameraName);

		return 0;
	}
}