#include "../jaSetup.hpp"
#include "Test.hpp"
#include <conio.h>
#include <stdarg.h>

#include "../Manager/LogManager.hpp"
#include "TimeStamp.hpp"

#ifdef JA_SDL2_VERSION
	#include <SDL2/SDL.h>
#else
	#include <SDL/SDL.h>
#endif

#include <intrin.h>

#ifdef _WIN32
#include <windows.h>
#endif

using namespace ja;

void Test::init( const char* testFileName)
{
	SDL_Init(SDL_INIT_NOPARACHUTE);
	LogManager::initLogManager();
	std::ifstream file;
	file.open(testFileName);
	
	if(!file.is_open())
	{
		std::cout << "Can't find " << testFileName << std::endl;
		getch();
		return;
	}

	while(!file.eof())
	{
		char testSuiteName[JATEST_SUITE_NAME_LENGTH + 2];
		file.getline(testSuiteName,JATEST_SUITE_NAME_LENGTH + 1);

		if(testSuiteName[0] == '+')
		{
			scheduledSuiteTests.push_back(&testSuiteName[1]);
		}
	}

	file.close();
	SDL_Quit();
}

void Test::clearTests()
{
	std::map<ja::string, Test*>::iterator itTests = suiteTests.begin();

	for(itTests; itTests != suiteTests.end(); ++itTests)
	{
		Test* test = itTests->second;
		delete test;
	}
	suiteTests.clear();
}

void Test::run(TestSuiteRun testSuiteRun)
{
	logFile.open("Test.html");
	logFile << "<html>" << std::endl;
	logFile << "<head>" << std::endl;
	logFile << "<link rel =\"stylesheet\" type=\"text/css\" href=\"styles.css\">" << std::endl;
	logFile << "<script type = \"text/javascript\" src=\"Chart.js\"></script>" << std::endl;
	logFile << "</head>" << std::endl;
	logFile << "<body>" << std::endl;

	std::list<ja::string>::iterator itScheduled = scheduledSuiteTests.begin();
	std::map<ja::string, Test*>::iterator suiteFound;
	int32_t passedTests = 0;
	int32_t testNum = 0;
	int32_t passedSuites = 0;
	int32_t suitesNum = 0;

#ifdef _WIN32
	SetThreadAffinityMask(GetCurrentThread(), 1);
#endif 

	for(itScheduled; itScheduled != scheduledSuiteTests.end(); ++itScheduled)
	{
		suiteFound = suiteTests.find(*itScheduled);
		Test* suite = suiteFound->second;
		passedTests = 0;
		testNum = 0;

		if(suiteFound != suiteTests.end())
		{
			++suitesNum;
			logHtml("p", "suite", "", "SUITE TEST: %s\n", itScheduled->c_str());
			std::map<ja::string, TestCase>::iterator itTestCase = suite->testCase.begin();

			suite->onInitSuite();

			for(itTestCase; itTestCase != suite->testCase.end(); ++itTestCase)
			{
				if ((itTestCase->second.testType & testSuiteRun) == 0)
					continue;

				++testNum;
				try
				{
					suite->onInitTest();
					{
						TimeStamp timeStamp;
						itTestCase->second.testCaseFunction();
					}
					suite->onDeinitTest();
					++passedTests;

					logHtml("p", "case", "passed", "\tTEST CASE: %s \tPASSED Time:%llu ms %llu ticks\n", itTestCase->first.c_str(), TimeStamp::getMiliseconds(), TimeStamp::getTicks());
				}
				catch(Exception& ex)
				{
					logHtml("p", "case", "failed", "\tTEST CASE: %s \tFAILED Time:%llu ms %llu ticks\n", itTestCase->first.c_str(), TimeStamp::getMiliseconds(), TimeStamp::getTicks());
					logHtml("p", "reason", "", "at %s\n",ex.fileName.c_str());
					logHtml("p", "reason", "", "F(%s)L(%d)R(%s)\n",ex.functionName.c_str(),ex.lineNumber,ex.reason.c_str());
					suite->onDeinitTest();
				}
				catch(...)
				{
					logHtml("p", "case", "failed" ,"\tTEST CASE: %s \tFAILED\n", itTestCase->first.c_str());
					suite->onDeinitTest();
				}
			}

			if (passedTests == testNum) {
				++passedSuites;
				logHtml("p", "suite_result", "passed", "SUITE %s TESTS PASSED %i/%i\n\n", suite->suiteName, passedTests, testNum);
			}
			else {
				logHtml("p", "suite_result", "failed", "SUITE %s TESTS PASSED %i/%i\n\n", suite->suiteName, passedTests, testNum);
			}
			suite->onDeinitSuite();
		}	
	}

	if (passedSuites == suitesNum) {
		logHtml("p", "suite_result", "passed", "***********ALL SUITE TESTS PASSED***********\n");
	}
	else {
		logHtml("p", "suite_result", "failed", "**********SOME SUITE TESTS FAILED**********\n");
	}

	logFile << "</body>" << std::endl;
	logFile << "</html>" << std::endl;

	logFile.close();
	getch();	

	clearTests();
}

void Test::log( const char* message,... )
{
	char convertedMessage[1024];
	va_list	args;
	int32_t messageSize = 0;

	if (message == nullptr)
	{
		*convertedMessage=0;
	}
	else
	{
		va_start(args, message);
#pragma warning(disable : 4996)
		messageSize = vsprintf(convertedMessage, message, args);
#pragma warning(default : 4996)
		va_end(args);	
	}

	if(messageSize > 0)
	{
		std::cout << convertedMessage;
		logFile.write(convertedMessage, messageSize);
	}
	else
	{
		std::cout << "Parsing Error: " << message << std::endl;
		logFile.write("Parsing Error: ", strlen("Parsing Error: "));
		logFile.write(message, strlen(message));
	}	
}

void Test::logHtml(const char* tag, const char* idTag, const char* classTag, const char* message, ...)
{
	char    convertedMessage[1024];
	va_list	args;
	int32_t messageSize = 0;

	if (message == nullptr)
	{
		*convertedMessage = 0;
	}
	else
	{
		va_start(args, message);
#pragma warning(disable : 4996)
		messageSize = vsprintf(convertedMessage, message, args);
#pragma warning(default : 4996)
		va_end(args);
	}

	if (messageSize > 0)
	{
		std::cout << convertedMessage;
		logFile << "<" << tag << " id=\"" << idTag << "\" class=\"" << classTag << "\">";
		logFile.write(convertedMessage, messageSize);
		logFile << "</" << tag << ">";
	}
	else
	{
		std::cout << "Parsing Error: " << message << std::endl;
		logFile.write("Parsing Error: ", strlen("Parsing Error: "));
		logFile.write(message, strlen(message));
	}
}

void Test::logChart(const char* chartName, int chartWidth, int chartHeight)
{
	logFile << "<canvas id=" << "\"" << chartName << "\" " << "width=\"" << chartWidth << "\" height=\"" << chartHeight << "\"></canvas>" << std::endl;
}


void Test::cleanUp()
{
	if(logFile.is_open())
	{
		logFile << "</body></html>";
		logFile.close();
	}
}


std::ofstream Test::logFile;
std::map<ja::string, Test*> Test::suiteTests;
std::list<ja::string> Test::scheduledSuiteTests;
