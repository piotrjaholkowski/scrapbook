#pragma once

#include "../jaSetup.hpp"
#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <atomic>

#include "../Allocators/StackAllocator.hpp"
#include "../Allocators/StackPoolAllocator.hpp"
#include "../Allocators/HashMap.hpp"
#include "../Utility/NameTag.hpp"

#define JA_HIGH_RESOLUTION_TIME

class CacheLineAllocator;

namespace ja
{

inline uint64_t QueryPerformance()
{
	/*uint32_t loword, hiword;

	_asm
	{
	RDTSC
	mov hiword, edx
	mov loword, eax
	}
	return (static_cast<uint64_t>(hiword) << 32) + loword;*/

	//int32_t cpuInfo[4];
	//__cpuid(cpuInfo, 0); // serialization instruction
	return __rdtsc();
	//return __rdtscp();
}

#ifdef JA_USE_PROFILER

#define PROFILER_REG(ProfilerTag) static uint32_t ProfilerTag##__LINE__ = ja::Profiler::registerOperationName(#ProfilerTag); ja::Profiler Profile##__LINE__(ProfilerTag##__LINE__);
//#define PROFILER_REGR(ProfilerTag) const char* ProfilerTag = #ProfilerTag;
#define PROFILER_FUN static uint32_t ProfilerTag##__FUNCTION__ = ja::Profiler::registerOperationName(__FUNCTION__); ja::Profiler Profile##__LINE__(ProfilerTag##__FUNCTION__);

#define PROFILER(ProfileTag) ja::Profiler Profile##__LINE__(ProfileTag);
#define JATEST_TIMESTAMP(Operation) ja::TimeStamp timeStamp##Operation(Operation)
#else
#define JATEST_TIMESTAMP(Operation)
#define PROFILER(ProfileTag)
#endif

class TimeStampData
{
public:
	uint64_t        deltaTime;
	uint64_t        ticks;
	uint8_t         operationType;
};

typedef NameTag<45> ProfileOperationTag;

class ProfileOperationData
{
public:
	uint64_t        ticks;
	uint32_t        operationNameHash;
	bool            isMeasuringStart;
};

class ProfileOperationInfo
{
public:
	uint64_t              startTime;
	uint64_t              endTime;
	double                deltaTime;
	uint32_t              operationStart;
	uint32_t              operationEnd;
	ProfileOperationInfo* parent;
	ProfileOperationTag   operationTag;

	StackPoolAllocator<ProfileOperationInfo> childOperations;

	ProfileOperationInfo(CacheLineAllocator* pCacheLineAllocator) : childOperations(pCacheLineAllocator) 
	{

	}

	~ProfileOperationInfo()
	{
		const uint32_t        childsNum         = childOperations.getSize();
		ProfileOperationInfo* pProfileOperation = childOperations.getFirst();

		for (uint32_t i = 0; i < childsNum; ++i)
		{
			pProfileOperation[i].~ProfileOperationInfo();
		}
	}
};

class InitCpuClockFrequency
{
public:
	InitCpuClockFrequency();
};

class TimeStamp
{
public:
	uint8_t operationType;
	std::chrono::steady_clock::time_point startTime;
	uint64_t             ticks;
private:
	static uint64_t      frequency;
public:
	static TimeStampData timeStamps[setup::profiler::timeStampNum];
	static uint32_t      currentTimeStamp;

	inline TimeStamp() : operationType(0)
	{
		std::atomic_signal_fence(std::memory_order_acq_rel);
		//std::atomic_thread_fence(std::memory_order_acq_rel);
		startTime = std::chrono::steady_clock::now();
		ticks = QueryPerformance();
	}

	inline TimeStamp(uint8_t opearationType) : operationType(opearationType)
	{
		std::atomic_signal_fence(std::memory_order_acq_rel);
		//std::atomic_thread_fence(std::memory_order_acq_rel);
		startTime = std::chrono::steady_clock::now();
		ticks = QueryPerformance();
	}

	inline ~TimeStamp()
	{
		std::atomic_signal_fence(std::memory_order_acq_rel);
		//std::atomic_thread_fence(std::memory_order_acq_rel);
		std::chrono::steady_clock::time_point endTime = std::chrono::steady_clock::now();
		ticks = QueryPerformance() - ticks;
		timeStamps[currentTimeStamp].deltaTime = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
		timeStamps[currentTimeStamp].ticks = ticks;
		timeStamps[currentTimeStamp].operationType = operationType;
		currentTimeStamp = ((currentTimeStamp + 1) % setup::profiler::timeStampNum);
	}

	inline static TimeStampData* getTimeStampData(uint8_t operationType) {
		for (uint32_t i = 0; i < currentTimeStamp; ++i) {
			if (timeStamps[i].operationType == operationType) {
				return &timeStamps[i];
			}
		}

		return nullptr;
	}

	inline static int32_t getLastTimeStampId()
	{
		return ((currentTimeStamp - 1) + setup::profiler::timeStampNum) % setup::profiler::timeStampNum;
	}

	inline static uint64_t getMiliseconds()
	{
		int32_t lastTimeStamp = getLastTimeStampId();
		return  timeStamps[lastTimeStamp].deltaTime;
	}

	inline static uint64_t getMicroseconds()
	{
		int32_t lastTimeStamp = getLastTimeStampId();
		return  timeStamps[lastTimeStamp].deltaTime * 1000;
	}

	inline static uint64_t getTicks()
	{
		int32_t lastTimeStamp = getLastTimeStampId();
		return  timeStamps[lastTimeStamp].ticks;
	}

	inline static TimeStampData getLastTimeStamp()
	{
		int32_t lastTimeStamp = getLastTimeStampId();
		return  timeStamps[lastTimeStamp];
	}

	inline static uint64_t getClockFrequency()
	{
		return frequency;
	}

	inline static void reset()
	{
		currentTimeStamp = 0;
	}

	friend class InitCpuClockFrequency;
};

class Profiler
{
public:
	uint32_t registeredOperationNameHash;

	static StackAllocator<ProfileOperationData> timeStamps;
	static HashMap<ProfileOperationTag>         registeredOperationNames;
	static CacheLineAllocator*                  operationNamesAllocator;

private:
	static uint64_t      frequency;
public:

	inline Profiler()
	{
		int32_t i = 0;
	}

	inline Profiler(uint32_t registeredOperationNameHash) : registeredOperationNameHash(registeredOperationNameHash)
	{
		std::atomic_signal_fence(std::memory_order_acq_rel);
		//std::atomic_thread_fence(std::memory_order_acq_rel);

		ProfileOperationData* pProfileData = timeStamps.push();

		pProfileData->ticks = QueryPerformance();
		pProfileData->operationNameHash = registeredOperationNameHash;
		pProfileData->isMeasuringStart  = true;
	}

	inline ~Profiler()
	{
		std::atomic_signal_fence(std::memory_order_acq_rel);
		//std::atomic_thread_fence(std::memory_order_acq_rel);

		ProfileOperationData* pProfileData = timeStamps.push();

		pProfileData->ticks = QueryPerformance();
		pProfileData->operationNameHash = this->registeredOperationNameHash;
		pProfileData->isMeasuringStart  = false;
	}

	static uint32_t registerOperationName(const char* operationName);

	inline static uint64_t getClockFrequency()
	{
		return frequency;
	}

	inline static void reset()
	{
		Profiler::timeStamps.clear();
	}

	inline static void clearRegisteredOperationNames()
	{
		Profiler::registeredOperationNames.clear();
	}

	static void getProfileTimes(ProfileOperationInfo& profileOperation);

	friend class InitCpuClockFrequency;
};

}