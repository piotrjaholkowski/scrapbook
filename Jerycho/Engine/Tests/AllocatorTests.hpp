#pragma once

#include "Test.hpp"
#include "../Allocators/StackAllocator.hpp"
#include "../Allocators/CacheLineAllocator.hpp"
#include "../Manager/RecycledMemoryManager.hpp"
#include <vector>

class AllocatorTests : public Test
{
public:
	AllocatorTests(const char* testName) : Test(testName)
	{
		addTestCase("StackAllocator allocation/deallocation", JATEST_UNIT_TEST, stackAllocatorAllocationDeallocationTest);
		addTestCase("RecycledMemoryManager allocation/deallocation", JATEST_UNIT_TEST, recycledMemoryManagerAllocationDeallocationTest);
		addTestCase("PoolAllocator allocation", JATEST_UNIT_TEST, poolAllocatorTest);
		addTestCase("CacheLineAllocator allocation", JATEST_UNIT_TEST, cacheLineAllocatorTest);
		addTestCase("Aligned/Unaligned operations performance", JATEST_PERFORMANCE_TEST, allignedUnalignedPerformance);
		addTestCase("Checking sizeof variables", JATEST_UNIT_TEST, checkingSizeofVariables);
		addTestCase("Allocators aligned to 32", JATEST_UNIT_TEST, allocatorsAlignedTo32);
		
		//addTestCase("Hardware test", hardwareTest);
	}

	void onInitSuite()
	{

	}

	void onDeinitSuite()
	{
		RecycledMemoryManager::cleanUp();
	}

	void onInitTest()
	{

	}
	
	void onDeinitTest()
	{

	}

	static void checkingSizeofVariables()
	{
		JATEST_TRUE(sizeof(int) == 4);
		JATEST_TRUE(sizeof(char) == 1);
		JATEST_TRUE(sizeof(short) == 2);
	}

	static void allocatorsAlignedTo32()
	{
		{
			StackAllocator<int32_t> buffer0;

			int32_t* pBuffer = buffer0.getFirst();

			uintptr_t alignTo = (uintptr_t)pBuffer % 32;

			JATEST_TRUE(alignTo == 0);

			pBuffer = buffer0.pushArray(5000);

			alignTo = (uintptr_t)pBuffer % 32;

			JATEST_TRUE(alignTo == 0);
		}
		
		{
			PoolAllocator<int32_t> poolAllocator = PoolAllocator<int32_t>(1000, 32);

			int32_t* pBuffer = poolAllocator.allocate();
			
			uintptr_t alignTo = (uintptr_t)pBuffer % 32;
			
			JATEST_TRUE(alignTo == 0);
		}

		{
			CacheLineAllocator cacheLineAllocator = CacheLineAllocator(1000, 32);

			void* pBuffer = cacheLineAllocator.allocate(4);

			uintptr_t alignTo = (uintptr_t)pBuffer % 32;

			JATEST_TRUE(alignTo == 0);
		}
		
	}

	static void allignedUnalignedPerformance()
	{
		const int32_t operationNum = 10000;
		char* data = new char[(operationNum + 3) * sizeof(float)];
	
		float* unalignedData = (float*)( (char*)(data) + ((uintptr_t)(data) % 5) );
		float* alignedData   = (float*)( (char*)(data) + ((uintptr_t)(data) % 4) );

		Test::logHtml("p", "description", "", "Test of aligned-unaligned %d airthmetic operations", operationNum);
		logHtml("p", "subdescription", "", "Unaligned address %p", unalignedData);
		logHtml("p", "subdescription", "", "Aligned address %p", alignedData);

		TimeStamp::reset();
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i)
			{
				int32_t z = rand() % operationNum;
				unalignedData[z] = unalignedData[z + 1] * unalignedData[z + 2] + unalignedData[z + 3];
			}
		}
		logHtml("p", "subdescription", "", "Unaligned operations: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i)
			{
				int32_t z = rand() % operationNum;
				alignedData[z] = alignedData[z + 1] * alignedData[z + 2] + alignedData[z + 3];
			}
		}
		logHtml("p", "subdescription", "", "Aligned operations: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		delete[] data;
	}

	static void recycledMemoryManagerAllocationDeallocationTest()
	{
		RecycledMemoryManager* recycledMemoryManager = RecycledMemoryManager::getSingleton();

		JATEST_TRUE(recycledMemoryManager != nullptr);

		char* memoryBlock = static_cast<char*>(recycledMemoryManager->getMemoryBlock(1024));

		for (int32_t i = 0; i < 1024; ++i) {
			memoryBlock[i] = 'x';
		}

		recycledMemoryManager->releaseMemoryBlock(memoryBlock);
		char* memoryBlock2 = static_cast<char*>(recycledMemoryManager->getMemoryBlock(1024));
		JATEST_TRUE(memoryBlock == memoryBlock2);

		for (int i = 0; i < 1024; ++i) {
			JATEST_TRUE(memoryBlock2[i] == 'x');
		}

		memoryBlock = static_cast<char*>(recycledMemoryManager->getMemoryBlock(1024));
		JATEST_TRUE(memoryBlock != memoryBlock2);

		recycledMemoryManager->releaseMemoryBlock(memoryBlock2);
		recycledMemoryManager->releaseMemoryBlock(memoryBlock);

		char* memoryBlock3 = static_cast<char*>(recycledMemoryManager->getMemoryBlock(2048));
		JATEST_TRUE(memoryBlock != memoryBlock3);
		JATEST_TRUE(memoryBlock2 != memoryBlock3);
	}

	static void stackAllocatorAllocationDeallocationTest()
	{
		StackAllocator<int32_t> allocator(8,16);
	
		for (int32_t i = 0; i < 4096; ++i) {
			allocator.push(i);
		}
		
		int* stackElements = allocator.getFirst();
		for (int32_t i = 0; i < 4096; ++i) {
			JATEST_TRUE(stackElements[i] == i);
		}

		for (int32_t i = 4095; i >= 0; --i) {
			JATEST_TRUE(i == allocator.pop());
		}
		
		JATEST_TRUE(allocator.getSize() == 0);
	}

	static void cacheLineAllocatorTest()
	{
		const int32_t blockSize = 1024;
		CacheLineAllocator allocator(blockSize, 16);

		void* pMem0 = allocator.allocate(1);
		void *pMem1 = allocator.allocate(16);

		JATEST_TRUE(pMem0 != nullptr);
		JATEST_TRUE(pMem1 != nullptr);

		allocator.free(pMem0, 1);
		allocator.free(pMem1, 16);

		void* pMem2 = allocator.allocate(1);
		void* pMem3 = allocator.allocate(16);

		JATEST_TRUE(pMem2 == pMem1);
		JATEST_TRUE(pMem3 == pMem0);

		void* pMem4 = allocator.allocate(16);
		void* pMem5 = allocator.allocate(17);

		JATEST_TRUE(pMem4 != nullptr);
		JATEST_TRUE(pMem5 != nullptr);

		allocator.free(pMem2, 1);
		allocator.free(pMem3, 16);
		allocator.free(pMem4, 16);
		allocator.free(pMem5, 17);

		void* zeroAllocation = allocator.allocate(0);
		JATEST_TRUE(zeroAllocation == 0);
		allocator.free(zeroAllocation, 0);

		allocator.clear();

		uint8_t*           data = nullptr;
		const uint32_t poolsNum = (blockSize / 16);

		for (uint32_t i = 0; i < poolsNum; ++i)
		{
			uint8_t* dataTemp = (uint8_t*)allocator.allocate(16);
			
			if (nullptr != data)
			{
				JATEST_TRUE(dataTemp == (data + 16));
			}

			data = dataTemp;
		}

		uint8_t* data2 = (uint8_t*)allocator.allocate(16);
		JATEST_TRUE(data2 != (data + 16));

		const uint32_t largestPoolType = allocator.getLargestPoolAllocationSize();

		uint8_t* largeAllocation = (uint8_t*)allocator.allocate(largestPoolType + 1);
		JATEST_TRUE(largeAllocation != (data2 + 16));

		JATEST_TRUE(0 == ((uintptr_t)largeAllocation % 16));

		uint8_t* largeAllocation2 = (uint8_t*)allocator.allocate(largestPoolType + 1);
		JATEST_TRUE(largeAllocation2 != (data2 + 16));

		JATEST_TRUE(largeAllocation != largeAllocation2);

		JATEST_TRUE(0 == ((uintptr_t)largeAllocation2 % 16));

		allocator.clear();
		allocator.clear();
	}

	static void poolAllocatorTest()
	{
		PoolAllocator<int32_t> poolAllocator = PoolAllocator<int32_t>(10 * sizeof(int32_t), 32);

		int32_t* oldVal = poolAllocator.allocate();

		for (int32_t i = 0; i < 9; ++i)
		{
			int32_t* newVal = poolAllocator.allocate();
			JATEST_TRUE(newVal == oldVal + 1);
			oldVal = newVal;
		}

		int32_t* newVal = poolAllocator.allocate();
		JATEST_TRUE(newVal != oldVal + 1);
	}
	
};
