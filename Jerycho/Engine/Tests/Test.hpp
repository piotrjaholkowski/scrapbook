#pragma once
#include "../jaSetup.hpp"
#include "../Utility/Exception.hpp"

#define JATEST_ASSERT(EXPRESSION) if(##EXPRESSION) throw ja::Exception(ExceptionType::TEST, #EXPRESSION, __FUNCTION__ ,__LINE__, __FILE__) 
#define JATEST_TRUE(EXPRESSION) if(!(##EXPRESSION)) throw ja::Exception(ExceptionType::TEST, #EXPRESSION, __FUNCTION__ ,__LINE__, __FILE__) 

#include <map>
#include <string>
#include <list>
#include <iostream>
#include <fstream>

namespace ja
{

	enum TestType {
		JATEST_UNIT_TEST = 1,
		JATEST_PERFORMANCE_TEST = 2
	};

	enum TestSuiteRun {
		JATEST_RUN_UNIT_TESTS = 1,
		JATEST_RUN_PERFORMANCE_TESTS = 2,
		JATEST_RUN_ALL = 3
	};

	typedef void(*TestCaseFunction)();

	struct ChartColor {
	public:
		uint8_t r, g, b, a;
	};

	class ChartSetColors
	{
	public:
		ChartColor fillColor;
		ChartColor strokeColor;
		ChartColor pointColor;
		ChartColor pointStrokeColor;
	};

	enum ChartType
	{
		JA_LINE_CHART = 0,
		JA_BAR_CHART = 1
	};

	template <typename T>
	class ChartDataSet
	{
	protected:
		uint32_t setsNum;
		uint32_t samplesNum;
		uint32_t labelsLength;
		char*    dataSetNames;
	public:
		ChartSetColors* setsColors;
		ChartType chartType;
		T** dataSets;

		ChartDataSet(uint32_t setsNum, uint32_t samplesNum, uint32_t labelsLength) {
			this->chartType = JA_LINE_CHART;
			this->setsNum = setsNum;
			this->samplesNum = samplesNum;
			this->labelsLength = labelsLength;
			if (labelsLength > 0)
				dataSetNames = new char[samplesNum * labelsLength];
			else
				dataSetNames = nullptr;

			dataSets = new T*[setsNum];
			for (uint32_t i = 0; i < setsNum; ++i)
			{
				dataSets[i] = new T[samplesNum];
			}

			setsColors = new ChartSetColors[setsNum];
		}

		void getLabelsStr(std::ostream& output) {
			if (dataSetNames != nullptr) {
				for (uint32_t i = 0; i < samplesNum; ++i) {
					output << "\"" << &dataSetNames[i * labelsLength] << "\"";
					if (i != (samplesNum - 1)) {
						output << ",";
					}
				}
			}
		}

		void saveColor(std::ostream& output, ChartColor& color) {
			output << "\"rgba(" << (int32_t)(color.r) << "," << (int32_t)(color.g) << "," << (int32_t)(color.b) << "," << (int32_t)(color.a) << ")\"," << std::endl;
		}

		void getDataSetsStr(std::ostream& output) {

			/*fillColor: "rgba(220,220,220,0.5)",
			strokeColor : "rgba(220,220,220,255)",
			pointColor : "rgba(220,220,220,1)",
			pointStrokeColor : "rgba(0,220,0,1)",*/
			for (uint32_t i = 0; i < setsNum; ++i) {
				output << "{" << std::endl;
				output << "fillColor : ";
				saveColor(output, setsColors[i].fillColor);
				output << std::endl;
				output << "strokeColor : ";
				saveColor(output, setsColors[i].strokeColor);
				output << std::endl;
				output << "pointColor : ";
				saveColor(output, setsColors[i].pointColor);
				output << std::endl;
				output << "pointStrokeColor : ";
				saveColor(output, setsColors[i].pointStrokeColor);
				output << std::endl;
				output << "data : [";
				for (uint32_t z = 0; z < samplesNum; ++z) {
					output << dataSets[i][z];
					if (z != (samplesNum - 1))
						output << ",";
				}
				output << "]" << std::endl;
				output << "}" << std::endl;
				if (i != (setsNum - 1))
					output << "," << std::endl;
			}
		}

		~ChartDataSet() {
			if (dataSetNames != nullptr) {
				delete[] dataSetNames;
				dataSetNames = nullptr;
			}
			delete[] setsColors;

			for (uint32_t i = 0; i < setsNum; ++i) {
				delete[] dataSets[i];
			}

			delete[] dataSets;
		}

		ChartSetColors* getSetColors(uint32_t setId) {
			return &setsColors[setId];
		}

		void setDataSampleName(uint32_t dataSetSampleId, const char* sampleName) {
			char* strDst = &dataSetNames[dataSetSampleId * this->labelsLength];
#pragma warning(disable:4996)
			strcpy(strDst, sampleName);
#pragma warning(default:4996)
		}
	};

	class TestCase
	{
	public:
		TestType testType;
		TestCaseFunction testCaseFunction;

		TestCase() {

		}

		TestCase(TestType testType, TestCaseFunction testCaseFunction)
		{
			this->testType = testType;
			this->testCaseFunction = testCaseFunction;
		}
	};

	class Test
	{
	private:
		static std::ofstream logFile;
		static std::map<ja::string, Test*> suiteTests;
		static std::list<ja::string> scheduledSuiteTests;
		char suiteName[JATEST_SUITE_NAME_LENGTH];

		//std::map<ja::string, jaTestCaseFunction> testCase;
		std::map<ja::string, TestCase> testCase;
		bool isScheduled;

		static void clearTests();
	public:

		Test(const char* suiteName)
		{
			isScheduled = false;
#pragma warning(disable:4996)
			strcpy(this->suiteName, suiteName);
#pragma warning(default:4996)
		}

		virtual ~Test()
		{

		}

		static void log(const char* message, ...);
		static void logHtml(const char* tag, const char* idTag, const char* classTag, const char* message, ...);
		static void logChart(const char* chartName, int32_t chartWidth, int32_t chartHeight);
		template <typename T>
		static void logChartData(const char* chartName, ChartDataSet<T>* chartDataSet)
		{
			logFile << "<script type = \"text/javascript\">" << std::endl;
			logFile << "var ctx = document.getElementById(\"" << chartName << "\").getContext(\"2d\");" << std::endl;
			logFile << "var data = {" << std::endl;
			logFile << "labels : [";
			chartDataSet->getLabelsStr(logFile);
			logFile << "]," << std::endl;
			logFile << "datasets : [" << std::endl;
			chartDataSet->getDataSetsStr(logFile);
			logFile << "]" << std::endl;
			logFile << "}" << std::endl;
			switch (chartDataSet->chartType) {
			case JA_LINE_CHART:
				logFile << "var newChart = new Chart(ctx).Line(data);" << std::endl;
				break;
			case JA_BAR_CHART:
				logFile << "var newChart = new Chart(ctx).Bar(data);" << std::endl;
				break;
			}

			logFile << "</script>" << std::endl;

		}
		virtual void onInitSuite() = 0;
		virtual void onDeinitSuite() = 0;
		virtual void onInitTest() = 0;
		virtual void onDeinitTest() = 0;

		static void init(const char* testFile);

		template<class T>
		static void registerSuite(const char* testName)
		{
			T* newTest = new T(testName);
			Test* myTest = newTest;

			suiteTests[testName] = myTest;
		}

		void addTestCase(ja::string caseName, TestType testType, TestCaseFunction caseFunction)
		{
			testCase[caseName] = TestCase(testType, caseFunction);
		}

		static Test* getSingleton();
		static void run(TestSuiteRun testSuiteRun);
		static void cleanUp();
	};

}