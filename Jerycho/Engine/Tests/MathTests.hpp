#pragma once

#include "Test.hpp"
#include "TimeStamp.hpp"
#include <math.h>
#include "../Math/Math.hpp"
#include "../Math/VectorMath2d.hpp"


inline float fastneg(float f)
{int i=((*(int*)&f)^0x80000000);return (*(float*)&i);}

inline int fastsgn(float f)
{return 1+(((*(int*)&f)>>31)<<1);}

using namespace ja;

class MathTests : public Test
{
private:
public:
	MathTests(const char* testName) : Test(testName)
	{
		//addTestCase("First Case", JATEST_UNIT_TEST, firstCase);
		addTestCase("Inverted root performance", JATEST_PERFORMANCE_TEST, invertedRootPerformance);
		addTestCase("Sinus performance", JATEST_PERFORMANCE_TEST, sinusPerformance);
		addTestCase("Compare of mathematic operations speed", JATEST_PERFORMANCE_TEST, compareOfMathematicOperationSpeed);
		addTestCase("Random functions speed", JATEST_PERFORMANCE_TEST, randomSpeed);
		addTestCase("Fast float negation performance", JATEST_PERFORMANCE_TEST, fastFloatNegationPerformance);
		addTestCase("Fast float sign detection", JATEST_PERFORMANCE_TEST, fastFloatSignPerformance);
		addTestCase("Line intersection", JATEST_UNIT_TEST, lineIntersection);
		addTestCase("Next power of 2", JATEST_UNIT_TEST, nextPowerOf2);
		addTestCase("Abs performance", JATEST_PERFORMANCE_TEST, absPerformance);
		addTestCase("SIMD math performace", JATEST_PERFORMANCE_TEST, simdPerformance);
		addTestCase("copyVector2WithTranslation_SSE validity", JATEST_UNIT_TEST, copyVector2WithTranslation_SSE_validity);
		addTestCase("copyVector2WithTranslation_SSE performance", JATEST_PERFORMANCE_TEST, copyVector2WithTranslation_SSE_performance);
	}

	void onInitSuite()
	{

	}

	void onDeinitSuite()
	{

	}

	void onInitTest()
	{

	}

	void onDeinitTest()
	{

	}

	static inline int32_t fastAbs(int32_t value)
	{
		uint32_t temp = value >> 31;     // make a mask of the sign bit
		value ^= temp;                   // toggle the bits if value is negative
		value += temp & 1;               // add one if value was negative
		return value;
	}
	

	static void absPerformance()
	{
		TimeStamp::reset();

		int32_t values[1000];
		for (int32_t i = 0; i < 1000; ++i)
		{
			values[i] = rand();
		}

		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < 1000; ++i)
			{
				values[i] = abs(values[i]);
			}
		}

		logHtml("p", "subdescription", "", "Abs %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < 1000; ++i)
			{
				values[i] = fastAbs(values[i]);
			}
		}

		logHtml("p", "subdescription", "", "Fastabs %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());	
	}

	static void simdPerformance()
	{
		TimeStamp::reset();

		const int32_t operationsNum = 1000;
		jaMatrix44    identityMatrix;

		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationsNum; ++i)
				jaVectormath2::setIdentityMatrix44(identityMatrix);
		}
		logHtml("p", "subdescription", "", "setIdentityMatrix44 %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationsNum; ++i)
				jaVectormath2::setIdentityMatrix44_SSE(identityMatrix);
		}
		logHtml("p", "subdescription", "", "setIdentityMatrix44_SSE %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationsNum; ++i)
				jaVectormath2::setIdentityMatrix44_AVX(identityMatrix);
		}
		logHtml("p", "subdescription", "", "setIdentityMatrix44_AVX %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationsNum; ++i)
				jaVectormath2::setTranslateMatrix44(jaVector2(1.0f,1.0f),identityMatrix);
		}
		logHtml("p", "subdescription", "", "setTranslateMatrix44 %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		/*{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationsNum; ++i)
				jaVectormath2::setTranslateMatrix44_SSE(jaVector2(1.0f, 1.0f), identityMatrix);
		}
		logHtml("p", "subdescription", "", "setTranslateMatrix44_SSE %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());*/
	}

	static void nextPowerOf2()
	{
		int32_t powerOf2 = 1;
		for (int32_t i = 1; i < 16; ++i)
		{
			powerOf2 *= 2;
			uint32_t powerOf2Mod = powerOf2 + 1;
			uint32_t nextPower    = math::getNextPowerOf2(powerOf2);
			uint32_t nextPowerMod = math::getNextPowerOf2(powerOf2Mod);

			JATEST_TRUE(nextPower == powerOf2);
			JATEST_TRUE((2 * nextPower) == nextPowerMod);
		}
	}

	static void fastFloatSignPerformance()
	{
		float numbers[500];

		for (int32_t i = 0; i < 500; ++i)
		{
			numbers[i] = (float)i;
		}

		TimeStamp::reset();
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < 500; ++i)
			{
				if (fastsgn(numbers[i]) < 0)
					numbers[i] = numbers[i];
			}
		}
		logHtml("p", "subdescription", "", "Fastsgn float negation detection %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < 500; ++i)
			{
				if (numbers[i] < 0.0f)
					numbers[i] = numbers[i];
			}
		}
		logHtml("p", "subdescription", "", "Normal float negation detection %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
	}

	static void fastFloatNegationPerformance()
	{
		float numbers[500];

		for (int32_t i = 0; i < 500; ++i)
		{
			numbers[i] = (float)i;
		}

		TimeStamp::reset();
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < 500; ++i)
			{
				numbers[i] = fastneg(numbers[i]);
			}
		}
		logHtml("p", "subdescription", "", "Fastneg float negation %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < 500; ++i)
			{
				numbers[i] = -numbers[i];
			}
		}
		logHtml("p", "subdescription", "", "Normal float negation %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
	}

	static void randomSpeed()
	{
		const int32_t iterationCount = 100000;
		int32_t result = 0;
		Uint64 ticksRandom;
		Uint64 ticksMathHelper;
		TimeStamp::reset();

		Test::logHtml("p", "description", "", "Generation of 100000 random integer values");
		{
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<iterationCount; ++i)
			{
				result += rand();
			}
		}

		ticksRandom = TimeStamp::getTicks();
		Test::logHtml("p", "subdescription", "", "STL random %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		
		{
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<iterationCount; ++i)
			{
				result += math::rand();
			}
		}

		ticksMathHelper = TimeStamp::getTicks();
		Test::logHtml("p", "subdescription", "", "gilMathelper::rand() %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		
		ChartDataSet<Uint64> chartDataSet(1, 2, 25);
		chartDataSet.dataSets[0][0] = ticksRandom;
		chartDataSet.dataSets[0][1] = ticksMathHelper;
		chartDataSet.chartType = JA_BAR_CHART;
		chartDataSet.setsColors[0].fillColor.r = 255;
		chartDataSet.setsColors[0].fillColor.g = 0;
		chartDataSet.setsColors[0].fillColor.b = 0;
		chartDataSet.setsColors[0].fillColor.a = 255;
		chartDataSet.setDataSampleName(0, "STL Random");
		chartDataSet.setDataSampleName(1, "gilMathelper::rand()");
		Test::logChart("randomSpeed", 300, 400);
		Test::logChartData("randomSpeed", &chartDataSet);
	}

	static void sinusPerformance()
	{
		float sinValue = 0.0f;
		const int32_t iterationCount = 100000;

		Test::logHtml("p", "description", "", "Test of sinus speed calculating %d sinus values",iterationCount);

		TimeStamp::reset();
		sinValue = 0.0f;
		{
			JATEST_TIMESTAMP(JA_DEFAULT);
			for(int32_t i=0; i< iterationCount; ++i)
			{
				sinValue += (float)(GILSIN(i * (float)(ja::math::PI) / iterationCount)); 
			}
		}

		Test::logHtml("p", "subdescription", "", "GILSIN %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		Test::logHtml("p", "subdescription", "", "GILSIN value: %f2.4\n", sinValue);

		sinValue = 0.0f;
		{
			JATEST_TIMESTAMP(JA_DEFAULT);
			for(int32_t i=0; i< iterationCount; ++i)
			{
				sinValue += sinf(i * (float)(ja::math::PI) / iterationCount);
			}
		}

		Test::logHtml("p", "subdescription", "", "sinf %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		Test::logHtml("p", "subdescription", "", "sinf value: %f2.4\n", sinValue);

		sinValue = 0.0f;
		{
			JATEST_TIMESTAMP(JA_DEFAULT);
			for(int32_t i=0; i< iterationCount; ++i)
			{
				sinValue += math::fastSin(i * (float)(math::PI) / iterationCount); 
			}
		}

		Test::logHtml("p", "subdescription", "", "gilMathHelper::fastSin %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		Test::logHtml("p", "subdescription", "", "gilMathHelper::fastSin value: %f2.4\n", sinValue);
	}	

	static void invertedRootPerformance()
	{
		const int32_t itOperations = 100000;
		Test::logHtml("p", "description", "", "Test of inverted root performance %d operations", itOperations);
		
		TimeStamp::reset();
		float invRoot = 0.0f;
		{
			JATEST_TIMESTAMP(JA_DEFAULT);
			
			for(int32_t i=0; i<itOperations; ++i)
			{
				invRoot += 1.0f / sqrt( (float)(i) );
			}
		}
		logHtml("p", "subdescription", "", "Normal inverted root time: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		
		
		TimeStamp::reset();
		invRoot = 0.0f;
		{
			JATEST_TIMESTAMP(JA_DEFAULT);

			for(int32_t i=0; i<itOperations; ++i)
			{
				invRoot += math::invSqrt( (float)(i) );
			}
		}
		logHtml("p", "subdescription", "", "Carmack inverted root time: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
	}

	static void firstCase()
	{
		int32_t b = 2 * 2;

		for(int32_t i=0; i<1000; ++i)
		{
			b = b * b + i;
		}

		JATEST_ASSERT(b != 5);
	}

	static void compareOfMathematicOperationSpeed()
	{
		const int32_t operationNum =10000;

		TimeStamp::reset();
		logHtml("p", "description", "", "Test of %d basic arithmetic operations performance", operationNum);
		//multiply float	
		{
			float result = 10000.0f;
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<operationNum; ++i)
			{
				result = 3.0f * i;
			}
		}
		logHtml("p", "subdescription", "", "Float multiply: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		//multiply integer
		{
			int32_t result = 100000;
			JATEST_TIMESTAMP(0);
			for(int i=0; i<operationNum; ++i)
			{
				result = i * 3;
			}
		}
		logHtml("p", "subdescription", "", "Integer multiply: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		//division float
		{
			float result = 10000.0f;
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<operationNum; ++i)
			{
				result = i / 3.0f;
			}
		}
		logHtml("p", "subdescription", "", "Float division: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		//division integer
		{
			int32_t result = 100000;
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<operationNum; ++i)
			{
				result = i / 3;
			}
		}
		logHtml("p", "subdescription", "", "Integer division: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		//add float
		{
			float result = 0.0f;
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<operationNum; ++i)
			{
				result = i + 2.5f;
			}
		}
		logHtml("p", "subdescription", "", "Float add: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		//add integer
		{
			int32_t result = 100000;
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<operationNum; ++i)
			{
				result = i + 3;
			}
		}
		logHtml("p", "subdescription", "", "Integer add: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		//sub float
		{
			float result = 0.0f;
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<operationNum; ++i)
			{
				result = i - 2.5f;
			}
		}
		logHtml("p", "subdescription", "", "Float sub: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		//sub integer
		{
			int32_t result = 100000;
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<operationNum; ++i)
			{
				result = i - 3;
			}
		}
		logHtml("p", "subdescription", "", "Integer sub: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		//less equal float
		{
			float result = 0.0f;
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<operationNum; ++i)
			{
				if(result <= 0.5f)
					result = 1.0f;
				else
					result = 0.0f;
			}
		}
		logHtml("p", "subdescription", "", "Float less equal compare: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		//less equal integer
		{
			int32_t result = 0;
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<operationNum; ++i)
			{
				if(result <= 5)
					result = 10;
				else
					result = 0;
			}
		}
		logHtml("p", "subdescription", "", "Integer less equal compare: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		
		//random
		{
			int32_t result = 0;
			JATEST_TIMESTAMP(0);
			for(int32_t i=0; i<operationNum; ++i)
			{
				result = rand();
			}
		}
		logHtml("p", "subdescription", "", "Random: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		
		//abs float
		//abs integer
		//root float
		//integer precision
		//float presision
	}

	static void lineIntersection()
	{
		{
			jaVector2 pointA0(-5.0f, 2.0f);
			jaVector2 pointA1( 5.0f, 2.0f);
			jaVector2 pointB0( 0.0f, 5.0f);
			jaVector2 pointB1( 0.0f,-5.0f);
			jaVector2 intersectionPoint(0.0f, 0.0f);

			JATEST_TRUE(jaVectormath2::isLineIntersecting(pointA0, pointA1, pointB0, pointB1, intersectionPoint) == true);
			JATEST_TRUE(intersectionPoint.x == 0.0f);
			JATEST_TRUE(intersectionPoint.y == 2.0f);
		}

		{
			jaVector2 pointA0(-5.0f , 0.0f);
			jaVector2 pointA1( 5.0f , 0.0f);
			jaVector2 pointB0( 0.0f , 5.0f);
			jaVector2 pointB1( 0.0f , 0.0f);
			jaVector2 intersectionPoint(0.0f, 0.0f);

			JATEST_TRUE(jaVectormath2::isLineIntersecting(pointA0, pointA1, pointB0, pointB1, intersectionPoint) == false);
		}

		{
			jaVector2 pointA0(-5.0f, 0.0f);
			jaVector2 pointA1(5.0f, 0.0f);
			jaVector2 pointB0(0.0f, 5.0f);
			jaVector2 pointB1(0.0f, 2.0f);
			jaVector2 intersectionPoint(0.0f, 0.0f);

			JATEST_TRUE(jaVectormath2::isLineIntersecting(pointA0, pointA1, pointB0, pointB1, intersectionPoint) == false);
		}

		{
			jaVector2 pointA0(-5.0f, 0.0f);
			jaVector2 pointA1( 5.0f , 0.0f);
			jaVector2 pointB0( 5.0f , 0.0f);
			jaVector2 pointB1( 10.0f, 0.0f);
			jaVector2 intersectionPoint(0.0f, 0.0f);

			JATEST_TRUE(jaVectormath2::isLineIntersecting(pointA0, pointA1, pointB0, pointB1, intersectionPoint) == false);
		}
	}

	static void copyVector2WithTranslation_SSE_validity()
	{
		uint32_t maxArraySize = 100;

		StackAllocator<jaVector2> verticesSrc0;
		StackAllocator<jaVector2> verticesSrc1;
		StackAllocator<jaVector2> verticesSrc2;

		StackAllocator<jaVector2> verticesDst0;
		StackAllocator<jaVector2> verticesDst1;
		StackAllocator<jaVector2> verticesDst2;

		jaVector2 translation(2.5f, 2.5f);

		jaVector2* pVerticesSrc0 = verticesSrc0.pushArray(maxArraySize);
		jaVector2* pVerticesSrc1 = verticesSrc1.pushArray(maxArraySize);
		jaVector2* pVerticesSrc2 = verticesSrc2.pushArray(maxArraySize);

		jaVector2* pVerticesDst0 = verticesDst0.pushArray(maxArraySize);
		jaVector2* pVerticesDst1 = verticesDst1.pushArray(maxArraySize);
		jaVector2* pVerticesDst2 = verticesDst2.pushArray(maxArraySize);

		for (uint32_t i = 0; i < maxArraySize; ++i)
		{
			pVerticesSrc0[i].setZero();
			pVerticesSrc1[i].setZero();
			pVerticesSrc2[i].setZero();

			pVerticesDst0[i].setZero();
			pVerticesDst1[i].setZero();
			pVerticesDst2[i].setZero();
		}
	
		jaVectormath2::copyVector2WithTranslation_SSE(pVerticesDst0, pVerticesSrc0, translation, 1);
		jaVectormath2::copyVector2WithTranslation_SSE(pVerticesDst1, pVerticesSrc1, translation, 4);
		jaVectormath2::copyVector2WithTranslation_SSE(pVerticesDst2, pVerticesSrc2, translation, 6);

		for (uint32_t i = 0; i < 1; ++i)
		{
			JATEST_TRUE(pVerticesDst0[i].x == translation.x);
			JATEST_TRUE(pVerticesDst0[i].y == translation.y);
		}

		for (uint32_t i = 1; i < maxArraySize; ++i)
		{
			JATEST_TRUE(pVerticesDst0[i].x == 0.0f);
			JATEST_TRUE(pVerticesDst0[i].y == 0.0f);
		}

		for (uint32_t i = 0; i < 4; ++i)
		{
			JATEST_TRUE(pVerticesDst1[i].x == translation.x);
			JATEST_TRUE(pVerticesDst1[i].y == translation.y);
		}

		for (uint32_t i = 4; i < maxArraySize; ++i)
		{
			JATEST_TRUE(pVerticesDst1[i].x == 0.0f);
			JATEST_TRUE(pVerticesDst1[i].y == 0.0f);
		}

		for (uint32_t i = 0; i < 6; ++i)
		{
			JATEST_TRUE(pVerticesDst2[i].x == translation.x);
			JATEST_TRUE(pVerticesDst2[i].y == translation.y);
		}

		for (uint32_t i = 6; i < maxArraySize; ++i)
		{
			JATEST_TRUE(pVerticesDst2[i].x == 0.0f);
			JATEST_TRUE(pVerticesDst2[i].y == 0.0f);
		}
	}

	static void copyVector2WithTranslation_SSE_performance()
	{
		uint32_t maxArraySize = 100;

		StackAllocator<jaVector2> verticesSrc0;
		StackAllocator<jaVector2> verticesSrc1;

		StackAllocator<jaVector2> verticesDst0;
		StackAllocator<jaVector2> verticesDst1;

		jaVector2 translation(2.5f, 2.5f);

		jaVector2* pVerticesSrc0 = verticesSrc0.pushArray(maxArraySize);
		jaVector2* pVerticesSrc1 = verticesSrc1.pushArray(maxArraySize);

		jaVector2* pVerticesDst0 = verticesDst0.pushArray(maxArraySize);
		jaVector2* pVerticesDst1 = verticesDst1.pushArray(maxArraySize);

		for (uint32_t i = 0; i < maxArraySize; ++i)
		{
			pVerticesSrc0[i].setZero();
			pVerticesSrc1[i].setZero();

			pVerticesDst0[i].setZero();
			pVerticesDst1[i].setZero();
		}

		TimeStamp::reset();
		//logHtml("p", "description", "", "Test of %d basic arithmetic operations performance", operationNum);
		//multiply float	
		{
			JATEST_TIMESTAMP(0);
			jaVectormath2::copyVector2WithTranslation(pVerticesDst0, pVerticesSrc0, translation, 100);
		}
		logHtml("p", "subdescription", "", "copyVector2WithTranslation: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		{
			JATEST_TIMESTAMP(0);
			jaVectormath2::copyVector2WithTranslation_SSE(pVerticesDst0, pVerticesSrc0, translation, 100);
		}
		logHtml("p", "subdescription", "", "copyVector2WithTranslation_SSE: %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
	}

};