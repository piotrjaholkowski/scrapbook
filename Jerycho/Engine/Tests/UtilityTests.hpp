#pragma once

#include "Test.hpp"
#include "TimeStamp.hpp"

#include "../Utility/String.hpp"
#include "../Utility/NameTag.hpp"


class UtilityTests : public Test
{
private:
public:
	UtilityTests(const char* testName) : Test(testName)
	{
		addTestCase("memset_SSE2_aligned16 validity", JATEST_UNIT_TEST, memset_SSE2_aligned16_validity);
		addTestCase("memset_SSE2 validity", JATEST_UNIT_TEST, memset_SSE2_validity);
		addTestCase("memset_AVX_aligned32 validity", JATEST_UNIT_TEST, memset_AVX_aligned32_validity);
		addTestCase("memset_int32 performance", JATEST_PERFORMANCE_TEST, memset_int32_performance);
		addTestCase("memcpyUint16ArrayWithOffset_SSE2 validity", JATEST_UNIT_TEST, memcpyUint16ArrayWithOffset_SSE2_validity);
		addTestCase("NameTag validity", JATEST_UNIT_TEST, nameTag_validity);
	}

	void onInitSuite()
	{

	}

	void onDeinitSuite()
	{

	}

	void onInitTest()
	{

	}

	void onDeinitTest()
	{

	}

	static void  memcpyUint16ArrayWithOffset_SSE2_validity()
	{
		const uint16_t tableSize = 30;

		__declspec(align(16)) uint16_t srcTab[tableSize];

		__declspec(align(16)) uint16_t dstTab[tableSize];
		__declspec(align(16)) uint16_t dstTab2[tableSize];
		__declspec(align(16)) uint16_t dstTab3[tableSize];

		memset(dstTab,  0, sizeof(dstTab));
		memset(dstTab2, 0, sizeof(dstTab2));
		memset(dstTab3, 0, sizeof(dstTab3));

		for (uint16_t i = 0; i < tableSize; ++i)
		{
			srcTab[i]  = i;
		}

		const uint16_t offset = 5;

		memcpyUint16ArrayWithOffset_SSE2(dstTab,  srcTab, 5, offset);
		memcpyUint16ArrayWithOffset_SSE2(dstTab2, srcTab, 8, offset);
		memcpyUint16ArrayWithOffset_SSE2(dstTab3, srcTab, 9, offset);
		
		for (uint16_t i = 0; i < 5; ++i)
		{
			JATEST_TRUE(dstTab[i] == (i + offset));
		}

		for (uint16_t i = 0; i < 8; ++i)
		{
			JATEST_TRUE(dstTab2[i] == (i + offset));
		}

		for (uint16_t i = 0; i < 9; ++i)
		{
			JATEST_TRUE(dstTab3[i] == (i + offset));
		}

		for (int16_t i = 5; i < tableSize; ++i)
		{
			JATEST_TRUE(dstTab[i] == 0);
		}

		for (int16_t i = 8; i < tableSize; ++i)
		{
			JATEST_TRUE(dstTab2[i] == 0);
		}

		for (int16_t i = 9; i < tableSize; ++i)
		{
			JATEST_TRUE(dstTab3[i] == 0);
		}
	}

	static void memset_SSE2_aligned16_validity()
	{
		const int32_t tableSize = 30;

		__declspec(align(16)) int32_t tab[tableSize];
		__declspec(align(16)) int32_t tab2[tableSize];
		__declspec(align(16)) int32_t tab3[tableSize];

		memset(tab, 0, sizeof(tab));
		memset(tab2, 0, sizeof(tab2));
		memset(tab3, 0, sizeof(tab3));

		memsetInt32_SSE2_aligned16(tab,  0xffff, 5);
		memsetInt32_SSE2_aligned16(tab2, 0xffff, 8);
		memsetInt32_SSE2_aligned16(tab3, 0xffff, 9);

		for (int32_t i = 0; i < 5; ++i)
		{
			JATEST_TRUE(tab[i] == 0xffff);
		}

		for (int32_t i = 0; i < 8; ++i)
		{
			JATEST_TRUE(tab2[i] == 0xffff);
		}

		for (int32_t i = 0; i < 9; ++i)
		{
			JATEST_TRUE(tab3[i] == 0xffff);
		}
		
		for (int32_t i = 5; i < tableSize; ++i)
		{
			JATEST_TRUE(tab[i] == 0);
		}

		for (int32_t i = 8; i < tableSize; ++i)
		{
			JATEST_TRUE(tab2[i] == 0);
		}

		for (int32_t i = 9; i < tableSize; ++i)
		{
			JATEST_TRUE(tab3[i] == 0);
		}
	}

	static void memset_SSE2_validity()
	{
		const int32_t tableSize     = 120;
		const int32_t iterationsNum = 100;

		int32_t tab[tableSize ];
		int32_t tab2[tableSize];
		int32_t tab3[tableSize];

		memset(tab, 0, sizeof(tab));
		memset(tab2, 0, sizeof(tab2));
		memset(tab3, 0, sizeof(tab3));

		memsetInt32_SSE2(tab, 0xffff, 5);
		memsetInt32_SSE2(tab2, 0xffff, 8);
		memsetInt32_SSE2(tab3, 0xffff, 9);

		for (int32_t i = 0; i < 5; ++i)
		{
			JATEST_TRUE(tab[i] == 0xffff);
		}

		for (int32_t i = 0; i < 8; ++i)
		{
			JATEST_TRUE(tab2[i] == 0xffff);
		}

		for (int32_t i = 0; i < 9; ++i)
		{
			JATEST_TRUE(tab3[i] == 0xffff);
		}

		for (int32_t i = 5; i < iterationsNum; ++i)
		{
			JATEST_TRUE(tab[i] == 0);
		}

		for (int32_t i = 8; i < iterationsNum; ++i)
		{
			JATEST_TRUE(tab2[i] == 0);
		}

		for (int32_t i = 9; i < iterationsNum; ++i)
		{
			JATEST_TRUE(tab3[i] == 0);
		}

		memset(tab,  0, sizeof(tab));
		memset(tab2, 0, sizeof(tab2));
		memset(tab3, 0, sizeof(tab3));

		memsetInt32_SSE2(tab , 0xffff, 25);
		memsetInt32_SSE2(tab2, 0xffff, 25);
		memsetInt32_SSE2(tab3, 0xffff, 25);

		memsetInt32_SSE2(&tab[50],  0xffff, 25);
		memsetInt32_SSE2(&tab2[50], 0xffff, 25);
		memsetInt32_SSE2(&tab3[50], 0xffff, 25);

		for (uint32_t i = 0; i < 25; ++i)
		{
			JATEST_TRUE(tab[i]  == 0xffff);
			JATEST_TRUE(tab2[i] == 0xffff);
			JATEST_TRUE(tab3[i] == 0xffff);
		}

		for (uint32_t i = 25; i < 50; ++i)
		{
			JATEST_TRUE(tab[i]  == 0);
			JATEST_TRUE(tab2[i] == 0);
			JATEST_TRUE(tab3[i] == 0);
		}

		for (uint32_t i = 50; i < 75; ++i)
		{
			JATEST_TRUE(tab[i]  == 0xffff);
			JATEST_TRUE(tab2[i] == 0xffff);
			JATEST_TRUE(tab3[i] == 0xffff);
		}

		for (uint32_t i = 75; i < iterationsNum; ++i)
		{
			JATEST_TRUE(tab[i]  == 0);
			JATEST_TRUE(tab2[i] == 0);
			JATEST_TRUE(tab3[i] == 0);
		}
	}

	static void memset_AVX_aligned32_validity()
	{
		const int32_t tableSize = 30;

		__declspec(align(32)) int32_t tab[tableSize];
		__declspec(align(32)) int32_t tab2[tableSize];
		__declspec(align(32)) int32_t tab3[tableSize];

		memset(tab, 0, sizeof(tab));
		memset(tab2, 0, sizeof(tab2));
		memset(tab3, 0, sizeof(tab3));

		memsetInt32_AVX_aligned32(tab,  0xffff, 5);
		memsetInt32_AVX_aligned32(tab2, 0xffff, 8);
		memsetInt32_AVX_aligned32(tab3, 0xffff, 9);

		for (int32_t i = 0; i < 5; ++i)
		{
			JATEST_TRUE(tab[i] == 0xffff);
		}

		for (int32_t i = 0; i < 8; ++i)
		{
			JATEST_TRUE(tab2[i] == 0xffff);
		}

		for (int32_t i = 0; i < 9; ++i)
		{
			JATEST_TRUE(tab3[i] == 0xffff);
		}

		for (int32_t i = 5; i < tableSize; ++i)
		{
			JATEST_TRUE(tab[i] == 0);
		}

		for (int32_t i = 8; i < tableSize; ++i)
		{
			JATEST_TRUE(tab2[i] == 0);
		}

		for (int32_t i = 9; i < tableSize; ++i)
		{
			JATEST_TRUE(tab3[i] == 0);
		}
	}

	static void memset_int32_performance()
	{
		StackAllocator<int32_t> buffer0;
		StackAllocator<int32_t> buffer1;
		StackAllocator<int32_t> buffer2;
		StackAllocator<int32_t> buffer3;
		StackAllocator<int32_t> buffer4;

		const int32_t intSmallElementsNum = 8;
		const int32_t intBigElementsNum   = 220;

		int32_t* pBuffer0 = buffer0.pushArray(intSmallElementsNum);
		int32_t* pBuffer1 = buffer1.pushArray(intSmallElementsNum);
		int32_t* pBuffer2 = buffer2.pushArray(intSmallElementsNum);
		int32_t* pBuffer3 = buffer3.pushArray(intSmallElementsNum);
		int32_t* pBuffer4 = buffer4.pushArray(intSmallElementsNum);

		uint64_t ticks0 = 0;
		uint64_t ticks1 = 0;
		uint64_t ticks2 = 0;
		uint64_t ticks3 = 0;
		uint64_t ticks4 = 0;

		ja::TimeStamp::reset();
			
		{
			JATEST_TIMESTAMP(0);
			memsetInt32_SSE2_aligned16(pBuffer0, 0xfafa, intSmallElementsNum);
		}

		ticks0 = ja::TimeStamp::getTicks();


		{
			JATEST_TIMESTAMP(0);
			memsetInt32_AVX_aligned32(pBuffer1, 0xfafa, intSmallElementsNum);
		}

		ticks1 = ja::TimeStamp::getTicks();

		{ 
			JATEST_TIMESTAMP(0);
			std::fill_n(pBuffer2, intSmallElementsNum, 0xfafa);
		}

		ticks2 = ja::TimeStamp::getTicks();

		{
			JATEST_TIMESTAMP(0);
			memsetInt32_SSE2(pBuffer3, 0xfafa, intSmallElementsNum);
		}

		ticks3 = ja::TimeStamp::getTicks();

		{
			JATEST_TIMESTAMP(0);
			memsetInt32(pBuffer4, 0xfafa, intSmallElementsNum);
		}

		ticks4 = ja::TimeStamp::getTicks();
		
		logHtml("p", "subdescription", "", "array %d memsetInt32_SSE2_aligned16 %llu ticks\n", intSmallElementsNum, ticks0);
		logHtml("p", "subdescription", "", "array %d memsetInt32_AVX_aligned32 %llu ticks\n", intSmallElementsNum, ticks1);
		logHtml("p", "subdescription", "", "array %d std::fill_n %llu ticks\n", intSmallElementsNum, ticks2);
		logHtml("p", "subdescription", "", "array %d memsetInt32_SSE2 %llu ticks\n", intSmallElementsNum, ticks3);
		logHtml("p", "subdescription", "", "array %d memsetInt32 %llu ticks\n", intSmallElementsNum, ticks4);

		TimeStamp::reset();

		StackAllocator<int32_t> buffer00;
		StackAllocator<int32_t> buffer10;
		StackAllocator<int32_t> buffer20;
		StackAllocator<int32_t> buffer30;
		StackAllocator<int32_t> buffer40;

		pBuffer0 = buffer00.pushArray(intBigElementsNum);
		pBuffer1 = buffer10.pushArray(intBigElementsNum);
		pBuffer2 = buffer20.pushArray(intBigElementsNum);
		pBuffer3 = buffer30.pushArray(intBigElementsNum);
		pBuffer4 = buffer40.pushArray(intBigElementsNum);

		{
			JATEST_TIMESTAMP(0);
			memsetInt32_SSE2_aligned16(pBuffer0, 0xfafa, intBigElementsNum);
		}

		ticks0 = ja::TimeStamp::getTicks();


		{
			JATEST_TIMESTAMP(0);
			memsetInt32_AVX_aligned32(pBuffer1, 0xfafa, intBigElementsNum);
		}

		ticks1 = ja::TimeStamp::getTicks();

		{
			JATEST_TIMESTAMP(0);
			std::fill_n(pBuffer2, intBigElementsNum, 0xfafa);
		}

		ticks2 = ja::TimeStamp::getTicks();

		{
			JATEST_TIMESTAMP(0);
			memsetInt32_SSE2(pBuffer3, 0xfafa, intBigElementsNum);
		}

		ticks3 = ja::TimeStamp::getTicks();

		{
			JATEST_TIMESTAMP(0);
			memsetInt32(pBuffer4, 0xfafa, intBigElementsNum);
		}

		ticks4 = ja::TimeStamp::getTicks();

		logHtml("p", "subdescription", "", "array %d memsetInt32_SSE2_aligned16 %llu ticks\n", intBigElementsNum, ticks0);
		logHtml("p", "subdescription", "", "array %d memsetInt32_AVX_aligned32 %llu ticks\n", intBigElementsNum, ticks1);
		logHtml("p", "subdescription", "", "array %d std::fill_n %llu ticks\n", intBigElementsNum, ticks2);
		logHtml("p", "subdescription", "", "array %d memsetInt32_SSE2 %llu ticks\n", intBigElementsNum, ticks3);
		logHtml("p", "subdescription", "", "array %d memsetInt32 %llu ticks\n", intBigElementsNum, ticks4);
	}

	static void nameTag_validity()
	{
		NameTag<10> nameTag;
		NameTag<10> nameTag2;
		NameTag<10> nameTag3;

		nameTag.setName("test");
		nameTag2.setName("text");
		nameTag3.setName("test");
		
		JATEST_TRUE(nameTag.getHash() != nameTag2.getHash());
		JATEST_TRUE(nameTag.getHash() == nameTag3.getHash());

		JATEST_TRUE(0 == strcmp(nameTag.getName(), "test"));

		nameTag.setName("0123456789");
		JATEST_TRUE(0 == strcmp(nameTag.getName(), "012345678"));

		nameTag2.setName("012345678");
		JATEST_TRUE(nameTag.getHash() == nameTag2.getHash());

		nameTag.setName("0123456789ABCDEFG");
		nameTag2.setName("012345678ERYTGSZ");
		JATEST_TRUE(nameTag.getHash() == nameTag2.getHash());
		JATEST_TRUE(0 == strcmp(nameTag.getName(), "012345678"));
		JATEST_TRUE(0 == strcmp(nameTag2.getName(), "012345678"));
	}

};