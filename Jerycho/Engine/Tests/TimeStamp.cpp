#pragma once

#include "TimeStamp.hpp"
#include "../Allocators/CacheLineAllocator.hpp"

#ifdef  JA_HIGH_RESOLUTION_TIME
#include <Windows.h>
#endif

namespace ja
{
	TimeStampData         TimeStamp::timeStamps[setup::profiler::timeStampNum];
	uint64_t              TimeStamp::frequency = 1;
	uint32_t              TimeStamp::currentTimeStamp = 0;

	uint64_t                             Profiler::frequency = 1;
	StackAllocator<ProfileOperationData> Profiler::timeStamps;
	HashMap<ProfileOperationTag>         Profiler::registeredOperationNames;
	CacheLineAllocator*                  Profiler::operationNamesAllocator = new CacheLineAllocator(sizeof(ProfileOperationTag) * 64, sizeof(ProfileOperationTag));

	InitCpuClockFrequency initCpuClockFrequency;

	InitCpuClockFrequency::InitCpuClockFrequency()
	{
#ifdef  JA_HIGH_RESOLUTION_TIME
		LARGE_INTEGER clockFrequency;
		BOOL result = QueryPerformanceFrequency(&clockFrequency);

		if (0 != result)
		{
			TimeStamp::frequency = clockFrequency.QuadPart;
			Profiler::frequency  = clockFrequency.QuadPart;
		}
#endif		
	}

	uint32_t Profiler::registerOperationName(const char* operationName)
	{
		ProfileOperationTag* pProfileOperationTag = registeredOperationNames.find(operationName);

		if (nullptr != pProfileOperationTag)
			return pProfileOperationTag->getHash();

		ProfileOperationTag* pNewOperation = new (operationNamesAllocator->allocate(sizeof(ProfileOperationTag))) ProfileOperationTag();
		pNewOperation->setName(operationName);

		registeredOperationNames.add(pNewOperation->getHash(), pNewOperation);

		return pNewOperation->getHash();
	}

	void Profiler::getProfileTimes(ProfileOperationInfo& profileOperation)
	{
		ProfileOperationData* pProfileOperation = timeStamps.getFirst();
		const       uint32_t  timeStampsNum     = timeStamps.getSize();

		if (0 == timeStampsNum)
			return;

		CacheLineAllocator* pCacheLineAllocator = profileOperation.childOperations.getMemoryAllocator();

		profileOperation.startTime      = pProfileOperation[0].ticks;
		profileOperation.endTime        = pProfileOperation[timeStampsNum - 1].ticks;
		profileOperation.deltaTime      = (double)(profileOperation.endTime - profileOperation.startTime) / (double)Profiler::frequency;
		profileOperation.operationStart = 0;
		profileOperation.operationEnd   = timeStampsNum - 1;
		profileOperation.parent         = nullptr;
		profileOperation.operationTag.setName("PROFILE");	
		
		uint32_t maxTasksNum = 0;
		if (profileOperation.operationEnd > profileOperation.operationStart)
			maxTasksNum = (profileOperation.operationEnd - profileOperation.operationStart) / 2;
		
		profileOperation.childOperations.resizeCapacity(maxTasksNum);

		StackAllocator<ProfileOperationInfo*> profilesStack;
		profilesStack.push(&profileOperation);

		while (0 != profilesStack.getSize())
		{
			ProfileOperationInfo* pParentOperation = profilesStack.pop();

			for (uint32_t queryStart = pParentOperation->operationStart; queryStart <= pParentOperation->operationEnd; ++queryStart)
			{
				const uint32_t searchForTagHash = pProfileOperation[queryStart].operationNameHash;
				int32_t        openTagsNum      = 1;

				uint32_t queryEnd;

				for (queryEnd = queryStart + 1; queryEnd <= pParentOperation->operationEnd; ++queryEnd)
				{
					if (pProfileOperation[queryEnd].operationNameHash == searchForTagHash)
					{
						int32_t operationVal = (pProfileOperation[queryEnd].isMeasuringStart) ? 1 : -1;
						openTagsNum += operationVal;

						if (0 == openTagsNum)
							break;
					}
				}

				if (0 != openTagsNum)
					break;

				ProfileOperationInfo* pChildOperation = new (pParentOperation->childOperations.push()) ProfileOperationInfo(pCacheLineAllocator);

				pChildOperation->startTime      = pProfileOperation[queryStart].ticks;
				pChildOperation->endTime        = pProfileOperation[queryEnd].ticks;
				pChildOperation->deltaTime      = (double)(pChildOperation->endTime - pChildOperation->startTime) / (double)frequency;
				pChildOperation->operationStart = queryStart + 1;
				pChildOperation->operationEnd   = queryEnd   - 1;
				pChildOperation->parent         = pParentOperation;
				pChildOperation->operationTag   = *registeredOperationNames.find(searchForTagHash);

				uint32_t maxChildOperationTasksNum = 0;
					maxChildOperationTasksNum = (queryEnd - queryStart) / 2;

				pChildOperation->childOperations.resizeCapacity(maxChildOperationTasksNum);

				profilesStack.push(pChildOperation);

				queryStart = queryEnd;
			}
		}
		
	}
	
}