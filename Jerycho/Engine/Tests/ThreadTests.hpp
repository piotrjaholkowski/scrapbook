#pragma once

#include "Test.hpp"
#include <mutex>
#include "../Manager/ThreadManager.hpp"
#include "../Physics/RigidBody2d.hpp"
#include "../Gilgamesh/Threads/Threads.hpp"
#include "../Utility/CpuInfo.hpp"
#include "../Gilgamesh/Broad/HashGrid.hpp"


class ThreadTests : public Test
{
private:
	static ThreadTests* singleton;
	const  uint32_t     rigidBodyNum = 4000;
	RigidBody2d*        rigidBodies;
	int32_t             logicalCoresNum;
public:

	ThreadTests(const char* testName) : Test(testName)
	{
		singleton = this;
		addTestCase("Thread array mode initialization performance", JATEST_PERFORMANCE_TEST, threadArrayModeInitializationPerformance);
		addTestCase("Clear test pair bitarray performance", JATEST_PERFORMANCE_TEST, clearTestPairBitArray);
		addTestCase("Compute forces performance", JATEST_PERFORMANCE_TEST ,computeForcesPerformance);
		//addTestCase("Initiazition of thread array mode", threadArrayModeInitialization);
		addTestCase("Locking-Unlocking gate performance", JATEST_PERFORMANCE_TEST, synchroniztionPerformance);
		addTestCase("Is type atomic lock-free", JATEST_UNIT_TEST, isTypeAtomicLockFree);
		addTestCase("Atomic counter", JATEST_UNIT_TEST, atomicCounter);
		addTestCase("Atomic mutex", JATEST_UNIT_TEST, atomicMutex);
		addTestCase("Waiters performance", JATEST_PERFORMANCE_TEST, waitersPerformance);
	}

	void onInitSuite()
	{
		//jaThreadManager::initThreadManager();
	}

	void onDeinitSuite()
	{
		
	}

	void onInitTest()
	{
		ThreadManager::init();
		ThreadManager::getSingleton()->useAtomicBarriers = true;
		
		ThreadManager::getSingleton()->setWarmUpMode(true);
		//logicalCoresNum = cpuInfo.getLogicalCoresNum();
		logicalCoresNum = CpuInfo::getSingleton()->nLogicalProcessors;
		ThreadManager::getSingleton()->setThreadsNum(logicalCoresNum);
	}

	void onDeinitTest()
	{
		ThreadManager::cleanUp();
	}

	static void isTypeAtomicLockFree() {
		std::atomic<bool>     boolAtomic(false);
		std::atomic<uint32_t> uIntAtomic(0);

		int32_t isLockFree = (int32_t)(boolAtomic.is_lock_free());
		//jaTest::logHtml("p", "subdescription", "", "Is bool atomic lock-free : %d", isLockFree);
		isLockFree = (int32_t)(uIntAtomic.is_lock_free());
		//jaTest::logHtml("p", "subdescription", "", "Is unsigned int atomic lock-free : %d", isLockFree);
		
		JATEST_TRUE(true == ( boolAtomic.is_lock_free() && uIntAtomic.is_lock_free()) );
	}

	static void waitersPerformance()
	{
		const int32_t operationNum = 100;

		Test::logHtml("p", "description", "", "Test of solution for active and inactive waiting");

		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i) {
				_mm_pause();
			}
		}
		Test::logHtml("p", "subdescription", "", "_mm_pause %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i) {
				std::this_thread::yield();
			}
		}
		Test::logHtml("p", "subdescription", "", "std::this_thread::yield %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

#ifdef _WIN32
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i) {
				SwitchToThread();
			}
		}
		Test::logHtml("p", "subdescription", "", "SwitchToThread %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());
#endif

	}

	static void synchroniztionPerformance()
	{
		const uint32_t operationNum = 1000;
		
		SDL_mutex* sdlMutex = SDL_CreateMutex();
		TimeStamp::reset();
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i) {
				SDL_mutexP(sdlMutex);
				SDL_mutexV(sdlMutex);
			}
		}
		SDL_DestroyMutex(sdlMutex);
		
		Test::logHtml("p", "subdescription", "", "Releasing-Locking gate SDL mutex %d operations %llu ticks %llu ms", operationNum, TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		
		std::mutex stdMutex;
		TimeStamp::reset();
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i) {
				stdMutex.lock();
				stdMutex.unlock();
			}
		}

		Test::logHtml("p", "subdescription", "", "Releasing-Locking gate std::mutex %d operations %llu ticks %llu ms", operationNum, TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		std::atomic<bool> atomicBool(false);
		TimeStamp::reset();
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i) {
				atomicBool.store(true);
				atomicBool.store(false);
			
			}
		}
		Test::logHtml("p", "subdescription", "", "Releasing-Locking gate std::atomic<bool> %d operations %llu ticks %llu ms", operationNum, TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		std::atomic<int32_t> atomicInt(0);
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i) {
				atomicInt.fetch_add(1);
			}
		}
		Test::logHtml("p", "subdescription", "", "std::atomic<bool> fetch_add %d operations %llu ticks %llu ms", operationNum, TimeStamp::getTicks(), TimeStamp::getMiliseconds());

#ifdef _WIN32
		CRITICAL_SECTION cs;
		InitializeCriticalSection(&cs);
		TimeStamp::reset();
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i) {
				TryEnterCriticalSection(&cs);
				LeaveCriticalSection(&cs);
			}
		}
		DeleteCriticalSection(&cs);
		Test::logHtml("p", "subdescription", "", "Releasing-Locking gate CRITICAL_SECTION %d operations %llu ticks %llu ms", operationNum, TimeStamp::getTicks(), TimeStamp::getMiliseconds());
		
		HANDLE winMutex = CreateMutex(nullptr, false, nullptr);
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i) {
				WaitForSingleObject(winMutex, INFINITE); 
				ReleaseMutex(winMutex);
			}
		}
		CloseHandle(winMutex);
		Test::logHtml("p", "subdescription", "", "Releasing-Locking windows mutex %d operations %llu ticks %llu ms", operationNum, TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		LONG volatile * interlocked = (LONG*)( _aligned_malloc(sizeof(LONG), 32) );
		{
			JATEST_TIMESTAMP(0);
			for (int32_t i = 0; i < operationNum; ++i) {
				InterlockedExchangeAdd(interlocked, 1);
			}
		}
		_aligned_free(  (void*)interlocked  );
		Test::logHtml("p", "subdescription", "", "Releasing-Locking gate InterlockedExchangeAdd %d operations %llu ticks %llu ms", operationNum, TimeStamp::getTicks(), TimeStamp::getMiliseconds());

#endif
	}

	static void clearTestPairBitArray()
	{
		const uint32_t sizeInKB         = 16;
		const uint32_t arraySize        = (HashGrid::GIL_TEST_PAIR_ARRAY_SIZE) / sizeof(int32_t); // 16 * 4 KB
		int32_t*       testBitPairArray = new int32_t[arraySize];

		Test::logHtml("p", "description", "", "Clearing of %d KB memory block", sizeInKB);

		TimeStamp::reset();
		{
			JATEST_TIMESTAMP(0);
			memset(testBitPairArray, 0xcfcfcfcf, arraySize * sizeof(int32_t));
		}		
		Test::logHtml("p", "subdescription", "submain", "Clear of test bit array - single thread %llu ticks %llu ms", TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		ThreadManager::getSingleton()->setThreadsNum(singleton->logicalCoresNum);
		{
			JATEST_TIMESTAMP(0);
			ThreadManager::getSingleton()->createTaskArray(testBitPairArray, arraySize * sizeof(int32_t), 0, 0, Thread::clearTestBitArray, nullptr);
			ThreadManager::getSingleton()->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "submain", "Clear of test bit array - %d threads %llu ticks %llu ms\n", singleton->logicalCoresNum, TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		ThreadManager::getSingleton()->setThreadsNum(singleton->logicalCoresNum / 2);
		{
			JATEST_TIMESTAMP(0);
			ThreadManager::getSingleton()->createTaskArray(testBitPairArray, arraySize * sizeof(int32_t), 0, 0, Thread::clearTestBitArray, nullptr);
			ThreadManager::getSingleton()->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "submain", "Clear of test bit array - %d threads %llu ticks %llu ms", singleton->logicalCoresNum / 2, TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		ThreadManager::getSingleton()->setThreadsNum(2);
		{
			JATEST_TIMESTAMP(0);
			ThreadManager::getSingleton()->createTaskArray(testBitPairArray, arraySize * sizeof(int32_t), 0, 0, Thread::clearTestBitArray, nullptr);
			ThreadManager::getSingleton()->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "submain", "Clear of test bit array - %d threads %llu ticks %llu ms", 2, TimeStamp::getTicks(), TimeStamp::getMiliseconds());

		for (uint32_t i = 0; i < arraySize; ++i) {
			JATEST_TRUE(testBitPairArray[i] == 0);
		}

		delete[] testBitPairArray;
	}

	static void addAtomicCounter(WorkingThread* workingThread) {
		std::atomic<int32_t>* counter = (std::atomic<int>*)(workingThread->taskData);
		
		for (int32_t i = 0; i < 8000; ++i)
			counter->fetch_add(1);
	}

	static void atomicCounter() {
		std::atomic<int32_t> counter(0);

		ThreadManager::getSingleton()->setThreadsNum(4);
		ThreadManager::getSingleton()->createTaskArray(nullptr, 0, nullptr, 0, addAtomicCounter, &counter);
		ThreadManager::getSingleton()->waitUntilFinish();

		JATEST_TRUE(counter.load() == 32000);
	}

	static  void addAtomicMutex(WorkingThread* workingThread) {
		std::atomic<int32_t>* atomicGuard         = (std::atomic<int32_t>*)(workingThread->taskData);
		std::atomic<int32_t>* atomicThreadCounter = (std::atomic<int32_t>*)(workingThread->taskArrayData);

		if (workingThread->threadId == 0) {
			while (atomicGuard->load() == 0) {

			}
			return;
		}
		else {
			while (true) {
				int32_t threadNumber = atomicThreadCounter->fetch_sub(1);
				if (--threadNumber == 1) {
					atomicGuard->store(1);
				}
			}
		}
	}

	static void atomicMutex() {

		std::atomic<int32_t> atomicGuard(0);
		std::atomic<int32_t> atomicThreadCounter(8);

		ThreadManager::getSingleton()->setThreadsNum(8);
		ThreadManager::getSingleton()->createTaskArray(&atomicThreadCounter, 0, nullptr, 0, addAtomicCounter, &atomicGuard);
		ThreadManager::getSingleton()->waitUntilFinish();
	}

	static void computeForcesSSE2(WorkingThread* workingThread)
	{
		uint32_t threadId = workingThread->threadId;
		uint32_t threadsNum = workingThread->threadsNum;
		uint32_t itCounter = 0;
		uint32_t taskId = threadId + (threadsNum * itCounter);

		//gilFloat time = 1.0f / 60.0f;

		__declspec(align(16)) __m128 netForceX;
		__declspec(align(16)) __m128 netForceY;
		__declspec(align(16)) __m128 damping;
		__declspec(align(16)) __m128 temp;
		__declspec(align(16)) __m128 timeFactor;
		__declspec(align(16)) __m128 acceleration;
		__declspec(align(16)) __m128 linearDamping;
		__declspec(align(16)) __m128 linearVelocityX;
		__declspec(align(16)) __m128 linearVelocityY;
		__declspec(align(16)) __m128 gravityX;
		__declspec(align(16)) __m128 gravityY;
		__declspec(align(16)) __m128 mass;
		__declspec(align(16)) __m128 invMass;
		
		
		RigidBody2d* rigidBodyTemp[4];
		int32_t bodyNum = 0;

		for (itCounter = 1; taskId < workingThread->taskNum; ++itCounter)
		{
			RigidBody2d* rigidBody = (RigidBody2d*)(workingThread->taskArrayData) + taskId;		

			rigidBodyTemp[bodyNum] = rigidBody;
			linearDamping.m128_f32[bodyNum] = rigidBody->linearDamping;
			timeFactor.m128_f32[bodyNum] = rigidBody->timeFactor;
			linearVelocityX.m128_f32[bodyNum] = rigidBody->linearVelocity.x;
			linearVelocityY.m128_f32[bodyNum] = rigidBody->linearVelocity.y;
			gravityX.m128_f32[bodyNum] = rigidBody->gravity.x;
			gravityY.m128_f32[bodyNum] = rigidBody->gravity.y;
			mass.m128_f32[bodyNum] = rigidBody->mass;
			invMass.m128_f32[bodyNum] = rigidBody->invMass;
			netForceX.m128_f32[bodyNum] = rigidBody->netForce.x;
			netForceY.m128_f32[bodyNum] = rigidBody->netForce.y;

			if (!rigidBody->isFixedRotation()) // for fixed rotation invInteria is 0
			{
				rigidBody->angularVelocity -= (rigidBody->angularVelocity * rigidBody->angularDamping * rigidBody->timeFactor);
				jaFloat angularAcceleration = rigidBody->torque * rigidBody->invInteria;
				rigidBody->angularVelocity += angularAcceleration;
			}

			++bodyNum;

			if (bodyNum == 4) {
				temp = _mm_mul_ps(gravityX, mass);
				netForceX = _mm_add_ps(netForceX, temp);
				temp = _mm_mul_ps(gravityY, mass);
				netForceY = _mm_add_ps(netForceY, temp);
				damping = _mm_mul_ps(linearDamping, timeFactor);

				temp = _mm_mul_ps(linearVelocityX, damping);
				linearVelocityX = _mm_sub_ps(linearVelocityX, temp);
				temp = _mm_mul_ps(linearVelocityY, damping);
				linearVelocityY = _mm_sub_ps(linearVelocityY, temp);
				
				acceleration = _mm_mul_ps(netForceX, invMass);
				linearVelocityX = _mm_add_ps(linearVelocityX, acceleration);
				acceleration = _mm_mul_ps(netForceY, invMass);
				linearVelocityY = _mm_add_ps(linearVelocityY, acceleration);

				rigidBody = rigidBodyTemp[0];
				rigidBody->netForce.x = netForceX.m128_f32[0];
				rigidBody->netForce.y = netForceY.m128_f32[0];
				rigidBody->linearVelocity.x = linearVelocityX.m128_f32[0];
				rigidBody->linearVelocity.y = linearVelocityY.m128_f32[0];

				rigidBody = rigidBodyTemp[1];
				rigidBody->netForce.x = netForceX.m128_f32[1];
				rigidBody->netForce.y = netForceY.m128_f32[1];
				rigidBody->linearVelocity.x = linearVelocityX.m128_f32[1];
				rigidBody->linearVelocity.y = linearVelocityY.m128_f32[1];

				rigidBody = rigidBodyTemp[2];
				rigidBody->netForce.x = netForceX.m128_f32[2];
				rigidBody->netForce.y = netForceY.m128_f32[2];
				rigidBody->linearVelocity.x = linearVelocityX.m128_f32[2];
				rigidBody->linearVelocity.y = linearVelocityY.m128_f32[2];

				rigidBody = rigidBodyTemp[3];
				rigidBody->netForce.x = netForceX.m128_f32[3];
				rigidBody->netForce.y = netForceY.m128_f32[3];
				rigidBody->linearVelocity.x = linearVelocityX.m128_f32[3];
				rigidBody->linearVelocity.y = linearVelocityY.m128_f32[3];
				
				bodyNum = 0;
			}

			taskId = threadId + (threadsNum * itCounter);
		}
	}

	static void computeForcesTaskArray(WorkingThread* workingThread)
	{	
		uint32_t threadId   = workingThread->threadId;
		uint32_t threadsNum = workingThread->threadsNum;
		uint32_t itCounter  = 0;
		uint32_t taskId     = threadId;

		jaVector2 acceleration;
		jaFloat angluarAcceleration;
		jaFloat damping;
		
		for ( itCounter = 1; taskId < workingThread->taskNum; ++itCounter)
		{
			RigidBody2d* rigidBody = (RigidBody2d*)(workingThread->taskArrayData) + taskId;
	
			rigidBody->netForce.x += rigidBody->gravity.x * rigidBody->mass;
			rigidBody->netForce.y += rigidBody->gravity.y * rigidBody->mass;
			damping = rigidBody->linearDamping * rigidBody->timeFactor;
			rigidBody->linearVelocity.x -= (rigidBody->linearVelocity.x * damping);
			rigidBody->linearVelocity.y -= (rigidBody->linearVelocity.y * damping);
	
			acceleration.x = rigidBody->netForce.x * rigidBody->invMass;
			acceleration.y = rigidBody->netForce.y * rigidBody->invMass;
		
			rigidBody->linearVelocity.x += acceleration.x;
			rigidBody->linearVelocity.y += acceleration.y;

			if (!rigidBody->isFixedRotation()) // for fixed rotation invInteria is 0
			{
				rigidBody->angularVelocity -= (rigidBody->angularVelocity * rigidBody->angularDamping * rigidBody->timeFactor);
				angluarAcceleration = rigidBody->torque * rigidBody->invInteria;
				rigidBody->angularVelocity += angluarAcceleration;
			}

			taskId = threadId + (threadsNum * itCounter);
		}
	}

	static void computeForcesTaskArrayIntervaled(WorkingThread* workingThread)
	{
		uint32_t taskNum = workingThread->taskNum;
		ThreadManager* threadManager = ThreadManager::getSingleton();
		uint32_t taskId = threadManager->getTaskIdAndIncreaseTaskSolvedNumber(1);

		jaVector2 acceleration;
		jaFloat angluarAcceleration;
		jaFloat damping;

		while(taskId < taskNum)
		{
			RigidBody2d* rigidBody = (RigidBody2d*)(workingThread->taskArrayData) + taskId;

			rigidBody->netForce.x += rigidBody->gravity.x * rigidBody->mass;
			rigidBody->netForce.y += rigidBody->gravity.y * rigidBody->mass;
			damping = rigidBody->linearDamping * rigidBody->timeFactor;
			rigidBody->linearVelocity.x -= (rigidBody->linearVelocity.x * damping);
			rigidBody->linearVelocity.y -= (rigidBody->linearVelocity.y * damping);

			acceleration.x = rigidBody->netForce.x * rigidBody->invMass;
			acceleration.y = rigidBody->netForce.y * rigidBody->invMass;

			rigidBody->linearVelocity.x += acceleration.x;
			rigidBody->linearVelocity.y += acceleration.y;

			if (!rigidBody->isFixedRotation()) // for fixed rotation invInteria is 0
			{
				rigidBody->angularVelocity -= (rigidBody->angularVelocity * rigidBody->angularDamping * rigidBody->timeFactor);
				angluarAcceleration = rigidBody->torque * rigidBody->invInteria;
				rigidBody->angularVelocity += angluarAcceleration;
			}

			taskId = threadManager->getTaskIdAndIncreaseTaskSolvedNumber(1);
		}
	}

	static void computeForcesSSE3(WorkingThread* workingThread)
	{
		uint32_t threadId   = workingThread->threadId;
		uint32_t threadsNum = workingThread->threadsNum;
		uint32_t itCounter  = 0;
		uint32_t taskId     = threadId;

		__declspec(align(16)) __m128 acceleration;
		__declspec(align(16)) __m128 linearVelocity;
		jaFloat angluarAcceleration;
		jaFloat damping;


		for (itCounter = 1; taskId < workingThread->taskNum; ++itCounter)
		{
			RigidBody2d* rigidBody = static_cast<RigidBody2d*>(workingThread->taskArrayData) + taskId;

			rigidBody->netForce.x = rigidBody->netForce.x + (rigidBody->gravity.x * rigidBody->mass);
			rigidBody->netForce.y = rigidBody->netForce.y + (rigidBody->gravity.y * rigidBody->mass);

			damping = rigidBody->linearDamping * rigidBody->timeFactor;
			linearVelocity.m128_f32[0] = rigidBody->linearVelocity.x - (rigidBody->linearVelocity.x * damping);
			linearVelocity.m128_f32[1] = rigidBody->linearVelocity.y - (rigidBody->linearVelocity.y * damping);

			acceleration.m128_f32[0] = rigidBody->netForce.x * rigidBody->invMass;
			acceleration.m128_f32[1] = rigidBody->netForce.y * rigidBody->invMass;

			__m128 linVel = _mm_add_ps(linearVelocity, acceleration);
			rigidBody->linearVelocity.x = linVel.m128_f32[0];
			rigidBody->linearVelocity.y = linVel.m128_f32[1];
			//rigidBody->linearVelocity.x = rigidBody->linearVelocity.x + acceleration.x;
			//rigidBody->linearVelocity.y = rigidBody->linearVelocity.y + acceleration.y;

			if (!rigidBody->isFixedRotation()) // for fixed rotation invInteria is 0
			{
				rigidBody->angularVelocity -= (rigidBody->angularVelocity * rigidBody->angularDamping * rigidBody->timeFactor);
				angluarAcceleration = rigidBody->torque * rigidBody->invInteria;
				rigidBody->angularVelocity = rigidBody->angularVelocity + angluarAcceleration;
			}

			taskId = threadId + (threadsNum * itCounter);
		}
	}

	static void computeForcesPerformance()
	{
		Test::logHtml("p", "description", "", "Test of computing forces performance %d bodies", singleton->rigidBodyNum);
		RigidBody2d* rigidBodies = new RigidBody2d[singleton->rigidBodyNum];

		//gilFloat time = 1.0f / 60.0f;
		jaVector2 acceleration;
		jaFloat angluarAcceleration = 0.0f;
		jaFloat damping;

		for (unsigned int i = 0; i < singleton->rigidBodyNum; ++i) {
			rigidBodies[i].linearVelocity.x = 0.0f;
			rigidBodies[i].linearVelocity.y = 0.0f;
			rigidBodies[i].linearDamping = 0.2f;
			rigidBodies[i].gravity.x = 0.0f;
			rigidBodies[i].gravity.y = -0.2f;
			//rigidBodies[i].setFixedRotation(false);
			rigidBodies[i].bodyFlag = rigidBodies[i].bodyFlag & (uint16_t)BodyFlag::BODY_FIXED_ROTATION;
			rigidBodies[i].timeFactor = 1.0f;
			rigidBodies[i].netForce.x = 0.0f;
			rigidBodies[i].netForce.y = 0.0f;
			rigidBodies[i].invInteria = 0.0f;
			rigidBodies[i].angularVelocity = 0.0f;
			rigidBodies[i].angularDamping = 0.2f;
			rigidBodies[i].torque = 0.0f;
		}

		TimeStamp::reset();
		{
			JATEST_TIMESTAMP(0);
			for (uint32_t i = 0; i < singleton->rigidBodyNum; ++i) {
				rigidBodies[i].netForce += (rigidBodies[i].gravity * rigidBodies[i].mass);
				damping = rigidBodies[i].linearDamping * rigidBodies[i].timeFactor;
				rigidBodies[i].linearVelocity.x -= (rigidBodies[i].linearVelocity.x * damping);
				rigidBodies[i].linearVelocity.y -= (rigidBodies[i].linearVelocity.y * damping);
				acceleration = rigidBodies[i].netForce * rigidBodies[i].invMass;
				rigidBodies[i].linearVelocity += acceleration;

				if (!rigidBodies[i].isFixedRotation()) // for fixed rotation invInteria is 0
				{
					rigidBodies[i].angularVelocity -= (rigidBodies[i].angularVelocity * rigidBodies[i].angularDamping * rigidBodies[i].timeFactor);
					angluarAcceleration = rigidBodies[i].torque * rigidBodies[i].invInteria;
					rigidBodies[i].angularVelocity += angluarAcceleration;
				}
			}
		}
		Test::logHtml("p","subdescription","","Normal function time %llu ms %llu ticks", TimeStamp::getMiliseconds(), TimeStamp::getTicks() );

		ThreadManager::getSingleton()->setThreadsNum(singleton->logicalCoresNum);

		{
			JATEST_TIMESTAMP(0);
			ThreadManager::getSingleton()->createTaskArray(rigidBodies, singleton->rigidBodyNum, nullptr, 0, computeForcesTaskArray, nullptr);
			ThreadManager::getSingleton()->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "", "TaskArray function %d threads time %llu ms %llu ticks", singleton->logicalCoresNum, TimeStamp::getMiliseconds(), TimeStamp::getTicks());

		{
			ThreadManager::getSingleton()->zeroTaskCounter();
			JATEST_TIMESTAMP(0);
			ThreadManager::getSingleton()->createTaskArray(rigidBodies, singleton->rigidBodyNum, nullptr, 0, computeForcesTaskArrayIntervaled, nullptr);
			ThreadManager::getSingleton()->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "", "TaskArrayIntervaled function %d threads time %llu ms %llu ticks", singleton->logicalCoresNum, TimeStamp::getMiliseconds(), TimeStamp::getTicks());

		{
			JATEST_TIMESTAMP(0);
			ThreadManager::getSingleton()->createTaskArray(rigidBodies, singleton->rigidBodyNum, nullptr, 0, computeForcesSSE3, nullptr);
			ThreadManager::getSingleton()->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "", "Multithreaded SSE3 function %d threads time %llu ms %llu ticks",singleton->logicalCoresNum, TimeStamp::getMiliseconds(), TimeStamp::getTicks());
	
		ThreadManager::getSingleton()->setThreadsNum(singleton->logicalCoresNum / 2);
		{
			JATEST_TIMESTAMP(0);
			ThreadManager::getSingleton()->createTaskArray(rigidBodies, singleton->rigidBodyNum, nullptr, 0, computeForcesSSE3, nullptr);
			ThreadManager::getSingleton()->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "", "Multithreaded SSE3 function %d threads time %llu ms %llu ticks", singleton->logicalCoresNum / 2, TimeStamp::getMiliseconds(), TimeStamp::getTicks());

		{
			JATEST_TIMESTAMP(0);
			ThreadManager::getSingleton()->createTaskArray(rigidBodies, singleton->rigidBodyNum, nullptr, 0, computeForcesTaskArray, nullptr);
			ThreadManager::getSingleton()->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "", "TaskArray function %d threads time %llu ms %llu ticks", singleton->logicalCoresNum / 2,TimeStamp::getMiliseconds(), TimeStamp::getTicks());

		{
			ThreadManager::getSingleton()->zeroTaskCounter();
			JATEST_TIMESTAMP(0);
			ThreadManager::getSingleton()->createTaskArray(rigidBodies, singleton->rigidBodyNum, nullptr, 0, computeForcesTaskArrayIntervaled, nullptr);
			ThreadManager::getSingleton()->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "", "TaskArrayIntervaled function %d threads time %llu ms %llu ticks", singleton->logicalCoresNum / 2, TimeStamp::getMiliseconds(), TimeStamp::getTicks());

		/*__declspec(align(16)) __m128 floats1;
		__declspec(align(16)) __m128 floats2;
		__declspec(align(16)) __m128 floats3;
		
		floats1.m128_f32[0] = 1.0f;
		floats1.m128_f32[1] = 2.0f;
		floats1.m128_f32[2] = 5.0f;
		floats1.m128_f32[3] = 8.0f;

		floats2.m128_f32[0] = 2.0f;
		floats2.m128_f32[1] = 3.0f;
		floats2.m128_f32[2] = 4.0f;
		floats2.m128_f32[3] = 5.0f;

		floats3 = _mm_mul_ps(floats1, floats2);
		for (int i = 0; i < 4; ++i) {
			std::cout << "P " << floats1.m128_f32[i] << std::endl;
		}*/

		delete[] rigidBodies;
	}

	static void add5WorkingThread(WorkingThread* workingThread) {
		uint32_t threadId   = workingThread->threadId;
		uint32_t threadsNum = workingThread->threadsNum;
		uint32_t itCounter  = 0;
		uint32_t taskId     = threadId;
		uint32_t taskNum    = workingThread->taskNum;
		int32_t* numbers    = (int32_t*)(workingThread->taskArrayData);

		numbers[taskId] += 5;
	}

	static void threadArrayModeInitialization()
	{
		ThreadManager* threadManager = ThreadManager::getSingleton();
		threadManager->setThreadsNum(16);

		int32_t numbers[16];

		int32_t zEnd = 3;
		if (threadManager->isWarmUpMode())
			zEnd = 0;

		for (int32_t z = 0; z < zEnd; ++z)
		{
			for (uint32_t i = 0; i < 16; ++i) {
				numbers[i] = 0;
			}

			for (int32_t i = 0; i < 5000; ++i) {
				threadManager->createTaskArray(numbers, 16, nullptr, 0, add5WorkingThread, nullptr);
				threadManager->waitUntilFinish();
				//Sleep(1);
			}

			for (int32_t i = 0; i < 8; ++i) {
				JATEST_TRUE(numbers[i] == 25000);
			}
		}	
	}

	static void emptyWorkingThread(WorkingThread* workingThread) {

	}

	static void threadArrayModeInitializationPerformance()
	{
		Test::logHtml("p", "description", "", "Thread array mode initialization performance");
		ThreadManager* threadManager = ThreadManager::getSingleton();
		
		threadManager->setWarmUpMode(false);
		threadManager->setThreadsNum(singleton->logicalCoresNum);
		{
			JATEST_TIMESTAMP(0);
			threadManager->createTaskArray(nullptr, 0, nullptr, 0, emptyWorkingThread, nullptr);
			threadManager->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "", "Thread array mode initialization %d threads time %llu ms %llu ticks", singleton->logicalCoresNum, TimeStamp::getMiliseconds(), TimeStamp::getTicks());

		threadManager->setThreadsNum(singleton->logicalCoresNum / 2);
		{
			JATEST_TIMESTAMP(0);
			threadManager->createTaskArray(nullptr, 0, nullptr, 0, emptyWorkingThread, nullptr);
			threadManager->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "", "Thread array mode initialization %d threads time %llu ms %llu ticks", singleton->logicalCoresNum / 2, TimeStamp::getMiliseconds(), TimeStamp::getTicks());

		threadManager->setWarmUpMode(true);
		threadManager->setThreadsNum(singleton->logicalCoresNum);
		{
			JATEST_TIMESTAMP(0);
			threadManager->createTaskArray(nullptr, 0, nullptr, 0, emptyWorkingThread, nullptr);
			threadManager->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "", "Thread array atomic mode initialization %d threads time %llu ms %llu ticks",singleton->logicalCoresNum, TimeStamp::getMiliseconds(), TimeStamp::getTicks());

		threadManager->setThreadsNum(singleton->logicalCoresNum / 2);
		{
			JATEST_TIMESTAMP(0);
			threadManager->createTaskArray(nullptr, 0, nullptr, 0, emptyWorkingThread, nullptr);
			threadManager->waitUntilFinish();
		}
		Test::logHtml("p", "subdescription", "", "Thread array atomic mode initialization %d threads time %llu ms %llu ticks", singleton->logicalCoresNum / 2, TimeStamp::getMiliseconds(), TimeStamp::getTicks());

	}
};

ThreadTests* ThreadTests::singleton = nullptr;
