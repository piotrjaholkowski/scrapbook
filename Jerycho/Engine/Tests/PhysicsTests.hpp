#pragma once

#include "Test.hpp"
#include "../Manager/LevelManager.hpp"
#include "../Manager/PhysicsManager.hpp"
#include "../Manager/ThreadManager.hpp"
#include "../Utility/CpuInfo.hpp"


class PhysicsTests : public Test
{
private:

public:
	PhysicsTests(const char* testName) : Test(testName)
	{
		addTestCase("Deterministic validity", JATEST_UNIT_TEST, deterministicValidity);
		addTestCase("BoxStacks.xml energy test", JATEST_UNIT_TEST, boxStacksEnergyTest);
		addTestCase("SlidingBox.xml energy test", JATEST_UNIT_TEST, slidingBoxEnergyTest);
		addTestCase("FallingCircles.xml energy test", JATEST_UNIT_TEST, fallingCirclesEnergyTest);
		addTestCase("Multiple-Single thread performance", JATEST_PERFORMANCE_TEST, multipleSingleThreadPerformance);
		addTestCase("Structures padding validity", JATEST_UNIT_TEST, structuresPaddingValidity);
		addTestCase("Hash test pair", JATEST_UNIT_TEST, hashTestPair);
	}

	void onInitSuite()
	{
		//LevelManager::init();
		ThreadManager::init();
		ThreadManager::getSingleton()->setWarmUpMode(true);
		ThreadManager::getSingleton()->setThreadsNum(1);
	}

	void onDeinitSuite()
	{
		//LevelManager::cleanUp();
		ThreadManager::cleanUp();
	}

	void onInitTest()
	{
		PhysicsManager::init(PhysicsDefaultConfiguration());
	}

	void onDeinitTest()
	{
		PhysicsManager::cleanUp();
	}

	static void hashTestPair()
	{
		HashCodes codes0;
		HashCodes codes1;
		HashCodes bucketMask0;
		HashCodes bucketMask1;

		codes0.clear();
		codes1.clear();

		codes0.addHashCode(0);
		codes1.addHashCode(0);

		bucketMask0.hashCodes[0] = 0x7fff;
		bucketMask0.hashCodes[1] = 0xffff;
		bucketMask0.hashCodes[2] = 0xffff;
		bucketMask0.hashCodes[3] = 0xffff;

		bucketMask1.hashCodes[0] = 0x3fff;
		bucketMask1.hashCodes[1] = 0xffff;
		bucketMask1.hashCodes[2] = 0xffff;
		bucketMask1.hashCodes[3] = 0xffff;

		bool test0 = HashCodes::testPair(codes0, codes1, bucketMask0);
		bool test1 = HashCodes::testPair(codes0, codes1, bucketMask1);

		codes0.addHashCode(64);
		codes1.addHashCode(64);
		bool test2 = HashCodes::testPair(codes0, codes1, bucketMask1);

	}

	static void structuresPaddingValidity() {
		JATEST_TRUE(sizeof(CollisionDataPairMultithread) == 64);
		JATEST_TRUE(sizeof(HashUpdateMultithread) == 64);
	}

	static void deterministicValidity()
	{
		jaFloat timeStep = 1.0f / 60.0f;

		//LevelManager::getSingleton()->loadLevel("performance\\boxesFalling.xml");
		PhysicsManager* physicsManager1 = PhysicsManager::getSingleton();

		PhysicsManager* physicsManager2 = new PhysicsManager(PhysicsDefaultConfiguration());
		PhysicsManager::singleton = physicsManager2;
		//LevelManager::getSingleton()->loadLevel("performance\\boxesFalling.xml");
	
		for (int32_t i = 0; i < 300; ++i) {

			ThreadManager::getSingleton()->setThreadsNum(1);
			physicsManager1->step(timeStep);
			ThreadManager::getSingleton()->setThreadsNum(3);
			physicsManager2->step(timeStep);

			HashGrid* hashGrid1 = (HashGrid*)(physicsManager1->getScene());
			HashGrid* hashGrid2 = (HashGrid*)(physicsManager2->getScene());

			ObjectHandle2d* objectHandle1 = hashGrid1->getLastDynamicObject();
			ObjectHandle2d* objectHandle2 = hashGrid2->getLastDynamicObject();

			ConstraintIsland2d* island1 = physicsManager1->constraintIsland;
			ConstraintIsland2d* island2 = physicsManager2->constraintIsland;

			while ((island1 != nullptr) && (island2 != nullptr)) {
				ConstraintList2d* constraintList1 = island1->firstElement;
				ConstraintList2d* constraintList2 = island2->firstElement;

				while ((constraintList1 != nullptr) && (constraintList2 != nullptr)) {
					Constraint2d* constraint1 = constraintList1->constraint;
					Constraint2d* constraint2 = constraintList2->constraint;

					JATEST_TRUE(constraint1->objectA->getId() == constraint2->objectA->getId());
					JATEST_TRUE(constraint1->objectB->getId() == constraint2->objectB->getId());

					constraintList1 = constraintList1->next;
					constraintList2 = constraintList2->next;
				}

				JATEST_TRUE(constraintList1 == nullptr);
				JATEST_TRUE(constraintList2 == nullptr);

				island1 = island1->next;
				island2 = island2->next;
			}
			JATEST_TRUE(island1 == nullptr);
			JATEST_TRUE(island2 == nullptr);

			while ((objectHandle1 != nullptr) && (objectHandle2 != nullptr)) {
				JATEST_TRUE(objectHandle1->getId() == objectHandle2->getId());

				HashProxy* proxy1 = &objectHandle1->hashProxy;
				HashProxy* proxy2 = &objectHandle2->hashProxy;

				HashNode* hashNode1 = proxy1->firstNode;
				HashNode* hashNode2 = proxy1->firstNode;

				while ((hashNode1 != nullptr) && (hashNode2 != nullptr)) {
					JATEST_TRUE(hashNode1->bucketId == hashNode2->bucketId);

					hashNode1 = hashNode1->nextProxyNode;
					hashNode2 = hashNode2->nextProxyNode;
				}
				JATEST_TRUE(hashNode1 == nullptr);
				JATEST_TRUE(hashNode2 == nullptr);

				objectHandle1 = objectHandle1->prev;
				objectHandle2 = objectHandle2->prev;
			}
			JATEST_TRUE(objectHandle1 == nullptr);
			JATEST_TRUE(objectHandle2 == nullptr);

			uint16_t objectsNum = hashGrid1->getHighestGeneratedId();
			for (uint16_t x = 0; x < objectsNum; ++x)
			{
				PhysicObject2d* physicObject1 = physicsManager1->physicObjects[x];
				PhysicObject2d* physicObject2 = physicsManager2->physicObjects[x];
				if (physicObject1 == nullptr)
					continue;
				RigidBody2d* rigidBody1 = (RigidBody2d*)(physicObject1);
				RigidBody2d* rigidBody2 = (RigidBody2d*)(physicObject2);
				JATEST_TRUE(rigidBody1->linearVelocity.x == rigidBody2->linearVelocity.x);
				JATEST_TRUE(rigidBody1->linearVelocity.y == rigidBody2->linearVelocity.y);
				JATEST_TRUE(rigidBody1->angularVelocity == rigidBody2->angularVelocity);
			}

			int16_t bucketNum = hashGrid1->getBucketNum();
			for (int16_t z = 0; z < bucketNum; ++z) {
				HashNode* hashNode1 = hashGrid1->hashBucketDynamic[z].firsNode;
				HashNode* hashNode2 = hashGrid2->hashBucketDynamic[z].firsNode;

				while ((hashNode1 != nullptr) && (hashNode2 != nullptr)) {
					JATEST_TRUE(hashNode1->bucketId == hashNode2->bucketId);

					hashNode1 = hashNode1->next;
					hashNode2 = hashNode2->next;
				}

				JATEST_TRUE(hashNode1 == nullptr);
				JATEST_TRUE(hashNode2 == nullptr);
			}

		}

		delete physicsManager1;
	}

	static void boxStackEnergyTest()
	{
		jaFloat density = GIL_ONE;
		Box2d box(2.5f, 2.5f);
		box.setDensity(density);
		box.setFriction(0.2f);
		Box2d ground(20.0f, 2.5f);
		ground.setDensity(density);
		ground.setFriction(0.0f);
		//jaVector2 position(0.0f, 0.0f);
		jaTransform2 transform(jaVector2(0.0f,0.0f), 0.0f);
		jaTransform2 groundTransform( jaVector2(0.0f, -2.5f), 0.0f);
		jaVector2 gravity(0.0f, setup::physics::DEFAULT_GRAVITY);

		RigidBody2d* dynamicBodies[15];

		for (int i = 0; i < 15; ++i)
		{
			transform.position.y += 2.5f;
			dynamicBodies[i] = PhysicsManager::getSingleton()->createBody(&box, BodyType::DYNAMIC_BODY, transform, false, false);
			dynamicBodies[i]->setGravity(gravity);
			dynamicBodies[i]->setAllowSleep(true);
		}

		PhysicsManager::getSingleton()->createBody(&ground, BodyType::STATIC_BODY, groundTransform, true, true);

		jaFloat timeStep = 1.0f / 60.0f;
		bool allBodiesAsleep = false;

		while (!allBodiesAsleep)
		{
			PhysicsManager::getSingleton()->step(timeStep);
			allBodiesAsleep = true;

			for (int i = 0; i < 15; ++i)
			{
				if (!dynamicBodies[i]->isSleeping())
				{
					allBodiesAsleep = false;
					break;
				}
			}
		}
	}

	static void energyTest(jaFloat sleepTime, jaFloat maxLinearVelocity, jaFloat maxAngularVelocity)
	{
		jaFloat timeStep = 1.0f / 60.0f;
		bool allBodiesAsleep = false;

		while (!allBodiesAsleep)
		{
			TimeStamp::reset();
			PhysicsManager::getSingleton()->step(timeStep);
			sleepTime -= timeStep;
			allBodiesAsleep = true;

			PhysicObject2d* itPhysicObject = PhysicsManager::getSingleton()->getLastAwakePhysicObject();

			for (itPhysicObject; itPhysicObject != nullptr; itPhysicObject = itPhysicObject->getPrevious())
			{
				if (!itPhysicObject->isSleeping())
				{
					if (itPhysicObject->getObjectType() == (uint8_t)PhysicObjectType::RIGID_BODY)
					{
						RigidBody2d* rigidBody = static_cast<RigidBody2d*>(itPhysicObject);
						jaFloat angularVelocity = rigidBody->getAngularVelocity();
						jaVector2 linearVelocity = rigidBody->getLinearVelocity();
						jaFloat velocity = jaVectormath2::length(linearVelocity);

						JATEST_ASSERT(velocity > maxLinearVelocity);
						JATEST_ASSERT(angularVelocity > maxAngularVelocity);
					}

					allBodiesAsleep = false;
					break;
				}
			}

			JATEST_ASSERT(sleepTime <= GIL_ZERO);
		}
	}

	static void boxStacksEnergyTest()
	{
		//LevelManager::getSingleton()->loadLevel("tests\\boxStacks.xml");

		jaFloat sleepTime = 19.37967f;
		jaFloat maxLinearVelocity = 5.62000f;
		jaFloat maxAngularVelocity = 19.37967f;

		energyTest(sleepTime, maxLinearVelocity, maxAngularVelocity);
	}

	static void slidingBoxEnergyTest()
	{
		//LevelManager::getSingleton()->loadLevel("tests\\slidingBox.xml");

		jaFloat sleepTime = 41.41400f;
		jaFloat maxLinearVelocity = 3.57372f;
		jaFloat maxAngularVelocity = 41.41400f;

		energyTest(sleepTime, maxLinearVelocity, maxAngularVelocity);
	}

	static void fallingCirclesEnergyTest()
	{
		//LevelManager::getSingleton()->loadLevel("tests\\fallingCircles.xml");

		jaFloat sleepTime = 32.71600f;
		jaFloat maxLinearVelocity = 5.63734f;
		jaFloat maxAngularVelocity = 32.71600f;

		energyTest(sleepTime, maxLinearVelocity, maxAngularVelocity);
	}

	class jaLogTimeData
	{
	public:

		Uint64 longestTimeTicks;
		Uint64 shortestTimeTicks;
		Uint64 averageTimeTicks;
		Uint64 longestTimeMs;
		Uint64 shortestTimeMs;
		Uint64 averageTimeMs;

		jaLogTimeData() {

		}

		jaLogTimeData(Uint64 longestTimeTicks, Uint64 shortestTimeTicks, Uint64 averageTimeTicks, Uint64 longestTimeMs, Uint64 shortestTimeMs, Uint64 averageTimeMs) {
			this->longestTimeMs = longestTimeMs;
			this->shortestTimeMs = shortestTimeMs;
			this->averageTimeMs = averageTimeMs;
			this->longestTimeTicks = longestTimeTicks;
			this->shortestTimeTicks = shortestTimeTicks;
			this->averageTimeTicks = averageTimeTicks;
		}
	};

	static jaLogTimeData logTimes(Uint64** ticks, Uint64** ms, unsigned char operationtype, unsigned int simulatedFrames)
	{
		Uint64 longestTimeTicks = 0;
		Uint64 shortestTimeTicks = INT_MAX;
		Uint64 averageTimeTicks = 0;
		Uint64 longestTimeMs;
		Uint64 shortestTimeMs;
		Uint64 averageTimeMs;

		longestTimeTicks = ticks[operationtype][0];
		shortestTimeTicks = ticks[operationtype][0];
		averageTimeTicks = ticks[operationtype][0];
		longestTimeMs = ms[operationtype][0];
		shortestTimeMs = ms[operationtype][0];
		averageTimeMs = ms[operationtype][0];

		for (unsigned int i = 0; i < simulatedFrames; ++i) {
			if (ms[operationtype][i] > longestTimeMs)
				longestTimeMs = ms[operationtype][i];
			if (ms[operationtype][i] < shortestTimeMs)
				shortestTimeMs = ms[operationtype][i];
			averageTimeMs = (averageTimeMs + ms[operationtype][i]) / 2;

			if (ticks[operationtype][i] > longestTimeTicks)
				longestTimeTicks = ticks[operationtype][i];
			if (ticks[operationtype][i] < shortestTimeTicks)
				shortestTimeTicks = ticks[operationtype][i];
			averageTimeTicks = (averageTimeTicks + ticks[operationtype][i]) / 2;
		}

		Test::logHtml("p", "description", "submain", "Longest ms %llu", longestTimeMs);
		Test::logHtml("p", "description", "submain", "Shortest ms %llu", shortestTimeMs);
		Test::logHtml("p", "description", "submain", "Average ms %llu", averageTimeMs);
		Test::logHtml("p", "description", "submain", "Longest ticks %llu", longestTimeTicks);
		Test::logHtml("p", "description", "submain", "Shortest ticks %llu", shortestTimeTicks);
		Test::logHtml("p", "description", "submain", "Average ticks %llu", averageTimeTicks);

		return jaLogTimeData(longestTimeTicks, shortestTimeTicks, averageTimeTicks, longestTimeMs, shortestTimeMs, averageTimeMs);
	}

	static void multipleSingleThreadPerformance()
	{
		const unsigned int framesToSimulate = 360;
		jaFloat timeStep = 1.0f / 60.0f;

		Uint64** ticks = new Uint64*[8];
		Uint64** ms = new Uint64*[8];

		for (unsigned int i = 0; i < 8; ++i) {
			ticks[i] = new Uint64[framesToSimulate];
			ms[i] = new Uint64[framesToSimulate];
		}

		//gilCpuInfo cpuInfo;
		ChartDataSet<Uint64> timeChart(2, 8, 20);
		timeChart.setDataSampleName(0, "Compute forces");
		timeChart.setDataSampleName(1, "Generate contacts");
		timeChart.setDataSampleName(2, "Generate islands");
		timeChart.setDataSampleName(3, "Resolve constraints");
		timeChart.setDataSampleName(4, "Make island sleep");
		timeChart.setDataSampleName(5, "Collision detection");
		timeChart.setDataSampleName(6, "Integration");
		timeChart.setDataSampleName(7, "Clear forces");
		timeChart.chartType = JA_BAR_CHART;
		ChartSetColors* singleThreadColors = timeChart.getSetColors(0);
		ChartSetColors* multiThreadColors = timeChart.getSetColors(1);

		singleThreadColors->fillColor.r = 255;
		singleThreadColors->fillColor.g = 0;
		singleThreadColors->fillColor.b = 0;
		singleThreadColors->fillColor.a = 255;

		multiThreadColors->fillColor.r = 0;
		multiThreadColors->fillColor.g = 0;
		multiThreadColors->fillColor.b = 255;
		multiThreadColors->fillColor.a = 255;

		Test::logHtml("p", "description", "", "Single thread test");
		//LevelManager::getSingleton()->loadLevel("performance\\boxesFalling.xml");
		ThreadManager::getSingleton()->setThreadsNum(1);

		for (unsigned int i = 0; i < framesToSimulate; ++i) {
			TimeStamp::reset();
			PhysicsManager::getSingleton()->step(timeStep);

			for (unsigned char z = 0; z < 8; ++z) {
				TimeStampData* timeStampData = TimeStamp::getTimeStampData(z + 1);
				ms[z][i] = timeStampData->deltaTime;
				ticks[z][i] = timeStampData->ticks;
			}
		}

		/*JA_COMPUTE_FORCES_TIME_STAMP = 1,
		JA_GENERATE_CONTACTS_TIME_STAMP = 2,
		JA_GENERATE_CONSTRAINT_ISLAND_TIME_STAMP = 3,
		JA_RESOLVE_CONSTRAINTS_TIME_STAMP = 4,
		JA_MAKE_ISLAND_SLEEP_TIME_STAMP = 5,
		JA_COLLISION_DETECTION_TIME_STAMP = 6,
		JA_INTEGRATION_TIME_STAMP = 7,
		JA_CLEAR_FORCES_TIME_STAMP = 8*/
		jaLogTimeData singleTimeData[8];

		Test::logHtml("p", "description", "", "Compute forces");
		singleTimeData[0] = logTimes(ticks, ms, 0, framesToSimulate);
		Test::logHtml("p", "description", "", "Generate contacts");
		singleTimeData[1] = logTimes(ticks, ms, 1, framesToSimulate);
		Test::logHtml("p", "description", "", "Generate constraints");
		singleTimeData[2] = logTimes(ticks, ms, 2, framesToSimulate);
		Test::logHtml("p", "description", "", "Resolve constraints");
		singleTimeData[3] = logTimes(ticks, ms, 3, framesToSimulate);
		Test::logHtml("p", "description", "", "Make island sleep");
		singleTimeData[4] = logTimes(ticks, ms, 4, framesToSimulate);
		Test::logHtml("p", "description", "", "Collision detection");
		singleTimeData[5] = logTimes(ticks, ms, 5, framesToSimulate);
		Test::logHtml("p", "description", "", "Integration");
		singleTimeData[6] = logTimes(ticks, ms, 6, framesToSimulate);
		Test::logHtml("p", "description", "", "Clear forces");
		singleTimeData[7] = logTimes(ticks, ms, 7, framesToSimulate);

		Test::logHtml("p", "description", "", "Multiple thread test");
		PhysicsManager::cleanUp();
		PhysicsManager::init(PhysicsDefaultConfiguration());

		//LevelManager::getSingleton()->loadLevel("performance\\boxesFalling.xml");
		//jaThreadManager::getSingleton()->setThreadsNum(cpuInfo.getLogicalCoresNum());
		ThreadManager::getSingleton()->setThreadsNum(3);

		for (unsigned int i = 0; i < framesToSimulate; ++i) {
			TimeStamp::reset();
			PhysicsManager::getSingleton()->step(timeStep);

			for (unsigned char z = 0; z < 8; ++z) {
				TimeStampData* timeStampData = TimeStamp::getTimeStampData(z + 1);
				ms[z][i] = timeStampData->deltaTime;
				ticks[z][i] = timeStampData->ticks;
			}
		}

		jaLogTimeData multipleTimeData[8];

		Test::logHtml("p", "description", "", "Compute forces");
		multipleTimeData[0] = logTimes(ticks, ms, 0, framesToSimulate);
		Test::logHtml("p", "description", "", "Generate contacts");
		multipleTimeData[1] = logTimes(ticks, ms, 1, framesToSimulate);
		Test::logHtml("p", "description", "", "Generate constraints");
		multipleTimeData[2] = logTimes(ticks, ms, 2, framesToSimulate);
		Test::logHtml("p", "description", "", "Resolve constraints");
		multipleTimeData[3] = logTimes(ticks, ms, 3, framesToSimulate);
		Test::logHtml("p", "description", "", "Make island sleep");
		multipleTimeData[4] = logTimes(ticks, ms, 4, framesToSimulate);
		Test::logHtml("p", "description", "", "Collision detection");
		multipleTimeData[5] = logTimes(ticks, ms, 5, framesToSimulate);
		Test::logHtml("p", "description", "", "Integration");
		multipleTimeData[6] = logTimes(ticks, ms, 6, framesToSimulate);
		Test::logHtml("p", "description", "", "Clear forces");
		multipleTimeData[7] = logTimes(ticks, ms, 7, framesToSimulate);

		for (unsigned int i = 0; i < 8; ++i) {
			timeChart.dataSets[0][i] = singleTimeData[i].longestTimeTicks;
			timeChart.dataSets[1][i] = multipleTimeData[i].longestTimeTicks;
		}

		logChart("LongestTicks", 650, 400);
		logChartData("LongestTicks", &timeChart);

		for (unsigned int i = 0; i < 8; ++i) {
			timeChart.dataSets[0][i] = singleTimeData[i].longestTimeMs;
			timeChart.dataSets[1][i] = multipleTimeData[i].longestTimeMs;
		}

		logChart("LongestMs", 650, 400);
		logChartData("LongestMs", &timeChart);
		logHtml("p", "", "", "Max");

		//////////////////////////
		for (unsigned int i = 0; i < 8; ++i) {
			timeChart.dataSets[0][i] = singleTimeData[i].averageTimeTicks;
			timeChart.dataSets[1][i] = multipleTimeData[i].averageTimeTicks;
		}
		logChart("AverageTicks", 650, 400);
		logChartData("AverageTicks", &timeChart);


		for (unsigned int i = 0; i < 8; ++i) {
			timeChart.dataSets[0][i] = singleTimeData[i].averageTimeMs;
			timeChart.dataSets[1][i] = multipleTimeData[i].averageTimeMs;
		}
		logChart("AverageMs", 650, 400);
		logChartData("AverageMs", &timeChart);
		logHtml("p", "", "", "Average");
		////////////////////////////
		for (unsigned int i = 0; i < 8; ++i) {
			timeChart.dataSets[0][i] = singleTimeData[i].shortestTimeTicks;
			timeChart.dataSets[1][i] = multipleTimeData[i].shortestTimeTicks;
		}
		logChart("ShortestTicks", 650, 400);
		logChartData("ShortestTicks", &timeChart);


		for (unsigned int i = 0; i < 8; ++i) {
			timeChart.dataSets[0][i] = singleTimeData[i].shortestTimeMs;
			timeChart.dataSets[1][i] = multipleTimeData[i].shortestTimeMs;
		}
		logChart("ShortestMs", 650, 400);
		logChartData("ShortestMs", &timeChart);
		logHtml("p", "", "", "Shortest");

		for (int i = 0; i < 8; ++i)
		{
			delete[] ticks[i];
			delete[] ms[i];
		}

		delete[] ticks;
		delete[] ms;
	}
};
