#pragma once

inline jaMatrix2::jaMatrix2() : sinTheta(0), cosTheta(0)
{

}

inline jaMatrix2::jaMatrix2(jaFloat radians)
{
	sinTheta = GILSIN(radians);
	cosTheta = GILCOS(radians);
}

inline jaMatrix2::jaMatrix2(jaFloat sinTheta, jaFloat cosTheta) : sinTheta(sinTheta), cosTheta(cosTheta)
{

}

inline jaMatrix2::jaMatrix2(const jaMatrix2& matrix)
{
	memcpy(this,&matrix,sizeof(jaMatrix2));
}


inline jaFloat jaMatrix2::getRadians() const
{
	//return GILACOS(cosTheta);
	//gilFloat e = gilVectormath2::arcTan(cosTheta,sinTheta);
	//gilFloat f = GILATAN(sinTheta,cosTheta);
	return GILATAN(sinTheta,cosTheta);
	//return gilVectormath2::arcTan(sinTheta,cosTheta);
}

inline jaFloat jaMatrix2::getDegrees() const
{
	return jaVectormath2::radiansToDegrees(getRadians());
}

#pragma warning( disable : 4244)
inline void jaMatrix2::setDegrees(jaFloat degrees)
{
	setRadians((degrees * ja::math::PI / GIL_REAL(180.0)));
}
#pragma warning( default : 4244)

inline void jaMatrix2::setRadians(jaFloat radians)
{
	sinTheta = GILSIN(radians);
	cosTheta = GILCOS(radians);
}

inline void jaMatrix2::loadIdentity()
{
	sinTheta = GIL_ZERO;
	cosTheta = GIL_ONE;
}

/*float32 GetAngle() const
{
	return b2Atan2(col1.y, col1.x);
}

inline b2Vec2 b2Mul(const b2Mat22& A, const b2Vec2& v)
{
	return b2Vec2(A.col1.x * v.x + A.col2.x * v.y, A.col1.y * v.x + A.col2.y * v.y);
}

explicit b2Mat22(float32 angle)
{
	// TODO_ERIN compute sin+cos together.
	float32 c = cosf(angle), s = sinf(angle);
	col1.x = c; col2.x = -s;
	col1.y = s; col2.y = c;
}

// A * B
inline b2Mat22 b2Mul(const b2Mat22& A, const b2Mat22& B)
{
	return b2Mat22(b2Mul(A, B.col1), b2Mul(A, B.col2));
}*/

inline jaVector2 jaMatrix2::operator*(const jaVector2& v) const
{
	return jaVector2(v.x * cosTheta - v.y * sinTheta, v.x * sinTheta + v.y * cosTheta);
}

inline jaMatrix2 jaMatrix2::operator*(const jaMatrix2& m) const
{
	return jaMatrix2(sinTheta * m.cosTheta + cosTheta *  m.sinTheta, cosTheta * m.cosTheta - sinTheta * m.sinTheta);
}

inline jaMatrix2 jaMatrix2::operator/(const jaMatrix2& m) const
{
	return jaMatrix2(sinTheta * m.cosTheta - cosTheta * m.sinTheta, cosTheta * m.cosTheta + sinTheta * m.sinTheta);
}

namespace jaVectormath2
{
	inline jaMatrix2 inverse(const jaMatrix2& matrix)
	{
		//return gilMatrix2(-GILACOS(matrix.cosTheta)); // nie dzia�a ca�kowicie
		//return gilMatrix2(-GILATAN(matrix.sinTheta,matrix.cosTheta)); // za wolne
		return jaMatrix2(-matrix.sinTheta, matrix.cosTheta);
	}

	inline jaMatrix2 rotate(const jaMatrix2& matrix, jaFloat radians)
	{
		//return gilMatrix2(matrix.getRadians() + radians);
		jaFloat sinTheta = GILSIN(radians);
		jaFloat cosTheta = GILCOS(radians);
		return jaMatrix2(matrix.sinTheta * cosTheta + matrix.cosTheta * sinTheta, matrix.cosTheta * cosTheta - matrix.sinTheta * sinTheta);
	}

	inline void      rotate(const jaMatrix2& matrix, const jaVector2& inVector, jaVector2& outVector)
	{
		outVector.x = inVector.x * matrix.cosTheta - inVector.y * matrix.sinTheta;
		outVector.y = inVector.x * matrix.sinTheta + inVector.y * matrix.cosTheta;
	}

	inline jaMatrix2 transpose(const jaMatrix2& matrix)
	{
		return jaMatrix2(-matrix.sinTheta, matrix.cosTheta);
	}

}
