#pragma once

#include "../jaSetup.hpp"
#include "Math.hpp"
//#include "MathHelper.hpp"
#include <math.h>
#include <assert.h>
#include <iostream>
#include <string.h>
//#pragma warning disable C4244

class jaVector2;
class jaMatrix2;
class jaMatrix22;
class jaTransform2;
union jaVector4;
union jaMatrix44;

class jaVector2
{
public:

	jaFloat x,y;

	inline jaVector2();
	inline jaVector2(jaFloat x, jaFloat y);
	inline void addScaledVector(const jaVector2& a, jaFloat scale);
	inline void scaleVector(const jaFloat scale);
	inline jaVector2(const jaVector2& v);
	inline bool       operator==(const jaVector2& a);
	inline jaVector2& operator+=(const jaVector2& a);
	inline jaVector2& operator-=(const jaVector2& a);
	inline jaVector2& operator*=(const jaVector2& a);
	inline jaVector2& operator*=(const jaFloat& a);
	inline jaVector2 operator+(const jaVector2& a) const;
	inline jaVector2 operator-(const jaVector2& a) const;
	inline jaVector2 operator*(const jaVector2& a) const;
	inline const jaVector2 operator*(jaFloat a) const;
	inline const jaVector2 operator/(jaFloat a) const;
	
	//inline gilVector2 operator*(const gilFloat a);
	inline jaVector2 operator-() const;
	inline jaFloat normalize();
	inline void setZero();
	inline bool equalsZero();
};

class jaMatrix2
{
public:
	jaFloat cosTheta;
	jaFloat sinTheta;

	inline jaMatrix2();
	inline jaMatrix2(jaFloat radians);
	inline jaMatrix2(jaFloat sinTheta, jaFloat cosTheta);
	inline jaMatrix2(const jaMatrix2& matrix);
	inline jaFloat getRadians() const;
	inline jaFloat getDegrees() const;
	inline void setDegrees(jaFloat degrees);
	inline void setRadians(jaFloat radians);
	inline void loadIdentity();
	inline jaVector2 operator*(const jaVector2& v) const;
	//adds angle of two matrices
	inline jaMatrix2 operator*(const jaMatrix2& m) const;
	//subbs angle of two matrices
	inline jaMatrix2 operator/(const jaMatrix2& m) const;
};

class jaMatrix22
{
public:
	jaVector2 col1, col2;

	inline jaMatrix22();
	inline jaMatrix22(const jaVector2& col1, const jaVector2& col2);
	inline jaMatrix22(jaFloat radians);

	inline jaMatrix22 operator + (const jaMatrix22& b) const;
	inline jaMatrix22 operator * (const jaMatrix22& b) const;
	inline jaVector2  operator * (const jaVector2& v) const;
};

class jaTransform2
{
private:
	
public:
	jaVector2 position;
	jaMatrix2 rotationMatrix;

	inline jaTransform2();
	inline jaTransform2(const jaVector2& position, jaFloat radians);
	inline jaTransform2(const jaVector2& position, const jaMatrix2& matrix);
	inline jaTransform2(const jaTransform2& transform);
	inline jaMatrix2 getRotationMatrix() const;
	inline jaVector2 getPosition() const;
	inline void setPosition(const jaVector2& postion);
	inline void setPosition(jaFloat x, jaFloat y);
	inline void setTranslation(const jaVector2& translation);
	inline void setRotationMatrix(const jaMatrix2& rotationMatrix);
	inline jaVector2 operator*(const jaVector2& v) const;
};

__declspec(align(16)) union jaVector4
{
	struct
	{
		float x;
		float y;
		float z;
		float w;
	};

	__m128 column0;
};

__declspec(align(32)) union jaMatrix44
{
	float  element[16];
	__m128 column[4];

	struct
	{
		jaVector4 vec0;
		jaVector4 vec1;
		jaVector4 vec2;
		jaVector4 vec3;
	};

	struct
	{
		__m128 column0;
		__m128 column1;
		__m128 column2;
		__m128 column3;
	};

	struct
	{
		__m256 column01;
		__m256 column23;
	};
	
};

inline const jaVector2 operator*(jaFloat scalar, const jaVector2& v);

namespace jaVectormath2
{
	// Returns angle of vector
	// @param a Vector to return angle beetwen X axis
	// @return Angle in radians
	inline jaFloat getAngle(const jaVector2& a);
	// Returns angle beetwen two vectors
	// @return Angle in radians
	inline jaFloat getAngle(const jaVector2& a, const jaVector2& b);
	inline bool equals(const jaVector2& a, const jaVector2& b);
	inline jaFloat length(const jaVector2& a);
	inline jaFloat lengthSqr(const jaVector2& a);
	inline jaVector2 normalize(const jaVector2& a);
	inline jaVector2 perpendicular(const jaVector2& a);
	inline jaVector2 perpendicular(const jaVector2& a, jaFloat s);
	inline jaVector2 perpendicular(jaFloat s, const jaVector2& a);
	inline bool      isLineIntersecting(const jaVector2& pointA0, const jaVector2& pointA1, const jaVector2& pointB0, const jaVector2& pointB1, jaVector2& intersectionPoint);
	inline jaMatrix2  transpose(const jaMatrix2& matrix);
	inline jaMatrix22 transpose(const jaMatrix22& matrix);
	
	inline void setRotMatrix44(const jaMatrix2& rotMatrix, jaMatrix44& matrix);
	inline void setRotMatrix44_SSE(const jaMatrix2& rotMatrix, jaMatrix44& matrix);
	inline void setTranslateMatrix44(const jaVector2& translate, jaMatrix44& matrix);
	//inline void setTranslateMatrix44_SSE(const jaVector2& translate, jaMatrix44& matrix); too slow
	//inline void setTranslateMatrix44_AVX(const jaVector2& translate, jaMatrix44& matrix); too slow
	inline void setScaleMatrix44(const jaVector2& scale, jaMatrix44& matrix);
	inline void setRotScaleMatrix44(const jaMatrix2& rotMatrix, const jaVector2& scale, jaMatrix44& matrix);
	inline void setTranslateRotScaleMatrix44(const jaVector2& translation, const jaMatrix2& rotMatrix, const jaVector2& scale, jaMatrix44& matrix);
	inline void setWorldAndRotateMatrix44_SSE(const jaVector2& translation, const jaMatrix2& rotMatrix, const jaVector2& scale, jaMatrix44& worldMatrix, jaMatrix44& rotateMatrix);
	inline void setOrthoFrustumMatrix44(float left, float right, float bottom, float top, float nearZ, float farZ, jaMatrix44& matrix);
	inline void setIdentityMatrix44(jaMatrix44& matrix);
	inline void mulMatrix44_SSE(const jaMatrix44& matrix, const jaVector4& vector, jaVector4& vectorOut);
	//inline void mulMatrix44_AVX(const jaMatrix44& matrix, const jaVector4& vector, jaVector4& vectorOut); // too slow
	//inline void mulMatrix44_AVX(const jaMatrix44& matrix, const jaVector4& vector, jaVector4& vectorOut);
	inline void setIdentityMatrix44_SSE(jaMatrix44& matrix);
	inline void setIdentityMatrix44_AVX(jaMatrix44& matrix);
	inline void mulMatrix44_SSE(const jaMatrix44& matrixA, const jaMatrix44& matrixB, jaMatrix44& matrixOut);
	inline void mulMatrix44_SSE(const jaMatrix44* modelMatrices, const jaMatrix44* viewProjectionMatrix, jaMatrix44* mvpMatrices, const uint32_t matricesNum);
	inline void copyVector2WithTranslation(jaVector2* const dst, const jaVector2* const src, const jaVector2& translateVec, const uint32_t vectorsNum);
	inline void copyVector2WithTranslation_SSE(jaVector2* const dst, const jaVector2* const src, const jaVector2& translateVec, const uint32_t vectorsNum);
	//inline void mulMatrix44_AVX(const jaMatrix44& matrixA, const jaMatrix44& matrixB, jaMatrix44& matrixOut); // too slow
	inline void morphDataStream_SSE(const __m128* startMorphData, const __m128* endMorphData, float morphValue, const uint32_t dataNum, __m128* morhedData);

	inline jaFloat det(const jaVector2& a, const jaVector2& b);
	inline jaMatrix2 inverse(const jaMatrix2& matrix);
	inline jaMatrix22 inverse(const jaMatrix22& matrix);
	inline jaFloat projection(const jaVector2& a, const jaVector2& b);
	inline jaMatrix2    rotate(const jaMatrix2& matrix, jaFloat radians);
	inline void         rotate(const jaMatrix2& matrix, const jaVector2& inVector, jaVector2& outVector);
	inline jaTransform2 rotate(const jaTransform2& transform, jaFloat radians);
	inline jaVector2    lerp(const jaVector2& a, const jaVector2& b, jaFloat t);

	inline jaFloat min( jaFloat a, jaFloat b)
	{
		if(a < b) return a;
		return b;
	}

	inline jaFloat max( jaFloat a, jaFloat b)
	{
		if(a > b) return a;
		return b;
	}

	inline jaFloat clamp(jaFloat a, jaFloat low, jaFloat high)
	{
		return jaVectormath2::max(low, jaVectormath2::min(a, high));
	}

	inline jaFloat degreesToRadians(jaFloat degrees)
	{
		return (jaFloat)(degrees * ja::math::PI / GIL_REAL(180.0));
	}

	inline jaFloat radiansToDegrees(jaFloat radians)
	{
		return (jaFloat)(radians * GIL_REAL(180.0) / ja::math::PI);
	}

	inline jaFloat arcTan(jaFloat x, jaFloat y)
	{
		jaFloat ax = GILFABS(x);
		jaFloat ay = GILFABS(y);
		if(x > GIL_REAL(0.0))
		{
			if(y > GIL_REAL(0.0))
			{
				if(ax < ay) //1
					return GIL_REAL(1.0) + (GIL_REAL(1.0)-(ax/ay));
				else // 2
					return ay/ax;
			}
			else
			{
				if(ax > ay) // 3
					return GIL_REAL(7.0) + (GIL_REAL(1.0)-(ay/ax));
				else // 4
					return GIL_REAL(6.0) + (ax/ay);
			}
		}
		else
		{
			if(y < GIL_REAL(0.0))
			{
				if(ax<ay) // 5
					return GIL_REAL(5.0) + (GIL_REAL(1.0)-(ax/ay));
				else // 6
					return GIL_REAL(4.0) + (ay/ax);
			}
			else
			{
				if(ax>ay) // 7
					return GIL_REAL(3.0) + (GIL_REAL(1.0)-(ay/ax));
				else // 8
					return GIL_REAL(2.0) + (ax/ay);

			}
		}
	}
}

#include "Vector2.hpp"
#include "Matrix2.hpp"
#include "Matrix22.hpp"
#include "Matrix44.hpp"
#include "Transform2.hpp"
#include "MorphSOA.hpp"
