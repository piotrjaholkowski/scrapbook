#pragma once

namespace jaVectormath2
{

inline void morphDataStream_SSE(const __m128* startMorphData, const __m128* endMorphData, float morphValue, const uint32_t dataNum, __m128* morhedData)
{
	//pCurrentVertex[i].r = pVerticesStart[i].r + ((pVerticesEnd[i].r - pVerticesStart[i].r) * morph);
	
	const uint16_t itNum = (uint16_t)dataNum; //dataNum / 2;

	__m128 morphBroadcast = _mm_set_ps1(morphValue);

	for (uint16_t i = 0; i < itNum; ++i)
	{
		__m128 temp0 = _mm_sub_ps(endMorphData[i], startMorphData[i]);
		//__m128 temp1    = _mm_sub_ps(startMorphData[i+1], endMorphData[i+1]);
		__m128 mul0 = _mm_mul_ps(temp0, morphBroadcast);
		//__m128 mul1     = _mm_mul_ps(temp1, morphBroadcast);
		morhedData[i] = _mm_add_ps(startMorphData[i], mul0);
		//morhedData[i+1] = _mm_add_ps(startMorphData[i+1], mul1);
	}

	//if (isOdd)
	//{
	//	//one more calculation
	//}
}

}

