#pragma once

#include "../jaSetup.hpp"
#include <math.h>
#include <float.h>
#include <xmmintrin.h>
#include <emmintrin.h>

namespace ja
{
	namespace math
	{
		const float  PI = 3.14159265359f;
		const float  PI_OVER_2 = 1.570796327f;
		const float  EPSILON = 0.0001f;
		const float  MIN_POSITIVE_FLOAT = FLT_MIN;
		const double GOLDEN_RATIO = 1.618033987; // Golden ratio fibonacci magic number	

		/*inline float fastAbs(float f)
		{
			int32_t i = ((*(int32_t*)&f) & 0x7fffffff);
			return (*(float*)&i);
		}*/


		inline float fastSin(float x)
		{
			const float B = 4.0f / math::PI;
			const float C = -4.0f / (math::PI*math::PI);

			return B * x + C * x * GILFABS(x);

#ifdef EXTRA_PRECISION
			const float P = 0.225;

			y = P * (y * GILFABS(y) - y) + y;   
#endif
												
		}
		inline float invSqrt(float number)
		{
			long i;
			float x2, y;
			const float threehalfs = 1.5f;

			x2 = number * 0.5f;
			y = number;
			i = *(long *)&y;                       // evil floating point bit level hacking
			i = 0x5f3759df - (i >> 1);               // what the f***?
			y = *(float *)&i;
			y = y * (threehalfs - (x2 * y * y));   // 1st iteration
			//      y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

			return y;
		}

		inline uint64_t getFibonacci(int32_t sequence)
		{
			double dSequence = (double)(sequence);

			double qn  = pow(GOLDEN_RATIO, sequence);
			double kqn = pow(-GOLDEN_RATIO, -sequence);
			return (uint64_t)((qn - kqn) / 2.2360679774); // (qn - kqn) / root(5,2);
		}

		inline uint32_t convertToHash(const char* name)
		{
			const char* c = name;

			uint32_t hashTag = 0;
			uint8_t  i       = 1;
			
			while (*c)
			{
				hashTag += *c * i;
				++c;
				++i;
			}

			return hashTag;
		}

		inline uint32_t	rand()
		{
			static int32_t rnd_a = 12345678;
			static int32_t rnd_b = 12393455;

			rnd_a  = rnd_a ^ 0x10010100;
			rnd_a  = (rnd_a << 1) | ((rnd_a >> 31) & 1);
			rnd_a ^= rnd_b;
			rnd_b  = rnd_b * 255 + 32769;
			uint32_t return_value = rnd_a;

			return return_value;
		}

		// return random number in the range 0 .. a-1 
		inline uint32_t rand(uint32_t a)
		{
			return rand() % a;
		}

		// return random number in the range 0 .. f
		inline float randf(float f = 1.0f)
		{
			return f*(float)rand() / 4294967296.0f;
		}

		inline float randf(float f1, float f2)
		{
			return f1 + (f2 - f1)*(float)rand() / 4294967296.0f;
		}

		inline uint32_t getNextPowerOf2(uint32_t value)
		{
			value--;
			value |= value >> 1;
			value |= value >> 2;
			value |= value >> 4;
			value |= value >> 8;
			value |= value >> 16;
			value++;

			return value;
		}

		
	}
}
