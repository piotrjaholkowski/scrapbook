#pragma once

inline jaMatrix22::jaMatrix22()
{

}

inline jaMatrix22::jaMatrix22(jaFloat radians)
{
	jaFloat c = GILCOS(radians), s = GILSIN(radians);
	col1.x = c; col2.x = -s;
	col1.y = s; col2.y = c;
}

inline jaMatrix22::jaMatrix22(const jaVector2& col1, const jaVector2& col2) : col1(col1), col2(col2)
{

}

inline jaMatrix22 jaMatrix22::operator + (const jaMatrix22& b) const
{
	return jaMatrix22(col1 + b.col1, col2 + b.col2);
}

inline jaVector2 jaMatrix22::operator * (const jaVector2& v) const
{
	return jaVector2(col1.x * v.x + col2.x * v.y, col1.y * v.x + col2.y * v.y);
}

inline jaMatrix22 jaMatrix22::operator * (const jaMatrix22& b) const
{
	return jaMatrix22(*this * b.col1, *this * b.col2);
}

namespace jaVectormath2
{
	inline jaMatrix22 inverse(const jaMatrix22& matrix)
	{
		//for rotation matrix inverse is transpose
		jaFloat a = matrix.col1.x, b = matrix.col2.x, c = matrix.col1.y, d = matrix.col2.y;
		jaMatrix22 B;
		jaFloat det = a * d - b * c;
		assert(det != GIL_ZERO);
		det = GIL_ONE / det;
		B.col1.x =  det * d;	B.col2.x = -det * b;
		B.col1.y = -det * c;	B.col2.y =  det * a;
		return B;

		/*gilFloat a = matrix.col1.x, b = matrix.col2.x, c = matrix.col1.y, d = matrix.col2.y;
		gilMatrix22 B;
		gilFloat det = matrix.col1.x * matrix.col2.y - matrix.col2.x * matrix.col1.y;
		assert(det != GIL_ZERO);
		det = GIL_ONE / det;
		B.col1.x =  det * matrix.col2.y;	B.col2.x = -det * matrix.col2.x;
		B.col1.y = -det * matrix.col1.y;	B.col2.y =  det * matrix.col1.x;
		return B;
		return gilMatrix22();*/
	}

	inline jaMatrix22 transpose(const jaMatrix22& matrix)
	{
		return jaMatrix22(jaVector2(matrix.col1.x, matrix.col2.x), jaVector2(matrix.col1.y, matrix.col2.y));
	}
}