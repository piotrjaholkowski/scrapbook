#pragma once

inline jaVector2::jaVector2() : x(0), y(0)
{

}

inline jaVector2::jaVector2(jaFloat x, jaFloat y) : x(x), y(y)
{

}

inline void jaVector2::addScaledVector(const jaVector2& a, jaFloat scale)
{
	x += a.x * scale;
	y += a.y * scale;
}

inline void jaVector2::scaleVector(const jaFloat scale)
{
	x = x * scale;
	y = y * scale;
}

inline jaVector2::jaVector2(const jaVector2& v)
{
	//memcpy(this,&v,sizeof(gilVector2));
	x = v.x;
	y = v.y;
}

inline bool jaVector2::operator==(const jaVector2& a)
{
	return ((this->x == a.x) && (this->y == a.y));
}

inline jaVector2& jaVector2::operator+=(const jaVector2& a)
{
	x += a.x;
	y += a.y;
	return *this;
}

inline jaVector2& jaVector2::operator-=(const jaVector2& a)
{
	x -= a.x;
	y -= a.y;
	return *this;
}

inline jaVector2& jaVector2::operator*=(const jaVector2& a)
{
	x *= a.x;
	y *= a.y;
	return *this;
}

inline jaVector2& jaVector2::operator*=(const jaFloat& a)
{
	x *= a;
	y *= a;
	return *this;
}

inline jaVector2 jaVector2::operator+(const jaVector2& a) const
{
	return jaVector2(x + a.x, y + a.y);
}

inline jaVector2 jaVector2::operator-(const jaVector2& a) const
{
	return jaVector2(x - a.x, y - a.y);
}

inline jaVector2 jaVector2::operator*(const jaVector2& a) const
{
	return jaVector2(x * a.x, y * a.y);
}

inline const jaVector2 jaVector2::operator*(jaFloat a) const
{
	return jaVector2(a * x, a * y);
}

inline const jaVector2 jaVector2::operator/(jaFloat scalar) const
{
	return jaVector2(x/scalar,y/scalar);
}

inline jaVector2 jaVector2::operator-() const
{
	return jaVector2(-x,-y);
}

inline const jaVector2 operator*(jaFloat scalar, const jaVector2& v)
{
	return jaVector2(scalar * v.x, scalar * v.y);
}

inline jaFloat jaVector2::normalize()
{
	jaFloat len = GILSQRT(x * x + y * y);
	if(len == GIL_ZERO)
		return len;
	x/=len;
	y/=len;
	return len;
}

inline void jaVector2::setZero()
{
	x = GIL_REAL(0.0);
	y = GIL_REAL(0.0);
}

inline bool jaVector2::equalsZero()
{
	return (x == GIL_REAL(0.0)) && (y == GIL_REAL(0.0));
}



namespace jaVectormath2
{
#pragma warning ( disable : 4244 )
	inline jaFloat getAngle(const jaVector2& a)
	{
		//return (GILATAN(a.y,a.x) + (2*GILPI));
		return GILATAN(a.y,a.x);
	}

	inline jaFloat getAngle(const jaVector2& a, const jaVector2& b)
	{
		return (GILATAN(a.y,a.x) - GILATAN(b.y,b.x));// + (2*GILPI));
	}
#pragma warning ( default : 4244 )

	inline bool equals(const jaVector2& a, const jaVector2& b)
	{
		return (a.x == b.x) && (a.y == b.y);
	}

	inline jaFloat length(const jaVector2& a)
	{
		return GILSQRT(a.x * a.x + a.y * a.y);
	}

	inline jaFloat lengthSqr(const jaVector2& a)
	{
		return (a.x * a.x + a.y * a.y);
	}

	inline jaFloat det(const jaVector2& a, const jaVector2& b)
	{
		return (a.x * b.y - a.y * b.x);
	}

	inline jaFloat projection(const jaVector2& a, const jaVector2& b)
	{
		return ((a.x * b.x) + (a.y * b.y));
	}

	inline jaVector2 perpendicular(const jaVector2& a)
	{
		return jaVector2(-a.y,a.x); // normal on the left of vector a
	}

	inline jaVector2 perpendicular(const jaVector2& a, jaFloat s)
	{
		return jaVector2(s * a.y, -s * a.x);
	}

	inline jaVector2 perpendicular(jaFloat s, const jaVector2& a)
	{
		return jaVector2(-s * a.y, s * a.x);
	}

	inline bool isLineIntersecting(const jaVector2& pointA0, const jaVector2& pointA1, const jaVector2& pointB0, const jaVector2& pointB1, jaVector2& intersectionPoint)
	{
		jaFloat s1_x, s1_y, s2_x, s2_y;
		s1_x = pointA1.x - pointA0.x; // s1_x = p1_x - p0_x;
		s1_y = pointA1.y - pointA0.y; // s1_y = p1_y - p0_y;
		s2_x = pointB1.x - pointB0.x; // s2_x = p3_x - p2_x; 
		s2_y = pointB1.y - pointB0.y; // s2_y = p3_y - p2_y;

		//jaFloat s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
		jaFloat s = (-s1_y * (pointA0.x - pointB0.x) + s1_x * (pointA0.y - pointB0.y)) / (-s2_x * s1_y + s1_x * s2_y);
		//jaFloat t = (s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);
		jaFloat t = ( s2_x * (pointA0.y - pointB0.y) - s2_y * (pointA0.x - pointB0.x)) / (-s2_x * s1_y + s1_x * s2_y);

		//if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
		if (s > 0.0f && s < 1.0f && t > 0.0f && t < 1.0f)
		{
			// Collision detected
			intersectionPoint.x = pointA0.x + (t * s1_x);
			intersectionPoint.y = pointA0.y + (t * s1_y);
			return true;
		}

		return false; // No collision
	}

	inline jaVector2 normalize(const jaVector2& a)
	{
		jaFloat len =length(a);
		return jaVector2(a.x / len, a.y / len);
	}

	inline jaVector2 lerp(const jaVector2& a, const jaVector2& b, jaFloat t)
	{
		return ( ( (GIL_REAL(1.0) - t) * a) + (t * b) );
	}

	inline void copyVector2WithTranslation(jaVector2* const dst, const jaVector2* const src, const jaVector2& translateVec, const uint32_t vectorsNum)
	{
		{
			register const uint32_t itNum = vectorsNum;
			for (register uint32_t i = 0; i < itNum; ++i)
			{
				dst[i] = src[i] + translateVec;
			}
		}	
	}

	inline void copyVector2WithTranslation_SSE(jaVector2* const dst, const jaVector2* const src, const jaVector2& translateVec, const uint32_t vectorsNum)
	{
		{
			register const uint32_t itNum = vectorsNum;
			for (register uint32_t i = 0; i < itNum; ++i)
			{
				dst[i] = src[i] + translateVec;
			}
		}
	}
}
