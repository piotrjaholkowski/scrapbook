#pragma once

inline jaTransform2::jaTransform2()
{

}

inline jaTransform2::jaTransform2(const jaVector2& position, jaFloat radians) : position(position), rotationMatrix(radians)
{

}

inline jaTransform2::jaTransform2(const jaVector2& position, const jaMatrix2& matrix) : position(position), rotationMatrix(matrix)
{

}

inline jaTransform2::jaTransform2(const jaTransform2& transform)
{
	memcpy(this, &transform, sizeof(jaTransform2));
	//position = transform.position;
	//rotationMatrix = transform.rotationMatrix;
}

inline jaMatrix2 jaTransform2::getRotationMatrix() const
{
	return rotationMatrix;
}

inline void jaTransform2::setPosition(const jaVector2& postion)
{
	this->position = position;
}

inline void jaTransform2::setPosition(jaFloat x, jaFloat y)
{
	position.x = x;
	position.y = y;
}

inline jaVector2 jaTransform2::getPosition() const
{
	return position;
}

inline void jaTransform2::setTranslation(const jaVector2& translation)
{
	position += translation;
}

inline void jaTransform2::setRotationMatrix(const jaMatrix2& rotationMatrix)
{
	this->rotationMatrix = rotationMatrix;
}

inline jaVector2 jaTransform2::operator*(const jaVector2& v) const
{
	return (rotationMatrix * v) + position;
	//return (rotationMatrix * (v+position));
	//return //rotationMatrix * v;
}

namespace jaVectormath2
{
	inline jaTransform2 rotate(const jaTransform2& transform, jaFloat radians)
	{
		return jaTransform2(transform.getPosition(), rotate(transform.getRotationMatrix(), radians));
	}
}
