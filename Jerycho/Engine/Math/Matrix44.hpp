#pragma once

namespace jaVectormath2
{

	const static jaMatrix44 identityMatrix = { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f };

	inline void setRotMatrix44(const jaMatrix2& rotMatrix, jaMatrix44& matrix)
	{
		matrix.element[0] = rotMatrix.cosTheta;
		matrix.element[1] = rotMatrix.sinTheta;
		matrix.element[2] = GIL_ZERO;
		matrix.element[3] = GIL_ZERO;

		matrix.element[4] = -rotMatrix.sinTheta;
		matrix.element[5] = rotMatrix.cosTheta;
		matrix.element[6] = GIL_ZERO;
		matrix.element[7] = GIL_ZERO;

		matrix.element[8]  = GIL_ZERO;
		matrix.element[9]  = GIL_ZERO;
		matrix.element[10] = GIL_ONE;
		matrix.element[11] = GIL_ZERO;

		matrix.element[12] = GIL_ZERO;
		matrix.element[13] = GIL_ZERO;;
		matrix.element[14] = GIL_ZERO;
		matrix.element[15] = GIL_ONE;
	}

	inline void setRotMatrix44_SSE(const jaMatrix2& rotMatrix, jaMatrix44& matrix)
	{
		matrix.column0 = _mm_set_ps(0.0f, 0.0f, rotMatrix.sinTheta, rotMatrix.cosTheta); // it can be changed to use shuffle
		matrix.column1 = _mm_set_ps(0.0f, 0.0f, rotMatrix.cosTheta, -rotMatrix.sinTheta);
		matrix.column2 = _mm_set_ps(0.0f, 1.0f, 0.0f, 0.0f);
		matrix.column3 = _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
	}

	inline void setTranslateMatrix44(const jaVector2& translate, jaMatrix44& matrix)
	{
		matrix.element[0] = GIL_ONE;
		matrix.element[1] = GIL_ZERO;
		matrix.element[2] = GIL_ZERO;
		matrix.element[3] = GIL_ZERO;

		matrix.element[4] = GIL_ZERO;
		matrix.element[5] = GIL_ONE;
		matrix.element[6] = GIL_ZERO;
		matrix.element[7] = GIL_ZERO;

		matrix.element[8] = GIL_ZERO;
		matrix.element[9] = GIL_ZERO;
		matrix.element[10] = GIL_ONE;
		matrix.element[11] = GIL_ZERO;

		matrix.element[12] = translate.x;
		matrix.element[13] = translate.y;
		matrix.element[14] = GIL_ZERO;;
		matrix.element[15] = GIL_ONE;
	}

	/*inline void setTranslateMatrix44_SSE(const jaVector2& translate, jaMatrix44& matrix)
	{
		matrix.column0 = _mm_set_ps(0.0f, 0.0f, 0.0f, 1.0f); // check it
		//matrix.column1 = _mm_shuffle_ps(matrix.column0, matrix.column0, _MM_SHUFFLE(0, 0, 3, 0));// slower
		//matrix.column2 = _mm_shuffle_ps(matrix.column0, matrix.column0, _MM_SHUFFLE(0, 3, 0, 0));// slower
		matrix.column1 = _mm_set_ps(0.0f, 0.0f, 1.0f, 0.0f);
		matrix.column2 = _mm_set_ps(0.0f, 1.0f, 0.0f, 0.0f);
		matrix.column3 = _mm_set_ps(1.0f, 0.0f, translate.y, translate.x);
	}*/

	/*inline void setTranslateMatrix44_AVX(const jaVector2& translate, jaMatrix44& matrix)
	{
		matrix.column01 = _mm256_set_ps(0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
		matrix.column23 = _mm256_set_ps(1.0f, 0.0f, translate.y, translate.x, 0.0f, 1.0f, 0.0f, 0.0f);
	}*/

	inline void setScaleMatrix44(const jaVector2& scale, jaMatrix44& matrix)
	{
		matrix.element[0] = scale.x;
		matrix.element[1] = GIL_ZERO;
		matrix.element[2] = GIL_ZERO;
		matrix.element[3] = GIL_ZERO;

		matrix.element[4] = GIL_ZERO;
		matrix.element[5] = scale.y;
		matrix.element[6] = GIL_ZERO;
		matrix.element[7] = GIL_ZERO;

		matrix.element[8] = GIL_ZERO;
		matrix.element[9] = GIL_ZERO;
		matrix.element[10] = GIL_ONE;
		matrix.element[11] = GIL_ZERO;

		matrix.element[12] = GIL_ZERO;
		matrix.element[13] = GIL_ZERO;;
		matrix.element[14] = GIL_ZERO;
		matrix.element[15] = GIL_ONE;
	}

	inline void setRotScaleMatrix44(const jaMatrix2& rotMatrix, const jaVector2& scale, jaMatrix44& matrix)
	{
		matrix.element[0] = rotMatrix.cosTheta * scale.x;
		matrix.element[1] = rotMatrix.sinTheta * scale.x;
		matrix.element[2] = GIL_ZERO;
		matrix.element[3] = GIL_ZERO;

		matrix.element[4] = -rotMatrix.sinTheta * scale.y;
		matrix.element[5] = rotMatrix.cosTheta  * scale.y;
		matrix.element[6] = GIL_ZERO;
		matrix.element[7] = GIL_ZERO;

		matrix.element[8]  = GIL_ZERO;
		matrix.element[9]  = GIL_ZERO;
		matrix.element[10] = GIL_ONE;
		matrix.element[11] = GIL_ZERO;

		matrix.element[12] = GIL_ZERO;
		matrix.element[13] = GIL_ZERO;;
		matrix.element[14] = GIL_ZERO;
		matrix.element[15] = GIL_ONE;
	}

	inline void setTranslateRotScaleMatrix44(const jaVector2& translation, const jaMatrix2& rotMatrix, const jaVector2& scale, jaMatrix44& matrix)
	{
		matrix.element[0] = rotMatrix.cosTheta * scale.x;
		matrix.element[1] = rotMatrix.sinTheta * scale.x;
		matrix.element[2] = GIL_ZERO;
		matrix.element[3] = GIL_ZERO;

		matrix.element[4] = -rotMatrix.sinTheta * scale.y;
		matrix.element[5] = rotMatrix.cosTheta  * scale.y;
		matrix.element[6] = GIL_ZERO;
		matrix.element[7] = GIL_ZERO;

		matrix.element[8]  = GIL_ZERO;
		matrix.element[9]  = GIL_ZERO;
		matrix.element[10] = GIL_ONE;
		matrix.element[11] = GIL_ZERO;

		matrix.element[12] = translation.x;
		matrix.element[13] = translation.y;
		matrix.element[14] = GIL_ZERO;
		matrix.element[15] = GIL_ONE;
	}

	inline void setWorldAndRotateMatrix44_SSE(const jaVector2& translation, const jaMatrix2& rotMatrix, const jaVector2& scale, jaMatrix44& worldMatrix, jaMatrix44& rotateMatrix)
	{
		//__m128 scale = _mm_set_ps(scale.x, scale.y, -scale.y, scale.y);

		worldMatrix.element[0] = rotMatrix.cosTheta * scale.x;
		worldMatrix.element[1] = rotMatrix.sinTheta * scale.x;
		worldMatrix.element[2] = GIL_ZERO;
		worldMatrix.element[3] = GIL_ZERO;

		worldMatrix.element[4] = -rotMatrix.sinTheta * scale.y;
		worldMatrix.element[5] = rotMatrix.cosTheta  * scale.y;
		worldMatrix.element[6] = GIL_ZERO;
		worldMatrix.element[7] = GIL_ZERO;

		worldMatrix.element[8] = GIL_ZERO;
		worldMatrix.element[9] = GIL_ZERO;
		worldMatrix.element[10] = GIL_ONE;
		worldMatrix.element[11] = GIL_ZERO;
	
		worldMatrix.element[12] = translation.x;
		worldMatrix.element[13] = translation.y;
		worldMatrix.element[14] = GIL_ZERO;
		worldMatrix.element[15] = GIL_ONE;

		///////////////////////////////////////////////////

		rotateMatrix.element[0] = rotMatrix.cosTheta;
		rotateMatrix.element[1] = rotMatrix.sinTheta;
		rotateMatrix.element[2] = GIL_ZERO;
		rotateMatrix.element[3] = GIL_ZERO;
	
		rotateMatrix.element[4] = -rotMatrix.sinTheta;
		rotateMatrix.element[5] = rotMatrix.cosTheta;
		rotateMatrix.element[6] = GIL_ZERO;
		rotateMatrix.element[7] = GIL_ZERO;
	
		rotateMatrix.element[8] = GIL_ZERO;
		rotateMatrix.element[9] = GIL_ZERO;
		rotateMatrix.element[10] = GIL_ONE;
		rotateMatrix.element[11] = GIL_ZERO;

		rotateMatrix.element[12] = GIL_ZERO;
		rotateMatrix.element[13] = GIL_ZERO;;
		rotateMatrix.element[14] = GIL_ZERO;
		rotateMatrix.element[15] = GIL_ONE;	
	}

	inline void setOrthoFrustumMatrix44(float left, float right, float bottom, float top, float nearZ, float farZ, jaMatrix44& matrix)
	{
		//float farSubNear   = far - near;
		const float topSubBottom = top - bottom;
		const float rightSubLeft = right - left;
		const float farSubNear = farZ - nearZ;
		
		matrix.element[0] = 2.0f / rightSubLeft;
		matrix.element[1] = 0.0f;
		matrix.element[2] = 0.0f;
		matrix.element[3] = 0.0f;
		
		matrix.element[4] = 0.0f;
		matrix.element[5] = 2.0f / topSubBottom;
		matrix.element[6] = 0.0f;
		matrix.element[7] = 0.0f;
		
		matrix.element[8] = 0.0f;
		matrix.element[9] = 0.0f;
		matrix.element[10] = -2.0f / farSubNear;
		matrix.element[11] = 0.0f;
		
		matrix.element[12] = -(right + left) / rightSubLeft;
		matrix.element[13] = -(top + bottom) / topSubBottom;
		matrix.element[14] = -(farZ + nearZ) / farSubNear;
		matrix.element[15] = 1.0f;
	}	

	inline void setIdentityMatrix44(jaMatrix44& matrix)
	{
		matrix = identityMatrix;
	}

	inline void setIdentityMatrix44_SSE(jaMatrix44& matrix)
	{
		//check it
		matrix.column0 = _mm_set_ps(0.0f, 0.0f, 0.0f, 1.0f);
		//matrix.column1 = _mm_shuffle_ps(matrix.column0, matrix.column0, _MM_SHUFFLE(3, 3, 0, 3)); //_mm_set_ps(0.0f, 0.0f, 1.0f, 0.0f); // slower
		//matrix.column2 = _mm_shuffle_ps(matrix.column0, matrix.column0, _MM_SHUFFLE(3, 0, 3, 3)); //_mm_set_ps(0.0f, 1.0f, 0.0f, 0.0f);
		//matrix.column3 = _mm_shuffle_ps(matrix.column0, matrix.column0, _MM_SHUFFLE(0, 3, 3, 3)); //_mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
		matrix.column1 = _mm_set_ps(0.0f, 0.0f, 1.0f, 0.0f);
		matrix.column2 = _mm_set_ps(0.0f, 1.0f, 0.0f, 0.0f);
		matrix.column3 = _mm_set_ps(1.0f, 0.0f, 0.0f, 0.0f);
	}

	inline void setIdentityMatrix44_AVX(jaMatrix44& matrix)
	{
		matrix.column01 = _mm256_set_ps(0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
		matrix.column23 = _mm256_set_ps(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
	}

	inline void mulMatrix44_SSE(const jaMatrix44& matrix, const jaVector4& vector, jaVector4& vectorOut)
	{
		vectorOut.column0 =
			_mm_add_ps(
			_mm_add_ps(_mm_mul_ps(matrix.column0, _mm_shuffle_ps(vector.column0, vector.column0, _MM_SHUFFLE(0, 0, 0, 0))), _mm_mul_ps(matrix.column1, _mm_shuffle_ps(vector.column0, vector.column0, _MM_SHUFFLE(1, 1, 1, 1)))),
			_mm_add_ps(_mm_mul_ps(matrix.column2, _mm_shuffle_ps(vector.column0, vector.column0, _MM_SHUFFLE(2, 2, 2, 2))), _mm_mul_ps(matrix.column3, _mm_shuffle_ps(vector.column0, vector.column0, _MM_SHUFFLE(3, 3, 3, 3)))));
	}

	/*inline void mulMatrix44_AVX(const jaMatrix44& matrix, const jaVector4& vector, jaVector4& vectorOut)
	{
		__m256 add0 = _mm256_add_ps(_mm256_mul_ps(matrix.column01, _mm256_set_m128(_mm_shuffle_ps(vector.column0, vector.column0, _MM_SHUFFLE(0, 0, 0, 0)), _mm_shuffle_ps(vector.column0, vector.column0, _MM_SHUFFLE(1, 1, 1, 1)))), _mm256_mul_ps(matrix.column23, _mm256_set_m128(_mm_shuffle_ps(vector.column0, vector.column0, _MM_SHUFFLE(2, 2, 2, 2)), _mm_shuffle_ps(vector.column0, vector.column0, _MM_SHUFFLE(3, 3, 3, 3)))));
		vectorOut.column0 = _mm_add_ps(_mm256_extractf128_ps(add0, 0), _mm256_extractf128_ps(add0, 1));
	}*/

	inline void mulMatrix44_SSE(const jaMatrix44& matrixA, const jaMatrix44& matrixB, jaMatrix44& matrixOut)
	{
		mulMatrix44_SSE(matrixA, matrixB.vec0, matrixOut.vec0);
		mulMatrix44_SSE(matrixA, matrixB.vec1, matrixOut.vec1);
		mulMatrix44_SSE(matrixA, matrixB.vec2, matrixOut.vec2);
		mulMatrix44_SSE(matrixA, matrixB.vec3, matrixOut.vec3);
	}

	inline void mulMatrix44_SSE(const jaMatrix44& viewProjectionMatrix, const jaMatrix44* modelMatrices, jaMatrix44* mvpMatrices, const uint32_t matricesNum)
	{
		for (uint32_t i = 0; i < matricesNum; ++i)
		{
			mulMatrix44_SSE(viewProjectionMatrix, modelMatrices[i].vec0, mvpMatrices[i].vec0);
			mulMatrix44_SSE(viewProjectionMatrix, modelMatrices[i].vec1, mvpMatrices[i].vec1);
			mulMatrix44_SSE(viewProjectionMatrix, modelMatrices[i].vec2, mvpMatrices[i].vec2);
			mulMatrix44_SSE(viewProjectionMatrix, modelMatrices[i].vec3, mvpMatrices[i].vec3);
		}
	}

	/*inline void mulMatrix44_AVX(const jaMatrix44& matrixA, const jaMatrix44& matrixB, jaMatrix44& matrixOut)
	{
		mulMatrix44_AVX(matrixA, matrixB.vec0, matrixOut.vec0);
		mulMatrix44_AVX(matrixA, matrixB.vec1, matrixOut.vec1);
		mulMatrix44_AVX(matrixA, matrixB.vec2, matrixOut.vec2);
		mulMatrix44_AVX(matrixA, matrixB.vec3, matrixOut.vec3);
	}*/
}