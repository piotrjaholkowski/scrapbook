#include "../jaSetup.hpp"

namespace ja
{

	class LinearMotor2d : public Constraint2d
	{
	public:
		jaVector2 anchorPoint;
		jaVector2 normal;
		jaVector2 transformedNormal;
		jaVector2 transformedAnchorPoint;
		jaFloat maxLinearVelocity;
		jaFloat acceleration;
		jaFloat relaxationBias;
	public:

		LinearMotor2d(PhysicObject2d* physicObject, const jaVector2& localAnchorPoint, const jaVector2& normal, jaFloat maxLinearImpulse) : Constraint2d(physicObject, physicObject, JA_LINEAR_MOTOR2D)
		{
			this->markAsPernament();

			setConstraint(localAnchorPoint, normal, maxLinearImpulse);
			//constraintAllocator = customAllocator;
			//relaxationBias = JA_CONTACT2D_RELAXATION_BIAS; // Amount of error correction during one simulation step default 0.2f
		}

		static Constraint2d* allocateConstraint(CacheLineAllocator* cacheLineAllocator, ConstraintDef2d* constraintDef)
		{
			Constraint2d* constraint = new(cacheLineAllocator->allocate(sizeof(LinearMotor2d))) LinearMotor2d(constraintDef->objectA, constraintDef->anchorPointA, constraintDef->impulseNormal, constraintDef->maxLinearImpulse);
			return constraint;
		}

		void markAndDeleteOldData()
		{

		}

		~LinearMotor2d()
		{
			removeData();
		}

		inline void removeData()
		{
			unmarkPernament();
		}

		void setConstraint(const jaVector2& anchorPoint, const jaVector2&normal, jaFloat maxLinearImpulse)
		{
			//gilTransform2* tA = &objectA->shapeHandle->transform;
			//gilTransform2* tB = &objectB->shapeHandle->transform;
			this->anchorPoint = anchorPoint;
			this->normal = normal;
			this->relaxationBias = 0;

			//jAcc.x = GIL_ZERO;
			//jAcc.y = GIL_ZERO;


			//localAnchorA = gilVectormath2::inverse(tA->rotationMatrix) * (anchorPoint - tA->position);
			//localAnchorB = gilVectormath2::inverse(tB->rotationMatrix) * (anchorPoint - tB->position);
		}

		void setConstraint(ConstraintDef2d* constraintDef)
		{
			setConstraint(constraintDef->anchorPointA, constraintDef->impulseNormal, constraintDef->maxLinearImpulse);
		}

		void preStep(jaFloat invDeltaTime)
		{
			jaTransform2* tA = objectA->getShapeHandle()->getTransformP();
			transformedNormal = tA->rotationMatrix * normal;
			transformedAnchorPoint = *tA * anchorPoint;

			/*jaRigidBody2d* bodyA = (jaRigidBody2d*)objectA;
			jaRigidBody2d* bodyB = (jaRigidBody2d*)objectB;

			gilTransform2* tA = bodyA->shapeHandle->getTransformP();
			gilTransform2* tB = bodyB->shapeHandle->getTransformP();

			// Pre-compute anchors, mass matrix, and bias.
			rA = tA->rotationMatrix * localAnchorA;
			rB = tB->rotationMatrix * localAnchorB;

			// deltaV = deltaV0 + K * impulse
			// invM = [(1/m1 + 1/m2) * eye(2) - skew(r1) * invI1 * skew(r1) - skew(r2) * invI2 * skew(r2)]
			//      = [1/m1+1/m2     0    ] + invI1 * [r1.y*r1.y -r1.x*r1.y] + invI2 * [r1.y*r1.y -r1.x*r1.y]
			//        [    0     1/m1+1/m2]           [-r1.x*r1.y r1.x*r1.x]           [-r1.x*r1.y r1.x*r1.x]
			gilMatrix22 K1;
			K1.col1.x = bodyA->invMass + bodyB->invMass;	K1.col2.x = GIL_ZERO;
			K1.col1.y = GIL_ZERO;							K1.col2.y = bodyA->invMass + bodyB->invMass;

			gilMatrix22 K2;
			K2.col1.x =  bodyA->invInteria * rA.y * rA.y;		K2.col2.x = -bodyA->invInteria * rA.x * rA.y;
			K2.col1.y = -bodyA->invInteria * rA.x * rA.y;		K2.col2.y =  bodyA->invInteria * rA.x * rA.x;

			gilMatrix22 K3;
			K3.col1.x =  bodyB->invInteria * rB.y * rB.y;		K3.col2.x = -bodyB->invInteria * rB.x * rB.y;
			K3.col1.y = -bodyB->invInteria * rB.x * rB.y;		K3.col2.y =  bodyB->invInteria * rB.x * rB.x;

			gilMatrix22 K = K1 + K2 + K3;
			K.col1.x += softness;
			K.col2.y += softness;

			M = gilVectormath2::inverse(K);

			gilVector2 p1 = tA->position + rA;
			gilVector2 p2 = tB->position + rB;
			gilVector2 dp = p2 - p1;

			//Position correction
			bias = -relaxationBias * invDeltaTime * dp;		*/
		}

		void applyCachedImpulse()
		{
			/*jaRigidBody2d* bodyA = (jaRigidBody2d*)objectA;
			jaRigidBody2d* bodyB = (jaRigidBody2d*)objectB;

			// Apply accumulated impulse.
			bodyA->linearVelocity -= bodyA->invMass * jAcc;
			bodyA->angularVelocity -= bodyA->invInteria * gilVectormath2::det(rA, jAcc);

			bodyB->linearVelocity += bodyB->invMass * jAcc;
			bodyB->angularVelocity += bodyB->invInteria * gilVectormath2::det(rB, jAcc);*/

		}

		bool resolveVelocity()
		{
			/*jaRigidBody2d* bodyA = (jaRigidBody2d*)objectA;
			jaRigidBody2d* bodyB = (jaRigidBody2d*)objectB;

			//Relative velocity
			gilVector2 dv = bodyB->linearVelocity + gilVectormath2::perpendicular(bodyB->angularVelocity, rB) - bodyA->linearVelocity - gilVectormath2::perpendicular(bodyA->angularVelocity, rA);

			gilVector2 impulse;

			impulse = M * (bias - dv - softness * jAcc);

			bodyA->linearVelocity -= bodyA->invMass * impulse;
			bodyA->angularVelocity -= bodyA->invInteria * gilVectormath2::det(rA, impulse);

			bodyB->linearVelocity += bodyB->invMass * impulse;
			bodyB->angularVelocity += bodyB->invInteria * gilVectormath2::det(rB, impulse);

			jAcc += impulse;

			if((abs(impulse.x) + abs(impulse.y)) < JA_CONTACT2D_SLEEP_TRESHOLD)
			return false; // constraint satisfied don't need more correction

			return true;*/
			return false;
		}

		bool resolvePosition()
		{
			return true;
		}

		void draw()
		{
			jaVector2 transformedPoint = objectA->shapeHandle->transform * anchorPoint;
			DisplayManager::setColorUb(0, 255, 0);
			DisplayManager::setPointSize(2.75f);
			DisplayManager::drawPoint(transformedPoint.x, transformedPoint.y);
			/*jaRigidBody2d* bodyA = (jaRigidBody2d*)objectA;
			jaRigidBody2d* bodyB = (jaRigidBody2d*)objectB;
			gilTransform2* tA = &bodyA->shapeHandle->transform;
			gilTransform2* tB = &bodyB->shapeHandle->transform;

			gilVector2 x1 = tA->position;
			gilVector2 p1 = x1 + (tA->rotationMatrix * localAnchorA);

			gilVector2 x2 = tB->position;
			gilVector2 p2 = x2 + (tB->rotationMatrix * localAnchorB);

			jaDisplayManager::setPointSize(2.75);
			jaDisplayManager::setColorUb(128,128,190);

			jaDisplayManager::drawLine(x1.x,x1.y,p1.x,p1.y);
			jaDisplayManager::drawLine(x2.x,x2.y,p2.x,p2.y);*/
		}

		uint32_t sizeOf()
		{
			return sizeof(LinearMotor2d);
		}
	};

}