#include "PhysicObjectDef2d.hpp"
#include "../Manager/PhysicsManager.hpp"

//ja::PhysicObjectDef2d::PhysicObjectDef2d(ja::PhysicsManager* physicsManager)

namespace ja
{
	void PhysicObjectDef2d::init(PhysicsManager* physicsManager)
	{
		this->shapeTypeId  = (int8_t)ShapeType::Box;
		this->shape        = &box;
		this->isSleeping   = false;
		this->multiShape   = new (physicsManager->temporaryShapeAllocator->allocate(sizeof(MultiShape2d))) MultiShape2d(physicsManager->temporaryShapeAllocator);
		this->triangleMesh = new (physicsManager->temporaryShapeAllocator->allocate(sizeof(TriangleMesh2d))) TriangleMesh2d(physicsManager->temporaryShapeAllocator);

		//this->density     = 1.0f;
		//this->friction    = 0.2f;
		//this->restitution = 0.0f;

		//box.setDensity(1.0f);
		//box.setFriction(0.2f);
		//box.setRestitution(0.0f);
	}
}
