#include "RigidBody2d.hpp"

using namespace ja;

RigidBody2d::RigidBody2d() : PhysicObject2d((uint8_t)PhysicObjectType::RIGID_BODY), bodyFlag(0)
{
	torque = GIL_ZERO;
}

RigidBody2d::~RigidBody2d()
{
#ifndef _JA_TEST
	//This destructor shouldn't be called
	assert(false);
#endif
}

jaVector2 RigidBody2d::getPosition()
{
	return shapeHandle->transform.position;
}

jaFloat RigidBody2d::getAngleDeg()
{
	return jaVectormath2::radiansToDegrees(shapeHandle->transform.rotationMatrix.getRadians());
}

jaFloat RigidBody2d::getAngle()
{
	return shapeHandle->transform.rotationMatrix.getRadians();
}

jaVector2 RigidBody2d::getScale()
{
	return jaVector2(1,1);
}

void RigidBody2d::setGravity(const jaVector2& gravity)
{
	this->gravity = gravity;
}

jaVector2 RigidBody2d::getGravity()
{
	return gravity;
}

void RigidBody2d::resetMassData()
{
	gil::MassData2d massData;

	shapeHandle->getShape()->getMassData(massData);
	localCenterOfMass = massData.centerOfMass;

	if (bodyType != (uint8_t)BodyType::DYNAMIC_BODY) // static and kinematic
	{
		mass = GIL_ZERO;
		invMass = GIL_ZERO;
	}
	else
	{
		mass    = massData.mass;
		invMass = GIL_ONE / mass;
	}


	if (isFixedRotation() || (bodyType == (uint8_t)BodyType::STATIC_BODY))
	{
		invInteria = GIL_ZERO;
		interia    = GIL_ZERO;
	}
	else
	{
		if (massData.interia == GIL_ZERO)
		{
			interia = GIL_ZERO;
			invInteria = GIL_ZERO;
		}
		else
		{
			//According to Steiner thesis you can calcuate moment of interia around not center of mass point s
			//by Js = J0 + Mr^2 where: 
			//J0 is moment of interia around center of mass
			//M total mass of body
			//r vector from center of mass to s point
			//interia    = massData.interia + (mass * jaVectormath2::projection(massData.centerOfMass, massData.centerOfMass));
			
			interia    = massData.interia;
			invInteria = GIL_ONE / interia;
		}	
	}
}

void RigidBody2d::updateBodyTypeChange()
{

	resetMassData();
}

void RigidBody2d::fillRigidBodyDef( PhysicObjectDef2d* physicObjectDef )
{
	physicObjectDef->allowSleep = isSleepingAllowed();
	physicObjectDef->angluarDamping = angularDamping;
	physicObjectDef->angluarVelocity = angularVelocity;
	physicObjectDef->bodyType = (BodyType)(getBodyType());
	physicObjectDef->categoryMask = getCategoryMask();
	physicObjectDef->copyShape(getShape());
	physicObjectDef->fixedRotation = isFixedRotation();
	physicObjectDef->gravity = gravity;
	physicObjectDef->groupIndex = getGroupIndex();
	physicObjectDef->linearDamping = linearDamping;
	physicObjectDef->linearVelocity = linearVelocity;
	physicObjectDef->maskBits = getMaskBits();
	physicObjectDef->transform  = shapeHandle->transform;
	physicObjectDef->timeFactor = timeFactor;
	physicObjectDef->isSleeping = isSleeping();
}