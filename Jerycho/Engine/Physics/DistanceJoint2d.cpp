#include "DistanceJoint2d.hpp"
#include "../Gilgamesh/ObjectHandle2d.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"

using namespace ja;

DistanceJoint2d::DistanceJoint2d(PhysicObject2d* objectA, PhysicObject2d* objectB, const jaVector2& anchorPointA, const jaVector2& anchorPointB) : Constraint2d(objectA, objectB, JA_DISTANCE_JOINT2D)
{
	markAsPernament();

	if (objectA == this->objectA)
		setConstraint(anchorPointA, anchorPointB);
	else
		setConstraint(anchorPointB, anchorPointA);
}

DistanceJoint2d::~DistanceJoint2d()
{
	removeData();
}

void DistanceJoint2d::setConstraint(const jaVector2& anchorPointA, const jaVector2& anchorPointB)
{
	jaTransform2* tA = &objectA->shapeHandle->transform;
	jaTransform2* tB = &objectB->shapeHandle->transform;

	//this->relaxationBias = relaxationBias;

	jnAcc = GIL_ZERO;

	localAnchorA = jaVectormath2::inverse(tA->rotationMatrix) * (anchorPointA - tA->position);
	localAnchorB = jaVectormath2::inverse(tB->rotationMatrix) * (anchorPointB - tB->position);
	fixedDistance = jaVectormath2::length(anchorPointB - anchorPointA);
}

void DistanceJoint2d::setConstraint(ConstraintDef2d* constraintDef)
{
	setConstraint(constraintDef->anchorPointA, constraintDef->anchorPointB);
}

void DistanceJoint2d::preStep(jaFloat invDeltaTime)
{
	RigidBody2d* bodyA = (RigidBody2d*)(objectA);
	RigidBody2d* bodyB = (RigidBody2d*)(objectB);

	jaTransform2* tA = bodyA->shapeHandle->getTransformP();
	jaTransform2* tB = bodyB->shapeHandle->getTransformP();

	// Pre-compute anchors, mass matrix, and bias.
	rA = tA->rotationMatrix * localAnchorA;
	rB = tB->rotationMatrix * localAnchorB;

	n = (tB->position + rB) - (tA->position + rA);
	distance = n.normalize();
	distance = distance - fixedDistance;

	//this->mass = bodyA->mass + bodyB->mass;
	this->invMassA = bodyA->invMass;
	this->invIA = bodyA->invInteria;
	this->invMassB = bodyB->invMass;
	this->invIB = bodyB->invInteria;

	jaFloat crAu = jaVectormath2::det(rA, n);
	jaFloat crBu = jaVectormath2::det(rB, n);
	jaFloat invNMass = invMassA + (invIA * crAu * crAu) + invMassB + (invIB * crBu * crBu);

	// Compute the effective mass matrix.
	nMass = invNMass != 0.0f ? 1.0f / invNMass : 0.0f;

}

void DistanceJoint2d::applyCachedImpulse()
{
	RigidBody2d* bodyA = (RigidBody2d*)objectA;
	RigidBody2d* bodyB = (RigidBody2d*)objectB;

	jaVector2 accumulatedImpulse = jnAcc * n;
	// Apply accumulated impulse.
	bodyA->linearVelocity -= bodyA->invMass * accumulatedImpulse;
	bodyA->angularVelocity -= bodyA->invInteria * jaVectormath2::det(rA, accumulatedImpulse);

	bodyB->linearVelocity += bodyB->invMass * accumulatedImpulse;
	bodyB->angularVelocity += bodyB->invInteria * jaVectormath2::det(rB, accumulatedImpulse);
}

bool DistanceJoint2d::resolveVelocity()
{
	RigidBody2d* bodyA = (RigidBody2d*)objectA;
	RigidBody2d* bodyB = (RigidBody2d*)objectB;

	jaVector2 vpA = bodyA->linearVelocity + jaVectormath2::perpendicular(bodyA->angularVelocity, rA);
	jaVector2 vpB = bodyB->linearVelocity + jaVectormath2::perpendicular(bodyB->angularVelocity, rB);
	jaFloat   Cdot = jaVectormath2::projection(n, vpB - vpA); 

	//float32 impulse = -m_mass * (Cdot + m_bias + m_gamma * m_impulse);
	jaFloat impulse = -nMass * Cdot;
	jnAcc += impulse;

	jaVector2 P = impulse * n;
	bodyA->linearVelocity  -= invMassA * P;
	bodyA->angularVelocity -= invIA * jaVectormath2::det(rA, P);
	bodyB->linearVelocity  += invMassB * P;
	bodyB->angularVelocity += invIB * jaVectormath2::det(rB, P);

	return abs(impulse) < ja::setup::physics::DISTANCE_JOINT2D_SLEEP_TRESHOLD;
}

bool DistanceJoint2d::resolvePosition()
{
	RigidBody2d* bodyA = static_cast<RigidBody2d*>(objectA);
	RigidBody2d* bodyB = static_cast<RigidBody2d*>(objectB);

	jaTransform2* tA = bodyA->shapeHandle->getTransformP();
	jaTransform2* tB = bodyB->shapeHandle->getTransformP();

	// Pre-compute anchors, mass matrix, and bias.
	rA = tA->rotationMatrix * localAnchorA;
	rB = tB->rotationMatrix * localAnchorB;

	jaVector2 delta = (tB->position + rB) - (tA->position + rA);
	distance = delta.normalize();
	distance = distance - fixedDistance;
	distance = distance * ja::setup::physics::DISTANCE_JOINT2D_RELAXATION_BIAS; //relaxationBias;

	//distance = b2Clamp(distance, -b2_maxLinearCorrection, b2_maxLinearCorrection);

	jaFloat impulse = -this->nMass * distance;
	jaVector2 P = impulse * delta;

	tA->position -= this->invMassA * P;
	jaFloat orientationA = tA->getRotationMatrix().getDegrees();
	orientationA -= this->invIA * jaVectormath2::det(this->rA, P);
	tA->rotationMatrix.setDegrees(orientationA);

	tB->position += this->invMassB * P;
	jaFloat orientationB = tB->getRotationMatrix().getDegrees();
	orientationB += this->invIB * jaVectormath2::det(this->rB, P);
	tB->rotationMatrix.setDegrees(orientationB);

	return abs(distance) < ja::setup::physics::DISTANCE_JOINT2D_SLOPE;
}

void DistanceJoint2d::getWorldAnchorPoints(jaVector2& worldPointA, jaVector2& worldPointB)
{
	RigidBody2d* bodyA = static_cast<RigidBody2d*>(objectA);
	RigidBody2d* bodyB = static_cast<RigidBody2d*>(objectB);

	jaTransform2* tA = bodyA->shapeHandle->getTransformP();
	jaTransform2* tB = bodyB->shapeHandle->getTransformP();

	// Pre-compute anchors, mass matrix, and bias.
	rA = tA->rotationMatrix * localAnchorA;
	rB = tB->rotationMatrix * localAnchorB;

	worldPointA = tA->position + rA;
	worldPointB = tB->position + rB;
}

void DistanceJoint2d::draw()
{
	RigidBody2d* bodyA = (RigidBody2d*)objectA;
	RigidBody2d* bodyB = (RigidBody2d*)objectB;
	jaTransform2* tA = &bodyA->shapeHandle->transform;
	jaTransform2* tB = &bodyB->shapeHandle->transform;

	jaVector2 x1 = tA->position;
	jaVector2 p1 = x1 + (tA->rotationMatrix * localAnchorA);

	jaVector2 x2 = tB->position;
	jaVector2 p2 = x2 + (tB->rotationMatrix * localAnchorB);

	DisplayManager::setPointSize(2.75);
	DisplayManager::setColorUb(128, 128, 190);

	DisplayManager::drawLine(p1.x, p1.y, p2.x, p2.y);
}