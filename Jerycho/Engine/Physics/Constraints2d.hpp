#pragma once

#include <assert.h>
#include "../Allocators/LinearAllocator.hpp"
#include "PhysicObject2d.hpp"

class PhysicsTests;

namespace ja
{
	enum Constraint2dType
	{
		JA_RIGID_BODY_CONTACT2D = 0,
		JA_REVOLUTE_JOINT2D     = 1,
		JA_SPRING_JOINT2D       = 2,
		JA_LINEAR_MOTOR2D       = 3,
		JA_DISTANCE_JOINT2D     = 4
	};

	enum Constraint2dFlag
	{
		JA_MARK_TO_DELETE_CONSTRAINT2D = 1, // Set if contact is not touching contact
		JA_ENDPOINT_CONSTRAINT2D       = 2, // Set if constraint is end-point of constraint island
		JA_PERMANENT_CONSTRAINT2D      = 4 // Set if constraint lives for more than one frame
	};

	class Constraint2d
	{
	protected:
		uint32_t hash; // Don't move that part or you will crash multithreaded part
		uint8_t constraintSynchronizer;
		uint8_t type; // jaConstraint2dType
	private:
		uint8_t constraintFlag;
	protected:
		PhysicObject2d* objectA;
		PhysicObject2d* objectB;
	public:
		Constraint2d* nextA;
		Constraint2d* nextB;
		Constraint2d* previousA;
		Constraint2d* previousB;
	protected:
		//gilFloat relaxationBias;

	public:

		Constraint2d(PhysicObject2d* objectA, PhysicObject2d* objectB, Constraint2dType type) : type(type), constraintFlag(0), nextA(nullptr), nextB(nullptr), previousA(nullptr), previousB(nullptr)
		{
			if (objectB != nullptr)
			{
				if (objectA->objectId > objectB->objectId)
				{
					this->objectA = objectB;
					this->objectB = objectA;
				}
				else
				{
					this->objectA = objectA;
					this->objectB = objectB;
				}

				hash = getHash(this->objectA->objectId, this->objectB->objectId, type);

				if (this->objectA->bodyType != this->objectB->bodyType) // dynamic - static or dynamic - kinematic
				{
					markAsEndpoint();
				}
			}
			else
			{
				this->objectA = objectA;
				this->objectB = objectA;
				hash = getHash(this->objectA->getId(), this->objectA->getId(), type);
				//constraint type static - dynamic flag equal zero
			}
		}


		virtual void removeData() = 0;

		virtual ~Constraint2d()
		{
		
		}

		inline uint8_t getConstraintType()
		{
			return type;
		}

		inline PhysicObject2d* getPhysicObjectA()
		{
			return objectA;
		}

		inline PhysicObject2d* getPhysicObjectB()
		{
			return objectB;
		}

		inline static uint32_t getHash(uint32_t objectAId, uint32_t objectBId, uint8_t constraintType)
		{
			return (objectAId * 73856093 ^ objectBId * 19349663) + constraintType;
		}

		//Return next constraint of object
		inline Constraint2d* getNext(PhysicObject2d* object)
		{
			if (object == objectA)
				return nextA;
			return nextB;
		}

		inline uint16_t getLowerId()
		{
			return objectA->objectId;
		}

		inline uint16_t getHigherId()
		{
			return objectB->objectId;
		}

		inline void markAsPernament()
		{
			constraintFlag |= JA_PERMANENT_CONSTRAINT2D;
		}

		inline void unmarkPernament()
		{
			constraintFlag ^= JA_PERMANENT_CONSTRAINT2D;
		}

		inline void markAsEndpoint()
		{
			constraintFlag |= JA_ENDPOINT_CONSTRAINT2D;
		}

		inline bool isEndpoint()
		{
			return ((constraintFlag & JA_ENDPOINT_CONSTRAINT2D) != 0);
		}

		inline bool isMarkedToDelete()
		{
			return ((constraintFlag & JA_MARK_TO_DELETE_CONSTRAINT2D) != 0);
		}

		inline void markToDelete()
		{
			constraintFlag |= JA_MARK_TO_DELETE_CONSTRAINT2D;
		}

		inline void unmarkToDelete()
		{
			constraintFlag |= JA_MARK_TO_DELETE_CONSTRAINT2D;
			constraintFlag ^= JA_MARK_TO_DELETE_CONSTRAINT2D; // remove delete mark
		}

		inline bool isEqual(uint16_t lowerId, uint16_t higherId, uint8_t type)
		{
			if (lowerId != this->objectA->objectId)
				return false;
			if (higherId != objectB->objectId)
				return false;
			if (type != this->type)
				return false;
			return true;
		}

		inline uint32_t getHash()
		{
			return hash;
		}

		inline void synchronizeConstraint(uint8_t constraintSynchronizer)
		{
			this->constraintSynchronizer    = constraintSynchronizer;
			objectA->constraintSynchronizer = constraintSynchronizer;
			objectB->constraintSynchronizer = constraintSynchronizer;
		}

		inline bool isSynchronized(uint8_t constraintSynchronizer)
		{
			return (this->constraintSynchronizer == constraintSynchronizer);
		}

		inline bool isPermanent()
		{
			return ((constraintFlag & JA_PERMANENT_CONSTRAINT2D) != 0);
		}

		//Makes island union if constraint has two synchronized objects with diffrent islands
		inline ConstraintIsland2d* getIsland(uint8_t constraintSynchronizer);

		virtual void preStep(jaFloat invDeltaTime) = 0;
		virtual void markAndDeleteOldData() = 0;
		virtual void applyCachedImpulse() = 0;
		// If returns false then constraint is satisfied and it doesn't need more impulse correction
		virtual bool resolveVelocity() = 0;
		virtual bool resolvePosition() = 0;
		virtual void draw() = 0;

		virtual uint32_t sizeOf() = 0;
		friend class PhysicsManager;
		friend class ConstraintIsland2d;
		friend class PhysicThreadWorkers;
		friend class PhysicsTests;
	};

	class ConstraintList2d
	{
	public:
		Constraint2d* constraint;

		ConstraintList2d* next;
		ConstraintList2d* previous;

		ConstraintList2d(Constraint2d* constraint) : constraint(constraint), next(nullptr), previous(nullptr)
		{

		}
	};

	class ConstraintIslandCache2d
	{
	public:
		jaFloat  sleepTime;
		uint32_t hash;
		bool     exists;
		bool     isFreeSlot;
	};

	class ConstraintIsland2d
	{
		ConstraintList2d* firstElement;
		ConstraintList2d* lastElement;

		ConstraintIsland2d* next;
		ConstraintIsland2d* previous;

		uint32_t hash;
		uint16_t id;

		bool isRelaxed;
		bool isSleepingAllowed;

		int32_t contactsNum;
		int32_t jointsNum;
		Constraint2d** contacts;
		Constraint2d** joints;
		

		ConstraintIsland2d(uint16_t id) : id(id), hash(0), firstElement(nullptr), lastElement(nullptr), next(nullptr), previous(nullptr), isRelaxed(false), isSleepingAllowed(true), contactsNum(0), jointsNum(0)
		{

		}

		void shockPropagation()
		{

		}

		void addConstraint(CacheLineAllocator* cacheLineAllocator, Constraint2d* constraint)
		{
			constraint->objectA->island = this;
			constraint->objectB->island = this;
			hash += constraint->hash;

			if (lastElement != nullptr)
			{
				lastElement->next = new (cacheLineAllocator->allocate(sizeof(ConstraintList2d))) ConstraintList2d(constraint);
				lastElement->next->previous = lastElement;
				lastElement = lastElement->next;
			}
			else
			{
				firstElement = new (cacheLineAllocator->allocate(sizeof(ConstraintList2d))) ConstraintList2d(constraint);
				lastElement  = firstElement;
			}

			if (constraint->getConstraintType() == JA_RIGID_BODY_CONTACT2D)
				this->contactsNum++;
			else
				this->jointsNum++;
		}

		inline bool isSleepingTresholdBelow()
		{
			for (ConstraintList2d* itConstraint = firstElement; itConstraint != nullptr; itConstraint = itConstraint->next)
			{
				Constraint2d* constraint = itConstraint->constraint;
				if (constraint->objectA == constraint->objectB)
				{
					if (constraint->objectA->isSleepingTresholdBelow() == false)
						return false;
				}
				else
				{
					if (constraint->objectA->isSleepingTresholdBelow() == false)
						return false;

					if (constraint->objectB->isSleepingTresholdBelow() == false)
						return false;
				}
			}

			return true;
		}

		friend class PhysicsManager;
		friend class Constraint2d;
		friend class PhysicThreadWorkers;
		friend class ::PhysicsTests;
	};

	ConstraintIsland2d* Constraint2d::getIsland(uint8_t constraintSynchronizer)
	{
		ConstraintIsland2d* island = nullptr;

		if (isEndpoint())
		{
			if (objectA->bodyType == (uint8_t)BodyType::DYNAMIC_BODY) // A is dynamic
			{
				if (objectA->constraintSynchronizer == constraintSynchronizer)
				{
					island = objectA->island;
				}
			}
			else // B is dynamic
			{
				if (objectB->constraintSynchronizer == constraintSynchronizer)
				{
					island = objectB->island;
				}
			}
		}
		else
		{
			if (objectA->constraintSynchronizer == constraintSynchronizer)
			{
				island = objectA->island;
			}

			if (objectB->constraintSynchronizer == constraintSynchronizer)
			{
				if (objectB->island->id < island->id)
				{
					island = objectB->island;
				}
			}
		}

		return island; // If it returns null it means objectA and objectB is not added to any island
	}

}