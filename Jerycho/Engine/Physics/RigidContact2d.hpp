#pragma once

#include "../jaSetup.hpp"
#include "RigidBody2d.hpp"
#include "PhysicObject2d.hpp"
#include "Contact2d.hpp"
#include "../Allocators/CacheLineAllocator.hpp"
#include "../Gilgamesh/Narrow/Narrow2d.hpp"
#include "../Math/VectorMath2d.hpp"
#include "../Gilgamesh/ObjectHandle2d.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"
#include "../Entity/PhysicGroup.hpp"

namespace ja
{

	class RigidContact2d : public Constraint2d
	{
	private:
		//jaFloat friction;
		jaFloat             relaxationBias;
		CacheLineAllocator* constraintAllocator;
	public:
		Contact2d* contacts;

		RigidContact2d(PhysicObject2d* physicObjectA, PhysicObject2d* physicObjectB, CacheLineAllocator* cacheLineAllocator) : Constraint2d(physicObjectA, physicObjectB, JA_RIGID_BODY_CONTACT2D), contacts(nullptr)
		{
			constraintAllocator = cacheLineAllocator;
			relaxationBias      = setup::physics::CONTACT2D_RELAXATION_BIAS; // Amount of error correction during one simulation step default 0.2f

			//called on begin contact
			{
				if (nullptr != objectA->componentData)
				{
					PhysicComponent* physicComponent = (PhysicComponent*)objectA->componentData;
					physicComponent->callBeginConstraint(this);
				}

				if (nullptr != physicObjectB->componentData)
				{
					PhysicComponent* physicComponent = (PhysicComponent*)objectB->componentData;
					physicComponent->callBeginConstraint(this);
				}
			}
		}

		~RigidContact2d()
		{
			removeData();
		}

		inline void removeData()
		{
			//called on end contact
			{
				if (nullptr != objectA->componentData)
				{
					PhysicComponent* physicComponent = (PhysicComponent*)objectA->componentData;
					physicComponent->callEndConstraint(this);
				}

				if (nullptr != objectB->componentData)
				{
					PhysicComponent* physicComponent = (PhysicComponent*)objectB->componentData;
					physicComponent->callEndConstraint(this);
				}
			}

			for (Contact2d* itContact = contacts; itContact != nullptr; itContact = itContact->next)
			{
				constraintAllocator->free(itContact, sizeof(Contact2d));
			}
		}

		void markAndDeleteOldData()
		{
			Contact2d* previousContact = nullptr;

			for (Contact2d* itC = contacts; itC != nullptr; itC = itC->next)
			{
				--itC->framePersistence;
				if (itC->framePersistence == 0)
				{
					if (contacts == itC)
					{
						contacts = itC->next;
					}
					else
					{
						previousContact->next = itC->next;
					}

					constraintAllocator->free(itC, sizeof(Contact2d));
					continue;
				}

				previousContact = itC;
			}
		}

		void preStep(jaFloat invDeltaTime)
		{
			RigidBody2d* bodyA = (RigidBody2d*)(objectA);
			RigidBody2d* bodyB = (RigidBody2d*)(objectB);

			for (Contact2d* itC = contacts; itC != nullptr; itC = itC->next)
			{

				// Precompute normal mass, tangent mass, and bias.
				jaFloat rn1 = jaVectormath2::projection(itC->rA, itC->n);
				jaFloat rn2 = jaVectormath2::projection(itC->rB, itC->n);
				jaFloat kNormal = bodyA->invMass + bodyB->invMass;
				kNormal += bodyA->invInteria * (jaVectormath2::projection(itC->rA, itC->rA) - rn1 * rn1) + bodyB->invInteria * (jaVectormath2::projection(itC->rB, itC->rB) - rn2 * rn2);
				itC->nMass = GIL_ONE / kNormal;

				jaVector2 tangent = jaVectormath2::perpendicular(itC->n, GIL_ONE);
				jaFloat rt1 = jaVectormath2::projection(itC->rA, tangent);
				jaFloat rt2 = jaVectormath2::projection(itC->rB, tangent);
				jaFloat kTangent = bodyA->invMass + bodyB->invMass;
				kTangent += bodyA->invInteria * (jaVectormath2::projection(itC->rA, itC->rA) - rt1 * rt1) + bodyB->invInteria * (jaVectormath2::projection(itC->rB, itC->rB) - rt2 * rt2);
				itC->tMass = GIL_ONE / kTangent;

				itC->jBias = relaxationBias * invDeltaTime * jaVectormath2::max(GIL_ZERO, itC->depth - setup::physics::CONTACT2D_SKIN_THICKNESS);
				/////////////////////////////////////////////
				//CODE FOR ELASTIC COLLISION

				//Consider that if two bodie were proviously in contact at some
				//contact point two bodies won't bounce off each other.
				//To find if two bodies were at contact at some point check
				//if bias correction impulse is diffrent from 0. If it is 0
				//than body won't bounce

				if (itC->jnAcc == GIL_ZERO) // maybe treat as elastic
				{
					jaFloat restitution = itC->restitution;


					// Relative velocity at contact
					jaVector2 dv = bodyB->linearVelocity + jaVectormath2::perpendicular(bodyB->angularVelocity, itC->rB) - bodyA->linearVelocity - jaVectormath2::perpendicular(bodyA->angularVelocity, itC->rA);

					// Compute normal impulse
					jaFloat vn = jaVectormath2::projection(dv, itC->n);
					//gilFloat test = vn * invDeltaTime;

					if (abs(vn) > setup::physics::CONTACT2D_ELASTIC_ACCELERATION_TRESHOLD) // treat as elastic
					{
						itC->jnElastic = itC->nMass * -vn * restitution;
						itC->jnElastic = jaVectormath2::max(itC->jnElastic, GIL_ZERO);
					}
					else
					{
						itC->jnElastic = GIL_ZERO;
					}
				}
				else
				{
					itC->jnElastic = GIL_ZERO;
				}
				//CODE FOR ELASTIC COLLISION
				/////////////////////////////////////////////
			}
		}

		bool resolveVelocity()
		{
			RigidBody2d* bodyA = static_cast<RigidBody2d*>(objectA);
			RigidBody2d* bodyB = static_cast<RigidBody2d*>(objectB);

			jaFloat dP = GIL_ZERO; // sum of delta impulses (tangent impulse + normal impulse) of all contacts

			for (Contact2d* itC = contacts; itC != nullptr; itC = itC->next)
			{
				// Relative velocity at contact
				jaVector2 dv = bodyB->linearVelocity + jaVectormath2::perpendicular(bodyB->angularVelocity, itC->rB) - bodyA->linearVelocity - jaVectormath2::perpendicular(bodyA->angularVelocity, itC->rA);
				// Compute normal impulse
				jaFloat vn = jaVectormath2::projection(dv, itC->n);
				//gilFloat dPn = itC->nMass * (-vn + itC->jBias); // Here add some position correction bias from prestep
				jaFloat dPn = itC->nMass * (-vn + itC->jBias + itC->jnElastic);

				// Clamp the accumulated impulse
				jaFloat jnAcc0 = itC->jnAcc;
				itC->jnAcc = jaVectormath2::max(jnAcc0 + dPn, GIL_ZERO);
				dPn = itC->jnAcc - jnAcc0;

				// Apply contact impulse
				jaVector2 Pn = dPn * itC->n;

				bodyA->linearVelocity -= bodyA->invMass * Pn;
				bodyA->angularVelocity -= bodyA->invInteria * jaVectormath2::det(itC->rA, Pn);

				bodyB->linearVelocity += bodyB->invMass * Pn;
				bodyB->angularVelocity += bodyB->invInteria * jaVectormath2::det(itC->rB, Pn);

				// Relative velocity at contact
				dv = bodyB->linearVelocity + jaVectormath2::perpendicular(bodyB->angularVelocity, itC->rB) - bodyA->linearVelocity - jaVectormath2::perpendicular(bodyA->angularVelocity, itC->rA);

				jaVector2 tangent = jaVectormath2::perpendicular(itC->n, GIL_ONE);
				jaFloat vt = jaVectormath2::projection(dv, tangent);
				jaFloat dPt = itC->tMass * (-vt);

				// Compute friction impulse
				jaFloat maxPt = itC->friction * itC->jnAcc;

				// Clamp friction accumulated impuse
				jaFloat oldTangentImpulse = itC->jtAcc;
				itC->jtAcc = jaVectormath2::clamp(oldTangentImpulse + dPt, -maxPt, maxPt);
				dPt = itC->jtAcc - oldTangentImpulse;

				// Apply contact impulse
				jaVector2 Pt = dPt * tangent;

				bodyA->linearVelocity -= bodyA->invMass * Pt;
				bodyA->angularVelocity -= bodyA->invInteria * jaVectormath2::det(itC->rA, Pt);

				bodyB->linearVelocity += bodyB->invMass * Pt;
				bodyB->angularVelocity += bodyB->invInteria * jaVectormath2::det(itC->rB, Pt);

				dP += abs(dPn) + abs(dPt);
			}

			if (dP < setup::physics::CONTACT2D_SLEEP_TRESHOLD)
				return false;
			return true;
		}

		bool resolvePosition()
		{
			return true;
		}

		void applyCachedImpulse()
		{
			RigidBody2d* bodyA = (RigidBody2d*)objectA;
			RigidBody2d* bodyB = (RigidBody2d*)objectB;

			for (Contact2d* itContact = contacts; itContact != nullptr; itContact = itContact->next)
			{
				jaVector2 P = itContact->jnAcc * itContact->n + itContact->jtAcc * jaVectormath2::perpendicular(itContact->n, GIL_ONE);

				//Applying cached impulse doesn't have anything in common with elastic collision
				//because when collision is elastic two bodies will separate

				// Remove some velocity by applying correction impulse from previous frame
				bodyA->linearVelocity -= bodyA->invMass * P;
				bodyA->angularVelocity -= bodyA->invInteria * jaVectormath2::det(itContact->rA, P);

				bodyB->linearVelocity += bodyB->invMass * P;
				bodyB->angularVelocity += bodyB->invInteria * jaVectormath2::det(itContact->rB, P);
			}
		}

		void updateContact(gil::Contact2d* contact)
		{
			ja::Contact2d* itContact = contacts;

			for (itContact = contacts; itContact != nullptr; itContact = itContact->next)
			{
				if (contact->hash == itContact->hash)
				{
					itContact->update(contact);

					if (contact->pointNum > 1)
					{
						itContact->next->update(contact);
					}

					return;
				}
			}

		
			ja::Contact2d* newContact = new(constraintAllocator->allocate(sizeof(ja::Contact2d))) ja::Contact2d(contact, 0);

			if (contacts == nullptr)
				contacts = newContact;
			else {
				newContact->next = contacts;
				contacts = newContact;
			}

			if (contact->pointNum > 1)
			{
				newContact = new(constraintAllocator->allocate(sizeof(ja::Contact2d))) ja::Contact2d(contact, 1);
				newContact->next = contacts;
				contacts = newContact;
			}
		}

		/*void addNewContact(gil::Contact2d* contact)
		{
			//RigidBody2d* bodyA = (RigidBody2d*)objectA;
			//RigidBody2d* bodyB = (RigidBody2d*)objectB;
			//jaVector2 bodyACom = bodyA->shapeHandle->transform.rotationMatrix * bodyA->localCenterOfMass;
			//jaVector2 bodyBCom = bodyB->shapeHandle->transform.rotationMatrix * bodyB->localCenterOfMass;

			ja::Contact2d* newContact = new(constraintAllocator->allocate(sizeof(ja::Contact2d))) ja::Contact2d(contact, 0);

			if (contacts == nullptr)
				contacts = newContact;
			else {
				newContact->next = contacts;
				contacts         = newContact;
			}
				
			if (contact->pointNum > 1)
			{
				newContact = new(constraintAllocator->allocate(sizeof(ja::Contact2d))) ja::Contact2d(contact, 1);
				newContact->next = contacts;
				contacts         = newContact;
			}

		}*/

		//inline void updateFriction()
		//{
			//friction = GILSQRT((static_cast<RigidBody2d*>(objectA))->getFriction() * (static_cast<RigidBody2d*>(objectB))->getFriction());
		//}

		inline uint32_t sizeOf()
		{
			return sizeof(RigidContact2d);
		}

		inline void draw()
		{
			DisplayManager::setPointSize(2.75);
			for (Contact2d* itContact = contacts; itContact != nullptr; itContact = itContact->next)
			{
				jaVector2 cA = itContact->rA + objectA->shapeHandle->transform.position;
				jaVector2 cB = itContact->rB + objectB->shapeHandle->transform.position;


				DisplayManager::setColorUb(255, 255, 255);
				DisplayManager::drawLine(cA.x, cA.y, cA.x + itContact->n.x, cA.y + itContact->n.y);

				DisplayManager::setColorUb(0, 0, 255);
				DisplayManager::drawLine(cA.x, cA.y, cA.x + itContact->n.x * itContact->depth, cA.y + itContact->n.y * itContact->depth);

				DisplayManager::setColorUb(255, 128, 64);
				DisplayManager::drawPoint(cA.x, cA.y);
				DisplayManager::drawPoint(cB.x, cB.y);
			}
		}

	};

}