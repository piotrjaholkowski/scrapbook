#pragma once

#include "../jaSetup.hpp"
#include "../Gilgamesh/Narrow/Narrow2d.hpp"

namespace ja
{

	class Contact2d
	{
		jaVector2 n; // normal vector of collision

		// Calculated during preStep phase
		jaVector2 rA; // vector from center of mass of bodyA to collision point on body a 
		// Calculated during preStep phase
		jaVector2 rB; // vector from center of mass of bodyB to collision point on body b 

		jaFloat depth; // penetration depth

		jaFloat nMass; // normal mass
		jaFloat tMass; // tangent mass

		jaFloat jnAcc; // accumulated normal impulse
		jaFloat jtAcc; // accumulated tangent impulse
		jaFloat jBias; // position correction bias
		jaFloat jnElastic; // elastic impulse
		jaFloat restitution;
		jaFloat friction; 

		Contact2d* next;

		// Hash returned by collision detection module
		// If collision detection return always this same hash then jaBodyContact2d
		// has maximum number of one jaContact2d
		uint16_t hash;
		// It is decreased by every simulation step.
		// If is equal to zero then contact is deleted from jaBodyContact2d
		uint8_t framePersistence;

		uint8_t pointIndex;
		// Max 2 points on the bodies which has this same penetration depth
		// This value doesn't change during simulation due to diffrent hash

	public:

		Contact2d(gil::Contact2d* contact, uint8_t pointIndex) : next(nullptr), framePersistence(setup::physics::CONTACT2D_FRAME_PERSISTENCE)
		{
			jnAcc     = GIL_ZERO;
			jtAcc     = GIL_ZERO;
			jBias     = GIL_ZERO;
			jnElastic = GIL_ZERO;
			this->pointIndex = pointIndex;
			hash = contact->hash;

			n = contact->normal;
			rA = contact->rA[pointIndex];
			rB = contact->rB[pointIndex];
			depth = contact->depth;

			Shape2d* shapeA = contact->objectA->shape;
			Shape2d* shapeB = contact->objectB->shape;
			friction = GILSQRT(shapeA->getFriction() * shapeB->getFriction());
			float restitutionA = shapeA->getRestitution();
			float restitutionB = shapeB->getRestitution();

			if (restitutionA > restitutionB)
			{
				restitution = restitutionA;
			}
			else
			{
				restitution = restitutionB;
			}
		}

		void update(gil::Contact2d* contact)
		{
			n     = contact->normal;
			rA    = contact->rA[pointIndex];
			rB    = contact->rB[pointIndex];
			depth = contact->depth;
			framePersistence = setup::physics::CONTACT2D_FRAME_PERSISTENCE;

			Shape2d* shapeA = contact->objectA->shape;
			Shape2d* shapeB = contact->objectB->shape;
			friction = GILSQRT(shapeA->getFriction() * shapeB->getFriction());
			float restitutionA = shapeA->getRestitution();
			float restitutionB = shapeB->getRestitution();

			if (restitutionA > restitutionB)
			{
				restitution = restitutionA;
			}
			else
			{
				restitution = restitutionB;
			}
		}

		friend class RigidContact2d;
	};
}
