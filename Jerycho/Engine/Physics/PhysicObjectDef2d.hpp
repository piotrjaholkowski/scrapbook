#pragma once

#include "Constraints2d.hpp"
#include "../Gilgamesh/Shapes/Box2d.hpp"
#include "../Gilgamesh/Shapes/Circle2d.hpp"
#include "../Gilgamesh/Shapes/ConvexPolygon2d.hpp"
#include "../Gilgamesh/Shapes/TriangleMesh2d.hpp"
#include "../Gilgamesh/Shapes/MultiShape2d.hpp"
#include "../Gilgamesh/Shapes/Point2d.hpp"

using namespace gil;

namespace ja
{
	class PhysicsManager;

	class PhysicObjectDef2d
	{
	public:
		Box2d           box;
		Circle2d        circle;
		ConvexPolygon2d polygon;
		TriangleMesh2d* triangleMesh;
		MultiShape2d*   multiShape;
		Point2d         point;


		BodyType     bodyType;
		jaTransform2 transform;
	
		jaVector2 gravity;
		jaVector2 linearVelocity;
		
		jaFloat   linearDamping;
		jaFloat   angluarDamping;
		jaFloat   timeFactor;
		jaFloat   angluarVelocity;

	private:
		//jaFloat   restitution;
		//jaFloat   friction;
		//jaFloat   density;
	public:	

		uint16_t categoryMask;
		uint16_t maskBits;
		int8_t   groupIndex;
		bool     fixedRotation;
		bool     allowSleep;
		bool     isSleeping;

	private:
		int8_t   shapeTypeId;
		Shape2d* shape;
	public:

		void init(PhysicsManager* physicsManager);

	private:

		PhysicObjectDef2d()
		{
			this->shapeTypeId = (int8_t)ShapeType::Box;
			this->shape       = &box;
			this->isSleeping  = false;

			this->multiShape   = nullptr;
			this->triangleMesh = nullptr;

			this->box.setDensity(1.0f);
			this->box.setFriction(0.2f);
			this->box.setRestitution(0.0f);

			this->circle.setDensity(1.0f);
			this->circle.setFriction(0.2f);
			this->circle.setRestitution(0.0f);

			this->polygon.setDensity(1.0f);
			this->polygon.setFriction(0.2f);
			this->polygon.setRestitution(0.0f);

			this->point.setDensity(1.0f);
			this->point.setFriction(0.2f);
			this->point.setRestitution(0.0f);

			//this->density     = 1.0f;
			//this->friction    = 0.2f;
			//this->restitution = 0.0f;
		}

	public:

		~PhysicObjectDef2d()
		{
			
		}

		void setShapeType(int8_t shapeTypeId)
		{
			this->shapeTypeId = shapeTypeId;

			switch (shapeTypeId)
			{
			case (int8_t)ShapeType::Box:
				shapeTypeId = (int8_t)ShapeType::Box;
				this->shape = &box;
				break;
			case (int8_t)ShapeType::Circle:
				shapeTypeId = (int8_t)ShapeType::Circle;
				this->shape = &circle;
				break;
			case (int8_t)ShapeType::ConvexPolygon:
				shapeTypeId = (int8_t)ShapeType::ConvexPolygon;
				this->shape = &polygon;
				break;
			case (int8_t)ShapeType::Point:
				shapeTypeId = (int8_t)ShapeType::Point;
				this->shape = &point;
				break;
			case (int8_t)ShapeType::MultiShape:
				shapeTypeId = (int8_t)ShapeType::MultiShape;
				this->shape = multiShape;
				break;
			case (int8_t)ShapeType::TriangleMesh:
				shapeTypeId = (int8_t)ShapeType::TriangleMesh;
				this->shape = triangleMesh;
			}

			//this->shape->setDensity(density);
			//this->shape->setFriction(friction);
			//this->shape->setRestitution(restitution);
		}

		void copyShape(Shape2d* shape)
		{
			switch (shape->getType())
			{
			case (uint8_t)ShapeType::Box:
			{
				shapeTypeId = (uint8_t)ShapeType::Box;
				this->box   = *(Box2d*)(shape);
				this->shape = &this->box;
			}
				break;
			case (uint8_t)ShapeType::Circle:
			{
				shapeTypeId  = (uint8_t)ShapeType::Circle;
				this->circle = *(Circle2d*)(shape);
				this->shape  = &this->circle;
			}
				break;
			case (uint8_t)ShapeType::ConvexPolygon:
			{
				shapeTypeId   = (uint8_t)ShapeType::ConvexPolygon;
				this->polygon = *(ConvexPolygon2d*)(shape);
				this->shape   = &this->polygon;
			}
				break;
			case (uint8_t)ShapeType::Point:
			{
				shapeTypeId = (uint8_t)ShapeType::Point;
				this->point = *(Point2d*)(shape);
				this->shape = &this->point;
			}
				break;
			case (uint8_t)ShapeType::MultiShape:
			{
				shapeTypeId = (uint8_t)ShapeType::MultiShape;
				MultiShape2d* multiShapeSrc = (MultiShape2d*)(shape);
				multiShapeSrc->copy(this->multiShape);
				this->shape = this->multiShape;
			}				
				break;	
			case (uint8_t)ShapeType::TriangleMesh:
			{
				shapeTypeId = (uint8_t)ShapeType::TriangleMesh;
				TriangleMesh2d* triangleMeshSrc = (TriangleMesh2d*)(shape);
				triangleMeshSrc->copy(this->triangleMesh);
				this->shape = this->triangleMesh;
			}	
				break;
			
			}

			//this->density     = shape->getDensity();
			//this->friction    = shape->getFriction();
			//this->restitution = shape->getRestitution();
		}

		Shape2d* getShape()
		{
			//this->shape->setDensity(density);
			//this->shape->setFriction(friction);
			//this->shape->setRestitution(restitution);
			return this->shape;
		}

		friend class PhysicsManager;
	};

	class ConstraintDef2d
	{
	public:
		PhysicObject2d*  objectA;
		PhysicObject2d*  objectB;
		jaVector2        anchorPointA;
		jaVector2        anchorPointB;
		jaVector2        impulseNormal;
		jaFloat          maxLinearImpulse;
		jaFloat          relaxationBias;
		jaFloat          softness;
		Constraint2dType constraintType;
	};

}