#pragma once

#include "../jaSetup.hpp"
#include "../Gilgamesh/Shapes/Particles2d.hpp"
#include "RigidBody2d.hpp"

#include "../Allocators/CacheLineAllocator.hpp"

namespace ja
{

	struct ParticleDef2d
	{
		jaVector2 initVelocity;
		jaVector2 initVelocityRandom;
		jaVector2 gravity;
		jaFloat initAngluarVelocity;
		jaFloat initAngluarVelocityRandom;
		jaFloat alphaOverTime;
		jaFloat initAlpha;
		jaFloat initAlphaRandom;
		jaFloat initScale;
		jaFloat initScaleRandom;
		jaFloat scaleOverTime;
		jaFloat initAngleRandom;
		jaFloat particleLinearDamping;
		jaFloat particleAngluarDamping;
		jaFloat particleRestitution;
		jaFloat particleFriction;
		jaFloat additionalBound; // this field is not for definition of particle
		jaFloat additionalBoundToImage;
		jaFloat spawnDelay;
		jaFloat spawnDelayRandom;
		jaFloat initParticleLife;
		jaFloat initParticleLifeRandom;
		jaFloat timeFactor;

		uint16_t maxParticlesNum;
		uint16_t emitterId;
		uint16_t categoryBits;
		uint16_t maskBits;
		int8_t   groupIndex;
		uint8_t  particleGroup; // -1 undefined
		bool     affectedByExplosion;
		bool     removeOnCollide;
		bool     isColliding;
		bool     velocityAffectedByEmmiter;
		bool     velocityAffectedByEmmiterVelocity;
	};

	class ParticleGenerator2d : public PhysicObject2d
	{
	public:
		uint16_t particleGeneratorFlag;
		jaFloat  nextSpawnTime; // set to bellow zero
	private:
		CacheLineAllocator* particleAllocator;
		RigidBody2d* emmiter;
		jaTransform2 emmiterTransform;

		inline void initNewParticle(Particle2d* particle);
	public:
		Particle2d*   particles;
		//PhysicObject2d* particles;
		ParticleDef2d particleDef;

		ParticleGenerator2d(CacheLineAllocator* particlesAllocator);
		~ParticleGenerator2d();

		void clearParticlesLife();

		void setParticleDef(ParticleDef2d& particleDef);
		void setEmmiter(RigidBody2d* body);
		RigidBody2d* getEmitter();
		uint16_t     getActiveParticlesNum();
		void clearEmitter();
		void integrate(jaFloat deltaTime);
		void resolveCollision(uint8_t particleId);

		inline void setCategoryMask(uint16_t& categoryMask);
		inline uint16_t getCategoryMask();
		inline void setMaskBits(uint16_t& maskBits);
		inline uint16_t getMaskBits();
		inline void setGroupIndex(int8_t groupIndex);
		inline int8_t getGroupIndex();

		void updateBodyTypeChange();
		inline void setOrientation(const jaMatrix2& rotMatrix);

		inline bool isSleepingTresholdBelow()
		{
			return false;
		}

		friend class PhysicsManager;
	};

	void ParticleGenerator2d::setCategoryMask(uint16_t& categoryMask)
	{
		shapeHandle->categoryBits = categoryMask;
	}

	uint16_t ParticleGenerator2d::getCategoryMask()
	{
		return shapeHandle->categoryBits;
	}

	void ParticleGenerator2d::setMaskBits(uint16_t& maskBits)
	{
		shapeHandle->maskBits = maskBits;
	}

	uint16_t ParticleGenerator2d::getMaskBits()
	{
		return shapeHandle->maskBits;
	}

	void ParticleGenerator2d::setGroupIndex(int8_t groupIndex)
	{
		shapeHandle->groupIndex = groupIndex;
	}

	int8_t ParticleGenerator2d::getGroupIndex()
	{
		return shapeHandle->groupIndex;
	}

	void ParticleGenerator2d::setOrientation(const jaMatrix2& rotMatrix)
	{

	}

	void ParticleGenerator2d::initNewParticle(Particle2d* particle)
	{
		//calculating life of particle
		if (particleDef.initParticleLifeRandom == GIL_ZERO)
			particle->life = particleDef.initParticleLife;
		else
			particle->life = particleDef.initParticleLife + fmodf((float)rand(), particleDef.initParticleLifeRandom);

		//calculating initial velocity
		if (particleDef.initVelocityRandom.x == GIL_ZERO)
			particle->linearVelocity.x = particleDef.initVelocity.x;
		else
			particle->linearVelocity.x = particleDef.initVelocity.x + fmodf((float)rand(), particleDef.initVelocityRandom.x);

		if (particleDef.initVelocityRandom.y == GIL_ZERO)
			particle->linearVelocity.y = particleDef.initVelocity.y;
		else
			particle->linearVelocity.y = particleDef.initVelocity.y + fmodf((float)rand(), particleDef.initVelocityRandom.y);

		if (particleDef.velocityAffectedByEmmiter)
		{
			particle->linearVelocity = emmiterTransform.rotationMatrix * particle->linearVelocity;
		}

		if (particleDef.initAngluarVelocityRandom == GIL_ZERO)
			particle->angluarVelocity = particleDef.initAngluarVelocity;
		else
			particle->angluarVelocity = particleDef.initAngluarVelocity + fmodf((float)rand(), particleDef.initAngluarVelocityRandom);

		//calculating particle position, scale and rotation	and alpha
		if (particleDef.initAlphaRandom == GIL_ZERO)
			particle->alpha = particleDef.initAlpha;
		else
			particle->alpha = particleDef.initAlpha + fmodf((float)rand(), particleDef.initAlphaRandom);

		if (particleDef.initScaleRandom == GIL_ZERO)
			particle->scale = particleDef.initScale;
		else
			particle->scale = particleDef.initScale + fmodf((float)rand(), particleDef.initScaleRandom);

		if (particleDef.initAngleRandom == GIL_ZERO)
			particle->angle = GIL_ZERO;
		else
			particle->angle = fmodf((float)rand(), particleDef.initAngleRandom);


		emmiter->getShape()->getInnerRandomPoint(particle->position);
		particle->position = emmiterTransform * particle->position;

		if (particleDef.velocityAffectedByEmmiterVelocity)
		{
			jaVector2 T0 = particle->position - emmiter->shapeHandle->transform.position;
			T0 = jaVectormath2::perpendicular(T0);
			particle->linearVelocity += emmiter->linearVelocity + (emmiter->angularVelocity * T0);
		}
	}

}