#pragma once

#include "../Manager/ThreadManager.hpp"
#include "../Physics/RigidContact2d.hpp"

class PhysicThreadsInitializer {
public:
	PhysicThreadsInitializer();
};

namespace PhysicThread {
	
	extern void(*computeForces)(ja::WorkingThread* workingThread);
	extern void(*clearForces)(ja::WorkingThread* workingThread);
	extern void(*integrate)(ja::WorkingThread* workingThread);
	extern void(*preStep)(ja::WorkingThread* workingThread);
	extern void(*solveIslands)(ja::WorkingThread* workingThread);
	extern void(*generateContacts)(ja::WorkingThread* workingThread);
	extern void(*createUpdateAwakeContacts)(ja::WorkingThread* workingThread);
	extern void(*getConstraintsToDelete)(ja::WorkingThread* workingThread);
	extern void(*removeConstraints)(ja::WorkingThread* workingThread);
	//extern void(*generateConstraintIslands)(jaWorkingThread* workingThread);
	extern PhysicThreadsInitializer initFunctions; // This initializes thread function pointers at start of program
};

/*class jaRigidContactUpdateData {
public:
	jaRigidContact2d* constraint;
	gilContact2d* contact;
};*/

enum GenerateContactsOpearationType {
	JA_OBJECT_BODY_TYPE_CHANGE = 0,
	JA_ADD_NEW_CONTACT = 1,
	JA_ADD_NEW_CONSTRAINT = 2
};

/*class jaGenerateContactsData {
public:
	union {
		jaConstraint2d* constraint;
		gilObjectHandle2d* objectHandle;
	};
	
	gilContact2d* contact;
	unsigned char operation;
};*/

namespace ja
{

	class PhysicThreadWorkers {
	private:
	public:

		static void computeForces(WorkingThread* workingThread);
		static void computeForces2(WorkingThread* workingThread);
		static void clearForces(WorkingThread* workingThread);
		static void integrate(WorkingThread* workingThread);
		static void preStep(WorkingThread* workingThread);
		static void solveIslands(WorkingThread* workingThread);
		//static void generateContacts2(jaWorkingThread* workingThread);
		static void generateContacts(WorkingThread* workingThread);
		static void createUpdateAwakeContacts(WorkingThread* workingThread);
		static void getConstraintsToDelete(WorkingThread* workingThread);
		static void removeConstraints(WorkingThread* workingThread);
		//static void getHighestConstraintId(jaWorkingThread* workingThread);
	};

}