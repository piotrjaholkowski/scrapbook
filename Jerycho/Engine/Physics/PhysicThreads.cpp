#include "PhysicThreads.hpp"
#include "RigidBody2d.hpp"
#include "ParticleGenerator2d.hpp"
#include "../Manager/PhysicsManager.hpp"

using namespace ja;

void (*PhysicThread::computeForces)(WorkingThread*) = nullptr;
void (*PhysicThread::clearForces)(WorkingThread*) = nullptr;
void (*PhysicThread::integrate)(WorkingThread*) = nullptr;
void (*PhysicThread::preStep)(WorkingThread*) = nullptr;
void (*PhysicThread::solveIslands)(WorkingThread*) = nullptr;
void (*PhysicThread::generateContacts)(WorkingThread*) = nullptr;
void (*PhysicThread::createUpdateAwakeContacts)(WorkingThread*) = nullptr;
void (*PhysicThread::getConstraintsToDelete)(WorkingThread*) = nullptr;
void (*PhysicThread::removeConstraints)(WorkingThread*) = nullptr;
PhysicThreadsInitializer PhysicThread::initFunctions;

PhysicThreadsInitializer::PhysicThreadsInitializer()
{
	//jaPhysicThread::computeForces = jaPhysicThreadWorkers::computeForces;
	PhysicThread::computeForces = PhysicThreadWorkers::computeForces2;
	PhysicThread::clearForces = PhysicThreadWorkers::clearForces;
	PhysicThread::integrate = PhysicThreadWorkers::integrate;
	PhysicThread::preStep = PhysicThreadWorkers::preStep;
	PhysicThread::solveIslands = PhysicThreadWorkers::solveIslands;
	PhysicThread::generateContacts = PhysicThreadWorkers::generateContacts;
	PhysicThread::createUpdateAwakeContacts = PhysicThreadWorkers::createUpdateAwakeContacts;
	PhysicThread::getConstraintsToDelete = PhysicThreadWorkers::getConstraintsToDelete;
	PhysicThread::removeConstraints = PhysicThreadWorkers::removeConstraints;
	//jaPhysicThread::generateConstraintIslands = jaPhysicThreadWorkers::generateConstraintIslands;
}

void PhysicThreadWorkers::computeForces(WorkingThread* workingThread)
{
	uint32_t threadId   = workingThread->threadId;
	uint32_t threadsNum = workingThread->threadsNum;
	void**   taskArray  = (void**)(workingThread->taskArrayData);
	uint32_t itCounter  = 0;
	uint32_t taskId     = threadId;
	uint32_t taskNum    = workingThread->taskNum;
	
	jaVector2 acceleration;
	jaFloat angluarAcceleration;
	jaFloat damping;

	for (itCounter; taskId < taskNum; ++itCounter)
	{
		taskId = threadId + (threadsNum * itCounter);
		PhysicObject2d* physicObject = (PhysicObject2d*)( *(taskArray + taskId) );

		if (physicObject == nullptr)
			continue;

		switch (physicObject->getObjectType())
		{
		case (uint8_t)PhysicObjectType::RIGID_BODY:
		{
			RigidBody2d* dynamicBody = (RigidBody2d*)(physicObject);

			if (dynamicBody->bodyType == (uint8_t)BodyType::STATIC_BODY)
				continue;

			if (dynamicBody->isSleeping())
				continue;

			dynamicBody->netForce.x += dynamicBody->gravity.x * dynamicBody->mass;
			dynamicBody->netForce.y += dynamicBody->gravity.y * dynamicBody->mass;
			damping = dynamicBody->linearDamping * dynamicBody->timeFactor;
			dynamicBody->linearVelocity.x -= (dynamicBody->linearVelocity.x * damping);
			dynamicBody->linearVelocity.y -= (dynamicBody->linearVelocity.y * damping);
			acceleration.x = dynamicBody->netForce.x * dynamicBody->invMass;
			acceleration.y = dynamicBody->netForce.y * dynamicBody->invMass;
			dynamicBody->linearVelocity.x += acceleration.x;
			dynamicBody->linearVelocity.y += acceleration.y;

			if (!dynamicBody->isFixedRotation()) // for fixed rotation invInteria is 0
			{
				dynamicBody->angularVelocity -= (dynamicBody->angularVelocity * dynamicBody->angularDamping * dynamicBody->timeFactor);
				angluarAcceleration = dynamicBody->torque * dynamicBody->invInteria;
				dynamicBody->angularVelocity += angluarAcceleration;
			}
		}
			break;
		case (uint8_t)PhysicObjectType::DEFORMABLE_BODY:
			break;
		case (uint8_t)PhysicObjectType::PARTICLE_GENERATOR:
			break;
		case (uint8_t)PhysicObjectType::LIQUID:
			break;
		}
	}
}

void PhysicThreadWorkers::computeForces2(WorkingThread* workingThread)
{
	uint32_t threadId = workingThread->threadId;
	uint8_t  threadsNum = workingThread->threadsNum;
	uint8_t  taskSkip = threadId;

	ObjectHandle2d* dynamicHandle = (ObjectHandle2d*)(workingThread->taskArrayData);
	jaVector2 acceleration;
	jaFloat angluarAcceleration;
	jaFloat damping;

	for (dynamicHandle; dynamicHandle != nullptr; dynamicHandle = dynamicHandle->prev)
	{
		if (taskSkip > 0)
		{
			--taskSkip;
			continue;
		}

		taskSkip = threadsNum - 1;

		PhysicObject2d* physicObject = (PhysicObject2d*)(dynamicHandle->getUserData());
		switch (physicObject->getObjectType())
		{
		case (uint8_t)PhysicObjectType::RIGID_BODY:
		{
			RigidBody2d* dynamicBody = (RigidBody2d*)(physicObject);

			if (dynamicBody->isSleeping())
				continue;

			dynamicBody->netForce.x += dynamicBody->gravity.x * dynamicBody->mass;
			dynamicBody->netForce.y += dynamicBody->gravity.y * dynamicBody->mass;
			damping = dynamicBody->linearDamping * dynamicBody->timeFactor;
			dynamicBody->linearVelocity.x -= (dynamicBody->linearVelocity.x * damping);
			dynamicBody->linearVelocity.y -= (dynamicBody->linearVelocity.y * damping);
			acceleration.x = dynamicBody->netForce.x * dynamicBody->invMass;
			acceleration.y = dynamicBody->netForce.y * dynamicBody->invMass;
			dynamicBody->linearVelocity.x += acceleration.x;
			dynamicBody->linearVelocity.y += acceleration.y;

			if (!dynamicBody->isFixedRotation()) // for fixed rotation invInteria is 0
			{
				dynamicBody->angularVelocity -= (dynamicBody->angularVelocity * dynamicBody->angularDamping * dynamicBody->timeFactor);
				angluarAcceleration = dynamicBody->torque * dynamicBody->invInteria;
				dynamicBody->angularVelocity += angluarAcceleration;
			}
		}
			break;
		case (uint8_t)PhysicObjectType::DEFORMABLE_BODY:
			break;
		case (uint8_t)PhysicObjectType::PARTICLE_GENERATOR:
			break;
		case (uint8_t)PhysicObjectType::LIQUID:
			break;
		}
	}
}

void PhysicThreadWorkers::clearForces(WorkingThread* workingThread)
{
	uint32_t        threadId   = workingThread->threadId;
	uint32_t        threadsNum = workingThread->threadsNum;
	ObjectHandle2d* itObject   = (ObjectHandle2d*)(workingThread->taskArrayData);
	uint8_t         taskSkip   = threadId;

	for (itObject; itObject != nullptr; itObject = itObject->prev) {
		if (taskSkip > 0) {
			--taskSkip;
			continue;
		}

		taskSkip = threadsNum - 1;
		PhysicObject2d* physicObject = (PhysicObject2d*)(itObject->getUserData());

		switch (physicObject->getObjectType())
		{
		case (uint8_t)PhysicObjectType::RIGID_BODY:
		{
			RigidBody2d* dynamicBody = (RigidBody2d*)(physicObject);

			dynamicBody->netForce.setZero();
			dynamicBody->torque = GIL_ZERO;
		}
			break;
		case (uint8_t)PhysicObjectType::DEFORMABLE_BODY:
			break;
		case (uint8_t)PhysicObjectType::PARTICLE_GENERATOR:
			break;
		case (uint8_t)PhysicObjectType::LIQUID:
			break;
		}
	}
}

void PhysicThreadWorkers::integrate(WorkingThread* workingThread)
{
	uint32_t        threadId   = workingThread->threadId;
	uint32_t        threadsNum = workingThread->threadsNum;
	ObjectHandle2d* itObject   = (ObjectHandle2d*)(workingThread->taskArrayData);
	uint8_t         taskSkip   = threadId;
	jaFloat         deltaTime  = *(jaFloat*)(workingThread->taskData);

	for (itObject; itObject != nullptr; itObject = itObject->prev) {
		if (taskSkip > 0) {
			--taskSkip;
			continue;
		}

		taskSkip = threadsNum - 1;
		PhysicObject2d* physicObject = (PhysicObject2d*)(itObject->getUserData());

		jaFloat factoredTime = deltaTime * physicObject->timeFactor;
		switch (physicObject->getObjectType())
		{
		case (uint8_t)PhysicObjectType::RIGID_BODY:
		{
			RigidBody2d* dynamicBody = (RigidBody2d*)(physicObject);

			if (!dynamicBody->isFixedRotation())
			{
				dynamicBody->orientation += (factoredTime * dynamicBody->angularVelocity);
				dynamicBody->shapeHandle->transform.rotationMatrix.setRadians(dynamicBody->orientation);
			}
			dynamicBody->shapeHandle->transform.position.addScaledVector(dynamicBody->linearVelocity, factoredTime);
		}
			break;
		case (uint8_t)PhysicObjectType::DEFORMABLE_BODY:
			break;
		case (uint8_t)PhysicObjectType::PARTICLE_GENERATOR:
		{
			ParticleGenerator2d* particleGenerator = (ParticleGenerator2d*)(physicObject);
			particleGenerator->integrate(deltaTime);
		}
			break;
		case (uint8_t)PhysicObjectType::LIQUID:
			break;
		}
	}
}

void PhysicThreadWorkers::preStep(WorkingThread* workingThread)
{
	uint32_t threadId = workingThread->threadId;
	uint32_t threadsNum = workingThread->threadsNum;
	uint32_t taskId = threadId;
	uint8_t  taskSkip = threadId;
	ConstraintIsland2d* constraintIsland = (ConstraintIsland2d*)(workingThread->taskArrayData);
	jaFloat             invDeltaTime     = *(jaFloat*)(workingThread->taskData);

	for (ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
	{
		int32_t contactsNum = itIsland->contactsNum;
		int32_t jointsNum   = itIsland->jointsNum;
		ja::Constraint2d** contacts = itIsland->contacts;
		ja::Constraint2d** joints   = itIsland->joints;

		for (int32_t i = 0; i < contactsNum; ++i)
		{
			if (taskSkip > 0) {
				--taskSkip;
				continue;
			}

			contacts[i]->preStep(invDeltaTime);

			taskSkip = threadsNum - 1;
		}

		for (int32_t i = 0; i < jointsNum; ++i)
		{
			if (taskSkip > 0) {
				--taskSkip;
				continue;
			}

			joints[i]->preStep(invDeltaTime);

			taskSkip = threadsNum - 1;
		}
	}
}

void PhysicThreadWorkers::solveIslands(WorkingThread* workingThread)
{
	uint32_t threadId   = workingThread->threadId;
	uint32_t threadsNum = workingThread->threadsNum;
	uint32_t taskId     = threadId;
	uint8_t  taskSkip   = threadId;
	ConstraintIsland2d* constraintIsland = (ConstraintIsland2d*)(workingThread->taskArrayData);
	jaFloat             invDeltaTime     = *(jaFloat*)(workingThread->taskData);
	PhysicsManager*     physicsManager   = PhysicsManager::getSingleton();

	if (physicsManager->isWarmStart)
	{
		for (ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
		{
			if (taskSkip > 0) {
				--taskSkip;
				continue;
			}

			int32_t contactsNum = itIsland->contactsNum;
			int32_t jointsNum   = itIsland->jointsNum;
			ja::Constraint2d** contacts = itIsland->contacts;
			ja::Constraint2d** joints   = itIsland->joints;

			for (int32_t i = 0; i < contactsNum; ++i)
			{
				contacts[i]->applyCachedImpulse();
			}

			for (int32_t i = 0; i < jointsNum; ++i)
			{
				joints[i]->applyCachedImpulse();
			}

			taskSkip = threadsNum - 1;
		}
	}
	
	taskSkip = threadId;
	for (ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
	{
		if (taskSkip > 0) {
			--taskSkip;
			continue;
		}

		// By checking if whole island is relaxed you can effectively
		// stop applying unecessary impulses and put a group of bodies to sleep
		for (uint32_t i = 0; i < physicsManager->velocityIterations; ++i)
		{
			bool islandSolved = true;
			/*for (ConstraintList2d* itConstraint = itIsland->firstElement; itConstraint != nullptr; itConstraint = itConstraint->next)
			{
				if (itConstraint->constraint->resolveVelocity())
				{
					islandSolved = false;
				}
			}*/

			int32_t            contactsNum = itIsland->contactsNum;
			int32_t            jointsNum   = itIsland->jointsNum;
			ja::Constraint2d** contacts    = itIsland->contacts;
			ja::Constraint2d** joints      = itIsland->joints;

			for (int32_t i = 0; i < contactsNum; ++i)
			{
				if (contacts[i]->resolveVelocity())
					islandSolved = false;
			}

			for (int32_t i = 0; i < jointsNum; ++i)
			{
				if (joints[i]->resolveVelocity())
					islandSolved = false;
			}

			if (islandSolved)
			{
				if (itIsland->isSleepingAllowed)
				{
					if (itIsland->isSleepingTresholdBelow())
						itIsland->isRelaxed = true;
				}

				break;
			}
		}

		taskSkip = threadsNum - 1;
	}
}

void PhysicThreadWorkers::generateContacts(WorkingThread* workingThread)
{/*
	unsigned int threadId = workingThread->threadId;
	unsigned int threadsNum = workingThread->threadsNum;
	unsigned int itCounter = 0;
	unsigned int taskId = threadId;
	gilStackAllocator<gilContact2d>* contactList = static_cast<gilStackAllocator<gilContact2d>*>(workingThread->taskArrayData);
	jaPhysicsManager* physicsManager = static_cast<jaPhysicsManager*>(workingThread->taskData);
	void* constraintDataMem = workingThread->threadAllocatedMem;
	jaGenerateContactsData* constraintData = reinterpret_cast<jaGenerateContactsData*>(static_cast<char*>(constraintDataMem)+sizeof(unsigned int));

	gilContact2d* contact = contactList->getFirst();
	unsigned int bodyContactNum = contactList->getSize();
	unsigned int lowerId;
	unsigned int higherId;
	unsigned int constraintHash;
	unsigned int objectsToWakeNum;
	jaRigidContact2d* bodyContact;
	jaConstraint2d* itConstraint;
	jaPhysicObject2d* physicObjectA;
	jaPhysicObject2d* physicObjectB;
	jaPhysicObject2d** physicObjects = physicsManager->physicObjects;
	jaConstraint2d** constraints = physicsManager->constraints;
	bool solved = false;

	for (unsigned int i = threadId; i < bodyContactNum; i+= threadsNum)
	{
		contact[i].getHandleId(lowerId, higherId);
		physicObjectA = physicObjects[lowerId];
		physicObjectB = physicObjects[higherId];

		if ((physicObjectA->bodyType != JA_DYNAMIC_BODY) && (physicObjectB->bodyType != JA_DYNAMIC_BODY))
		{
			//Don't let make kinematic-static or kinematic-kinematic constraints
			continue;
		}

		if (physicObjectA->isSleeping()) // Kinematic and static bodies can't be put to sleep
		{		
			jaConstraint2d* constraint = constraints[lowerId];
			objectsToWakeNum += physicsManager->getConnectedConstraintsToWakeUpAndChangeObjectType(constraint, &constraintData); //wake up bodies linked to that body
			constraintData = constraintData + objectsToWakeNum;
		}

		if (physicObjectB->isSleeping()) // Kinematic and static bodies can't be put to sleep
		{
			jaConstraint2d* constraint = constraints[higherId];
			objectsToWakeNum += physicsManager->getConnectedConstraintsToWakeUpAndChangeObjectType(constraint, &constraintData);//wake up bodies linked to that body
			constraintData = constraintData + objectsToWakeNum;
		}

		//check for right type of contact deformable body - rigid etc.
		constraintHash = jaConstraint2d::getHash(lowerId, higherId, JA_RIGID_BODY_CONTACT2D);

		if (constraints[lowerId] != NULL)
		{
			itConstraint = constraints[lowerId];

			//Find if contact already exists
			for (itConstraint; itConstraint != NULL; itConstraint = itConstraint->getNext(physicObjectA))
			{
				if (itConstraint->hash == constraintHash)
				{
					// do some high quality check
					if (itConstraint->isEqual(lowerId, higherId, JA_RIGID_BODY_CONTACT2D)) //update contact
					{
						bodyContact = static_cast<jaRigidContact2d*>(itConstraint);
						if (!bodyContact->updateContact(&contact[i])) {
							constraintData->constraint = bodyContact;
							constraintData->contact = &contact[i];
							constraintData->operation = JA_ADD_NEW_CONTACT;
							++constraintData;
						}

						solved = true;
						break; // add next contact
					}
				}
			}

			if (solved == true)
			{
				solved = false;
				continue; // Add next contact
			}
		}

		//(*constraintsToAdd)->constraint = new (constraintAllocator->allocate(sizeof(jaRigidContact2d))) jaRigidContact2d(&contact[i], constraintAllocator);
		constraintData->contact = &contact[i];
		constraintData->operation = JA_ADD_NEW_CONSTRAINT;
		++constraintData;
		//bodyContact->updateFriction();
		//bodyContact->updateContact(contact);
		//addConstraint(bodyContact);
	}

	unsigned int* constraintDataNum = static_cast<unsigned int*>(constraintDataMem);
	*constraintDataNum = (reinterpret_cast<char*>(constraintData)-static_cast<char*>(constraintDataMem)+sizeof(unsigned int)) / sizeof(jaGenerateContactsData);
	*/
}

void PhysicThreadWorkers::createUpdateAwakeContacts(WorkingThread* workingThread)
{

}

void PhysicThreadWorkers::getConstraintsToDelete(WorkingThread* workingThread)
{
	uint32_t threadId = workingThread->threadId;
	uint32_t threadsNum = workingThread->threadsNum;
	uint32_t itCounter = 0;
	uint32_t taskId = threadId;
	uint8_t  taskSkip = threadId;
	ConstraintIsland2d* constraintIsland  = (ConstraintIsland2d*)(workingThread->taskArrayData);
	PhysicsManager*     physicsManager    = (PhysicsManager*)(workingThread->taskData);
	void*               constraintDataMem = workingThread->threadAllocatedMem;
	Constraint2d** constraintData = (Constraint2d**)((char*)(constraintDataMem)+sizeof(uint32_t));

	//Remove old constraints
	for (ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
	{
		ConstraintList2d* constraintElement = itIsland->firstElement;
		for (constraintElement; constraintElement != nullptr; constraintElement = constraintElement->next)
		{
			if (taskSkip > 0) {
				--taskSkip;
				continue;
			}

			taskSkip = threadsNum - 1;
			Constraint2d* constraint = constraintElement->constraint;

			if (constraint->isPermanent())
				continue;

			if (constraint->isMarkedToDelete())
			{
				*constraintData = constraint;
				++constraintData;
				continue;
			}

			constraint->markToDelete();
			constraint->markAndDeleteOldData(); 
		}
	}

	uint32_t* constraintsNum = (uint32_t*)(constraintDataMem);
	*constraintsNum = ( (char*)(constraintData) - (char*)(constraintDataMem) - sizeof(uint32_t) ) / sizeof(Constraint2d*);
}

void PhysicThreadWorkers::removeConstraints(WorkingThread* workingThread)
{
	uint32_t threadId = workingThread->threadId;

	if (threadId > 1)
		return;

	uint32_t threadsNum = workingThread->threadsNum;
	PhysicsManager*     physicsManager      = (PhysicsManager*)(workingThread->taskData);
	WorkingThread*      workingThreads      = ThreadManager::getSingleton()->getThreadArray();
	CacheLineAllocator* constraintAllocator = physicsManager->constraintAllocator;

	if (threadId == 0) {
		for (uint32_t i = 0; i < threadsNum; ++i) {
			uint32_t constraintsToDeleteNum = *(uint32_t*)(workingThreads->threadAllocatedMem);
		
			Constraint2d** constraint = (Constraint2d**)( (int8_t*)(workingThreads->threadAllocatedMem) + sizeof(uint32_t) );

			for (uint32_t z = 0; z < constraintsToDeleteNum; ++z) {
				physicsManager->removeConstraintLinks(*constraint); // remove links
				++constraint;
			}
			++workingThreads;
		}
	}
	else {
		for (uint32_t i = 0; i < threadsNum; ++i) {
			uint32_t constraintsToDeleteNum = *(uint32_t*)(workingThreads->threadAllocatedMem);
			Constraint2d** constraint = (Constraint2d**)((int8_t*)(workingThreads->threadAllocatedMem) + sizeof(uint32_t));

			for (uint32_t z = 0; z < constraintsToDeleteNum; ++z) {
				uint32_t constraintSize = (*constraint)->sizeOf();
				(*constraint)->removeData();
				constraintAllocator->free(*constraint, constraintSize);
				++constraint;
			}
			++workingThreads;
		}
	}
	
}

/*void jaPhysicThreadWorkers::getHighestConstraintId(jaWorkingThread* workingThread)
{
	unsigned int threadId = workingThread->threadId;
	unsigned int threadsNum = workingThread->threadsNum;
	void** taskArray = static_cast<void**>(workingThread->taskArrayData);
	unsigned short* threadAllocatedMem = static_cast<unsigned short*>(workingThread->threadAllocatedMem);
	unsigned int itCounter = 0;
	unsigned int taskId = threadId;// = threadId + (threadsNum * itCounter);
	unsigned int memoryAllocatedByThread = 0;
	unsigned int taskNum = workingThread->taskNum;

	for (itCounter; taskId < taskNum; ++itCounter)
	{
		taskId = threadId + (threadsNum * itCounter);
		jaConstraint2d* constraint = static_cast<jaConstraint2d*>(*(taskArray + taskId));

		if (constraint == NULL)
			continue;

		unsigned short higherId = 0;
		for (constraint; constraint != NULL; higherId = constraint->getHigherId()) {
			constraint = static_cast<jaConstraint2d*>(*(taskArray + higherId));
		}
		*threadAllocatedMem = higherId;
		threadAllocatedMem += 1;
		memoryAllocatedByThread += sizeof(unsigned short);
	}
	workingThread->memoryAllocatedByThread = memoryAllocatedByThread;
}*/

