#include "ParticleGenerator2d.hpp"

namespace ja
{

	ParticleGenerator2d::ParticleGenerator2d(CacheLineAllocator* particlesAllocator) : PhysicObject2d((uint8_t)PhysicObjectType::PARTICLE_GENERATOR)
	{
		this->particleAllocator     = particleAllocator;
		this->particleGeneratorFlag = 0;
		this->timeFactor            = GIL_ONE;
		this->nextSpawnTime         = GIL_ZERO;
		this->emmiter               = nullptr;
		this->particles             = nullptr;
	}

	ParticleGenerator2d::~ParticleGenerator2d()
	{
		if (nullptr != this->particles)
		{
			this->particleAllocator->free(this->particles, particleDef.maxParticlesNum * sizeof(Particle2d));
			this->particles = nullptr;
		}
	}

	void ParticleGenerator2d::clearParticlesLife()
	{
		for (uint16_t i = 0; i < particleDef.maxParticlesNum; i++)
		{
			particles[i].life = -GIL_ONE;
		}
	}

	uint16_t ParticleGenerator2d::getActiveParticlesNum()
	{
		return ((Particles2d*)(shapeHandle->shape))->activeParticles;
	}


	void ParticleGenerator2d::setEmmiter(RigidBody2d* body)
	{
		emmiter = body;
		Particles2d* particles = (Particles2d*)shapeHandle->shape;
		particles->emitterId = body->objectId;
	}

	RigidBody2d* ParticleGenerator2d::getEmitter()
	{
		return emmiter;
	}

	void ParticleGenerator2d::clearEmitter()
	{
		Particles2d* particles = (Particles2d*)shapeHandle->shape;
		particles->emitterId = setup::physics::ID_UNASSIGNED;
		emmiter = nullptr;
	}

	void ParticleGenerator2d::setParticleDef(ParticleDef2d& particleDef)
	{
		if (nullptr != this->particles)
		{
			this->particleAllocator->free(this->particles, this->particleDef.maxParticlesNum * sizeof(gil::Particle2d));
			this->particles = nullptr;
		}

		this->particleDef = particleDef;
		this->particles   = (gil::Particle2d*)this->particleAllocator->allocate(this->particleDef.maxParticlesNum * sizeof(gil::Particle2d));

		gil::Particles2d* particleShape = (gil::Particles2d*)(this->shapeHandle->getShape());

		particleShape->activeParticles = 0;
		particleShape->particles       = this->particles;
	}

	void ParticleGenerator2d::integrate(jaFloat deltaTime)
	{
		jaFloat factoredTime = deltaTime * timeFactor;
		Particle2d* smallBody;

		if (emmiter != nullptr)
		{
			emmiterTransform = emmiter->shapeHandle->transform;
		}
		else
		{
			emmiterTransform.position = shapeHandle->volume.min + (shapeHandle->volume.max - shapeHandle->volume.min) * GIL_REAL(0.5);
		}

		//Calculating particles velocities and position
		for (uint16_t i = 0; i < particleDef.maxParticlesNum; i++)
		{
			smallBody = &particles[i];
			smallBody->linearVelocity += particleDef.gravity;
			smallBody->position += smallBody->linearVelocity * factoredTime;
		}

		if ((particleDef.initAngluarVelocity != GIL_ZERO) || (particleDef.initAngluarVelocityRandom != GIL_ZERO))
			for (uint16_t i = 0; i < particleDef.maxParticlesNum; i++)
			{
				smallBody = &particles[i];
				smallBody->angle += smallBody->angluarVelocity * factoredTime;
			}

		if (particleDef.particleLinearDamping != GIL_ZERO)
		{
			jaFloat damping = particleDef.particleLinearDamping * timeFactor;
			for (uint16_t i = 0; i < particleDef.maxParticlesNum; i++)
			{
				smallBody = &particles[i];
				smallBody->linearVelocity.x -= damping * smallBody->linearVelocity.x;
				smallBody->linearVelocity.y -= damping * smallBody->linearVelocity.x;
			}
		}

		if (particleDef.particleAngluarDamping != GIL_ZERO)
		{
			jaFloat damping = particleDef.particleAngluarDamping * timeFactor;
			for (uint16_t i = 0; i < particleDef.maxParticlesNum; i++)
			{
				smallBody = &particles[i];
				smallBody->angluarVelocity -= damping * smallBody->angluarVelocity;
			}
		}

		if (particleDef.alphaOverTime != GIL_ZERO)
		{
			jaFloat cummulatedAlphaOverTime = particleDef.alphaOverTime * factoredTime;
			for (uint16_t i = 0; i < particleDef.maxParticlesNum; i++)
			{
				particles[i].alpha += cummulatedAlphaOverTime;
			}
		}

		if (particleDef.scaleOverTime != GIL_ZERO)
		{
			jaFloat cummulatedScaleOverTime = particleDef.scaleOverTime * factoredTime;
			for (uint16_t i = 0; i < particleDef.maxParticlesNum; i++)
			{
				particles[i].scale += cummulatedScaleOverTime;
			}
		}

		//Calculating life of particles and creating new
		uint16_t newParticlesNum = 0;
		jaFloat  restSpawnTime = GIL_ZERO;

		nextSpawnTime -= factoredTime;
		if (nextSpawnTime < GIL_ZERO)
			restSpawnTime = nextSpawnTime;

		while (restSpawnTime < GIL_ZERO)
		{
			newParticlesNum++;
			if (particleDef.spawnDelayRandom == GIL_ZERO)
			{
				nextSpawnTime = particleDef.spawnDelay;
			}
			else
			{
				nextSpawnTime = particleDef.spawnDelay + fmodf((float)rand(), particleDef.spawnDelayRandom);
			}

			nextSpawnTime += restSpawnTime;
			restSpawnTime = nextSpawnTime;
		}


		if (particleDef.initParticleLife != 0.0f)
		{
			for (uint16_t i = 0; i < particleDef.maxParticlesNum; i++)
			{
				particles[i].life -= factoredTime;
			}
		}


		uint16_t i = 0;
		if (emmiter != nullptr)
		{
			while (newParticlesNum)
			{
				if (particles[i].life < GIL_ZERO)
				{
					initNewParticle(&particles[i]);
					--newParticlesNum;
				}
				++i;
				if (i > particleDef.maxParticlesNum)
					break;
			}
		}

		shapeHandle->transform = emmiterTransform;
	}

	void ParticleGenerator2d::updateBodyTypeChange()
	{

	}

}