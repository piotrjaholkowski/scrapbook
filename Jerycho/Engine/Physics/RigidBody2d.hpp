#pragma once

#include "../jaSetup.hpp"
#include "PhysicObject2d.hpp"
#include "../Math/VectorMath2d.hpp"
#include "../Gilgamesh/ObjectHandle2d.hpp"
#include "PhysicObjectDef2d.hpp"

class ThreadTests;

namespace ja
{

	class RigidBody2d : public PhysicObject2d
	{
	private:
		jaVector2 netForce;
		jaVector2 gravity; // It is better than using force generators
		jaVector2 linearVelocity;

		jaFloat mass;
		jaFloat invMass;

		jaFloat   linearDamping; // It is better than using force generators
		jaFloat   angularDamping; // It is better than using force generators
		//Body rotates in counter-clockwise order
		jaFloat   angularVelocity; // radiant / second 
		jaFloat   orientation; // radians
		jaFloat   torque;
		jaFloat   interia;
		jaFloat   invInteria;
		jaVector2 localCenterOfMass;

		uint16_t bodyFlag; // header common thing for particle generator and physic body
	public:

		RigidBody2d();
		~RigidBody2d();

		inline void setPosition(jaFloat x, jaFloat y);
		inline void setAngleDeg(jaFloat angleDeg);
		inline void setAngle(jaFloat angleRad);
		inline void setOrientation(const jaMatrix2& rotMatrix);
		inline void setScale(jaFloat x, jaFloat y);
		jaVector2 getPosition();
		jaFloat getAngleDeg();
		jaFloat getAngle();
		inline jaMatrix2 getRotMatrix();
		jaVector2 getScale();
		inline Shape2d* getShape();

		//Call it when body changes density or shape
		//void setMass(jaFloat density); // after  setting shape

		inline jaFloat   getMass();
		inline jaFloat   getInteria();
		inline jaVector2 getLocalCenterOfMass();

		inline void    setLinearDamping(jaFloat linearDamping);
		inline jaFloat getLinearDamping();
		inline void    setAngularDamping(jaFloat angluarDamping);
		inline jaFloat getAngularDamping();
		//Use this after setting mass
		
		void resetMassData();
		void updateBodyTypeChange();

		inline void setFixedRotation(bool state);
		inline bool isFixedRotation();

		inline void      setLinearVelocity(const jaVector2& linearVelocity);
		inline jaVector2 getLinearVelocity();
		inline void      setAngularVelocity(jaFloat angluarVelocity);
		inline jaFloat   getAngularVelocity();

		/// Apply a force at a world point. If the force is not
		/// applied at the center of mass, it will generate a torque and
		/// affect the angular velocity.
		/// @param force the world force vector, usually in Newtons (N).
		/// @param point the world position of the point of application.
		inline void applyForceAtWorldPoint(const jaVector2& force, const jaVector2& point);
		inline void applyForceAtLocalPoint(const jaVector2& force, const jaVector2& point);

		inline void applyForceToCenter(const jaVector2& force);
		inline void applyTorque(jaFloat torque);

		/// Apply an impulse at a point. This immediately modifies the velocity.
		/// It also modifies the angular velocity if the point of application
		/// is not at the center of mass.
		/// @param impulse the world impulse vector, usually in N-seconds or kg-m/s.
		/// @param point the world position of the point of application.
		inline void applyLinearImpulseToWorldPoint(const jaVector2& impulse, const jaVector2& point);
		inline void applyLinearImpulseToLocalPoint(const jaVector2& impulse, const jaVector2& point);

		inline void setCategoryMask(uint16_t& categoryMask);
		inline uint16_t getCategoryMask();
		inline void setMaskBits(uint16_t& maskBits);
		inline uint16_t getMaskBits();
		inline void   setGroupIndex(int8_t groupIndex);
		inline int8_t getGroupIndex();

		/// Apply an angular impulse.
		/// @param impulse the angular impulse in units of kg*m*m/s
		inline void applyAngularImpulse(jaFloat impulse);

		void      setGravity(const jaVector2& gravity);
		jaVector2 getGravity();

		inline bool isSleepingTresholdBelow()
		{
			if (angularVelocity * angularVelocity > (setup::physics::ANGULAR_VELOCITY_SLEEP_TRESHOLD * setup::physics::ANGULAR_VELOCITY_SLEEP_TRESHOLD))
				return false;

			if (jaVectormath2::projection(linearVelocity, linearVelocity) > (setup::physics::LINEAR_VELOCITY_SLEEP_TRESHOLD * setup::physics::LINEAR_VELOCITY_SLEEP_TRESHOLD))
				return false;

			return true;
		}

		void fillRigidBodyDef(PhysicObjectDef2d* physicObjectDef);

		friend class PhysicsManager;
		friend class RigidContact2d;
		friend class RevoluteJoint2d;
		friend class DistanceJoint2d;
		friend class ParticleGenerator2d;
		friend class ::ThreadTests;
		friend class ::PhysicsTests;
		friend class PhysicThreadWorkers;
	};

	Shape2d* RigidBody2d::getShape()
	{
		return shapeHandle->shape;
	}

	jaFloat RigidBody2d::getMass()
	{
		return mass;
	}

	jaFloat RigidBody2d::getInteria()
	{
		return interia;
	}

	jaVector2 RigidBody2d::getLocalCenterOfMass()
	{
		return localCenterOfMass;
	}

	void RigidBody2d::setLinearDamping(jaFloat linearDamping)
	{
		this->linearDamping = linearDamping;
	}

	jaFloat RigidBody2d::getLinearDamping()
	{
		return linearDamping;
	}

	void RigidBody2d::setAngularDamping(jaFloat angularDamping)
	{
		this->angularDamping = angularDamping;
	}

	jaFloat RigidBody2d::getAngularDamping()
	{
		return angularDamping;
	}

	void RigidBody2d::setPosition(jaFloat x, jaFloat y)
	{
		shapeHandle->transform.position.x = x;
		shapeHandle->transform.position.y = y;
	}

	void RigidBody2d::setAngleDeg(jaFloat angleDeg)
	{
		orientation = jaVectormath2::degreesToRadians(angleDeg);
		shapeHandle->transform.rotationMatrix.setRadians(orientation);
	}

	void RigidBody2d::setAngle(jaFloat angleRad)
	{
		orientation = angleRad;
		shapeHandle->transform.rotationMatrix.setRadians(angleRad);
	}

	void RigidBody2d::setOrientation(const jaMatrix2& rotMatrix)
	{
		orientation = rotMatrix.getRadians();
		shapeHandle->transform.rotationMatrix = rotMatrix;
	}

	jaMatrix2 RigidBody2d::getRotMatrix()
	{
		return shapeHandle->transform.rotationMatrix;
	}

	void RigidBody2d::setScale(jaFloat x, jaFloat y)
	{
		Shape2d* shape = shapeHandle->shape;

	}

	void RigidBody2d::setLinearVelocity(const jaVector2& linearVelocity)
	{
		//wake up body or something
		this->linearVelocity = linearVelocity;
	}

	jaVector2 RigidBody2d::getLinearVelocity()
	{
		return linearVelocity;
	}

	void RigidBody2d::setAngularVelocity(jaFloat angularVelocity)
	{
		//wake up body or something
		//if(!fixedRotation)
		this->angularVelocity = angularVelocity;
	}

	jaFloat RigidBody2d::getAngularVelocity()
	{
		return angularVelocity;
	}

	void RigidBody2d::applyForceAtWorldPoint(const jaVector2& force, const jaVector2& point)
	{
		netForce += force;

		jaVector2 r = point - (shapeHandle->transform * localCenterOfMass);

		torque = jaVectormath2::det(r, force);
	}

	void RigidBody2d::applyForceAtLocalPoint(const jaVector2& force, const jaVector2& point)
	{
		applyForceAtWorldPoint(shapeHandle->transform.rotationMatrix * force, shapeHandle->transform * point);
	}

	void RigidBody2d::applyForceToCenter(const jaVector2& force)
	{
		//wake up body or something
		netForce += force;
	}

	void RigidBody2d::applyTorque(jaFloat torque)
	{
		//wake up body or something
		//if(!fixedRotation)
		this->torque += torque;
	}	

	void RigidBody2d::applyLinearImpulseToWorldPoint(const jaVector2& impulse, const jaVector2& point)
	{
		//wake up body or something
		//m_linearVelocity += m_invMass * impulse;
		//m_angularVelocity += m_invI * b2Cross(point - m_sweep.c, impulse);
		linearVelocity += impulse * invMass;

		jaVector2 r = point - (shapeHandle->transform * localCenterOfMass);
		//if(!fixedRotation)
		//angularVelocity += (gilVectormath2::det(point, impulse) / interia);
		angularVelocity += (jaVectormath2::det(r, impulse) * invInteria);
	}

	void RigidBody2d::applyLinearImpulseToLocalPoint(const jaVector2& impulse, const jaVector2& point)
	{
		applyLinearImpulseToWorldPoint(shapeHandle->transform.rotationMatrix * impulse, shapeHandle->transform * point);
	}

	void RigidBody2d::applyAngularImpulse(jaFloat impulse)
	{
		//wake up body or something
		//if(!fixedRotation)
		//angularVelocity += (impulse / interia);
		angularVelocity += (impulse * invInteria);
	}

	bool RigidBody2d::isFixedRotation()
	{
		return ((bodyFlag & (uint16_t)BodyFlag::BODY_FIXED_ROTATION) != 0);
	}

	void RigidBody2d::setFixedRotation(bool state)
	{
		if (bodyType != (uint8_t)BodyType::STATIC_BODY)
		{
			if (state)
			{
				bodyFlag |= (uint16_t)BodyFlag::BODY_FIXED_ROTATION;
			}
			else
			{
				bodyFlag ^= bodyFlag & (uint16_t)BodyFlag::BODY_FIXED_ROTATION;
			}

			angularVelocity = GIL_ZERO;
			//Torque is cleared every step
		}
		
		resetMassData();
	}

	void RigidBody2d::setCategoryMask(uint16_t& categoryMask)
	{
		shapeHandle->categoryBits = categoryMask;
	}

	uint16_t RigidBody2d::getCategoryMask()
	{
		return shapeHandle->categoryBits;
	}

	void RigidBody2d::setMaskBits(uint16_t& maskBits)
	{
		shapeHandle->maskBits = maskBits;
	}

	uint16_t RigidBody2d::getMaskBits()
	{
		return shapeHandle->maskBits;
	}

	void RigidBody2d::setGroupIndex(int8_t groupIndex)
	{
		shapeHandle->groupIndex = groupIndex;
	}

	int8_t RigidBody2d::getGroupIndex()
	{
		return shapeHandle->groupIndex;
	}

}