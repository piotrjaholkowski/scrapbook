#pragma once

#include "../jaSetup.hpp"
#include "RigidBody2d.hpp"
#include "PhysicObject2d.hpp"
#include "PhysicObjectDef2d.hpp"
#include "../Math/VectorMath2d.hpp"


namespace ja
{

	class DistanceJoint2d : public Constraint2d
	{
	private:

		jaVector2 localAnchorA, localAnchorB;
		jaVector2 rA, rB;
		jaVector2 n;
		
		jaFloat jnBias;
		jaFloat jnAcc;

		jaFloat fixedDistance;
		jaFloat distance;
		//jaVector2 jAcc;		// accumulated impulse
		jaFloat nMass; // effective mass
		//jaFloat invNMass; // inverted effective mass
		//jaFloat mass;  // mass of A and B
		jaFloat invMassA;
		jaFloat invMassB;
		jaFloat invIA;
		jaFloat invIB;
		//jaFloat softness;
		//jaFloat relaxationBias;

	public:
		//relaxationBias should be from 0.0 to 1.0 range it corrects position error through simulation
		DistanceJoint2d(PhysicObject2d* objectA, PhysicObject2d* objectB, const jaVector2& anchorPointA, const jaVector2& anchorPointB);

		~DistanceJoint2d();

		inline void removeData() {
			unmarkPernament();
		}

		void setConstraint(const jaVector2& anchorPointA, const jaVector2& anchorPointB);
		void setConstraint(ConstraintDef2d* constraintDef);

		static Constraint2d* allocateConstraint(CacheLineAllocator* cacheLineAllocator, ConstraintDef2d* constraintDef)
		{
			Constraint2d* constraint = new(cacheLineAllocator->allocate(sizeof(DistanceJoint2d))) DistanceJoint2d(constraintDef->objectA, constraintDef->objectB, constraintDef->anchorPointA, constraintDef->anchorPointB);
			return constraint;
		}

		void markAndDeleteOldData()
		{

		}

		void preStep(jaFloat invDeltaTime);
		void applyCachedImpulse();

		bool resolveVelocity();
		bool resolvePosition();
		void getWorldAnchorPoints(jaVector2& worldPointA, jaVector2& worldPointB);

		void draw();

		uint32_t sizeOf()
		{
			return sizeof(DistanceJoint2d);
		}
	};

}