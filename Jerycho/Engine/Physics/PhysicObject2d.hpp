#pragma once

#include "../Gilgamesh/ObjectHandle2d.hpp"

namespace ja
{

	enum class PhysicObjectType
	{
		RIGID_BODY           = 0,
		PARTICLE_GENERATOR   = 1,
		DEFORMABLE_BODY      = 2,
		LIQUID               = 3
	};

	enum class BodyType
	{
		STATIC_BODY    = 0,
		KINEMATIC_BODY = 1, //Don't change that order or simulation will blow up
		DYNAMIC_BODY   = 2
	};

	enum class BodyFlag
	{
		BODY_ALLOW_SLEEP    = 4,
		BODY_SLEEP          = 32,
		BODY_FIXED_ROTATION = 64
	};

	class ConstraintIsland2d;

	class PhysicObject2d
	{
	protected:
		gil::ObjectHandle2d* shapeHandle;
		ConstraintIsland2d* island;
		jaFloat timeFactor; // It should not be larger than 1.0 or simulation will be unstable
		uint16_t objectId;
		uint8_t  objectType;
		uint8_t  bodyType;
		uint8_t  bodyFlag;
		uint8_t  constraintSynchronizer; // It helps to identify to which island object belongs to

		void* componentData;

	public:
		PhysicObject2d(uint8_t objectType) : objectType(objectType), island(nullptr), componentData(nullptr), bodyFlag(0)
		{

		}

		virtual bool isSleepingTresholdBelow() = 0;

		inline PhysicObject2d* getPrevious()
		{
			if (shapeHandle->prev != nullptr)
				return (PhysicObject2d*)(shapeHandle->prev->getUserData());

			return nullptr;
		}

		inline PhysicObject2d* getNext()
		{
			if (shapeHandle->next != nullptr)
				return (PhysicObject2d*)(shapeHandle->next->getUserData());

			return nullptr;
		}

		inline uint16_t getId() const
		{
			return objectId;
		}

		inline uint8_t getBodyType() const
		{
			return bodyType;
		}

		inline const char* getBodyTypeName() const
		{
			switch (bodyType)
			{
			case (uint8_t)BodyType::STATIC_BODY:
				return "BodyType.Static";
				break;
			case (uint8_t)BodyType::KINEMATIC_BODY:
				return "BodyType.Kinematic";
				break;
			case (uint8_t)BodyType::DYNAMIC_BODY:
				return "BodyType.Dynamic";
				break;
			}

			return nullptr;
		}

		inline uint8_t getObjectType() const
		{
			return objectType;
		}

		inline gil::Shape2d* getShape() const
		{
			return shapeHandle->getShape();
		}

		inline gil::ObjectHandle2d* getShapeHandle() const
		{
			return shapeHandle;
		}

		inline void* getComponentData()
		{
			return this->componentData;
		}

		inline bool isSleeping()
		{
			if (bodyType != (uint8_t)BodyType::DYNAMIC_BODY)
			{
				return false;
			}

			return !shapeHandle->isDynamic();
		}

		inline bool isSleepingAllowed() const
		{
			if (bodyType != (uint8_t)BodyType::DYNAMIC_BODY)
			{
				return false;
			}

			return ((bodyFlag & (uint8_t)BodyFlag::BODY_ALLOW_SLEEP) != 0);
		}

		inline void setAllowSleep(bool allowSleep)
		{
			if (allowSleep)
			{
				bodyFlag |= (uint8_t)BodyFlag::BODY_ALLOW_SLEEP;
			}
			else
			{
				bodyFlag |= (uint8_t)BodyFlag::BODY_ALLOW_SLEEP;
				bodyFlag ^= (uint8_t)BodyFlag::BODY_ALLOW_SLEEP;
			}
		}

		inline void setTimeFactor(jaFloat timeFactor)
		{
			this->timeFactor = timeFactor;
		}

		inline jaFloat getTimeFactor() const
		{
			return timeFactor;
		}

		virtual void setOrientation(const jaMatrix2& rotMatrix) = 0;

		virtual void updateBodyTypeChange() = 0;

		virtual ~PhysicObject2d()
		{
			//This destructor shouldn't be called
#ifndef _JA_TEST
			assert(false);
#endif
		}

		friend class PhysicsManager;
		friend class PhysicGroup;
		friend class PhysicComponent;
		friend class Constraint2d;
		friend class RigidContact2d;
		friend class RevoluteJoint2d;
		friend class DistanceJoint2d;
		friend class ConstraintIsland2d;
		friend class LinearMotor2d;
		friend class PhysicThreadWorkers;
	};

}
