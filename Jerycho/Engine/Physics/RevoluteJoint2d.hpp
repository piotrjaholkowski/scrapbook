#pragma once

#include "../jaSetup.hpp"
#include "RigidBody2d.hpp"
#include "PhysicObject2d.hpp"
#include "PhysicObjectDef2d.hpp"
#include "../Math/VectorMath2d.hpp"
#include "../Gilgamesh/ObjectHandle2d.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"

namespace ja
{

	class RevoluteJoint2d : public Constraint2d
	{
	private:

		jaMatrix22 M;
		jaVector2 localAnchorA, localAnchorB;
		jaVector2 rA, rB;
		jaVector2 bias;
		jaVector2 jAcc;		// accumulated impulse
		jaFloat softness;
		jaFloat relaxationBias;

	public:
		//relaxationBias should be from 0.0 to 1.0 range it corrects position error through simulation
		RevoluteJoint2d(PhysicObject2d* objectA, PhysicObject2d* objectB, const jaVector2& anchorPoint, jaFloat relaxationBias, jaFloat softness) : Constraint2d(objectA, objectB, JA_REVOLUTE_JOINT2D)
		{
			markAsPernament();

			setConstraint(anchorPoint, relaxationBias, softness);
		}

		~RevoluteJoint2d()
		{
			removeData();
		}

		inline void removeData() {
			unmarkPernament();
		}

		void setConstraint(const jaVector2& anchorPoint, jaFloat relaxationBias, jaFloat softness)
		{
			jaTransform2* tA = &objectA->shapeHandle->transform;
			jaTransform2* tB = &objectB->shapeHandle->transform;

			this->relaxationBias = relaxationBias;
			this->softness = softness;

			jAcc.x = GIL_ZERO;
			jAcc.y = GIL_ZERO;


			localAnchorA = jaVectormath2::inverse(tA->rotationMatrix) * (anchorPoint - tA->position);
			localAnchorB = jaVectormath2::inverse(tB->rotationMatrix) * (anchorPoint - tB->position);
		}

		void setConstraint(ConstraintDef2d* constraintDef)
		{
			setConstraint(constraintDef->anchorPointA, constraintDef->relaxationBias, constraintDef->softness);
		}

		static Constraint2d* allocateConstraint(CacheLineAllocator* cacheLineAllocator, ConstraintDef2d* constraintDef)
		{
			Constraint2d* constraint = new(cacheLineAllocator->allocate(sizeof(RevoluteJoint2d))) RevoluteJoint2d(constraintDef->objectA, constraintDef->objectB, constraintDef->anchorPointA, constraintDef->relaxationBias, constraintDef->softness);
			return constraint;
		}

		void markAndDeleteOldData()
		{

		}

		void preStep(jaFloat invDeltaTime)
		{
			RigidBody2d* bodyA = static_cast<RigidBody2d*>(objectA);
			RigidBody2d* bodyB = static_cast<RigidBody2d*>(objectB);

			jaTransform2* tA = bodyA->shapeHandle->getTransformP();
			jaTransform2* tB = bodyB->shapeHandle->getTransformP();

			// Pre-compute anchors, mass matrix, and bias.
			rA = tA->rotationMatrix * localAnchorA;
			rB = tB->rotationMatrix * localAnchorB;

			// deltaV = deltaV0 + K * impulse
			// invM = [(1/m1 + 1/m2) * eye(2) - skew(r1) * invI1 * skew(r1) - skew(r2) * invI2 * skew(r2)]
			//      = [1/m1+1/m2     0    ] + invI1 * [r1.y*r1.y -r1.x*r1.y] + invI2 * [r1.y*r1.y -r1.x*r1.y]
			//        [    0     1/m1+1/m2]           [-r1.x*r1.y r1.x*r1.x]           [-r1.x*r1.y r1.x*r1.x]
			jaMatrix22 K1;
			K1.col1.x = bodyA->invMass + bodyB->invMass;	K1.col2.x = GIL_ZERO;
			K1.col1.y = GIL_ZERO;							K1.col2.y = bodyA->invMass + bodyB->invMass;

			jaMatrix22 K2;
			K2.col1.x = bodyA->invInteria * rA.y * rA.y;		K2.col2.x = -bodyA->invInteria * rA.x * rA.y;
			K2.col1.y = -bodyA->invInteria * rA.x * rA.y;		K2.col2.y = bodyA->invInteria * rA.x * rA.x;

			jaMatrix22 K3;
			K3.col1.x = bodyB->invInteria * rB.y * rB.y;		K3.col2.x = -bodyB->invInteria * rB.x * rB.y;
			K3.col1.y = -bodyB->invInteria * rB.x * rB.y;		K3.col2.y = bodyB->invInteria * rB.x * rB.x;

			jaMatrix22 K = K1 + K2 + K3;
			K.col1.x += softness;
			K.col2.y += softness;

			M = jaVectormath2::inverse(K);

			jaVector2 p1 = tA->position + rA;
			jaVector2 p2 = tB->position + rB;
			jaVector2 dp = p2 - p1;

			//Position correction
			bias = -relaxationBias * invDeltaTime * dp;
		}

		void applyCachedImpulse()
		{
			RigidBody2d* bodyA = (RigidBody2d*)objectA;
			RigidBody2d* bodyB = (RigidBody2d*)objectB;

			// Apply accumulated impulse.
			bodyA->linearVelocity -= bodyA->invMass * jAcc;
			bodyA->angularVelocity -= bodyA->invInteria * jaVectormath2::det(rA, jAcc);

			bodyB->linearVelocity += bodyB->invMass * jAcc;
			bodyB->angularVelocity += bodyB->invInteria * jaVectormath2::det(rB, jAcc);

		}

		bool resolveVelocity()
		{
			RigidBody2d* bodyA = (RigidBody2d*)objectA;
			RigidBody2d* bodyB = (RigidBody2d*)objectB;

			//Relative velocity
			jaVector2 dv = bodyB->linearVelocity + jaVectormath2::perpendicular(bodyB->angularVelocity, rB) - bodyA->linearVelocity - jaVectormath2::perpendicular(bodyA->angularVelocity, rA);

			jaVector2 impulse;

			impulse = M * (bias - dv - softness * jAcc);

			bodyA->linearVelocity -= bodyA->invMass * impulse;
			bodyA->angularVelocity -= bodyA->invInteria * jaVectormath2::det(rA, impulse);

			bodyB->linearVelocity += bodyB->invMass * impulse;
			bodyB->angularVelocity += bodyB->invInteria * jaVectormath2::det(rB, impulse);

			jAcc += impulse;

			if ((abs(impulse.x) + abs(impulse.y)) < setup::physics::CONTACT2D_SLEEP_TRESHOLD)
				return false; // constraint satisfied don't need more correction

			return true;
		}

		bool resolvePosition()
		{
			return true;
		}

		void draw()
		{
			RigidBody2d* bodyA = (RigidBody2d*)objectA;
			RigidBody2d* bodyB = (RigidBody2d*)objectB;
			jaTransform2* tA = &bodyA->shapeHandle->transform;
			jaTransform2* tB = &bodyB->shapeHandle->transform;

			jaVector2 x1 = tA->position;
			jaVector2 p1 = x1 + (tA->rotationMatrix * localAnchorA);

			jaVector2 x2 = tB->position;
			jaVector2 p2 = x2 + (tB->rotationMatrix * localAnchorB);

			DisplayManager::setPointSize(2.75);
			DisplayManager::setColorUb(128, 128, 190);

			DisplayManager::drawLine(x1.x, x1.y, p1.x, p1.y);
			DisplayManager::drawLine(x2.x, x2.y, p2.x, p2.y);
		}

		uint32_t sizeOf()
		{
			return sizeof(RevoluteJoint2d);
		}
	};

}