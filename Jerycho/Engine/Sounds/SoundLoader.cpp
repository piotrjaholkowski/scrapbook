#include "SoundLoader.hpp"
#include "../Manager/SoundManager.hpp"

#include "../Utility/Exception.hpp"

#include <vorbis/vorbisfile.h>

using namespace ja;

SoundLoader* SoundLoader::singleton = nullptr;

SoundLoader::SoundLoader()
{

}

bool SoundLoader::loadOggFile(const char *fileName, std::vector<char> &buffer, ALenum &format, ALsizei &freq)
{
	int32_t endian = 0;                         // 0 for Little-Endian, 1 for Big-Endian
	int32_t bitStream;
	long bytes;
	char bufferArray[4092];                // Local fixed size array
	vorbis_info *pInfo;
	OggVorbis_File oggFile;

#ifdef _WIN32
	if (ov_fopen(fileName, &oggFile) != 0)
	{
		std::cerr << "Error opening " << fileName << " for decoding..." << std::endl;
		return false;
	}
#else
	FILE *f;
	// Open for binary reading
	f = fopen(fileName, "rb");

	if (f == nullptr)
	{
		std::cerr << "Cannot open " << fileName << " for reading..." << std::endl;
		return false;
	}

	// Try opening the given file
	if (ov_open(f, &oggFile, nullptr, 0) != 0)
	{
		std::cerr << "Error opening " << fileName << " for decoding..." << std::endl;
		return false;
	}
#endif

	// Get some information about the OGG file
	pInfo = ov_info(&oggFile, -1); 
	
	/*std::cout << std::endl;
	std::cout << "pInfo " << std::endl;
	std::cout << "bitrate lower " << pInfo->bitrate_nominal << std::endl;
	std::cout << "bitrate nominal " << pInfo->bitrate_nominal << std::endl;
	std::cout << "bitrate upper " << pInfo->bitrate_upper << std::endl;
	std::cout << "bitrate window " << pInfo->bitrate_window << std::endl;
	std::cout << "channels " << pInfo->channels << std::endl;
	std::cout << "codec setup " << pInfo->codec_setup << std::endl;
	std::cout << "frequency " << pInfo->rate << std::endl;
	std::cout << "bytes per second " << (pInfo->channels * pInfo->rate * 2) << std::endl;
	std::cout << "version " << pInfo->version << std::endl;
	
	std::cout << "bittrack " << oggFile.bittrack << std::endl;
	std::cout << "end " << oggFile.end << std::endl;
	std::cout << "links " << oggFile.links << std::endl;
	std::cout << "offset " << oggFile.offset << std::endl;
	std::cout << "samptrack " << oggFile.samptrack << std::endl;
	std::cout << "seekable " << oggFile.seekable << std::endl;*/

	// Check the number of channels... always use 16-bit samples
	if (pInfo->channels == 1)
		format = AL_FORMAT_MONO16;
	else
		format = AL_FORMAT_STEREO16;

	// The frequency of the sampling rate
	freq = pInfo->rate;

	// Keep reading until all is read
	do
	{
		// Read up to a buffer's worth of decoded sound data
		bytes = ov_read(&oggFile, bufferArray, 4092, endian, 2, 1, &bitStream);

		if (bytes < 0)
		{
			ov_clear(&oggFile);
			std::cerr << "Error decoding " << fileName << "..." << std::endl;
			return false;
		}

		// Append to end of buffer
		buffer.insert(buffer.end(), bufferArray, bufferArray + bytes);
	}
	while (bytes > 0);

	/*std::cout << "buffer size " << buffer.size() << std::endl;
	std::cout << "seconds " << buffer.size() / (pInfo->rate * pInfo->channels * 2);
	unsigned int miliseconds = buffer.size() % (pInfo->rate * pInfo->channels * 2);
	miliseconds = miliseconds * 1000 / (pInfo->rate * pInfo->channels * 2);
	std::cout << "miliseconds " << miliseconds << std::endl;*/

	// Clean up!
	ov_clear(&oggFile);
	return true;
}

bool SoundLoader::loadOggStream(const char *fileName, SoundStream* musicStream)
{
	musicStream->endian = 0;
	vorbis_info *pInfo;
	
#ifdef _WIN32
	if (ov_fopen(fileName, &musicStream->oggFile) != 0)
	{
		//std::cerr << "Error opening " << fileName << " for decoding..." << std::endl;
		//return false;
		delete musicStream;
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_SOUND_STREAM, fileName);
	}
#else
	FILE *f;
	// Open for binary reading
	f = fopen(fileName, "rb");

	if (f == nullptr)
	{
		std::cerr << "Cannot open " << fileName << " for reading..." << std::endl;
		return false;
	}

	// Try opening the given file
	if (ov_open(f, &musicStream->oggFile, nullptr, 0) != 0)
	{
		std::cerr << "Error opening " << fileName << " for decoding..." << std::endl;
		return false;
	}
#endif

	// Get some information about the OGG file
	pInfo = ov_info(&musicStream->oggFile, -1);
	
	// Check the number of channels... always use 16-bit samples
	if (pInfo->channels == 1) {
		musicStream->format = AL_FORMAT_MONO16;
		musicStream->depth = 2;
	}
	else {
		musicStream->format = AL_FORMAT_STEREO16;
		musicStream->depth = 2;
	}

	musicStream->channelsNum = pInfo->channels;
	// The frequency of the sampling rate
	musicStream->freq = pInfo->rate;
	musicStream->numberOfSamples = ov_pcm_total(&musicStream->oggFile, -1);
	
	//std::cout << "samples " << musicStream->numberOfSamples << std::endl;
	//std::cout << "time s " << (musicStream->numberOfSamples / musicStream->freq) << std::endl;
	
	musicStream->lengthInMiliseconds = 1000 * static_cast<int32_t>(musicStream->numberOfSamples / musicStream->freq);
	musicStream->lengthInMiliseconds += 1000 * static_cast<int32_t>(musicStream->numberOfSamples % musicStream->freq) / static_cast<int>(musicStream->numberOfSamples);
	//std::cout << "time ms " << musicStream->lengthInMiliseconds << std::endl;
	//alGenBuffers(JA_STREAM_BUFFS_NUM, musicStream->buffers);

	return true;
}

SoundLoader* SoundLoader::getSingleton()
{
	return singleton;
}

void SoundLoader::init()
{
	if(singleton==nullptr)
	{
		singleton = new SoundLoader();
	}
}

void SoundLoader::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

SoundEffect* SoundLoader::loadSoundEffect(const ja::string& fileName, uint32_t resourceId)
{
	//ALenum error = alGetError();
	//error = alGetError();
	alGetError();
	/*AL_NO_ERROR
	AL_INVALID_NAME
	AL_INVALID_ENUM
	AL_INVALID_VALUE
	AL_INVALID_OPERATION
	AL_OUT_OF_MEMORY*/

	std::vector<char> bufferData;
	ALenum format;
	ALsizei freq;
	ALuint bufferId; 

	if(loadOggFile(fileName.c_str(),bufferData,format,freq))
	{
		alGenBuffers(1, &bufferId);
		if(bufferId != AL_FALSE) {
			alBufferData(bufferId, format, &bufferData[0], static_cast<ALsizei>(bufferData.size()), freq);
			/*std::cout << "name " << fileName << std::endl;
			std::cout << "buffer size " << bufferData.size() << std::endl;
			std::cout << "frequency " << freq << std::endl;
			switch (format)
			{
			case AL_FORMAT_MONO16:
				std::cout << "format mono 16" << std::endl;
				break;
			case AL_FORMAT_STEREO16:
				std::cout << "format stereo 16" << std::endl;
				break;
			case AL_FORMAT_MONO8:
				std::cout << "format mono 8" << std::endl;
				break;
			case AL_FORMAT_STEREO8:
				std::cout << "format mono 16" << std::endl;
				break;
			}
			std::cout << "sound length " << bufferData.size() / freq << std::endl;*/
		}
		else {
			//std::cout << "Couldn't create sound buffer" << std::endl;
			//return nullptr;
			ja::string errorMessage = "Couldn't create sound buffer ";
			errorMessage.append(fileName);
			throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_SOUND_EFFECT, errorMessage);
		}
		
	}
	else {
		//std::cout << "Couldn't load file " << fileName.c_str() << std::endl;
		//return nullptr;
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_SOUND_EFFECT, fileName);
	}

	SoundEffect* soundEffect = new SoundEffect(bufferId,resourceId);
	/*std::cout << "channels " << soundEffect->getChannels() << std::endl;
	std::cout << "depth " << soundEffect->getDepth() << std::endl;
	std::cout << "frequency " << soundEffect->getFrequency() << std::endl;
	std::cout << "size " << soundEffect->getSize() << std::endl;*/
	return soundEffect;
}

SoundStream* SoundLoader::loadSoundStream(const ja::string& fileName, uint32_t resourceId)
{	
	alGetError();
	SoundStream* musicStream = new SoundStream(resourceId);

	loadOggStream(fileName.c_str(), musicStream);

	return musicStream;
}