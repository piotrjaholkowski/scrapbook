#pragma once

#include "SoundEffect.hpp"
#include "SoundStream.hpp"
#include "../Utility/String.hpp"

#include <vector>
#include <stdint.h>

namespace ja
{

	class SoundLoader
	{
	private:
		static SoundLoader* singleton;
		SoundLoader();
		static bool loadOggFile(const char *fileName, std::vector<char> &buffer, ALenum &format, ALsizei &freq);
		static bool loadOggStream(const char *fileName, SoundStream* musicStream);
	public:
		static void init();
		static SoundLoader* getSingleton();
		static SoundEffect* loadSoundEffect(const ja::string& fileName, uint32_t resourceId);
		static SoundStream* loadSoundStream(const ja::string& fileName, uint32_t resourceId);
		// static jaMusic loadMusic(const ja::string& fileName, unsigned int resourceId);
		static void cleanUp();
	};

}