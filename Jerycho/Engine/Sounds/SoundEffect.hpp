#pragma once

#include <AL/al.h>

#include <stdint.h>

namespace ja
{

class SoundEffect
{
private:
public:
	uint32_t resourceId;
	ALuint   soundId;
	int32_t  lengthInMiliseconds;

	SoundEffect(ALuint soundId, unsigned int resourceId)
	{
		this->soundId = soundId;
		this->resourceId = resourceId;

		int32_t channelsNum = getChannelsNum();
		int32_t depth       = getDepth() / 8;
		int32_t size        = getSize();
		int32_t frequency   = getFrequency();
		int32_t bytesPerSecond = channelsNum * depth * frequency;
		lengthInMiliseconds = size / bytesPerSecond;
		lengthInMiliseconds = (lengthInMiliseconds * 1000) + (1000 * (size % bytesPerSecond) / bytesPerSecond);
	}

	~SoundEffect()
	{
		 alDeleteBuffers(1, &soundId);
	}

	inline int32_t getFrequency()
	{
		int32_t frequency;
		alGetBufferi(soundId,AL_FREQUENCY,&frequency);
		return frequency;
	}

	inline int32_t getDepth()
	{
		//get bit depth of buffer 8 or 16 bits
		int32_t depth;
		alGetBufferi(soundId,AL_BITS,&depth);
		return depth;
	}

	int32_t getLengthInMiliseconds()
	{
		return lengthInMiliseconds;
	}

	float getLengthInSeconds()
	{
		return (float)(lengthInMiliseconds) / 1000.0f;
	}

	inline int32_t getChannelsNum()
	{
		int32_t channels;
		alGetBufferi(soundId,AL_CHANNELS,&channels);
		return channels;
	}

	inline int32_t getSize()
	{
		int32_t size;
		alGetBufferi(soundId,AL_SIZE,&size);
		return size;
	}

};

}
