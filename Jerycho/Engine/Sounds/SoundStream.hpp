#pragma once

#include "../jaSetup.hpp"

#ifdef JA_SDL2_VERSION
	#include<SDL2/SDL.h>
#else
	#include<SDL/SDL.h>
#endif

#include <vorbis/vorbisfile.h>
#include <iostream>

#include <stdint.h>

namespace ja
{

	class SoundStream
	{
	public:
		uint32_t resourceId;
		int32_t lengthInMiliseconds;
		long long numberOfSamples;
		long long currentSample;
		ALenum format;
		ALsizei freq;
		int32_t endian;                         // 0 for Little-Endian, 1 for Big-Endian
		int32_t depth;
		int32_t channelsNum;
		OggVorbis_File oggFile;


		SoundStream(uint32_t resourceId)
		{
			this->resourceId = resourceId;
		}

		ALsizei getBufferDataFromStream(ALsizei buffersNum, char* bufferArray, int32_t& bitstream, int32_t bytesNumToRead, ALuint* soundBuffers, bool& isEndOfStream)
		{
			long bytesNum;
			ALsizei i = 0;
			ALsizei bytesRead = 0;
			char* bufferP = bufferArray;

			// Keep reading until all is read
			do
			{
				while (bytesRead < bytesNumToRead) {
					bytesNum = ov_read(&oggFile, bufferP, bytesNumToRead, endian, depth, 1, &bitstream); // depth 2 - for 16 bit sample 1 - for 8 bit sample
					//sgned Signed or unsigned data. 0 for unsigned, 1 for signed.Typically 1
					bytesRead += bytesNum;
					bufferP += bytesNum;

					if (bytesNum < 0) {
						std::cerr << "Error decoding ogg file ..." << std::endl;
						return 0;
					}

					if (bytesNum == 0)  {// end of stream
						isEndOfStream = true;
						return i;
					}
				}

				alBufferData(soundBuffers[i], format, bufferArray, bytesRead, freq);
				bytesRead = bytesNumToRead;
				bufferP = bufferArray;

				i++;
			} while ((bytesNum > 0) && (i < buffersNum));

			isEndOfStream = false;
			return i;
		}

		int32_t getLengthInMiliseconds()
		{
			return lengthInMiliseconds;
		}

		double getTimeInSeconds()
		{
			return ov_time_total(&oggFile, -1);
		}

		inline int32_t getChannelsNum()
		{
			return channelsNum;
		}

		inline int32_t getFrequency()
		{
			return freq;
		}

		~SoundStream()
		{
			//alDeleteBuffers(JA_STREAM_BUFFS_NUM, buffers);
			// Clean up!
			ov_clear(&oggFile);
		}
	};

}
