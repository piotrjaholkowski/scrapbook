#pragma once

namespace ja
{

class SoundEmitterDef
{
public:
	char  name[15];
	bool  relativeToListener;
	float position[3];
	float rollofFactor;
	float direction[3];
	float gain;
	float pitch;
	float maxDistance;
	float referenceDistance;
	float velocity[3];
};

}

