#pragma once

#include "../jaSetup.hpp"
#include "SoundEffect.hpp"
#include <AL/al.h>
#include <AL/alc.h>

namespace ja
{

	class SoundEmitter
	{
	private:
		//ALuint soundEmitterId[JA_MAX_SOUND_CHANNELS];
		ALuint soundEmitterId;
		//unsigned char soundEmitterFlag;
		//unsigned char channelsNum;
		//unsigned short emitterId; // for identification
		//unsigned char soundGroup;
		SoundEffect* soundEffect;

	public:
		SoundEmitter();
		~SoundEmitter();

		//AL_CONE_OUTER_GAIN default 0 the gain when outside the oriented cone
		//AL_CONE_INNER_ANGLE default 360 the gain when inside the oriented cone
		//AL_CONE_OUTER_ANGLE default 360 outer angle of the sound cone, in degrees


		inline void setRelativeToListener(bool isRelative); // determines if the positions are relative to the listener
		inline void setReferenceDistance(float referenceDistance); //the distance under which the volume for the source would normally drop by half (before being influenced by rolloff factor or AL_MAX_DISTANCE)
		inline void setMaxDistance(float maxDistance); // max distance which after reaching sound won't be mixed used with the Inverse Clamped Distance Model
		inline void setRolloffFactor(float rollofFactor);
		inline void setPosition(float x, float y, float z);
		inline void setVelocity(float x, float y, float z);
		inline void setDirection(float x, float y, float z);
		inline void setPitch(float pitch); // speed of streaming sound
		inline void setGain(float gain); // volume multiplier
		inline void setMinGain(float gain); // lowest volume of sound
		inline void setMaxGain(float gain); // max volume of sound
		inline void setLoop(bool isLoop);
		inline bool isLoop();
		inline void stop();
		inline void setSound(SoundEffect* soundEffect);
		inline void play();
		inline ALint getState();
		inline void pause();
		inline void setElapsedTime(float time);
		inline void getElapsedTime(float &elapsedTime);
		inline unsigned char getSoundGroup();
		inline SoundEffect* getSoundEffect();

		friend class SoundManager;
	};

	void SoundEmitter::setRelativeToListener(bool isRelative)
	{
		// default 1.0
		alSourcei(soundEmitterId, AL_SOURCE_RELATIVE, isRelative); // deafult AL_FALSE
	}

	void SoundEmitter::setReferenceDistance(float referenceDistance)
	{
		alSourcef(soundEmitterId, AL_REFERENCE_DISTANCE, referenceDistance); // deafult 1.0
	}

	void SoundEmitter::setRolloffFactor(float rollofFactor)
	{
		alSourcef(soundEmitterId, AL_ROLLOFF_FACTOR, rollofFactor); // deafult 1.0
	}

	void SoundEmitter::setMaxDistance(float maxDistance)
	{
		alSourcef(soundEmitterId, AL_MAX_DISTANCE, maxDistance); // deafult maxFloat
	}

	void SoundEmitter::setPosition(float x, float y, float z)
	{
		alSource3f(soundEmitterId, AL_POSITION, x, y, z); // default 0,0,0
	}

	void SoundEmitter::setDirection(float x, float y, float z)
	{
		alSource3f(soundEmitterId, AL_DIRECTION, x, y, z); // if not 0,0,0 then source is directional
		//default (0.0f, 0.0f, 0.0f)
	}

	void SoundEmitter::setVelocity(float x, float y, float z)
	{
		alSource3f(soundEmitterId, AL_VELOCITY, x, y, z); // default 0,0,0
	}

	void SoundEmitter::setPitch(float pitch)
	{
		alSourcef(soundEmitterId, AL_PITCH, pitch); // default 1.0
	}

	void SoundEmitter::setGain(float gain)
	{
		alSourcef(soundEmitterId, AL_GAIN, gain); // default 1.0 [0, any]
	}

	void SoundEmitter::setMinGain(float gain)
	{
		alSourcef(soundEmitterId, AL_MIN_GAIN, gain); // default 0.0
	}

	void SoundEmitter::setMaxGain(float gain)
	{
		alSourcef(soundEmitterId, AL_MAX_GAIN, gain); // default 1.0
	}

	void SoundEmitter::setLoop(bool isLoop)
	{
		alSourcei(soundEmitterId, AL_LOOPING, isLoop);
	}

	bool SoundEmitter::isLoop()
	{
		int state;
		alGetSourcei(soundEmitterId, AL_LOOPING, &state);
		if (state > 0)
			return true;
		return false;
	}

	void SoundEmitter::stop()
	{
		alSourceStop(soundEmitterId);
	}

	void SoundEmitter::setSound(SoundEffect* soundEffect)
	{
		this->soundEffect = soundEffect;
		alSourcei(soundEmitterId, AL_BUFFER, soundEffect->soundId);
	}

	void SoundEmitter::play()
	{
		/*int state;
		for(unsigned char i=0; i<channelsNum; ++i)
		{
		alGetSourcei(soundEmitterId[i],AL_SOURCE_STATE,&state);
		if(state != AL_PLAYING) // AL_INITIAL, AL_STOPPED, AL_PAUSED, AL_INITIAL
		{
		alSourcei(soundEmitterId[i],AL_BUFFER,soundEffect->soundId);
		alSourcePlay(soundEmitterId[i]);
		}
		}*/

		int state;
		alGetSourcei(soundEmitterId, AL_SOURCE_STATE, &state);
		if (state == AL_STOPPED)
			alSourceRewind(soundEmitterId);
		alSourcePlay(soundEmitterId);
	}

	ALint SoundEmitter::getState()
	{
		int state;
		alGetSourcei(soundEmitterId, AL_SOURCE_STATE, &state);

		return state;
	}

	void SoundEmitter::pause()
	{
		alSourcePause(soundEmitterId);
	}

	void SoundEmitter::setElapsedTime(float time)
	{
		alSourcef(soundEmitterId, AL_SEC_OFFSET, time); // time in seconds
		//AL_SAMPLE_OFFSET mo�na ustawi� w samplach
		//AL_BYTE_OFFSET mo�na w bajtach
	}

	void SoundEmitter::getElapsedTime(float& elapsedTime)
	{
		alGetSourcef(soundEmitterId, AL_SEC_OFFSET, &elapsedTime);
	}

	/*unsigned char jaSoundEmitter::getSoundGroup()
	{
	return soundGroup;
	}*/

	SoundEffect* SoundEmitter::getSoundEffect()
	{
		return soundEffect;
	}

}

