#pragma once

#include "Widget.hpp"

namespace ja
{

	class Layout : public Widget
	{
	public:
		Layout(WidgetType widgetType, uint32_t id, Widget* parent) : Widget(widgetType, id, parent)
		{
			this->setAsLayout(true);
		}

		void increaseReference(Widget* widget)
		{
			++widget->references;
		}

		bool removeReference(Widget* widget) // if widget is deleted returns true
		{
			if (widget->removeReference())
			{
				delete widget;
				return true;
			}

			return false;
		}

		void forcePosition(Widget* widget, int32_t x, int32_t y)
		{
			widget->forcePosition(x, y);
		}

		void forceSize(Widget* widget, int32_t width, int32_t height)
		{
			widget->forceSize(width, height);
		}

		inline void setCollisionMask(Widget* widget, int32_t minX, int32_t minY, int32_t maxX, int32_t maxY)
		{
			widget->updateCollisionMask();
		}

		inline void setCollisionMaskDirecly(Widget* widget, int32_t minX, int32_t minY, int32_t maxX, int32_t maxY)
		{
			widget->collisionMask.setMask(minX, minY, maxX, maxY);

			widget->updateChildrenCollisionMask();
		}

		virtual void getOriginPosition(int32_t& x, int32_t& y)
		{
			this->getPosition(x, y);
		}
	};

}