#pragma once

#include "DebugHud.hpp"

namespace ja
{

class TestHud : public DebugHud
{
private:
	static TestHud* singleton;
	TestHud();
public:
	static void initTestHud(const char* fontName, uint32_t fontSize);
	static TestHud* getSingleton();
	static void cleanUp();
	~TestHud();
};

}
