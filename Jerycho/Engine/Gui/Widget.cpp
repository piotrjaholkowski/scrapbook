#include "Widget.hpp"
#include "Interface.hpp"
#include "../jaSetup.hpp"
#include "../Utility/Exception.hpp"
#include "../Manager/InputManager.hpp"
#include "../Manager/UIManager.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/ScriptManager.hpp"
//#include "../Utility/jaException.h"

#pragma warning( disable : 4482 )
#pragma warning( disable : 4800 )

namespace ja
{ 

Widget::Widget(WidgetType widgetType, uint32_t id = 0, Widget* parent = nullptr) : widgetType(widgetType), x(0), y(0), width(0), height(0), id(id), componentFlag(0), references(0), texBuffer(setup::graphics::TEXTURE_UNASSIGNED), zOrder(0)
{
	if(parent != nullptr)
		setParent(parent);
	else
		this->parent = nullptr;
}

Widget::~Widget()
{
	deleteTexBuffer();
}

uint32_t Widget::getId()
{
	return id;
}

Widget* Widget::getParent()
{
	return parent;
}

bool Widget::isRendered()
{
	return (componentFlag & ComponentFlag::JA_DRAW);
}

bool Widget::isActive()
{
	return (componentFlag & ComponentFlag::JA_ACTIVE);
}

bool Widget::isSelectable()
{
	return (componentFlag & ComponentFlag::JA_SELECTABLE);
}

bool Widget::isBorderRendered()
{
	return (componentFlag & ComponentFlag::JA_DRAWBORDER);
}

bool Widget::isUsingUnicode()
{
	return false;
}

bool Widget::isLayoutChild()
{
	return (componentFlag & ComponentFlag::JA_LAYOUT_CHILD);
}


bool Widget::isLayout()
{
	return (componentFlag & ComponentFlag::JA_LAYOUT);
}


bool Widget::isModal()
{
	return (componentFlag & ComponentFlag::JA_MODAL);
}

bool Widget::isDragable()
{
	return (componentFlag & ComponentFlag::JA_DRAGABLE);
}

void Widget::setModal(bool state)
{
	if(state)
	{
		componentFlag |= ComponentFlag::JA_MODAL;

		if(parent != nullptr)
		{
			if(isActive())
			{
				parent->setTopWidget(this);
			}
		}
		else
		{
			throw JA_EXCEPTIONR(ExceptionType::GUI,"You can't make widget modal before adding it to interface");
		}
	}
	else
	{
		componentFlag |= ComponentFlag::JA_MODAL;
		componentFlag ^= ComponentFlag::JA_MODAL;
	}	
}

void Widget::setDragable(bool state)
{
	if(state)
	{
		componentFlag |= ComponentFlag::JA_DRAGABLE;
	}
	else
	{
		componentFlag |= ComponentFlag::JA_DRAGABLE;
		componentFlag ^= ComponentFlag::JA_DRAGABLE;
	}	
}

void Widget::setRender(bool state)
{
	if(state)
	{
		componentFlag |= ComponentFlag::JA_DRAW;
	}
	else
	{
		componentFlag |= ComponentFlag::JA_DRAW;
		componentFlag ^= ComponentFlag::JA_DRAW;
	}	
}

void Widget::setActive(bool state)
{
	if(state)
	{
		componentFlag |= ComponentFlag::JA_ACTIVE;

		if(isModal())
		{
			if(parent != nullptr)
			{
				parent->setTopWidget(this);
			}
		}
	}
	else
	{
		componentFlag |= ComponentFlag::JA_ACTIVE;
		componentFlag ^= ComponentFlag::JA_ACTIVE;
	}	
}


void Widget::setLayoutChild( bool state )
{
	if(state)
	{
		componentFlag |= ComponentFlag::JA_LAYOUT_CHILD;
	}
	else
	{
		componentFlag |= ComponentFlag::JA_LAYOUT_CHILD;
		componentFlag ^= ComponentFlag::JA_LAYOUT_CHILD;
	}	
}

void Widget::setParent(Widget* parent)
{
	if(isLayoutChild())
		return;

	if(parent == nullptr)
	{
		this->parent = nullptr;
		setLayoutChild(false);
		return;
	}

	if(parent->isLayout())
	{
		setLayoutChild(true);
	}

	this->parent = parent;

	updateCollisionMask();
}

void Widget::setAsLayout( bool state )
{
	if(state)
	{
		componentFlag |= ComponentFlag::JA_LAYOUT;
	}
	else
	{
		componentFlag |= ComponentFlag::JA_LAYOUT;
		componentFlag ^= ComponentFlag::JA_LAYOUT;
	}	
}

void Widget::setSelectable(bool state)
{
	if(state)
	{
		componentFlag |= ComponentFlag::JA_SELECTABLE;
	}
	else
	{
		componentFlag |= ComponentFlag::JA_SELECTABLE;
		componentFlag ^= ComponentFlag::JA_SELECTABLE;
	}	
}

void Widget::setRenderBorder(bool state)
{
	if(state)
	{
		componentFlag |= ComponentFlag::JA_DRAWBORDER;
	}
	else
	{
		componentFlag |= ComponentFlag::JA_DRAWBORDER;
		componentFlag ^= ComponentFlag::JA_DRAWBORDER;
	}	
}

void Widget::draw(int32_t parentX, int32_t parentY)
{
	float myX = (float)(parentX + x);
	float myY = (float)(parentY + y);

	if(texBuffer != setup::graphics::TEXTURE_UNASSIGNED)
	{
		DisplayManager::beginDrawingSprites();
		//jaDisplayManager::drawSprite(texture,myX + imageOffsetX + imageOriginX, myY + imageOffsetY + imageOriginY, halfWidth, halfHeight, 0);
		DisplayManager::endDrawingSprites();
	}
};

void Widget::update()
{
	
};

void Widget::actionEvent(Widget* sender, WidgetEvent action, void* data)
{

}

void Widget::setPosition( int32_t x, int32_t y )
{
	if(isLayoutChild())
		return;

	this->x = x;
	this->y = y;

	updateCollisionMask();
	
	// if some code here is changed copy it to forcePosition
}

void Widget::getRelativePosition(int32_t& x, int32_t& y)
{
	x = this->x;
	y = this->y;
}

void Widget::getPosition(int32_t& x, int32_t& y)
{
	if(parent == nullptr) 
	{
		x = this->x;
		y = this->y;
	}
	else
	{
		Widget* parent = getParent();
		x = this->x;
		y = this->y;
		while(parent != nullptr)
		{
			x += parent->x;
			y += parent->y;
			parent = parent->getParent();
		}
	}
}

void Widget::getAbsolutePosition(int32_t& x, int32_t& y)
{

	Widget* parent = getParent();
	
	x = this->x;
	y = this->y;

	if (nullptr != parent)
	{
		int32_t parentX;
		int32_t parentY;

		parent->getTransformPosition(parentX, parentY);

		x += parentX;
		y += parentY;
	}
	
}

void Widget::getTransformPosition(int32_t& x, int32_t& y)
{
	Widget* parent = getParent();

	x = this->x;
	y = this->y;

	if (nullptr != parent)
	{
		int32_t parentX;
		int32_t parentY;

		parent->getTransformPosition(parentX, parentY);

		x += parentX;
		y += parentY;
	}
}

WidgetType Widget::getWidgetType()
{
	return (WidgetType)widgetType;
}

void Widget::setSize( int32_t width, int32_t height )
{
	if(isLayoutChild())
		return;

	this->width  = width;
	this->height = height;

	updateCollisionMask();

	updateSize();
	resizeTexBuffer();
}

int32_t Widget::getWidth()
{
	return width;
}

int32_t Widget::getHeight()
{
	return height;
}

bool Widget::isMouseOver()
{
	if(collisionMask.intersect(UIManager::mouseX,UIManager::mouseY))
	{
		return UIManager::isMouseOver(this);
	}

	return false;
}

bool Widget::removeReference()
{
	--references;

	if(references <= 0)
		return true;

	return false;
}

void Widget::updateTexBuffer()
{
	//set scissor test

	//glClear(GL_COLOR_BUFFER_BIT);
	glColor4f(1,1,1,0);
	glBegin(GL_QUADS);
		glVertex2i(0,0);
		glVertex2i(width,0);
		glVertex2i(width,height);
		glVertex2i(0,height);
	glEnd();

	draw(0,0);
	glBindTexture(GL_TEXTURE_2D,texBuffer);
	//glCopyTexSubImage2D(GL_TEXTURE_2D, 0, GL_RGBA,x,y, width, height);

	glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,0,0, width, height, 0);
}

void Widget::createTexBuffer()
{
	if((width == 0) && (height == 0))
		return;

	if(texBuffer != setup::graphics::TEXTURE_UNASSIGNED)
		glDeleteTextures(1,&texBuffer);


	glGenTextures(1,&texBuffer);
	glBindTexture(GL_TEXTURE_2D,texBuffer);
	

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); // zastosuj efekty �wiat�a na teksturze

	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST); // it repairs seams
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST); 

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // przytnij jak powy�ej jeden
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE,nullptr);
	
	updateTexBuffer();
}

void Widget::resizeTexBuffer()
{
	if (texBuffer != setup::graphics::TEXTURE_UNASSIGNED)
		createTexBuffer();
}

void Widget::deleteTexBuffer()
{
	if (texBuffer != setup::graphics::TEXTURE_UNASSIGNED)
	{
		glDeleteTextures(1,&texBuffer);
		texBuffer = setup::graphics::TEXTURE_UNASSIGNED;
	}
}

void Widget::forcePosition( int32_t x, int32_t y )
{
	this->x = x;
	this->y = y;

	updateCollisionMask();
}

void Widget::forceSize( int32_t width, int32_t height )
{
	this->width = width;
	this->height = height;

	if(this->width < 0)
		this->width = 1;

	if(this->height < 0)
		this->height = 1;

	updateCollisionMask();

	updateSize();
	resizeTexBuffer();
}

void Widget::setLayoutBehaviour( Widget* widget )
{

}

int8_t Widget::getReferenceNumber()
{
	return references;
}

void Widget::updateSize()
{

}

void Widget::setScriptGlobal( ja::string& globalName )
{
	lua_State* L = ScriptManager::getSingleton()->getLuaState();


	lua_getglobal(L,globalName.c_str());
	if(!lua_isnil(L,-1))
	{
		lua_pop(L,1);
		throw JA_EXCEPTIONR(ExceptionType::GUI,"Can't add script global value: duplicated name");
	}

	lua_pop(L,1);
	lua_pushlightuserdata(L,this);
	lua_setglobal(L,globalName.c_str());
}

bool Widget::isRadioButton()
{
	return false;
}

void Widget::setButtonGroup( Widget* widget )
{

}

void Widget::setCheck( bool state )
{

}

bool Widget::isChecked()
{
	return false;
}

void Widget::updateCollisionMask()
{
	if(parent)
	{
		int32_t posX,posY;

		parent->getTransformPosition(posX, posY);
		posX += x;
		posY += y;

		collisionMask.setMask(posX, posY, posX + width, posY + height);
		collisionMask.makeIntersection(parent->collisionMask);
	}
	else
	{
		collisionMask.setMask(x, y, x + width, y + height);
	}
}

WidgetMask Widget::getCollisionMask()
{
	return collisionMask;
}

void Widget::updateChildrenCollisionMask()
{

}

void Widget::setTopWidget(Widget* topWidget)
{
	
}

void ScriptDelegate::run(Widget* widget, WidgetEvent widgetEvent, void* data)
{
	lua_State* L = ScriptManager::getSingleton()->getLuaState();

	lua_getglobal(L, delegateName.c_str());

	if(lua_isfunction(L, 1))
	{
		lua_pushlightuserdata(L,widget);
		lua_pushinteger(L,static_cast<int>(widgetEvent));
		lua_pushlightuserdata(L,data);
		lua_call(L, 3, 0);
	}
	else
	{
		lua_remove(L,1);
	}
}
////////// jaWidgetListener

WidgetListener::WidgetListener(WidgetEvent listenerType) : listenerType(listenerType), listenerFlag(0), scriptCallback(nullptr)
{

}

bool WidgetListener::isActive()
{
	return listenerFlag & JA_ACTIVE;
}

void WidgetListener::setActive(bool state)
{
	if(state)
	{
		listenerFlag |= JA_ACTIVE;
	}
	else
	{
		listenerFlag |= JA_ACTIVE;
		listenerFlag ^= JA_ACTIVE;
	}
}

bool WidgetListener::processListener(Widget* widget)
{
	return false;
}

void WidgetListener::fireEvent(Widget* widget)
{

}

WidgetListener::~WidgetListener()
{

}

}

#pragma warning( default : 4482 )
#pragma warning( default : 4800 )