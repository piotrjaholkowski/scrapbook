#pragma once

#include "ImageButton.hpp"
#include "../Graphics/Texture2d.hpp"
#include "OnChangeListener.hpp"

namespace ja
{

	class ProgressButton : public ImageButton
	{
	protected:
		uint32_t widgetStyle;

		int32_t mousePosX, mousePosY;

		double changeValue;
		double previousAngle;
		double stepValue;

		OnChangeListener onChange;
	public:
		ProgressButton();
		ProgressButton(unsigned int id, Widget* parent);

		virtual void update();
		virtual void draw(int parentX, int parentY);
		void setStepValue(double stepValue);

		~ProgressButton();

		template < class Y, class X >
		void setOnChangeEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onChange.setActionEvent(pthis, pmethod);
			onChange.setActive(true);
		}
	};

}