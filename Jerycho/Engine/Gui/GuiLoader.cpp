#include "GuiLoader.hpp"

#include "../Manager/ResourceManager.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/UIManager.hpp"
#include "../Utility/Exception.hpp"

#include "Margin.hpp"
#include "Menu.hpp"
#include "Label.hpp"
#include "Checkbox.hpp"
#include "Button.hpp"
#include "RadioButton.hpp"
#include "Editbox.hpp"
#include "Scrollbar.hpp"
#include "ScrollPane.hpp"
#include "GridLayout.hpp"

using namespace ja;
using namespace tinyxml2;

GuiLoader* GuiLoader::singleton = nullptr;
std::map<ja::string,loadWidgetCallback> GuiLoader::registeredLoaders = std::map<ja::string,loadWidgetCallback>();
std::map<WidgetType,saveWidgetCallback> GuiLoader::registeredSavers = std::map<WidgetType,saveWidgetCallback>();

void GuiLoader::attachToParent(Widget* widget, Widget* parent)
{
	switch(parent->getWidgetType())
	{
	case JA_INTERFACE:
		{
			Interface* inter = (Interface*)parent;
			inter->addWidget(widget);
		}
		break;
	case JA_MENU:
		{
			Interface* inter = (Interface*)parent;
			inter->addWidget(widget);
		}
		break;
	case JA_GRID_LAYOUT:
		{
			GridLayout* grid = (GridLayout*)parent;
			WidgetDef* widgetDef = &UIManager::getSingleton()->widgetDef;
			grid->addWidget(widget,widgetDef->cellX,widgetDef->cellY);
		}
		break;
	case JA_SCROLL_PANE:
		{
			ScrollPane* scrollPane = (ScrollPane*)parent;
			scrollPane->addWidget(widget);
		}
		break;
	}
}

void GuiLoader::loadWidgetProperties(XMLElement* widgetElement)
{
	WidgetDef* widgetDef = &UIManager::getSingleton()->widgetDef;
	if(XML_NO_ERROR != widgetElement->QueryUnsignedAttribute(gui::tag::ID_ATTRIBUTE,&widgetDef->widgetId))
	{
		widgetDef->widgetId = 0;
	}

	const char* scriptGlobalName = widgetElement->Attribute(gui::tag::GLOBAL_ATTRIBUTE);
	if (scriptGlobalName != nullptr)
	{
		widgetDef->scriptGlobalName = scriptGlobalName;
		widgetDef->isScriptGlobal = true;
	}
	else
	{
		widgetDef->isScriptGlobal = false;
	}

	XMLElement* isRenderedProperty = widgetElement->FirstChildElement(gui::tag::IS_RENDERED_PROPERTY);
	if(isRenderedProperty)
	{
		isRenderedProperty->QueryBoolAttribute(gui::tag::STATE_ATTRIBUTE, &widgetDef->isRendered);
	}

	XMLElement* isActiveProperty = widgetElement->FirstChildElement(gui::tag::IS_ACTIVE_PROPERTY);
	if(isActiveProperty)
	{
		isActiveProperty->QueryBoolAttribute(gui::tag::STATE_ATTRIBUTE, &widgetDef->isActive);
	}

	XMLElement* positionProperty = widgetElement->FirstChildElement(gui::tag::POSITION_PROPERTY);
	if(positionProperty)
	{
		positionProperty->QueryIntAttribute(gui::tag::X_ATTRIBUTE, &widgetDef->positionX);
		positionProperty->QueryIntAttribute(gui::tag::Y_ATTRIBUTE, &widgetDef->positionY);

		float xp,yp;
		if (positionProperty->QueryFloatAttribute(gui::tag::XP_ATTRIBUTE, &xp) == XML_NO_ERROR)
		{
			float resolutionWidth = static_cast<float>(DisplayManager::getResolutionWidth());

			widgetDef->positionX = static_cast<int>((resolutionWidth * xp) / 100.0f);
		}

		if (positionProperty->QueryFloatAttribute(gui::tag::YP_ATTRIBUTE, &yp) == XML_NO_ERROR)
		{
			float resolutionHeight = static_cast<float>(DisplayManager::getResolutionHeight());

			widgetDef->positionY = static_cast<int>((resolutionHeight * yp) / 100.0f);
		}
	}

	XMLElement* fontNameElement = widgetElement->FirstChildElement(gui::tag::FONT_PROPERTY);
	if(fontNameElement)
	{
		const char* fontName = fontNameElement->Attribute(gui::tag::NAME_ATTRIBUTE);
		if (fontName != nullptr)
			widgetDef->fontName = fontName;
	}

	XMLElement* color = widgetElement->FirstChildElement(gui::tag::COLOR_PROPERTY);
	if(color)
	{
		uint32_t r,g,b,a;

		if (XML_NO_ERROR == color->QueryUnsignedAttribute(gui::tag::R_ATTRIBUTE, &r))
		{
			widgetDef->r = static_cast<unsigned char>(r);
		}

		if (XML_NO_ERROR == color->QueryUnsignedAttribute(gui::tag::G_ATTRIBUTE, &g))
		{
			widgetDef->g = static_cast<unsigned char>(g);
		}

		if (XML_NO_ERROR == color->QueryUnsignedAttribute(gui::tag::B_ATTRIBUTE, &b))
		{
			widgetDef->b = static_cast<unsigned char>(b);
		}

		if (XML_NO_ERROR == color->QueryUnsignedAttribute(gui::tag::A_ATTRIBUTE, &a))
		{
			widgetDef->a = static_cast<unsigned char>(a);
		}
	}

	XMLElement* alignProperty = widgetElement->FirstChildElement(gui::tag::ALIGN_PROPERTY);
	if(alignProperty)
	{
		uint32_t marginHorizontal;
		uint32_t marginVertical;

		alignProperty->QueryUnsignedAttribute(gui::tag::HORIZONTAL_ATTRIBUTE, &marginHorizontal);
		alignProperty->QueryUnsignedAttribute(gui::tag::VERTICAL_ATTRIBUTE, &marginVertical);

		switch(marginHorizontal)
		{
		case 0:
			widgetDef->align = (uint32_t)Margin::LEFT;
			break;
		case 1:
			widgetDef->align = (uint32_t)Margin::CENTER;
			break;
		case 2:
			widgetDef->align = (uint32_t)Margin::RIGHT;
			break;
		case 3:
			widgetDef->align = (uint32_t)Margin::LEFT;
			break;
		}

		switch(marginVertical)
		{
		case 0:
			widgetDef->align |= (uint32_t)Margin::DOWN;
			break;
		case 1:
			widgetDef->align |= (uint32_t)Margin::MIDDLE;
			break;
		case 2:
			widgetDef->align |= (uint32_t)Margin::TOP;
			break;
		case 3:
			widgetDef->align |= (uint32_t)Margin::NORMAL;
			break;
		}
	}

	XMLElement* sizeProperty = widgetElement->FirstChildElement(gui::tag::SIZE_PROPERTY);
	if(sizeProperty)
	{
		sizeProperty->QueryIntAttribute(gui::tag::WIDTH_ATTRIBUTE, &widgetDef->width);
		sizeProperty->QueryIntAttribute(gui::tag::HEIGHT_ATTRIBUTE, &widgetDef->height);

		float widthp,heightp;
		if (sizeProperty->QueryFloatAttribute(gui::tag::WIDTHP_ATTRIBUTE, &widthp) == XML_NO_ERROR)
		{
			float resolutionWidth = static_cast<float>(DisplayManager::getResolutionWidth());
			widgetDef->width = static_cast<int>((resolutionWidth * widthp) / 100.0f);
		}

		if (sizeProperty->QueryFloatAttribute(gui::tag::HEIGHTP_ATTRIBUTE, &heightp) == XML_NO_ERROR)
		{
			float resolutionHeight = static_cast<float>(DisplayManager::getResolutionHeight());
			widgetDef->height = static_cast<int>((resolutionHeight * heightp) / 100.0f);
		}
	}

	XMLElement* rangeProperty = widgetElement->FirstChildElement(gui::tag::RANGE_PROPERTY);
	if(rangeProperty)
	{
		rangeProperty->QueryDoubleAttribute(gui::tag::MIN_ATTRIBUTE, &widgetDef->minRange);
		rangeProperty->QueryDoubleAttribute(gui::tag::MAX_ATTRIBUTE, &widgetDef->maxRange);
		rangeProperty->QueryDoubleAttribute(gui::tag::CURRENT_ATTRIBUTE, &widgetDef->currentValue);
	}

	XMLElement* textProperty = widgetElement->FirstChildElement(gui::tag::TEXT_PROPERTY);
	if(textProperty)
	{
		const char* text = textProperty->GetText();
		if(text != nullptr)
		{
			widgetDef->text = text;
		}
		else
		{
			widgetDef->text = "";
		}
	}

	XMLElement* checkProperty = widgetElement->FirstChildElement(gui::tag::IS_CHECKED_PROPERTY);
	if(checkProperty)
	{
		checkProperty->QueryBoolAttribute(gui::tag::STATE_ATTRIBUTE, &widgetDef->isChecked);
	}

	XMLElement* gridSizeProperty = widgetElement->FirstChildElement(gui::tag::GRID_SIZE_PROPERTY);
	if(gridSizeProperty)
	{
		gridSizeProperty->QueryIntAttribute(gui::tag::BLOCKS_X_ATTRIBUTE, &widgetDef->blocksX);
		gridSizeProperty->QueryIntAttribute(gui::tag::BLOCKS_Y_ATTRIBUTE, &widgetDef->blocksY);
	}

	XMLElement* gapsProperty = widgetElement->FirstChildElement(gui::tag::GAPS_PROPERTY);
	if(gapsProperty)
	{
		gapsProperty->QueryIntAttribute(gui::tag::HORIZONTAL_ATTRIBUTE, &widgetDef->horizontalGap);
		gapsProperty->QueryIntAttribute(gui::tag::VERTICAL_ATTRIBUTE, &widgetDef->verticalGap);
	}

	XMLElement* buttonGroupProperty = widgetElement->FirstChildElement(gui::tag::BUTTON_GROUP_PROPERY);
	if(buttonGroupProperty)
	{
		int id;
		buttonGroupProperty->QueryIntAttribute(gui::tag::ID_ATTRIBUTE, &id);
		
		std::map<int,Widget*>::iterator it = GuiLoader::getSingleton()->buttonGroups.find(id);
		if(it != GuiLoader::getSingleton()->buttonGroups.end())
		{
			//widgetDef->buttonGroup = it;
			GuiLoader::getSingleton()->activeButtonGroup = it->second;
		}
	}

	XMLElement* cornerProperty = widgetElement->FirstChildElement(gui::tag::CORNER_PROPERTY);
	if(cornerProperty)
	{
		unsigned int corner;
		if (XML_NO_ERROR == cornerProperty->QueryUnsignedAttribute(gui::tag::TYPE_ATTRIBUTE, &corner))
		{
			widgetDef->scrollbarCorner = static_cast<WidgetCorner>(corner);
		}
	}

	XMLElement* scrollbarPolicyProperty = widgetElement->FirstChildElement(gui::tag::SCROLLBAR_POLICY_PROPERTY);
	if(scrollbarPolicyProperty)
	{
		unsigned int horizontal, vertical;
		if (XML_NO_ERROR == scrollbarPolicyProperty->QueryUnsignedAttribute(gui::tag::HORIZONTAL_ATTRIBUTE, &horizontal))
		{
			widgetDef->horizontalPolicy = static_cast<ScrollbarPolicy>(horizontal);
		}

		if (XML_NO_ERROR == scrollbarPolicyProperty->QueryUnsignedAttribute(gui::tag::VERTICAL_ATTRIBUTE, &vertical))
		{
			widgetDef->verticalPolicy = static_cast<ScrollbarPolicy>(vertical);
		}
	}

	XMLElement* scrollbarSize = widgetElement->FirstChildElement(gui::tag::SCROLLBAR_SIZE_PROPERTY);
	if(scrollbarSize)
	{
		scrollbarSize->QueryIntAttribute(gui::tag::SIZE_ATTRIBUTE, &widgetDef->scrollbarSize);
	}

	XMLElement* autoScrollHorizontal = widgetElement->FirstChildElement(gui::tag::AUTO_SCROLL_HORIZONTAL_PROPERTY);
	if(autoScrollHorizontal)
	{
		autoScrollHorizontal->QueryBoolAttribute(gui::tag::ENABLED_ATTRIBUTE, &widgetDef->autoScrollHorizontal);
		autoScrollHorizontal->QueryBoolAttribute(gui::tag::SCROLL_TO_LEFT_ATTRIBUTE, &widgetDef->scrollToLeft);
	}

	XMLElement* autoScrollVertical = widgetElement->FirstChildElement(gui::tag::AUTO_SCROLL_VERTICAL_PROPERTY);
	if(autoScrollVertical)
	{
		autoScrollVertical->QueryBoolAttribute(gui::tag::ENABLED_ATTRIBUTE, &widgetDef->autoScrollVertical);
		autoScrollVertical->QueryBoolAttribute(gui::tag::SCROLL_TO_TOP_ATTRIBUTE, &widgetDef->scrollToTop);
	}

	XMLElement* acceptNewLine = widgetElement->FirstChildElement(gui::tag::ACCEPT_NEW_LINE_PROPERTY);
	if(acceptNewLine)
	{
		acceptNewLine->QueryBoolAttribute(gui::tag::STATE_ATTRIBUTE, &widgetDef->acceptNewLine);
	}
}


void GuiLoader::loadWidget(XMLElement* widgetElement, Widget* parent)
{
	const char* elementName = widgetElement->Value();

	std::map<ja::string,loadWidgetCallback>::iterator it = GuiLoader::registeredLoaders.find(elementName);
	
	if(it != GuiLoader::registeredLoaders.end())
	{
		it->second(widgetElement,parent);
	}
}

void GuiLoader::loadMenu(XMLElement* menuElement, Widget* parent)
{
	loadWidgetProperties(menuElement);
	UIManager::getSingleton()->widgetDef.widgetType = JA_MENU;
	Menu* menu = (Menu*)UIManager::getSingleton()->createWidgetDef();

	XMLElement* events = menuElement->FirstChildElement(gui::tag::EVENTS_ELEMENT);
	if(events)
	{
		ja::string delegateName;
		ScriptDelegate* callback;

		XMLElement* onUpdate = events->FirstChildElement(gui::tag::ONUPDATE_EVENT);
		if(onUpdate)
		{
			delegateName = onUpdate->GetText();
			callback = UIManager::getSingleton()->registerDelegate(delegateName);
			menu->setOnUpdateEvent(callback);
		}
	}

	XMLElement* widgetsElement = menuElement->FirstChildElement(gui::tag::WIDGETS_ELEMENT);
	if(widgetsElement)
	{
		
		XMLElement* firstChild = widgetsElement->FirstChildElement();
		for(firstChild; firstChild; firstChild = firstChild->NextSiblingElement())
		{
			loadWidget(firstChild,menu);
		}
	}

	if(parent)
	{
		attachToParent(menu, parent);
	}
	else
	{
		UIManager::getSingleton()->addInterface(menu->getText(),menu);
	}
}

void GuiLoader::loadEditbox(XMLElement* editboxElement, Widget* parent)
{
	if(parent)
	{
		loadWidgetProperties(editboxElement);
		UIManager::getSingleton()->widgetDef.widgetType = JA_EDITBOX;
		Editbox* editbox = (Editbox*)UIManager::getSingleton()->createWidgetDef();
		attachToParent(editbox, parent);

		XMLElement* events = editboxElement->FirstChildElement(gui::tag::EVENTS_ELEMENT);

		if(events)
		{
			ja::string delegateName;
			ScriptDelegate* callback;

			XMLElement* onClick = events->FirstChildElement(gui::tag::ONCLICK_EVENT);
			if(onClick)
			{
				delegateName = onClick->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				editbox->setOnClickEvent(callback);
			}

			XMLElement* onMouseOver = events->FirstChildElement(gui::tag::ONMOUSEOVER_EVENT);
			if(onMouseOver)
			{
				delegateName = onMouseOver->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				editbox->setOnMouseOverEvent(callback);
			}

			XMLElement* onMouseLeave = events->FirstChildElement(gui::tag::ONMOUSELEAVE_EVENT);
			if(onMouseLeave)
			{
				delegateName = onMouseLeave->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				editbox->setOnMouseLeaveEvent(callback);
			}

			XMLElement* onSelect = events->FirstChildElement(gui::tag::ONSELECT_EVENT);
			if(onSelect)
			{
				delegateName = onSelect->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				editbox->setOnSelectEvent(callback);
			}

			XMLElement* onDeselect = events->FirstChildElement(gui::tag::ONDESELECT_EVENT);
			if(onDeselect)
			{
				delegateName = onDeselect->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				editbox->setOnDeselectEvent(callback);
			}
		}
	}
}

void GuiLoader::loadScrollbar(XMLElement* scrollbarElement, Widget* parent)
{
	if(parent)
	{
		loadWidgetProperties(scrollbarElement);
		UIManager::getSingleton()->widgetDef.widgetType = JA_SCROLLBAR;
		Scrollbar* scrollbar = (Scrollbar*)UIManager::getSingleton()->createWidgetDef();
		attachToParent(scrollbar, parent);

		XMLElement* events = scrollbarElement->FirstChildElement(gui::tag::EVENTS_ELEMENT);

		if(events)
		{
			ja::string delegateName;
			ScriptDelegate* callback;

			XMLElement* onClick = events->FirstChildElement(gui::tag::ONCLICK_EVENT);
			if(onClick)
			{
				delegateName = onClick->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				scrollbar->setOnClickEvent(callback);
			}

			XMLElement* onMouseOver = events->FirstChildElement(gui::tag::ONMOUSEOVER_EVENT);
			if(onMouseOver)
			{
				delegateName = onMouseOver->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				scrollbar->setOnMouseOverEvent(callback);
			}

			XMLElement* onMouseLeave = events->FirstChildElement(gui::tag::ONMOUSELEAVE_EVENT);
			if(onMouseLeave)
			{
				delegateName = onMouseLeave->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				scrollbar->setOnMouseLeaveEvent(callback);
			}

			XMLElement* onSelect = events->FirstChildElement(gui::tag::ONSELECT_EVENT);
			if(onSelect)
			{
				delegateName = onSelect->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				scrollbar->setOnSelectEvent(callback);
			}

			XMLElement* onDeselect = events->FirstChildElement(gui::tag::ONDESELECT_EVENT);
			if(onDeselect)
			{
				delegateName = onDeselect->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				scrollbar->setOnDeselectEvent(callback);
			}

			XMLElement* onChange = events->FirstChildElement(gui::tag::ONCHANGE_EVENT);
			if(onChange)
			{
				delegateName = onChange->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				scrollbar->setOnChangeEvent(callback);
			}
		}
	}
}


void GuiLoader::loadGridLayout(XMLElement* gridElement, Widget* parent)
{
	if(parent)
	{
		loadWidgetProperties(gridElement);
		UIManager::getSingleton()->widgetDef.widgetType = JA_GRID_LAYOUT;
		Widget* gridLayout = UIManager::getSingleton()->createWidgetDef();
		attachToParent(gridLayout, parent);

		XMLElement* cellsElement = gridElement->FirstChildElement(gui::tag::CELLS_ELEMENT);

		if(cellsElement)
		{
			XMLElement* cellElement = cellsElement->FirstChildElement(gui::tag::CELL_ELEMENT);
			WidgetDef* widgetDef = &UIManager::getSingleton()->widgetDef;

			for( cellElement; cellElement; cellElement = cellElement->NextSiblingElement())
			{
				cellElement->QueryIntAttribute(gui::tag::X_ATTRIBUTE, &widgetDef->cellX);
				cellElement->QueryIntAttribute(gui::tag::Y_ATTRIBUTE, &widgetDef->cellY);

				XMLElement* childElement = cellElement->FirstChildElement();
				
				if(childElement)
				{
					loadWidget(childElement,gridLayout);
				}
			}
		}

	}
}

void GuiLoader::loadButton(XMLElement* buttonElement, Widget* parent)
{
	if(parent)
	{
		loadWidgetProperties(buttonElement);
		UIManager::getSingleton()->widgetDef.widgetType = JA_BUTTON;
		Button* button = (Button*)UIManager::getSingleton()->createWidgetDef();
		attachToParent(button, parent);

		XMLElement* events = buttonElement->FirstChildElement(gui::tag::EVENTS_ELEMENT);

		if(events)
		{
			ja::string delegateName;
			ScriptDelegate* callback;

			XMLElement* onClick = events->FirstChildElement(gui::tag::ONCLICK_EVENT);
			if(onClick)
			{
				delegateName = onClick->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				button->setOnClickEvent(callback);
			}

			XMLElement* onMouseOver = events->FirstChildElement(gui::tag::ONMOUSEOVER_EVENT);
			if(onMouseOver)
			{
				delegateName = onMouseOver->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				button->setOnMouseOverEvent(callback);
			}

			XMLElement* onMouseLeave = events->FirstChildElement(gui::tag::ONMOUSELEAVE_EVENT);
			if(onMouseLeave)
			{
				delegateName = onMouseLeave->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				button->setOnMouseLeaveEvent(callback);
			}

			XMLElement* onSelect = events->FirstChildElement(gui::tag::ONSELECT_EVENT);
			if(onSelect)
			{
				delegateName = onSelect->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				button->setOnSelectEvent(callback);
			}

			XMLElement* onDeselect = events->FirstChildElement(gui::tag::ONDESELECT_EVENT);
			if(onDeselect)
			{
				delegateName = onDeselect->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				button->setOnDeselectEvent(callback);
			}
		}
	}
}

void GuiLoader::loadLabel(XMLElement* labelElement, Widget* parent)
{
	if(parent)
	{
		loadWidgetProperties(labelElement);
		UIManager::getSingleton()->widgetDef.widgetType = JA_LABEL;
		Label* label = (Label*)UIManager::getSingleton()->createWidgetDef();
		attachToParent(label, parent);

		XMLElement* events = labelElement->FirstChildElement(gui::tag::EVENTS_ELEMENT);

		if(events)
		{
			//ja::string delegateName;
			//jaScriptDelegate* callback;

		}
	}
}

void GuiLoader::loadCheckbox(XMLElement* checkboxElement, Widget* parent)
{
	if(parent)
	{
		loadWidgetProperties(checkboxElement);
		UIManager::getSingleton()->widgetDef.widgetType = JA_CHECKBOX;
		Checkbox* checkbox = (Checkbox*)UIManager::getSingleton()->createWidgetDef();
		attachToParent(checkbox, parent);

		XMLElement* events = checkboxElement->FirstChildElement(gui::tag::EVENTS_ELEMENT);

		if(events)
		{
			ja::string delegateName;
			ScriptDelegate* callback;

			XMLElement* onClick = events->FirstChildElement(gui::tag::ONCLICK_EVENT);
			if(onClick)
			{
				delegateName = onClick->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				checkbox->setOnClickEvent(callback);
			}

			XMLElement* onMouseOver = events->FirstChildElement(gui::tag::ONMOUSEOVER_EVENT);
			if(onMouseOver)
			{
				delegateName = onMouseOver->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				checkbox->setOnMouseOverEvent(callback);
			}

			XMLElement* onMouseLeave = events->FirstChildElement(gui::tag::ONMOUSELEAVE_EVENT);
			if(onMouseLeave)
			{
				delegateName = onMouseLeave->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				checkbox->setOnMouseLeaveEvent(callback);
			}

			XMLElement* onSelect = events->FirstChildElement(gui::tag::ONSELECT_EVENT);
			if(onSelect)
			{
				delegateName = onSelect->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				checkbox->setOnSelectEvent(callback);
			}

			XMLElement* onDeselect = events->FirstChildElement(gui::tag::ONDESELECT_EVENT);
			if(onDeselect)
			{
				delegateName = onDeselect->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				checkbox->setOnDeselectEvent(callback);
			}

			XMLElement* onChange = events->FirstChildElement(gui::tag::ONCHANGE_EVENT);
			if(onChange)
			{
				delegateName = onChange->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				checkbox->setOnChangeEvent(callback);
			}
		}
	}
}


void GuiLoader::loadRadioButton(XMLElement* radioButtonElement, Widget* parent)
{
	if(parent)
	{
		loadWidgetProperties(radioButtonElement);
		UIManager::getSingleton()->widgetDef.widgetType = JA_RADIO_BUTTON;
		RadioButton* radioButton = (RadioButton*)UIManager::getSingleton()->createWidgetDef();
		attachToParent(radioButton, parent);

		if(GuiLoader::getSingleton()->activeButtonGroup != nullptr)
		{
			ButtonGroup* buttonGroup = (ButtonGroup*)GuiLoader::getSingleton()->activeButtonGroup;
			buttonGroup->addButton(radioButton);

			GuiLoader::getSingleton()->activeButtonGroup = nullptr; // remove it from button group definition
		}

		XMLElement* events = radioButtonElement->FirstChildElement(gui::tag::EVENTS_ELEMENT);

		if(events)
		{
			ja::string delegateName;
			ScriptDelegate* callback;

			XMLElement* onClick = events->FirstChildElement(gui::tag::ONCLICK_EVENT);
			if(onClick)
			{
				delegateName = onClick->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				radioButton->setOnClickEvent(callback);
			}

			XMLElement* onMouseOver = events->FirstChildElement(gui::tag::ONMOUSEOVER_EVENT);
			if(onMouseOver)
			{
				delegateName = onMouseOver->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				radioButton->setOnMouseOverEvent(callback);
			}

			XMLElement* onMouseLeave = events->FirstChildElement(gui::tag::ONMOUSELEAVE_EVENT);
			if(onMouseLeave)
			{
				delegateName = onMouseLeave->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				radioButton->setOnMouseLeaveEvent(callback);
			}

			XMLElement* onSelect = events->FirstChildElement(gui::tag::ONSELECT_EVENT);
			if(onSelect)
			{
				delegateName = onSelect->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				radioButton->setOnSelectEvent(callback);
			}

			XMLElement* onDeselect = events->FirstChildElement(gui::tag::ONDESELECT_EVENT);
			if(onDeselect)
			{
				delegateName = onDeselect->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				radioButton->setOnDeselectEvent(callback);
			}

			XMLElement* onChange = events->FirstChildElement(gui::tag::ONCHANGE_EVENT);
			if(onChange)
			{
				delegateName = onChange->GetText();
				callback = UIManager::getSingleton()->registerDelegate(delegateName);
				radioButton->setOnChangeEvent(callback);
			}
		}
	}
}

void GuiLoader::loadButtonGroup(XMLElement* buttonGroupElement, Widget* parent)
{
	if(parent)
	{
		loadWidgetProperties(buttonGroupElement);
		UIManager::getSingleton()->widgetDef.widgetType = JA_BUTTON_GROUP;
		ButtonGroup* buttonGroup = (ButtonGroup*)UIManager::getSingleton()->createWidgetDef();
		attachToParent(buttonGroup, parent);

		GuiLoader::getSingleton()->buttonGroups[buttonGroup->getId()] = buttonGroup;

		XMLElement* events = buttonGroupElement->FirstChildElement(gui::tag::EVENTS_ELEMENT);

		if(events)
		{
			
		}
	}
}

void GuiLoader::loadScrollPane(XMLElement* scrollPaneElement, Widget* parent)
{
	if(parent)
	{
		loadWidgetProperties(scrollPaneElement);
		UIManager::getSingleton()->widgetDef.widgetType = JA_SCROLL_PANE;
		ScrollPane* scrollPane = (ScrollPane*)UIManager::getSingleton()->createWidgetDef();
		attachToParent(scrollPane, parent);

		XMLElement* widgetsElement = scrollPaneElement->FirstChildElement(gui::tag::WIDGETS_ELEMENT);
		if(widgetsElement)
		{
			XMLElement* firstChild = widgetsElement->FirstChildElement();
			for(firstChild; firstChild; firstChild = firstChild->NextSiblingElement())
			{
				loadWidget(firstChild,scrollPane);
			}
		}

		XMLElement* events = scrollPaneElement->FirstChildElement(gui::tag::EVENTS_ELEMENT);
		if(events)
		{

		}
	}
}


void GuiLoader::saveWidget(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{

	std::map<WidgetType,saveWidgetCallback>::iterator it = GuiLoader::registeredSavers.find(widget->getWidgetType());

	if(it != GuiLoader::registeredSavers.end())
	{
		it->second(doc, parentElement,widget);
	}
}

void GuiLoader::saveMenu(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* menuElement = doc->NewElement(gui::tag::MENU_ELEMENT);
	Menu* menu = (Menu*)widget;
	menuElement->SetAttribute(gui::tag::ID_ATTRIBUTE, widget->getId());

	//////Properties
	savePosition(doc, menuElement,widget);
	saveSize(doc, menuElement,widget);

	/////////////save childs
	std::vector<Widget*> menuWidgets;
	menu->getWidgets(menuWidgets);

	XMLElement* widgetsElement = doc->NewElement(gui::tag::WIDGETS_ELEMENT);

	std::vector<Widget*>::iterator it = menuWidgets.begin();
	for(it; it != menuWidgets.end(); ++it)
	{
		saveWidget(doc, menuElement,(*it));
	}

	menuElement->LinkEndChild(widgetsElement);
	/////////////save childs

	parentElement->LinkEndChild(menuElement);
}

void GuiLoader::saveButton(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* buttonElement = doc->NewElement(gui::tag::BUTTON_ELEMENT);
	Button* button = (Button*)widget;
	buttonElement->SetAttribute(gui::tag::ID_ATTRIBUTE, widget->getId());

	//////Properties
	savePosition(doc, buttonElement,widget);
	saveSize(doc, buttonElement,widget);

	parentElement->LinkEndChild(buttonElement);
}

void GuiLoader::saveLabel(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* labelElement = doc->NewElement(gui::tag::LABEL_ELEMENT);
	Label* label = (Label*)widget;
	labelElement->SetAttribute(gui::tag::ID_ATTRIBUTE, widget->getId());

	//////Properties
	savePosition(doc, labelElement,widget);
	saveSize(doc, labelElement,widget);

	parentElement->LinkEndChild(labelElement);
}

void GuiLoader::saveScrollbar(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* scrollbarElement = doc->NewElement(gui::tag::SCROLLBAR_ELEMENT);
	Scrollbar* scrollbar = (Scrollbar*)widget;
	scrollbarElement->SetAttribute(gui::tag::ID_ATTRIBUTE, widget->getId());

	//////Properties
	savePosition(doc, scrollbarElement,widget);
	saveSize(doc, scrollbarElement,widget);

	parentElement->LinkEndChild(scrollbarElement);
}

void GuiLoader::saveEditbox(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* editboxElement = doc->NewElement(gui::tag::EDITBOX_ELEMENT);
	Editbox* editbox = (Editbox*)widget;
	editboxElement->SetAttribute(gui::tag::ID_ATTRIBUTE, widget->getId());

	//////Properties
	savePosition(doc, editboxElement,widget);
	saveSize(doc, editboxElement,widget);

	parentElement->LinkEndChild(editboxElement);
}

void GuiLoader::saveGridLayout(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* gridLayoutElement = doc->NewElement(gui::tag::GRID_LAYOUT_ELEMENT);
	GridLayout* gridLayout = (GridLayout*)widget;
	gridLayoutElement->SetAttribute(gui::tag::ID_ATTRIBUTE, widget->getId());

	//////Properties
	savePosition(doc, gridLayoutElement,widget);
	saveSize(doc, gridLayoutElement,widget);

	parentElement->LinkEndChild(gridLayoutElement);
}

void GuiLoader::saveScrollPane(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* scrollPaneElement = doc->NewElement(gui::tag::SCROLL_PANE_ELEMENT);
	ScrollPane* scrollPane = (ScrollPane*)widget;
	scrollPaneElement->SetAttribute(gui::tag::ID_ATTRIBUTE, widget->getId());

	//////Properties
	savePosition(doc, scrollPaneElement,widget);
	saveSize(doc, scrollPaneElement,widget);

	parentElement->LinkEndChild(scrollPaneElement);
}

void GuiLoader::saveCheckbox(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* checkboxElement = doc->NewElement(gui::tag::CHECKBOX_ELEMENT);
	Checkbox* checkbox = (Checkbox*)widget;
	checkboxElement->SetAttribute(gui::tag::ID_ATTRIBUTE, widget->getId());

	//////Properties
	savePosition(doc, checkboxElement,widget);
	saveSize(doc, checkboxElement,widget);

	parentElement->LinkEndChild(checkboxElement);
}

void GuiLoader::saveRadioButton(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* radioButtonElement = doc->NewElement(gui::tag::RADIO_BUTTON_ELEMENT);
	RadioButton* radioButton = (RadioButton*)widget;
	radioButtonElement->SetAttribute(gui::tag::ID_ATTRIBUTE, widget->getId());

	//////Properties
	savePosition(doc, radioButtonElement,widget);
	saveSize(doc, radioButtonElement,widget);

	parentElement->LinkEndChild(radioButtonElement);
}

void GuiLoader::saveButtonGroup(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* buttonGroupElement = doc->NewElement(gui::tag::BUTTON_GROUP_ELEMENT);
	ButtonGroup* buttonGroup = (ButtonGroup*)widget;
	buttonGroupElement->SetAttribute(gui::tag::ID_ATTRIBUTE, widget->getId());

	parentElement->LinkEndChild(buttonGroupElement);
}

/////////////////////////////////////////////
/////////////////////////////////////////////

void GuiLoader::savePosition(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	int x,y;
	widget->getPosition(x,y);

	XMLElement* positionProperty = doc->NewElement(gui::tag::POSITION_PROPERTY);
	positionProperty->SetAttribute(gui::tag::X_ATTRIBUTE, x);
	positionProperty->SetAttribute(gui::tag::Y_ATTRIBUTE, y);

	parentElement->LinkEndChild(positionProperty);
}

void GuiLoader::savePosition(XMLDocument* doc, XMLElement* parentElement, int x, int y, bool usePercents)
{
	XMLElement* positionProperty = doc->NewElement(gui::tag::POSITION_PROPERTY);
	if(usePercents)
	{
		positionProperty->SetAttribute(gui::tag::XP_ATTRIBUTE, x);
		positionProperty->SetAttribute(gui::tag::YP_ATTRIBUTE, y);
	}
	else
	{
		positionProperty->SetAttribute(gui::tag::X_ATTRIBUTE, x);
		positionProperty->SetAttribute(gui::tag::Y_ATTRIBUTE, y);
	}
	parentElement->LinkEndChild(positionProperty);
}

void GuiLoader::saveSize(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* sizeProperty = doc->NewElement(gui::tag::SIZE_PROPERTY);
	sizeProperty->SetAttribute(gui::tag::WIDTH_ATTRIBUTE, widget->getWidth());
	sizeProperty->SetAttribute(gui::tag::HEIGHT_ATTRIBUTE, widget->getHeight());

	parentElement->LinkEndChild(sizeProperty);
}

void GuiLoader::saveSize(XMLDocument* doc, XMLElement* parentElement, int width, int height, bool usePercents)
{
	XMLElement* sizeProperty = doc->NewElement(gui::tag::SIZE_PROPERTY);
	if(usePercents)
	{
		sizeProperty->SetAttribute(gui::tag::WIDTHP_ATTRIBUTE, width);
		sizeProperty->SetAttribute(gui::tag::HEIGHTP_ATTRIBUTE, height);
	}
	else
	{
		sizeProperty->SetAttribute(gui::tag::WIDTH_ATTRIBUTE, width);
		sizeProperty->SetAttribute(gui::tag::HEIGHT_ATTRIBUTE, height);
	}
	parentElement->LinkEndChild(sizeProperty);
}

void GuiLoader::saveText(XMLDocument* doc, XMLElement* parentElement, ja::string text)
{
	XMLElement* textProperty = doc->NewElement(gui::tag::TEXT_PROPERTY);
	//textProperty->SetAttribute(textProperty, text);
	XMLText* textElement = doc->NewText(text.c_str());
	textProperty->LinkEndChild(textElement);

	parentElement->LinkEndChild(textProperty);
}

void GuiLoader::saveIsRendered(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* isRenderedProperty = doc->NewElement(gui::tag::IS_RENDERED_PROPERTY);
	isRenderedProperty->SetAttribute(gui::tag::STATE_ATTRIBUTE, widget->isRendered());
	parentElement->LinkEndChild(isRenderedProperty);
}

void GuiLoader::saveIsRendered(XMLDocument* doc, XMLElement* parentElement, bool isRendered)
{
	XMLElement* isRenderedProperty = doc->NewElement(gui::tag::IS_RENDERED_PROPERTY);
	isRenderedProperty->SetAttribute(gui::tag::STATE_ATTRIBUTE, isRendered);
	parentElement->LinkEndChild(isRenderedProperty);
}

void GuiLoader::saveIsActive(XMLDocument* doc, XMLElement* parentElement, Widget* widget)
{
	XMLElement* isActiveProperty = doc->NewElement(gui::tag::IS_ACTIVE_PROPERTY);
	isActiveProperty->SetAttribute(gui::tag::IS_ACTIVE_PROPERTY, widget->isActive());
	parentElement->LinkEndChild(isActiveProperty);
}

void GuiLoader::saveIsActive(XMLDocument* doc, XMLElement* parentElement, bool isActive)
{
	XMLElement* isActiveProperty = doc->NewElement(gui::tag::IS_ACTIVE_PROPERTY);
	isActiveProperty->SetAttribute(gui::tag::IS_ACTIVE_PROPERTY, isActive);
	parentElement->LinkEndChild(isActiveProperty);
}

void GuiLoader::saveGridSize(XMLDocument* doc, XMLElement* parentElement, int blocksX, int blocksY)
{
	XMLElement* gridSize = doc->NewElement(gui::tag::GRID_SIZE_PROPERTY);
	gridSize->SetAttribute(gui::tag::BLOCKS_X_ATTRIBUTE, blocksX);
	gridSize->SetAttribute(gui::tag::BLOCKS_Y_ATTRIBUTE, blocksY);
	parentElement->LinkEndChild(gridSize);
}

/*void saveId( XMLElement* parentElement, jaWidget* widget )
{
	parentElement->SetAttribute(JA_ID_ATTRIBUTE, widget->getId());
}*/

void GuiLoader::saveId(XMLDocument* doc, XMLElement* parentElement, unsigned int id)
{
	parentElement->SetAttribute(gui::tag::ID_ATTRIBUTE, id);
}

GuiLoader::GuiLoader()
{
	//cellX = 0;
	//cellY = 0;
	registerWidgetLoaders();
	registerWidgetSavers();
}

void GuiLoader::registerWidgetLoaders()
{
	registeredLoaders[gui::tag::MENU_ELEMENT] = loadMenu;
	registeredLoaders[gui::tag::GRID_LAYOUT_ELEMENT] = loadGridLayout;
	registeredLoaders[gui::tag::BUTTON_ELEMENT] = loadButton;
	registeredLoaders[gui::tag::LABEL_ELEMENT] = loadLabel;
	registeredLoaders[gui::tag::EDITBOX_ELEMENT] = loadEditbox;
	registeredLoaders[gui::tag::SCROLLBAR_ELEMENT] = loadScrollbar;
	registeredLoaders[gui::tag::CHECKBOX_ELEMENT] = loadCheckbox;
	registeredLoaders[gui::tag::RADIO_BUTTON_ELEMENT] = loadRadioButton;
	registeredLoaders[gui::tag::BUTTON_GROUP_ELEMENT] = loadButtonGroup;
	registeredLoaders[gui::tag::SCROLL_PANE_ELEMENT] = loadScrollPane;
}



void GuiLoader::registerWidgetSavers()
{
	registeredSavers[JA_MENU] = saveMenu;
	registeredSavers[JA_GRID_LAYOUT] = saveGridLayout;
	registeredSavers[JA_BUTTON] = saveButton;
	registeredSavers[JA_LABEL] = saveLabel;
	registeredSavers[JA_EDITBOX] = saveEditbox;
	registeredSavers[JA_SCROLLBAR] = saveScrollbar;
	registeredSavers[JA_CHECKBOX] = saveCheckbox;
	registeredSavers[JA_RADIO_BUTTON] = saveRadioButton;
	registeredSavers[JA_BUTTON_GROUP] = saveButtonGroup;
	registeredSavers[JA_SCROLL_PANE] = saveScrollPane;
	
}

GuiLoader::~GuiLoader()
{

}

GuiLoader* GuiLoader::getSingleton()
{
	return singleton;
}

void GuiLoader::init()
{
	if(singleton==nullptr)
	{
		singleton = new GuiLoader();
	}
}

void GuiLoader::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void GuiLoader::loadGui( const ja::string& fileName )
{
	XMLDocument doc;

	XMLError status = doc.LoadFile(fileName.c_str());
	if (status == XML_NO_ERROR)
	{
		singleton->buttonGroups.clear(); // clean previous button groups

		XMLElement* guiElement = doc.RootElement();
		XMLElement* firstChild = guiElement->FirstChildElement();

		for(firstChild;firstChild;firstChild = firstChild->NextSiblingElement())
		{
			loadWidget(firstChild,nullptr);
		}
	
	}
	else
	{
		std::cout << "Failed to load " << fileName  << " due to: " << std::endl;
		std::cout << "Parsing file " << fileName.c_str() << " error: " << status << std::endl;
		std::cout << "Error string 1: " << doc.GetErrorStr1() << std::endl;
		std::cout << "Error string 2: " << doc.GetErrorStr2() << std::endl;
	}
}

void GuiLoader::saveGui( ja::string& interfaceName, ja::string& fileName)
{
	XMLDocument doc;
	//TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );

	XMLElement* guiElement = doc.NewElement(gui::tag::GUI_ELEMENT);
	Widget* menuInterface = (Widget*)UIManager::getSingleton()->getInterface(interfaceName);

	saveWidget(&doc, guiElement, menuInterface);

	doc.LinkEndChild(guiElement);
	doc.SaveFile(fileName.c_str());
}