#include "Interface.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/UIManager.hpp"
#include "../Utility/Exception.hpp"

using namespace ja;

Interface::Interface(uint32_t id, Widget* parent) : Widget(JA_INTERFACE,id,parent)
{
	setRender(true);
	setActive(true);

	collisionMask.setMask(INT_MIN,INT_MIN,INT_MAX,INT_MAX);
}


void Interface::draw()
{
	if(isRendered())
	{
		uint32_t widgetsNum = widgets.size();

		DisplayManager* display = DisplayManager::getSingleton();
		display->pushScreenCoordinate();
		display->clearWorldMatrix();

		for(uint32_t i=0; i<widgetsNum; ++i)
		{
			if(widgets[i]->isRendered())
			{
				widgets[i]->draw(x,y);
			}
		}

		display->popWorldCoordinate();
	}
}

void Interface::draw( int32_t parentX, int32_t parentY )
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;

	if(parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	if(isRendered())
	{
		uint32_t widgetsNum = widgets.size();
		DisplayManager* display = DisplayManager::getSingleton();
		display->pushScreenCoordinate();
		display->clearWorldMatrix();
		for(uint32_t i=0; i<widgetsNum; ++i)
		{
			if(widgets[i]->isRendered())
			{
				widgets[i]->draw(myX,myY);
			}
		}
		display->popWorldCoordinate();
	}
}

void Interface::update()
{
	int32_t widgetsNum = widgets.size();

	if(isActive())
	{
		for(int32_t i=widgetsNum - 1; i >= 0; --i)
		{
			if(widgets[i]->isActive())
			{
				widgets[i]->update();
				// don't update any other widgets if this
				// widget is modal
				if(widgets[i]->isModal()) 
				{
					break;
				}
			}
		}
	}

	onUpdate.processListener(this);
}

Interface::~Interface()
{
	uint32_t widgetsNum = widgets.size();
	for(uint32_t i=0; i<widgetsNum; ++i)
	{
		if(widgets[i]->removeReference())
		{
			delete widgets[i];
		}
	}

	widgets.clear();
}

void Interface::addWidget(Widget* widget)
{
	widget->zOrder = 0; // Highest priority

	uint32_t widgetNum = widgets.size();
	for(uint32_t i=0; i<widgetNum; ++i)
	{
		++widgets[i]->zOrder;
	}

	widgets.push_back(widget);
	widget->setParent(this);
	++widget->references;
}

void Interface::updateSize()
{
	
}

Widget* Interface::getWidget( uint32_t id )
{
	uint32_t widgetsNum = widgets.size();
	for(uint32_t i=0; i<widgetsNum; ++i)
	{
		if(widgets[i]->id == id)
			return widgets[i];
	}

	return nullptr;
}

void Interface::setPosition( int32_t x, int32_t y )
{
	Widget::setPosition(x,y);

	if(parent == nullptr)
	{
		collisionMask.setMask(INT_MIN,INT_MIN,INT_MAX,INT_MAX);
	}
}

void Interface::setSize( int32_t width, int32_t height )
{
	Widget::setSize(width,height);

	/*if(parent == NULL)
	{
		collisionMask.setMask(INT_MIN,INT_MIN,INT_MAX,INT_MAX);
	}*/
}

void Interface::getWidgets( std::vector<Widget*>& widgetList )
{
	uint32_t widgetsNum = widgets.size();
	for(uint32_t i=0; i<widgetsNum; ++i)
	{
		widgetList.push_back(widgets[i]);
	}
}

void Interface::setTopWidget(Widget* topWidget)
{
	uint32_t widgetsNum = widgets.size();
	bool replaceWidget = false;

	for(uint32_t i=0; i<widgetsNum; ++i)
	{
		if(replaceWidget)
		{
			++widgets[i]->zOrder;
			widgets[i-1] = widgets[i];
			widgets[i] = topWidget;
		}
		else
		{
			if(widgets[i] == topWidget)
			{
				widgets[i]->zOrder = 0;
				replaceWidget = true;
			}
		}
	}
}