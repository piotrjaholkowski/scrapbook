#pragma once

#include "Widget.hpp"
#include "../Manager/InputManager.hpp"
#include "../Manager/UIManager.hpp"

namespace ja
{

	class OnClickListener : public WidgetListener
	{
	public:
		uint8_t wasPressed; // was mouse button pressed
		uint8_t wasClicked; // was mouse button pressed and released
		int32_t activateByKey;

		OnClickListener() : WidgetListener(JA_ONCLICK), wasPressed(0), wasClicked(0), activateByKey(JA_RETURN)
		{

		}

		inline void setPressedMouseFlags(uint8_t& mouseFlags)
		{
			if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
			{
				mouseFlags = mouseFlags | 1;
			}

			if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_RIGHT))
			{
				mouseFlags = mouseFlags | 2;
			}
		}

		inline void setReleasedMouseFlags(uint8_t& mouseFlags)
		{
			if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
			{
				mouseFlags = mouseFlags | 1;
			}

			if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_RIGHT))
			{
				mouseFlags = mouseFlags | 2;
			}
		}

		bool processListener(Widget* widget)
		{
			if (isActive())
				if (widget->isActive())
				{
					uint8_t pressedMouseFlags = 0;
					setPressedMouseFlags(pressedMouseFlags);

					if (pressedMouseFlags)
					{
						if (widget->isMouseOver())
						{
							wasPressed = pressedMouseFlags;
							wasClicked = 0;
							if (widget->isDragable() && (wasPressed & 1))
								UIManager::setDragged(widget);
							return false;
						}
					}

					uint8_t releasedMouseFlags = 0;
					setReleasedMouseFlags(releasedMouseFlags);

					if (releasedMouseFlags & wasPressed)
					{
						if (widget->isMouseOver())
						{
							wasClicked = releasedMouseFlags & wasPressed;
							wasPressed = 0;
							UIManager::setSelected(widget);
							fireEvent(widget);
							return true;
						}
						wasPressed = 0;
						wasClicked = 0;
					}

					if (InputManager::isKeyPressed(activateByKey))
					{
						if (UIManager::getLastSelected() == widget)
						{
							wasPressed = 0;
							wasClicked = 4;
							UIManager::setSelected(widget);
							fireEvent(widget);
							return true;
						}
					}
				}
			wasClicked = 0;
			return false;
		}

		void fireEvent(Widget* widget)
		{
			UIManager::getSingleton()->setLastClicked(widget);

			if (callback)
				callback(widget, JA_ONCLICK, &wasClicked);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONCLICK, &wasClicked);
		}

		~OnClickListener()
		{

		}
	};

}