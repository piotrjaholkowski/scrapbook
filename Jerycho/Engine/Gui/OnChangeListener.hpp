#pragma once

#include "Widget.hpp"
#include "../Manager/InputManager.hpp"
#include "../Manager/UIManager.hpp"

#include <iostream>

namespace ja
{

	class OnChangeListener : public WidgetListener
	{
	public:
		bool stateChanged; // was mouse button pressed
		void* change;

		OnChangeListener() : WidgetListener(JA_ONCHANGE)
		{
			change = nullptr;
			stateChanged = false;
		}

		bool processListener(Widget* widget)
		{
			if (isActive())
				if (widget->isActive())
				{
					if (stateChanged)
					{
						if (change != nullptr)
							fireEvent(widget);
						stateChanged = false;
						return true;
					}
				}
			return false;
		}

		void setChange(void* change)
		{
			stateChanged = true;
			this->change = change;
		}

		void fireEvent(Widget* widget)
		{
			if (callback)
				callback(widget, JA_ONCHANGE, change);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONCHANGE, change);
		}

		~OnChangeListener()
		{

		}
	};

}