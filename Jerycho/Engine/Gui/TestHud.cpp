#include "TestHud.hpp"

namespace ja
{

TestHud* TestHud::singleton = nullptr;

TestHud::TestHud()
{

}

TestHud::~TestHud()
{

}

TestHud* TestHud::getSingleton()
{
	return singleton;
}

void TestHud::initTestHud(const char* fontName, uint32_t fontSize)
{
	if (singleton == nullptr)
	{
		singleton = new TestHud();
		singleton->setFont(fontName, fontSize, jaLeftLowerCorner, 255, 255, 255, 255);
		singleton->setFont(fontName, fontSize, jaRightLowerCorner, 255, 255, 255, 255);
		singleton->setFont(fontName, fontSize, jaLeftUpperCorner, 255, 255, 255, 255);
		singleton->setFont(fontName, fontSize, jaRightUpperCorner, 255, 255, 255, 255);
	}
}

void TestHud::cleanUp()
{
	delete singleton;
}
	
}