#include "Menu.hpp"
#include "Layout.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/ResourceManager.hpp"

namespace ja
{

Menu::Menu(uint32_t id, Widget* parent) : Interface(id, parent)
{
	widgetType = JA_MENU;
}

void Menu::setColor(uint8_t r, uint8_t g, uint8_t b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}

void Menu::setText(const char* text)
{
	this->text = text;
}

void Menu::setAlign(uint32_t marginStyle)
{
	this->marginStyle = marginStyle;
}

void Menu::setFont(const char* fontName, uint32_t fontSize)
{
	ResourceManager* resource = ResourceManager::getSingleton();
	this->font = resource->getEternalFont(fontName, fontSize)->resource;
}

Menu::~Menu()
{

}

ja::string Menu::getText()
{
	return text;
}

void Menu::updateCollisionMask()
{
	Widget::updateCollisionMask();
	updateChildrenCollisionMask();
}

void Menu::updateChildrenCollisionMask()
{
	int32_t positionX, positionY;
	int32_t childX, childY;
	Widget* widget;

	if (isLayoutChild())
	{
		Layout* layout = static_cast<Layout*>(parent);
		layout->getOriginPosition(positionX, positionY);
		positionX += x;
		positionY += y;
	}
	else
	{
		getPosition(positionX, positionY); //doesn't change position if on the scroll pane
	}

	uint32_t widgetsNum = widgets.size();
	for (uint32_t i = 0; i<widgetsNum; ++i)
	{
		widget = widgets[i];
		widget->getRelativePosition(childX, childY);
		childX += positionX;
		childY += positionY;

		widget->collisionMask.setMask(childX, childY, childX + widget->getWidth(), childY + widget->getHeight());
		widget->collisionMask.makeIntersection(this->collisionMask);

		widget->updateChildrenCollisionMask();
	}

}

}