#include "Scrollbar.hpp"

#include "../Manager/DisplayManager.hpp"
#include "../Manager/ResourceManager.hpp"

#include <iostream>
#include <string>

namespace ja
{

Scrollbar::Scrollbar() : Widget(JA_SCROLLBAR,0,nullptr)
{
	r = g = b = 0;
	a = 255;
	setRender(true);
	setActive(true);
	setSelectable(true);
	onClick.setActive(true);
	isHorizontalScroll = true;

	this->currentValue = 0.0;
	this->biggestValue = 0.0;
	this->lowestValue  = 0.0;
	this->windowRange  = 0.0;
}

Scrollbar::Scrollbar(uint32_t id, Widget* parent) : Widget(JA_SCROLLBAR,id,parent)
{
	r = g = b = 0;
	a = 255;
	setRender(true);
	setActive(true);
	setSelectable(true);
	onClick.setActive(true);
	isHorizontalScroll = true;

	this->currentValue = 0.0f;
	this->biggestValue = 0.0f;
	this->lowestValue  = 0.0f;
	this->windowRange = 0.0;
}

void Scrollbar::draw(int32_t parentX, int32_t parentY)
{
	float myX = (float)(parentX + x);
	float myY = (float)(parentY + y);

	if(parent == nullptr)
	{
		myX = (float)(x);
		myY = (float)(y);
	}

	if(!collisionMask.canIntersect) // scissors region invalidate whole rendering area
		return;

	DisplayManager::setLineWidth(0.1f);
	glEnable(GL_SCISSOR_TEST);
	glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);

		DisplayManager::setColorUb(r,g,b,a);
		if(a != 255)
		{
			DisplayManager::enableAlphaBlending();
			DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, true);
			DisplayManager::disableAlphaBlending();
		}
		else
		{
			DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, true);
		}

		int16_t beginSlideArea;
		int16_t endSlideArea;

		if (this->windowRange != 0.0)
		{
			double range = this->biggestValue - this->lowestValue;

			int16_t windowsNum  = (int16_t)(ceil(range / windowRange));
			int16_t windowId    = (int16_t)(floor(this->currentValue / windowRange));
			int16_t slideLength = this->endPixel - this->beginPixel;

			if (0 == windowsNum)
				windowsNum = 1;

			int16_t slideArea = slideLength / windowsNum;

			if (windowId == windowsNum)
				--windowId;

			beginSlideArea = beginPixel + windowId * slideArea;
			endSlideArea   = beginSlideArea + slideArea;
			if (endSlideArea > (endPixel))
				endSlideArea = endPixel;
		}

		if(isHorizontalScroll)
		{
			DisplayManager::setColorUb(0,0,0,255);
			DisplayManager::drawLine(myX + beginPixel,myY + middleOpposite, myX + endPixel, myY + middleOpposite);

			if (this->windowRange == 0.0)
			{
				DisplayManager::setColorUb(255, 255, 255, 255);
				DisplayManager::drawLine(myX + currentPixel, myY, myX + currentPixel, myY + height);
			}
			else
			{
				DisplayManager::setColorUb(255, 255, 255, 255);
				DisplayManager::drawRectangle(myX + beginSlideArea, myY, myX + endSlideArea, myY + height, true);
			}
		}
		else
		{
			DisplayManager::setColorUb(0,0,0,255);
			DisplayManager::drawLine(myX + middleOpposite,myY + beginPixel, myX + middleOpposite, myY + endPixel);

			if (this->windowRange == 0.0)
			{
				DisplayManager::setColorUb(255, 255, 255, 255);
				DisplayManager::drawLine(myX, myY + currentPixel, myX + width, myY + currentPixel);
			}
			else
			{
				DisplayManager::setColorUb(255, 255, 255, 255);
				DisplayManager::drawRectangle(myX, myY + beginSlideArea, myX + width, myY + endSlideArea, true);
			}
		}

		if(this->isBorderRendered())
		{
			DisplayManager::setColorUb(borderR,borderG,borderB,borderA);
			DisplayManager::drawRectangle(myX + 1, myY + 1, myX + width - 1, myY + height - 1, false);
		}

	glDisable(GL_SCISSOR_TEST);
}

void Scrollbar::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

void Scrollbar::setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->borderR = r;
	this->borderG = g;
	this->borderB = b;
	this->borderA = a;
}

void Scrollbar::updateSize()
{
	if(isHorizontalScroll)
		setHorizontal();
	else
		setVertical();

	setValue(this->currentValue); // sets current pixel
}

void Scrollbar::update()
{      
	onMouseOver.processListener(this);
	onMouseLeave.processListener(this);
	if(onClick.processListener(this))
	{
		UIManager::getSingleton()->setSelected(this);
	}

	if(onClick.wasPressed)
	{
		uint16_t lastData = currentPixel;
		int32_t  mouseX = InputManager::getMousePositionX();
		int32_t  mouseY = DisplayManager::getSingleton()->getResolutionHeight() - InputManager::getMousePositionY();

		int32_t x,y;
		getPosition(x,y);

		if(isHorizontalScroll)
			currentPixel = mouseX - x;
		else
			currentPixel = mouseY - y;

		if(currentPixel < beginPixel)
			currentPixel = beginPixel;
		else
			if(currentPixel > endPixel)
				currentPixel = endPixel;

		if(lastData!=currentPixel)
		{
			updateValue(); // sets current value respect to current pixel
			onChange.setChange(&currentPixel);
		}
	}

	onChange.processListener(this);
	onSelect.processListener(this);
	onDeselect.processListener(this);
}

void Scrollbar::setAlpha( uint8_t alpha )
{
	a = alpha;
}

void Scrollbar::setHorizontal()
{
	isHorizontalScroll = true;

	beginPixel     = 5;
	endPixel       = width - 5;
	middleOpposite = height / 2; // for drawing middle line
	//from left to right
}

void Scrollbar::setVertical()
{
	isHorizontalScroll = false;

	beginPixel     = 5;
	endPixel       = height - 5;
	middleOpposite = width / 2; // for drawing middle line
	//from down to top
}

void Scrollbar::setRange( double lowestValue, double biggestValue )
{
	if(biggestValue < lowestValue)
	{
		isRaisingRange = false;
		this->lowestValue = biggestValue;
		this->biggestValue = lowestValue;
	}
	else
	{
		isRaisingRange = true;
		this->lowestValue = lowestValue;
		this->biggestValue = biggestValue;
	}

}

void Scrollbar::setWindowRange(double windowRange)
{
	this->windowRange = windowRange;
}

void Scrollbar::setValue( double currentValue )
{
	if(currentValue < lowestValue)
		currentValue = lowestValue;
	else
		if(currentValue > biggestValue)
			currentValue = biggestValue;

	double interpolation;

	if(biggestValue == lowestValue)
	{
		interpolation = 0;
	}
	else
	{
		interpolation = (currentValue - lowestValue) / (biggestValue - lowestValue);
	}
	
	if(!isRaisingRange)
		interpolation = -(interpolation - 1);

	currentPixel = beginPixel +  (int16_t)(interpolation * (endPixel - beginPixel)); 
	this->currentValue = currentValue;
}

double Scrollbar::getValue()
{
	return currentValue;
}

void Scrollbar::updateValue()
{
	double interpolation = (double)((currentPixel - beginPixel)) / (double)((endPixel -beginPixel));

	if(!isRaisingRange)
		interpolation = -(interpolation - 1);

	currentValue = (lowestValue + ((biggestValue - lowestValue) * interpolation));
}

bool Scrollbar::isHorizontal()
{
	return isHorizontalScroll;
}

}