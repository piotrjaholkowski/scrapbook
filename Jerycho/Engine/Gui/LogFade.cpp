#include "LogFade.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/ResourceManager.hpp"
#include "../Manager/UIManager.hpp"
#include "../Gui/Margin.hpp"
#include "../Utility/Exception.hpp"

#include <iostream>
#include <string>

namespace ja
{
	LogFade::LogFade(uint32_t id, Widget* parent) : Widget(JA_LOG_FADE, id, parent)
	{
		setRender(true);
		setActive(true);
		setSelectable(false);

		this->align = (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE;
		setFont("default.ttf",10);

		this->isWorldMatrix = false;
		rS, gS, gS, aS = 0;
		this->shadowOffsetX = 0;
		this->shadowOffsetY = 0;
		this->worldScaleMultiplier = 1.0f;
	}

	void LogFade::setFont(const char* fontName, uint32_t fontSize)
	{
		ResourceManager* resource = ResourceManager::getSingleton();
		this->font = resource->getEternalFont(fontName, fontSize)->resource;
	}

	void LogFade::setFadeTime(float fadeTime)
	{
		this->fadeTime = fadeTime;
	}

	void LogFade::setVerticalLineSpace(uint16_t verticalLineSpace)
	{
		this->verticalLineSpace = verticalLineSpace;
	}

	void LogFade::draw(int32_t parentX, int32_t parentY)
	{
		int32_t myX = parentX + x;
		int32_t myY = parentY + y;

		if (parent == nullptr)
		{
			myX = x;
			myY = y;
		}

		onBeginDraw.processListener(this);
		if (isWorldMatrix)
		{		
			DisplayManager::pushWorldMatrix();			
			std::deque<LogFadeData>::iterator itFade = logFadeData.begin();
			float shadowXScaled           = (this->shadowOffsetX * this->worldScaleMultiplier);
			float shadowYScaled           = (this->shadowOffsetY * this->worldScaleMultiplier);
			float verticalLineSpaceScaled = (this->verticalLineSpace * this->worldScaleMultiplier);

			float myYf = (float)myY;

			for (itFade; itFade != logFadeData.end(); ++itFade)
			{
				jaFloat alphaLinear = itFade->fadeTime / this->fadeTime;
				uint8_t alpha       = (uint8_t)(255.0f * alphaLinear);

				if (aS != 0)
				{
					uint8_t alphaShadow = (uint8_t)(aS * alphaLinear);
					DisplayManager::RenderTextWorld(font, myX + shadowXScaled, myYf + shadowYScaled, this->worldScaleMultiplier, itFade->info, align, rS, gS, bS, alphaShadow);
				}

				DisplayManager::RenderTextWorld(font, myX, myYf, worldScaleMultiplier, itFade->info, align, itFade->r, itFade->g, itFade->b, alpha);

				myYf += verticalLineSpaceScaled;
			}
			DisplayManager::popWorldMatrix();
		}
		else
		{
			std::deque<LogFadeData>::iterator itFade = logFadeData.begin();
			for (itFade; itFade != logFadeData.end(); ++itFade)
			{
				jaFloat alphaLinear = itFade->fadeTime / this->fadeTime;
				uint8_t alpha = (uint8_t)(255.0f * alphaLinear);

				if (aS != 0)
				{
					uint8_t alphaShadow = (uint8_t)(aS * alphaLinear);
					DisplayManager::RenderText(font, myX + shadowOffsetX, myY + shadowOffsetY, itFade->info, align, rS, gS, bS, alphaShadow);
				}

				DisplayManager::RenderText(font, myX, myY, itFade->info, align, itFade->r, itFade->g, itFade->b, alpha);

				myY += verticalLineSpace;
			}
		}
		onEndDraw.processListener(this);
	}

	void LogFade::setPosition(int32_t x, int32_t y)
	{
		Widget::setPosition(x, y);
	}

	void LogFade::update()
	{
		std::deque<LogFadeData>::iterator itFade = logFadeData.begin();
		float deltaTime = UIManager::getSingleton()->getDeltaTime();

		uint32_t removedNum = 0;

		for (itFade; itFade != logFadeData.end(); ++itFade)
		{
			itFade->fadeTime -= deltaTime;
			if (itFade->fadeTime < 0.0f)
			{
				++removedNum;
			}
		}

		for (uint32_t i = 0; i<removedNum; ++i)
		{
			logFadeData.pop_back();
		}

		onUpdate.processListener(this);
	}

	void LogFade::addInfo(uint8_t r, uint8_t g, uint8_t b, const char* info)
	{
		int32_t strLength = strlen(info) + 1;
		if (strLength > setup::gui::LOG_FADE_MAX_CHARS_NUM)
			strLength = setup::gui::LOG_FADE_MAX_CHARS_NUM;

		logFadeData.push_front(LogFadeData());
		LogFadeData& newData = logFadeData.front();
		newData.r        = r;
		newData.g        = g;
		newData.b        = b;
		newData.fadeTime = this->fadeTime;
#pragma warning(disable:4996)
		strncpy(newData.info, info, strLength);
		newData.info[setup::gui::LOG_FADE_MAX_CHARS_NUM - 1] = '\0';
#pragma warning(default:4996)
	}

	void LogFade::addInfoC(uint8_t r, uint8_t g, uint8_t b, const char* info, ...)
	{
		char temp[ja::setup::gui::LOG_FADE_MAX_CHARS_NUM];
		va_list	args;

		if (info == nullptr) *temp = 0;
		else {
			va_start(args, info);
#pragma warning(disable : 4996)
			vsprintf(temp, info, args);
#pragma warning(default : 4996)
			va_end(args);
		}

		addInfo(r, g, b, temp);
	}

	void LogFade::setShadowColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
	{
		rS = r;
		gS = g;
		bS = b;
		aS = a;
	}

	void LogFade::setShadowOffset(float x, float y)
	{
		shadowOffsetX = x;
		shadowOffsetY = y;
	}

	void LogFade::setAlign(uint32_t marginStyle)
	{
		align = marginStyle;
	}

}