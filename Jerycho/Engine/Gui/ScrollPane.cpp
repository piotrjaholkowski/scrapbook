#include "ScrollPane.hpp"
#include "../Manager/UIManager.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Utility/Exception.hpp"

namespace ja
{

ScrollPane::ScrollPane(uint32_t id, Widget* parent) : Layout(JA_SCROLL_PANE, id, parent), scrollbarHorizontal(1, this), scrollbarVertical(2,this)
{
	r = g = b = 0;
	a = 255;
	scrollbarSize = 10;

	setRender(true);
	setActive(true);
	setAsLayout(true);
	
	scrollbarHorizontal.setHorizontal();
	scrollbarHorizontal.setOnChangeEvent(this, &ScrollPane::onChangeHorizontal);
	horizontalPolicy = JA_SCROLLBAR_AS_NEEDED;
	scrollbarHorizontal.setRender(false);
	scrollbarHorizontal.setActive(false);

	scrollbarVertical.setVertical();
	scrollbarVertical.setOnChangeEvent(this, &ScrollPane::onChangeVertical);
	verticalPolicy = JA_SCROLLBAR_AS_NEEDED;
	scrollbarVertical.setRender(false);
	scrollbarVertical.setActive(false);

	scrollbarCorner = JA_RIGHT_LOWER_CORNER;

	autoScrollVertical = true;
	autoScrollHorizontal = true;
	scrollToLeft = true;
	scrollToTop = true;

	minX = INT_MAX;
	maxX = INT_MIN;
	minY = INT_MAX;
	maxY = INT_MIN;

	currentX = 0;
	currentY = 0;
}

void ScrollPane::draw(int32_t parentX, int32_t parentY)
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;

	if(parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	if(!collisionMask.canIntersect) // scissors region invalidate whole rendering area
		return;

	if(isRendered())
	{
		glEnable(GL_SCISSOR_TEST);
		glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);
		DisplayManager::setColorUb(r,g,b,a);
		if(a!=255)
		{
			DisplayManager::enableAlphaBlending();
			DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
			DisplayManager::disableAlphaBlending();
		}
		else
		{
			DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
		}
		glDisable(GL_SCISSOR_TEST);

		std::vector<Widget*>::iterator it = widgets.begin();

		int32_t tempX, tempY;

		getScrollPanelPosition(tempX,tempY); // get local position of scroll pane
		tempX += (myX - currentX);
		tempY += (myY - currentY); 

		for(it; it != widgets.end(); ++it)
		{
			(*it)->draw(tempX,tempY);
		}

		if(scrollbarHorizontal.isRendered())
			scrollbarHorizontal.draw(myX,myY);
		if(scrollbarVertical.isRendered())
			scrollbarVertical.draw(myX,myY);

		glEnable(GL_SCISSOR_TEST);
		glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);
		if(isBorderRendered())
		{
			DisplayManager::setColorUb(borderR,borderG,borderB,borderA);
			DisplayManager::drawRectangle(myX + 1, myY + 1, myX + width - 1, myY + height - 1, false);
		}
		glDisable(GL_SCISSOR_TEST);
	}
}

void ScrollPane::setChildrenPosition( Widget* widget, int32_t x, int32_t y)
{
	int32_t width = widget->getWidth() + x;
	int32_t height = widget->getHeight() + y;


	// if scroll position doesn't change update only one children
	if(x < minX)
	{
		minX = x;
		//currentX = minX; // scrollbar on the right	
	}
	
	if(width > maxX)
	{
		maxX = width;
	}

	if(y < minY)
	{
		minY = y;
	}
	
	if(height > maxY)
	{
		maxY = height;
		//currentY = maxY - getPanelHeight(); // depends on scrollbar position
	}

	updateScrollPanelPosition(); // it enables scrollbars if needed and sets right scroll panel position
	updateCollisionMask();
	//updateChildrenCollisionMask();
}

void ScrollPane::updateScrollbarCollisionMask()
{
	int32_t parentX,parentY;
	int32_t scrollbarX, scrollbarY;
	int32_t tempX, tempY;

	//getPosition(parentX,parentY); // Position of parent
	getAbsolutePosition(parentX, parentY);

	switch(scrollbarCorner)
	{
	case JA_RIGHT_LOWER_CORNER:
		Layout::forcePosition(&scrollbarHorizontal, 0,0);
		break;
	case JA_RIGHT_UPPER_CORNER:
		Layout::forcePosition(&scrollbarHorizontal, 0,0);
		break;
	case JA_LEFT_UPPER_CORNER:
		Layout::forcePosition(&scrollbarHorizontal, getVerticalScrollbarSize(), getPanelHeight());
		break;
	case JA_LEFT_LOWER_CORNER:
		Layout::forcePosition(&scrollbarHorizontal, getVerticalScrollbarSize(), 0);
		break;
	}

	Layout::forceSize(&scrollbarHorizontal, getPanelWidth(), scrollbarSize);

	scrollbarHorizontal.getRelativePosition(scrollbarX,scrollbarY);
	tempX = parentX + scrollbarX;
	tempY = parentY + scrollbarY;
	Layout::setCollisionMaskDirecly(&scrollbarHorizontal, tempX, tempY, tempX + scrollbarHorizontal.getWidth(), tempY + scrollbarHorizontal.getHeight());

	switch(scrollbarCorner)
	{
	case JA_RIGHT_LOWER_CORNER:
		Layout::forcePosition(&scrollbarVertical, getPanelWidth(),getHorizontalScrollbarSize());
		break;
	case JA_RIGHT_UPPER_CORNER:
		Layout::forcePosition(&scrollbarVertical, getPanelWidth(),0);
		break;
	case JA_LEFT_UPPER_CORNER:
		Layout::forcePosition(&scrollbarVertical, 0, 0);
		break;
	case JA_LEFT_LOWER_CORNER:
		Layout::forcePosition(&scrollbarVertical, 0, getHorizontalScrollbarSize());
		break;
	}

	Layout::forceSize(&scrollbarVertical, scrollbarSize, getPanelHeight());

	scrollbarVertical.getRelativePosition(scrollbarX,scrollbarY);
	tempX = parentX + scrollbarX;
	tempY = parentY + scrollbarY;
	Layout::setCollisionMaskDirecly(&scrollbarVertical,tempX,tempY,tempX + scrollbarVertical.getWidth(), tempY + scrollbarVertical.getHeight());
}

void ScrollPane::updateScrollPanelPosition()
{
	switch(scrollbarCorner)
	{
	case JA_RIGHT_LOWER_CORNER:
		{
			if(autoScrollVertical)
			{
				if(scrollToTop)
					currentY = maxY - getPanelHeight();
				else
					currentY = minY - getHorizontalScrollbarSize();

				scrollbarVertical.setValue(currentY);
			}

			if(autoScrollHorizontal)
			{
				if(scrollToLeft)
					currentX = minX;
				else
					currentX = maxX - this->getPanelWidth();

				scrollbarVertical.setValue(currentX);
			}		
		}
		break;
	case JA_RIGHT_UPPER_CORNER:
	
		break;
	case JA_LEFT_UPPER_CORNER:
	
		break;
	case JA_LEFT_LOWER_CORNER:

		break;
	}
}

void ScrollPane::updateScrollbars()
{
	for(uint8_t i=0; i<2; ++i) // to avoid incorrect scrollbar seting if two scrollbar are depended
	{
		switch(this->verticalPolicy)
		{
		case JA_SCROLLBAR_AS_NEEDED:
			{
				if((maxY - minY) > this->getPanelHeight())
				{
					scrollbarVertical.setRender(true);
					scrollbarVertical.setActive(true);
				}
				else
				{
					scrollbarVertical.setRender(false);
					scrollbarVertical.setActive(false);
				}
			}
			break;
		case JA_SCROLLBAR_NEVER:
			scrollbarVertical.setRender(false);
			scrollbarVertical.setActive(false);
			break;
		case JA_SCROLLBAR_ALWAYS:
			scrollbarVertical.setRender(true);
			scrollbarVertical.setActive(true);
			break;
		}

		switch(this->horizontalPolicy)
		{
		case JA_SCROLLBAR_AS_NEEDED:
			{
				if((maxX - minX) > this->getPanelWidth())
				{
					scrollbarHorizontal.setRender(true);
					scrollbarHorizontal.setActive(true);
				}
				else
				{
					scrollbarHorizontal.setRender(false);
					scrollbarHorizontal.setActive(false);
				}
			}
			break;
		case JA_SCROLLBAR_NEVER:
			scrollbarHorizontal.setRender(false);
			scrollbarHorizontal.setActive(false);
			break;
		case JA_SCROLLBAR_ALWAYS:
			scrollbarHorizontal.setRender(true);
			scrollbarHorizontal.setActive(true);
			break;
		}
	}

	//set range
	switch(scrollbarCorner)
	{
	case JA_RIGHT_LOWER_CORNER:
		scrollbarHorizontal.setRange(minX,maxX - getPanelWidth());
		scrollbarVertical.setRange(minY,maxY - getPanelHeight());
		break;
	case JA_RIGHT_UPPER_CORNER:
		break;
	case JA_LEFT_UPPER_CORNER:
		break;
	case JA_LEFT_LOWER_CORNER:
		break;
	}
	//set range

	updateScrollbarCollisionMask();
}

void ScrollPane::setScrollbarSize(int32_t scrollbarSize )
{
	this->scrollbarSize = scrollbarSize;
	updateCollisionMask(); // it updates collision mask of scroll panel
}

void ScrollPane::updateChildrenCollisionMask()
{
	updateScrollbars(); // it updates scrollbar ranges and enables them if needed and updates scrollbar collision mask


	std::vector<Widget*>::iterator it = widgets.begin();

	
	for(it; it != widgets.end(); ++it)
	{
		(*it)->updateCollisionMask();
	}
}

void ScrollPane::setHorizontalScrollbarPolicy( ScrollbarPolicy scrollbarPolicy )
{
	if(scrollbarPolicy != this->horizontalPolicy)
	{
		this->horizontalPolicy = scrollbarPolicy;

		updateCollisionMask(); // it updates collision mask of scroll panel
	}	
}

void ScrollPane::setVerticalScrollbarPolicy( ScrollbarPolicy scrollbarPolicy )
{
	if(scrollbarPolicy != this->verticalPolicy)
	{
		this->verticalPolicy = scrollbarPolicy;
		
		updateCollisionMask(); // it updates collision mask of scroll panel
	}	
}

void ScrollPane::setCorner( WidgetCorner scrollbarCorner )
{
	if(scrollbarCorner != this->scrollbarCorner)
	{
		this->scrollbarCorner = scrollbarCorner;

		updateCollisionMask(); // it updates collision mask of scroll panel
	}	
}

void ScrollPane::getScrollPanelPosition( int32_t& x, int32_t& y )
{
	switch(scrollbarCorner)
	{
	case JA_RIGHT_LOWER_CORNER:
		x = 0;
		y = getHorizontalScrollbarSize();
		break;
	case JA_RIGHT_UPPER_CORNER:
		x = 0;
		y = 0;
		break;
	case JA_LEFT_UPPER_CORNER:
		x = getVerticalScrollbarSize();
		y = 0;
		break;
	case JA_LEFT_LOWER_CORNER:
		x = getVerticalScrollbarSize();
		y = getHorizontalScrollbarSize();
		break;
	}
}

void ScrollPane::onChangeVertical( Widget* sender, WidgetEvent action, void* data )
{
	int32_t scrollValue = (int32_t)(this->scrollbarVertical.getValue());

	this->currentY = scrollValue;
	updateChildrenCollisionMask();
}

void ScrollPane::onChangeHorizontal( Widget* sender, WidgetEvent action, void* data )
{
	int32_t scrollValue = (int32_t)(this->scrollbarHorizontal.getValue());

	this->currentX = scrollValue;
	updateChildrenCollisionMask();
}

void ScrollPane::setScrollX( int32_t scrollX )
{
	this->currentX = scrollX;
	this->scrollbarHorizontal.setValue(this->currentX);

	updateChildrenCollisionMask();
}

void ScrollPane::setScrollY( int32_t scrollY )
{
	this->currentY = scrollY;
	this->scrollbarVertical.setValue(this->currentY);

	updateChildrenCollisionMask();
}

void ScrollPane::updateCollisionMask()
{
	updateScrollbars(); // it updates scrollbar ranges and enables them if needed and updates scrollbar collision mask
	//updateScrollPanelPosition(); // it enables scrollbars if needed and sets right scroll panel position

	int32_t advanceMinX = 0,advanceMaxX = 0,advanceMinY = 0,advanceMaxY = 0;

	getScrollPanelPosition(advanceMinX,advanceMinY);
	advanceMaxX = advanceMinX + getPanelWidth();
	advanceMaxY = advanceMinY + getPanelHeight();

	int32_t posX,posY;

	//if(isLayoutChild())
	//{
	//	Layout* layout = static_cast<Layout*>(parent);
	//	layout->getOriginPosition(posX,posY);
	//}
	//else
	//{
	//	getPosition(posX,posY); //doesn't change position if on the scroll pane
	//}
	getAbsolutePosition(posX, posY);

	advanceMinX += posX;
	advanceMaxX += posX;
	advanceMinY += posY;
	advanceMaxY += posY;

	if(parent)
	{
		collisionMask.setMask(advanceMinX, advanceMinY, advanceMaxX, advanceMaxY);
		collisionMask.makeIntersection(parent->getCollisionMask());
	}
	else
	{
		collisionMask.setMask(advanceMinX,advanceMinY,advanceMaxX,advanceMaxY);
	}

	updateChildrenCollisionMask();
}

Widget* ScrollPane::getWidget( uint32_t widgetId )
{
	return nullptr;
}

void ScrollPane::update()
{
	std::vector<Widget*>::iterator it;

	if(isActive())
	{
		if(scrollbarHorizontal.isActive())
			scrollbarHorizontal.update();

		if(scrollbarVertical.isActive())
			scrollbarVertical.update();

		for(it = widgets.begin(); it != widgets.end(); ++it)
		{
			(*it)->update();
		}
	}

	if(!UIManager::isMouseOverGui() && isRendered())
	{
		if(this->isRendered())
		{
			if(this->isMouseOver())
			{
				UIManager::setMouseOverGui(true);
				return;
			}
		}
	}
}

void ScrollPane::addWidget(Widget* widget)
{
	if(widget->isLayoutChild())
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI,"Widget is already added in diffrent layout");
	}

	if(widget->getReferenceNumber())
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI, "Widget is already added to diffrent interface");
	}

	int32_t x,y;
	widgets.push_back(widget);

	Layout::increaseReference(widget);

	widget->setParent(this);
	widget->getRelativePosition(x,y);

	setChildrenPosition(widget,x,y);	
}

int32_t ScrollPane::getHorizontalScrollbarSize()
{
	if(this->scrollbarHorizontal.isActive())
	{
		return this->scrollbarSize;
	}

	return 0;
}

int32_t ScrollPane::getVerticalScrollbarSize()
{
	if(this->scrollbarVertical.isActive())
	{
		return this->scrollbarSize;
	}

	return 0;
}

int32_t ScrollPane::getPanelWidth()
{
	if(scrollbarVertical.isActive())
	{
		return (width - scrollbarSize); // if scrollbar is showed
	}

	return width;
}

int32_t ScrollPane::getPanelHeight()
{
	if(scrollbarHorizontal.isActive())
	{
		return (height - scrollbarSize); // if scrollbar is showed
	}

	return height;
}

void ScrollPane::setColor( uint8_t r, uint8_t g, uint8_t b )
{
	this->r = r;
	this->g = g;
	this->b = b;
}

void ScrollPane::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

ScrollPane::~ScrollPane()
{
	clearWidgets();

	
}

void ScrollPane::clearWidgets()
{
	std::vector<Widget*>::iterator it = widgets.begin();

	for (it; it != widgets.end(); it++)
	{
		Layout::removeReference(*it);
	}

	widgets.clear();

	minX = INT_MAX;
	maxX = INT_MIN;
	minY = INT_MAX;
	maxY = INT_MIN;

}

void ScrollPane::setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	borderR = r;
	borderG = g;
	borderB = b;
	borderA = a;
}

void ScrollPane::setVerticalAutoScroll( bool state, bool scrollToTop)
{
	this->autoScrollVertical = state;
	this->scrollToTop = scrollToTop;
}

void ScrollPane::setHorizontalAutoScroll( bool state, bool scrollToleft )
{
	this->autoScrollHorizontal = state;
	this->scrollToLeft = scrollToleft;
}

void ScrollPane::getOriginPosition( int32_t& x, int32_t& y )
{
	int32_t tempX, tempY;
	Widget::getPosition(x,y);

	getScrollPanelPosition(tempX,tempY); // get local position of scroll pane

	x += tempX - currentX;
	y += tempY - currentY;
}

void ScrollPane::getAbsolutePosition(int32_t& x, int32_t& y)
{
	if (nullptr != parent)
	{
		parent->getTransformPosition(x, y);
	}

	//int32_t tempX, tempY;
	//getScrollPanelPosition(tempX, tempY); // get local position of scroll pane

	x += this->x;
	y += this->y;
}

void ScrollPane::getTransformPosition(int32_t& x, int32_t& y)
{
	if (nullptr != parent)
	{
		//parent->getAbsolutePosition(x, y);
		parent->getTransformPosition(x, y);
	}

	int32_t tempX, tempY;
	getScrollPanelPosition(tempX, tempY); // get local position of scroll pane

	x += tempX - this->currentX;
	y += tempY - this->currentY;
}

Scrollbar* ScrollPane::getVerticalScrollbar()
{
	return &scrollbarVertical;
}

Scrollbar* ScrollPane::getHorizontalScrollbar()
{
	return &scrollbarHorizontal;
}

}
