#pragma once

#include "Widget.hpp"
#include "OnClickListener.hpp"
#include "OnMouseOverListener.hpp"
#include "OnMouseLeaveListener.hpp"
#include "OnSelectListener.hpp"
#include "OnDeselectListener.hpp"
#include "OnChangeListener.hpp"

namespace ja
{

	class Scrollbar : public Widget
	{

	protected:
		uint32_t widgetStyle;
		uint8_t  r, g, b, a;
		uint8_t  borderR, borderG, borderB, borderA;

		double lowestValue;
		double biggestValue;
		double currentValue;
		double windowRange;

		bool    isHorizontalScroll;
		bool    isRaisingRange; // is range goes from lowest to biggest
		int16_t beginPixel;
		int16_t endPixel;
		int16_t middleOpposite;
		int16_t currentPixel;

	public:
		void*                vUserData;

	protected:
		OnSelectListener     onSelect;
		OnDeselectListener   onDeselect;
		OnClickListener      onClick;
		OnMouseOverListener  onMouseOver;
		OnMouseLeaveListener onMouseLeave;
		OnChangeListener     onChange;

		void updateValue();
	public:
		Scrollbar();
		Scrollbar(uint32_t id, Widget* parent);

		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setAlpha(uint8_t alpha);

		void update();
		void updateSize();
		void draw(int32_t parentX, int32_t parentY);
		void setHorizontal();
		void setVertical();
		bool isHorizontal();
		void setRange(double lowestValue, double biggestValue);
		void setWindowRange(double windowRange);
		void setValue(double currentValue);
		double getValue();

		template < class Y, class X >
		void setOnClickEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onClick.setActionEvent(pthis, pmethod);
			onClick.setActive(true);
		}

		void setOnClickEvent(ScriptDelegate* scriptDelegate)
		{
			onClick.setActionEvent(scriptDelegate);
			onClick.setActive(true);
		}

		template < class Y, class X >
		void setOnMouseOverEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onMouseOver.setActionEvent(pthis, pmethod);
			onMouseOver.setActive(true);
		}

		void setOnMouseOverEvent(ScriptDelegate* scriptDelegate)
		{
			onMouseOver.setActionEvent(scriptDelegate);
			onMouseOver.setActive(true);
		}

		template < class Y, class X >
		void setOnMouseLeaveEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onMouseLeave.setActionEvent(pthis, pmethod);
			onMouseLeave.setActive(true);
		}

		void setOnMouseLeaveEvent(ScriptDelegate* scriptDelegate)
		{
			onMouseLeave.setActionEvent(scriptDelegate);
			onMouseLeave.setActive(true);
		}

		template < class Y, class X >
		void setOnSelectEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onSelect.setActionEvent(pthis, pmethod);
			onSelect.setActive(true);
		}

		void setOnSelectEvent(ScriptDelegate* scriptDelegate)
		{
			onSelect.setActionEvent(scriptDelegate);
			onSelect.setActive(true);
		}

		template < class Y, class X >
		void setOnDeselectEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onDeselect.setActionEvent(pthis, pmethod);
			onDeselect.setActive(true);
		}

		void setOnDeselectEvent(ScriptDelegate* scriptDelegate)
		{
			onDeselect.setActionEvent(scriptDelegate);
			onDeselect.setActive(true);
		}

		template < class Y, class X >
		void setOnChangeEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onChange.setActionEvent(pthis, pmethod);
			onChange.setActive(true);
		}

		void setOnChangeEvent(ScriptDelegate* scriptDelegate)
		{
			onChange.setActionEvent(scriptDelegate);
			onChange.setActive(true);
		}
	};

}
