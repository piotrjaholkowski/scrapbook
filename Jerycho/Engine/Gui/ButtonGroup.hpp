#pragma once

#include "Widget.hpp"
#include <vector>

namespace ja
{

	class ButtonGroup : public Widget
	{
	private:
		Widget*              selectedWidget;
		std::vector<Widget*> widgets;
		Widget*              checkedWidget;
	public:
		ButtonGroup(uint32_t id, Widget* parent);

		void update();
		//virtual void draw(int parentX, int parentY);

		void addButton(Widget* widget);
		void removeButton(Widget* widget);

		//void clearSelection();

		~ButtonGroup();
	};

}
