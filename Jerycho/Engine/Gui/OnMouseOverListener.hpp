#pragma once

#include "Widget.hpp"
#include "../Manager/UIManager.hpp"
#include "../Font/Font.hpp"
#include <string>

namespace ja
{

	class OnMouseOverListener : public WidgetListener
	{
	public:

		bool alreadyMouseOver;

		OnMouseOverListener() : WidgetListener(JA_ONMOUSEOVER)
		{
			alreadyMouseOver = false;
		}

		bool processListener(Widget* widget)
		{
			if (isActive())
				if (widget->isActive())
				{
					if (widget->isMouseOver())
					{
						if (alreadyMouseOver == true)
							return false;
						fireEvent(widget);
						alreadyMouseOver = true;
						return true;
					}
					else
					{
						alreadyMouseOver = false;
					}
				}
			return false;
		}

		void fireEvent(Widget* widget)
		{
			if (callback)
				callback(widget, JA_ONMOUSEOVER, nullptr);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONMOUSEOVER, nullptr);
		}

		~OnMouseOverListener()
		{

		}
	};

}
