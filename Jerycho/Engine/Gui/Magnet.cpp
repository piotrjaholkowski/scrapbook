#include "Magnet.hpp"
#include "../Manager/DisplayManager.hpp"

#include <iostream>
#include <string>

namespace ja
{ 

Magnet::Magnet(uint32_t id, Widget* parent) : ImageButton(id,parent)
{
	this->widgetType = JA_MAGNET;
	setFont("default.ttf",10);
}

void Magnet::update()
{      	
	ImageButton::update();

	int32_t mouseX = InputManager::getMousePositionX();
	int32_t mouseY = InputManager::getMousePositionY();
	mouseY = DisplayManager::getSingleton()->getResolutionHeight() - mouseY;

	if(onClick.wasPressed)
	{
		int32_t deltaX = mouseX - oldPositionX;
		int32_t deltaY = mouseY - oldPositionY;

		if((deltaX != 0) || (deltaY != 0))
		{
			int32_t tempX,tempY;
			setPosition(x + deltaX, y + deltaY);
			std::vector<Widget*>::iterator it = linkedWidgets.begin();
			for(it; it != linkedWidgets.end(); ++it)
			{
				(*it)->getPosition(tempX,tempY);
				(*it)->setPosition(tempX + deltaX, tempY + deltaY);
			}
		}

		UIManager::setTopWidget(this);
	}

	oldPositionX = mouseX;
	oldPositionY = mouseY;
}

Magnet::~Magnet()
{

}

void Magnet::addLink( Widget* widget )
{
	linkedWidgets.push_back(widget);
}

}
