#pragma once

#include "Widget.hpp"
#include "OnTimerListener.hpp"

namespace ja
{

	class Timer : public Widget
	{
	protected:
		OnTimerListener onTimer;
	public:
		Timer(uint32_t id, Widget* parent);

		virtual void update();
		virtual void draw(int32_t parentX, int32_t parentY);
		void setTimeInterval(double timeInterval);
		void setAutoTimer(bool state);
		void setTimer(bool state);
		double getElapsedTime();
		void restartTimer();
		~Timer();

		template < class Y, class X >
		void setOnTimerEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onTimer.setActionEvent(pthis, pmethod);
		}
	};

}