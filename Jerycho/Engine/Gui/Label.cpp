#include "Label.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/ResourceManager.hpp"
#include "../Utility/Exception.hpp"
#include "../Gui/Margin.hpp"
#include <iostream>
#include <string>

//#define JA_LABEL_INNER_MODE_M 1
//#define JA_LABEL_NORMAL_MODE_M 2

namespace ja
{ 

Label::Label() : Widget(JA_LABEL,0,nullptr)
{
	font = nullptr;
	r=g=b=rS=gS=bS=aS=0;
	shadowOffsetX = -2;
	shadowOffsetY = -2;
	a = 1;
	setRender(true);
	width = 0;
	height = 0;
	//labelMode = JA_LABEL_NORMAL_MODE_M;
}

Label::Label(uint32_t id, Widget* parent) : Widget(JA_LABEL,id,parent)
{
	font = nullptr;
	r=g=b=rS=gS=bS=aS=0;
	shadowOffsetX = -2;
	shadowOffsetY = -2;
	a = 1;
	setRender(true);
	width = 0;
	height = 0;
	//labelMode = JA_LABEL_NORMAL_MODE_M;
}

void Label::setColor( uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

void Label::setShadowColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->rS = r;
	this->gS = g;
	this->bS = b;
	this->aS = a;
}

void Label::setShadowOffset(int8_t x, int8_t y)
{
	shadowOffsetX = x;
	shadowOffsetY = y;
}

void Label::setText(const char* text)
{
	this->text = text;

	if((width == 0) && (height == 0))
		return;

	if(font == nullptr)
		throw JA_EXCEPTIONR(ExceptionType::GUI, "You need to set font before setText");

	int32_t textWidth,textHeight;
	font->getTextSize(textWidth,textHeight,this->text);

	Widget::setSize(textWidth,textHeight);
}

void Label::appendText(ja::string& text)
{
	this->text.append(text);

	if((width == 0) && (height == 0))
		return;

	if(font == nullptr)
		throw JA_EXCEPTIONR(ExceptionType::GUI, "You need to set font before appendText");

	int32_t textWidth,textHeight;
	font->getTextSize(textWidth,textHeight,this->text);

	Widget::setSize(textWidth,textHeight);
}

ja::string Label::getText()
{
	return text;
}

ja::string& Label::getTextReference()
{
	return text;
}

const char* Label::getTextCstr()
{
	return text.c_str();
}

void Label::setTextPrintC(const char* fieldValue,...)
{
	char temp[256];
	va_list	args;

	if (fieldValue == nullptr)
		*temp=0;
	else
	{
		va_start(args, fieldValue);
#pragma warning(disable : 4996)
		vsprintf(temp, fieldValue, args);
#pragma warning(default : 4996)
		va_end(args);	
	}

	setText(temp);
}

void Label::setAlign( uint32_t marginStyle )
{
	this->align = marginStyle;
}

void Label::update()
{
	Widget::update();
}

void Label::draw(int32_t parentX, int32_t parentY)
{
	DisplayManager* display = DisplayManager::getSingleton();
	if((text != "") && (font != nullptr))
	{
		float myX = (float)(parentX + x);
		float myY = (float)(parentY + y);

		if((width == 0) && (height == 0))
		{
			if(aS != 0)
			{
				display->RenderText(font, (float)(myX + shadowOffsetX), (float)(myY + shadowOffsetY),text.c_str(),align,rS,gS,bS,aS);
			}
			display->RenderText(font,myX,myY,text.c_str(),align,r,g,b,a);
		}
		else // in
		{
			glEnable(GL_SCISSOR_TEST);
			glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);

			uint32_t iflags = align & 4294967288;
			if (iflags != (uint32_t)Margin::NORMAL)
			{
				switch(iflags)
				{
				case (uint32_t)Margin::TOP:
					myY = myY + height; 
					break;
				case (uint32_t)Margin::DOWN:
					break;
				case (uint32_t)Margin::MIDDLE:
					myY = myY + (height/2);
					break;
				}
			}

			iflags = align & 7;
		
			switch(iflags)
			{
			case (uint32_t)Margin::LEFT:
				break;
			case (uint32_t)Margin::CENTER:
				myX = myX + (width / 2);
				break;
			case (uint32_t)Margin::RIGHT:
				myX = myX + width;
				break;
			}

			if(aS != 0)
			{
				display->RenderText(font,myX + shadowOffsetX,myY + shadowOffsetY,text.c_str(),align,rS,gS,bS,aS);
			}
			display->RenderText(font,myX,myY,text.c_str(),align,r,g,b,a);

			glDisable(GL_SCISSOR_TEST);
		}
	}
}

void Label::setFont( const char* fontName, uint32_t fontSize )
{
	ResourceManager* resourceManager = ResourceManager::getSingleton();
	this->font = resourceManager->getEternalFont(fontName, fontSize)->resource;

	if(!text.empty())
		setText(text.c_str());
}

Font* Label::getFont()
{
	return font;
}

Label::~Label()
{

}

void Label::getTextSize(int32_t& textWidth, int32_t& textHeight)
{
	font->getTextSize(textWidth, textHeight, text);
}

void Label::setLayoutBehaviour( Widget* widget )
{
	
}

void Label::adjustToText(int32_t additionalWidth, int32_t height)
{
	int32_t textWidth, textHeight;
	font->getTextSize(textWidth, textHeight, text);
	setSize(textWidth + additionalWidth,height);
}

}