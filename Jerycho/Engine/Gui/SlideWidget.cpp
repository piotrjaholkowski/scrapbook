#include "SlideWidget.hpp"
#include "../Manager/InputManager.hpp"
#include "../Gui/Margin.hpp"

namespace ja
{ 

SlideWidget::SlideWidget(uint32_t id, Widget* activateWidget, Widget* slideWidget, Widget* parent) : Layout(JA_SLIDE_WIDGET, id, parent)
{
	setRender(true);
	setActive(true);
	setAsLayout(true);

	this->activateWidget = activateWidget;
	this->slideWidget    = slideWidget;

	this->align = (uint32_t)Margin::TOP | (uint32_t)Margin::RIGHT;

	slideWidget->setParent(this);
	activateWidget->setParent(this);
	Layout::increaseReference(slideWidget);
	Layout::increaseReference(activateWidget);

	int32_t activateX, activateY;
	activateWidget->getPosition(activateX, activateY);
	
	Widget::forcePosition(activateX, activateY);
	Layout::forcePosition(activateWidget, 0, 0);
	Layout::forceSize(activateWidget, activateWidget->getWidth(), activateWidget->getHeight());

	this->hideAfterTime         = 0.5f;
	this->elapsedMouseLeaveTime = 0.0f;
	this->isSlided      = false;
}

SlideWidget::~SlideWidget()
{
	Layout::removeReference(slideWidget);
	Layout::removeReference(activateWidget);
}

void SlideWidget::draw(int parentX, int parentY)
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;

	if (parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	this->activateWidget->draw(myX, myY);

	if (this->isSlided)
	{
		slideWidget->draw(myX, myY);
	}
}

void SlideWidget::update()
{
	this->activateWidget->update();
	bool isMouseOver = this->activateWidget->isMouseOver();

	UIManager::setTopWidget(this);

	if (this->isSlided || isMouseOver)
	{
		this->isSlided = true;

		int32_t activateX, activateY;
		getPosition(activateX, activateY);

		int32_t activateWidth, activateHeight, slideWidth, slideHeight;
		int32_t slideX = 0;
		int32_t slideY = 0;
		activateWidth  = activateWidget->getWidth();
		activateHeight = activateWidget->getHeight();
		slideWidth     = slideWidget->getWidth();
		slideHeight    = slideWidget->getHeight();

		uint32_t iflags = align & 0xfffffff8;

		if (iflags != (uint32_t)Margin::NORMAL)
		{
			switch (iflags)
			{
			case (uint32_t)Margin::TOP:
				slideY = 0;
				break;
			case (uint32_t)Margin::DOWN:
				slideY = -slideHeight;
				break;
			case (uint32_t)Margin::MIDDLE:
				slideY = 0;
				break;
			}
		}

		iflags = align & 7;

		switch (iflags)
		{
		case (uint32_t)Margin::LEFT:
			slideX = activateWidth - slideWidth;
			break;
		case (uint32_t)Margin::RIGHT:
			slideX = activateWidth;
			break;
		case (uint32_t)Margin::CENTER:
			slideX = activateWidth;
			break;
		}

		Layout::forcePosition(this->slideWidget, slideX, slideY);
		slideX += activateX;
		slideY += activateY;
		Layout::setCollisionMaskDirecly(this->slideWidget, slideX, slideY, slideX + slideWidth, slideY + slideHeight);  // adds parent intersection

		if (this->slideWidget->isMouseOver())
		{
			UIManager::setTopWidget(this->slideWidget);
		}

		slideWidget->update();
	}

	Widget* activeSlideMenu = UIManager::getTopWidget();

	if (activeSlideMenu == this)
	{
		if (!isMouseOver)
		{
			this->elapsedMouseLeaveTime += UIManager::getSingleton()->getDeltaTime();
		}
		else {
			this->elapsedMouseLeaveTime = 0.0f;
		}

		if (this->elapsedMouseLeaveTime >= this->hideAfterTime)
		{
			this->isSlided = isMouseOver;
		}
	}	
}

void SlideWidget::setAlign(uint32_t marginStyle)
{
	this->align = marginStyle;
}

void SlideWidget::setPosition(int x, int y)
{
	Widget::setPosition(x, y);
	Layout::forcePosition(this->activateWidget, 0, 0);
}

void SlideWidget::setSize(int width, int height)
{
	Widget::setSize(width, height);
	Layout::forceSize(this->activateWidget, width, height);
}

void SlideWidget::forcePosition(int32_t x, int32_t y)
{
	Widget::forcePosition(x, y);
	Layout::forcePosition(this->activateWidget, 0, 0);
}

void SlideWidget::forceSize(int width, int height)
{
	Widget::forceSize(width, height);
	Layout::forceSize(this->activateWidget, width, height);
}

void SlideWidget::updateCollisionMask()
{
	Widget::updateCollisionMask();
	updateChildrenCollisionMask();
}

void SlideWidget::updateChildrenCollisionMask()
{
	int positionX, positionY;

	if (isLayoutChild())
	{
		Layout* layout = static_cast<Layout*>(parent);
		layout->getOriginPosition(positionX, positionY);
		positionX += x;
		positionY += y;
	}
	else
	{
		getPosition(positionX, positionY); //doesn't change position if on the scroll pane
	}

	Layout::setCollisionMask(this->activateWidget, positionX, positionY, positionX + width, positionY + height);  // adds parent intersection
}

}

