#include "ProgressButton.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/ResourceManager.hpp"

#include <iostream>
#include <string>

using namespace ja;

ProgressButton::ProgressButton() : ImageButton(0,nullptr)
{
	widgetType = JA_PROGRESS_BUTTON;
}

ProgressButton::ProgressButton(uint32_t id, Widget* parent) : ImageButton(id,parent)
{
	widgetType = JA_PROGRESS_BUTTON;
}

void ProgressButton::draw(int32_t parentX, int32_t parentY)
{
	float myX = (float)(parentX + x);
	float myY = (float)(parentY + y);

	DisplayManager::setColorUb(r,g,b,a);
	if(a != 255)
	{
		DisplayManager::enableAlphaBlending();
		DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, true);
		DisplayManager::disableAlphaBlending();
	}
	else
	{
		DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, true);
	}


	if(texture != nullptr)
	{
		DisplayManager::beginDrawingSprites();
		DisplayManager::drawSprite(texture,myX + imageOriginX, myY + imageOriginY, halfWidth, halfHeight, 0);
		DisplayManager::endDrawingSprites();
	}

	if(onClick.wasPressed)
	{
		DisplayManager::setColorUb(255,255,255,255);
		DisplayManager::drawLine(myX + (float)(imageOriginX), myY + (float)(imageOriginY), (float)(mousePosX), (float)(mousePosY));
	}

}

void ProgressButton::update()
{      
	ImageButton::update();

	if(onClick.wasPressed)
	{
		mousePosX = InputManager::getMousePositionX();
		mousePosY = DisplayManager::getSingleton()->getResolutionHeight() - InputManager::getMousePositionY();

		int x,y;
		getPosition(x,y);

		x += imageOriginX;
		y += imageOriginY;

		double diffX = mousePosX - x;
		double diffY = mousePosY - y;

		double angle = atan2(diffY,diffX) * 180.0 / math::PI;
		angle += 180; // To make 0 - 360 range
		double angleDiffrence = angle - previousAngle;

		if(angleDiffrence != 0)
		{
			if(abs(angleDiffrence) < 180)
			{
				changeValue = stepValue * (angleDiffrence / 360);
				onChange.setChange(&changeValue);
			}		
		}

		previousAngle = angle;
	}
	else
	{
		previousAngle = 0;
	}

	onChange.processListener(this);
}

void ProgressButton::setStepValue(double stepValue)
{
	this->stepValue = stepValue;
}

ProgressButton::~ProgressButton()
{

}