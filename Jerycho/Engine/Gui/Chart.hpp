#pragma once

#include "Widget.hpp"

namespace ja
{

	class Chart : public Widget
	{
	protected:
		uint8_t  r, g, b, a;
		float*   chartValues;
		uint32_t chartValuesNum;

	public:
		Chart(uint32_t id, Widget* parent);


		void setColor(uint8_t r, uint8_t g, uint8_t b);
		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setChartValues(float* valuesPointer, uint32_t valuesNum);

		virtual void setPosition(int32_t x, int32_t y);

		virtual void setSize(int32_t width, int32_t height);
		virtual void update();

		virtual void draw(int32_t parentX, int32_t parentY);
		~Chart();

	};

}