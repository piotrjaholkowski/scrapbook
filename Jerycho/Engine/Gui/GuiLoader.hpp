#pragma once

#include <tinyxml2/tinyxml2.h>
#include "Widget.hpp"
#include <map>
#include <string>

namespace ja
{
	namespace gui
	{
		namespace tag {
			const char* const GUI_ELEMENT = "gui";
			const char* const MENUS_ELEMENT = "menus";
			const char* const MENU_ELEMENT = "menu";
			const char* const GRID_LAYOUT_ELEMENT = "gridLayout";
			const char* const BUTTON_ELEMENT = "button";
			const char* const SCROLLBAR_ELEMENT = "scrollbar";
			const char* const LABEL_ELEMENT = "label";
			const char* const EDITBOX_ELEMENT = "editbox";
			const char* const RADIO_BUTTON_ELEMENT = "radioButton";
			const char* const BUTTON_GROUP_ELEMENT = "buttonGroup";
			const char* const SCROLL_PANE_ELEMENT = "scrollPane";
			const char* const CELLS_ELEMENT = "cells";
			const char* const CELL_ELEMENT = "cell";
			const char* const WIDGETS_ELEMENT = "widgets";
			const char* const EVENTS_ELEMENT = "events";
			const char* const CHECKBOX_ELEMENT = "checkbox";
			const char* const IS_RENDERED_PROPERTY = "isRendered";
			const char* const IS_ACTIVE_PROPERTY = "isActive";
			const char* const TEXT_PROPERTY = "text";
			const char* const POSITION_PROPERTY = "position";
			const char* const COLOR_PROPERTY = "color";
			const char* const FONT_PROPERTY = "font";
			const char* const SIZE_PROPERTY = "size";
			const char* const RANGE_PROPERTY = "range";
			const char* const GRID_SIZE_PROPERTY = "gridSize";
			const char* const ALIGN_PROPERTY = "align";
			const char* const IS_CHECKED_PROPERTY = "isChecked";
			const char* const BUTTON_GROUP_PROPERY = "buttonGroup";
			const char* const GAPS_PROPERTY = "gaps";
			const char* const SCROLLBAR_POLICY_PROPERTY = "scrollbarPolicy";
			const char* const SCROLLBAR_SIZE_PROPERTY = "scrollbarSize";
			const char* const AUTO_SCROLL_HORIZONTAL_PROPERTY = "autoScrollHorizontal";
			const char* const AUTO_SCROLL_VERTICAL_PROPERTY = "autoScrollVertical";
			const char* const ACCEPT_NEW_LINE_PROPERTY = "acceptNewLine";
			const char* const ENABLED_ATTRIBUTE = "enabled";
			const char* const CORNER_PROPERTY = "corner";
			const char* const ID_ATTRIBUTE = "id";
			const char* const GLOBAL_ATTRIBUTE = "global";
			const char* const BLOCKS_X_ATTRIBUTE = "blocksX";
			const char* const BLOCKS_Y_ATTRIBUTE = "blocksY";
			const char* const STATE_ATTRIBUTE = "state";
			const char* const TYPE_ATTRIBUTE = "type";
			const char* const SIZE_ATTRIBUTE = "size";
			const char* const X_ATTRIBUTE = "x";
			const char* const Y_ATTRIBUTE = "y";
			const char* const XP_ATTRIBUTE = "xp";
			const char* const YP_ATTRIBUTE = "yp";
			const char* const WIDTH_ATTRIBUTE = "width";
			const char* const HEIGHT_ATTRIBUTE = "height";
			const char* const WIDTHP_ATTRIBUTE = "widthp";
			const char* const HEIGHTP_ATTRIBUTE = "heightp";
			const char* const VERTICAL_ATTRIBUTE = "vertical";
			const char* const HORIZONTAL_ATTRIBUTE = "horizontal";
			const char* const SCROLL_TO_LEFT_ATTRIBUTE = "scrollToLeft";
			const char* const SCROLL_TO_TOP_ATTRIBUTE = "scrollToRight";
			const char* const NAME_ATTRIBUTE = "name";
			const char* const R_ATTRIBUTE = "r";
			const char* const G_ATTRIBUTE = "g";
			const char* const B_ATTRIBUTE = "b";
			const char* const A_ATTRIBUTE = "a";
			const char* const MIN_ATTRIBUTE = "min";
			const char* const MAX_ATTRIBUTE = "max";
			const char* const CURRENT_ATTRIBUTE = "current";

			const char* const ONCLICK_EVENT = "onClick";
			const char* const ONMOUSEOVER_EVENT = "onMouseOver";
			const char* const ONMOUSELEAVE_EVENT = "onMouseLeave";
			const char* const ONSELECT_EVENT = "onSelect";
			const char* const ONDESELECT_EVENT = "onDeselect";
			const char* const ONCHANGE_EVENT = "onChange";
			const char* const ONUPDATE_EVENT = "onUpdate";
		}
	}


	typedef void(*loadWidgetCallback)(tinyxml2::XMLElement*, Widget*);
	typedef void(*saveWidgetCallback)(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement*, Widget*);

	class GuiLoader
	{
	private:

		//int cellX, cellY;
		static GuiLoader* singleton;

		static void loadWidgetProperties(tinyxml2::XMLElement* widgetElement);
		static void attachToParent(Widget* widget, Widget* parent);
		static void loadWidget(tinyxml2::XMLElement* widgetElement, Widget* parent);
		static void loadMenu(tinyxml2::XMLElement* menuElement, Widget* parent);
		static void loadGridLayout(tinyxml2::XMLElement* gridElement, Widget* parent);
		static void loadButton(tinyxml2::XMLElement* buttonElement, Widget* parent);
		static void loadLabel(tinyxml2::XMLElement* labelElement, Widget* parent);
		static void loadEditbox(tinyxml2::XMLElement* editboxElement, Widget* parent);
		static void loadScrollbar(tinyxml2::XMLElement* scrollbarElement, Widget* parent);
		static void loadCheckbox(tinyxml2::XMLElement* checkboxElement, Widget* parent);
		static void loadScrollPane(tinyxml2::XMLElement* scrollPaneElement, Widget* parent);
		static void loadRadioButton(tinyxml2::XMLElement* radioButtonElement, Widget* parent);
		static void loadButtonGroup(tinyxml2::XMLElement* buttonGroupElement, Widget* parent);

		static void savePosition(tinyxml2::XMLDocument* doc , tinyxml2::XMLElement* parentElement, Widget* widget);
		static void savePosition(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, int x, int y, bool usePercenst);
		static void saveSize(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveSize(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, int width, int height, bool usePercents);
		static void saveText(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, ja::string text);
		static void saveIsRendered(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveIsRendered(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, bool isRendered);
		static void saveIsActive(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveIsActive(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, bool isActive);
		static void saveGridSize(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, int blocksX, int blocksY);
		static void saveId(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, unsigned int id);

		static void saveWidget(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveMenu(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveButton(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveLabel(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveScrollbar(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveEditbox(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveGridLayout(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveScrollPane(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveCheckbox(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveRadioButton(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);
		static void saveButtonGroup(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parentElement, Widget* widget);

		GuiLoader();
	public:
		std::map<int, Widget*> buttonGroups;
		Widget* activeButtonGroup;

		static std::map<ja::string, loadWidgetCallback> registeredLoaders;
		static std::map<WidgetType, saveWidgetCallback> registeredSavers;

		static void init();
		void registerWidgetLoaders();
		void registerWidgetSavers();
		static GuiLoader* getSingleton();
		static void loadGui(const ja::string& fileName);
		static void saveGui(ja::string& interfaceName, ja::string& fileName);

		~GuiLoader();
		static void cleanUp();
	};

}

