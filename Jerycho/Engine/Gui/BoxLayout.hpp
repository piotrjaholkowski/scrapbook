#pragma once

#include "Widget.hpp"
#include "Layout.hpp"
#include <vector>

namespace ja
{

	class BoxLayout : public Layout
	{
	private:
		int32_t  horizontalGap, verticalGap;
		uint32_t align;
		std::vector<Widget*> widgets;
		uint8_t r, g, b, a;
		bool xAxis;

		void setChildrenPosition(Widget* widget, uint32_t widgetTableId);
	public:
		BoxLayout(uint32_t id, Widget* parent, uint32_t align, bool xAxis, int32_t horizontalGap, int32_t verticalGap);
		~BoxLayout();

		void clear();
		void setSize(int32_t width, int32_t height);
		Widget* getWidget(uint32_t widgetId);

		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void draw(int32_t parentX, int32_t parentY);
		void update();
		void addWidget(Widget* widget);

		void updateCollisionMask();
		void updateChildrenCollisionMask();
	};

}