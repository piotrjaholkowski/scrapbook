#include "BoxLayout.hpp"
#include "../Manager/UIManager.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Utility/Exception.hpp"
#include "../Gui/Margin.hpp"

namespace ja
{

BoxLayout::BoxLayout(uint32_t id, Widget* parent, uint32_t align, bool xAxis, int32_t horizontalGap = 0, int32_t verticalGap = 0) : Layout(JA_BOX_LAYOUT,id,parent)
{
	setRender(true);
	setActive(true);
	setAsLayout(true);

	this->xAxis = xAxis;
	this->align = align;
	this->verticalGap = verticalGap;
	this->horizontalGap = horizontalGap;
}

BoxLayout::~BoxLayout()
{
	uint32_t widgetsNum = widgets.size();

	for(uint32_t i=0; i < widgetsNum; ++i)
	{
		Layout::removeReference(widgets[i]);
	}

	widgets.clear();
}


void BoxLayout::addWidget(Widget* widget)
{
	if(widget->isLayoutChild())
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI,"Widget is already added in diffrent layout");
	}

	if(widget->getReferenceNumber())
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI, "Widget is already added to diffrent interface");
	}

	widgets.push_back(widget);
	Layout::increaseReference(widget);
	widget->setParent(this); //it sets layout child flag

	setChildrenPosition(widget,widgets.size() - 1);
}

void BoxLayout::update()
{
	if(isActive())
	{
		uint32_t widgetsNum = widgets.size();
		for(uint32_t i=0; i<widgetsNum; ++i)
		{
			if(widgets[i]->isActive())
			{
				widgets[i]->update();
			}	
		}

		if (this->isSelectable())
		{
			if (this->isMouseOver())
			{
				UIManager::setMouseOverGui(true);
			}
		}
	}
}

void BoxLayout::draw(int32_t parentX, int32_t parentY)
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;

	if(parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	if(isRendered())
	{
		DisplayManager::setColorUb(r,g,b,a);
		if(a != 255)
		{
			DisplayManager::enableAlphaBlending();
			DisplayManager::drawRectangle(collisionMask.minX,collisionMask.minY,collisionMask.maxX,collisionMask.maxY,true);
			DisplayManager::disableAlphaBlending();
		}
		else
		{
			DisplayManager::drawRectangle(collisionMask.minX,collisionMask.minY,collisionMask.maxX,collisionMask.maxY,true);
		}

		uint32_t widgetsNum = widgets.size();
		for(uint32_t i=0; i<widgetsNum; ++i)
		{
			widgets[i]->draw(myX,myY);
		}
	}
}

void BoxLayout::setSize( int32_t width, int32_t height )
{
	Widget::setSize(width, height);
	uint32_t widgetsNum = widgets.size();
	
	for(uint32_t i=0; i<widgetsNum; ++i)
	{
		setChildrenPosition(widgets[i],i);
	}
}

void BoxLayout::setChildrenPosition(Widget* widget, uint32_t widgetTableId)
{
	int32_t  newPositionX;
	int32_t  newPositionY;
	uint32_t xFlags = align & 4294967288;
	uint32_t yFlags = align & 7;

	if(widgetTableId == 0)
	{
		if(xAxis)
		{
			newPositionX = horizontalGap;
			if (xFlags != (uint32_t)Margin::NORMAL)
			{
				switch(xFlags)
				{
				case (uint32_t)Margin::TOP:
					newPositionY = height - widget->getHeight() - verticalGap;
					break;
				case (uint32_t)Margin::DOWN:
					newPositionY = verticalGap;
					break;
				case (uint32_t)Margin::MIDDLE:
					newPositionY = (height / 2) - (widget->getHeight() / 2);
					break;
				}
			}
			else
			{
				newPositionY = verticalGap;
			}
		}
		else // yAxis
		{
			newPositionY = height - verticalGap - widget->getHeight();
			switch(yFlags)
			{
			case (uint32_t)Margin::LEFT:
				newPositionX = horizontalGap;
				break;
			case (uint32_t)Margin::CENTER:
				newPositionX = (width/2) - (widget->getWidth() / 2);
				break;
			case (uint32_t)Margin::RIGHT:
				newPositionX = width - widget->getWidth();
				break;
			default:
				newPositionX = horizontalGap;
				break;
			}
		}
	}
	else
	{
		Widget* pWidget = widgets[widgetTableId - 1];
		pWidget->getRelativePosition(newPositionX,newPositionY);
		
		if(xAxis)
		{
			newPositionX += pWidget->getWidth() + horizontalGap;
			if (xFlags != (uint32_t)Margin::NORMAL)
			{
				switch(xFlags)
				{
				case (uint32_t)Margin::TOP:
					newPositionY = height - widget->getHeight() - verticalGap;
					break;
				case (uint32_t)Margin::DOWN:
					newPositionY = verticalGap;
					break;
				case (uint32_t)Margin::MIDDLE:
					newPositionY = (height / 2) - (widget->getHeight() / 2);
					break;
				}
			}
			else
			{
				newPositionY = verticalGap;
			}
		}
		else
		{
			newPositionY -= (verticalGap + widget->getHeight());
			switch(yFlags)
			{
			case (uint32_t)Margin::LEFT:
				newPositionX = horizontalGap;
				break;
			case (uint32_t)Margin::CENTER:
				newPositionX = (width/2) - (widget->getWidth() / 2);
				break;
			case (uint32_t)Margin::RIGHT:
				newPositionX = width - widget->getWidth();
				break;
			default:
				newPositionX = horizontalGap;
				break;
			}
		}
	}

	Layout::forcePosition(widget, newPositionX, newPositionY);
	Layout::forceSize(widget, widget->getWidth(), widget->getHeight());

	widget->setLayoutBehaviour(this);
}

Widget* BoxLayout::getWidget( uint32_t widgetId )
{
	uint32_t widgetsNum = widgets.size();
	for(uint32_t i=0; i<widgetsNum; ++i)
	{
		if(widgets[i]->getId() == widgetId)
			return widgets[i];
	}

	return nullptr;
}

void BoxLayout::updateCollisionMask()
{
	Widget::updateCollisionMask();
	updateChildrenCollisionMask();
}

void BoxLayout::updateChildrenCollisionMask()
{
	uint32_t widgetsNum = widgets.size();
	for(uint32_t i=0; i<widgetsNum; ++i)
	{		
		widgets[i]->updateCollisionMask();
	}
}

void BoxLayout::setColor( uint8_t r, uint8_t g, uint8_t b, uint8_t a )
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

void BoxLayout::clear()
{
	uint32_t widgetsNum = widgets.size();

	for(uint32_t i=0; i < widgetsNum; ++i)
	{
		Layout::removeReference(widgets[i]);
	}

	widgets.clear();
}

}