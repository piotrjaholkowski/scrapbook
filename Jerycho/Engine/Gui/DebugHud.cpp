#include "DebugHud.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Gui/Margin.hpp"

namespace ja
{

DebugHud::DebugHud() : Interface(0,nullptr)
{
	widgetType = JA_HUD;

	leftLower  = new Label(jaLeftLowerCorner,nullptr);
	leftUpper  = new Label(jaLeftUpperCorner,nullptr);
	rightLower = new Label(jaRightLowerCorner,nullptr);
	rightUpper = new Label(jaRightUpperCorner,nullptr);

	leftLower->setAlign((uint32_t)Margin::DOWN | (uint32_t)Margin::LEFT);
	leftUpper->setAlign((uint32_t)Margin::TOP | (uint32_t)Margin::LEFT);
	rightLower->setAlign((uint32_t)Margin::RIGHT | (uint32_t)Margin::DOWN);
	rightUpper->setAlign((uint32_t)Margin::RIGHT | (uint32_t)Margin::TOP);

	addWidget(leftLower);
	addWidget(rightUpper);
	addWidget(rightLower);
	addWidget(leftUpper);
}

DebugHud::DebugHud(uint32_t id, Widget* parent) : Interface(id,parent)
{
	widgetType = JA_HUD;

	leftLower  = new Label(jaLeftLowerCorner,nullptr);
	leftUpper  = new Label(jaLeftUpperCorner,nullptr);
	rightLower = new Label(jaRightLowerCorner,nullptr);
	rightUpper = new Label(jaRightUpperCorner,nullptr);

	leftLower->setAlign((uint32_t)Margin::DOWN | (uint32_t)Margin::LEFT);
	leftUpper->setAlign((uint32_t)Margin::TOP | (uint32_t)Margin::LEFT);
	rightLower->setAlign((uint32_t)Margin::RIGHT | (uint32_t)Margin::DOWN);
	rightUpper->setAlign((uint32_t)Margin::RIGHT | (uint32_t)Margin::TOP);

	addWidget(leftLower);
	addWidget(rightUpper);
	addWidget(rightLower);
	addWidget(leftUpper);
	
}

void DebugHud::addField( ScreenPosition position, uint32_t fieldId, ja::string fieldValue)
{
	fields[fieldId] = DebugHudField(position, fieldValue);
}

void DebugHud::setFieldValue( uint32_t fieldId, const char* fieldValue )
{
	fields[fieldId].fieldValue = fieldValue;
}

void DebugHud::appendFieldValue( uint32_t fieldId, const char* fieldValue )
{
	fields[fieldId].fieldValue.append(fieldValue);
}

DebugHud::~DebugHud()
{
	fields.clear();
}

void DebugHud::update()
{
	int32_t width, height;
	DisplayManager::getSingleton()->getResolution(&width,&height);

	ja::string lu,ru,ll,rl;

	std::map<uint32_t, DebugHudField>::iterator it;
	for(it = fields.begin(); it != fields.end(); it++)
	{
		switch((*it).second.hudFieldPosition)
		{
		case jaLeftUpperCorner:
			lu.append((*it).second.fieldValue);
			lu.append("\n");
			break;
		case jaRightLowerCorner:
			rl.append((*it).second.fieldValue);
			rl.append("\n");
			break;
		case jaLeftLowerCorner:
			ll.append((*it).second.fieldValue);
			ll.append("\n");
			break;
		case jaRightUpperCorner:
			ru.append((*it).second.fieldValue);
			ru.append("\n");
			break;
		}
	}

	leftLower->setText(ll.c_str());
	rightLower->setText(rl.c_str());
	leftUpper->setText(lu.c_str());
	rightUpper->setText(ru.c_str());

	leftUpper->setPosition(5,height);
	rightUpper->setPosition(width,height);
	leftLower->setPosition(5,0);
	rightLower->setPosition(width,0);

	Interface::update();
}

void DebugHud::draw()
{
	Interface::draw();
}

void DebugHud::setFont( const char* fontName, uint32_t fontSize, ScreenPosition fieldID, uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	switch(fieldID)
	{
	case jaLeftLowerCorner:
		leftLower->setFont(fontName,fontSize);
		leftLower->setColor(r,g,b,a);
		break;
	case jaLeftUpperCorner:
		leftUpper->setFont(fontName,fontSize);
		leftUpper->setColor(r,g,b,a);
		break;
	case jaRightLowerCorner:
		rightLower->setFont(fontName,fontSize);
		rightLower->setColor(r,g,b,a);
		break;
	case jaRightUpperCorner:
		rightUpper->setFont(fontName,fontSize);
		rightUpper->setColor(r,g,b,a);
		break;
	}
}

void DebugHud::setFieldValuePrintC( uint32_t fieldId, const char* fieldValue,... )
{
	char temp[256];
	va_list	args;

	if (fieldValue == nullptr) *temp=0;
	else {
		va_start(args, fieldValue);
		#pragma warning(disable : 4996)
		vsprintf(temp, fieldValue, args);
		#pragma warning(default : 4996)
		va_end(args);											
	}

	fields[fieldId].fieldValue = temp;
}

void DebugHud::appendFieldValuePrintC( uint32_t fieldId, const char* fieldValue, ... )
{
	char temp[256];
	va_list	args;

	if (fieldValue == nullptr) *temp=0;
	else {
		va_start(args, fieldValue);
#pragma warning(disable : 4996)
		vsprintf(temp, fieldValue, args);
#pragma warning(default : 4996)
		va_end(args);											
	}

	fields[fieldId].fieldValue.append(temp);
}

}