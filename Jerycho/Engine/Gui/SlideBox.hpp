#pragma once

#include "Layout.hpp"
#include "OnUpdateListener.hpp"
#include <map>
#include <vector>
#include <string>

namespace ja
{

	class SlideBox : public Layout
	{
	protected:
		uint32_t dockedTo;
		int32_t slideArea;
		int32_t area;
		WidgetMask slideMask;
		WidgetMask hideMask;
		Widget* box;

	public:
		SlideBox(uint32_t id);

		void updateSize();
		virtual void draw(int32_t parentX, int32_t parentY);

		bool isSlided();
		void setPosition(int32_t x, int32_t y);
		void setSize(int32_t width, int32_t height);
		void setBox(Widget* widget);
		void dockTo(uint32_t margin);
		void updateCollisionMask();
		void setSlideArea(int32_t slideArea);
		void setArea(int32_t area);

		void update();
		virtual ~SlideBox();

	};

}
