#pragma once

#include "Interface.hpp"
#include "../Font/Font.hpp"
#include <string>

namespace ja
{

	class Menu : public Interface
	{
	protected:
		ja::string text;
		uint8_t  r, g, b;
		uint32_t marginStyle;
		Font* font;

	public:
		Menu(uint32_t id, Widget* parent);
		void setColor(uint8_t r, uint8_t g, uint8_t b);
		void setText(const char* text);
		void setAlign(uint32_t marginStyle);
		void setFont(const char* fontName, uint32_t fontSize);
		ja::string getText();

		void updateCollisionMask();
		void updateChildrenCollisionMask();

		virtual ~Menu();
	};

}
