#pragma once

#include "Widget.hpp"
#include "Label.hpp"
#include "Layout.hpp"
#include "Scrollbar.hpp"
#include "OnClickListener.hpp"
#include "OnMouseOverListener.hpp"
#include "OnMouseLeaveListener.hpp"
#include "OnSelectListener.hpp"
#include "OnDeselectListener.hpp"
#include "OnDropListener.hpp"
#include "OnChangeListener.hpp"
#include <string>

#include <stdint.h>

namespace ja
{
	class SequenceMarker
	{
	public:
		float    position;
		bool     isMarkerTop;
		uint16_t markerSize;
		uint8_t  r, g, b;

	private:
		void getMarkerMask(int16_t parentX, int16_t parentY, int16_t parentWidth, int16_t parentHeight, WidgetMask& markerMask)
		{
			const uint16_t posX       = (uint16_t)((float)parentWidth * this->position);
			const uint16_t markerPosX = parentX + posX;

			if (this->isMarkerTop)
			{
				const uint16_t markerTopPos = parentY + parentHeight;
				markerMask.minX = markerPosX - markerSize;
				markerMask.minY = markerTopPos - markerSize;
				markerMask.maxX = markerPosX + markerSize;
				markerMask.maxY = markerTopPos;
			}
			else
			{
				const uint16_t markerBottomPos = parentY;
				markerMask.minX = markerPosX - markerSize;
				markerMask.minY = markerBottomPos;
				markerMask.maxX = markerPosX + markerSize;
				markerMask.maxY = markerBottomPos + markerSize;
			}
			
		}

		friend class SequenceEditor;
	};

	class SequenceEditor : public Layout
	{
	protected:
		uint8_t r, g, b, a;
		uint8_t borderR, borderG, borderB, borderA;

		uint8_t selectedMarkerR, selectedMarkerG, selectedMarkerB;

		Scrollbar scrollbar;

		SequenceMarker* editedMarker;
		int32_t         markerMoveSensitivity;
		int32_t         editedStartPosition;
		bool            editedMarkerMoves;
		SequenceMarker* selectedMarker;

		std::vector<SequenceMarker*> markers;

	public:
		OnSelectListener     onSelect;
		OnClickListener      onClick;
		OnMouseOverListener  onMouseOver;
		OnMouseLeaveListener onMouseLeave;
		OnDeselectListener   onDeselect;
		OnChangeListener     onChange;

		void* vUserData;
	public:
		SequenceEditor(uint32_t id, Widget* parent);
		~SequenceEditor();
		SequenceMarker* createMarker();
		SequenceMarker* getSelectedMarker();
		std::vector<SequenceMarker*>& getMarkers();
		void clearMarkers();
		void setColor(uint8_t r, uint8_t g, uint8_t b);
		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setSelectedMarkerColor(uint8_t r, uint8_t g, uint8_t b);
		void setMarkerMoveSensitivity(uint32_t sensitivity);
		void setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setAlpha(uint8_t alpha);
		void setFontColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setScrollbarColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

		virtual void setPosition(int32_t x, int32_t y);
		void setActivateClickByKey(int32_t key);
		virtual void update();
		virtual void updateSize();
		virtual void draw(int32_t parentX, int32_t parentY);

		void updateCollisionMask();
		void updateChildrenCollisionMask();

		template < class Y, class X >
		void setOnClickEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onClick.setActionEvent(pthis, pmethod);
			onClick.setActive(true);
		}

		void setOnClickEvent(ScriptDelegate* scriptDelegate)
		{
			onClick.setActionEvent(scriptDelegate);
			onClick.setActive(true);
		}

		template < class Y, class X >
		void setOnMouseOverEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onMouseOver.setActionEvent(pthis, pmethod);
			onMouseOver.setActive(true);
		}

		void setOnMouseOverEvent(ScriptDelegate* scriptDelegate)
		{
			onMouseOver.setActionEvent(scriptDelegate);
			onMouseOver.setActive(true);
		}

		template < class Y, class X >
		void setOnMouseLeaveEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onMouseLeave.setActionEvent(pthis, pmethod);
			onMouseLeave.setActive(true);
		}

		void setOnMouseLeaveEvent(ScriptDelegate* scriptDelegate)
		{
			onMouseLeave.setActionEvent(scriptDelegate);
			onMouseLeave.setActive(true);
		}

		template < class Y, class X >
		void setOnSelectEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onSelect.setActionEvent(pthis, pmethod);
			onSelect.setActive(true);
		}

		void setOnSelectEvent(ScriptDelegate* scriptDelegate)
		{
			onSelect.setActionEvent(scriptDelegate);
			onSelect.setActive(true);
		}

		template < class Y, class X >
		void setOnDeselectEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onDeselect.setActionEvent(pthis, pmethod);
			onDeselect.setActive(true);
		}

		void setOnDeselectEvent(ScriptDelegate* scriptDelegate)
		{
			onDeselect.setActionEvent(scriptDelegate);
			onDeselect.setActive(true);
		}

		template < class Y, class X >
		void setOnChangeEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onChange.setActionEvent(pthis, pmethod);
			onChange.setActive(true);
		}

		void setOnChangeEvent(ScriptDelegate* scriptDelegate)
		{
			onChange.setActionEvent(scriptDelegate);
			onChange.setActive(true);
		}

	};

}