#pragma once

#include "Widget.hpp"
#include "../Manager/UIManager.hpp"

namespace ja
{

	class OnSelectListener : public WidgetListener
	{
	public:
		bool lastSelected;

		OnSelectListener() : WidgetListener(JA_ONSELECT)
		{
			lastSelected = false;
		}

		bool processListener(Widget* widget)
		{
			if (widget->isActive())
			{
				if (isActive())
				{
					if (UIManager::getLastSelected() == widget)
					{
						if (lastSelected == true)
						{
							return false;
						}
						lastSelected = true;
						fireEvent(widget);
						return true;
					}
					else
					{
						lastSelected = false;
					}
				}
			}
			return false;
		}

		void fireEvent(Widget* widget)
		{
			if (callback)
				callback(widget, JA_ONSELECT, nullptr);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONSELECT, nullptr);
		}

		~OnSelectListener()
		{

		}
	};

}