#include "SlideBox.hpp"
#include "../Manager/InputManager.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/UIManager.hpp"
#include "../Utility/Exception.hpp"
#include "../Gui/Margin.hpp"

namespace ja
{ 

SlideBox::SlideBox(uint32_t id) : Layout(JA_SLIDEBOX, id, nullptr)
{
	setRender(true);
	setActive(true);

	collisionMask.setMask(INT_MIN, INT_MIN, INT_MAX, INT_MAX);
	dockedTo = (uint32_t)Margin::TOP;
	box = nullptr;
	slideArea = 10;
	area = 28;
}

void SlideBox::draw(int32_t parentX, int32_t parentY)
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;

	if (parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	if (isRendered()) {
		if (box->isRendered()) {
			box->draw(hideMask.minX,hideMask.minY);
		}
	}
}

void SlideBox::update()
{
	int32_t mouseX = InputManager::getMousePositionX();
	int32_t mouseY = InputManager::getMousePositionY();

	mouseY = DisplayManager::getSingleton()->getResolutionHeight() - mouseY;

	if (slideMask.intersect(mouseX, mouseY))
	{
		box->setRender(true);
		box->setActive(true);
	}
	else {

		if (!hideMask.intersect(mouseX, mouseY))
		{
			box->setRender(false);
			box->setActive(false);
		}
	}

	if (isActive()) {
		if (box->isActive())
		{
			box->update();
			// don't update any other widgets if this
			// widget is modal
			if (box->isModal())
			{
				return;
			}
		}
	}
}

SlideBox::~SlideBox()
{
	if (box != nullptr) {
		Layout::removeReference(box);
	}
}

void SlideBox::updateSize()
{

}

void SlideBox::setPosition(int32_t x, int32_t y)
{
	Widget::setPosition(x, y);

	if (parent == nullptr)
	{
		collisionMask.setMask(INT_MIN, INT_MIN, INT_MAX, INT_MAX);
	}
}

void SlideBox::setSize(int32_t width, int32_t height)
{
	/*jaWidget::setSize(width, height);

	if (parent == NULL)
	{
		collisionMask.setMask(INT_MIN, INT_MIN, INT_MAX, INT_MAX);
	}*/
}

void SlideBox::dockTo(uint32_t margin)
{
	//int width, height;
	//jaDisplayManager::getSingleton()->getResolution(&width, &height);
	dockedTo = margin;

	if (parent != nullptr)
		updateCollisionMask();
}

void SlideBox::setBox(Widget* widget)
{
	if (box != nullptr)
		return;

	box = widget;
	box->setActive(false);
	box->setRender(false);

	//widget->setParent(this);
	Layout::increaseReference(widget);

	if (parent != nullptr)
		updateCollisionMask();
}

void SlideBox::updateCollisionMask()
{
	//called every time parent changes size or position

	if (parent)
	{
		WidgetMask parentMask = parent->getCollisionMask();

		switch (dockedTo) {
		case (uint32_t)Margin::LEFT:
			break;
		case (uint32_t)Margin::RIGHT:
			break;
		case (uint32_t)Margin::TOP:
			slideMask.minX = parentMask.minX;
			slideMask.minY = parentMask.maxY - slideArea;
			slideMask.maxX = parentMask.maxX;
			slideMask.maxY = parentMask.maxY;
			hideMask.minX = parentMask.minX;
			hideMask.minY = parentMask.maxY - area;
			hideMask.maxX = parentMask.maxX;
			hideMask.maxY = parentMask.maxY;
		case (uint32_t)Margin::DOWN:
			break;
		}

		if (box != nullptr) {
			switch (dockedTo) {
			case (uint32_t)Margin::LEFT:
				break;
			case (uint32_t)Margin::RIGHT:
				break;
			case (uint32_t)Margin::TOP:
				box->x = hideMask.minX;
				box->y = hideMask.minY;
				box->width = hideMask.maxX - hideMask.minX;
				box->height = hideMask.maxY - hideMask.minY;

				box->updateCollisionMask();
				break;
			case (uint32_t)Margin::DOWN:
				break;
			}
		}
	}
}

void SlideBox::setSlideArea(int32_t slideArea)
{
	this->slideArea = slideArea;
	updateCollisionMask();
}

void SlideBox::setArea(int32_t area)
{
	this->area = area;
	updateCollisionMask();
}

bool SlideBox::isSlided()
{
	if (box != nullptr)
	{
		return box->isRendered();
	}

	return false;
}

}