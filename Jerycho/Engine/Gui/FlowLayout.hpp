#pragma once

#include "Widget.hpp"
#include "Layout.hpp"
#include <vector>

namespace ja
{

	class FlowLayout : public Layout
	{
	private:
		int32_t  horizontalGap, verticalGap;
		uint32_t align;
		std::vector<Widget*> widgets;
		uint8_t r, g, b, a;

		void setChildrenPosition(Widget* widget, uint32_t xFlags, int32_t& lastRowXPosition, int32_t lastRowYPosition, uint32_t rowHeight);
		void getRowSize(uint32_t startWidgetRow, uint32_t& nextWidgetRow, int32_t& rowWidth, int32_t& rowHeight);
	public:
		FlowLayout(uint32_t id, Widget* parent, uint32_t align, int32_t horizontalGap, int32_t verticalGap);

		~FlowLayout();

		void setSize(int32_t width, int32_t height);

		Widget* getWidget(uint32_t widgetId);

		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void draw(int32_t parentX, int32_t parentY);
		void update();

		void addWidget(Widget* widget);

		void updateCollisionMask();
		void updateChildrenCollisionMask();
	};

}