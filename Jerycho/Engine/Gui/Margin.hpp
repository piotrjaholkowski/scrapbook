#pragma once

namespace ja
{
	enum class Margin{
		LEFT   = 1,
		CENTER = 2,
		RIGHT  = 4,
		TOP    = 8,
		MIDDLE = 16,
		DOWN   = 32,
		NORMAL = 64
	};
}