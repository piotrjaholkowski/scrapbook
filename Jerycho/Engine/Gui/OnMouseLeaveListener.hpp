#pragma once

#include "Widget.hpp"
#include "../Manager/UIManager.hpp"
#include "../Font/Font.hpp"
#include <string>

namespace ja
{

	class OnMouseLeaveListener : public WidgetListener
	{
	public:
		int mouseLeave;

		OnMouseLeaveListener() : WidgetListener(JA_ONMOUSELEAVE), mouseLeave(0)
		{

		}

		bool processListener(Widget* widget)
		{
			if (isActive())
				if (widget->isActive())
				{
					if (widget->isMouseOver())
					{
						mouseLeave = 1;
						return false;
					}
					else
					{
						if (mouseLeave == 1)
						{
							mouseLeave = 0;
							fireEvent(widget);
							return true;
						}
					}
				}
			return false;
		}

		void fireEvent(Widget* widget)
		{
			if (callback)
				callback(widget, JA_ONMOUSELEAVE, nullptr);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONMOUSELEAVE, nullptr);
		}

		~OnMouseLeaveListener()
		{

		}
	};

}