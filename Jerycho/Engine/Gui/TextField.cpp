#include "TextField.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Utility/Exception.hpp"

#include "../../Engine/Gui/Margin.hpp"

#include <iostream>
#include <string>

namespace ja
{ 

TextField::TextField(uint32_t id, Widget* parent) : Widget(JA_TEXTFIELD,id,parent)
{
	textOffsetX = 0;
	textOffsetY = 0;
	setRender(true);
	setActive(true);
	setSelectable(true);
	onClick.setActive(true);
	onClick.activateByKey = 0;
	onSelect.setActive(true);
	onDeselect.setActive(true);

	acceptOnlyNumbers = false;
	allowDynamicBuffering = false;
	setFont("default.ttf",10);
	label.setText("");
	label.setParent(this);
	label.setAlign((uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE);
	textBuffer = "";
	currentColumn = 0;
	columnsNum = 0;
}

void TextField::setFont(const char* fontName, uint32_t fontSize)
{
	label.setFont(fontName,fontSize);
	updateTextPosition();
}

void TextField::setText(const char* text)
{
	clearText();
	processText(text);
}

void TextField::updateTextPosition()
{
	Font* font = label.getFont();
	letterHeight = font->getHighestCharacterSize();
	cursorWidth  = font->getWidthestCharacterSize();

	int32_t width, height;
	float textX, textY;
	if(font == nullptr)
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI,"You need to set font before updateTextPosition");
	}
	font->getTextSize(width,height,textBuffer);
	textX = (this->width/2) - ((float)(width)/2);
	textY = (this->height/2);
	label.setPosition(textX, textY);
}

void TextField::setFieldValue(const char* fieldValue,...)
{
	char temp[256];
	va_list	args;

	if (fieldValue == nullptr) *temp=0;
	else {
		va_start(args, fieldValue);
	#pragma warning(disable : 4996)
		vsprintf(temp, fieldValue, args);
	#pragma warning(default : 4996)
		va_end(args);	
	}

	setText(temp);
}

#pragma warning(disable:4018)
void TextField::processText(const char* text)
{
	if(!allowDynamicBuffering)
	{
		if(textBuffer.size() == columnsNum)
		{
			return;
		}
	}

	if (nullptr == text)
	{
		label.setText("");
	}
	else
	{
		for (uint32_t i = 0;; i++)
		{
			if (text[i] == '\n')
			{
				continue;
			}

			if (text[i] == '\0')
			{
				label.setText(textBuffer.c_str());
				break;
			}

			if (textBuffer.size() >= columnsNum)
			{
				if (allowDynamicBuffering)
				{
					++columnsNum;
				}
				else
				{
					label.setText(textBuffer.c_str());
					break;
				}
			}

			textBuffer.insert(currentColumn, 1, text[i]);
			++currentColumn;
		}
	}

	updateTextPosition();
}
#pragma warning(default:4018)

ja::string TextField::getText()
{
	return textBuffer;
}

const char* TextField::getTextPtr()
{
	return textBuffer.c_str();
}

#pragma warning(disable : 4018)
#pragma warning(disable : 4996)

bool TextField::getAsFloat(float* val)
{
	const char* text = textBuffer.c_str();;
	int32_t num = sscanf(text,"%f",val);
	if(num == 1)
		return true;
	return false;
}

bool TextField::getAsInt32(int32_t* val)
{
	const char* text = textBuffer.c_str();;
	int32_t num = sscanf(text,"%d",val);
	if(num == 1)
		return true;
	return false;
}

bool TextField::getAsInt16(int16_t* val)
{
	const char* text = textBuffer.c_str();;
	int32_t num = sscanf(text,"%hd",val);
	if(num == 1)
		return true;
	return false;
}

bool TextField::getAsUInt32( uint32_t* val )
{
	const char* text = textBuffer.c_str();;
	int32_t num = sscanf(text,"%u",val);
	if(num == 1)
		return true;
	return false;
}

bool TextField::getAsUInt16(uint16_t* val)
{
	const char* text = textBuffer.c_str();
	int32_t num = sscanf(text,"%hu",val);
	if(num == 1)
		return true;
	return false;
}

bool TextField::getAsUInt8(uint8_t* val)
{
	int32_t valI;
	const char* text = textBuffer.c_str();
	int32_t num = sscanf(text, "%u", &valI);
	if (num == 1)
	{
		*val = (uint8_t)(valI);
		return true;
	}
	return false;
}

bool TextField::getAsCustom(const char* query, void* custom)
{
	const char* text = textBuffer.c_str();
	int32_t num = sscanf(text,query,custom);
	if(num == 1)
		return true;
	return false;
}

bool TextField::getAsInt8(int8_t* val)
{
	int32_t valI;
	const char* text = textBuffer.c_str();
	int num = sscanf(text,"%d",&valI);
	if(num == 1)
	{
		*val = static_cast<char>(valI);
		return true;
	}
	return false;
}
#pragma warning(default : 4996)
#pragma warning(default : 4018)

void TextField::clearText()
{
	textBuffer = "";
	label.setText("");
	currentColumn = 0;
}

void TextField::draw(int32_t parentX, int32_t parentY)
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;

	if(parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	if(!collisionMask.canIntersect) // scissors region invalidate whole rendering area
		return;

	glEnable(GL_SCISSOR_TEST);
	glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);
	DisplayManager::setColorUb(r,g,b,a);
	if(a!=255)
	{
		DisplayManager::enableAlphaBlending();
		DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
		DisplayManager::disableAlphaBlending();
	}
	else
	{
		DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
	}

	if(onSelect.lastSelected)
	{
		float cursorX,cursorY;
		getCursorPosition(cursorX,cursorY);

		cursorX += myX;
		cursorY += myY;

		DisplayManager::setColorUb(153,217,234,255);
		DisplayManager::drawRectangle(cursorX,cursorY,cursorX + cursorWidth,cursorY + letterHeight,true);
	}

	if(this->isBorderRendered())
	{
		DisplayManager::setColorUb(borderR,borderG,borderB,borderA);
		DisplayManager::drawRectangle(myX + 1, myY + 1, myX + width - 1, myY + height - 1, false);
	}

	label.draw(myX + textOffsetX, myY + textOffsetY);
	glDisable(GL_SCISSOR_TEST);
}

void TextField::setFontColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	label.setColor(r,g,b,a);
}

void TextField::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

void TextField::setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->borderR = r;
	this->borderG = g;
	this->borderB = b;
	this->borderA = a;
}

void TextField::setAlpha(uint8_t a)
{
	this->a = a;
}

void TextField::setPosition(int32_t x, int32_t y )
{
	Widget::setPosition(x, y);
	updateTextPosition();
}

void TextField::setSize(int32_t width, int32_t height)
{
	ja::string text = getText(); 

	clearText();
	Widget::setSize(width, height);

	Font* font = label.getFont();
	
	textOffsetX = 0;
	textOffsetY = 0;
	columnsNum = width / cursorWidth;

	processText(text.c_str()); // to doda�em
}

void TextField::setTextSize(int32_t columnsNum, int32_t fieldHeight)
{
	ja::string text = getText(); // to doda�em

	clearText();
	this->columnsNum = columnsNum;

	// sets widget size, height
	Font* font = label.getFont();

	textOffsetX = 0;
	textOffsetY = 0;
	if(fieldHeight == 0)
	{
		Widget::setSize(cursorWidth * columnsNum, letterHeight);
	}
	else
	{
		Widget::setSize(cursorWidth * columnsNum, fieldHeight);
	}

	processText(text.c_str());
}


void TextField::getCursorPosition( float& cursorX, float& cursorY )
{
	Font* font = label.getFont();

	int32_t lineWidth;
	int32_t lineHeight;

	font->getTextSize(lineWidth,lineHeight,textBuffer);

	if(lineWidth == 0)
	{
		cursorX = (float)(width) / 2;
	}
	else
	{
		cursorX = ((float)(width) / 2) -  (lineWidth / 2) + (currentColumn * cursorWidth);//static_cast<float>(lineWidth - cursorWidth); 
	}

	cursorY = (this->height/2) - ((float)(letterHeight) / 2);
}

#pragma warning(disable : 4018)
void TextField::setCursorPosition(int32_t posX, int32_t posY)
{
	int32_t widgetX, widgetY;
	getPosition(widgetX,widgetY);
	posX -= widgetX;

	int32_t rX,rY;
	label.getRelativePosition(rX,rY);
	posX -= rX;

	int32_t index;
	label.getFont()->getLetterIndex(textBuffer,posX,index);
	currentColumn = index;
}
#pragma warning(default : 4018)

////////////////////////////////////

void TextField::update()
{ 
	if(onSelect.lastSelected) // get pressed keys
	{
		char keyBuffer[12];
		char ck = 0;

		if(InputManager::isKeyPressed(JA_LEFT))
		{
			if(currentColumn > 0)
				--currentColumn;
		}

		if(InputManager::isKeyPressed(JA_RIGHT))
		{
			if(currentColumn < textBuffer.size())
				++currentColumn;
		}

		if(InputManager::isKeyPressed(JA_BACKSPACE))
		{
			if(currentColumn > 0)
			{
				textBuffer.erase(--currentColumn,1);
				if(textBuffer.size() != 0)
					label.setText(textBuffer.c_str());
				else
					label.setText("");

				updateTextPosition();
			}
		}


		std::list<InputAction>* inputEvents = InputManager::getInputEvents();

		if(!inputEvents->empty())
		{
			std::list<InputAction>::iterator it;
		
			for(it = inputEvents->begin();it!=inputEvents->end();it++)
			{
				if(((*it).inputEvent == JA_KEYPRESSED) && ((*it).unicodeChar != 0))
				{
					if(acceptOnlyNumbers)
					{
						if((it->unicodeChar >= 48) && (it->unicodeChar <= 57))
						{
							//numbers 48 - 0 ; 57 - 9
						}
						else
						{
							//sign or point
							//45 -
							//46 .
							if((it->unicodeChar == 45) || (it->unicodeChar == 46))
							{

							}
							else
							{
								if(it->unicodeChar == 13)
								{
									keyBuffer[ck++] = '\n';
									continue;
								}
								continue;
							}
						}
					}

					if(it->unicodeChar == 13)
					{
						keyBuffer[ck++] = '\n';
						continue;
					}

					if(it->unicodeChar < 13)
						continue;

					keyBuffer[ck++] = (char)it->unicodeChar;
					if(ck == 12)
						break;
				}
				
			}
		}

		if(ck != 0)
		{
			keyBuffer[ck++] = '\0';
			processText(keyBuffer);
		}
		
	}
	
	onMouseOver.processListener(this);
	onMouseLeave.processListener(this);

	if(onClick.processListener(this))
	{
		UIManager::getSingleton()->setSelected(this);

		int32_t mousePosX = InputManager::getMousePositionX();
		int32_t mousePosY = DisplayManager::getSingleton()->getResolutionHeight() - InputManager::getMousePositionY();
		setCursorPosition(mousePosX,mousePosY);
	}

	if(onSelect.processListener(this)) 
	{
	
	}

	if(onDeselect.processListener(this)) 
	{
		
	}
	
}

void TextField::updateSize()
{
	updateTextPosition();
}

bool TextField::isUsingUnicode()
{
	return true;
}

}