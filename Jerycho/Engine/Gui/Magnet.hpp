#pragma once

#include "ImageButton.hpp"
#include "OnClickListener.hpp"

namespace ja
{

	class Magnet : public ImageButton
	{
	protected:
		std::vector<Widget*> linkedWidgets;
		int32_t oldPositionX, oldPositionY;

	public:
		Magnet(uint32_t id, Widget* parent);

		void addLink(Widget* widget);

		virtual void update();
		virtual ~Magnet();
	};

}