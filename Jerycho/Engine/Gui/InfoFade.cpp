#include "InfoFade.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/ResourceManager.hpp"
#include "../Manager/UIManager.hpp"
#include "../Utility/Exception.hpp"
#include "../Gui/Margin.hpp"

#include <iostream>
#include <string>

namespace ja
{ 

InfoFade::InfoFade(uint32_t id, Widget* parent) : Widget(JA_INFO_FADE,id,parent)
{
	setRender(true);
	setActive(true);
	setSelectable(false);
	
	align = (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE;
	setFont("default.ttf",10);

	isWorldMatrix = false;
	rS, gS, gS, aS = 0;
	shadowOffsetX = 0;
	shadowOffsetY = 0;
	worldScaleMultiplier = 1.0f;
}

void InfoFade::setFont(const char* fontName, uint32_t fontSize)
{
	ResourceManager* resource = ResourceManager::getSingleton();
	this->font = resource->getEternalFont(fontName, fontSize)->resource;
}

void InfoFade::draw(int32_t parentX, int32_t parentY)
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;

	if(parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	onBeginDraw.processListener(this);
	if (isWorldMatrix)
	{
		DisplayManager::pushWorldMatrix();
		std::list<InfoFadeData>::iterator itFade = infoFadeData.begin();
		float shadowXScaled = (shadowOffsetX * worldScaleMultiplier);
		float shadowYScaled = (shadowOffsetY * worldScaleMultiplier);
		for (itFade; itFade != infoFadeData.end(); ++itFade)
		{
			jaFloat alphaLinear = itFade->fadeTime / itFade->fadeTimeLength;
			uint8_t alpha = (uint8_t)(255.0f * alphaLinear);

			if (aS != 0)
			{
				uint8_t alphaShadow = (uint8_t)(aS * alphaLinear);
				DisplayManager::RenderTextWorld(font, itFade->x + shadowXScaled, itFade->y + shadowYScaled, worldScaleMultiplier, itFade->info, align, rS, gS, bS, alphaShadow);
			}

			DisplayManager::RenderTextWorld(font, itFade->x, itFade->y, worldScaleMultiplier, itFade->info, align, itFade->r, itFade->g, itFade->b, alpha);
		}
		DisplayManager::popWorldMatrix();
	}
	else
	{
		std::list<InfoFadeData>::iterator itFade = infoFadeData.begin();

		for (itFade; itFade != infoFadeData.end(); ++itFade)
		{
			jaFloat alphaLinear = itFade->fadeTime / itFade->fadeTimeLength;
			uint8_t alpha = (uint8_t)(255.0f * alphaLinear);

			if (aS != 0)
			{
				uint8_t alphaShadow = (uint8_t)(aS * alphaLinear);
				DisplayManager::RenderText(font, myX + itFade->x + shadowOffsetX, myY + itFade->y + shadowOffsetY, itFade->info, align, rS, gS, bS, alphaShadow);
			}

			DisplayManager::RenderText(font, myX + itFade->x, myY + itFade->y, itFade->info, align, itFade->r, itFade->g, itFade->b, alpha);
		}
	}
	onEndDraw.processListener(this);
}

/*void jaInfoFade::setFontColor(unsigned char r, unsigned char g, unsigned char b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}*/

void InfoFade::setPosition(int32_t x, int32_t y )
{
	Widget::setPosition(x, y);
}

void InfoFade::update()
{      
	std::list<InfoFadeData>::iterator itFade = infoFadeData.begin();
	float deltaTime = UIManager::getSingleton()->getDeltaTime();
	const int32_t removeMaxCount = 10;
	std::list<InfoFadeData>::iterator removeFadeData[removeMaxCount];
	int32_t removeFadeDataNum = 0;

	for(itFade; itFade != infoFadeData.end(); ++itFade)
	{
		itFade->fadeTime -= deltaTime;
		if(itFade->fadeTime < 0.0f)
		{
			removeFadeData[removeFadeDataNum] = itFade;
			++removeFadeDataNum;
			if(removeFadeDataNum == removeMaxCount)
				break;
		}
	}

	for(int32_t i=0; i<removeFadeDataNum; ++i)
	{
		infoFadeData.erase(removeFadeData[i]); // error here
	}

	onUpdate.processListener(this);
}

void InfoFade::addInfo( uint16_t infoId, float x, float y, uint8_t r, uint8_t g, uint8_t b, float fadeTime, const char* info)
{
	std::list<InfoFadeData>::iterator itData = infoFadeData.begin();

	int32_t strLength = strlen(info);
	if(strLength > ja::setup::gui::INFO_FADE_MAX_CHARS_NUM)
		strLength = ja::setup::gui::INFO_FADE_MAX_CHARS_NUM;
	++strLength;

	for(itData; itData != infoFadeData.end(); ++itData)
	{
		if(itData->infoId == infoId)
		{
			itData->x = x;
			itData->y = y;
			itData->r = r;
			itData->g = g;
			itData->b = b;
			itData->fadeTime = fadeTime;
			itData->fadeTimeLength = fadeTime;
#pragma warning(disable:4996)
			strncpy(itData->info, info, strLength);
#pragma warning(default:4996)
			return;
		}
	}

	infoFadeData.push_back(InfoFadeData());
	InfoFadeData& newData = infoFadeData.back();
	newData.infoId = infoId;
	newData.x = x;
	newData.y = y;
	newData.r = r;
	newData.g = g;
	newData.b = b;
	newData.fadeTime = fadeTime;
	newData.fadeTimeLength = fadeTime;
#pragma warning(disable:4996)
	strncpy(newData.info, info, strLength);
#pragma warning(default:4996)
}

void InfoFade::addInfoC( uint16_t infoId, float x, float y, uint8_t r, uint8_t g, uint8_t b, float fadeTime, const char* info, ... )
{
	char temp[ja::setup::gui::INFO_FADE_MAX_CHARS_NUM];
	va_list	args;

	if (info == nullptr) *temp=0;
	else {
		va_start(args, info);
#pragma warning(disable : 4996)
		vsprintf(temp, info, args);
#pragma warning(default : 4996)
		va_end(args);	
	}

	addInfo(infoId,x,y,r,g,b,fadeTime,temp);
}

void InfoFade::setShadowColor( uint8_t r, uint8_t g, uint8_t b, uint8_t a )
{
	rS = r;
	gS = g;
	bS = b;
	aS = a;
}

void InfoFade::setShadowOffset( float x, float y )
{
	shadowOffsetX = x;
	shadowOffsetY = y;
}

void InfoFade::setAlign(uint32_t marginStyle)
{
	align = marginStyle;
}

}