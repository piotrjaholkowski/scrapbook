#pragma once

#include "Widget.hpp"

namespace ja
{

	class OnDrawListener : public WidgetListener
	{
	public:

		OnDrawListener() : WidgetListener(JA_ONDRAW)
		{

		}

		bool processListener(Widget* widget)
		{
			if (isActive())
			{
				if (widget->isRendered())
				{
					fireEvent(widget);
					return true;
				}
			}

			return false;
		}

		void fireEvent(Widget* widget)
		{
			if (callback)
				callback(widget, JA_ONDRAW, nullptr);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONDRAW, nullptr);
		}

		void fireEvent(Widget* widget, void* data)
		{
			if (callback)
				callback(widget, JA_ONDRAW, data);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONDRAW, data);
		}

		~OnDrawListener()
		{

		}
	};

}