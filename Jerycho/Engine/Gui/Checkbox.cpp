#include "Checkbox.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/ResourceManager.hpp"
#include "../Gui/Margin.hpp"

#include <iostream>
#include <string>

using namespace ja;

Checkbox::Checkbox() : ImageButton(0,nullptr)
{
	widgetType = JA_CHECKBOX;
	align = (uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER;
}

Checkbox::Checkbox(uint32_t id, Widget* parent) : ImageButton(id,parent)
{
	widgetType = JA_CHECKBOX;
	align = (uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER;
}


void Checkbox::draw(int32_t parentX, int32_t parentY)
{
	float myX = (float)(parentX + x);
	float myY = (float)(parentY + y);

	DisplayManager::setColorUb(r,g,b,a);
	if(a != 255)
	{
		DisplayManager::enableAlphaBlending();
		DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, true);
		DisplayManager::disableAlphaBlending();
	}
	else
	{
		DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, true);
	}

	float checkBoxX = myX + imageOffsetX;
	float checkBoxY = myY + imageOffsetY;
	float checkBoxW = checkBoxX + checkBoxWidth;
	float checkBoxH = checkBoxY + checkBoxHeight;

	DisplayManager::setColorUb(255,255,255,255);
	DisplayManager::drawRectangle(checkBoxX, checkBoxY, checkBoxW, checkBoxH, true);

	if(isCheckedValue)
	{
		DisplayManager::setColorUb(0,0,0,255);
		DisplayManager::setLineWidth(1.0f);
		DisplayManager::drawLine(checkBoxX, checkBoxY, checkBoxW, checkBoxH);
		DisplayManager::drawLine(checkBoxX, checkBoxH, checkBoxW, checkBoxY);
	}

	if(this->isBorderRendered())
	{
		DisplayManager::setLineWidth(1.0f);
		DisplayManager::setColorUb(borderR,borderG,borderB,borderA);
		DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, false);
	}
}

void Checkbox::updateImagePosition()
{
	uint32_t iflags = align & 4294967288;
	if (iflags != (uint32_t)Margin::NORMAL)
	{
		switch(iflags)
		{
		case (uint32_t)Margin::TOP:
			imageOffsetY = height - checkBoxHeight;	
			break;
		case (uint32_t)Margin::DOWN:
			imageOffsetY = 0;
			break;
		case (uint32_t)Margin::MIDDLE:
			imageOffsetY = (height / 2) - (checkBoxHeight / 2);
			break;
		}
	}
	else
	{
		imageOffsetY = (height / 2) - (checkBoxHeight / 2);
	}

	iflags = align & 7;

	switch(iflags)
	{
	case (uint32_t)Margin::LEFT:
		imageOffsetX = 0;
		break;
	case (uint32_t)Margin::CENTER:
		imageOffsetX = (width / 2) - (checkBoxWidth / 2);
		break;
	case (uint32_t)Margin::RIGHT:
		imageOffsetX  = width - checkBoxWidth;
		break;
	default:
		imageOffsetX = (width / 2) - (checkBoxWidth / 2);
		break;
	}
}

void Checkbox::update()
{      
	onMouseOver.processListener(this);
	onMouseLeave.processListener(this);
	if(onClick.processListener(this))
	{
		setCheck(!isCheckedValue);

		UIManager::getSingleton()->setSelected(this);
	}
	onChange.processListener(this);
	onSelect.processListener(this);
	onDeselect.processListener(this);
}

Checkbox::~Checkbox()
{

}

void Checkbox::setAlign( unsigned int marginStyle )
{
	align = marginStyle;
}

void Checkbox::setCheck( bool state )
{
	isCheckedValue = state;
	onChange.setChange(&isCheckedValue);
}

void Checkbox::setCheckWithoutEvent(bool state)
{
	isCheckedValue = state;
}

bool Checkbox::isChecked()
{
	return isCheckedValue;
}