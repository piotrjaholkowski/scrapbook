#pragma once

#include "ImageButton.hpp"
#include "OnClickListener.hpp"

namespace ja
{

	class Hider : public ImageButton
	{
	protected:
		std::vector<Widget*> widgets;
		Texture2d* hideTexture;
		Texture2d* showTexture;
		bool isHiding;

	public:
		Hider(uint32_t id, Widget* parent);

		void addWidget(Widget* widget);

		virtual void update();
		void setShowImage(Texture2d* showImage);
		void setHideImage(Texture2d* hideImage);
		void showAll();
		void hideAll();
		virtual ~Hider();
	};

}