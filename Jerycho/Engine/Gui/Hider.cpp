#include "Hider.hpp"
#include "../Manager/DisplayManager.hpp"

#include <iostream>
#include <string>

namespace ja
{

Hider::Hider(uint32_t id, Widget* parent) : ImageButton(id,parent)
{
	this->widgetType = JA_HIDER;
	isHiding = false;
	hideTexture = nullptr;
	showTexture = nullptr;
	setFont("default.ttf",10);
}

void Hider::update()
{      	
	ImageButton::update();

	if(onClick.wasClicked)
	{
		isHiding = !isHiding;

		if(isHiding)
			hideAll();
		else
			showAll();
	}
}

Hider::~Hider()
{

}

void Hider::addWidget( Widget* widget )
{
	widgets.push_back(widget);

	if(isHiding)
	{
		widget->setRender(false);
		widget->setActive(false);
	}
	else
	{
		widget->setRender(true);
		widget->setActive(true);
	}
}

void Hider::showAll()
{
	std::vector<Widget*>::iterator it = widgets.begin();
	isHiding = false;
	texture = hideTexture;

	for(it; it != widgets.end(); ++it)
	{
		(*it)->setRender(true);
		(*it)->setActive(true);
	}
}

void Hider::hideAll()
{
	std::vector<Widget*>::iterator it = widgets.begin();
	isHiding = true;
	texture = showTexture;
	

	for(it; it != widgets.end(); ++it)
	{
		(*it)->setRender(false);
		(*it)->setActive(false);
	}
}

void Hider::setShowImage( Texture2d* showImage )
{
	this->showTexture = showImage;

	if(isHiding)
	{
		setImageTexture(showTexture);
	}
}

void Hider::setHideImage( Texture2d* hideImage )
{
	this->hideTexture = hideImage;

	if(!isHiding)
	{
		setImageTexture(hideTexture);
	}
}

}