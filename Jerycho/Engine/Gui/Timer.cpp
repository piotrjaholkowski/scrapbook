#include "Timer.hpp"


#include <iostream>
#include <string>

namespace ja
{ 

Timer::Timer(uint32_t id, Widget* parent) : Widget(JA_BUTTON,id,parent)
{
	setRender(false);
	setActive(true);
	//setUpdate(true);
	setSelectable(false);

}

void Timer::draw(int parentX, int parentY)
{
	
}

void Timer::setTimeInterval(double timeInterval)
{
	onTimer.timeInterval = timeInterval;
}

void Timer::setAutoTimer(bool state)
{
	onTimer.autoTimer = state;
}

void Timer::setTimer(bool state)
{
	onTimer.setActive(state);
}

double Timer::getElapsedTime()
{
	return onTimer.elapsedTime;
}

void Timer::restartTimer()
{
	onTimer.elapsedTime = 0;
	onTimer.setActive(true);
}

void Timer::update()
{      
	onTimer.processListener(this);
}

Timer::~Timer()
{

}

}
