#pragma once

#include "../Gui/Widget.hpp"
#include "../Gui/ButtonGroup.hpp"

namespace ja
{

	class WidgetDef
	{
	public:
		uint32_t   widgetId;
		int32_t    positionX;
		int32_t    positionY;
		int32_t    width;
		int32_t    height;
		ja::string text;

		uint8_t  r, g, b, a;
		uint8_t  borderR, borderG, borderB, borderA;
		uint8_t  fontR, fontG, fontB, fontA;
		uint32_t align;

		bool isScriptGlobal;
		ja::string scriptGlobalName;

		bool isRendered;
		bool isActive;
		bool isBorderRendered;
		bool isChecked; // checkbox

		int32_t cellX;
		int32_t cellY;
		int32_t blocksX;
		int32_t blocksY;
		int32_t horizontalGap;
		int32_t verticalGap;

		bool    acceptNewLine;
		bool    acceptOnlyNumbers;
		bool    acceptDynamicBuffering;
		int32_t columnsNum;
		int32_t linesNum;

		bool   isHorizontal;
		double minRange, maxRange, currentValue;

		WidgetCorner scrollbarCorner;
		ScrollbarPolicy verticalPolicy;
		ScrollbarPolicy horizontalPolicy;
		int32_t scrollbarSize;
		bool    autoScrollHorizontal;
		bool    autoScrollVertical;
		bool    scrollToLeft;
		bool    scrollToTop;

		WidgetType widgetType;
		ja::string fontName;
		uint32_t   fontSize;
		Widget* parent;
		//jaButtonGroup* buttonGroup;
	};

}