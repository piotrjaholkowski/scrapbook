#pragma once

#include "../jaSetup.hpp"
#include "Widget.hpp"
#include "Label.hpp"

#include "OnDrawListener.hpp"
#include "OnUpdateListener.hpp"
#include <deque>

namespace ja
{

	class LogFadeData
	{
	public:
		float   fadeTime;
		uint8_t r, g, b;
		char    info[setup::gui::LOG_FADE_MAX_CHARS_NUM];
	};

	class LogFade : public Widget
	{
	protected:
		uint32_t align;
		Font*    font;
		OnUpdateListener onUpdate;
		OnDrawListener   onBeginDraw;
		OnDrawListener   onEndDraw;

		uint8_t  rS, gS, bS, aS;
		int8_t   shadowOffsetX;
		int8_t   shadowOffsetY;
		float    fadeTime;
		uint16_t verticalLineSpace;
	public:
		bool  isWorldMatrix;
		float worldScaleMultiplier;
		std::deque<LogFadeData> logFadeData;

	public:
		LogFade(uint32_t id, Widget* parent);
		void setFont(const char* font, uint32_t fontSize);
		void setFadeTime(float fadeTime);
		void setVerticalLineSpace(uint16_t verticalLineSpace);
	
		void setShadowColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setShadowOffset(float x, float y);
		void addInfo(uint8_t r, uint8_t g, uint8_t b, const char* info);
		void addInfoC(uint8_t r, uint8_t g, uint8_t b, const char* info, ...);
		void setAlign(uint32_t marginStyle);

		void setPosition(int32_t x, int32_t y);
		void update();
		void draw(int32_t parentX, int32_t parentY);

		template < class Y, class X >
		void setOnUpdateEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onUpdate.setActionEvent(pthis, pmethod);
			onUpdate.setActive(true);
		}

		void setOnUpdateEvent(ScriptDelegate* scriptDelegate)
		{
			onUpdate.setActionEvent(scriptDelegate);
			onUpdate.setActive(true);
		}

		template < class Y, class X >
		void setOnBeginDrawEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onBeginDraw.setActionEvent(pthis, pmethod);
			onBeginDraw.setActive(true);
		}

		void setOnBeginDrawEvent(ScriptDelegate* scriptDelegate)
		{
			onBeginDraw.setActionEvent(scriptDelegate);
			onBeginDraw.setActive(true);
		}

		template < class Y, class X >
		void setOnEndDrawEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onEndDraw.setActionEvent(pthis, pmethod);
			onEndDraw.setActive(true);
		}

		void setOnEndDrawEvent(ScriptDelegate* scriptDelegate)
		{
			onEndDraw.setActionEvent(scriptDelegate);
			onEndDraw.setActive(true);
		}
	};

}