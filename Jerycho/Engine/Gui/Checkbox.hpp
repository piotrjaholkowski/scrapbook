#pragma once

#include "ImageButton.hpp"
//#include "jaLabel.h"
#include "OnClickListener.hpp"
#include "OnMouseOverListener.hpp"
#include "OnMouseLeaveListener.hpp"
#include "OnSelectListener.hpp"
#include "OnDeselectListener.hpp"
#include "OnChangeListener.hpp"
#include "../Graphics/Texture2d.hpp"
#include <string>

namespace ja
{

	class Checkbox : public ImageButton
	{
	protected:
		uint32_t widgetStyle;
		uint32_t align;
		bool isCheckedValue;

		static const int32_t checkBoxWidth = 17;
		static const int32_t checkBoxHeight = 17;

		OnChangeListener onChange;

		void updateImagePosition();
	public:
		Checkbox();
		Checkbox(uint32_t id, Widget* parent);
		bool isSelected();
		void setCheck(bool state);
		void setCheckWithoutEvent(bool state);
		bool isChecked();

		void setAlign(uint32_t marginStyle);

		virtual void update();
		virtual void draw(int32_t parentX, int32_t parentY);
		~Checkbox();

		template < class Y, class X >
		void setOnChangeEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onChange.setActionEvent(pthis, pmethod);
			onChange.setActive(true);
		}

		void setOnChangeEvent(ScriptDelegate* scriptDelegate)
		{
			onChange.setActionEvent(scriptDelegate);
			onChange.setActive(true);
		}
	};

}
