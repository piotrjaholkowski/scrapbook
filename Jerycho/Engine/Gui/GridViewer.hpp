#pragma once

#include "Widget.hpp"
#include "Button.hpp"
#include "Scrollbar.hpp"
#include "Layout.hpp"
#include "OnClickListener.hpp"
#include "OnMouseOverListener.hpp"
#include "OnMouseLeaveListener.hpp"
#include "OnSelectListener.hpp"
#include "OnDeselectListener.hpp"
#include "OnDropListener.hpp"
#include "OnDrawListener.hpp"
#include <string>

namespace ja
{

	class GridViewer : public Layout
	{
	protected:
		int32_t blocksX, blocksY;
		int32_t blocksNum;
		uint8_t r, g, b, a;
		uint8_t borderR, borderG, borderB, borderA;

		Scrollbar scrollbar;
		Button**  gridButtons;
		std::vector<void*>* gridList;

	public:
		OnDrawListener onDraw;

	public:
		GridViewer(uint32_t id, int32_t blocksX, int32_t blocksY, Widget* parent);

		void setColor(uint8_t r, uint8_t g, uint8_t b);
		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setCellColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setAlpha(uint8_t alpha);

		void setActivateClickByKey(int32_t key);
		void update();
		void draw(int32_t parentX, int32_t parentY);

		void updateCollisionMask();
		void updateChildrenCollisionMask();

		void updateSize();
		void setGridList(std::vector<void*>* gridList);
		void setScrollbarHorizontal();
		void setScrollbarVertical();
		void setScrollbarColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

		~GridViewer();

		template < class Y, class X >
		void setOnClickEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			for (int32_t i = 0; i < blocksNum; i++)
			{
				gridButtons[i]->onClick.setActionEvent(pthis, pmethod);
				gridButtons[i]->onClick.setActive(true);
			}
		}

		void setOnClickEvent(ScriptDelegate* scriptDelegate)
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onClick.setActionEvent(scriptDelegate);
				gridButtons[i]->onClick.setActive(true);
			}
		}

		template < class Y, class X >
		void setOnDropEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onDrop.setActionEvent(pthis, pmethod);
				gridButtons[i]->onDrop.setActive(true);
			}
		}

		void setOnDropEvent(ScriptDelegate* scriptDelegate)
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onDrop.setActionEvent(scriptDelegate);
				gridButtons[i]->onDrop.setActive(true);
			}
		}

		template < class Y, class X >
		void setOnMouseOverEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onMouseOver.setActionEvent(pthis, pmethod);
				gridButtons[i]->onMouseOver.setActive(true);
			}
		}

		void setOnMouseOverEvent(ScriptDelegate* scriptDelegate)
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onMouseOver.setActionEvent(scriptDelegate);
				gridButtons[i]->onMouseOver.setActive(true);
			}
		}

		template < class Y, class X >
		void setOnMouseLeaveEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onMouseLeave.setActionEvent(pthis, pmethod);
				gridButtons[i]->onMouseLeave.setActive(true);
			}
		}

		void setOnMouseLeaveEvent(ScriptDelegate* scriptDelegate)
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onMouseLeave.setActionEvent(scriptDelegate);
				gridButtons[i]->onMouseLeave.setActive(true);
			}
		}

		template < class Y, class X >
		void setOnSelectEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onSelect.setActionEvent(pthis, pmethod);
				gridButtons[i]->onSelect.setActive(true);
			}
		}

		void setOnSelectEvent(ScriptDelegate* scriptDelegate)
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onSelect.setActionEvent(scriptDelegate);
				gridButtons[i]->onSelect.setActive(true);
			}
		}

		template < class Y, class X >
		void setOnDeselectEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onDeselect.setActionEvent(pthis, pmethod);
				gridButtons[i]->onDeselect.setActive(true);
			}
		}

		void setOnDeselectEvent(ScriptDelegate* scriptDelegate)
		{
			for (int32_t i = 0; i < blocksNum; ++i)
			{
				gridButtons[i]->onDeselect.setActionEvent(scriptDelegate);
				gridButtons[i]->onDeselect.setActive(true);
			}
		}

		template < class Y, class X >
		void setOnDrawEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onDraw.setActionEvent(pthis, pmethod);
			onDraw.setActive(true);
		}

		void setOnDrawEvent(ScriptDelegate* scriptDelegate)
		{
			onDraw.setActionEvent(scriptDelegate);
			onDraw.setActive(true);
		}
	};

}