#pragma once

#include "Widget.hpp"
#include "Layout.hpp"

namespace ja
{

	class GridLayout : public Layout
	{
	private:
		int32_t  blocksX, blocksY;
		int32_t  blocksNum;
		int32_t  blockSizeX, blockSizeY;
		int32_t  horizontalGap, verticalGap;
		int32_t  tempHorizontalGap, tempVerticalGap;
		Widget** blocks;
		uint8_t  r, g, b, a;

		void setChildrenPosition(Widget* widget, int32_t x, int32_t y);
	public:
		GridLayout(uint32_t id, Widget* parent, int32_t blocksX, int32_t blocksY, int32_t horizontalGap, int32_t verticalGap);
		~GridLayout();

		void setSize(int32_t width, int32_t height);
		Widget* getWidget(uint32_t widgetId);
		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void draw(int32_t parentX, int32_t parentY);
		void update();

		void addWidget(Widget* widget, int32_t x, int32_t y);

		void updateCollisionMask();
		void updateChildrenCollisionMask();
	};

}
