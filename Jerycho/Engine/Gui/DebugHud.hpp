#pragma once

#include <map>
#include <string>
#include "Interface.hpp"
#include "Label.hpp"

namespace ja
{

	typedef enum {
		jaLeftUpperCorner = 0,
		jaRightUpperCorner = 1,
		jaLeftLowerCorner = 2,
		jaRightLowerCorner = 3
	} ScreenPosition;

	struct DebugHudField {
		ScreenPosition hudFieldPosition;
		ja::string fieldValue;

		DebugHudField()
		{

		}

		DebugHudField(ScreenPosition hudFieldPosition, ja::string fieldValue)
		{
			this->hudFieldPosition = hudFieldPosition;
			this->fieldValue = fieldValue;
		}
	};

	class DebugHud : public Interface{
	private:
		Label* leftLower;
		Label* rightLower;
		Label* leftUpper;
		Label* rightUpper;
		std::map<uint32_t, DebugHudField> fields;
	public:
		DebugHud();
		DebugHud(uint32_t id, Widget* parent);
		void setFont(const char* fontName, uint32_t fontSize, ScreenPosition fieldID, uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void addField(ScreenPosition position, uint32_t fieldId, ja::string fieldValue);
		void setFieldValue(uint32_t fieldId, const char* fieldValue);
		void appendFieldValue(uint32_t fieldId, const char* fieldValue);
		void setFieldValuePrintC(uint32_t fieldId, const char* fieldValue, ...);
		void appendFieldValuePrintC(uint32_t fieldId, const char* fieldValue, ...);

		//void tem(unsigned int qwe,const char* format,... );
		virtual void update();
		virtual void draw();
		virtual ~DebugHud();
	};

}