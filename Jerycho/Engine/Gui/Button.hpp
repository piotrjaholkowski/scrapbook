#pragma once

#include "Widget.hpp"
#include "Label.hpp"
#include "OnClickListener.hpp"
#include "OnMouseOverListener.hpp"
#include "OnMouseLeaveListener.hpp"
#include "OnSelectListener.hpp"
#include "OnDeselectListener.hpp"
#include "OnDropListener.hpp"
#include <string>

#include <stdint.h>

namespace ja
{

	class Button : public Widget
	{
	protected:
		uint8_t  r, g, b, a;
		uint8_t  borderR, borderG, borderB, borderA;
		int32_t  textOffsetX, textOffsetY;
		uint32_t textAlign;
		Label    label;

		void updateTextPosition();
	public:
		OnSelectListener     onSelect;
		OnClickListener      onClick;
		OnMouseOverListener  onMouseOver;
		OnMouseLeaveListener onMouseLeave;
		OnDeselectListener   onDeselect;
		OnDropListener       onDrop;
		void*                vUserData;
	public:
		Button(uint32_t id, Widget* parent);
		void       setFont(const char* font, uint32_t fontSize);
		ja::Label* getLabel();
		void       adjustToText(int32_t additionalWidth, int32_t height);
		void       setText(const char* text);
		void       setTextValue(const char* fieldValue, ...);
		void       setColor(uint8_t r, uint8_t g, uint8_t b);
		void       setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void       setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void       setAlpha(uint8_t alpha);
		void       setFontColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

		virtual void setPosition(int32_t x, int32_t y);
		void setTextOffset(int32_t xOffset, int32_t yOffset);
		void getTextOffset(int32_t& xOffset, int32_t& yOffset);
		void setActivateClickByKey(int32_t key);

		virtual void update();
		virtual void updateSize();
		virtual void draw(int32_t parentX, int32_t parentY);

		template < class Y, class X >
		void setOnClickEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onClick.setActionEvent(pthis, pmethod);
			onClick.setActive(true);
		}

		void setOnClickEvent(ScriptDelegate* scriptDelegate)
		{
			onClick.setActionEvent(scriptDelegate);
			onClick.setActive(true);
		}

		template < class Y, class X >
		void setOnDropEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onDrop.setActionEvent(pthis, pmethod);
			onDrop.setActive(true);
		}

		void setOnDropEvent(ScriptDelegate* scriptDelegate)
		{
			onDrop.setActionEvent(scriptDelegate);
			onDrop.setActive(true);
		}

		template < class Y, class X >
		void setOnMouseOverEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onMouseOver.setActionEvent(pthis, pmethod);
			onMouseOver.setActive(true);
		}

		void setOnMouseOverEvent(ScriptDelegate* scriptDelegate)
		{
			onMouseOver.setActionEvent(scriptDelegate);
			onMouseOver.setActive(true);
		}

		template < class Y, class X >
		void setOnMouseLeaveEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onMouseLeave.setActionEvent(pthis, pmethod);
			onMouseLeave.setActive(true);
		}

		void setOnMouseLeaveEvent(ScriptDelegate* scriptDelegate)
		{
			onMouseLeave.setActionEvent(scriptDelegate);
			onMouseLeave.setActive(true);
		}

		template < class Y, class X >
		void setOnSelectEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onSelect.setActionEvent(pthis, pmethod);
			onSelect.setActive(true);
		}

		void setOnSelectEvent(ScriptDelegate* scriptDelegate)
		{
			onSelect.setActionEvent(scriptDelegate);
			onSelect.setActive(true);
		}

		template < class Y, class X >
		void setOnDeselectEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onDeselect.setActionEvent(pthis, pmethod);
			onDeselect.setActive(true);
		}

		void setOnDeselectEvent(ScriptDelegate* scriptDelegate)
		{
			onDeselect.setActionEvent(scriptDelegate);
			onDeselect.setActive(true);
		}

	};

}