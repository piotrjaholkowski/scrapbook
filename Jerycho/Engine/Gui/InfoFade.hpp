#pragma once

#include "../jaSetup.hpp"
#include "Widget.hpp"
#include "Label.hpp"

#include "OnDrawListener.hpp"
#include "OnUpdateListener.hpp"
#include <list>

namespace ja
{

	class InfoFadeData
	{
	public:
		float    x;
		float    y;
		float    fadeTime;
		float    fadeTimeLength;
		uint16_t infoId;
		uint8_t  r, g, b;
		char     info[ja::setup::gui::INFO_FADE_MAX_CHARS_NUM + 1];
	};

	class InfoFade : public Widget
	{
	protected:
		uint32_t align;
		Font*    font;
		OnUpdateListener onUpdate;
		OnDrawListener   onBeginDraw;
		OnDrawListener   onEndDraw;

		uint8_t rS, gS, bS, aS;
		int8_t  shadowOffsetX;
		int8_t  shadowOffsetY;
	public:
		bool  isWorldMatrix;
		float worldScaleMultiplier;
		std::list<InfoFadeData> infoFadeData;

	public:
		InfoFade(uint32_t id, Widget* parent);
		void setFont(const char* font, uint32_t fontSize);
		void setShadowColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setShadowOffset(float x, float y);
		void addInfo(uint16_t infoId, float x, float y, uint8_t r, uint8_t g, uint8_t b, float fadeTime, const char* info);
		void addInfoC(uint16_t infoId, float x, float y, uint8_t r, uint8_t g, uint8_t b, float fadeTime, const char* info, ...);
		void setAlign(uint32_t marginStyle);

		void setPosition(int32_t x, int32_t y);
		void update();
		void draw(int32_t parentX, int32_t parentY);

		template < class Y, class X >
		void setOnUpdateEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onUpdate.setActionEvent(pthis, pmethod);
			onUpdate.setActive(true);
		}

		void setOnUpdateEvent(ScriptDelegate* scriptDelegate)
		{
			onUpdate.setActionEvent(scriptDelegate);
			onUpdate.setActive(true);
		}

		template < class Y, class X >
		void setOnDrawEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onDraw.setActionEvent(pthis, pmethod);
			onDraw.setActive(true);
		}

		template < class Y, class X >
		void setOnBeginDrawEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onBeginDraw.setActionEvent(pthis, pmethod);
			onBeginDraw.setActive(true);
		}

		void setOnBeginDrawEvent(ScriptDelegate* scriptDelegate)
		{
			onBeginDraw.setActionEvent(scriptDelegate);
			onBeginDraw.setActive(true);
		}

		template < class Y, class X >
		void setOnEndDrawEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onEndDraw.setActionEvent(pthis, pmethod);
			onEndDraw.setActive(true);
		}

		void setOnEndDrawEvent(ScriptDelegate* scriptDelegate)
		{
			onEndDraw.setActionEvent(scriptDelegate);
			onEndDraw.setActive(true);
		}
	};

}