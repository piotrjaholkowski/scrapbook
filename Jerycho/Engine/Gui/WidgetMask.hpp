#pragma once

#include <stdint.h>

class WidgetMask
{
public:
	int32_t minX,minY; //min
	int32_t maxX,maxY; //max
	bool    canIntersect;

	bool intersect(int32_t x, int32_t y)
	{
		if (maxX < x) return false;
		if (minX > x) return false;
		if (maxY < y) return false;
		if (minY > y) return false;
		return true;
	}

	bool isPointInside(int32_t x, int32_t y) const
	{
		if (x < this->minX)
			return false;
		if (x > this->maxX)
			return false;
		if (y < this->minY)
			return false;
		if (y > this->maxY)
			return false;

		return true;
	}

	bool makeIntersection(WidgetMask& parentMask) 
	{
		if(canIntersect)
		{
			//X
			if(maxX < parentMask.minX)
			{
				canIntersect = false;
				return false;
			}
			else
			{
				if(minX > parentMask.maxX)
				{
					canIntersect = false;
					return false;
				}
				else
				{
					if(minX < parentMask.minX)
						minX = parentMask.minX;

					if(maxX > parentMask.maxX)
						maxX = parentMask.maxX;

					//Y
					if(maxY < parentMask.minY)
					{
						canIntersect = false;
						return false;
					}
					else
					{
						if(minY > parentMask.maxY)
						{
							canIntersect = false;
							return false;
						}
						else
						{
							if(minY < parentMask.minY)
								minY = parentMask.minY;

							if(maxY > parentMask.maxY)
								maxY = parentMask.maxY;

							//Y end
						}
					}
				}
			}
		}
		else
		{
			return false;
		}

		return true;
	}

	void setMask(int32_t minX, int32_t minY, int32_t maxX, int32_t maxY)
	{
		canIntersect = true;
		this->minX = minX;
		this->minY = minY;
		this->maxX = maxX;
		this->maxY = maxY;

		if(minX >= maxX)
			canIntersect = false;
		if(minY >= maxY)
			canIntersect = false;
	}

};
