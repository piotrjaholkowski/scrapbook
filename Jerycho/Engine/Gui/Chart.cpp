#include "Chart.hpp"
#include "../Manager/DisplayManager.hpp"

#include <iostream>
#include <string>

using namespace ja;

Chart::Chart(uint32_t id, Widget* parent) : Widget(JA_CHART,id,parent)
{
	r = g = b = 0;
	a = 255;
	//textOffsetX = 0;
	//textOffsetY = 0;
	setRender(true);
	setActive(true);
	//setUpdate(true);
	//setSelectable(true);
	setSelectable(false);
	//onClick.setActive(true);
}

//void jaChart::draw(int parentX, int parentY, int parentWidth = INT_MAX, int parentHeight = INT_MAX)
void Chart::draw(int32_t parentX, int32_t parentY)
{
	float myX = parentX + x;
	float myY = parentY + y;

	DisplayManager::setColorUb(r,g,b,a);

	if(a!=255)
	{
		DisplayManager::enableAlphaBlending();
		DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, false);
		DisplayManager::disableAlphaBlending();
	}
	else
	{
		DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, false);
	}

	float drawXOffset = (float)width / (float)chartValuesNum;

	for(uint32_t i=1; i<chartValuesNum; ++i)
	{
		DisplayManager::drawLine(myX, myY + (height * chartValues[i - 1]), myX + drawXOffset, myY + (height * chartValues[i]));
		myX += drawXOffset;
	}

	

	//label.draw(myX + textOffsetX, myY + textOffsetY);
}

void Chart::setColor(uint8_t r, uint8_t g, uint8_t b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}

void Chart::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

void Chart::setPosition(int32_t x, int32_t y )
{
	Widget::setPosition(x, y);
	//updateTextPosition();
}

void Chart::setSize(int32_t width, int32_t height)
{
	Widget::setSize(width, height);
	//updateTextPosition();
}

void Chart::update()
{   
	/*
	onMouseOver.processListener(this);
	onMouseLeave.processListener(this);
	if(onClick.processListener(this))
	{
		jaUIManager::getSingleton()->setSelected(this);
	}
	onSelect.processListener(this);
	onDeselect.processListener(this);
	*/
}

Chart::~Chart()
{

}

void Chart::setChartValues( float* valuesPointer, uint32_t valuesNum )
{
	this->chartValues = valuesPointer;
	this->chartValuesNum = valuesNum;
}