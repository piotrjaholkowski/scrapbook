#include "RadioButton.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/ResourceManager.hpp"

#include "Margin.hpp"

#include <iostream>
#include <string>

namespace ja
{

RadioButton::RadioButton() : Checkbox(0,nullptr)
{
	widgetType = JA_RADIO_BUTTON;
	align = (uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER;
	group = nullptr;
}

RadioButton::RadioButton(unsigned int id, Widget* parent) : Checkbox(id,parent)
{
	widgetType = JA_RADIO_BUTTON;
	align = (uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER;
	group = nullptr;
}


void RadioButton::draw(int32_t parentX, int32_t parentY)
{
	float myX = (float)(parentX + x);
	float myY = (float)(parentY + y);

	if(parent == nullptr)
	{
		myX = (float)(x);
		myY = (float)(y);
	}

	if(!collisionMask.canIntersect) // scissors region invalidate whole rendering area
		return;

	glEnable(GL_SCISSOR_TEST);
	glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);
		DisplayManager::setColorUb(r,g,b,a);
		if(a != 255)
		{
			DisplayManager::enableAlphaBlending();
			DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, true);
			DisplayManager::disableAlphaBlending();
		}
		else
		{
			DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, true);
		}

		float checkBoxX = myX + imageOffsetX;
		float checkBoxY = myY + imageOffsetY;
		float checkBoxW = checkBoxX + checkBoxWidth;
		float checkBoxH = checkBoxY + checkBoxHeight;

		DisplayManager::setColorUb(255,255,255,255);
		DisplayManager::drawRectangle(checkBoxX, checkBoxY, checkBoxW, checkBoxH, true);

		if(isCheckedValue)
		{
			DisplayManager::setColorUb(0,0,0,255);
			DisplayManager::drawRectangle(checkBoxX, checkBoxY, checkBoxW, checkBoxH, true);
		}

		if(this->isBorderRendered())
		{
			DisplayManager::setColorUb(borderR,borderG,borderB,borderA);
			DisplayManager::drawRectangle(myX + 1, myY + 1, myX + width - 1, myY + height - 1, false);
		}
	glDisable(GL_SCISSOR_TEST);
}

void RadioButton::update()
{      
	onMouseOver.processListener(this);
	onMouseLeave.processListener(this);
	if(onClick.processListener(this))
	{
		if(!isCheckedValue)
			setCheck(!isCheckedValue);

		UIManager::getSingleton()->setSelected(this);
	}
	onChange.processListener(this);
	onSelect.processListener(this);
	onDeselect.processListener(this);
}

RadioButton::~RadioButton()
{
	if(group != nullptr)
		group->removeButton(this);
}

bool RadioButton::isRadioButton()
{
	return true;
}

void RadioButton::setButtonGroup( Widget* widget )
{
	group = (ButtonGroup*)(widget);
}

}