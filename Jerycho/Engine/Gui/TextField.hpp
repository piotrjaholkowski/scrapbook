#pragma once

#include "Widget.hpp"
#include "Label.hpp"
#include "OnClickListener.hpp"
#include "OnMouseOverListener.hpp"
#include "OnMouseLeaveListener.hpp"
#include "OnSelectListener.hpp"
#include "OnDeselectListener.hpp"

#include <string>
#include <sstream>
#include <vector>

namespace ja
{

	class TextField : public Widget
	{
	protected:
		Label   label;
		int32_t columnsNum;

		int32_t  letterHeight;
		int32_t  cursorWidth;
		uint32_t currentColumn;

		ja::string textBuffer;
		bool acceptOnlyNumbers;
		bool allowDynamicBuffering;

		uint8_t r, g, b, a;
		uint8_t borderR, borderG, borderB, borderA;
		int32_t textOffsetX, textOffsetY;
		OnSelectListener     onSelect;
		OnDeselectListener   onDeselect;
		OnClickListener      onClick;
		OnMouseOverListener  onMouseOver;
		OnMouseLeaveListener onMouseLeave;

		void updateTextPosition();
	public:
		TextField(uint32_t id, Widget* parent);

		void setFont(const char* fontName, uint32_t fontSize);
		void setText(const char* text);
		void setFieldValue(const char* fieldValue, ...);

		//filter
		void setAcceptOnlyNumbers(bool state)
		{
			acceptOnlyNumbers = state;
		}

		void setAllowDynamicBuffering(bool state)
		{
			allowDynamicBuffering = state;
		}
		//filter

		bool isUsingUnicode();
		ja::string getText();
		const char* getTextPtr();
		bool getAsDouble(double* val);
		bool getAsFloat(float* val);
		bool getAsInt32(int32_t* val);
		bool getAsInt16(int16_t* val);
		bool getAsUInt16(uint16_t* val);
		bool getAsUInt8(uint8_t* val);
		bool getAsUInt32(uint32_t* val);
		bool getAsCustom(const char* query, void* custom);
		bool getAsInt8(int8_t* val);

		void clearText();
		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setAlpha(uint8_t a);
		void setFontColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		virtual void setPosition(int32_t x, int32_t y);

		void processText(const char* text);

		virtual void setSize(int32_t width, int32_t height);

		void    setTextSize(int32_t columnsNum, int32_t fieldHeight = 0);
		void    getCursorPosition(float& cursorX, float& cursorY);
		void    setCursorPosition(int32_t posX, int32_t posY);
		int32_t getCurrentLineNumber();

		void setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

		virtual void update();
		virtual void updateSize();
		virtual void draw(int32_t parentX, int32_t parentY);

		template < class Y, class X >
		void setOnClickEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onClick.setActionEvent(pthis, pmethod);
			onClick.setActive(true);
		}

		void setOnClickEvent(ScriptDelegate* scriptDelegate)
		{
			onClick.setActionEvent(scriptDelegate);
			onClick.setActive(true);
		}

		template < class Y, class X >
		void setOnMouseOverEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onMouseOver.setActionEvent(pthis, pmethod);
			onMouseOver.setActive(true);
		}

		void setOnMouseOverEvent(ScriptDelegate* scriptDelegate)
		{
			onMouseOver.setActionEvent(scriptDelegate);
			onMouseOver.setActive(true);
		}

		template < class Y, class X >
		void setOnMouseLeaveEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onMouseLeave.setActionEvent(pthis, pmethod);
			onMouseLeave.setActive(true);
		}

		void setOnMouseLeaveEvent(ScriptDelegate* scriptDelegate)
		{
			onMouseLeave.setActionEvent(scriptDelegate);
			onMouseLeave.setActive(true);
		}

		template < class Y, class X >
		void setOnSelectEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onSelect.setActionEvent(pthis, pmethod);
			onSelect.setActive(true);
		}

		void setOnSelectEvent(ScriptDelegate* scriptDelegate)
		{
			onSelect.setActionEvent(scriptDelegate);
			onSelect.setActive(true);
		}

		template < class Y, class X >
		void setOnDeselectEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onDeselect.setActionEvent(pthis, pmethod);
			onDeselect.setActive(true);
		}

		void setOnDeselectEvent(ScriptDelegate* scriptDelegate)
		{
			onDeselect.setActionEvent(scriptDelegate);
			onDeselect.setActive(true);
		}
	};

}