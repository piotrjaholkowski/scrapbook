#include "Button.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Utility/Exception.hpp"
#include "../Gui/Margin.hpp"

#include <iostream>
#include <string>

namespace ja
{

Button::Button(uint32_t id, Widget* parent) : Widget(JA_BUTTON,id,parent)
{
	r = g = b = 0;
	a = 255;
	textOffsetX = 0;
	textOffsetY = 0;
	textAlign = (uint32_t)Margin::CENTER | (uint32_t)Margin::MIDDLE;
	setRender(true);
	setActive(true);
	setSelectable(true);
	onClick.setActive(true);

	label.setParent(this);
	label.setAlign((uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE);
	setFont("default.ttf",10);
}

void Button::setFont(const char* fontName, uint32_t fontSize)
{
	label.setFont(fontName,fontSize);
	updateTextPosition();
}

Label* Button::getLabel()
{
	return &label;
}

void Button::setText(const char* text)
{
	label.setText(text);
	updateTextPosition();
}

void Button::setTextValue(const char* fieldValue,...)
{
	char temp[256];
	va_list	args;

	if (fieldValue == nullptr) *temp=0;
	else {
		va_start(args, fieldValue);
#pragma warning(disable : 4996)
		vsprintf(temp, fieldValue, args);
#pragma warning(default : 4996)
		va_end(args);	
	}

	setText(temp);
}

void Button::draw(int32_t parentX, int32_t parentY)
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;
	
	if(parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	if(!collisionMask.canIntersect) // scissors region invalidate whole rendering area
		return;

	glEnable(GL_SCISSOR_TEST);
	glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);
		DisplayManager::setColorUb(r,g,b,a);
		if(a!=255)
		{
			DisplayManager::enableAlphaBlending();
			DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
			DisplayManager::disableAlphaBlending();
		}
		else
		{
			DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
		}

		if(this->isBorderRendered())
		{
			DisplayManager::setLineWidth(1.0f);
			DisplayManager::setColorUb(borderR,borderG,borderB,borderA);
			DisplayManager::drawRectangle(myX + 1, myY + 1, myX + width - 1, myY + height - 1, false);
		}

		label.draw(myX + textOffsetX, myY + textOffsetY); 
	glDisable(GL_SCISSOR_TEST);
}

void Button::setFontColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	label.setColor(r,g,b,a);
}

void Button::updateTextPosition()
{
	int32_t width, height;
	float   textX, textY;
	Font*   font = label.getFont();

	if(font == nullptr)
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI, "You need to set font before updateTextPosition");
	}
	font->getTextSize(width,height,label.getText());

	uint32_t verticalFlags   = textAlign & 4294967288;
	uint32_t horizontalFlags = textAlign & 7;
	uint32_t labelAlign = 0;

	switch (verticalFlags)
	{
	case (uint32_t)Margin::TOP:
		labelAlign = (uint32_t)Margin::TOP;
		break;
	case (uint32_t)Margin::DOWN:
		labelAlign = (uint32_t)Margin::DOWN;
		break;
	case (uint32_t)Margin::MIDDLE:
		labelAlign = (uint32_t)Margin::MIDDLE;
		textY = (this->height / 2);
		break;
	}

	switch (horizontalFlags)
	{
	case (uint32_t)Margin::LEFT:
		labelAlign |= (uint32_t)Margin::LEFT;
		break;
	case (uint32_t)Margin::CENTER:
		labelAlign |= (uint32_t)Margin::CENTER;
		textX = (this->width / 2);
		break;
	case (uint32_t)Margin::RIGHT:
		labelAlign |= (uint32_t)Margin::RIGHT;
		break;
	}

	//textX = (this->width/2) - ((float)width/2);
	//textY = (this->height/2);
	label.setAlign(labelAlign);
	label.setPosition(textX, textY);
}

void Button::setColor(uint8_t r, uint8_t g, uint8_t b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}

void Button::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

void Button::setAlpha(uint8_t alpha)
{
	a = alpha;
}

void Button::setPosition(int32_t x, int32_t y )
{
	Widget::setPosition(x, y);
	updateTextPosition();
}

void Button::setTextOffset(int32_t textOffsetX, int32_t textOffsetY)
{
	this->textOffsetX = textOffsetX;
	this->textOffsetY = textOffsetY;
}

void Button::getTextOffset(int32_t& textOffsetX, int32_t& textOffsetY)
{
	this->textOffsetX = textOffsetX;
	this->textOffsetY = textOffsetY;
}

void Button::update()
{      
	onMouseOver.processListener(this);
	onMouseLeave.processListener(this);
	onClick.processListener(this);
	//if(onClick.processListener(this))
	//{
	//	UIManager::getSingleton()->setSelected(this);
	//}
	onDrop.processListener(this);
	onSelect.processListener(this);
	onDeselect.processListener(this);
}

void Button::setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	borderR = r;
	borderG = g;
	borderB = b;
	borderA = a;
}

void Button::updateSize()
{
	updateTextPosition();
}

void Button::setActivateClickByKey( int32_t key )
{
	onClick.activateByKey = key;
}

void Button::adjustToText(int32_t additionalWidth, int32_t height)
{
	int32_t textWidth, textHeight;
	label.getTextSize(textWidth, textHeight);
	setSize(textWidth + additionalWidth,height);
}

}