#pragma once

#include "Widget.hpp"
#include "Layout.hpp"

#include "OnMouseOverListener.hpp"

namespace ja
{
	class SlideWidget : public Layout
	{
	private:
		Widget* slideWidget;
		Widget* activateWidget;
		
		uint32_t  align;
		float     hideAfterTime;
		float     elapsedMouseLeaveTime;
		bool      isSlided;

	public:
		SlideWidget(uint32_t id, Widget* activateWidget, Widget* slideWidget, Widget* parent);
		~SlideWidget();

		void draw(int32_t parentX, int32_t parentY);
		void update();

		void setAlign(uint32_t marginStyle);

		void setPosition(int32_t x, int32_t y);
		void setSize(int32_t width, int32_t height);

		void forcePosition(int32_t x, int32_t y);
		void forceSize(int32_t width, int32_t height);

		void updateCollisionMask();
		void updateChildrenCollisionMask();

		//void setSlideWidget(Widget* widget);
		//void setActivateWidget(Widget* widget);
	};
}