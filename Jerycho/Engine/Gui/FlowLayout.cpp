#include "FlowLayout.hpp"
#include "../Manager/UIManager.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Utility/Exception.hpp"
#include "../Gui/Margin.hpp"

namespace ja
{ 

FlowLayout::FlowLayout(uint32_t id, Widget* parent, uint32_t align, int32_t horizontalGap = 0, int32_t verticalGap = 0) : Layout(JA_FLOW_LAYOUT,id,parent)
{
	setRender(true);
	setActive(true);
	setAsLayout(true);

	this->align = align;
	this->verticalGap = verticalGap;
	this->horizontalGap = horizontalGap;
}

FlowLayout::~FlowLayout()
{
	uint32_t widgetsNum = widgets.size();
	
	for(uint32_t i=0; i < widgetsNum; ++i)
	{
		Layout::removeReference(widgets[i]);
	}

	widgets.clear();
}


void FlowLayout::addWidget(Widget* widget)
{
	if(widget->isLayoutChild())
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI, "Widget is already added in diffrent layout");
	}

	/*if(widget->getReferenceNumber())
	{
		jaWidget* widgetParent = widget->getParent();
		if(widgetParent != NULL)
		{
			widgetParent->
			if(!widgetParent->isRadioButton())
			throw JA_EXCEPTIONR(JA_GUI_EXCEPTION,"Widget is already added to diffrent interface");
		}
	}*/

	widgets.push_back(widget);
	Layout::increaseReference(widget);
	widget->setParent(this); //it sets layout child flag
	
	setSize(width,height);	
}

void FlowLayout::update()
{
	if(isActive())
	{
		uint32_t widgetsNum = widgets.size();
		for(uint32_t i=0; i<widgetsNum; ++i)
		{
			if(widgets[i]->isActive())
			{
				widgets[i]->update();
			}	
		}
	}
}

void FlowLayout::draw(int32_t parentX, int32_t parentY)
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;

	if(parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	if(isRendered())
	{
		DisplayManager::setColorUb(r,g,b,a);
		if(a != 255)
		{
			DisplayManager::enableAlphaBlending();
			DisplayManager::drawRectangle(collisionMask.minX,collisionMask.minY,collisionMask.maxX,collisionMask.maxY,true);
			DisplayManager::disableAlphaBlending();
		}
		else
		{
			DisplayManager::drawRectangle(collisionMask.minX,collisionMask.minY,collisionMask.maxX,collisionMask.maxY,true);
		}

		uint32_t widgetsNum = widgets.size();
		for(uint32_t i=0; i<widgetsNum; ++i)
		{
			widgets[i]->draw(myX,myY);
		}
	}
}

void FlowLayout::setSize( int32_t width, int32_t height )
{
	Widget::setSize(width, height);
	uint32_t widgetsNum = widgets.size();

	uint32_t nextWidgetRow = 0;
	int32_t  rowWidth = 0;
	int32_t  rowHeight = 0;
	uint32_t startWidget = 0;

	int32_t lastRowYPosition = height;
	int32_t lastRowXPosition = 0;
	uint32_t xFlags = align & 4294967288;
	uint32_t yFlags = align & 7;

	while(nextWidgetRow < widgetsNum)
	{
		startWidget = nextWidgetRow;
		getRowSize(startWidget,nextWidgetRow,rowWidth,rowHeight);

		lastRowYPosition -= rowHeight;

		switch(yFlags)
		{
		case (uint32_t)Margin::LEFT:
			lastRowXPosition = 0;
			break;
		case (uint32_t)Margin::CENTER:
			lastRowXPosition = (width/2) - (rowWidth / 2);
			break;
		case (uint32_t)Margin::RIGHT:
			lastRowXPosition = width - rowWidth;
			break;
		default:
			lastRowXPosition = 0;
			break;
		}

		for(uint32_t i = startWidget; i<nextWidgetRow; ++i) // set position of row of a widgets
		{
			setChildrenPosition(widgets[i],xFlags,lastRowXPosition,lastRowYPosition,rowHeight);
		}		
		lastRowYPosition -= verticalGap;
	}
}

void FlowLayout::getRowSize(uint32_t startWidgetRow, uint32_t& nextWidgetRow, int32_t& rowWidth, int32_t& rowHeight)
{
	rowWidth = 0;
	rowHeight = widgets[startWidgetRow]->getHeight();

	uint32_t widgetsNum = widgets.size();
	uint32_t i;

	for(i=startWidgetRow; i<widgetsNum; ++i)
	{
		Widget* widget = widgets[i];
		rowWidth += widget->getWidth();

		if(rowWidth > width)
		{
			if(i==startWidgetRow)
			{
				++i;
			}
			break;
		}

		rowWidth += horizontalGap;

		int32_t rowTemp = widget->getHeight();
		if(rowHeight < rowTemp)
		{
			rowHeight = rowTemp;
		}
	}

	nextWidgetRow = i;
}

void FlowLayout::setChildrenPosition(Widget* widget, uint32_t xFlags, int32_t& lastRowXPosition, int32_t lastRowYPosition, uint32_t rowHeight)
{
	int32_t newPositionY = 0;

	if(xFlags != (uint32_t)Margin::NORMAL)
	{
		switch(xFlags)
		{
		case (uint32_t)Margin::TOP:
			newPositionY = lastRowYPosition + (rowHeight - widget->getHeight());
			break;
		case (uint32_t)Margin::DOWN:
			newPositionY = lastRowYPosition;
			break;
		case (uint32_t)Margin::MIDDLE:
			newPositionY = lastRowYPosition + ((rowHeight - widget->getHeight()) / 2);
			break;
		}
	}
	else
	{
		newPositionY = lastRowYPosition;
	}

	Layout::forcePosition(widget, lastRowXPosition, newPositionY);
	Layout::forceSize(widget, widget->getWidth(), widget->getHeight());

	widget->setLayoutBehaviour(this);
	lastRowXPosition += widget->getWidth() + horizontalGap;
}

Widget* FlowLayout::getWidget( uint32_t widgetId )
{
	uint32_t widgetsNum = widgets.size();
	for(uint32_t i=0; i<widgetsNum; ++i)
	{
		if(widgets[i]->getId() == widgetId)
			return widgets[i];
	}

	return nullptr;
}

void FlowLayout::updateCollisionMask()
{
	Widget::updateCollisionMask();
	updateChildrenCollisionMask();
}

void FlowLayout::updateChildrenCollisionMask()
{
	uint32_t widgetsNum = widgets.size();
	for(uint32_t i=0; i<widgetsNum; ++i)
	{
		widgets[i]->updateCollisionMask();
	}
}

void FlowLayout::setColor( uint8_t r, uint8_t g, uint8_t b, uint8_t a )
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

}