#include "GridLayout.hpp"
#include "../Manager/UIManager.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Utility/Exception.hpp"

namespace ja
{ 

GridLayout::GridLayout(uint32_t id, Widget* parent, int32_t blocksX, int32_t blocksY, int32_t horizontalGap = 0, int32_t verticalGap = 0) : Layout(JA_GRID_LAYOUT,id,parent)
{
	setRender(true);
	setActive(true);
	setAsLayout(true);

	if((blocksX < 1) || (blocksY < 1))
	{
		blocksX = 1;
		blocksY = 1;
	}

	this->verticalGap = verticalGap;
	this->horizontalGap = horizontalGap;

	this->blocksX = blocksX;
	this->blocksY = blocksY;
	this->blocksNum = blocksX * blocksY;

	blocks = new Widget*[blocksX * blocksY];

	for(int32_t i=0; i < blocksNum; ++i)
	{
		blocks[i] = nullptr;
	}
}

GridLayout::~GridLayout()
{
	for(int32_t i=0; i < blocksNum; ++i)
	{

		if(blocks[i] != nullptr)
		{
			if(Layout::removeReference(blocks[i]))
			{
				blocks[i] = nullptr;
			}
		}
	}

	delete[] blocks;
}


void GridLayout::addWidget(Widget* widget, int32_t x, int32_t y )
{
	if(blocksX <= x)
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI, "Wrong grid slot position X");
	}

	if(blocksY <= y)
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI, "Wrong grid slot position Y");
	}

	if(widget->isLayoutChild())
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI, "Widget is already added in diffrent layout");
	}

	if(widget->getReferenceNumber())
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI, "Widget is already added to diffrent interface");
	}

	blocks[(y * blocksX) + x] = widget;

	Layout::increaseReference(widget);
	widget->setParent(this); //it sets layout child flag
	
	setChildrenPosition(widget,x,y);	
}

void GridLayout::update()
{
	if(isActive())
	{
		for(int32_t i=0; i<blocksNum; ++i)
		{
			if(blocks[i] != nullptr)
			{
				if(blocks[i]->isActive())
				{
					blocks[i]->update();
				}
			}	
		}
	}
}


void GridLayout::draw(int32_t parentX, int32_t parentY)
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;

	if(parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	if(isRendered())
	{
		DisplayManager::setColorUb(r,g,b,a);
		if(a != 255)
		{
			DisplayManager::enableAlphaBlending();
			DisplayManager::drawRectangle(collisionMask.minX,collisionMask.minY,collisionMask.maxX,collisionMask.maxY,true);
			DisplayManager::disableAlphaBlending();
		}
		else
		{
			DisplayManager::drawRectangle(collisionMask.minX,collisionMask.minY,collisionMask.maxX,collisionMask.maxY,true);
		}

		for(int i=0; i<blocksNum; ++i)
		{
			if(blocks[i] != nullptr)
			{
				blocks[i]->draw(myX,myY);
			}
		}
	}
}

void GridLayout::setSize( int32_t width, int32_t height )
{
	Widget::setSize(width, height);
	
	blockSizeX = width / blocksX;
	blockSizeY = height / blocksY;

	tempHorizontalGap = horizontalGap;
	tempVerticalGap = verticalGap;

	if((blockSizeX / 2) < horizontalGap)
		tempHorizontalGap = blockSizeX / 2;

	if((blockSizeY / 2) < verticalGap)
		tempVerticalGap = blockSizeY / 2;

	int x,y;

	for(int32_t i=0; i<blocksNum; ++i)
	{
		if(blocks[i] != nullptr)
		{
			y = i / blocksX;
			x = i % blocksX;

			setChildrenPosition(blocks[i],x,y);
		}
	}
}

void GridLayout::setChildrenPosition( Widget* widget, int32_t x, int32_t y)
{
	Layout::forcePosition(widget, x * blockSizeX + tempHorizontalGap, (height - blockSizeY) - (y * blockSizeY - tempVerticalGap));
	Layout::forceSize(widget, blockSizeX - (2 * tempHorizontalGap), blockSizeY - (2* tempVerticalGap));

	widget->setLayoutBehaviour(this);
}

Widget* GridLayout::getWidget( uint32_t widgetId )
{
	for(int32_t i=0; i<blocksNum; ++i)
	{
		if(blocks[i] != nullptr)
		{
			if(blocks[i]->getId() == widgetId)
				return blocks[i];
		}
	}

	return nullptr;
}

void GridLayout::updateCollisionMask()
{
	Widget::updateCollisionMask();
	updateChildrenCollisionMask();
}

void GridLayout::updateChildrenCollisionMask()
{
	int32_t positionX, positionY;
	int32_t childX, childY;
	Widget* widget;

	if(isLayoutChild())
	{
		Layout* layout = (Layout*)(parent);
		layout->getOriginPosition(positionX,positionY);
		positionX += x;
		positionY += y;
	}
	else
	{
		getPosition(positionX, positionY); //doesn't change position if on the scroll pane
	}

	for(int32_t i=0; i<blocksNum; ++i)
	{
		if(blocks[i] != nullptr)
		{
			widget = blocks[i];
			widget->getRelativePosition(childX,childY);
			childX += positionX;
			childY += positionY;

			Layout::setCollisionMask(widget, childX, childY, childX + widget->getWidth(), childY + widget->getHeight());  // adds parent intersection
		}
	}
}

void GridLayout::setColor( uint8_t r, uint8_t g, uint8_t b, uint8_t a )
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

}