#include "ImageButton.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/ResourceManager.hpp"

#include <iostream>
#include <string>

namespace ja
{

ImageButton::ImageButton() : Button(0,nullptr)
{
	widgetType = JA_IMAGE_BUTTON;
	imageOffsetX = 0;
	imageOffsetY = 0;
	imageOriginX = 0;
	imageOriginY = 0;
	texture = nullptr;
	setFont("default.ttf",10);
}

ImageButton::ImageButton(uint32_t id, Widget* parent) : Button(id,parent)
{
	widgetType = JA_IMAGE_BUTTON;
	imageOffsetX = 0;
	imageOffsetY = 0;
	imageOriginX = 0;
	imageOriginY = 0;
	texture = nullptr;
	setFont("default.ttf",10);
}

void ImageButton::setImage(const char* fileName)
{
	texture = ResourceManager::getSingleton()->getEternalTexture2d(fileName)->resource;
	halfWidth = texture->getWidth() / 2.0f;
	halfHeight = texture->getHeight() / 2.0f;
}

void ImageButton::setImageTexture( Texture2d* image )
{
	texture = image;
	
	if(texture != nullptr)
	{
		halfWidth = texture->getWidth() / 2.0f;
		halfHeight = texture->getHeight() / 2.0f;
	}
}

void ImageButton::setImageSize(uint32_t width, uint32_t height)
{
	halfWidth = width / 2.0f;
	halfHeight = height / 2.0f;
}

void ImageButton::getImageSize(uint32_t& width, uint32_t& height)
{
	width  = (uint32_t)(2.0f * halfWidth);
	height = (uint32_t)(2.0f * halfHeight);
}

void ImageButton::draw(int32_t parentX, int32_t parentY)
{
	float myX = (float)(parentX + x);
	float myY = (float)(parentY + y);

	if(parent == nullptr)
	{
		myX = (float)(x);
		myY = (float)(y);
	}

	if(!collisionMask.canIntersect) // scissors region invalidate whole rendering area
		return;

	glEnable(GL_SCISSOR_TEST);
	glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);
	DisplayManager::setColorUb(r,g,b,a);
	if(a!=255)
	{
		DisplayManager::enableAlphaBlending();
		DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
		DisplayManager::disableAlphaBlending();
	}
	else
	{
		DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
	}

	if(texture != nullptr)
	{
		DisplayManager::beginDrawingSprites();
		DisplayManager::drawSprite(texture,myX + imageOffsetX + imageOriginX, myY + imageOffsetY + imageOriginY, halfWidth, halfHeight, 0);
		DisplayManager::endDrawingSprites();
	}

	if(this->isBorderRendered())
	{
		DisplayManager::setColorUb(borderR,borderG,borderB,borderA);
		DisplayManager::drawRectangle(myX + 1, myY + 1, myX + width - 1, myY + height - 1, false);
	}

	label.draw( (int32_t)(myX) + textOffsetX, (int32_t)(myY) + textOffsetY);
	glDisable(GL_SCISSOR_TEST);
}

void ImageButton::updateImagePosition()
{
	imageOriginX = width / 2;
	imageOriginY = height / 2;
}

void ImageButton::setImageOffset(int32_t imageOffsetX, int32_t imageOffsetY)
{
	this->imageOffsetX = imageOffsetX;
	this->imageOffsetY = imageOffsetY;
}

void ImageButton::getImageOffset(int32_t& imageOffsetX, int32_t& imageOffsetY)
{
	imageOffsetX = this->imageOffsetX;
	imageOffsetY = this->imageOffsetY;
}

void ImageButton::updateSize()
{
	updateTextPosition();
	updateImagePosition();
}

}