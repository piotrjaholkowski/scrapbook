#pragma once

#include "Widget.hpp"

namespace ja
{

	class OnUpdateListener : public WidgetListener
	{
	public:

		OnUpdateListener() : WidgetListener(JA_ONUPDATE)
		{

		}

		bool processListener(Widget* widget)
		{
			if (isActive())
			{
				fireEvent(widget);
				return true;
			}

			return false;
		}

		void fireEvent(Widget* widget)
		{
			if (callback)
				callback(widget, JA_ONUPDATE, nullptr);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONUPDATE, nullptr);
		}

		~OnUpdateListener()
		{

		}
	};

}
