#include "Editbox.hpp"
#include "../Manager/DisplayManager.hpp"

#include <iostream>
#include <string>

namespace ja
{

Editbox::Editbox(uint32_t id, Widget* parent) : Widget(JA_EDITBOX,id,parent)
{
	textOffsetX = 0;
	textOffsetY = 0;
	setRender(true);
	setActive(true);

	setSelectable(true);
	onClick.setActive(true);
	onClick.activateByKey = 0;
	onSelect.setActive(true);
	onDeselect.setActive(true);

	///////////////////////////////////
	acceptNewLine = false;
	acceptOnlyNumbers = false;
	///////////////////////////////////
	allowDynamicBuffering = false;

	scrollX = true;
	scrollY = true;
	currentLine = 0;
	
	textLines.push_back(TextLine(&editboxFont));
	bufferedLinesNum = 1;
	setFont("default.ttf",10);
	//bufferedColumnsNum = 256;
}

void Editbox::setFont(const char* fontName, uint32_t fontSize)
{
	editboxFont.setFont(fontName,fontSize);
	int32_t size = textLines.size();
	Font* font = editboxFont.getFont();

	letterHeight = font->getHighestCharacterSize();
	
	cursorWidth  = font->getWidthestCharacterSize();

	for(int32_t i=0; i<size; ++i)
	{
		textLines[i].textLabel = editboxFont;
	}
}

void Editbox::setText(const char* text)
{
	//bufferedColumnsNum = 0;
	currentLine = 0;
	textLines.clear();
	textLines.push_back(TextLine(&editboxFont));

	processText(text);
}

void Editbox::setFieldValue(const char* fieldValue,...)
{
	char temp[256];
	va_list	args;

	if (fieldValue == nullptr) *temp=0;
	else {
		va_start(args, fieldValue);
	#pragma warning(disable : 4996)
		vsprintf(temp, fieldValue, args);
	#pragma warning(default : 4996)
		va_end(args);	
	}

	setText(temp);
}

#pragma warning(disable:4018)
void Editbox::processText(const char* text)
{
	if(!allowDynamicBuffering)
	{
		if(textLines[currentLine].textBuffer.size() == columnsNum)
		{
			if(bufferedLinesNum == 1)
				return;
		}
	}
	
	for(uint32_t i=0; ; i++)
	{
		if (text[i] == '\n')
		{
			if(acceptNewLine)
			{
				if(allowDynamicBuffering)
				{
					textLines[currentLine].textLabel.setText(textLines[currentLine].textBuffer.c_str());
					++currentLine;
					if (currentLine == bufferedLinesNum)
					{
						textLines.push_back(TextLine(&editboxFont));
						++bufferedLinesNum;
					}
					continue;
				}
				else
				{
					if(currentLine < (bufferedLinesNum - 1))
					{
						textLines.push_back(TextLine(&editboxFont));
						++currentLine;	
					}
				
					continue;
				}
			}
			else
			{
				continue;
			}
		}
	
		if(text[i] == '\0')
		{
			textLines[currentLine].textLabel.setText(textLines[currentLine].textBuffer.c_str());
			break;
		}

		if(textLines[currentLine].textBuffer.size() >= columnsNum)
		{
			bool isFreeSpace = false;
			for(int x=currentLine; x<textLines.size(); ++x)
			{
				if(textLines[x].textBuffer.size() < columnsNum)
				{
					isFreeSpace = true;
					break;
				}
			}

			if(isFreeSpace == false)
			{
				if((textLines.size() - 1) < bufferedLinesNum)
				{
					textLines.push_back(TextLine(&editboxFont));
					isFreeSpace = true;
				}
				// here put case when dynamic buffering is allowed
			}

			if(isFreeSpace)
			{
				int x = currentLine;
				char tempChar = text[i];
				while(1==1)
				{
					textLines[x].textBuffer.insert(textLines[x].currentColumn,1,tempChar); // insert letter
					textLines[x].currentColumn++;

					if(textLines[currentLine].currentColumn > columnsNum)
					{
						++currentLine;
					}

					tempChar = textLines[x].textBuffer[textLines[x].textBuffer.size() - 1]; // put last letter in buffer
					
					if(textLines[x].textBuffer.size() >= columnsNum)
					{
						textLines[x].textBuffer.erase(textLines[x].textBuffer.size() - 1,1);
						textLines[x].textLabel.setText(textLines[x].textBuffer.c_str());
					}
					else
					{
						textLines[x].textLabel.setText(textLines[x].textBuffer.c_str());
						break;
					}

					++x;
					if(x >= textLines.size())
					{
						if((textLines.size() - 1) < bufferedLinesNum)
							textLines.push_back(TextLine(&editboxFont));
						else
							break;
					}
					textLines[x].currentColumn = 0;
				}

				continue;
				
			}
			else
			{
				return;
			}
		}

		textLines[currentLine].textBuffer.insert(textLines[currentLine].currentColumn,1,text[i]);
		textLines[currentLine].currentColumn++;

		if(textLines[currentLine].textBuffer.size() == columnsNum)
		{
			if(allowDynamicBuffering)
			{
				textLines[currentLine].textLabel.setText(textLines[currentLine].textBuffer.c_str());
				++currentLine;
				if(currentLine == bufferedLinesNum)
				{
					textLines.push_back(TextLine(&editboxFont));
					++bufferedLinesNum;
				}
			}
			else
			{
				textLines[currentLine].textLabel.setText(textLines[currentLine].textBuffer.c_str());
				break;
			}
		}
	}
}
#pragma warning(default:4018)

ja::string Editbox::getText()
{
	ja::string text;

	int32_t size = textLines.size();
	for(int32_t i=0; i<size; ++i)
	{
		text.append(textLines[i].textBuffer);
	}

	return (ja::string&&)text;
}

#pragma warning(disable : 4018)
#pragma warning(disable : 4996)
void Editbox::setAsFloats(float* values, int32_t valuesNum)
{
	clearText();

	if(valuesNum > linesNum)
		valuesNum = linesNum;

	for(int32_t i=0; i<valuesNum; ++i)
	{
		if(textLines.size() < valuesNum)
			textLines.push_back(TextLine(&editboxFont));

		textLines[i].textLabel.setTextPrintC("%f",values[i]);
		textLines[i].textBuffer = textLines[i].textLabel.getTextCstr();
		textLines[i].currentColumn = 0;
	}

	currentLine = 0;
}

void Editbox::getAsFloats(float* values, int32_t& valuesNum, int32_t maxValues)
{
	uint32_t currentLineTemp = currentLine;
	valuesNum = 0;

	for(int32_t i=0; i<textLines.size(); ++i)
	{
		currentLine = i;
		if(getAsFloat(&values[i]))
		{
			++valuesNum;
			if(valuesNum == maxValues)
				break;

		}
	}

	currentLine = currentLineTemp;
}

void Editbox::setAsInts(int32_t* values, int32_t valuesNum)
{
	clearText();

	if(valuesNum > linesNum)
		valuesNum = linesNum;

	for(int32_t i=0; i<valuesNum; ++i)
	{
		if(textLines.size() < valuesNum)
			textLines.push_back(TextLine(&editboxFont));
 
		textLines[i].textLabel.setTextPrintC("%d",values[i]);
		textLines[i].textBuffer = textLines[i].textLabel.getTextCstr();
		textLines[i].currentColumn = 0;
	}

	currentLine = 0;
}

void Editbox::getAsInts(int32_t* values, int32_t& valuesNum, int32_t maxValues)
{
	uint32_t currentLineTemp = currentLine;
	valuesNum = 0;

	for(int32_t i=0; i<textLines.size(); ++i)
	{
		currentLine = i;
		if(getAsInt(&values[i]))
		{
			++valuesNum;
			if(valuesNum == maxValues)
				break;
		}
	}

	currentLine = currentLineTemp;
}

void Editbox::setAsShorts(short* values, int32_t valuesNum)
{
	clearText();

	if(valuesNum > linesNum)
		valuesNum = linesNum;

	for(int32_t i=0; i<valuesNum; ++i)
	{
		if(textLines.size() < valuesNum)
			textLines.push_back(TextLine(&editboxFont));

		textLines[i].textLabel.setTextPrintC("%hd",values[i]);
		textLines[i].textBuffer = textLines[i].textLabel.getTextCstr();
		textLines[i].currentColumn = 0;
	}

	currentLine = 0;
}

void Editbox::getAsShorts(int16_t* values, int32_t& valuesNum, int32_t maxValues)
{
	uint32_t currentLineTemp = currentLine;
	valuesNum = 0;

	for(int32_t i=0; i<textLines.size(); ++i)
	{
		currentLine = i;
		if(getAsShort(&values[i]))
		{
			++valuesNum;
			if(valuesNum == maxValues)
				break;
		}
	}

	currentLine = currentLineTemp;
}

void Editbox::setAsChars(char* values, int32_t valuesNum)
{
	clearText();

	if(valuesNum > linesNum)
		valuesNum = linesNum;

	for(int32_t i=0; i<valuesNum; ++i)
	{
		if(textLines.size() < valuesNum)
			textLines.push_back(TextLine(&editboxFont));

		textLines[i].textLabel.setTextPrintC("%hhd",values[i]);
		textLines[i].textBuffer = textLines[i].textLabel.getTextCstr();
		textLines[i].currentColumn = 0;
	}

	currentLine = 0;
}

void Editbox::getAsChars(char* values, int32_t& valuesNum, int32_t maxValues)
{
	uint32_t currentLineTemp = currentLine;
	valuesNum = 0;

	for(int32_t i=0; i<textLines.size(); ++i)
	{
		currentLine = i;
		if(getAsChar(&values[i]))
		{
			++valuesNum;
			if(valuesNum == maxValues)
				break;
		}
	}

	currentLine = currentLineTemp;
}

bool Editbox::getAsDouble(double* val)
{
	const char* text = textLines[currentLine].textBuffer.c_str();
	int32_t     num  = sscanf(text,"%lf",val);
	if(num == 1)
		return true;
	return false;
}

bool Editbox::getAsFloat(float* val)
{
	const char* text = textLines[currentLine].textBuffer.c_str();
	int32_t      num = sscanf(text,"%f",val);
	if(num == 1)
		return true;
	return false;
}

bool Editbox::getAsInt(int32_t* val)
{
	const char* text = textLines[currentLine].textBuffer.c_str();
	int32_t     num = sscanf(text,"%d",val);
	if(num == 1)
		return true;
	return false;
}

bool Editbox::getAsShort(int16_t* val)
{
	const char* text = textLines[currentLine].textBuffer.c_str();
	int32_t      num = sscanf(text,"%hd",val);
	if(num == 1)
		return true;
	return false;
}

bool Editbox::getAsUnsignedInt( uint32_t* val )
{
	const char* text = textLines[currentLine].textBuffer.c_str();
	int32_t      num = sscanf(text,"%u",val);
	if(num == 1)
		return true;
	return false;
}

bool Editbox::getAsUnsignedShort(uint16_t* val)
{
	const char* text = textLines[currentLine].textBuffer.c_str();
	int32_t      num = sscanf(text,"%hu",val);
	if(num == 1)
		return true;
	return false;
}

bool Editbox::getAsUnsignedChar(unsigned char* val)
{
	int32_t valI;
	const char* text = textLines[currentLine].textBuffer.c_str();
	int32_t num = sscanf(text, "%u", &valI);
	if (num == 1)
	{
		*val = (unsigned char)(valI);
		return true;
	}
	return false;
}

bool Editbox::getAsCustom(const char* query, void* custom)
{
	const char* text = textLines[currentLine].textBuffer.c_str();
	int32_t      num = sscanf(text,query,custom);
	if(num == 1)
		return true;
	return false;
}

bool Editbox::getAsChar(char* val)
{
	int32_t valI;
	const char* text = textLines[currentLine].textBuffer.c_str();
	int32_t num = sscanf(text,"%d",&valI);
	if(num == 1)
	{
		*val = (char)(valI);
		return true;
	}
	return false;
}
#pragma warning(default : 4996)
#pragma warning(default : 4018)

void Editbox::clearText()
{

	textLines.clear();
	textLines.push_back(TextLine(&editboxFont));

	currentLine = 0;
}

void Editbox::draw(int32_t parentX, int32_t parentY)
{
	float myX = parentX + x;
	float myY = parentY + y;

	if(parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	if(!collisionMask.canIntersect) // scissors region invalidate whole rendering area
		return;

	glEnable(GL_SCISSOR_TEST);
	glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);
		int size = textLines.size();

		DisplayManager::setColorUb(r,g,b,a);
		if(a != 255)
		{
			DisplayManager::enableAlphaBlending();
			DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, true);
			DisplayManager::disableAlphaBlending();
		}
		else
		{
			DisplayManager::drawRectangle(myX, myY, myX + width, myY + height, true);
		}

		if(onSelect.lastSelected)
		{
			float cursorX,cursorY;
			getCursorPosition(cursorX,cursorY);

			cursorX += myX;
			cursorY += myY;

			DisplayManager::setColorUb(153,217,234,255);
			DisplayManager::drawRectangle(cursorX,cursorY,cursorX + cursorWidth,cursorY + letterHeight,true);
		}

		if(this->isBorderRendered())
		{
			DisplayManager::setColorUb(borderR,borderG,borderB,borderA);
			DisplayManager::drawRectangle(myX + 1, myY + 1, myX + width - 1, myY + height - 1, false);
		}

		//rendering text lines
		myY += height - this->letterHeight;
		for(int i=0; i<size; ++i)
		{
			textLines[i].textLabel.draw(myX + textOffsetX, myY + textOffsetY - (i * this->letterHeight));
		}
		//rendering text lines
	glDisable(GL_SCISSOR_TEST);
}

void Editbox::setFontColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	editboxFont.setColor(r,g,b,a);
	int32_t size = textLines.size();
	for(int32_t i=0; i<size; ++i)
	{
		textLines[i].textLabel.setColor(r,g,b,a);
	}
}

void Editbox::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

void Editbox::setBorderColor( uint8_t r, uint8_t g, uint8_t b, uint8_t a )
{
	this->borderR = r;
	this->borderG = g;
	this->borderB = b;
	this->borderA = a;
}

void Editbox::setAlpha(uint8_t a)
{
	this->a = a;
}

void Editbox::setPosition(int32_t x, int32_t y )
{
	Widget::setPosition(x, y);
}

///////////////////////////////////////////////

void Editbox::setHorizontalSrollbar(bool state)
{
	this->scrollX = true;
}

void Editbox::setVerticalScrollbar(bool state)
{
	this->scrollY = true;
}

void Editbox::setSize(int32_t width, int32_t height)
{
	ja::string text = getText(); 

	clearText();
	Widget::setSize(width, height);

	Font* font = editboxFont.getFont();
	
	textOffsetX = 0;
	textOffsetY = -font->getLowest(); 
	columnsNum = width / cursorWidth;
	linesNum = height / letterHeight;

	processText(text.c_str()); 
}

void Editbox::setTextSize(int32_t columnsNum, int32_t linesNum)
{
	ja::string text = getText(); // to doda�em

	clearText();
	this->columnsNum = columnsNum;
	this->linesNum = linesNum;

	// sets widget size, height
	Font* font = editboxFont.getFont();

	textOffsetX = 0;
	textOffsetY = -font->getLowest(); // �eby nie wyje�d�a�o
	Widget::setSize(cursorWidth * columnsNum, letterHeight * linesNum);
	bufferedLinesNum = linesNum;

	//clearText();
	processText(text.c_str()); // to doda�em
}

void Editbox::getCursorPosition( float& cursorX, float& cursorY )
{
	Font*   font          = editboxFont.getFont();
	int32_t currentColumn = textLines[currentLine].currentColumn;

	int32_t lineWidth;
	int32_t lineHeight;

	ja::string substring = textLines[currentLine].textBuffer.substr(0,currentColumn);
	font->getTextSize(lineWidth,lineHeight,substring);

	if(lineWidth == 0)
	{
		cursorX = 0.0f;
	}
	else
	{
		cursorX = (float)(lineWidth - cursorWidth);
	}

	cursorY = (float)(height - letterHeight - (currentLine * letterHeight));

}

#pragma warning(disable : 4018)
void Editbox::setCursorPosition(int32_t posX, int32_t posY)
{
	if(textLines.size() == 0)
		return;

	int32_t widgetX, widgetY;
	getPosition(widgetX,widgetY);

	posX -= widgetX;
	posY -= widgetY;
	posY = height - posY;

	if(posY < 0)
		posY = -posY;

	int32_t lineNum = posY / letterHeight;
	int32_t i;

	if((textLines.size() - 1) < lineNum)
	{
		lineNum = textLines.size() - 1;
	}
	
	/*for(i=lineNum; i >= 0; --i)
	{
		if(textLines[i].textBuffer.size() > 0)
			break;
	}

	if(i < 0)
		i = 0;*/
	i = lineNum;

	int32_t index;
	editboxFont.getFont()->getLetterIndex(textLines[i].textBuffer,posX,index);
	textLines[i].currentColumn = index;
	currentLine = i;
}
#pragma warning(default : 4018)

int32_t Editbox::getCurrentLineNumber()
{
	return currentLine;
}

////////////////////////////////////

void Editbox::update()
{ 
	if(onSelect.lastSelected) // odbieranie wci�ni�tych klawiszy
	{
		char keyBuffer[12];
		char ck = 0;

		if(InputManager::isKeyPressed(JA_LEFT))
		{
			if(textLines[currentLine].currentColumn > 0)
				--textLines[currentLine].currentColumn;
			else
			{
				if(currentLine != 0)
				{
					--currentLine;
					textLines[currentLine].currentColumn = textLines[currentLine].textBuffer.size();
				}
			}
		}

		if(InputManager::isKeyPressed(JA_RIGHT))
		{
			if(textLines[currentLine].currentColumn < textLines[currentLine].textBuffer.size())
				++textLines[currentLine].currentColumn;
			else
			{
				if(currentLine != (textLines.size() - 1))
				{
					++currentLine;
					textLines[currentLine].currentColumn = 0;
				}
			}
		}

		if(InputManager::isKeyPressed(JA_UP))
		{
			if(currentLine != 0)
			{
				if(textLines[currentLine].currentColumn < textLines[currentLine - 1].currentColumn)
					textLines[currentLine - 1].currentColumn = textLines[currentLine].currentColumn;
				--currentLine;
			}
		}

		if(InputManager::isKeyPressed(JA_DOWN))
		{
			if(currentLine != (textLines.size() - 1))
			{
				if(textLines[currentLine].currentColumn < textLines[currentLine + 1].currentColumn)
					textLines[currentLine + 1].currentColumn = textLines[currentLine].currentColumn;
				++currentLine;
			}
		}

		if(InputManager::isKeyPressed(JA_BACKSPACE))
		{
			if(textLines[currentLine].currentColumn > 0)
			{
				textLines[currentLine].textBuffer.erase(--textLines[currentLine].currentColumn,1);
				if(textLines[currentLine].textBuffer.size() != 0)
					textLines[currentLine].textLabel.setText(textLines[currentLine].textBuffer.c_str());
				else
					textLines[currentLine].textLabel.setText("");
			}
			else
			{
				if(currentLine != 0)
				{
					if(textLines[currentLine].textBuffer.size() == 0) // if line is empty delete
					{
						textLines.erase(textLines.begin() + currentLine);
					}
					else
					{
						//text tranfering
						int freeColumns = this->columnsNum - textLines[currentLine - 1].textBuffer.size();
						if(freeColumns == 0) // there is no space to transfering words transfer only one letter
						{
							textLines[currentLine - 1].textBuffer[textLines[currentLine].currentColumn] = textLines[currentLine].textBuffer[0];
							
							//updating
							if(textLines[currentLine].textBuffer.size() > 1)
							{
								textLines[currentLine].textBuffer.erase(0,1); // delete only first letter which was transfered
								textLines[currentLine].textLabel.setText(textLines[currentLine].textBuffer.c_str());
							}
							else
								textLines.erase(textLines.begin() + currentLine); // line is empty delete it
						}
						else
						{

						}
					}
					--currentLine;
					textLines[currentLine].currentColumn = textLines[currentLine].textBuffer.size();
				}
			}
		}

		if (InputManager::isKeyPressed(JA_RETURN))
		{
			processText("\n");
		}


		std::list<InputAction>* inputEvents = InputManager::getInputEvents();

		if(!inputEvents->empty())
		{
			std::list<InputAction>::iterator it;
		
			for(it = inputEvents->begin();it!=inputEvents->end();it++)
			{
				if(((*it).inputEvent == JA_KEYPRESSED) && ((*it).unicodeChar != 0))
				{
					if(acceptOnlyNumbers)
					{
						if((it->unicodeChar >= 48) && (it->unicodeChar <= 57))
						{
							//numbers 48 - 0 ; 57 - 9
						}
						else
						{
							//sign or point
							//45 -
							//46 .
							if((it->unicodeChar == 45) || (it->unicodeChar == 46))
							{

							}
							else
							{
								if(it->unicodeChar == 13)
								{
									keyBuffer[ck++] = '\n';
									continue;
								}
								continue;
							}
						}
					}

					if(it->unicodeChar == 13)
					{
						keyBuffer[ck++] = '\n';
						continue;
					}

					if(it->unicodeChar < 13)
						continue;

					keyBuffer[ck++] = (char)it->unicodeChar;
					if(ck == 12)
						break;
				}
				
			}
		}

		if(ck != 0)
		{
			keyBuffer[ck++] = '\0';
			processText(keyBuffer);
		}
		
	}
	
	onMouseOver.processListener(this);
	onMouseLeave.processListener(this);

	if(onClick.processListener(this)) // zrobi� to te� przy buttonie i imageButtonie
	{
		UIManager::getSingleton()->setSelected(this);

		int32_t mousePosX = InputManager::getMousePositionX();
		int32_t mousePosY = DisplayManager::getSingleton()->getResolutionHeight() - InputManager::getMousePositionY();
		setCursorPosition(mousePosX,mousePosY);
	}

	if(onSelect.processListener(this)) // trzeba ustawi� active i �eby nie wo�a�o callback jak null
	{
	
	}

	if(onDeselect.processListener(this)) 
	{
		
	}
	
}

Editbox::~Editbox()
{
	textLines.clear();
}

bool Editbox::isUsingUnicode()
{
	return true;
}

}
