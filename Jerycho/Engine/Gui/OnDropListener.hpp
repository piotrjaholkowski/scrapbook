#pragma once

#include "Widget.hpp"
#include "../Manager/InputManager.hpp"
#include "../Manager/UIManager.hpp"

namespace ja
{

	class OnDropListener : public WidgetListener
	{
	private:
		Widget* lastDragged;
	public:

		OnDropListener() : WidgetListener(JA_ONDROP), lastDragged(nullptr)
		{

		}

		bool processListener(Widget* widget)
		{
			if (isActive())
			{
				if (widget->isActive())
				{
					if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
					{
						if (widget->isMouseOver())
						{
							lastDragged = UIManager::getLastDragged();
							if ((widget == lastDragged) || (lastDragged == nullptr))
								return false;
							fireEvent(widget);
							return true;
						}
					}
				}
				return false;
			}
			return false;
		}

		void fireEvent(Widget* widget)
		{
			if (callback)
				callback(widget, JA_ONDROP, lastDragged);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONDROP, lastDragged);
		}

	};

}