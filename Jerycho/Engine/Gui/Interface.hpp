#pragma once

#include "Widget.hpp"
#include "OnUpdateListener.hpp"
#include <map>
#include <vector>
#include <string>

namespace ja
{

	class Interface : public Widget
	{
	protected:
		std::vector<Widget*> widgets;
		OnUpdateListener onUpdate;

	public:
		Interface(uint32_t id, Widget* parent);
		void addWidget(Widget* widget);
		Widget* getWidget(uint32_t id);
		void updateSize();
		virtual void draw();
		virtual void draw(int32_t parentX, int32_t parentY);

		void setPosition(int32_t x, int32_t y);
		void setSize(int32_t width, int32_t height);
		void getWidgets(std::vector<Widget*>& widgetList);
		void setTopWidget(Widget* topWidget);

		virtual void update();
		virtual ~Interface();

		template < class Y, class X >
		void setOnUpdateEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			onUpdate.setActionEvent(pthis, pmethod);
			onUpdate.setActive(true);
		}

		void setOnUpdateEvent(ScriptDelegate* scriptDelegate)
		{
			onUpdate.setActionEvent(scriptDelegate);
			onUpdate.setActive(true);
		}
	};

}