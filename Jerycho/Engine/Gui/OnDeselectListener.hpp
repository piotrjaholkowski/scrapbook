#pragma once

#include "Widget.hpp"
#include "../Manager/UIManager.hpp"

namespace ja
{

	class OnDeselectListener : public WidgetListener
	{
	public:
		bool lastSelected;

		OnDeselectListener() : WidgetListener(JA_ONDESELECT)
		{
			lastSelected = false;
		}

		bool processListener(Widget* widget)
		{
			if (widget->isActive())
			{
				if (isActive())
				{
					if (UIManager::getLastSelected() == widget)
					{
						if (InputManager::isKeyPressed(JA_TAB)) // if tab is pressed then deselect
						{
							UIManager::setUnselected();
							fireEvent(widget);
							lastSelected = false;
							return true;
						}
						lastSelected = true;
						return false;
					}
					else
					{
						if (lastSelected == true)
						{
							fireEvent(widget);
							lastSelected = false;
							return true;
						}
					}
				}
			}
			return false;
		}

		void fireEvent(Widget* widget)
		{
			if (callback)
				callback(widget, JA_ONDESELECT, nullptr);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONDESELECT, nullptr);
		}

		~OnDeselectListener()
		{

		}
	};

}
