#pragma once

#include "Button.hpp"
#include "../Graphics/Texture2d.hpp"
#include <string>

namespace ja
{

	class ImageButton : public Button
	{
	protected:
		uint32_t   widgetStyle;
		int32_t    imageOriginX, imageOriginY;
		int32_t    imageOffsetX, imageOffsetY;
		float      halfWidth, halfHeight;
		Texture2d* texture;
	public:
		ImageButton();
		ImageButton(uint32_t id, Widget* parent);
		void setImage(const char* fileName);
		void setImageTexture(Texture2d* image);
		void setImageSize(uint32_t width, uint32_t height);
		void getImageSize(uint32_t& width, uint32_t& height);
		virtual void updateImagePosition();

		void setImageOffset(int32_t imageOffsetX, int32_t imageOffsetY);
		void getImageOffset(int32_t& imageOffsetX, int32_t& imageOffsetY);
		virtual void updateSize();

		virtual void draw(int32_t parentX, int32_t parentY);
	};

}
