#pragma once

#include "Widget.hpp"
#include "../Manager/InputManager.hpp"
#include "../Manager/UIManager.hpp"

namespace ja
{

	class OnTimerListener : public WidgetListener
	{
	public:
		bool   autoTimer; // was mouse button pressed
		double timeInterval;
		double elapsedTime;

		OnTimerListener() : WidgetListener(JA_ONTIMER)
		{
			autoTimer    = false;
			timeInterval = 5;
			elapsedTime  = 0;
		}

		bool processListener(Widget* widget)
		{
			if (isActive())
				if (widget->isActive())
				{
					double deltaTime = (double)(UIManager::getSingleton()->getDeltaTime());

					elapsedTime += deltaTime;

					if (autoTimer)
					{
						if (elapsedTime > timeInterval)
						{
							fireEvent(widget);
							//elapsedTime = elapsedTime - timeInterval;
							elapsedTime -= (timeInterval * (int)(elapsedTime / timeInterval));
							return true;
						}
						return false;
					}
					else
					{
						if (elapsedTime > timeInterval)
						{
							fireEvent(widget);
							//elapsedTime = elapsedTime - timeInterval;
							elapsedTime -= (timeInterval * (int)(elapsedTime / timeInterval));
							setActive(false);
							return true;
						}
						return false;
					}

				}
			return false;
		}


		void fireEvent(Widget* widget)
		{
			if (callback)
				callback(widget, JA_ONTIMER, nullptr);

			if (scriptCallback)
				scriptCallback->run(widget, JA_ONTIMER, nullptr);
		}

		~OnTimerListener()
		{

		}
	};

}

