#pragma once

#include "Checkbox.hpp"
#include "ButtonGroup.hpp"
#include "OnChangeListener.hpp"
#include "../Graphics/Texture2d.hpp"
#include <string>

namespace ja
{

	class RadioButton : public Checkbox
	{
	protected:
		uint32_t widgetStyle;
		uint32_t align;
		//bool isCheckedValue;

		ButtonGroup* group;

	public:
		RadioButton();
		RadioButton(uint32_t id, Widget* parent);

		virtual void setButtonGroup(Widget* widget);

		virtual void update();
		virtual bool isRadioButton();
		void draw(int32_t parentX, int32_t parentY);
		~RadioButton();

	};

}