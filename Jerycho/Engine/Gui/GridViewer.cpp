#include "GridViewer.hpp"
#include "Layout.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Utility/Exception.hpp"

#include <iostream>
#include <string>

namespace ja
{

GridViewer::GridViewer(uint32_t id, int32_t blocksX, int32_t blocksY, Widget* parent) : Layout(JA_GRID_VIEWER, id, parent)
{
	r = g = b = 0;
	a = 255;
	setRender(true);
	setActive(true);
	setSelectable(true);
	
	this->blocksX = blocksX;
	this->blocksY = blocksY;

	blocksNum = blocksX * blocksY;
	gridButtons = new Button*[blocksNum];
	for (int32_t i = 0; i < blocksNum; ++i)
	{
		gridButtons[i] = new Button(i, this);
		gridButtons[i]->setRenderBorder(true);
		gridButtons[i]->setBorderColor(255, 255, 255, 255);
		gridButtons[i]->setColor(0, 0, 0, 0);
	}

	scrollbar.setVertical();
	scrollbar.setSize(10, 10);
	scrollbar.setParent(this);
	scrollbar.setValue(0.0f);
	scrollbar.setWindowRange((double)blocksNum);

	gridList = nullptr;
}

void GridViewer::draw(int32_t parentX, int32_t parentY)
{
	int32_t myX = parentX + x;
	int32_t myY = parentY + y;

	if(parent == nullptr)
	{
		myX = x;
		myY = y;
	}

	if(!collisionMask.canIntersect) // scissors region invalidate whole rendering area
		return;

	glEnable(GL_SCISSOR_TEST);
	glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);
	DisplayManager::setColorUb(r,g,b,a);
	if(a!=255)
	{
		DisplayManager::enableAlphaBlending();
		DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
		DisplayManager::disableAlphaBlending();
	}
	else
	{
		DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
	}

	if(this->isBorderRendered())
	{
		DisplayManager::setLineWidth(1.0f);
		DisplayManager::setColorUb(borderR,borderG,borderB,borderA);
		DisplayManager::drawRectangle(myX + 1, myY + 1, myX + width - 1, myY + height - 1, false);
	}
	glDisable(GL_SCISSOR_TEST);

	for (int32_t i = 0; i < blocksNum; ++i)
	{
		gridButtons[i]->draw(myX, myY);
	}

	if (onDraw.isActive() && (gridList != nullptr))
	{
		int32_t elementsNum = gridList->size();

		int32_t elementIt = floor(scrollbar.getValue());
		if (elementIt == elementsNum)
			--elementIt;

		const int32_t elementMul = elementIt / blocksNum;

		glEnable(GL_SCISSOR_TEST);
		for (int32_t i = 0; i < blocksNum; ++i)
		{
			const int32_t elementIdx = (elementMul * blocksNum) + i;

			if (elementIdx >= elementsNum)
				break;

			WidgetMask buttonMask = gridButtons[i]->getCollisionMask();
			glScissor(buttonMask.minX, buttonMask.minY, buttonMask.maxX - buttonMask.minX, buttonMask.maxY - buttonMask.minY);
			onDraw.fireEvent(gridButtons[i], gridList->at(elementIdx));
		}
		glDisable(GL_SCISSOR_TEST);
	}

	scrollbar.draw(myX, myY);
}

void GridViewer::setColor(uint8_t r, uint8_t g, uint8_t b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}

void GridViewer::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
}

void GridViewer::setAlpha(uint8_t alpha)
{
	a = alpha;
}

void GridViewer::update()
{      
	if (gridList != nullptr)
	{
		const int32_t elementsNum = (int32_t)(gridList->size());
		scrollbar.setRange(0.0, (double)(elementsNum));

		int32_t elementIt = floor(scrollbar.getValue());
		if (elementIt == elementsNum)
			--elementIt;

		const int32_t elementMul = elementIt / blocksNum;

		for (int32_t i = 0; i < blocksNum; ++i)
		{
			const int32_t elementIdx = (elementMul * blocksNum) + i;

			if (elementIdx >= elementsNum)
			{
				gridButtons[i]->vUserData = nullptr;
				continue;
			}
		
			gridButtons[i]->vUserData = gridList->at(elementIdx);
		}
	}

	for (int32_t i = 0; i < blocksNum; ++i)
	{
		gridButtons[i]->update();
	}

	scrollbar.update();
}

GridViewer::~GridViewer()
{
	for (int32_t i = 0; i < blocksNum; ++i)
	{
		if (gridButtons[i] != nullptr)
		{
			delete gridButtons[i];
		}
	}

	delete[] gridButtons;
}

void GridViewer::setBorderColor( uint8_t r, uint8_t g, uint8_t b, uint8_t a )
{
	borderR = r;
	borderG = g;
	borderB = b;
	borderA = a;
}

void GridViewer::setActivateClickByKey( int32_t key )
{
	for (int32_t i = 0; i < blocksNum; ++i)
	{
		gridButtons[i]->onClick.activateByKey = key;
	}
}

void GridViewer::updateCollisionMask()
{
	Widget::updateCollisionMask();
	updateChildrenCollisionMask();
}

void GridViewer::updateChildrenCollisionMask()
{
	int32_t positionX, positionY;
	int32_t childX, childY;
	Widget* widget;

	if (isLayoutChild())
	{
		Layout* layout = (Layout*)(parent);
		layout->getOriginPosition(positionX, positionY);
		positionX += x;
		positionY += y;
	}
	else
	{
		getPosition(positionX, positionY); //doesn't change position if on the scroll pane
	}

	int32_t blockWidth  = this->width / blocksX;
	int32_t blockHeight = this->height / blocksY;
	for (int32_t i = 0; i < blocksNum; ++i)
	{
		widget = gridButtons[i];
		widget->getRelativePosition(childX, childY);
		childX += positionX;
		childY += positionY;

		Layout::setCollisionMask(widget, childX, childY, childX + widget->width, childY + widget->height);  // adds parent intersection
	}

	scrollbar.getRelativePosition(childX, childY);
	childX += positionX;
	childY += positionY;
	Layout::setCollisionMask(&scrollbar, childX, childY, childX + scrollbar.width, childY + scrollbar.height);  // adds parent intersection
}

void GridViewer::updateSize()
{
	int32_t positionX, positionY;
	Widget* widget;

	if (isLayoutChild())
	{
		Layout* layout = (Layout*)(parent);
		layout->getOriginPosition(positionX, positionY);
		positionX += x;
		positionY += y;
	}
	else
	{
		getPosition(positionX, positionY); //doesn't change position if on the scroll pane
	}

	int32_t blockWidth  = this->width / blocksX;
	int32_t blockHeight = this->height / blocksY;
	int32_t childX, childY;
	
	if (scrollbar.isHorizontal())
	{
		scrollbar.y = 0;
		scrollbar.x = 0;
	
		Layout::forceSize(&scrollbar, this->width, 10);
		positionY += scrollbar.getHeight();

		scrollbar.getRelativePosition(childX, childY);
		childX += positionX;
		childY += positionY;
		Layout::setCollisionMask(&scrollbar, childX, childY, childX + scrollbar.width, childY + scrollbar.height);  // adds parent intersection
	}
	else
	{
		blockWidth = blockWidth - scrollbar.getWidth() / blocksX;
		scrollbar.x = blocksX * blockWidth;
		scrollbar.y = 0;
		Layout::forceSize(&scrollbar, 10, this->height);

		scrollbar.getRelativePosition(childX, childY);
		childX += positionX;
		childY += positionY;
		Layout::setCollisionMask(&scrollbar, childX, childY, childX + scrollbar.width, childY + scrollbar.height);  // adds parent intersection
	}

	int32_t blockPosX, blockPosY;
	for (int32_t i = 0; i < blocksNum; ++i)
	{
		widget = gridButtons[i];
		
		blockPosY = i / blocksX;
		blockPosX = i % blocksX;

		widget->x = blockPosX * blockWidth;
		widget->y = blockPosY * blockHeight;
		widget->width = blockWidth;
		widget->height = blockHeight;

		widget->getRelativePosition(childX, childY);
		childX += positionX;
		childY += positionY;

		Layout::setCollisionMask(widget, childX, childY, childX + widget->width, childY + widget->height);  // adds parent intersection
	}
}

void GridViewer::setGridList(std::vector<void*>* gridList)
{
	this->gridList = gridList;
}

void GridViewer::setScrollbarHorizontal()
{
	scrollbar.setHorizontal();
}

void GridViewer::setScrollbarVertical()
{
	scrollbar.setVertical();
}

void GridViewer::setScrollbarColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	scrollbar.setColor(r, g, b, a);
}

void GridViewer::setCellColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	for (int32_t i = 0; i < blocksNum; ++i)
	{
		gridButtons[i]->setColor(r, g, b, a);
	}
}

}