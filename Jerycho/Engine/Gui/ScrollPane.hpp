#pragma once

#include "Widget.hpp"
#include "Scrollbar.hpp"
#include "Layout.hpp"

namespace ja
{

	class ScrollPane : public Layout
	{
	private:
		std::vector<Widget*> widgets;

		bool autoScrollVertical;
		bool autoScrollHorizontal;
		bool scrollToLeft;
		bool scrollToTop;

		Scrollbar  scrollbarHorizontal;
		Scrollbar  scrollbarVertical;
		int32_t    scrollbarSize;

		ScrollbarPolicy horizontalPolicy; // implement
		ScrollbarPolicy verticalPolicy;
		WidgetCorner    scrollbarCorner;

		int32_t minX, minY, maxX, maxY;
		int32_t currentX, currentY;

		uint8_t r, g, b, a;
		uint8_t borderR, borderG, borderB, borderA;

		void setChildrenPosition(Widget* widget, int32_t x, int32_t y);

		void    updateScrollbarCollisionMask();
		void    updateScrollPanelPosition();
		void    updateScrollbars();
		void    getScrollPanelPosition(int32_t& x, int32_t& y);
		int32_t getHorizontalScrollbarSize();
		int32_t getVerticalScrollbarSize();
	public:
		ScrollPane(uint32_t id, Widget* parent);
		~ScrollPane();

		void clearWidgets();
		void setScrollbarSize(int32_t scrollbarSize);

		Widget* getWidget(uint32_t widgetId);

		void setColor(uint8_t r, uint8_t g, uint8_t b);
		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

		void draw(int32_t parentX, int32_t parentY);
		void update();

		void addWidget(Widget* widget);

		void setHorizontalScrollbarPolicy(ScrollbarPolicy scrollbarPolicy);
		void setVerticalScrollbarPolicy(ScrollbarPolicy scrollbarPolicy);

		void setCorner(WidgetCorner scrollbarCorner);

		void updateCollisionMask();
		void updateChildrenCollisionMask(); // optimize

		virtual void getOriginPosition(int32_t& x, int32_t& y);
		virtual void getAbsolutePosition(int32_t& x, int32_t& y);
		virtual void getTransformPosition(int32_t& x, int32_t& y);

		void setScrollX(int32_t scrollX);
		void setScrollY(int32_t scrollY);
		int32_t getPanelWidth(); // width without scrollbars
		int32_t getPanelHeight(); // height without scrollbars

		void onChangeVertical(Widget* sender, WidgetEvent action, void* data);
		void onChangeHorizontal(Widget* sender, WidgetEvent action, void* data);

		void setVerticalAutoScroll(bool state, bool scrollToTop);
		void setHorizontalAutoScroll(bool state, bool scrollToLeft);

		Scrollbar* getVerticalScrollbar();
		Scrollbar* getHorizontalScrollbar();
	};

}