#pragma once


#include "WidgetMask.hpp"
#include "../Utility/String.hpp"

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include <FastDelegate.h>

namespace ja
{

	enum ScrollbarPolicy
	{
		JA_SCROLLBAR_AS_NEEDED = 0,
		JA_SCROLLBAR_ALWAYS    = 1,
		JA_SCROLLBAR_NEVER     = 2
	};

	enum WidgetCorner
	{
		JA_RIGHT_LOWER_CORNER = 0,
		JA_RIGHT_UPPER_CORNER = 1,
		JA_LEFT_UPPER_CORNER  = 2,
		JA_LEFT_LOWER_CORNER  = 3
	};

	enum ComponentFlag
	{
		//JA_UPDATE = 1,
		JA_LAYOUT_CHILD = 1,
		JA_ACTIVE = 2,
		JA_DRAW = 4, // render or not
		JA_SELECTABLE = 8, // can be selected
		JA_DRAWBORDER = 16,
		JA_LAYOUT = 32,
		JA_MODAL = 64,
		JA_DRAGABLE = 128
	};

	/*
	enum jaWidgetState {
	JA_SELECTED = 2,
	JA_PRESSED = 4
	};*/

	enum WidgetType{
		JA_LABEL = 1,
		JA_INTERFACE = 2,
		JA_BUTTON = 3,
		JA_MENU = 4,
		JA_HUD = 5,
		JA_IMAGE_BUTTON = 6,
		JA_SWITCH_BUTTON = 7,
		JA_EDITBOX = 8,
		JA_SCROLLBAR = 9,
		JA_PROGRESS_BUTTON = 10,
		JA_TIMER = 11,
		JA_CHART = 12,
		JA_GRID_LAYOUT = 13,
		JA_CHECKBOX = 14,
		JA_BUTTON_GROUP = 15,
		JA_RADIO_BUTTON = 16,
		JA_SCROLL_PANE = 17,
		JA_MAGNET = 18,
		JA_HIDER = 19,
		JA_FLOW_LAYOUT = 20,
		JA_TEXTFIELD = 21,
		JA_BOX_LAYOUT = 22,
		JA_GRID_VIEWER = 23,
		JA_INFO_FADE = 24,
		JA_SLIDEBOX = 25,
		JA_SLIDE_WIDGET = 26,
		JA_SEQUENCE_EDITOR = 27,
		JA_LOG_FADE = 28
	};

	enum WidgetEvent
	{
		JA_ONCLICK = 0,
		JA_ONSELECT = 1,
		JA_ONDESELECT = 2,
		JA_ONMOUSEOVER = 3,
		JA_ONMOUSELEAVE = 4,
		JA_ONCHANGE = 5,
		JA_ONTIMER = 6,
		JA_ONUPDATE = 7,
		JA_ONDROP = 8,
		JA_ONDRAW = 9
	};

	class Widget;
	class WidgetListener;

	typedef fastdelegate::FastDelegate3<Widget*, WidgetEvent, void*> WidgetCallback;

	class Widget
	{
	protected:
		uint32_t   texBuffer;
		int32_t    widgetType;
		int32_t    componentFlag;
		uint32_t   id;
		WidgetMask collisionMask;
		Widget*    parent;
		uint16_t   zOrder;

		int8_t references; // number of interfaces the control is in
		bool   removeReference(); // decreases number of references and check if object can be deleted

		virtual void forcePosition(int32_t x, int32_t y);
		virtual void forceSize(int32_t width, int32_t height);
		void setLayoutChild(bool state);
		void setAsLayout(bool state);
	public:
		int32_t x, y; // relative x,y position
		int32_t width, height;
		bool    bUserData;

		Widget(WidgetType widgetType, uint32_t id, Widget* parent);
		virtual ~Widget();
		uint32_t getId();
		Widget*  getParent();

		WidgetMask getCollisionMask();
		bool isRendered();
		bool isActive();
		bool isSelectable();
		bool isLayout();
		bool isModal();
		bool isDragable();
		bool isLayoutChild();
		bool isBorderRendered();
		virtual bool isUsingUnicode();
		void setModal(bool state);
		void setDragable(bool state);
		void setRender(bool state);
		void setActive(bool state);
		void setSelectable(bool state);
		void setRenderBorder(bool state);

		virtual void draw(int32_t parentX, int32_t parentY);
		virtual void update();
		virtual void setTopWidget(Widget* topWidget);
		void actionEvent(Widget* sender, WidgetEvent action, void* data);
		virtual void setPosition(int32_t x, int32_t y);
		void getRelativePosition(int32_t& x, int32_t& y);
		void getPosition(int32_t& x, int32_t& y);
		virtual void getAbsolutePosition(int32_t& x, int32_t& y);
		virtual void getTransformPosition(int32_t& x, int32_t& y);
		WidgetType getWidgetType();
		virtual void setSize(int32_t width, int32_t height);
		int32_t getWidth();
		int32_t getHeight();
		bool    isMouseOver();

		virtual void updateCollisionMask();
		virtual void updateChildrenCollisionMask();

		int8_t getReferenceNumber();
		virtual bool isRadioButton();
		virtual void setButtonGroup(Widget* widget);
		virtual void setCheck(bool state);
		virtual bool isChecked();
		void setParent(Widget* parent);

		virtual void updateSize();
		//Rendering methods
		void createTexBuffer(); // called when widget uses (render to texture) to generate texture
		void deleteTexBuffer(); // deletes current tex buffer;
		void resizeTexBuffer(); // called when widget has texBuffer and changes size
		///////////////////
		virtual void setLayoutBehaviour(Widget* widget); // this method is called by layout after setting position of widget
		void setScriptGlobal(ja::string& globalName);
	private:
		void updateTexBuffer();
		///////////////////

		friend class Interface;
		friend class Layout;
		friend class UIManager;
		friend class Menu;
	};

	class ScriptDelegate
	{
	private:
		ja::string delegateName;
		int16_t    references;

		bool removeReference() // decreases number of references and check if object can be deleted
		{
			--references;

			if (references <= 0)
				return true;

			return false;
		}

	public:

		ScriptDelegate(ja::string delegateName)
		{
			this->delegateName = delegateName;
			references = 0;
		}

		void run(Widget* widget, WidgetEvent widgetEvent, void* data);

		friend class WidgetListener;
		friend class UIManager;
	};


	class WidgetListener
	{
	protected:
		WidgetEvent listenerType;
		fastdelegate::FastDelegate3<Widget*, WidgetEvent, void*> callback;
		ScriptDelegate* scriptCallback;

		uint32_t listenerFlag;
	public:
		WidgetListener(WidgetEvent listenerType);
		bool isActive();
		void setActive(bool state);

		template < class Y, class X >
		void setActionEvent(Y *pthis, void (X::*pmethod)(Widget*, WidgetEvent, void*))
		{
			callback.bind(pthis, pmethod);
		}

		void setActionEvent(ScriptDelegate* scriptCallback)
		{
			this->scriptCallback = scriptCallback;
			++scriptCallback->references;
		}

		virtual bool processListener(Widget* widget);
		virtual void fireEvent(Widget* widget);
		~WidgetListener();
	};

}