#include "ButtonGroup.hpp"

using namespace ja;

ButtonGroup::ButtonGroup( unsigned int id, Widget* parent ) : Widget(JA_BUTTON_GROUP,id,parent)
{
	setRender(false);
	setActive(true);
	checkedWidget = nullptr;
}

ButtonGroup::~ButtonGroup()
{
}

void ButtonGroup::addButton( Widget* widget )
{
	if(widget->isRadioButton())
	{
		widget->setButtonGroup(this);
		widgets.push_back(widget);

		if(widget->isChecked())
		{
			this->checkedWidget = widget;
		}

		++this->references;
	}
}

void ButtonGroup::removeButton( Widget* widget )
{
	std::vector<Widget*>::iterator it = widgets.begin();

	for(it; it != widgets.end(); ++it)
	{
		if(*it == widget) 
		{
			widgets.erase(it);			
			
			if(this->removeReference())
			{
				delete this;
			}
			break;
		}
	}
}

void ButtonGroup::update()
{
	std::vector<Widget*>::iterator it = widgets.begin();

	for(it; it != widgets.end(); ++it)
	{
		if((*it)->isChecked())
		{
			if(checkedWidget != *it)
			{
				if(checkedWidget != nullptr)
					checkedWidget->setCheck(false);
				checkedWidget = *it;
			}
		}
	}
}