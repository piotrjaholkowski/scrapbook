#include "SequenceEditor.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Utility/Exception.hpp"

#include <iostream>
#include <string>
#include <algorithm>

namespace ja
{

	SequenceEditor::SequenceEditor(uint32_t id, Widget* parent) : Layout(JA_SEQUENCE_EDITOR, id, parent)
	{
		r = g = b = 0;
		a = 255;
		setRender(true);
		setActive(true);
		setSelectable(true);

		scrollbar.setHorizontal();
		scrollbar.setSize(10, 10);
		scrollbar.setParent(this);
		scrollbar.setValue(0.0f);	

		this->editedMarker   = nullptr;
		this->selectedMarker = nullptr;
	}

	SequenceEditor::~SequenceEditor()
	{	
		clearMarkers();
	}

	void SequenceEditor::clearMarkers()
	{
		this->editedMarker   = nullptr;
		this->selectedMarker = nullptr;

		std::vector<SequenceMarker*>::iterator it = this->markers.begin();
		for (it; it != this->markers.end(); ++it)
		{
			delete (*it);
		}

		this->markers.clear();
	}

	SequenceMarker* SequenceEditor::createMarker()
	{
		SequenceMarker* marker = new SequenceMarker();
		markers.push_back(marker);

		return marker;
	}
	
	SequenceMarker* SequenceEditor::getSelectedMarker()
	{
		return this->selectedMarker;
	}

	std::vector<SequenceMarker*>& SequenceEditor::getMarkers()
	{
		return markers;
	}

	void SequenceEditor::draw(int32_t parentX, int32_t parentY)
	{
		int32_t myX = parentX + x;
		int32_t myY = parentY + y;

		if (parent == nullptr)
		{
			myX = x;
			myY = y;
		}

		if (!collisionMask.canIntersect) // scissors region invalidate whole rendering area
			return;

		glEnable(GL_SCISSOR_TEST);
		glScissor(collisionMask.minX, collisionMask.minY, collisionMask.maxX - collisionMask.minX, collisionMask.maxY - collisionMask.minY);
		DisplayManager::setColorUb(r, g, b, a);
		if (a != 255)
		{
			DisplayManager::enableAlphaBlending();
			DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
			DisplayManager::disableAlphaBlending();
		}
		else
		{
			DisplayManager::drawRectangle(collisionMask.minX, collisionMask.minY, collisionMask.maxX, collisionMask.maxY, true);
		}

		if (this->isBorderRendered())
		{
			DisplayManager::setLineWidth(1.0f);
			DisplayManager::setColorUb(borderR, borderG, borderB, borderA);
			DisplayManager::drawRectangle(myX + 1, myY + 1, myX + width - 1, myY + height - 1, false);
		}

		{
			const float fWidth = (float)this->width;

			std::vector<SequenceMarker*>::iterator it = this->markers.begin();
			for (it; it != this->markers.end(); ++it)
			{
				SequenceMarker* marker = (*it);
				const uint16_t  posX       = (uint16_t)(fWidth * marker->position);
				const uint16_t  markerPosX = myX + posX;

				DisplayManager::setColorUb(marker->r, marker->g, marker->b);
				
				DisplayManager::drawLine(markerPosX, myY, markerPosX, myY + height);

				if (this->selectedMarker == marker)
				{
					DisplayManager::setColorUb(selectedMarkerR, selectedMarkerG, selectedMarkerB);
				}

				if (marker->isMarkerTop)
				{
					const uint16_t markerTopPos = myY + height;
					const uint16_t markerSize = marker->markerSize;
					DisplayManager::drawRectangle(markerPosX - markerSize, markerTopPos - markerSize, markerPosX + markerSize, markerTopPos, true);
				}
				else
				{
					const uint16_t markerBottomPos = myY + scrollbar.getHeight();
					const uint16_t markerSize      = marker->markerSize;
					DisplayManager::drawRectangle(markerPosX - markerSize, markerBottomPos, markerPosX + markerSize, markerBottomPos + markerSize, true);
				}
			}
		}

		glDisable(GL_SCISSOR_TEST);

		scrollbar.draw(myX, myY);
	}

	void SequenceEditor::setColor(uint8_t r, uint8_t g, uint8_t b)
	{
		this->r = r;
		this->g = g;
		this->b = b;
	}

	void SequenceEditor::setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
	{
		this->r = r;
		this->g = g;
		this->b = b;
		this->a = a;
	}

	void SequenceEditor::setSelectedMarkerColor(uint8_t r, uint8_t g, uint8_t b)
	{
		this->selectedMarkerR = r;
		this->selectedMarkerG = g;
		this->selectedMarkerB = b;
	}

	void SequenceEditor::setMarkerMoveSensitivity(uint32_t sensitivity)
	{
		this->markerMoveSensitivity = sensitivity;
	}

	void SequenceEditor::setAlpha(uint8_t alpha)
	{
		a = alpha;
	}

	void SequenceEditor::setPosition(int32_t x, int32_t y)
	{
		Widget::setPosition(x, y);
	}

	void SequenceEditor::update()
	{
		if (isMouseOver())
		{
			if (true == InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
			{
				int32_t parentX;
				int32_t parentY;

				this->getPosition(parentX, parentY);

				parentY              += scrollbar.getHeight();
				int32_t parentHeight  = height - scrollbar.getHeight();

				int32_t mouseX, mouseY;
				mouseX = UIManager::mouseX;
				mouseY = UIManager::mouseY;

				{
					//Check if marker is selected
					std::vector<SequenceMarker*>::iterator it = this->markers.begin();

					for (it; it != this->markers.end(); ++it)
					{
						WidgetMask markerMask;
						(*it)->getMarkerMask(parentX, parentY, width, parentHeight, markerMask);

						markerMask.makeIntersection(this->collisionMask);

						if (markerMask.isPointInside(mouseX, mouseY))
						{
							this->editedStartPosition = mouseX;
							this->editedMarker        = (*it);
							this->selectedMarker      = (*it);
							break;
						}
					}
				}
			}
		}
		
		onMouseOver.processListener(this);
		
		if (nullptr != this->editedMarker)
		{
			int32_t mouseX, mouseY;
			mouseX = UIManager::mouseX;
			mouseY = UIManager::mouseY;

			if (InputManager::isMouseButtonPress((uint8_t)MouseButton::BUTTON_LEFT))
			{
				int32_t mouseX = UIManager::mouseX;

				int32_t positionDiff = abs(editedStartPosition - mouseX);
				
				if ( (positionDiff > this->markerMoveSensitivity) || (editedMarkerMoves) )
				{
					int32_t parentX;
					int32_t parentY;
					this->getPosition(parentX, parentY);

					int32_t deltaLength = mouseX - parentX;
					deltaLength = std::min(deltaLength, width);
					deltaLength = std::max(0, deltaLength);

					const float position = (float)deltaLength / (float)width;
					this->editedMarker->position = position;
					this->editedMarkerMoves = true;
				}
			}

			if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
			{
				this->editedMarkerMoves = false;
				this->editedMarker      = nullptr;
			}

			onChange.change       = (void*)this->editedMarker;
			onChange.stateChanged = true;
		}

		onChange.processListener(this);
		onMouseLeave.processListener(this);
		if (onClick.processListener(this))
		{
			UIManager::getSingleton()->setSelected(this);			
		}
		onSelect.processListener(this);
		onDeselect.processListener(this);

		scrollbar.update();
	}

	void SequenceEditor::setBorderColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
	{
		borderR = r;
		borderG = g;
		borderB = b;
		borderA = a;
	}

	void SequenceEditor::updateSize()
	{
		int32_t positionX, positionY;

		if (isLayoutChild())
		{
			Layout* layout = (Layout*)(parent);
			layout->getOriginPosition(positionX, positionY);
			positionX += x;
			positionY += y;
		}
		else
		{
			getPosition(positionX, positionY); //doesn't change position if on the scroll pane
		}


		int32_t childX, childY;

		if (scrollbar.isHorizontal())
		{
			scrollbar.y = 0;
			scrollbar.x = 0;

			Layout::forceSize(&scrollbar, this->width, 10);
			positionY += scrollbar.getHeight();

			scrollbar.getRelativePosition(childX, childY);
			childX += positionX;
			childY += positionY;
			Layout::setCollisionMask(&scrollbar, childX, childY, childX + scrollbar.width, childY + scrollbar.height);  // adds parent intersection
		}
	}

	void SequenceEditor::setActivateClickByKey(int32_t key)
	{
		onClick.activateByKey = key;
	}

	void SequenceEditor::updateCollisionMask()
	{
		Widget::updateCollisionMask();
		updateChildrenCollisionMask();
	}

	void SequenceEditor::updateChildrenCollisionMask()
	{
		int32_t positionX, positionY;
		int32_t childX, childY;

		if (isLayoutChild())
		{
			Layout* layout = (Layout*)(parent);
			layout->getOriginPosition(positionX, positionY);
			positionX += x;
			positionY += y;
		}
		else
		{
			getPosition(positionX, positionY); //doesn't change position if on the scroll pane
		}

		scrollbar.getRelativePosition(childX, childY);
		childX += positionX;
		childY += positionY;
		Layout::setCollisionMask(&scrollbar, childX, childY, childX + scrollbar.width, childY + scrollbar.height);  // adds parent intersection
	}

	void SequenceEditor::setScrollbarColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
	{
		scrollbar.setColor(r, g, b, a);
	}
}