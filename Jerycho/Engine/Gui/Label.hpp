#pragma once

#include "Widget.hpp"
#include "../Font/Font.hpp"
#include <string>

namespace ja
{

	class Label : public Widget
	{
	private:
		ja::string text;
		uint8_t  r, g, b, a; // normal color
		uint8_t  rS, gS, bS, aS; // color of shadow if as != 0 then shadow enabled
		uint32_t align;

		int8_t shadowOffsetX;
		int8_t shadowOffsetY;
		Font* font;
	public:
		Label();
		Label(uint32_t id, Widget* parent);

		void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setShadowColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void setShadowOffset(int8_t x, int8_t y);
	
		void setText(const char* text);
		void adjustToText(int32_t additionalWidth, int32_t height);
		void appendText(ja::string& text);
		ja::string getText();
		ja::string& getTextReference();
		const char* getTextCstr();
		void setTextPrintC(const char* fieldValue, ...);
		void setAlign(uint32_t marginStyle);
		void getTextSize(int32_t& textWidth, int32_t& textHeight);
		void setFont(const char* fontName, uint32_t fontSize);
		Font* getFont();
		virtual void update();
		virtual void draw(int32_t parentX, int32_t parentY);
		virtual void setLayoutBehaviour(Widget* widget);
		~Label();
	};

}