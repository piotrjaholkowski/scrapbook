#include "UIManager.hpp"
#include "InputManager.hpp"
#include "DisplayManager.hpp"

#include "../Gui/Margin.hpp"

#include "../Gui/Button.hpp"
#include "../Gui/GridLayout.hpp"
#include "../Gui/Menu.hpp"
#include "../Gui/Editbox.hpp"
#include "../Gui/RadioButton.hpp"
#include "../Gui/Checkbox.hpp"
#include "../Gui/Scrollbar.hpp"
#include "../Gui/ScrollPane.hpp"
#include "../Tests/TimeStamp.hpp"

#include "../Utility/Exception.hpp"

namespace ja
{

SDL_mutex* UIManager::guiMutex  = nullptr;
UIManager* UIManager::singleton = nullptr;
std::map<ja::string, Interface*> UIManager::interfaces = std::map<ja::string, Interface*>();
std::map<ja::string, ScriptDelegate*> UIManager::scriptDelegates = std::map<ja::string, ScriptDelegate*>();
bool UIManager::IsMouseOverGui  = false;
Widget* UIManager::lastSelected = nullptr;
Widget* UIManager::lastDragged  = nullptr;
Widget* UIManager::lastClicked  = nullptr;
Widget* UIManager::mouseOverWidget = nullptr;
Widget* UIManager::topWidget = nullptr;
int32_t UIManager::mouseX = 0;
int32_t UIManager::mouseY = 0;
int32_t UIManager::draggedRelPosX = 0;
int32_t UIManager::draggedRelPosY = 0;

UIManager::UIManager()
{
	guiMutex = SDL_CreateMutex();
}

UIManager::~UIManager()
{
	std::map<ja::string, Interface*>::iterator it;
	for(it=interfaces.begin() ; it != interfaces.end(); it++ )
	{
		if((*it).second->removeReference())
		{
			delete (*it).second;
		}
	}

	std::map<ja::string, ScriptDelegate*>::iterator it2;
	for(it2=scriptDelegates.begin() ; it2 != scriptDelegates.end(); it2++ )
	{
		if((*it2).second->removeReference())
		{
			delete (*it2).second;
		}
	}

	// clear list
	interfaces.clear();

	SDL_DestroyMutex(guiMutex);
}

void UIManager::init()
{
	if (singleton == nullptr)
	{
		singleton = new UIManager();
	}
	else {
		delete singleton;
		singleton = new UIManager();
	}

	singleton->setDefaultParameters();
}


void UIManager::setDefaultParameters()
{
	widgetDef.parent = nullptr;
	widgetDef.positionX = 0;
	widgetDef.positionY = 0;
	widgetDef.width = 0;
	widgetDef.height = 0;
	widgetDef.fontName = "default";
	
	widgetDef.fontR = 255;
	widgetDef.fontG = 255;
	widgetDef.fontB = 255;
	widgetDef.fontA = 255;

	widgetDef.r = 255;
	widgetDef.g = 0;
	widgetDef.b = 0;
	widgetDef.a = 255;

	widgetDef.horizontalGap = 0;
	widgetDef.verticalGap = 0;

	widgetDef.align = (uint32_t)Margin::NORMAL;

	widgetDef.isRendered = true;
	widgetDef.isActive = true;

	widgetDef.widgetId = 1;
	widgetDef.widgetType = JA_BUTTON;
	widgetDef.text = "";

	widgetDef.acceptNewLine = false;
	widgetDef.acceptOnlyNumbers = false;
	widgetDef.acceptDynamicBuffering = false;
	widgetDef.columnsNum = 10;
	widgetDef.linesNum = 1;

	widgetDef.minRange = 0;
	widgetDef.maxRange = 100;
	widgetDef.currentValue = 50;
	widgetDef.isHorizontal;

	widgetDef.isChecked = false;

	widgetDef.scrollbarCorner = JA_RIGHT_LOWER_CORNER;
	widgetDef.verticalPolicy = JA_SCROLLBAR_AS_NEEDED;
	widgetDef.horizontalPolicy = JA_SCROLLBAR_AS_NEEDED;
	widgetDef.scrollbarSize = 10;
	widgetDef.autoScrollHorizontal = true;
	widgetDef.autoScrollVertical = true;
	widgetDef.scrollToLeft = true;
	widgetDef.scrollToTop = true;

	widgetDef.isScriptGlobal = false;
	widgetDef.scriptGlobalName = "";
}

void UIManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void UIManager::addInterface( ja::string name, Interface* newInterface )
{
	std::map<ja::string,Interface*>::iterator it;
	it = interfaces.find(name);

	if(it == interfaces.end())
	{
		++newInterface->references;
		interfaces[name] = newInterface;
	}
	else
	{
		throw JA_EXCEPTIONR(ExceptionType::GUI,"Can't add interface: duplicated name");
	}
}


void UIManager::addInterfaceSafe( ja::string name, Interface* newInterface )
{
	SDL_mutexP(guiMutex);
	addInterface(name,newInterface);
	SDL_mutexV(guiMutex);
}


Interface* UIManager::getInterface(ja::string& name )
{
	std::map<ja::string,Interface*>::iterator it;
	it = interfaces.find(name);

	if(it != interfaces.end())
	{
		return it->second;
	}
	
	return nullptr;
}

void UIManager::changeInterface( ja::string name )
{

}

void UIManager::setMouseOverGui(bool state)
{
	IsMouseOverGui = state;
}

bool UIManager::isMouseOverGui()
{
	return IsMouseOverGui;
}

bool UIManager::isTextInputSelected()
{
	if (nullptr == lastSelected)
		return false;

	return lastSelected->isUsingUnicode();
}

void UIManager::setSelected(Widget* widget)
{
	lastSelected = widget;
}

void UIManager::setLastClicked(Widget* widget)
{
	lastClicked = widget;
}

void UIManager::setDragged(Widget* widget)
{
	lastDragged = widget;
	int32_t x, y;
	widget->getPosition(x, y);
	draggedRelPosX = x - mouseX;
	draggedRelPosY = y - mouseY;
}

bool UIManager::isMouseOver( Widget* widget )
{
	if(widget->isSelectable() && widget->isRendered())
	{
		if(mouseOverWidget == nullptr)
		{
			mouseOverWidget = widget;
			setMouseOverGui(true);
			return true;
		}

		for(Widget* itWidget = widget; itWidget != nullptr; itWidget = itWidget->getParent())
		{
			if(itWidget == mouseOverWidget)
			{
				mouseOverWidget = itWidget;
				setMouseOverGui(true);
				return true;
			}
		}
	}	

	return false;
}

void UIManager::setTopWidget( Widget* widget )
{
	topWidget = widget;
}

void UIManager::setUnselected()
{
	lastSelected = nullptr;
}

Widget* UIManager::getLastSelected()
{
	return lastSelected;
}

Widget* UIManager::getLastClicked()
{
	return lastClicked;
}

Widget* UIManager::getLastDragged()
{
	return lastDragged;
}

Widget* UIManager::getTopWidget()
{
	return topWidget;
}

void UIManager::update(float deltaTime)
{
	PROFILER_FUN;

	this->deltaTime       = deltaTime;
	setMouseOverGui(false);

	mouseX = InputManager::getMousePositionX();
	mouseY = InputManager::getMousePositionY();
	mouseY = DisplayManager::getSingleton()->getResolutionHeight() - mouseY;
	
	if(topWidget != nullptr)
	{
		Widget* itChildren = topWidget;
		for(Widget* itWidget = topWidget->getParent(); itWidget != nullptr; itWidget = itWidget->getParent())
		{
			if(itChildren->zOrder != 0)
			{
				itWidget->setTopWidget(itChildren);
			}
			itChildren = itWidget;
		}
	}
	
	this->topWidget       = nullptr;
	this->mouseOverWidget = nullptr;
	this->lastClicked     = nullptr;
	
	if(lastSelected != nullptr)
	{
		if(lastSelected->isUsingUnicode())
		{
			InputManager::setUnicode(true);
		}
		else
		{
			InputManager::setUnicode(false);
		}

		Widget* parent = lastSelected->getParent();
		if(parent != nullptr)
		{
			if(!parent->isActive())
				lastSelected = nullptr;
		}
	}
	else
	{
		InputManager::setUnicode(false);
	}

	
	std::map<ja::string, Interface*>::iterator it;
	for (it = interfaces.begin(); it != interfaces.end(); it++)
	{
		(*it).second->update(); // Flags isActive inside this method
	}

	if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
	{
		lastDragged = nullptr;
	}

	if(!isMouseOverGui())
	{
		if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
			setUnselected();
	}
	//updateRenderBuffer();
}

/*
void jaUIManager::updateRenderBuffer()
{
	if(renderBufferUpdate.empty())
		return;

	//set enviroment
	//don't need to do that draw() method of widget probably will change that states
	//glEnable(GL_TEXTURE_2D);
	//glEnable(GL_ALPHA);
	//glEnable(GL_ALPHA_TEST);
	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//set envitoment

	std::vector<jaWidget*>::iterator it = renderBufferUpdate.end(); // rendering from child to parent
	while(it != renderBufferUpdate.begin())
	{
		it->updateTexBuffer(); 
		--it;
	}

	renderBufferUpdate.clear();
}*/

void UIManager::updateSafe( float deltaTime )
{
	SDL_mutexP(guiMutex);
	update(deltaTime);
	SDL_mutexV(guiMutex);
}

float UIManager::getDeltaTime()
{
	return deltaTime;
}

void UIManager::draw()
{
	PROFILER_FUN;

	std::map<ja::string, Interface*>::iterator it;
	for(it=interfaces.begin() ; it != interfaces.end(); it++ )
	{
		(*it).second->draw(); // flag isRendered inside this method
	}

	if(lastDragged != nullptr)
	{
		DisplayManager* display = DisplayManager::getSingleton();
		bool isBorderRendered = lastDragged->isBorderRendered();
		display->pushScreenCoordinate();
		display->clearWorldMatrix();
	
		int32_t widgetX, widgetY;
		lastDragged->getRelativePosition(widgetX,widgetY);
		WidgetMask collisionMask = lastDragged->getCollisionMask();
		Widget* parent = lastDragged->parent;
		lastDragged->collisionMask.minX = mouseX + draggedRelPosX;
		lastDragged->collisionMask.minY = mouseY + draggedRelPosY;
		lastDragged->collisionMask.maxX = lastDragged->collisionMask.minX + lastDragged->width;
		lastDragged->collisionMask.maxY = lastDragged->collisionMask.minY + lastDragged->height;
		lastDragged->parent = nullptr;
		lastDragged->x = lastDragged->collisionMask.minX;
		lastDragged->y = lastDragged->collisionMask.minY;
		lastDragged->setRenderBorder(false);
		lastDragged->draw(0, 0);
		lastDragged->setRenderBorder(isBorderRendered);
		lastDragged->x = widgetX;
		lastDragged->y = widgetY;
		lastDragged->parent = parent;
		lastDragged->collisionMask = collisionMask;

		display->popWorldCoordinate();
	}
}


void UIManager::drawSafe()
{
	SDL_mutexP(guiMutex);
	draw();
	SDL_mutexV(guiMutex);
}

Widget* UIManager::createWidgetDef()
{
	Widget* widget = nullptr;

	switch(widgetDef.widgetType)
	{
	case JA_LABEL:
		{
			Label* label = new Label(widgetDef.widgetId,widgetDef.parent);
			label->setFont(widgetDef.fontName.c_str(),widgetDef.fontSize);
			label->setColor(widgetDef.r,widgetDef.g,widgetDef.b,widgetDef.a);
			label->setAlign(widgetDef.align);
			label->setText(widgetDef.text.c_str());
			widget = label;
		}
		break;
	case JA_INTERFACE:
		break;
	case JA_BUTTON:
		{
			Button* button = new Button(widgetDef.widgetId,widgetDef.parent);
			button->setFont(widgetDef.fontName.c_str(),widgetDef.fontSize);
			button->setFontColor(widgetDef.fontR,widgetDef.fontG,widgetDef.fontB,widgetDef.fontA);
			button->setText(widgetDef.text.c_str());
			button->setColor(widgetDef.r,widgetDef.g,widgetDef.b,widgetDef.a);
			button->setBorderColor(widgetDef.borderR,widgetDef.borderG,widgetDef.borderB,widgetDef.borderA);
			widget = button;
		}
		break;
	case JA_CHECKBOX:
		{
			Checkbox* checkbox = new Checkbox(widgetDef.widgetId,widgetDef.parent);
			checkbox->setFont(widgetDef.fontName.c_str(),widgetDef.fontSize);
			checkbox->setFontColor(widgetDef.fontR,widgetDef.fontG,widgetDef.fontB,widgetDef.fontA);
			checkbox->setText(widgetDef.text.c_str());
			checkbox->setColor(widgetDef.r,widgetDef.g,widgetDef.b,widgetDef.a);
			checkbox->setBorderColor(widgetDef.borderR,widgetDef.borderG,widgetDef.borderB,widgetDef.borderA);
			checkbox->setCheck(widgetDef.isChecked);
			widget = checkbox;
		}
		break;
	case JA_RADIO_BUTTON:
		{
			RadioButton* radioButton = new RadioButton(widgetDef.widgetId,widgetDef.parent);
			radioButton->setFont(widgetDef.fontName.c_str(),widgetDef.fontSize);
			radioButton->setFontColor(widgetDef.fontR,widgetDef.fontG,widgetDef.fontB,widgetDef.fontA);
			radioButton->setText(widgetDef.text.c_str());
			radioButton->setColor(widgetDef.r,widgetDef.g,widgetDef.b,widgetDef.a);
			radioButton->setBorderColor(widgetDef.borderR,widgetDef.borderG,widgetDef.borderB,widgetDef.borderA);
			radioButton->setCheck(widgetDef.isChecked);
			widget = radioButton;
		}
		break;
	case JA_SCROLL_PANE:
		{
			ScrollPane* scrollPane = new ScrollPane(widgetDef.widgetId,widgetDef.parent);
			scrollPane->setScrollbarSize(widgetDef.scrollbarSize);
			scrollPane->setVerticalScrollbarPolicy(widgetDef.verticalPolicy);
			scrollPane->setHorizontalScrollbarPolicy(widgetDef.horizontalPolicy);
			scrollPane->setHorizontalAutoScroll(widgetDef.autoScrollHorizontal,widgetDef.scrollToLeft);
			scrollPane->setVerticalAutoScroll(widgetDef.autoScrollVertical,widgetDef.scrollToTop);
			scrollPane->setCorner(widgetDef.scrollbarCorner);
			scrollPane->setColor(widgetDef.r,widgetDef.g,widgetDef.b,widgetDef.a);
			scrollPane->setBorderColor(widgetDef.borderR,widgetDef.borderG,widgetDef.borderB,widgetDef.borderA);
			widget = scrollPane;
		}
		break;
	case JA_MENU:
		{
			Menu* menu = new Menu(widgetDef.widgetId,widgetDef.parent);
			menu->setText(widgetDef.text.c_str());
			widget = menu;
		}
		break;
	case JA_HUD:
		break;
	case JA_IMAGE_BUTTON:
		break;
	case JA_SWITCH_BUTTON:
		break;
	case JA_EDITBOX:
		{
			Editbox* editbox = new Editbox(widgetDef.widgetId,widgetDef.parent);
			editbox->setFont(widgetDef.fontName.c_str(),widgetDef.fontSize);
			editbox->setFontColor(widgetDef.fontR,widgetDef.fontG,widgetDef.fontB,widgetDef.fontA);
			editbox->setColor(widgetDef.r,widgetDef.g,widgetDef.b,widgetDef.a);
			editbox->setAcceptNewLine(widgetDef.acceptNewLine);
			editbox->setAcceptOnlyNumbers(widgetDef.acceptOnlyNumbers);
			editbox->setAllowDynamicBuffering(widgetDef.acceptDynamicBuffering);
			editbox->setTextSize(widgetDef.columnsNum,widgetDef.linesNum);
			//editbox->setSize(widgetDef.width,widgetDef.height);
			editbox->setText(widgetDef.text.c_str());
			editbox->setBorderColor(widgetDef.borderR,widgetDef.borderG,widgetDef.borderB,widgetDef.borderA);
			widget = editbox;
		}
		break;
	case JA_SCROLLBAR:
		{
			Scrollbar* scrollbar = new Scrollbar(widgetDef.widgetId,widgetDef.parent);
			scrollbar->setColor(widgetDef.r,widgetDef.g,widgetDef.b,widgetDef.a);
			scrollbar->setRange(widgetDef.minRange,widgetDef.maxRange);
			scrollbar->setValue(widgetDef.currentValue);
			if(widgetDef.isHorizontal)
				scrollbar->setHorizontal();
			else
				scrollbar->setVertical();
			scrollbar->setBorderColor(widgetDef.borderR,widgetDef.borderG,widgetDef.borderB,widgetDef.borderA);
			widget = scrollbar;
		}
		break;
	case JA_PROGRESS_BUTTON:
		break;
	case JA_TIMER:
		break;
	case JA_CHART:
		break;
	case JA_GRID_LAYOUT:
		{
			GridLayout* gridLayout = new GridLayout(widgetDef.widgetId,widgetDef.parent,widgetDef.blocksX,widgetDef.blocksY,widgetDef.horizontalGap,widgetDef.verticalGap);
			widget = gridLayout;
		}
		break;
	case JA_BUTTON_GROUP:
		{
			ButtonGroup* buttonGroup = new ButtonGroup(widgetDef.widgetId,widgetDef.parent);
			widget = buttonGroup;
		}
		break;
	}

	if(widget == nullptr)
		return widget;

	widget->setPosition(widgetDef.positionX,widgetDef.positionY);
	
	if((widgetDef.width != 0) || (widgetDef.height != 0))
	{
		widget->setSize(widgetDef.width,widgetDef.height);
	}

	if(widgetDef.isScriptGlobal)
	{
		widget->setScriptGlobal(widgetDef.scriptGlobalName);
		widgetDef.isScriptGlobal = false;
	}

	widget->setRenderBorder(widgetDef.isBorderRendered);

	widget->setRender(widgetDef.isRendered);
	widget->setActive(widgetDef.isActive);
	
	return widget;
}

ScriptDelegate* UIManager::registerDelegate(ja::string& delegateName )
{
	std::map<ja::string,ScriptDelegate*>::iterator it;
	it = scriptDelegates.find(delegateName);

	if(it == scriptDelegates.end())
	{
		scriptDelegates[delegateName] = new ScriptDelegate(delegateName);
		return scriptDelegates[delegateName];
	}
	else
	{
		return it->second;
	}
}

}