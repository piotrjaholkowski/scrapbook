#include "../jaSetup.hpp"
#include "GameManager.hpp"
#include "LogManager.hpp"
#include "DisplayManager.hpp"
#include "ResourceManager.hpp"
#include "InputManager.hpp"
#include "UIManager.hpp"
#include "LayerObjectManager.hpp"
#include "AnimationManager.hpp"
#include "PhysicsManager.hpp"
#include "LevelManager.hpp"
#include "ScriptManager.hpp"
#include "EntityManager.hpp"
#include "SoundManager.hpp"
#include "EffectManager.hpp"
#include "ThreadManager.hpp"
#include "RecycledMemoryManager.hpp"
#include "../Tests/TimeStamp.hpp"

namespace ja
{

GameManager* GameManager::singleton = nullptr;
bool GameManager::initializedGame = false;

GameManager::GameManager()
{
	tempObjectHandleList = new StackAllocator<ObjectHandle2d*>(setup::graphics::TEMP_LAYER_OBJECTS_NUM, 16);
	renderBounds         = false;
	renderAABB           = false;
}

GameManager::~GameManager()
{
	if(tempObjectHandleList != nullptr)
		delete tempObjectHandleList;
}

void GameManager::initGameManager( const char* title )
{
	if(!initializedGame)
	{
#ifdef JA_SDL_API
#ifdef JA_SDL2_VERSION
		//SDL_SetWindowTitle
#else
		SDL_WM_SetCaption(title.c_str(), title.c_str());
#endif
#endif
		singleton = new GameManager();
		initializedGame = true;
	}
}

void GameManager::cleanUpAll()
{
	LevelManager::cleanUp();
	ScriptManager::cleanUp();
	EffectManager::cleanUp();
	DisplayManager::cleanUp();	
	InputManager::cleanUp();
	UIManager::cleanUp();
	LayerObjectManager::cleanUp();
	AnimationManager::cleanUp();
	PhysicsManager::cleanUp();
	
	EntityManager::cleanUp();
	
	ResourceManager::cleanUp();
	SoundManager::cleanUp();
	ThreadManager::cleanUp();

	LogManager::cleanUp();
	RecycledMemoryManager::cleanUp();

	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void GameManager::update( float deltaTime )
{
	PROFILER_FUN;

	this->deltaTime = deltaTime;

	LayerObjectManager::getSingleton()->getLayerObjects(tempObjectHandleList, &cameraInterface);
	layerRenderer.refresh(tempObjectHandleList);
	tempObjectHandleList->clear();
}

CameraInterface* GameManager::getActiveCamera()
{
	return &cameraInterface;
}

void GameManager::beginRender()
{
	DisplayManager::pushWorldMatrix();
	DisplayManager::clearWorldMatrix();
}

void GameManager::renderScene()
{
	PROFILER_FUN;

	EffectManager* effectManager = EffectManager::getSingleton();
	
	effectManager->renderEffects(&layerRenderer);

	const uint8_t layersNum = effectManager->getLayersNum();
	
	if(renderDebug)
	{
		if(renderBounds)
		{
			DisplayManager::setColorUb(0,255,255);

			for (uint8_t i = 0; i < layersNum; ++i)
			{
				Layer* pLayer = effectManager->getLayer(i);

				if (pLayer->isActive)
					layerRenderer.renderLayerObjectBounds(i);
			}			
		}

		if(renderAABB)
		{
			DisplayManager::setColorUb(255,0,255);

			for (uint8_t i = 0; i < layersNum; ++i)
			{
				Layer* pLayer = effectManager->getLayer(i);

				if (pLayer->isActive)
					layerRenderer.renderLayerObjectAABB(i);
			}	
		}
	}
	
}

void GameManager::allowDropEvent(bool state)
{
#ifdef JA_SDL2_VERSION
	if (state)
	{
		SDL_EventState(SDL_DROPFILE, SDL_ENABLE);
	}
	else
	{
		SDL_EventState(SDL_DROPFILE, SDL_DISABLE);
	}
#endif
}

void GameManager::showMouse(bool state)
{
	if(state)
	{
		SDL_ShowCursor(SDL_ENABLE);
	}
	else
	{
		SDL_ShowCursor(SDL_DISABLE);
	}
	
}

void GameManager::grabInput(bool state)
{
#ifdef JA_SDL2_VERSION
	if (state)
	{
		SDL_SetRelativeMouseMode(SDL_TRUE);
	}
	else
	{
		SDL_SetRelativeMouseMode(SDL_FALSE);
	}
#else
	if (state)
	{
		SDL_WM_GrabInput(SDL_GRAB_ON);
	}
	else
	{
		SDL_WM_GrabInput(SDL_GRAB_OFF);
	}
#endif
}

void GameManager::blockMouseEvents(bool state)
{
	if(state)
	{
		SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE); // block mouse events
	}
	else
	{
		SDL_EventState(SDL_MOUSEMOTION, SDL_ENABLE);
	}
}

void GameManager::getWindowPosition( float worldX, float worldY, int32_t& windowX, int32_t& windowY )
{
	CameraInterface* camera2d = getActiveCamera();
	jaVector2 camPosition     = camera2d->position;
	jaFloat camAngle          = camera2d->angleRad;
	jaVector2 camScale        = camera2d->scale;

	//Position of point from center of camera in world units
	jaFloat deltaCamX = camPosition.x - worldX;
	jaFloat deltaCamY = camPosition.y - worldY;

	//unfinished
}

}