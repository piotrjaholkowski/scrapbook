#pragma once

#include "../jaSetup.hpp"
#include "../Graphics/Skeleton.hpp"

#include "../Gilgamesh/Mid/AABB2d.hpp"

#include "../Allocators//CacheLineAllocator.hpp"
#include "../Allocators/StackAllocator.hpp"

namespace ja
{
	

	class AnimationDefaultConfiguration 
	{
	public:
		CacheLineAllocator*      commonAllocator;

		AnimationDefaultConfiguration() 
		{
			//create allocator
			this->commonAllocator = new CacheLineAllocator(setup::graphics::ANIMATION_ALLOCATOR_SIZE, 64); // 16 * 4KB
		}
	};

	class AnimationManager
	{
	private:
		static AnimationManager* singleton;

		CacheLineAllocator* commonAllocator;

		Skeleton* firstSkeleton;
		Skeleton* lastSkeleton;

		uint32_t  skeletonsNum = 0;
		Skeleton* skeletons[setup::graphics::MAX_SKELETONS];
	
		AnimationManager(AnimationDefaultConfiguration& configuration);
	public:
		SkeletonDef skeletonDef;

		static void init(AnimationDefaultConfiguration& configuration);
		static void cleanUp();

		static inline AnimationManager* getSingleton()
		{
			return singleton;
		}
		~AnimationManager();		

		void update(float deltaTime);
		
		void      getSkeletonsAtAABB(const gil::AABB2d& aabb, StackAllocator<Skeleton*>* skeletonList);
		Skeleton* createSkeletonDef(uint16_t skeletonId = setup::animation::ID_UNASSIGNED);

		uint16_t getNextOwnerId(SkeletalAnimations* skeletalAnimations);

		void drawDebug(CameraInterface* camera);

		friend class LevelManager;
	};
}