#include "AnimationManager.hpp"
#include "ResourceManager.hpp"

#include "../Tests/TimeStamp.hpp"
#include "../Gui/Margin.hpp"

namespace ja
{
	AnimationManager* AnimationManager::singleton = nullptr;

	AnimationManager::AnimationManager(AnimationDefaultConfiguration& configuration)
	{
		this->commonAllocator = configuration.commonAllocator;
		this->firstSkeleton   = nullptr;
		this->lastSkeleton    = nullptr;
	}

	AnimationManager::~AnimationManager()
	{
		delete this->commonAllocator;
	}

	void AnimationManager::init(AnimationDefaultConfiguration& configuration)
	{
		if (singleton == nullptr)
		{
			singleton = new AnimationManager(configuration);
		}
	}

	void AnimationManager::cleanUp()
	{
		if (singleton != nullptr)
		{
			delete singleton;
			singleton = nullptr;
		}
	}

	void AnimationManager::update(float deltaTime)
	{
		PROFILER_FUN;

		Skeleton* itSkeleton = firstSkeleton;

		while(itSkeleton != nullptr)
		{
			itSkeleton->calculateTransformedBoneStates();
			itSkeleton = itSkeleton->next;
		}
	}

	void AnimationManager::getSkeletonsAtAABB(const gil::AABB2d& aabb, StackAllocator<Skeleton*>* skeletonList)
	{
		Skeleton* itSkeleton = firstSkeleton;

		while (itSkeleton != nullptr)
		{
			if (aabb.isPointInside(itSkeleton->position))
			{
				skeletonList->push(itSkeleton);
			}

			itSkeleton = itSkeleton->next;
		}
	}

	Skeleton* AnimationManager::createSkeletonDef(uint16_t skeletonId)
	{
		if (skeletonId == setup::animation::ID_UNASSIGNED)
		{
			skeletonId = skeletonsNum++; //handle->getId();
		}

		Skeleton* skeleton = new (this->commonAllocator->allocate(sizeof(Skeleton))) Skeleton(this->commonAllocator);

		skeleton->skeletonId = skeletonId;

		skeleton->setSkeletonDef(this->skeletonDef);

		char skeletonName[setup::TAG_NAME_LENGTH];
		sprintf(skeletonName, "skeleton %d", (int32_t)(skeletonId));
		skeleton->skeletonName.setName(skeletonName);

		this->skeletons[skeletonId] = skeleton;

		if (this->lastSkeleton == nullptr)
		{
			this->lastSkeleton  = skeleton;
			this->firstSkeleton = this->lastSkeleton;
		}
		else
		{
			this->lastSkeleton->next = skeleton;
			skeleton->prev           = this->lastSkeleton;
		}

		return skeleton;
	}

	uint16_t AnimationManager::getNextOwnerId(SkeletalAnimations* skeletalAnimations)
	{
		Skeleton* itSkeleton = this->firstSkeleton;

		while (itSkeleton != nullptr)
		{
			if (itSkeleton->skeletalAnimations == skeletalAnimations)
			{
				if (itSkeleton->skeletonId != skeletalAnimations->getOwnerId())
				{
					return itSkeleton->skeletonId;
				}
			}

			itSkeleton = itSkeleton->next;
		}

		return MAXUINT16;
	}

	void AnimationManager::drawDebug(CameraInterface* camera)
	{
		gil::AABB2d cameraAABB;
		camera->getBoundingBox(&cameraAABB);

		Skeleton* itSkeleton = this->firstSkeleton;

		while (itSkeleton != nullptr)
		{
			itSkeleton->draw();
			itSkeleton = itSkeleton->next;
		}

		Font* fontRender = ResourceManager::getSingleton()->getEternalFont("default.ttf",10)->resource;
		float scale = DisplayManager::getHeightMultiplier();

		itSkeleton = this->firstSkeleton;

		while (itSkeleton != nullptr)
		{
			jaVector2   position = itSkeleton->getPosition();
			
			if (cameraAABB.isPointInside(position))
			{
				const char* name = itSkeleton->skeletonName.getName();
				DisplayManager::RenderTextWorld(fontRender, position.x, position.y, scale, name, (uint32_t)Margin::DOWN | (uint32_t)Margin::RIGHT, 255, 255, 255, 200);
			}

			itSkeleton = itSkeleton->next;
		}
	}
}
