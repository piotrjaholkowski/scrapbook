#include "../jaSetup.hpp"
#include "InputManager.hpp"

namespace ja
{

InputManager::InputManager()
{
#ifdef JA_SDL_API
	
#ifdef JA_SDL2_VERSION
	keystate = const_cast<Uint8*>( SDL_GetKeyboardState(&bufforLength) );
	keystateBuffor1 = new Uint8[bufforLength];
	keystateBuffor2 = new Uint8[bufforLength];
#else
	keystate = SDL_GetKeyState(&bufforLength);
	keystateBuffor1 = new Uint8[bufforLength];
	keystateBuffor2 = new Uint8[bufforLength];
#endif
	
#endif JA_SDL_API
}

InputManager::~InputManager()
{
#ifdef JA_SDL_API
	delete [] keystateBuffor1;
	delete [] keystateBuffor2;
#endif JA_SDL_API
}

void InputManager::setUnicode(bool state)
{
#ifndef JA_SDL2_VERSION
	if(state == true)
	{
		if(SDL_EnableUNICODE(-1) == 0)
			SDL_EnableUNICODE(SDL_ENABLE);
	}
	else
	{
		if(SDL_EnableUNICODE(-1) == 1)
			SDL_EnableUNICODE(SDL_DISABLE);
	}
#else
	if (state)
		SDL_StartTextInput();
	else
		SDL_StopTextInput();
#endif
}

void InputManager::addInputEvent(InputEvent inputEvent, uint16_t inputKey, uint16_t unicodeChar)
{
	inputEvents.push_back(InputAction(inputEvent,inputKey,unicodeChar));
}

void InputManager::addInputEvent(InputEvent inputEvent, char* utf8Char)
{
	uint16_t unicodeChar = *(uint16_t*)(utf8Char);
	inputEvents.push_back(InputAction(inputEvent, unicodeChar, unicodeChar));
}

std::list<InputAction>* InputManager::getInputEvents()
{
	return &inputEvents;
}

void InputManager::updateInput()
{
#ifdef JA_SDL_API

	bufferedMouseButton2 = bufferedMouseButton1;
	bufferedMouse2X = bufferedMouse1X;
	bufferedMouse2Y = bufferedMouse1Y;

	memcpy(keystateBuffor2,keystateBuffor1,bufforLength * sizeof(Uint8));
	memcpy(keystateBuffor1,keystate,bufforLength * sizeof(Uint8));

	bufferedMouseButton2 = bufferedMouseButton1;
	bufferedMouse2X = bufferedMouse1X;
	bufferedMouse2Y = bufferedMouse1Y;
	bufferedMouseButton1 = SDL_GetMouseState(&bufferedMouse1X,&bufferedMouse1Y);
	
#endif JA_SDL_API
}

void InputManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void InputManager::clearInputEvents()
{
	inputEvents.clear();
	mouseWheelUp   = false;
	mouseWheelDown = false;
}

void InputManager::init()
{
	if(singleton==nullptr)
	{
		singleton = new InputManager();
	}
}

void InputManager::removeKeyEvent( int32_t key )
{
	keystateBuffor1[key] = 0;
	keystateBuffor2[key] = 0;
}


bool InputManager::isKeyPressed( int32_t key )
{
#ifdef JA_SDL2_VERSION
	SDL_Scancode  scancode = SDL_GetScancodeFromKey(key);
	if (keystateBuffor1[scancode])
	{
		if (keystateBuffor2[scancode])
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	return false;
#else
	if (keystateBuffor1[key])
	{
		if (keystateBuffor2[key])
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	return false;
#endif
}

bool InputManager::isKeyReleased( int32_t key )
{
#ifdef JA_SDL2_VERSION
	SDL_Scancode  scancode = SDL_GetScancodeFromKey(key);
	if (keystateBuffor2[scancode])
	{
		if (keystateBuffor1[scancode])
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	return false;
#else
	if (keystateBuffor2[key])
	{
		if (keystateBuffor1[key])
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	return false;
#endif
}

#pragma warning(disable : 4800)
bool InputManager::isKeyPress( int32_t key )
{
#ifdef JA_SDL2_VERSION
	SDL_Scancode  scancode = SDL_GetScancodeFromKey(key);
	return keystateBuffor1[scancode];
#else
	return keystateBuffor1[key];
#endif
}
#pragma warning(default : 4800)

bool InputManager::isKeyModPress(KeyMod mod)
{
	if (mod & (KeyMod)SDL_GetModState()) return true;
	return false;
}

bool InputManager::isMouseButtonPressed( uint8_t button )
{
	if(bufferedMouseButton1&SDL_BUTTON(button))
	{
		if(bufferedMouseButton2&SDL_BUTTON(button))
		{
			return false;
		}
		else return true;
	}
	else
	{
		return false;
	} 
}

bool InputManager::isMouseButtonReleased( uint8_t button )
{
	if(bufferedMouseButton1&SDL_BUTTON(button))
	{
		return false;
	}
	else
	{
		if(bufferedMouseButton2&SDL_BUTTON(button))
		{
			return true;
		}
		else return false;
	} 
}

void InputManager::setMouseWheelUp()
{
	mouseWheelUp = true;
}

void InputManager::setMouseWheelDown()
{
	mouseWheelDown = true;
}

bool InputManager::isMouseWheelUp()
{
	return mouseWheelUp;
}

bool InputManager::isMouseWheelDown()
{
	return mouseWheelDown;
}

#pragma warning(disable : 4800)
bool InputManager::isMouseButtonPress( uint8_t button )
{
	return bufferedMouseButton1 & SDL_BUTTON(button);
}
#pragma warning(default : 4800)

int32_t InputManager::getMousePositionX() 
{
	return bufferedMouse1X;
}

int32_t InputManager::getMousePositionY() 
{
	return bufferedMouse1Y;
}

int32_t InputManager::getRelativeMousePositionX() 
{
	return (bufferedMouse1X - bufferedMouse2X);
}

int32_t InputManager::getRelativeMousePositionY() 
{
	return (bufferedMouse1Y - bufferedMouse2Y);
}

void InputManager::getMousePositionXY(int32_t* x, int32_t* y) 
{
	*x = bufferedMouse1X;
	*y = bufferedMouse1Y;
}

void InputManager::getRelativeMousePositionXY(int32_t* x, int32_t* y) 
{
	//SDL_GetRelativeMouseState(x, y);
	*x = (bufferedMouse1X - bufferedMouse2X);
	*y = (bufferedMouse1Y - bufferedMouse2Y);
}

Uint8* InputManager::keystate = nullptr;

Uint8* InputManager::keystateBuffor2 = nullptr;

Uint8* InputManager::keystateBuffor1 = nullptr;

uint8_t InputManager::bufferedMouseButton1 = 0;

uint8_t InputManager::bufferedMouseButton2 = 0;

int32_t InputManager::bufferedMouse2Y = 0;

int32_t InputManager::bufferedMouse2X = 0;

int32_t InputManager::bufferedMouse1Y = 0;

int32_t InputManager::bufferedMouse1X = 0;

int32_t InputManager::bufforLength = 0;

bool InputManager::mouseWheelDown = false;

bool InputManager::mouseWheelUp = false;

InputManager* InputManager::singleton = nullptr;
std::list<InputAction> InputManager::inputEvents = std::list<InputAction>();

}
