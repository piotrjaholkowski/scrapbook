#include "PhysicsManager.hpp"
#include "LevelManager.hpp"
#include "../Physics/Constraints2d.hpp"
#include "../Entity/PhysicGroup.hpp"
#include "../Physics/PhysicThreads.hpp"
#include "../Gilgamesh/DebugDraw.hpp"
#include "../Tests/TimeStamp.hpp"
#include "RecycledMemoryManager.hpp"

namespace ja
{

PhysicsManager* PhysicsManager::singleton = nullptr;

PhysicsManager::PhysicsManager(PhysicsManagerConfiguration& configuration)
{
	//Debug configuration
	drawShapes            = true;
	fixConstraintPosition = false;
	drawAABB              = true;
	drawVelocities        = true;
	drawConstraints       = true;
	drawHashCells         = false;
	drawId                = false;
	drawUser              = nullptr;

	//Engine configuration
	velocityIterations = 8;
	positionIterations = 2;
	isWarmStart = true; // Apply impulses from previous frame for better stabilization
	isShockPropagation       = true; // Apply impulses in correct order dependent from isConstraintGroups
	scene2d                  = configuration.scene2d;
	physicsBodyAllocator     = configuration.physicBodyAllocator;
	temporaryShapeAllocator  = configuration.temporaryShapeAllocator;
	sceneAllocator           = configuration.sceneAllocator;

	constraintAllocator = configuration.constraintAllocator;
	for(int32_t i=0; i<setup::physics::MAX_CONSTRAINT_CREATOR; ++i)
	{
		constraintCreator[i] = configuration.constraintCreator[i];
	}

	islandAllocator = new CacheLineAllocator(setup::physics::PHYSICS_ISLAND_ALLOCATOR_SIZE, 32); //Allocator for island generation and schock propagation
	//islandAllocator = new CacheLineAllocator(setup::physics::PHYSICS_ISLAND_ALLOCATOR_SIZE, 4);

	constraintSynchronizer = 0;
	constraintIsland = nullptr;
	
	for (int32_t i = 0; i < setup::physics::MAX_PHYSIC_OBJECT; ++i)
	{
		this->physicObjects[i] = nullptr;
		this->constraints[i]   = nullptr;
	}

	for (int32_t i = 0; i < setup::physics::MAX_ISLAND_TO_SLEEP_CACHE; ++i)
	{
		islandToSleep->isFreeSlot = true;
	}

	this->physicObjectDef.init(this);


	std::cout << "Constraints" << std::endl;
	std::cout << "Allocator allign to " << constraintAllocator->getCacheLineSize() << std::endl;
	std::cout << "RigidContact2d "  << sizeof(RigidContact2d)  << std::endl; 
	std::cout << "RevoluteJoint2d " << sizeof(RevoluteJoint2d) << std::endl;
	std::cout << "DistanceJoint2d " << sizeof(DistanceJoint2d) << std::endl;
	std::cout << "Contact2d "       << sizeof(Contact2d)       << std::endl;

	std::cout << std::endl;
	std::cout << "Physic bodies" << std::endl;
	std::cout << "Allocator allign to " << physicsBodyAllocator->getCacheLineSize() << std::endl;
	std::cout << "RigidBody2d "         << sizeof(RigidBody2d) << std::endl; 
	std::cout << "ParticleGenerator2d " << sizeof(ParticleGenerator2d) << std::endl;
	
	
	std::cout << std::endl;
	std::cout << "Island" << std::endl;
	//std::cout << "Allocator allign to " << islandAllocator->getCacheLineSize() << std::endl;
	std::cout << "ConstraintList " << sizeof(ConstraintList2d) << std::endl;
	std::cout << "ConstraintIsland2d " << sizeof(ConstraintIsland2d) << std::endl;
	std::cout << "ConstraintIslandCache2d " << sizeof(ConstraintIslandCache2d) << std::endl;

	//std::cout << "gil::ObjectHandle2d " << sizeof(ObjectHandle2d) << std::endl;
	//std::cout << "gil::AABB2d " << sizeof(AABB2d) << std::endl;
	//std::cout << "gil::HashProxy " << sizeof(HashProxy) << std::endl;
	//std::cout << "gil::Box2d " << sizeof(Box2d) << std::endl;
	//std::cout << "gil::Circle2d " << sizeof(Circle2d) << std::endl;

	//std::cout << "gilConvexSlope2d " << sizeof(gilConvexSlope2d) << std::endl;
	//std::cout << "gilCapsule2d " << sizeof(gilCapsule2d) << std::endl;
	//std::cout << "gilParticles2d " << sizeof(gilParticles2d) << std::endl;*/
}

PhysicsManager::~PhysicsManager()
{
	delete this->scene2d; // this doesn't delete common allocator
	delete this->constraintAllocator; // delete allocator from configuration
	delete this->islandAllocator;
	delete this->physicsBodyAllocator; 
	delete this->sceneAllocator; // delete scene allocator from gilScene2d
}

void PhysicsManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void PhysicsManager::init(PhysicsManagerConfiguration& configuration)
{
	if(singleton==nullptr)
	{ 
		singleton = new PhysicsManager(configuration);
	}

}

PhysicObject2d* PhysicsManager::getLastAwakePhysicObject()
{
	ObjectHandle2d* objectHandle = scene2d->getLastDynamicObject();

	if(objectHandle != nullptr)
		return (PhysicObject2d*)(objectHandle->getUserData());
	
	return nullptr;
}

PhysicObject2d* PhysicsManager::getPhysicObject(uint16_t objectId)
{
	return physicObjects[objectId];
}

RigidBody2d* PhysicsManager::getBody(uint16_t bodyId)
{
	return (RigidBody2d*)(physicObjects[bodyId]);
}

ParticleGenerator2d* PhysicsManager::getParticleGenerator(uint16_t particleGeneratorId)
{
	return (ParticleGenerator2d*)(physicObjects[particleGeneratorId]);
}

void PhysicsManager::step(jaFloat deltaTime)
{
	if(deltaTime < GIL_ZERO)
		return;

	++constraintSynchronizer;
	{
		JATEST_TIMESTAMP(JA_COMPUTE_FORCES_TIME_STAMP);
		computeForces(deltaTime); // multithread
	}

	// solve time of impact

	{
		JATEST_TIMESTAMP(JA_GENERATE_CONTACTS_TIME_STAMP);
		generateContacts();
	}

	{
		JATEST_TIMESTAMP(JA_REMOVE_OLD_CONSTRAINTS_TIME_STAMP);
		removeOldConstraints();
	}
	
	{
		JATEST_TIMESTAMP(JA_GENERATE_CONSTRAINT_ISLAND_TIME_STAMP);
		generateConstraintIslands();
	}
	
	//shockPropagation();

	{
		JATEST_TIMESTAMP(JA_RESOLVE_CONSTRAINTS_TIME_STAMP);
		resolveConstraints(deltaTime); //resolve jacobsen, acImpulse and liquids, solve velocity, solve position
	}
	{
		JATEST_TIMESTAMP(JA_MAKE_ISLAND_SLEEP_TIME_STAMP);
		makeIslandSleep(deltaTime); // put relaxed constraint island to sleep
	}
	{
		JATEST_TIMESTAMP(JA_INTEGRATION_TIME_STAMP);
		integrate(deltaTime); // multithread
	}
	{
		JATEST_TIMESTAMP(JA_CLEAR_FORCES_TIME_STAMP);
		clearForces(); // multithread
	}
}

void PhysicsManager::debugStep(jaFloat deltaTime)
{
	PROFILER_FUN;

	if(deltaTime < GIL_ZERO)
		return;

	++constraintSynchronizer;
	
	generateContacts();
	removeOldConstraints();
	generateConstraintIslands();

	if (!fixConstraintPosition)
		return;

	preStepPhase(deltaTime);
	resolvePositionConstraints(deltaTime);
		
	return;	
}

void PhysicsManager::computeForcesSingleThread(jaFloat deltaTime)
{
	ObjectHandle2d* dynamicHandle = scene2d->getLastDynamicObject();
	jaVector2 acceleration;
	jaFloat angluarAcceleration;
	jaFloat damping;

	for (dynamicHandle; dynamicHandle != nullptr; dynamicHandle = dynamicHandle->prev)
	{
		PhysicObject2d* physicObject = (PhysicObject2d*)(dynamicHandle->getUserData());
		switch (physicObject->getObjectType())
		{ 
		case (uint8_t)PhysicObjectType::RIGID_BODY:
		{
			RigidBody2d* dynamicBody = (RigidBody2d*)(physicObject);

			if (dynamicBody->isSleeping())
				continue;

			dynamicBody->netForce.x += dynamicBody->gravity.x * dynamicBody->mass;
			dynamicBody->netForce.y += dynamicBody->gravity.y * dynamicBody->mass;
			damping = dynamicBody->linearDamping * dynamicBody->timeFactor;
			dynamicBody->linearVelocity.x -= (dynamicBody->linearVelocity.x * damping);
			dynamicBody->linearVelocity.y -= (dynamicBody->linearVelocity.y * damping);
			acceleration.x = dynamicBody->netForce.x * dynamicBody->invMass;
			acceleration.y = dynamicBody->netForce.y * dynamicBody->invMass;
			dynamicBody->linearVelocity.x += acceleration.x;
			dynamicBody->linearVelocity.y += acceleration.y;

			if (!dynamicBody->isFixedRotation()) // for fixed rotation invInteria is 0
			{
				dynamicBody->angularVelocity -= (dynamicBody->angularVelocity * dynamicBody->angularDamping * dynamicBody->timeFactor);
				angluarAcceleration = dynamicBody->torque * dynamicBody->invInteria;
				dynamicBody->angularVelocity += angluarAcceleration;
			}
		}
			break;
		case (uint8_t)PhysicObjectType::DEFORMABLE_BODY:
			break;
		case (uint8_t)PhysicObjectType::PARTICLE_GENERATOR:
			break;
		case (uint8_t)PhysicObjectType::LIQUID:
			break;
		}
	}
}

void PhysicsManager::computeForcesMultipleThread(jaFloat deltaTime)
{
	ThreadManager* threadManager = ThreadManager::getSingleton();
	//threadManager->createTaskArray(&physicObjects, scene2d->getHighestGeneratedId(), NULL, 0, jaPhysicThread::computeForces, NULL);
	threadManager->createTaskArray(scene2d->getLastDynamicObject(), 0, nullptr, 0, PhysicThread::computeForces, nullptr);
	threadManager->waitUntilFinish();
}

void PhysicsManager::computeForces(jaFloat deltaTime)
{
	uint8_t threadsNum = 0;
	ThreadManager* threadManager = ThreadManager::getSingleton();
	if (threadManager != nullptr)
		threadsNum = threadManager->getThreadsNum();
	
	if (threadsNum <= 1) {
		computeForcesSingleThread(deltaTime);
	}
	else {
		computeForcesMultipleThread(deltaTime);
	}
}

void PhysicsManager::generateContactsSingleThread()
{
	contactList.clear();
	{
		JATEST_TIMESTAMP(JA_COLLISION_DETECTION_TIME_STAMP);
		scene2d->generateCollisions(&contactList);
	}

	gil::Contact2d* contacts = contactList.getFirst();
	uint32_t bodyContactNum =  contactList.getSize();

	uint16_t lowerId;
	uint16_t higherId;
	uint32_t constraintHash;
	RigidContact2d* bodyContact;
	Constraint2d*   itConstraint;
	gil::Contact2d* contact;
	PhysicObject2d* physicObjectA;
	PhysicObject2d* physicObjectB;
	bool solved = false;

	for (uint32_t i = 0; i < bodyContactNum; ++i)
	{
		contact = &contacts[i];
		contact->getHandleId(lowerId, higherId);
		physicObjectA = physicObjects[lowerId];
		physicObjectB = physicObjects[higherId];

		//here use some preAdd to contact group callback if true solve if false don't
		//if colliding object are asleep they won't generate contact
		
		if (physicObjectA->bodyType != (uint8_t)BodyType::DYNAMIC_BODY)
		{
			if(physicObjectB->bodyType != (uint8_t)BodyType::DYNAMIC_BODY)
				continue;
			//Don't let make kinematic-static or kinematic-kinematic constraints	
		}

		if (physicObjectA->isSleeping()) // Kinematic and static bodies can't be put to sleep
		{
			//There is no collision between two sleeping bodies because
			//sleeping objects have static handles
			wakeUpPhysicObject(physicObjectA); //wake up bodies linked to that body
		}

		if (physicObjectB->isSleeping()) // Kinematic and static bodies can't be put to sleep
		{
			//There is no collision between two sleeping bodies because
			//sleeping objects have static handles
			wakeUpPhysicObject(physicObjectB);//wake up bodies linked to that body
		}

		//check for right type of contact deformable body - rigid etc.
		constraintHash = Constraint2d::getHash(lowerId, higherId, JA_RIGID_BODY_CONTACT2D);

		if (constraints[lowerId] != nullptr)
		{
			itConstraint = constraints[lowerId];

			//Find if contact already exists
			for (itConstraint; itConstraint != nullptr; itConstraint = itConstraint->getNext(physicObjectA))
			{
				if (itConstraint->hash == constraintHash)
				{
					// do some high quality check
					if (itConstraint->isEqual(lowerId, higherId, JA_RIGID_BODY_CONTACT2D)) //update contact
					{
						bodyContact = (RigidContact2d*)(itConstraint);
						bodyContact->unmarkToDelete();
						bodyContact->updateContact(contact);
						solved = true;
						break; // add next contact
					}
				}
			}

			if (solved == true)
			{
				solved = false;
				continue; // Add next contact
			}
		}

		bodyContact = new (constraintAllocator->allocate(sizeof(RigidContact2d))) RigidContact2d(physicObjectA, physicObjectB, constraintAllocator);
		bodyContact->unmarkToDelete();
		bodyContact->updateContact(contact);
		addConstraint(bodyContact);
	}

}

void PhysicsManager::generateContactsMultipleThread()
{
	ThreadManager* threadManager = ThreadManager::getSingleton();
	void* dataPairMem;
	void* contactsMem;
	CollisionDataPairMultithread* dataPair;
	uint32_t bodyContactNum;

	{
		JATEST_TIMESTAMP(JA_COLLISION_DETECTION_TIME_STAMP);
		unsigned long collisionDataPairSize = static_cast<unsigned long>(static_cast<unsigned long>(sizeof(CollisionDataPairMultithread)) * static_cast<unsigned long>(setup::gilgamesh::MAX_CONTACTS_PER_OBJECT) * static_cast<unsigned long>(setup::gilgamesh::MAX_OBJECTS) );
		unsigned long contactsSize = static_cast<unsigned long>(threadManager->getThreadsNum()) * static_cast<unsigned long>(sizeof(Contact2d)) * static_cast<unsigned long>(setup::gilgamesh::MAX_OBJECTS) * static_cast<unsigned long>(setup::gilgamesh::MAX_CONTACTS_PER_OBJECT);
		dataPairMem = RecycledMemoryManager::getSingleton()->getMemoryBlock(collisionDataPairSize);
		contactsMem = RecycledMemoryManager::getSingleton()->getMemoryBlock(contactsSize);
		dataPair = (CollisionDataPairMultithread*)( (int8_t*)(dataPairMem) + ((uint32_t)(dataPairMem) % setup::gilgamesh::CACHE_LINE_SIZE) );
		bodyContactNum = scene2d->generateCollisionsMultiThread(dataPair, contactsMem, contactsSize); 
	}
	
	uint32_t constraintHash;
	uint16_t lowerId;
	uint16_t higherId;

	for (uint32_t i = 0; i < bodyContactNum; ++i)
	{
		if (dataPair[i].contacsNum == 0)
			continue;

		bool solved = false;
		gil::Contact2d* contact = dataPair[i].contacts;
		Constraint2d* itConstraint;
		contact->getHandleId(lowerId, higherId);
		PhysicObject2d* physicObjectA = physicObjects[lowerId];
		PhysicObject2d* physicObjectB = physicObjects[higherId];

		if ((physicObjectA->bodyType != (uint8_t)BodyType::DYNAMIC_BODY) && (physicObjectB->bodyType != (uint8_t)BodyType::DYNAMIC_BODY))
		{
			//Don't let make kinematic-static or kinematic-kinematic constraints
			continue;
		}

		if (physicObjectA->isSleeping()) // Kinematic and static bodies can't be put to sleep
		{
			wakeUpPhysicObject(physicObjectA); //wake up bodies linked to that body
		}

		if (physicObjectB->isSleeping()) // Kinematic and static bodies can't be put to sleep
		{
			wakeUpPhysicObject(physicObjectB);//wake up bodies linked to that body
		}

		constraintHash = Constraint2d::getHash(lowerId, higherId, JA_RIGID_BODY_CONTACT2D);

		if (constraints[lowerId] != nullptr)
		{
			itConstraint = constraints[lowerId];

			//Find if contact already exists
			for (itConstraint; itConstraint != nullptr; itConstraint = itConstraint->getNext(physicObjectA))
			{
				if (itConstraint->hash == constraintHash)
				{
					// do some high quality check
					if (itConstraint->isEqual(lowerId, higherId, JA_RIGID_BODY_CONTACT2D)) //update contact
					{
						RigidContact2d* bodyContact = (RigidContact2d*)(itConstraint);
						bodyContact->unmarkToDelete();
						bodyContact->updateContact(contact);
							
						if (dataPair[i].contacsNum > 1) {
							for (uint32_t z = 0; z < dataPair[i].contacsNum; ++z)
							{
								++contact;
								bodyContact->updateContact(contact);
							}
						}
						solved = true;
						break; // add next contact
					}
				}
			}

			if (solved == true)
			{
				continue; // Add next contact
			}
		}

		RigidContact2d* bodyContact = new (constraintAllocator->allocate(sizeof(RigidContact2d))) RigidContact2d(physicObjectA, physicObjectB, constraintAllocator);
		bodyContact->unmarkToDelete();
		bodyContact->updateContact(contact);
		if (dataPair[i].contacsNum > 1) {
			for (uint32_t z = 0; z < dataPair[i].contacsNum; ++z) {
				++contact;
				bodyContact->updateContact(contact);
			}
		}
		addConstraint(bodyContact);
	}

	RecycledMemoryManager::getSingleton()->releaseMemoryBlock(dataPairMem);
	RecycledMemoryManager::getSingleton()->releaseMemoryBlock(contactsMem);
}

void PhysicsManager::generateContacts()
{
	uint32_t threadsNum = 0;
	ThreadManager* threadManager = ThreadManager::getSingleton();
	if (threadManager != nullptr)
		threadsNum = threadManager->getThreadsNum();

	if (threadsNum <= 1) {
		generateContactsSingleThread();
	}
	else {
		generateContactsMultipleThread();
	}	
}

void PhysicsManager::shockPropagation()
{
	if(isShockPropagation)
	{
		// sort in biggest impact order
	}
}


Constraint2d* PhysicsManager::getConstraints( PhysicObject2d* physicObject )
{
	return constraints[physicObject->objectId];
}

void PhysicsManager::deleteConstraint( Constraint2d* constraint )
{
	uint16_t lowerId  = constraint->getLowerId();
	uint16_t higherId = constraint->getHigherId();

	//Linking index
	if(constraints[lowerId] != constraint)
	{
		if(constraint->previousA->nextA == constraint)
		{
			constraint->previousA->nextA = constraint->nextA;
		}
		else
		{
			constraint->previousA->nextB = constraint->nextA;
		}

		if(constraint->nextA != nullptr) // constraint in the middle
		{
			if(constraint->nextA->previousA == constraint)
			{
				constraint->nextA->previousA = constraint->previousA; 
			}
			else
			{
				constraint->nextA->previousB = constraint->previousA;
			}
		}	
	}
	else
	{
		constraints[lowerId] = constraint->getNext(constraint->objectA);
		if (constraints[lowerId] != nullptr) {
			if (constraints[lowerId]->previousA == constraint)
				constraints[lowerId]->previousA = nullptr;
			else
				constraints[lowerId]->previousB = nullptr;
		}
	}
	
	if(constraint->objectA != constraint->objectB)
	{
		if(constraints[higherId] != constraint)
		{
			if(constraint->previousB->nextB == constraint)
			{
				constraint->previousB->nextB = constraint->nextB;
			}
			else
			{
				constraint->previousB->nextA = constraint->nextB;
			}

			if(constraint->nextB != nullptr) // constraint in the middle
			{
				if(constraint->nextB->previousB == constraint)
				{
					constraint->nextB->previousB = constraint->previousB;
				}
				else
				{
					constraint->nextB->previousA = constraint->previousB;
				}
			}
		}
		else
		{
			constraints[higherId] = constraint->getNext(constraint->objectB);
			if (constraints[higherId] != nullptr) {
				if (constraints[higherId]->previousA == constraint)
					constraints[higherId]->previousA = nullptr;
				else
					constraints[higherId]->previousB = nullptr;
			}
		}
	}
	
	uint32_t constraintSize = constraint->sizeOf();
	constraint->removeData();
	constraintAllocator->free(constraint, constraintSize);
}

void PhysicsManager::removeConstraintLinks(Constraint2d* constraint)
{
	uint16_t lowerId  = constraint->getLowerId();
	uint16_t higherId = constraint->getHigherId();

	//Linking index
	if (constraints[lowerId] != constraint)
	{
		if (constraint->previousA->nextA == constraint)
		{
			constraint->previousA->nextA = constraint->nextA;
		}
		else
		{
			constraint->previousA->nextB = constraint->nextA;
		}

		if (constraint->nextA != nullptr) // constraint in the middle
		{
			if (constraint->nextA->previousA == constraint)
			{
				constraint->nextA->previousA = constraint->previousA;
			}
			else
			{
				constraint->nextA->previousB = constraint->previousA;
			}
		}
	}
	else
	{
		constraints[lowerId] = constraint->getNext(constraint->objectA);
	}

	if (constraint->objectA != constraint->objectB)
	{
		if (constraints[higherId] != constraint)
		{
			if (constraint->previousB->nextB == constraint)
			{
				constraint->previousB->nextB = constraint->nextB;
			}
			else
			{
				constraint->previousB->nextA = constraint->nextB;
			}

			if (constraint->nextB != nullptr) // constraint in the middle
			{
				if (constraint->nextB->previousB == constraint)
				{
					constraint->nextB->previousB = constraint->previousB;
				}
				else
				{
					constraint->nextB->previousA = constraint->previousB;
				}
			}
		}
		else
		{
			constraints[higherId] = constraint->getNext(constraint->objectB);
		}
	}
}

void PhysicsManager::deleteConstraints(PhysicObject2d* physicObject)
{
	Constraint2d* constraint   = constraints[physicObject->getId()];
	Constraint2d* itConstraint = constraint;

	while(itConstraint != nullptr)
	{
		itConstraint = constraint->getNext(physicObject);
		deleteConstraint(constraint); // delete constraint removes nextA,nextB,previousA,previousB connections
		constraint = itConstraint;
	}
}

void PhysicsManager::removeJoint(Constraint2d* joint)
{
	this->constraintsToDelete.push(joint);
}

void PhysicsManager::preStepPhase(jaFloat deltaTime)
{
	//This method is used by debugStep during simulation creation
	if(deltaTime <= GIL_ZERO)
		return;

	jaFloat invDeltaTime = GIL_ONE / deltaTime;

	for(ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
	{		
		int32_t contactsNum = itIsland->contactsNum;
		int32_t jointsNum   = itIsland->jointsNum;
		ja::Constraint2d** contacts = itIsland->contacts;
		ja::Constraint2d** joints   = itIsland->joints;

		for (int32_t i = 0; i < contactsNum; ++i)
		{
			contacts[i]->preStep(invDeltaTime);
		}

		for (int32_t i = 0; i < jointsNum; ++i)
		{
			joints[i]->preStep(invDeltaTime);
		}
	}
}

void PhysicsManager::resolveVelocityConstraints(jaFloat deltaTime)
{
	if (isWarmStart)
	{
		for (ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
		{
			int32_t contactsNum = itIsland->contactsNum;
			int32_t jointsNum   = itIsland->jointsNum;
			ja::Constraint2d** contacts = itIsland->contacts;
			ja::Constraint2d** joints   = itIsland->joints;

			for (int32_t i = 0; i < contactsNum; ++i)
			{
				contacts[i]->applyCachedImpulse();
			}

			for (int32_t i = 0; i < jointsNum; ++i)
			{
				joints[i]->applyCachedImpulse();
			}
		}
	}

	for (ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
	{
		int32_t            contactsNum = itIsland->contactsNum;
		int32_t            jointsNum   = itIsland->jointsNum;
		ja::Constraint2d** contacts    = itIsland->contacts;
		ja::Constraint2d** joints      = itIsland->joints;

		// By checking if whole island is relaxed you can effectively
		// stop applying unecessary impulses and put a group of bodies to sleep
		for (uint32_t i = 0; i < velocityIterations; ++i)
		{
			bool islandSolved = true;

			for (int32_t i = 0; i < contactsNum; ++i)
			{
				if (contacts[i]->resolveVelocity())
					islandSolved = false;
			}

			for (int32_t i = 0; i < jointsNum; ++i)
			{
				if(joints[i]->resolveVelocity())
					islandSolved = false;
			}

			if (islandSolved)
			{
				if (itIsland->isSleepingAllowed)
				{
					if (itIsland->isSleepingTresholdBelow())
						itIsland->isRelaxed = true;
				}

				break;
			}
		}
	}
}

void PhysicsManager::resolvePositionConstraints(jaFloat deltaTime)
{
	for (ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
	{
		for (uint32_t i = 0; i < positionIterations; ++i)
		{
			bool islandSolved = true;

			int32_t            jointsNum = itIsland->jointsNum;
			ja::Constraint2d** joints    = itIsland->joints;

			for (int32_t i = 0; i < jointsNum; ++i)
			{
				if (joints[i]->resolvePosition())
					islandSolved = false;
			}

			if (islandSolved)
				break;
		}
	}

	scene2d->updateDynamicObjects();
}

void PhysicsManager::resolveConstraintsSingleThread(jaFloat deltaTime)
{
	jaFloat invDeltaTime = GIL_ONE / deltaTime;

	preStepPhase(deltaTime);
	resolveVelocityConstraints(deltaTime);
	resolvePositionConstraints(deltaTime);
}

void PhysicsManager::resolveConstraintsMultipleThread(jaFloat deltaTime)
{
	jaFloat invDeltaTime = GIL_ONE / deltaTime;

	ThreadManager* threadManager = ThreadManager::getSingleton();
	threadManager->createTaskArray(constraintIsland, 0, nullptr, 0, PhysicThread::preStep, &invDeltaTime);
	threadManager->waitUntilFinish();

	threadManager->createTaskArray(constraintIsland, 0, nullptr, 0, PhysicThread::solveIslands, &invDeltaTime);
	threadManager->waitUntilFinish();
}

void PhysicsManager::resolveConstraints(jaFloat deltaTime)
{
	uint8_t        threadsNum = 0;
	ThreadManager* threadManager = ThreadManager::getSingleton();
	if (threadManager != nullptr)
		threadsNum = threadManager->getThreadsNum();

	if (threadsNum <= 1) {
		resolveConstraintsSingleThread(deltaTime);
	}
	else {
		resolveConstraintsMultipleThread(deltaTime);
	}
}

void PhysicsManager::makeIslandSleep(jaFloat deltaTime)
{
	for(ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
	{
		if(!itIsland->isSleepingAllowed)
			continue;

		//If is relaxed then increase sleep time of that island
		//And if sleep time is long enough then put whole island to sleep
		uint32_t                 islandHash  = itIsland->hash % setup::physics::MAX_ISLAND_TO_SLEEP_CACHE;
		ConstraintIslandCache2d& islandCache = islandToSleep[islandHash];

		if(islandCache.isFreeSlot) // This slot is empty there is no previous information about this island
		{
			if(itIsland->isRelaxed)
			{
				islandCache.hash = itIsland->hash;
				islandCache.sleepTime = deltaTime;
				islandCache.isFreeSlot = false;
				islandCache.exists = true;
				++sleepIslandNum;
			}	
		}
		else
		{
			if(islandCache.hash == itIsland->hash) // Hit 
			{
				islandCache.exists = true;
				if(itIsland->isRelaxed) // Increase sleep time
				{
					islandCache.sleepTime += deltaTime;
					
					if(islandCache.sleepTime >= setup::physics::ISLAND_SLEEP_TIME)
					{
						makeIslandSleep(itIsland);
						islandCache.isFreeSlot = true;
						--sleepIslandNum;
					}
				}
				else
				{
					islandCache.isFreeSlot = true; // Remove island from sleep cache
					--sleepIslandNum;
				}	
			}
		}
	}

	for(int32_t i=0; i<setup::physics::MAX_ISLAND_TO_SLEEP_CACHE; ++i)
	{
		if (islandToSleep[i].exists == false)
		{
			islandToSleep[i].isFreeSlot = true;
		}
		else {
			islandToSleep[i].exists = false;
		}
	}
}

void PhysicsManager::makePhysicObjectSleep(PhysicObject2d* physicObject)
{
	/*if (physicObject->bodyType == JA_DYNAMIC_BODY)
		scene2d->setObjectType(physicObject->shapeHandle, false);*/
	if (!physicObject->isSleeping())
	{
		if (physicObject->bodyType == (uint8_t)BodyType::DYNAMIC_BODY)
		{
			scene2d->setObjectType(physicObject->shapeHandle, false); // switch handle to static
		}
	}
}

void PhysicsManager::wakeUpPhysicObject(PhysicObject2d* physicObject)
{
	if (physicObject->getBodyType() != (uint8_t)BodyType::DYNAMIC_BODY)
		return;
	uint16_t      objectId = physicObject->getId();
	Constraint2d* constraint = constraints[objectId];

	wakeUpConnectedConstraints(constraint);
}

void PhysicsManager::makeIslandSleep(ConstraintIsland2d* island)
{
	int32_t contactsNum = island->contactsNum;
	int32_t jointsNum   = island->jointsNum;
	ja::Constraint2d** contacts = island->contacts;
	ja::Constraint2d** joints   = island->joints;

	for (int32_t i = 0; i < contactsNum; ++i)
	{
		makePhysicObjectSleep(contacts[i]->objectA);
		makePhysicObjectSleep(contacts[i]->objectB);
	}

	for (int32_t i = 0; i < jointsNum; ++i)
	{
		makePhysicObjectSleep(joints[i]->objectA);
		makePhysicObjectSleep(joints[i]->objectB);
	}

	if(constraintIsland == island)
	{
		constraintIsland = constraintIsland->next;
	}
	else
	{
		island->previous->next = island->next;

		if(island->next != nullptr)
		{
			island->next->previous = island->previous;
		}	
	}
}

void PhysicsManager::wakeUpConnectedConstraints(Constraint2d* constraint)
{
	if(constraint != nullptr)
	{
		if(constraint->objectA->isSleeping())
		{
			scene2d->setObjectType(constraint->objectA->shapeHandle,true); // switch handle to dynamic
			wakeUpConnectedConstraints(constraint->nextA);
			wakeUpConnectedConstraints(constraint->previousA);
		}

		if(constraint->objectB->isSleeping()) 
		{
			scene2d->setObjectType(constraint->objectB->shapeHandle,true); // switch handle to dynamic
			wakeUpConnectedConstraints(constraint->nextB);
			wakeUpConnectedConstraints(constraint->previousB);
		}
	}
}

void PhysicsManager::integrateSingleThread(jaFloat deltaTime)
{
	ObjectHandle2d* dynamicHandle = scene2d->getLastDynamicObject();
	jaFloat factoredTime;

	for (dynamicHandle; dynamicHandle != nullptr; dynamicHandle = dynamicHandle->prev)
	{
		PhysicObject2d* physicObject = (PhysicObject2d*)(dynamicHandle->getUserData());
		if (physicObject->isSleeping())
			continue;

		factoredTime = deltaTime * physicObject->timeFactor;

		switch (physicObject->getObjectType())
		{
		case (uint8_t)PhysicObjectType::RIGID_BODY:
		{
			RigidBody2d* dynamicBody = (RigidBody2d*)(physicObject);

			if (!dynamicBody->isFixedRotation())
			{
				dynamicBody->orientation += (factoredTime * dynamicBody->angularVelocity);
				dynamicBody->shapeHandle->transform.rotationMatrix.setRadians(dynamicBody->orientation);
			}
			dynamicBody->shapeHandle->transform.position.addScaledVector(dynamicBody->linearVelocity, factoredTime);
		}
			break;
		case (uint8_t)PhysicObjectType::DEFORMABLE_BODY:
			break;
		case (uint8_t)PhysicObjectType::PARTICLE_GENERATOR:
		{
			ParticleGenerator2d* particleGenerator = static_cast<ParticleGenerator2d*>(physicObject);
			particleGenerator->integrate(deltaTime);
		}
			break;
		case (uint8_t)PhysicObjectType::LIQUID:
			break;
		}
	}

	scene2d->updateDynamicObjects();
}

void PhysicsManager::integrateMultipleThread(jaFloat deltaTime)
{
	ThreadManager* threadManager = ThreadManager::getSingleton();
	//threadManager->createTaskArray(&physicObjects, scene2d->getHighestGeneratedId(), NULL, 0, jaPhysicThread::integrate, &deltaTime);
	threadManager->createTaskArray(scene2d->getLastDynamicObject(), 0, nullptr, 0, PhysicThread::integrate, &deltaTime);
	threadManager->waitUntilFinish();

	//scene2d->updateDynamicObjectsMultiThread();
	scene2d->updateDynamicObjects();
}

void PhysicsManager::integrate(jaFloat deltaTime)
{
	uint8_t        threadsNum = 0;
	ThreadManager* threadManager = ThreadManager::getSingleton();
	if (threadManager != nullptr)
		threadsNum = threadManager->getThreadsNum();

	if (threadsNum <= 1) {
		integrateSingleThread(deltaTime);
	}
	else {
		integrateMultipleThread(deltaTime);
	}
}

void PhysicsManager::clearForcesSingleThread()
{
	ObjectHandle2d* dynamicHandle = scene2d->getLastDynamicObject();

	for (dynamicHandle; dynamicHandle != nullptr; dynamicHandle = dynamicHandle->prev)
	{
		PhysicObject2d* physicObject = (PhysicObject2d*)dynamicHandle->getUserData();

		switch (physicObject->getObjectType())
		{
		case (uint8_t)PhysicObjectType::RIGID_BODY:
		{
			RigidBody2d* dynamicBody = (RigidBody2d*)physicObject;
			dynamicBody->netForce.setZero();
			dynamicBody->torque = GIL_ZERO;
		}
			break;
		}
	}
}

void PhysicsManager::clearForcesMultipleThread()
{
	ThreadManager* threadManager = ThreadManager::getSingleton();
	//threadManager->createTaskArray(&physicObjects, scene2d->getHighestGeneratedId(), NULL, 0, jaPhysicThread::clearForces, NULL);
	threadManager->createTaskArray(scene2d->getLastDynamicObject(), 0, nullptr, 0, PhysicThread::clearForces, nullptr);
	threadManager->waitUntilFinish();
}

void PhysicsManager::clearForces()
{
	uint8_t        threadsNum = 0;
	ThreadManager* threadManager = ThreadManager::getSingleton();
	if (threadManager != nullptr)
		threadsNum = threadManager->getThreadsNum();

	if (threadsNum <= 1) {
		clearForcesSingleThread();
	}
	else {
		clearForcesMultipleThread();
	}
}

void PhysicsManager::drawHashGrid(CameraInterface* camera)
{
	Debug::drawHashGrid(camera,(HashGrid*)scene2d,true);
}

void PhysicsManager::deleteBodyImmediate(RigidBody2d* body)
{
	if(constraints[body->objectId] != nullptr)
	{
		deleteConstraints(body);
	}
	
	scene2d->removeObject(body->shapeHandle);

	physicObjects[body->objectId] = nullptr;
	physicsBodyAllocator->free(body, sizeof(RigidBody2d));
}

void PhysicsManager::deleteParticleGeneratorImmediate(ParticleGenerator2d* particleGenerator)
{
	particleGenerator->clearEmitter();
	scene2d->removeObject(particleGenerator->shapeHandle);

	particleGenerator->~ParticleGenerator2d();
	physicObjects[particleGenerator->objectId] = nullptr;
	physicsBodyAllocator->free(particleGenerator,sizeof(ParticleGenerator2d));
}

void PhysicsManager::deletePhysicObjectImmediate(PhysicObject2d* physicObject)
{
	if(physicObject->componentData != nullptr)
	{
		PhysicComponent* component = (PhysicComponent*)(physicObject->componentData);
		component->setPhysicObject(nullptr);
	}

	switch(physicObject->objectType)
	{
	case (uint8_t)PhysicObjectType::RIGID_BODY:
		deleteBodyImmediate((RigidBody2d*)(physicObject)); 
		break;
	case (uint8_t)PhysicObjectType::PARTICLE_GENERATOR:
		deleteParticleGeneratorImmediate((ParticleGenerator2d*)(physicObject));
		break;
	}
}

void PhysicsManager::changeBodyShape(RigidBody2d* body, Shape2d* shape)
{
	scene2d->changeObjectShape(body->shapeHandle,shape);
	body->shapeHandle->shape->setParent(body->shapeHandle);
	body->resetMassData();
}

void PhysicsManager::changeBodyType(PhysicObject2d* physicObject, BodyType bodyType)
{
	switch(bodyType)
	{
	case BodyType::STATIC_BODY:
		scene2d->setObjectType(physicObject->shapeHandle,false);
		break;
	case BodyType::DYNAMIC_BODY:
		if(physicObject->isSleeping())
			scene2d->setObjectType(physicObject->shapeHandle,false);
		else
			scene2d->setObjectType(physicObject->shapeHandle,true);
		break;
	case BodyType::KINEMATIC_BODY:
		if(physicObject->isSleeping())
			scene2d->setObjectType(physicObject->shapeHandle,false);
		else
			scene2d->setObjectType(physicObject->shapeHandle,true);
		break;
	}

	physicObject->bodyType = (uint8_t)bodyType;
	physicObject->updateBodyTypeChange();
}

void PhysicsManager::getBodies(std::list<RigidBody2d*>* bodiesList)
{
	for(uint32_t i=0; i< setup::physics::MAX_PHYSIC_OBJECT; ++i)
	{
		if(physicObjects[i] != nullptr)
		{
			if (physicObjects[i]->objectType == (uint8_t)PhysicObjectType::RIGID_BODY)
			{
				bodiesList->push_back((RigidBody2d*)physicObjects[i]);
			}
		}
	}
}

void PhysicsManager::getParticles(std::list<ParticleGenerator2d*>* particlesList)
{
	for(uint32_t i=0; i< setup::physics::MAX_PHYSIC_OBJECT; ++i)
	{
		if(physicObjects[i] != nullptr)
		{
			if (physicObjects[i]->getObjectType() == (uint8_t)PhysicObjectType::PARTICLE_GENERATOR)
			{
				particlesList->push_back((ParticleGenerator2d*)physicObjects[i]);
			}	
		}
	}
}

void PhysicsManager::clearPhysicObjects()
{
	for(uint32_t i=0; i< setup::physics::MAX_PHYSIC_OBJECT; ++i)
	{
		if(physicObjects[i] != nullptr)
		{
			switch(physicObjects[i]->getObjectType())
			{
			case (uint8_t)PhysicObjectType::RIGID_BODY:
				deleteBodyImmediate((RigidBody2d*)(physicObjects[i]));
				break;
			case (uint8_t)PhysicObjectType::PARTICLE_GENERATOR:
				deleteParticleGeneratorImmediate((ParticleGenerator2d*)(physicObjects[i]));
				break;
			}

			physicObjects[i] = nullptr;
		}
	}

	scene2d->clearIdGenerator();
}

void PhysicsManager::clearConstraints()
{
	for(uint32_t i=0; i< setup::physics::MAX_PHYSIC_OBJECT; ++i)
	{
		if(physicObjects[i] != nullptr)
		{
			if(constraints[i] != nullptr)
			{
				deleteConstraints(physicObjects[i]);
			}
		}
	}

	constraintAllocator->clear();
}

void PhysicsManager::clearIslandsData()
{
	for(int32_t i=0; i<setup::physics::MAX_ISLAND_TO_SLEEP_CACHE; ++i)
	{
		islandToSleep[i].isFreeSlot = true;
		islandToSleep[i].hash = 0;
		islandToSleep[i].sleepTime = 0.0f;
		islandToSleep[i].exists = false;
	}

	sleepIslandNum = 0;
	islandAllocator->clear();
	constraintIsland = nullptr;
	constraintIslandCounter = 0;

	contactList.clear();
}

void PhysicsManager::clearMem()
{
	physicsBodyAllocator->clear();
	sceneAllocator->clear();
	temporaryShapeAllocator->clear();

	this->physicObjectDef.init(this);
}

void PhysicsManager::clearWorld()
{
	clearPhysicObjects();
	clearConstraints();
	clearIslandsData();
	clearMem(); // Make new mem clean and unfragmented
}

void PhysicsManager::getPhysicObjectsAtPoint( const jaVector2& point, StackAllocator<ObjectHandle2d*>* objectList)
{
	scene2d->getObjectsAtPoint(objectList,point);
}

void PhysicsManager::getPhysicObjectsAtAABB( const AABB2d& aabb, StackAllocator<ObjectHandle2d*>* objectList )
{
	scene2d->getObjectsAtRectangle(objectList, aabb);
}

void PhysicsManager::getPhysicObjectsAtShape( const jaTransform2& transform, Shape2d* shape, StackAllocator<ObjectHandle2d*>* objectList)
{
	scene2d->getObjectsAtShape(objectList, transform, shape);
}

RigidBody2d* PhysicsManager::createBody(Shape2d* shape, BodyType bodyType, const jaTransform2& transform, bool isFixedRotation, bool isSleeping, uint16_t bodyId)
{
	ObjectHandle2d* handle = nullptr;
	RigidBody2d*    body   = nullptr;

	handle = createObjectOfType(bodyType, shape, transform, isSleeping);

	body =  new (physicsBodyAllocator->allocate(sizeof(RigidBody2d))) RigidBody2d();
	handle->setUserData(body);
	body->shapeHandle = handle;
	body->orientation = transform.rotationMatrix.getRadians();
	body->bodyType    = (uint8_t)bodyType;
	body->setFixedRotation(isFixedRotation);

	body->resetMassData();

	if(bodyId == setup::physics::ID_UNASSIGNED)
	{
		bodyId = handle->getId();
	}

	body->objectId = bodyId;
	body->constraintSynchronizer = constraintSynchronizer - 2;
	physicObjects[bodyId] = body;
 
	return body;
}

RigidBody2d* PhysicsManager::createBodyDef(uint16_t bodyId)
{
	RigidBody2d* body;

	body = createBody(physicObjectDef.getShape(), physicObjectDef.bodyType, physicObjectDef.transform, physicObjectDef.fixedRotation, physicObjectDef.isSleeping, bodyId);

	body->setGravity(physicObjectDef.gravity);
	body->linearDamping = physicObjectDef.linearDamping;
	body->angularDamping = physicObjectDef.angluarDamping;
	assert(physicObjectDef.timeFactor <= GIL_ONE); // for stabilization
	body->timeFactor = physicObjectDef.timeFactor;
	body->setLinearVelocity(physicObjectDef.linearVelocity);
	body->setAngularVelocity(physicObjectDef.angluarVelocity);
	body->setGroupIndex(physicObjectDef.groupIndex);
	body->setCategoryMask(physicObjectDef.categoryMask);
	body->setMaskBits(physicObjectDef.maskBits);
	body->setAllowSleep(physicObjectDef.allowSleep);

	return body;
}


Constraint2d* PhysicsManager::createConstraintDef()
{
	if ((constraintDef.objectA->bodyType != (uint8_t)BodyType::DYNAMIC_BODY) && constraintDef.objectB->bodyType != (uint8_t)BodyType::DYNAMIC_BODY)
		return nullptr; // It is not possible and even meaningless to create static-kinematic or kinematic-kinematic constraints

	Constraint2d* constraint = nullptr;
	constraint = constraintCreator[(int32_t)(constraintDef.constraintType)](constraintAllocator,&constraintDef);
	addConstraint(constraint);

	return constraint;
}

ParticleGenerator2d* PhysicsManager::createParticleDef( uint16_t particleId /*= JA_ID_UNASSIGNED*/ )
{
	ObjectHandle2d*      handle = nullptr;
	ParticleGenerator2d* particleGenerator = new (physicsBodyAllocator->allocate(sizeof(ParticleGenerator2d))) ParticleGenerator2d(this->sceneAllocator);
	RigidBody2d*         emitter = (RigidBody2d*)(physicObjects[particleDef.emitterId]);

	particleGenerator->setParticleDef(particleDef);
	Particle2d* particle = particleGenerator->particles;
	Particles2d particleShape(particleDef.maxParticlesNum,particle,particleDef.isColliding,emitter->objectId);
	handle = scene2d->createDynamicObject(&particleShape,emitter->shapeHandle->transform);

	particleGenerator->shapeHandle = handle;
	handle->setUserData(particleGenerator);

	//particleGenerator->init();
	particleGenerator->clearParticlesLife();
	particleGenerator->setEmmiter(emitter);

	particleGenerator->timeFactor = particleDef.timeFactor;
	particleGenerator->setGroupIndex(particleDef.groupIndex);
	particleGenerator->setCategoryMask(particleDef.categoryBits);
	particleGenerator->setMaskBits(particleDef.maskBits);

	if(particleId == setup::physics::ID_UNASSIGNED)
	{
		particleId = handle->getId();
	}

	particleGenerator->objectId = particleId;
	particleGenerator->constraintSynchronizer = constraintSynchronizer - 2;
	physicObjects[particleId] = particleGenerator;

	return particleGenerator;
}

void PhysicsManager::rayIntersection( jaVector2& ray0, jaVector2& ray1, StackAllocator<RayContact2d>* rayContacts)
{
	scene2d->rayIntersection(ray0,ray1,rayContacts); // It doesn't work yet
}

void PhysicsManager::generateConstraintIslandsSingleThread()
{
	PhysicObject2d* dynamicBody;
	constraintIsland = nullptr;
	constraintIslandCounter = 0;
	islandAllocator->clear(); //Clear island allocator

	ObjectHandle2d* dynamicHandle = scene2d->getLastDynamicObject();

	for (dynamicHandle; dynamicHandle != nullptr; dynamicHandle = dynamicHandle->prev)
	{
		dynamicBody = (PhysicObject2d*)dynamicHandle->getUserData(); // only dynamic bodies if sleeping aren't in static hashBucket

		if (dynamicBody->getBodyType() != (uint8_t)BodyType::DYNAMIC_BODY)
			continue; //Because there is no kinematic-kinematic or kinematic-static constraints it is no use to check kinematic body
		//constraints if dynamic body will be checked later

		Constraint2d* constraint = constraints[dynamicBody->objectId];
		if (constraint == nullptr)
		{
			dynamicBody->constraintSynchronizer = constraintSynchronizer - 1; // if not in collision with anything keep synchronization near to avoid some rare artifacts
			continue;
		}

		if (constraintIsland == nullptr)
		{
			constraintIsland = addToConstraintIsland(constraint, nullptr);
			if (!dynamicBody->isSleepingAllowed())
			{
				constraintIsland->isSleepingAllowed = false;
			}
		}
		else
		{
			ConstraintIsland2d* tempIsland = addToConstraintIsland(constraint, nullptr);

			if (tempIsland == nullptr)
				continue; // Constraint already synchronized

			if (!dynamicBody->isSleepingAllowed())
			{
				tempIsland->isSleepingAllowed = false;
			}

			if (tempIsland != constraintIsland)
			{
				if (tempIsland->previous == nullptr) // Island not added to constraint island list
				{
					constraintIsland->previous = tempIsland;
					tempIsland->next = constraintIsland;
					constraintIsland = tempIsland; // set it as first constraint island
				}
			}
		}
	} // for end

	for (ja::ConstraintIsland2d* island = constraintIsland; island != nullptr; island = island->next) // group joints and contacts
	{
		int32_t contactsNum = island->contactsNum;
		int32_t jointsNum   = island->jointsNum;

		island->contacts = (Constraint2d**)islandAllocator->allocate( sizeof(Constraint2d*) * contactsNum );
		island->joints   = (Constraint2d**)islandAllocator->allocate( sizeof(Constraint2d*) * jointsNum   );

		int32_t contactIndex = 0;
		int32_t jointIndex  = 0;

	    ConstraintList2d* constraintElement = island->firstElement;
		for (constraintElement; constraintElement != nullptr; constraintElement = constraintElement->next)
		{
			if (constraintElement->constraint->getConstraintType() == JA_RIGID_BODY_CONTACT2D)
				island->contacts[contactIndex++] = constraintElement->constraint;
			else
				island->joints[jointIndex++]     = constraintElement->constraint;
		}
	}
}

void PhysicsManager::generateConstraintIslands()
{
	ThreadManager* threadManager = ThreadManager::getSingleton();
	uint8_t        threadsNum = 0;
	if (threadManager != nullptr)
		threadsNum = threadManager->getThreadsNum();

	if (threadsNum <= 1) {
		generateConstraintIslandsSingleThread();
	}
	else {
		//threadManager->createTaskArray(&constraints, scene2d->getHighestGeneratedId(), NULL, 0, jaPhysicThread::generateConstraintIslands);
		//threadManager->setTaskArrayMode();
		//threadManager->waitUntilFinish();
		generateConstraintIslandsSingleThread();
	} // else end
	
}

void PhysicsManager::removeOldConstraints()
{
	//Remove marked joints
	uint32_t jointsToDeleteNum = this->constraintsToDelete.getSize();
	ja::Constraint2d** joints  = this->constraintsToDelete.getFirst();
	for (uint32_t i = 0; i < jointsToDeleteNum; ++i)
	{
		deleteConstraint(joints[i]);
	}
	this->constraintsToDelete.clear();

	//Remove old contacts
	for (ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
	{
		int32_t contactsNum = itIsland->contactsNum;
		ja::Constraint2d** contacts = itIsland->contacts;

		for (int32_t i = 0; i < contactsNum; ++i)
		{
			if (contacts[i]->isMarkedToDelete())
			{
				deleteConstraint(contacts[i]); // it removes nextA, nextB, previousA, previousB connections
				continue;
			}

			contacts[i]->markToDelete();
			contacts[i]->markAndDeleteOldData();
		}
	}
}

ConstraintIsland2d* PhysicsManager::addToConstraintIsland(Constraint2d* constraint, ConstraintIsland2d* island)
{
	if(constraint != nullptr)
	{
		if(constraint->constraintSynchronizer != constraintSynchronizer) // constraint not assigned to island
		{
			//if returns null then objectA and objectB is not added to any island yet
			//it doesn't check for island in static or kinematic body

			island = getUnitedIsland(constraint,constraintSynchronizer);
			if(island == nullptr)
				island = new (islandAllocator->allocate(sizeof(ConstraintIsland2d))) ConstraintIsland2d(constraintIslandCounter++);
			
			
			island->addConstraint(islandAllocator,constraint);
			constraint->synchronizeConstraint(constraintSynchronizer); // This sets information about body already assigned to island

			if(constraint->isEndpoint()) // static - dynamic, kinematic - dynamic
			{
				if(constraint->objectA->bodyType == (uint8_t)BodyType::DYNAMIC_BODY)
				{
					island = addToConstraintIsland(constraint->nextA, island);
				}
				else
				{
					island = addToConstraintIsland(constraint->nextB, island);
				}
			}
			else // dynamic - dynamic
			{
				island = addToConstraintIsland(constraint->nextA, island);
				island = addToConstraintIsland(constraint->nextB, island);
			}
		}
	}

	return island;
}

void PhysicsManager::addConstraint( Constraint2d* constraint ) {
	uint16_t lowerId  = constraint->getLowerId();
	uint16_t higherId = constraint->getHigherId();

	//Update lower id index
	constraint->nextA = constraints[lowerId];

	if(constraints[lowerId] != nullptr) {
		if(constraints[lowerId]->objectA == constraint->objectA) // previousA is index for lowerId
		{
			constraints[lowerId]->previousA = constraint;
		} else // previousB is index for lower id
		{
			constraints[lowerId]->previousB = constraint;
		}
	}
	constraints[lowerId] = constraint;

	//Update higher id index
	constraint->nextB = constraints[higherId];

	if(constraints[higherId] != nullptr)
	{
		if(constraints[higherId]->objectB == constraint->objectB) // previousB is index for higherId
		{
			constraints[higherId]->previousB = constraint;
		}
		else // previousA is index for higher id
		{
			constraints[higherId]->previousA = constraint;
		}
	}
	constraints[higherId] = constraint;
	constraint->constraintSynchronizer = constraintSynchronizer - 1; // To avoid synchronization of contact before constraint 
	// island generation
}

void PhysicsManager::drawDebug(CameraInterface* camera)
{
	PROFILER_FUN;

	AABB2d camBound;
	char   idNumber[5];
	float scale = DisplayManager::getHeightMultiplier();
	Font* fontRender = ResourceManager::getSingleton()->getEternalFont("debug.ttf",12)->resource;
	camera->getBoundingBox(&camBound);

	debugList.clear();
	scene2d->getObjectsAtRectangle(&debugList,camBound);

	ObjectHandle2d** list = debugList.getFirst();
	ObjectHandle2d* object;
	RigidBody2d*    body;
	PhysicObject2d* physicObject;
	//jaVector2       position;
	//jaTransform2    transform;
	int32_t size = debugList.getSize();

	DisplayManager::setCameraMatrix(camera);

	if(drawHashCells)
	{
		drawHashGrid(camera);
	}

	if(drawAABB)
	{
		DisplayManager::setColorUb(0,255,0);
		for(int32_t i=0; i<size; ++i)
		{
			object = list[i];
			Debug::drawAABB2d(object->volume);
		}
	}

	if(drawShapes)
	{
		DisplayManager::setColorUb(255,0,0);
		for(int32_t i=0; i<size; ++i)
		{
			object = list[i];
			physicObject = (PhysicObject2d*)object->getUserData();

			if(physicObject->isSleeping() && (physicObject->bodyType != (uint8_t)BodyType::STATIC_BODY))
			{
				DisplayManager::setLineWidth(2.0);
				DisplayManager::setColorUb(245,30,100);
				Debug::drawObjectHandle2d(object,object->transform);
				DisplayManager::setLineWidth(1.0);
			}

			switch(physicObject->bodyType)
			{
			case (uint8_t)BodyType::KINEMATIC_BODY:
				DisplayManager::setColorUb(0,30,255);
				break;
			case (uint8_t)BodyType::DYNAMIC_BODY:
				DisplayManager::setColorUb(255,0,0);
				break;
			case (uint8_t)BodyType::STATIC_BODY:
				DisplayManager::setColorUb(128,128,128);
				break;
			}
			Debug::drawObjectHandle2d(object,object->transform);

			if (physicObject->getObjectType() == (uint8_t)PhysicObjectType::RIGID_BODY)
			{
				RigidBody2d* rigidBody = (RigidBody2d*)physicObject;

				DisplayManager::pushWorldMatrix();
				DisplayManager::translateWorldMatrix(object->transform.position.x, object->transform.position.y, 0);
				DisplayManager::rotateWorldMatrix(object->transform.rotationMatrix);

				//jaVector2 centerOfMass = rigidBody->shapeHandle->transform.rotationMatrix * rigidBody->localCenterOfMass;
				jaVector2 centerOfMass = rigidBody->localCenterOfMass;
				DisplayManager::setPointSize(4.0f);
				DisplayManager::setColorUb(255, 255, 255);
				DisplayManager::drawPoint(centerOfMass.x, centerOfMass.y);
				DisplayManager::setPointSize(2.0f);
				DisplayManager::setColorUb(0, 0, 0);
				DisplayManager::drawPoint(centerOfMass.x, centerOfMass.y);
				DisplayManager::popWorldMatrix();
			}

			if(drawId)
			{
#pragma warning(disable : 4996)
				sprintf(idNumber, "%u", object->id);
#pragma warning(default : 4996)
				DisplayManager::RenderTextWorld(fontRender, object->transform.position.x, object->transform.position.y, scale, idNumber, (uint32_t)Margin::LEFT, 15, 70, 0, 255);
			}

			if(drawUser)
			{
				drawUser(physicObject);
			}
		}
	}

	if(drawVelocities)
	{
		DisplayManager::setColorUb(255,255,0);

		for(object = scene2d->getLastDynamicObject(); object != nullptr; object = object->prev)
		{
			physicObject = (PhysicObject2d*)object->getUserData();
			if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
				continue;
			body = (RigidBody2d*)physicObject;
			jaVector2 position = object->transform.position;
			DisplayManager::drawLine(position.x,position.y,position.x + body->linearVelocity.x,position.y + body->linearVelocity.y);
		}
	}

	if(drawConstraints)
	{
		for(ConstraintIsland2d* itIsland = constraintIsland; itIsland != nullptr; itIsland = itIsland->next)
		{
			Constraint2d** contacts = itIsland->contacts;
			int32_t contactsNum = itIsland->contactsNum;
			Constraint2d** joints   = itIsland->joints;
			int32_t jointsNum   = itIsland->jointsNum;
			

			for (int32_t i = 0; i < contactsNum; ++i)
			{
				contacts[i]->draw();
			}

			for (int32_t i = 0; i < jointsNum; ++i)
			{
				joints[i]->draw();
			}
		}
	}
}

void PhysicsManager::printHashes()
{
	HashGrid* hashGrid = static_cast<HashGrid*>(scene2d);
#ifdef GILDEBUG
	hashGrid->printObjectHashBuckets();
#endif
}

void PhysicsManager::printConstraintIslands()
{
	std::cout << "Constraint islands:" << std::endl;

	for(ConstraintIsland2d* island = constraintIsland; island != nullptr; island = island->next)
	{
		std::cout << "Island ID " << island->id << "--------------------" << std::endl;
		std::cout << "hash = " << island->hash << std::endl;

		for(ConstraintList2d* constraintElement = island->firstElement; constraintElement != nullptr; constraintElement = constraintElement->next)
		{
			Constraint2d* constraint = constraintElement->constraint;
			std::cout << "Constraint LOWER ID " << constraint->getLowerId() << " HIGHER ID " << constraint->getHigherId() << std::endl;
			std::cout << "hash = " << constraint->hash << std::endl;
			std::cout << "endpoint = " << (constraint->isEndpoint() ? "true" : "false") << std::endl; 
		}
	}
}

void PhysicsManager::printConstraints()
{
	std::cout << "Constraint list:" << std::endl;
	for(int32_t i=0; i<setup::physics::MAX_PHYSIC_OBJECT; ++i)
	{
		if(constraints[i] != nullptr)
		{
			PhysicObject2d* conObject = physicObjects[i];
			std::cout << "C[" << i << "]=";
			for(Constraint2d* itConstraint = constraints[i]; itConstraint != nullptr; itConstraint = itConstraint->getNext(conObject))
			{
				if(itConstraint->isEndpoint())
				{
					std::cout << "E";
				}

				std::cout << itConstraint->objectA->objectId << "-" << itConstraint->objectB->objectId << "|";
			}
			std::cout << std::endl;
		}
	}
}

void PhysicsManager::printIsland(ConstraintIsland2d* island)
{
	std::cout << "Island id=" << island->id << std::endl;
	std::cout << "hash" << island->hash << std::endl;
	std::cout << "first element=C" << island->firstElement->constraint->getLowerId() << "-" << island->firstElement->constraint->getHigherId() << std::endl;
	std::cout << "last element=C" << island->lastElement->constraint->getLowerId() << "-" << island->lastElement->constraint->getHigherId() << std::endl;

	ConstraintList2d* constraintElement = island->firstElement;

	std::cout << "Constraints" << std::endl;
	for(constraintElement; constraintElement != nullptr; constraintElement = constraintElement->next)
	{
		std::cout << "constraint="; 
		if(constraintElement->constraint->isEndpoint())
		{
			std::cout << "E";
		}
		std::cout << constraintElement->constraint->getLowerId() << "-" << constraintElement->constraint->getHigherId() << std::endl;
		std::cout << "synchronizerA=" << constraintElement->constraint->objectA->constraintSynchronizer << std::endl;
		std::cout << "synchronizerB=" << constraintElement->constraint->objectB->constraintSynchronizer << std::endl;
		std::cout << "constraintSynchronizer=" << constraintElement->constraint->constraintSynchronizer << std::endl;
		std::cout << "islandA=" << constraintElement->constraint->objectA->island->id << std::endl;
		std::cout << "islandB=" << constraintElement->constraint->objectB->island->id << std::endl;
	}	
}

}

