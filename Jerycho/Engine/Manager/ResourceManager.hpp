#pragma  once

#include "../Font/Font.hpp"

#include "../Utility/String.hpp"

#include "../Allocators/LinearAllocator.hpp"
#include "../Allocators/FastAllocatorSTL.hpp"
#include "../Graphics/Texture2d.hpp"
#include "../Graphics/Shader.hpp"
#include "../Graphics/ShaderBinder.hpp"
#include "../Graphics/ShaderStructureDef.hpp" 
#include "../Graphics/Material.hpp"
#include "../Sounds/SoundEffect.hpp"
#include "../Sounds/SoundStream.hpp"
#include "../Videos/VideoStream.hpp"
#include "../Scripts/Script.hpp"
#include "../Entity/DataGroup.hpp"

#include "../Allocators/HashMap.hpp"

#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <thread>

namespace ja
{

	template <class T>
	class Resource
	{
	public:
		T*         resource;
		ja::string resourceName;

		Resource() : resource(nullptr)
		{

		}

		Resource(T* resource, const char* resourceName)
		{
			this->resourceName = resourceName;
			this->resource = resource;
		}

		~Resource()
		{
			if (resource != nullptr)
			{
				delete resource; 
				resource = nullptr;
			}
		}
	};

	class ResourceManager;

	class ResourceLoader
	{
	private:
		volatile bool isRunning;
	    std::thread resourceLoaderThread;

		ResourceManager* pResourceManager;

		void execute();
	public:

		ResourceLoader();
		void run(ResourceManager* pResourceManager);
		void stop();	

		friend int32_t resourceLoaderFunction(ResourceLoader* pResourceLoader);
	};

	static int32_t resourceLoaderFunction(ResourceLoader* pResourceLoader)
	{
		pResourceLoader->execute();

		return 0;
	}

	class ResourceManager
	{
	private:
		static ResourceManager* singleton;

		ResourceLoader resourceLoader;
		
		//game resources
		HashMap<Resource<Texture2d>>   eternalTextures2d;
		HashMap<Resource<Font>>        eternalFonts;
		HashMap<Resource<FontPolygon>> eternalFontPolygons;
		HashMap<Resource<SoundEffect>> eternalSoundEffects;
		HashMap<Resource<SoundStream>> eternalSoundStreams;
		//game resources

		//level resources
		HashMap<Resource<Texture2d>>          textures2d;
		HashMap<Resource<Shader>>             shaders;
		HashMap<Resource<ShaderBinder>>       shaderBinders;
		HashMap<Resource<SoundEffect>>        soundEffects;
		HashMap<Resource<SoundStream>>        soundStreams;
		HashMap<Resource<Script>>             scripts; 
		HashMap<Resource<DataModel>>          dataModels;
		HashMap<Resource<ShaderStructureDef>> materialShaderDefs;
		HashMap<Resource<ShaderStructureDef>> shaderObjectDefs;
		HashMap<Resource<Material>>           materials;
		//level resources

		CacheLineAllocator* resourceAllocator;

		static const char fontsPath[];
		static const char soundStreamsPath[];
		static const char scriptsPath[];
		static const char shadersPath[];
		static const char soundEffectsPath[];
		static const char texturesPath[];

		ja::string resourcePath;

		ResourceManager();
	public:

		~ResourceManager();
		static void init();
		static inline ResourceManager* getSingleton()
		{
			return singleton;
		}
		void setResourcePath(const char* resourcePath);
		Resource<Texture2d>*   loadEternalTexture2d(const char* fileName);
		Resource<Font>*        loadEternalFont(const char* fileName, uint32_t size, FontEncode encode);
		Resource<FontPolygon>* loadEternalFontPolygon(const char* fileName, FontEncode encode);
		Resource<SoundEffect>* loadEternalSoundEffect(const char* fileName);
		Resource<SoundStream>* loadEternalSoundStream(const char* fileName);

		Resource<Texture2d>*   getEternalTexture2d(const char* fileName);
		Resource<Texture2d>*   getEternalTexture2d(uint32_t resourceId);
		Resource<SoundEffect>* getEternalSoundEffect(uint32_t resourceId);
		Resource<SoundStream>* getEternalSoundStream(uint32_t resourceId);
		Resource<Font>*        getEternalFont(const char* fileName, uint32_t size);
		Resource<Font>*        getEternalFont(uint32_t resourceId);
		Resource<FontPolygon>* getEternalFontPolygon(const char* fileName);
		Resource<FontPolygon>* getEternalFontPolygon(uint32_t resourceId);

		//level resources
		Resource<Texture2d>*          loadTexture2d(const char* fileName);
		Resource<Shader>*             loadShader(const char* fileName, ShaderType shaderType);
		Resource<ShaderBinder>*       createShaderBinder(const char* shaderBinderName, const char* vertexShaderName, const char* fragmentShaderName, const char* geometryShaderName);
		Resource<SoundEffect>*        loadSoundEffect(const char* fileName);
		Resource<SoundStream>*        loadSoundStream(const char* fileName);
		Resource<Script>*             addScript(const char* fileName);
		Resource<DataModel>*          addDataModel(const char* dataModelName, DataModel* dataModel);
		Resource<ShaderStructureDef>* addMaterialShaderDef(const char* materialShaderDefName, ShaderStructureDef* materialStructure);
		Resource<ShaderStructureDef>* addShaderObjectDef(const char* shaderObjetDefName, ShaderStructureDef* shaderObjectStructure);
		Resource<Material>*           createMaterial(const char* materialName, ShaderStructureDef* materialStructure);

		Resource<Script>* isLevelScriptLoaded(const char* fileName);
		void getInScriptPath(ja::string& fileInScriptPath, bool& isInScriptPath);

		Resource<Texture2d>*          getTexture2d(uint32_t resourceId);
		Resource<Shader>*             getShader(const char* fileName);
		Resource<Shader>*             getShader(uint32_t resourceId);
		Resource<ShaderBinder>*       getShaderBinder(const char* shaderBinderName);
		Resource<ShaderBinder>*       getShaderBinder(uint32_t resourceId);
		Resource<SoundEffect>*        getSoundEffect(uint32_t resourceId);
		Resource<Script>*             getScript(uint32_t resourceId);
		Resource<SoundStream>*        getSoundStream(uint32_t resourceId);
		Resource<DataModel>*          getDataModel(uint32_t resourceId);
		Resource<DataModel>*          getDataModel(const char* modelName);
		Resource<Material>*           getMaterial(uint32_t resourceId);
		Resource<Material>*           getMaterial(const char* materialName);
		Resource<ShaderStructureDef>* getMaterialShaderDef(uint32_t resourceId);
		Resource<ShaderStructureDef>* getMaterialShaderDef(const char* materialCoreName);
		Resource<ShaderStructureDef>* getShaderObjectDef(uint32_t resourceId);
		Resource<ShaderStructureDef>* getShaderObjectDef(const char* shaderObjectDefName);

		HashMap<Resource<Texture2d>>*    getTextures2d();
		HashMap<Resource<SoundEffect>>*  getSoundEffects();
		HashMap<Resource<SoundStream>>*  getSoundStreams();
		HashMap<Resource<Shader>>*       getShaders();
		HashMap<Resource<ShaderBinder>>* getShaderBinders();
		HashMap<Resource<Script>>*       getScripts();
		HashMap<Resource<DataModel>>*    getDataModels();
		void clearResources();
		//level resources

		void getTextures2d(std::vector<Resource<Texture2d>*>* textureList);

		ja::string  getResourcesPath();
		const char* getTexturesPath();
		const char* getFontsPath();
		const char* getShadersPath();
		const char* getSoundEffectsPath();
		const char* getSoundStreamPath();
		const char* getScriptsPath();

		//Used when resource memory allocator was used
		template <typename T>
		void clearResourcesMap(HashMap<T>* resourceList)
		{
			HashEntry<T>*   hashEntries = resourceList->getFirst();
			uint32_t size = resourceList->getSize();

			for (uint32_t i = 0; i < size; ++i)
			{
				delete hashEntries[i].data;
			}

			resourceList->clear();
		}

		inline CacheLineAllocator* getResourceAllocator()
		{
			return this->resourceAllocator;
		}

		static void cleanUp();
	};

}
