#pragma once

#include <stdio.h>
#include <string>
#include <list>
#include <tinyxml2/tinyxml2.h>
#include "../Graphics/Polygon.hpp"
#include "../Graphics/Layer.hpp"
#include "../Graphics/Fbo.hpp"
#include "../Physics/RigidBody2d.hpp"
#include "../Entity/Entity.hpp"
#include "ResourceManager.hpp" // proxies
#include <FastDelegate.h>

#include "../Entity/PhysicGroup.hpp"

namespace ja
{
	/*namespace tag {
		//Variable names
		const char* const ID_VARIABLE = "id";
		const char* const NAME_VARIABLE = "name";
		const char* const TYPE_VARIABLE = "type";
		const char* const GROUP_VARIABLE = "group";
		const char* const SUBGROUP_VARIABLE = "subGroup";
		const char* const FILE_VARIABLE = "file";
		const char* const FUNCTION_VARIABLE = "function";
		const char* const SPRITE_ENTIY_TYPE_VARIABLE = "spriteType";
		const char* const POSITION_VARIABLE = "position";
		const char* const TEXTURE_SCALE_VARIABLE = "texScale";
		const char* const SCALE_VARIABLE = "scale";
		const char* const SIZE_VARIABLE = "size";
		const char* const ANGLE_VARIABLE = "angle";
		const char* const DEPTH_VARIABLE = "depth";
		const char* const LAYER_VARIABLE = "layer";
		const char* const TEXTUREID_VARIABLE = "textureId";
		const char* const ALPHA_VARIABLE = "alpha";
		const char* const TEXTURE_REPEAT_VARIABLE = "texRepeat";
		const char* const TEXTURE_FILTER_VARIABLE = "texFilter";
		const char* const ELAPSED_TIME_VARIABLE = "elapsedTime";
		const char* const LOOP_VARIABLE = "loop";
		const char* const ANIMATIONID_VARIABLE = "animationId";
		const char* const SEQUENCEID_VARIABLE = "sequenceId";
		const char* const SPECIAL_VARIABLE = "special";
		const char* const COLOR_VARIABLE = "color";
		const char* const EXTID_VARIABLE = "extId";
		const char* const PARTICLEID_VARIABLE = "particleId";
		const char* const EMITTERID_VARIABLE = "emitterId";
		const char* const TEXTURE_PROXY_VARIABLE = "proxy";
		const char* const RESTITUTION_VARIABLE = "restitution";
		const char* const BODY_TYPE_VARIABLE = "bodyType";
		const char* const LINEAR_VELOCITY_VARIABLE = "linearVelocity";
		const char* const ANGLUAR_VELOCITY_VARIABLE = "angluarVelocity";
		const char* const LINEAR_DAMPING_VARIABLE = "linearDamping";
		const char* const ANGLUAR_DAMPING_VARIABLE = "angluarDamping";
		const char* const FIXED_ROTATION_VARIABLE = "fixedRotation";
		const char* const ALLOW_SLEEP_VARIABLE = "allowSleep";
		const char* const IS_SLEEPING_VARIABLE = "isSleeping";
		const char* const DENSITY_VARIABLE = "density";
		const char* const FRICTION_VARIABLE = "friction";
		const char* const GRAVITY_VARIABLE = "gravity";
		const char* const TIME_FACTOR_VARIABLE = "timeFactor";
		const char* const CATEGORY_MASK_VARIABLE = "categoryMask";
		const char* const MASK_BITS_VARIABLE = "maskBits";
		const char* const GROUP_INDEX_VARIABLE = "groupIndex";
		const char* const SPRITE_ID_VARIABLE = "spriteId";
		const char* const PHYSIC_OBJECT_ID_VARIABLE = "physicId";
		const char* const SPRITE_ABSOLUTE_VARIABLE = "spriteAbsolute";

		const char* const ADDITIONAL_BOUND_TO_IMAGE_VARIABLE = "additionalBoundToImage";
		const char* const AFFECTED_BY_EXPLOSION_VARIABLE = "affectedByExplosion";
		const char* const ALPHA_OVER_TIME_VARIABLE = "alphaOverTime";
		const char* const ALPHA_RANDOM_VARIABLE = "alphaRandom";
		const char* const ANGLUAR_VELOCITY_RANDOM_VARIABLE = "angluarVelocityRandom";
		const char* const LIFE_VARIABLE = "life";
		const char* const LIFE_RANDOM_VARIABLE = "lifeRandom";
		const char* const SCALE_RANDOM_VARIABLE = "scaleRandom";
		const char* const LINEAR_VELOCITY_RANDOM_VARIABLE = "velocityRandom";
		const char* const IS_COLLIDING_VARIABLE = "isColliding";
		const char* const MAX_NUMBER_VARIABLE = "maxNumber";
		const char* const REMOVE_ON_COLLIDE_VARIABLE = "removeOnCollide";
		const char* const SCALE_OVER_TIME_VARIABLE = "scaleOverTime";
		const char* const SPAWN_DELAY_VARIABLE = "spawnDelay";
		const char* const SPAWN_DELAY_RANDOM_VARIABLE = "spawnDelayRandom";
		const char* const VELOCITY_AFFECTED_BY_EMITTER_VARIABLE = "velocityAffectedByEmitter";
		const char* const VELOCITY_AFFECTED_BY_EMITTER_VELOCITY_VARIABLE = "velocityAffectedByEmitterVelocity";

		const char* const LAYER_BLENDER_VARIABLE = "layerBlender";
		const char* const ACTIVE_VARIABLE = "active";

		const char* const SHADER_TYPE_VARIABLE = "shaderType";
		const char* const FBOS_ELEMENT = "fbos";
		const char* const FBO_ELEMENT = "fbo";
		const char* const FBO_WIDTH_VARIABLE = "width";
		const char* const FBO_HEIGHT_VARIABLE = "height";
		const char* const FBO_LINEAR_VARIABLE = "linear";
		const char* const FBO_COLOR_VARIABLE = "color";
		const char* const FBO_DEPTH_VARIABLE = "depth";
		const char* const FBO_STENCIL_VARIABLE = "stencil";
		const char* const VERTEX_SHADER_ID_VARIABLE = "vertexShaderId";
		const char* const FRAGMENT_SHADER_ID_VARIABLE = "fragmentShaderId";
		const char* const UNIFORM_VARIABLE = "uniform";
	
		const char* const SHADER_USE_SPECIAL_VARIABLE = "useSpecial";

		const char* const TAG_VARIABLE = "tag";
		const char* const HASH_TAG_VARIABLE = "hashTag";
		const char* const AUTO_DELETE_VARIABLE = "autoDelete";
		const char* const SPRITE_COLLECTION_VARIABLE = "sprites";
		const char* const SCRIPT_ID_VARIABLE = "scriptId";
		const char* const FUNCTION_ID_VARIABLE = "functionId";

		//Types
		const char* const TYPE_NAME_ATTRIBUTE = "name"; // Name of variable attribute
		const char* const TYPE_VALUE_ATTRIBUTE = "value"; // Value of variable attribute
		const char* const USHORT_ELEMENT = "ushort"; // Unsigned short
		const char* const VECTOR2_ELEMENT = "vector2"; // gilVector2
		const char* const FLOAT_ELEMENT = "float"; // gilFloat
		const char* const SSTRING_ELEMENT = "sstring"; // short string value in attribute
		const char* const SHAPE_ELEMENT = "shape"; // gilShape

		//Elements
		const char* const LEVEL_ELEMENT = "level"; // Main element of level file
		const char* const RESOURCES_ELEMENT = "resources"; // Resources of level
		const char* const TEXTURES_ELEMENT = "textures";
		const char* const TEXTURE_ELEMENT = "texture";
		const char* const TEXTURE_PROXIES_ELEMENT = "proxies";
		const char* const ANIMATIONS_ELEMENT = "animations";
		const char* const ANIMATION_ELEMENT = "animation";
		const char* const SHADERS_ELEMENT = "shaders";
		const char* const SHADER_ELEMENT = "shader";
		const char* const SCRIPTS_ELEMENT = "scripts";
		const char* const SCRIPT_ELEMENT = "script";
		const char* const SOUND_EFFECTS_ELEMENT = "soundEffects";
		const char* const SOUND_EFFECT_ELEMENT = "soundEffect";
		const char* const SOUND_STREAMS_ELEMENT = "soundStreams";
		const char* const SOUND_STREAM_ELEMENT = "soundStream";
		const char* const CUSTOM_DATA_ELEMENT = "customData";
		const char* const CUSTOM_TYPE_ELEMENT = "customType";
		const char* const RESOURCE_GROUPS_ELEMENT = "resourceGroups";
		const char* const RESOURCE_GROUP_ELEMENT = "resourceGroup";
		const char* const GRAPHICS_ELEMENT = "graphics"; // Element with graphics defied
		const char* const SPRITE_ENTITIES_ELEMENT = "spriteEntities";// Element with Entities defied
		const char* const SPRITE_ENTITY_ELEMENT = "spriteEntity";// Element of sprite entity
		const char* const LAYER_OBJECT_ELEMENT = "layerObject";// Element with Entities defied
		const char* const LAYER_OBJECTS_ELEMENT   = "layerObject";// Element of sprite entity
		const char* const FUNCTIONS_ELEMENT = "functions";
		const char* const HASH_TAGS_ELEMENT = "hashTags";
		const char* const SCENE_ELEMENT = "scene";

		const char* const EFFECTS_ELEMENT = "effects";
		const char* const LAYER_EFFECTS_ELEMENT = "layerEffects";
		const char* const LAYER_EFFECT_ELEMENT = "layerEffect";
		const char* const LAYER_BLENDERS_ELEMENT = "layerBlenders";
		const char* const UNIFORMS_ELEMENT = "uniforms";
		const char* const SHADER_BINDERS_ELEMENT = "shaderBinders";
		const char* const SHADER_BINDER_ELEMENT = "shaderBinder";

		const char* const PHYSICS_ELEMENT = "physics"; // Element with physics defied
		const char* const BODIES_ELEMENT = "bodies"; // Element of bodies entity
		const char* const PARTICLES_ELEMENT = "particles"; // Element of particles entity
		const char* const PARTICLE_ELEMENT = "particle"; // Element of particle entity
		const char* const BODY_ELEMENT = "body"; // Element of body
		const char* const ENTITIES_ELEMENT = "entities"; // Element of entities
		const char* const ENTITY_ELEMENT = "entity"; // Element of entity

		const char* const COMPONENTS_ELEMENT = "components";
		const char* const SPRITE_COMPONENT_ELEMENT = "sprite";
		const char* const PHYSICS_COMPONENT_ELEMENT = "physic";
		const char* const SCRIPT_COMPONENT_ELEMENT = "script";

		const char* const AFTER_LOAD_ATTRIBUTE = "afterLoad";
	}*/


	typedef void(*levelCallback)();
	typedef void(*levelLogCallback)(const char*);

	class LevelManager
	{
	private:
		ja::string lastLoadedLevelName;
		ja::string afterLoadScript;
		//SDL_Thread* levelLoadingThread;
		fastdelegate::FastDelegate0<> saveLevelCallback;
		fastdelegate::FastDelegate0<> savedLevelCallback;
		fastdelegate::FastDelegate0<> loadLevelCallback;
		fastdelegate::FastDelegate0<> loadedLevelCallback;
		fastdelegate::FastDelegate0<> reloadLevelCallback;
		fastdelegate::FastDelegate0<> reloadedLevelCallback;
		fastdelegate::FastDelegate1<const char*> printCurrentTaskCallback;
		fastdelegate::FastDelegate1<const char*> printFinishedTaskCallback;
		fastdelegate::FastDelegate1<const char*> printFailureTaskCallback;

		static LevelManager* singleton;
		LevelManager();

		//saver
		/*void saveResources(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* levelElement);
		void saveTextures(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* resourcesElement);
		void saveTexture(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* texturesElement, Resource<Texture2d>* texture);
		void saveSoundEffects(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* resourcesElement);
		void saveSoundEffect(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* soundEffectsElement, Resource<SoundEffect>* soundEffect);
		void saveShaders(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* resourcesElement);
		void saveShader(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* shadersElement, Resource<Shader>* shader);
		void saveScripts(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* resourcesElement);
		void saveScript(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* scriptsElement, Resource<Script>* script);
		void saveCustomData(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* resourcesElement);
		void saveCustomType(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* customDataElement, Resource<CustomData>* customType);
		void saveResourceGroups(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* resourcesElement);
		void saveResourceGroup(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* resourceGroupsElement, ResourceGroupProxy* resourceGroup);*/

		/*void saveGraphics(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* levelElement);
		void saveGraphicsScene(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* graphicsElement);
		void saveLayerObjects(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement * graphicsElement);
		void saveLayerObject(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* polygonsElement, LayerObject* layerObject);
		void saveEffects(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* graphicsElement);
		void saveLayerBlenders(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* effectsElement);
		void saveLayerEffects(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* effectsElement);
		void saveShaderBinders(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* effectsElement);
		void saveFbos(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* effectsElement);*/

		/*void savePhysics(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* levelElement);
		void savePhysicsScene(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* physicsElement);
		void saveBodies(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* physicsElement);
		void saveParticles(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* physicsElement);
		void saveBody(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* bodiesElement, RigidBody2d* body);
		void saveParticle(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* particlesElement, ParticleGenerator2d* particleGenerator);
		void saveLayerEffect(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* layerEffectsElement, LayerEffect* effect, unsigned char layerId);
		void saveFbo(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* fbosElement, const char* variableName, Fbo* fbo);
		void saveShaderBinder(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* shaderBindersElement, const char* variableName, ShaderBinder* shaderBinder);

		void saveEntities(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* levelElement);
		void saveEntity(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* entitiesElement, Entity* entity);
		void savePhysicComponent(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* componentElement, EntityComponent* entityComponent);
		void saveSpriteComponent(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* componentElement, EntityComponent* entityComponent);
		void saveScriptComponent(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* componentElement, EntityComponent* entityComponent);

		void saveLayerBlender(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, LayerBlender* layerBlender);
		void saveColor(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void saveDouble(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, double variableValue);
		void saveBool(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, bool variableValue);
		void saveInt32(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, int32_t variableValue);
		void saveIntArray(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, int* valuePointer, unsigned int arraySize);
		void saveFloatArray(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, float* valuePointer, unsigned int arraySize);
		void saveShortArray(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, short* valuePointer, unsigned int arraySize);
		void saveCharArray(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, char* valuePointer, unsigned int arraySize);
		void saveUInt(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, unsigned int variableValue);
		void saveUInt16(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, unsigned short variableValue);
		void saveUChar(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, unsigned char variableValue);
		void saveUInt8(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, uint8_t variableValue);
		void saveChar(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, char variableValue);
		void saveFloat(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, float variableValue);
		void saveVector2(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, jaVector2& variableValue);
		void saveSString(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, const char* variableValue);
		void saveShape(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, Shape2d* variableValue);
		void saveBox(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, Shape2d* shape);
		void saveCircle(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, Shape2d* shape);
		//loader
		void loadResources(tinyxml2::XMLElement* levelHandle);
		void loadTextures(tinyxml2::XMLElement* resourcesElement);
		void loadShaders(tinyxml2::XMLElement* resourcesElement);
		void loadScripts(tinyxml2::XMLElement* resourcesHandle);
		void loadSoundEffects(tinyxml2::XMLElement* resourcesHandle);
		void loadSoundStreams(tinyxml2::XMLElement* resourcesHandle);
		void loadCustomData(tinyxml2::XMLElement* resourcesHandle);
		void loadSoundEffect(tinyxml2::XMLElement* soundEffectElement);
		void loadSoundStream(tinyxml2::XMLElement* musicStreamElement);
		void loadShader(tinyxml2::XMLElement* shaderElement);
		void loadScript(tinyxml2::XMLElement* scriptElement);
		void loadTexture(tinyxml2::XMLElement* textureElement);
		void loadCustomType(tinyxml2::XMLElement* customDataElement);
		void loadResourceGroups(tinyxml2::XMLElement* resourcesHandle);
		void loadResourceGroup(tinyxml2::XMLElement* resourceGroupElement);

		void loadGraphics(tinyxml2::XMLElement* levelHandle);
		void loadGraphicsScene(tinyxml2::XMLElement*graphicsHandle);


		void loadEffects(tinyxml2::XMLElement* graphicsHandle);
		void loadLayerBlenders(tinyxml2::XMLElement* effectsHandle);
		void loadLayerEffects(tinyxml2::XMLElement* effectsHandle);
		void loadShaderBinders(tinyxml2::XMLElement*effectsHandle);
		void loadFbos(tinyxml2::XMLElement* effectsHandle);

		void loadPhysics(tinyxml2::XMLElement* levelHandle);
		void loadPhysicsScene(tinyxml2::XMLElement* physicsHandle);
		void loadBodies(tinyxml2::XMLElement* physicsHandle);
		void loadParticles(tinyxml2::XMLElement* physicsHandle);
		void loadBody(tinyxml2::XMLElement* bodyElement);
		void loadParticle(tinyxml2::XMLElement* particleElement);

		void loadEntities(tinyxml2::XMLElement* levelHandle);
		void loadEntity(tinyxml2::XMLElement* entityElement);
		void loadPhysicComponents(tinyxml2::XMLElement* componentsElement, Entity* entity);
		void loadSpriteComponents(tinyxml2::XMLElement* componentsElement, Entity* entity);
		void loadScirptComponents(tinyxml2::XMLElement* componentsElement, Entity* entity);

		void loadLayerEffect(tinyxml2::XMLElement* layerEffectElement);
		void loadShaderBinder(tinyxml2::XMLElement* shaderBinderElement);
		void loadFbo(tinyxml2::XMLElement* fboElement);

		void loadColor(tinyxml2::XMLElement* parent, const char* variableName, uint8_t& variableValueR, uint8_t& variableValueG, uint8_t& variableValueB, uint8_t& variableValueA);
		void loadDouble(tinyxml2::XMLElement* parent, const char* variableName, double& variableValue);
		void loadBool(tinyxml2::XMLElement* parent, const char* variableName, bool& variableValue);
		void loadInt(tinyxml2::XMLElement* parent, const char* variableName, int& variableValue);
		void loadIntArray(tinyxml2::XMLElement* parent, const char* variableName, int* variableValue, unsigned int arraySize);
		void loadUInt(tinyxml2::XMLElement* parent, const char* variableName, unsigned int& variableValue);
		void loadUShort(tinyxml2::XMLElement* parent, const char* variableName, unsigned short& variableValue);
		void loadChar(tinyxml2::XMLElement* parent, const char* variableName, char& variableValue);
		void loadInt8(tinyxml2::XMLElement* parent, const char* variableName, int8_t& variableValue);
		void loadUChar(tinyxml2::XMLElement* parent, const char* variableName, unsigned char& variableValue);
		void loadUInt8(tinyxml2::XMLElement* parent, const char* variableName, uint8_t& variableValue);
		void loadFloat(tinyxml2::XMLElement* parent, const char* variableName, float& variableValue);
		void loadFloatArray(tinyxml2::XMLElement* parent, const char* variableName, float* variableValue, unsigned int arraySize);
		void loadShortArray(tinyxml2::XMLElement* parent, const char* variableName, short* variableValue, unsigned int& arraySize);
		void loadCharArray(tinyxml2::XMLElement* parent, const char* variableName, char* variableValue, unsigned int arraySize);
		void loadVector2(tinyxml2::XMLElement* parent, const char* variableName, jaVector2& variableValue);
		void loadSString(tinyxml2::XMLElement* parent, const char* variableName, ja::string& variableValue);
		Shape2d* loadShape(tinyxml2::XMLElement* parent, const char* variableName);
		Shape2d* loadBox(tinyxml2::XMLElement* parent);
		Shape2d* loadCircle(tinyxml2::XMLElement* parent);*/

	public:

		~LevelManager();
		static void init();

		void printCurrentTask(const char* currentTask);
		void printFailureTask(const char* failureTask);
		void printFinishedTask(const char* finishedTask);

		void setSaveLevelCallback(levelCallback callback);
		void setSavedLevelCallback(levelCallback callback);
		void setLoadLevelCallback(levelCallback callback);
		void setLoadedLevelCallback(levelCallback callback);
		void setReloadLevelCallback(levelCallback callback);
		void setReloadedLevelCallback(levelCallback callback);
		void setPrintCurrentTaskCallback(levelLogCallback callback);
		void setPrintFinishedTaskCallback(levelLogCallback callback);
		void setPrintFailureTaskCallback(levelLogCallback callback);

		void saveLevel(const ja::string& levelName);
		void loadLevel(const ja::string& levelName);
		void loadLevelSeparateThread(const ja::string& levelName);
		void reloadLevel();

		static inline LevelManager* LevelManager::getSingleton()
		{
			return singleton;
		}
		static void cleanUp();
	};

}

