#pragma once

#include <stdio.h>
#include <list>
#include "../jaSetup.hpp"
#include "../Entity/Entity.hpp"

namespace ja
{

	class EntityManager
	{
	private:
		std::list<uint16_t> entityIds;
		EntityGroup*        entityGroups[setup::entities::MAX_COMPONENT_GROUPS];
		uint8_t             componentGroupNum;

		CacheLineAllocator* entityAllocator;
		CacheLineAllocator* layerObjectComponentAllocator;
		CacheLineAllocator* physicComponentAllocator;
		CacheLineAllocator* scriptComponentAllocator;
		CacheLineAllocator* dataComponentAllocator;

		uint16_t nextId;
		Entity*  entities[setup::entities::MAX_ENTITIES];
		static EntityManager* singleton;
		EntityManager();
		void deinitEntityGroups();

		inline Entity* getNewEntity()
		{
			uint16_t id;

			if (entityIds.empty())
			{
				id = nextId++;
				//throw Exception
			}
			else
			{
				id = entityIds.front();
				entityIds.pop_front();
			}

			entities[id] = new (entityAllocator->allocate(sizeof(Entity))) Entity();
			Entity* entity = entities[id];

			entity->id = id;


			return entity;
		}
	public:

		~EntityManager();
		static void init();

		static inline EntityManager* EntityManager::getSingleton()
		{
			return singleton;
		}

		static void cleanUp();

		Entity* createEntity(uint16_t entityId);
		Entity* createEntity();

		Entity* getEntity(uint16_t entityId);
		void    getEntities(std::list<Entity*>* entitiesList);
		void    clearEntities();
		EntityGroup* getComponentGroup(uint8_t groupEntity);

		inline uint8_t getComponentGroupNum()
		{
			return componentGroupNum;
		}

		void update(float deltaTime);
		void deleteEntityImmediate(Entity* entity);

		static uint32_t EntityManager::getImageId(uint8_t componentGroupId)
		{
			return (2000 + componentGroupId);
		}

		uint16_t getNextId();
	};

}

