#include "LevelManager.hpp"
#include <tinyxml2/tinyxml2.h>

#include <thread>
#include "LayerObjectManager.hpp"
#include "PhysicsManager.hpp"
#include "GameManager.hpp"
#include "EffectManager.hpp"
#include "EntityManager.hpp"
#include "SoundManager.hpp"
#include "ScriptManager.hpp"
#include "LogManager.hpp"
#include "../Utility/Exception.hpp"
#include "../Graphics/Polygon.hpp"
#include "../Graphics/ShaderLoader.hpp"
#include "../../Jerycho/Gui/MainMenu.hpp"

#include "../Entity/LayerObjectGroup.hpp"
#include "../Entity/ScriptGroup.hpp"

using namespace tr;
using namespace tinyxml2;

namespace ja
{

LevelManager* LevelManager::singleton = nullptr;

int32_t loadLevelThreadFunction(void* data)
{
	ja::string* levelName = (ja::string*)data;
	LevelManager::getSingleton()->loadLevel(*levelName);

	return 0;
}

LevelManager::LevelManager()
{
	afterLoadScript = "";
}

LevelManager::~LevelManager()
{

}

void LevelManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void LevelManager::init()
{
	if(singleton==nullptr)
	{ 
		singleton = new LevelManager();
	}
}

void LevelManager::printCurrentTask(const char* currentTask)
{
	LogManager* logManager = LogManager::getSingleton();

	if(printCurrentTaskCallback)
		printCurrentTaskCallback(currentTask);
}

void LevelManager::printFailureTask(const char* failureTask)
{
	if(printFailureTaskCallback)
		printFailureTaskCallback(failureTask);
}

void LevelManager::printFinishedTask(const char* finishedTask)
{
	if(printFinishedTaskCallback)
		printFinishedTaskCallback(finishedTask);
}

void LevelManager::setSaveLevelCallback(levelCallback callback)
{
	saveLevelCallback.bind(callback);
}

void LevelManager::setSavedLevelCallback(levelCallback callback)
{
	savedLevelCallback.bind(callback);
}

void LevelManager::setLoadLevelCallback(levelCallback callback)
{
	loadLevelCallback.bind(callback);
}

void LevelManager::setLoadedLevelCallback(levelCallback callback)
{
	loadedLevelCallback.bind(callback);
}

void LevelManager::setReloadLevelCallback(levelCallback callback)
{
	reloadLevelCallback.bind(callback);
}

void LevelManager::setReloadedLevelCallback(levelCallback callback)
{
	reloadedLevelCallback.bind(callback);
}

void LevelManager::setPrintCurrentTaskCallback(levelLogCallback callback)
{
	printCurrentTaskCallback.bind(callback);
}

void LevelManager::setPrintFinishedTaskCallback(levelLogCallback callback)
{
	printFinishedTaskCallback.bind(callback);
}

void LevelManager::setPrintFailureTaskCallback(levelLogCallback callback)
{
	printFailureTaskCallback.bind(callback);
}

void LevelManager::loadLevel(const ja::string& levelName)
{
	LogManager*    logManager    = LogManager::getSingleton();
	ScriptManager* scriptManager = ScriptManager::getSingleton();

	if(loadLevelCallback)
		loadLevelCallback();

	ja::string fullPath(ResourceManager::getSingleton()->getScriptsPath());
	fullPath.append(levelName);

	char logBuffer[1024];

	sprintf(logBuffer,"Loading level %s", fullPath.c_str());
	printCurrentTask(logBuffer);
	logManager->addLogMessage(ja::LogType::INFO_LOG, logBuffer);
	//tinyxml2::XMLError status = doc.LoadFile(fullPath.c_str());

	bool status = ScriptManager::getSingleton()->runScriptFile(levelName);

	//if (status != tinyxml2::XML_NO_ERROR)
	if (status == false)
	{
		sprintf(logBuffer, "Can't open level %s", fullPath.c_str());
		printFailureTask(logBuffer);
		logManager->addLogMessage(ja::LogType::ERROR_LOG, logBuffer);

		//Already in log
		//std::cout << "Parsing file " << levelName.c_str() << " error: " << status << std::endl;
		//std::cout << "Error string 1: " << ScriptManager::getSingleton()->getLastError() << std::endl;

		if (loadedLevelCallback)
			loadedLevelCallback();
	}
	else
	{
		//printFinishedTask("File " + fullPath + " opened");
		//printCurrentTask("Parsing level file");
		//
		//tinyxml2::XMLElement* levelElement = doc.FirstChildElement();
		//
		//printCurrentTask("Loading resources");
		//loadResources(levelElement);
		//printFinishedTask("Resources loaded");
		//
		//printCurrentTask("Loading physics");
		//loadPhysics(levelElement);
		//printFinishedTask("Physics loaded");
		//
		//printCurrentTask("Loading graphics");
		//loadGraphics(levelElement);
		//printFinishedTask("Graphics loaded");
		//
		//printCurrentTask("Loading entities");
		//loadEntities(levelElement);
		//printFinishedTask("Entities loaded");
	
		//afterLoadScript = levelElement->Attribute(tag::AFTER_LOAD_ATTRIBUTE);
		//if (!afterLoadScript.empty())
		//{
		//	if (ScriptManager::getSingleton() != nullptr)
		//	{
		//		printCurrentTask("Running after load script");
		//		if (!ScriptManager::getSingleton()->runScriptFile(afterLoadScript))
		//		{
		//			printFailureTask("Couldn't compile file " + afterLoadScript);
		//		}
		//		printFinishedTask("After load script completed");
		//	}
		//}

		lua_State* L = scriptManager->getLuaState();
		lua_getglobal(L, "initLevel");
		lua_call(L, 0, 0);

		printCurrentTask("Loading complete");
		logManager->addLogMessage(LogType::SUCCESS_LOG, "Loading complete");

		if (loadedLevelCallback)
			loadedLevelCallback();

		logManager->saveLog("Run.log");
	}
}

void LevelManager::loadLevelSeparateThread(const ja::string& levelName)
{
	lastLoadedLevelName = levelName;
	
	MainMenu::getSingleton()->setGameState(GameState::LOADING_MODE);

	std::thread levelLoader(loadLevelThreadFunction, (void*)(&lastLoadedLevelName));
	levelLoader.detach();
}

void LevelManager::reloadLevel()
{
	ScriptManager* scriptManager = ScriptManager::getSingleton();

	if(lastLoadedLevelName.size() == 0)
		return;

	if(reloadLevelCallback)
		reloadLevelCallback();

	/*ja::string fullPath(ResourceManager::getSingleton()->getScriptsPath());
	fullPath.append(lastLoadedLevelName);
	XMLDocument doc;
	XMLError status = doc.LoadFile(fullPath.c_str());
	const char* afterLoadScriptFile;*/

	/*if (status != tinyxml2::XML_NO_ERROR)
	{
		printFailureTask("Can't open level " + fullPath);
		std::cout << "Parsing file " << lastLoadedLevelName.c_str() << " error: " << status << std::endl;
		std::cout << "Error string 1: " << doc.GetErrorStr1() << std::endl;
		std::cout << "Error string 2: " << doc.GetErrorStr2() << std::endl;
		
		if (reloadedLevelCallback)
			reloadedLevelCallback();
	}
	else
	{
		tinyxml2::XMLElement* levelElement = doc.FirstChildElement();

		printCurrentTask("Loading physics");
		loadPhysics(levelElement);
		printFinishedTask("Physics loaded");

		printCurrentTask("Loading graphics");
		loadGraphics(levelElement);
		printFinishedTask("Graphics loaded");

		printCurrentTask("Loading entities");
		loadEntities(levelElement);
		printFinishedTask("Entities loaded");

		afterLoadScriptFile = levelElement->Attribute(tag::AFTER_LOAD_ATTRIBUTE);
		if (afterLoadScriptFile)
		{
			printCurrentTask("Running after load script");
			if (!ScriptManager::getSingleton()->runScriptFile(afterLoadScript))
			{
				printFailureTask("Couldn't compile file " + afterLoadScript);
			}
			printFinishedTask("After load script completed");
		}

		printCurrentTask("Level reloaded");
		if (reloadedLevelCallback)
			reloadedLevelCallback();
	}*/

	lua_State* L = scriptManager->getLuaState();
	lua_getglobal(L, "initLevel");
	lua_call(L, 0, 0);

	printCurrentTask("Level reloaded");
	if (reloadedLevelCallback)
		reloadedLevelCallback();

}

void LevelManager::saveLevel(const ja::string& levelName)
{
	//tinyxml2::XMLDocument doc;

	if (saveLevelCallback)
		saveLevelCallback();

	//XMLElement* levelElement = doc.NewElement(tag::LEVEL_ELEMENT);
	//
	//if (afterLoadScript.size() != 0)
	//{
	//	levelElement->SetAttribute(tag::AFTER_LOAD_ATTRIBUTE, afterLoadScript.c_str());
	//}
	//
	//saveResources(&doc, levelElement);
	//saveGraphics(&doc, levelElement);
	//savePhysics(&doc, levelElement);
	//saveEntities(&doc, levelElement);
	//
	//doc.LinkEndChild(levelElement);
	//
	//ja::string fullPath(ResourceManager::getSingleton()->getLevelsPath());
	//fullPath.append(levelName);
	//doc.SaveFile(fullPath.c_str());
	if (savedLevelCallback)
		savedLevelCallback();
}

////////////////// loader
/*void LevelManager::loadPhysics(XMLElement* levelElement)
{
	XMLElement* physicsElement = levelElement->FirstChildElement(tag::PHYSICS_ELEMENT);
	printCurrentTask("Loading scene");
	loadPhysicsScene(physicsElement);
	printFinishedTask("Scene loaded");
	printCurrentTask("Loading bodies");
	loadBodies(physicsElement);
	printFinishedTask("Bodies loaded");
	printCurrentTask("Loading particles");
	loadParticles(physicsElement);
	printFinishedTask("Particles loaded");
}*/

/*void LevelManager::loadPhysicsScene(XMLElement* physicsElement)
{
	XMLElement* sceneElement = physicsElement->FirstChildElement(tag::SCENE_ELEMENT);

	if (sceneElement) {
		int cellX;
		int cellY;
		float cellSize;

		Scene2d* scene = PhysicsManager::getSingleton()->getScene();
		HashGrid* hashGrid = static_cast<HashGrid*>(scene);

		if (XML_NO_ERROR != sceneElement->QueryIntAttribute("cellX", &cellX)) {
			cellX = hashGrid->getCellWidth();
		}
		if (XML_NO_ERROR != sceneElement->QueryIntAttribute("cellY", &cellY)) {
			cellY = hashGrid->getCellHeight();
		}
		if (XML_NO_ERROR != sceneElement->QueryFloatAttribute("cellSize", &cellSize)) {
			cellSize = hashGrid->getCellSize();
		}

		hashGrid->rebuild(static_cast<short>(cellX), static_cast<short>(cellY), cellSize);
	}
}*/

/*void LevelManager::loadBodies(XMLElement* physicsElement)
{
	XMLElement* bodiesElement = physicsElement->FirstChildElement(tag::BODIES_ELEMENT);
	if (bodiesElement)
	{
		XMLElement* bodyElement = bodiesElement->FirstChildElement(tag::BODY_ELEMENT);

		for (bodyElement; bodyElement; bodyElement = bodyElement->NextSiblingElement())
		{
			loadBody(bodyElement);
		}
	}
}*/

/*void LevelManager::loadParticles(XMLElement* physicsElement)
{
	XMLElement* particlesElement = physicsElement->FirstChildElement(tag::PARTICLES_ELEMENT);
	if (particlesElement)
	{
		XMLElement* particleElement = particlesElement->FirstChildElement(tag::PARTICLE_ELEMENT);

		for (particleElement; particleElement; particleElement = particleElement->NextSiblingElement())
		{
			loadParticle(particleElement);
		}
	}
}*/

/*void LevelManager::loadBody(XMLElement* bodyElement)
{
	uint16_t bodyId;
	uint16_t bodyType;

	PhysicObjectDef2d* bodyDef = &PhysicsManager::getSingleton()->physicObjectDef;

	loadUShort(bodyElement, tag::ID_VARIABLE, bodyId);
	loadVector2(bodyElement, tag::POSITION_VARIABLE, bodyDef->transform.position);
	float angleDeg;
	loadFloat(bodyElement, tag::ANGLE_VARIABLE, angleDeg);
	bodyDef->transform.rotationMatrix.setDegrees(angleDeg);

	loadUShort(bodyElement, tag::BODY_TYPE_VARIABLE, bodyType);
	bodyDef->bodyType = static_cast<BodyType>(bodyType);

	loadVector2(bodyElement, tag::LINEAR_VELOCITY_VARIABLE, bodyDef->linearVelocity);
	loadFloat(bodyElement, tag::ANGLUAR_VELOCITY_VARIABLE, bodyDef->angluarVelocity);
	loadFloat(bodyElement, tag::LINEAR_DAMPING_VARIABLE, bodyDef->linearDamping);
	loadFloat(bodyElement, tag::ANGLUAR_DAMPING_VARIABLE, bodyDef->angluarDamping);
	loadBool(bodyElement, tag::FIXED_ROTATION_VARIABLE, bodyDef->fixedRotation);
	loadBool(bodyElement, tag::ALLOW_SLEEP_VARIABLE, bodyDef->allowSleep);
	loadBool(bodyElement, tag::IS_SLEEPING_VARIABLE, bodyDef->isSleeping);
	loadVector2(bodyElement, tag::GRAVITY_VARIABLE, bodyDef->gravity);
	loadFloat(bodyElement, tag::TIME_FACTOR_VARIABLE, bodyDef->timeFactor);
	loadInt8(bodyElement, tag::GROUP_INDEX_VARIABLE, bodyDef->groupIndex);
	loadUShort(bodyElement, tag::CATEGORY_MASK_VARIABLE, bodyDef->categoryMask);
	loadUShort(bodyElement, tag::MASK_BITS_VARIABLE, bodyDef->maskBits);
	Shape2d* shape = loadShape(bodyElement, tag::SHAPE_ELEMENT);
	bodyDef->copyShape(shape);

	PhysicsManager::getSingleton()->createBodyDef(bodyId);

	delete shape;
}*/

/*void LevelManager::loadParticle(XMLElement* particleElement)
{
	ParticleDef2d* particleDef = &PhysicsManager::getSingleton()->particleDef;
	unsigned short particleGeneratorId;

	loadUShort(particleElement, tag::ID_VARIABLE, particleGeneratorId);
	loadUShort(particleElement, tag::EMITTERID_VARIABLE, particleDef->emitterId);
	loadFloat(particleElement, tag::ADDITIONAL_BOUND_TO_IMAGE_VARIABLE, particleDef->additionalBoundToImage);
	loadBool(particleElement, tag::AFFECTED_BY_EXPLOSION_VARIABLE, particleDef->affectedByExplosion);
	loadFloat(particleElement, tag::ALPHA_OVER_TIME_VARIABLE, particleDef->alphaOverTime);
	loadVector2(particleElement, tag::GRAVITY_VARIABLE, particleDef->gravity);
	loadFloat(particleElement, tag::ALPHA_VARIABLE, particleDef->initAlpha);
	//loadFloat(particleElement,JA_,particleDef->initAlphaRandom);
	loadFloat(particleElement, tag::ANGLUAR_VELOCITY_VARIABLE, particleDef->initAngluarVelocity);
	loadFloat(particleElement, tag::ANGLUAR_VELOCITY_RANDOM_VARIABLE, particleDef->initAngluarVelocityRandom);
	loadFloat(particleElement, tag::LIFE_VARIABLE, particleDef->initParticleLife);
	loadFloat(particleElement, tag::LIFE_RANDOM_VARIABLE, particleDef->initParticleLifeRandom);
	loadFloat(particleElement, tag::TEXTURE_SCALE_VARIABLE, particleDef->initScale);
	loadFloat(particleElement, tag::SCALE_RANDOM_VARIABLE, particleDef->initScaleRandom);
	loadVector2(particleElement, tag::LINEAR_VELOCITY_VARIABLE, particleDef->initVelocity);
	loadVector2(particleElement, tag::LINEAR_VELOCITY_RANDOM_VARIABLE, particleDef->initVelocityRandom);
	loadBool(particleElement, tag::IS_COLLIDING_VARIABLE, particleDef->isColliding);
	loadFloat(particleElement, tag::ANGLUAR_DAMPING_VARIABLE, particleDef->particleAngluarDamping);
	loadFloat(particleElement, tag::FRICTION_VARIABLE, particleDef->particleFriction);
	loadUChar(particleElement, tag::GROUP_VARIABLE, particleDef->particleGroup);
	loadFloat(particleElement, tag::LINEAR_DAMPING_VARIABLE, particleDef->particleLinearDamping);
	loadUShort(particleElement, tag::MAX_NUMBER_VARIABLE, particleDef->maxParticlesNum);
	loadFloat(particleElement, tag::RESTITUTION_VARIABLE, particleDef->particleRestitution);
	loadBool(particleElement, tag::REMOVE_ON_COLLIDE_VARIABLE, particleDef->removeOnCollide);
	loadFloat(particleElement, tag::SCALE_OVER_TIME_VARIABLE, particleDef->scaleOverTime);
	loadFloat(particleElement, tag::SPAWN_DELAY_VARIABLE, particleDef->spawnDelay);
	loadFloat(particleElement, tag::SPAWN_DELAY_RANDOM_VARIABLE, particleDef->spawnDelayRandom);
	loadBool(particleElement, tag::VELOCITY_AFFECTED_BY_EMITTER_VARIABLE, particleDef->velocityAffectedByEmmiter);
	//loadBool(particleElement,JA_VELOCITY_AFFECTED_BY_EMITTER_VELOCITY_VARIABLE,particleDef->velocityAffectedByEmmiterVelocity);
	loadFloat(particleElement, tag::TIME_FACTOR_VARIABLE, particleDef->timeFactor);
	loadInt8(particleElement, tag::GROUP_INDEX_VARIABLE, particleDef->groupIndex);
	loadUShort(particleElement, tag::CATEGORY_MASK_VARIABLE, particleDef->categoryBits);
	loadUShort(particleElement, tag::MASK_BITS_VARIABLE, particleDef->maskBits);

	PhysicsManager::getSingleton()->createParticleDef(particleGeneratorId);
}*/

/*void LevelManager::loadEntities(XMLElement* levelElement)
{
	XMLElement* entitiesElement = levelElement->FirstChildElement(tag::ENTITIES_ELEMENT);
	if (entitiesElement)
	{
		XMLElement* hashesElement = entitiesElement->FirstChildElement(tag::HASH_TAGS_ELEMENT);

		//loadHashes(hashesElement);

		if (EntityManager::getSingleton() != nullptr)
		{
			if (entitiesElement)
			{
				XMLElement* entityElement = entitiesElement->FirstChildElement(tag::ENTITY_ELEMENT);

				for (entityElement; entityElement; entityElement = entityElement->NextSiblingElement())
				{
					loadEntity(entityElement);
				}
			}
		}
	}
}*/

/*void LevelManager::loadEntity(XMLElement* entityElement)
{
	uint16_t   entityId;
	uint16_t   bodyId  = setup::physics::ID_UNASSIGNED;
	uint16_t   hashTag = 0;
	ja::string tagName;

	loadUShort(entityElement,tag::ID_VARIABLE,entityId);
	loadUShort(entityElement,tag::PHYSIC_OBJECT_ID_VARIABLE,bodyId);
	loadSString(entityElement,tag::TAG_VARIABLE,tagName);
	//loadUShort(entityElement,JA_HASH_TAG_VARIABLE,hashTag);

	Entity* entity = EntityManager::getSingleton()->createEntity(entityId);	
	
	if(bodyId != setup::physics::ID_UNASSIGNED)
	{
		//entity->physicObject = PhysicsManager::getSingleton()->getPhysicObject(bodyId);
	}

	if(!tagName.empty())
	{
		entity->setTagName(tagName.c_str());
	}
	//if(hashTag != 0)
	//{
	//	entity->hashTag = hashTag;
	//}

	XMLElement* componentsElement = entityElement->FirstChildElement(tag::COMPONENTS_ELEMENT);

	if(componentsElement)
	{
		loadPhysicComponents(componentsElement, entity);
		//loadSpriteComponents(componentsElement, entity);
		loadScirptComponents(componentsElement, entity);
	}
}*/

/*void LevelManager::loadPhysicComponents(XMLElement* componentsElement, Entity* entity)
{
	XMLElement* physicElement = componentsElement->FirstChildElement(tag::PHYSICS_COMPONENT_ELEMENT);
	ja::string tag;

	for(physicElement; physicElement != nullptr; physicElement = physicElement->NextSiblingElement(tag::PHYSICS_COMPONENT_ELEMENT))
	{
		uint16_t physicId = setup::physics::ID_UNASSIGNED;

		PhysicComponent* physicComponent =  PhysicGroup::getSingleton()->createComponent(entity);
		loadSString(physicElement,tag::HASH_TAG_VARIABLE,tag);
		
		if(!tag.empty())
		{
			physicComponent->setTagName(tag.c_str());
			tag.clear();
		}

		//loadUShort(physicElement,tag::HASH_TAG_VARIABLE,physicComponent->hashTag);
		loadUShort(physicElement,tag::PHYSIC_OBJECT_ID_VARIABLE, physicId);
		loadBool(physicElement,tag::AUTO_DELETE_VARIABLE,physicComponent->autoDelete);

		//entity->addComponent(physicComponent);
		if(physicId != setup::physics::ID_UNASSIGNED)
		{
			PhysicObject2d* physicObject = PhysicsManager::getSingleton()->getPhysicObject(physicId);
			physicComponent->setPhysicObject(physicObject);
		}
	}
}*/

//void LevelManager::loadSpriteComponents(XMLElement* componentsElement, Entity2d* entity)
//{
//	XMLElement* spriteElement = componentsElement->FirstChildElement(tag::SPRITE_COMPONENT_ELEMENT);
//	short spritesId[256];
//	ja::string tag;
//
//	for(spriteElement; spriteElement != nullptr; spriteElement = spriteElement->NextSiblingElement(tag::SPRITE_COMPONENT_ELEMENT))
//	{
//		unsigned short physicId = setup::ID_UNASSIGNED;
//		bool isFixedRotation = false;
//		bool isAutoDelete = true;
//		unsigned int spritesNum = 256;
//
//
//		SpriteComponent2d* spriteComponent =  Sprite2dGroup::getSingleton()->createComponent();
//		//loadUShort(spriteElement,JA_HASH_TAG_VARIABLE,spriteComponent->hashTag);
//		loadSString(spriteElement, tag::HASH_TAG_VARIABLE, tag);
//		if (!tag.empty())
//		{
//			spriteComponent->setTagName(tag.c_str());
//			tag.clear();
//		}
//		loadBool(spriteElement, tag::FIXED_ROTATION_VARIABLE, isFixedRotation);
//		loadBool(spriteElement, tag::AUTO_DELETE_VARIABLE, isAutoDelete);
//		loadUShort(spriteElement, tag::PHYSIC_OBJECT_ID_VARIABLE, physicId);
//		loadShortArray(spriteElement, tag::SPRITE_COLLECTION_VARIABLE, spritesId, spritesNum);
//		
//		spriteComponent->setFixedRotation(isFixedRotation);
//		spriteComponent->setAutoDelete(isAutoDelete);
//
//		for(unsigned int i=0; i < spritesNum; ++i)
//		{
//			unsigned short spriteId = static_cast<unsigned short>(spritesId[i]);
//			Sprite* sprite = SpriteManager::getSingleton()->getSprite(spriteId);
//			spriteComponent->addSprite(sprite);
//		}
//
//		entity->addComponent(spriteComponent);
//
//		if(physicId != setup::ID_UNASSIGNED)
//		{
//			PhysicObject2d* physicObject = PhysicsManager::getSingleton()->getPhysicObject(physicId);
//			PhysicComponent2d* physicComponent = static_cast<PhysicComponent2d*>(physicObject->componentData);
//			spriteComponent->setPhysicObjectComponent(physicComponent);
//		}
//	}
//}

/*void LevelManager::loadScirptComponents( XMLElement* componentsElement, Entity* entity )
{
	XMLElement* scriptElement = componentsElement->FirstChildElement(tag::SCRIPT_COMPONENT_ELEMENT);
	ja::string tag;

	for(scriptElement; scriptElement != nullptr; scriptElement = scriptElement->NextSiblingElement(tag::SCRIPT_COMPONENT_ELEMENT))
	{
		uint16_t scriptId = setup::resources::ID_UNASSIGNED;

		ScriptComponent* scriptComponent =  ScriptGroup::getSingleton()->createComponent(entity);

		loadSString(scriptElement,tag::HASH_TAG_VARIABLE,tag);
		if (!tag.empty())
		{
			scriptComponent->setTagName(tag.c_str());
			tag.clear();
		}
		//loadUChar(scriptElement, tag::FUNCTION_ID_VARIABLE, scriptComponent->functionIndex);
		loadUShort(scriptElement,tag::SCRIPT_ID_VARIABLE, scriptId);
		//if(scriptId != setup::resources::ID_UNASSIGNED)
		//{
		//	scriptComponent->script = ResourceManager::getSingleton()->getLevelScript(scriptId)->resource;	
		//}

		//entity->addComponent(scriptComponent);
	}
}*/

/*void LevelManager::loadLayerEffect(XMLElement* layerEffectElement)
{
	uint8_t  layerId;
	bool     effectActive;
	uint16_t scriptId;
	uint8_t  functionId;
	
	loadUChar(layerEffectElement,tag::LAYER_VARIABLE, layerId);
	loadBool(layerEffectElement,tag::ACTIVE_VARIABLE, effectActive);
	loadUShort(layerEffectElement, tag::SCRIPT_ID_VARIABLE, scriptId);
	loadUChar(layerEffectElement, tag::FUNCTION_ID_VARIABLE, functionId);

	LayerEffect* layerEffect = EffectManager::getSingleton()->getLayerEffect(layerId);
	Script* script = ResourceManager::getSingleton()->getLevelScript(scriptId)->resource;
	layerEffect->renderingScript = script;
	layerEffect->renderingScriptFunctionId = functionId;
	layerEffect->isEffectActive = effectActive;
}*/

/*void LevelManager::loadShaderBinder(XMLElement* shaderBinderElement)
{
	uint32_t    vertexShaderId;
	uint32_t    fragmentShaderId;
	const char* shaderBinderName;

	loadUInt(shaderBinderElement, tag::VERTEX_SHADER_ID_VARIABLE, vertexShaderId);
	loadUInt(shaderBinderElement, tag::FRAGMENT_SHADER_ID_VARIABLE, fragmentShaderId);

	Shader* vertexShader   = ResourceManager::getSingleton()->getLevelShader(vertexShaderId)->resource;
	Shader* fragmentShader = ResourceManager::getSingleton()->getLevelShader(fragmentShaderId)->resource;
	shaderBinderName = shaderBinderElement->Attribute("name");

	if (shaderBinderName == nullptr)
		return;

	ShaderBinder* shaderBinder = ShaderLoader::createShaderBinder(shaderBinderName, vertexShader, fragmentShader);
	XMLElement* uniformsElement = shaderBinderElement->FirstChildElement(tag::UNIFORMS_ELEMENT);

	if (uniformsElement)
	{
		XMLElement* uniformElement = uniformsElement->FirstChildElement(tag::UNIFORM_VARIABLE);
		const char* uniformName;
		const char* valuesBuffer;
		uint32_t    uniformType;
		uint32_t    uniformSize;
		float   uniformValuesFloat[16];
		int32_t uniformValuesInt[16];
		char    valueBuffer[60];
		int32_t readIndex;
		int32_t readNumber;

		for (uniformElement; uniformElement; uniformElement = uniformElement->NextSiblingElement())
		{
			uniformName = uniformElement->Attribute("name");
			uniformElement->QueryUnsignedAttribute("type", &uniformType);
			uniformElement->QueryUnsignedAttribute("size", &uniformSize);
			valuesBuffer = uniformElement->GetText();

			if (valuesBuffer == nullptr)
				continue;

			//parsing values
			readIndex = 0;
			readNumber = 0;

			while (*valuesBuffer != '\0')
			{
				if (*valuesBuffer == ';')
				{
					valueBuffer[readIndex] = '\0';

					switch (uniformType)
					{
					case 0: //JA_SHADER_VARIABLE_TYPE_FLOAT
						sscanf_s(valueBuffer, "%f", &uniformValuesFloat[readNumber]);
						break;
					case 1: //JA_SHADER_VARIABLE_TYPE_INT
						sscanf_s(valueBuffer, "%d", &uniformValuesInt[readNumber]);
						break;
					case 2: // JA_SHADER_VARIABLE_TYPE_VEC3
						sscanf_s(valueBuffer, "%f,%f,%f", &uniformValuesFloat[readNumber * 3], &uniformValuesFloat[readNumber * 3 + 1], &uniformValuesFloat[readNumber * 3 + 2]);
					}

					++readNumber;
					readIndex = 0;

					if (readNumber == uniformSize)
						break;
				}
				else
				{
					valueBuffer[readIndex] = *valuesBuffer;
					++readIndex;
				}
				++valuesBuffer;
			}
			//parsing values

			switch (uniformType)
			{
			case 0: //JA_SHADER_VARIABLE_TYPE_FLOAT
				shaderBinder->addUniform(uniformName, (ShaderVariableType)uniformType, uniformSize, uniformValuesFloat);
				break;
			case 1: //JA_SHADER_VARIABLE_TYPE_INT
				shaderBinder->addUniform(uniformName, (ShaderVariableType)uniformType, uniformSize, uniformValuesInt);
				break;
			case 2:
				shaderBinder->addUniform(uniformName, (ShaderVariableType)uniformType, uniformSize, uniformValuesFloat);
				break;
			}
		}
	}
}*/

/*void LevelManager::loadFbo(XMLElement* fboElement)
{
	const char* name = fboElement->Attribute("name");

	if (name == nullptr)
		return;

	float widthPercent;
	float heightPercent;
	bool linear;
	bool color = false;
	bool depth = false;
	bool stencil = false;

	loadFloat(fboElement, tag::FBO_WIDTH_VARIABLE, widthPercent);
	loadFloat(fboElement, tag::FBO_HEIGHT_VARIABLE, heightPercent);
	loadBool(fboElement, tag::FBO_LINEAR_VARIABLE, linear);
	loadBool(fboElement, tag::FBO_COLOR_VARIABLE, color);
	loadBool(fboElement, tag::FBO_DEPTH_VARIABLE, depth);
	loadBool(fboElement, tag::FBO_STENCIL_VARIABLE, stencil);

	int textureFlag = 0;
	if (color)
		textureFlag |= (int32_t)FboTextureFlag::COLOR_TEXTURE;
	if (depth)
		textureFlag |= (int32_t)FboTextureFlag::DEPTH_TEXTURE;
	if (stencil)
		textureFlag |= (int32_t)FboTextureFlag::STENCIL_TEXTURE;

	ShaderLoader::createFbo(name, widthPercent, heightPercent, textureFlag, linear);
}*/

/*Shape2d* LevelManager::loadShape(XMLElement* parent, const char* variableName)
{
	XMLElement* shape = parent->FirstChildElement(variableName);
	const char* shapeType;
	shapeType = shape->Attribute("shapeType");

	if(0 == strcmp(shapeType,"box"))
	{
		return loadBox(shape);
	}

	if(0 == strcmp(shapeType,"circle"))
	{
		return loadCircle(shape);
	}

	return nullptr;
}*/

/*Shape2d* LevelManager::loadBox(XMLElement* parent)
{
	jaFloat width;
	jaFloat height;
	loadFloat(parent,"width",width);
	loadFloat(parent,"height",height);
	
	return new Box2d(width,height);
}*/

/*Shape2d* LevelManager::loadCircle(XMLElement* parent)
{
	jaFloat radius;
	loadFloat(parent,"radius",radius);

	return new Circle2d(radius);
}*/

/*void LevelManager::loadGraphics(XMLElement* levelElement)
{
	XMLElement* graphicsElement = levelElement->FirstChildElement(tag::GRAPHICS_ELEMENT);

	if (LayerObjectManager::getSingleton() != nullptr)
	{
		printCurrentTask("Loading scene");
		loadGraphicsScene(graphicsElement);
		printFinishedTask("Scene loaded");
	}

	if(EffectManager::getSingleton() != nullptr)
	{
		printCurrentTask("Loading effects");
		loadEffects(graphicsElement);
		printFinishedTask("Effects loaded");
	}

	//if(SpriteManager::getSingleton() != nullptr)
	//{
	//	printCurrentTask("Loading sprites");
	//	loadSprites(graphicsElement);
	//	printFinishedTask("Sprites loaded");
	//}

	if (LayerObjectManager::getSingleton() != nullptr)
	{
		printCurrentTask("Loading layer objects");
		//loadLayerObjects(graphicsElement);
		printFinishedTask("Layer objects loaded");
	}
}*/

/*void LevelManager::loadGraphicsScene(XMLElement* graphicsElement)
{
	XMLElement* sceneElement = graphicsElement->FirstChildElement(tag::SCENE_ELEMENT);

	if (sceneElement) {
		int32_t cellX;
		int32_t cellY;
		float cellSize;

		//Scene2d* scene = SpriteManager::getSingleton()->getScene();
		Scene2d*  scene    = LayerObjectManager::getSingleton()->getScene();
		HashGrid* hashGrid = (HashGrid*)(scene);
		
		if (XML_NO_ERROR != sceneElement->QueryIntAttribute("cellX", &cellX)) {
			cellX = hashGrid->getCellWidth();
		}
		if (XML_NO_ERROR != sceneElement->QueryIntAttribute("cellY", &cellY)) {
			cellY = hashGrid->getCellHeight();
		}
		if (XML_NO_ERROR != sceneElement->QueryFloatAttribute("cellSize", &cellSize)) {
			cellSize = hashGrid->getCellSize();
		}
		
		hashGrid->rebuild((int16_t)(cellX), (int16_t)(cellY), cellSize);
	}
}*/

//void LevelManager::loadSprites(XMLElement* graphicsElement)
//{
//	XMLElement* spritesElement = graphicsElement->FirstChildElement(tag::SPRITE_ENTITIES_ELEMENT);
//	XMLElement* spriteElement = spritesElement->FirstChildElement(tag::SPRITE_ENTITY_ELEMENT);
//
//	for( spriteElement; spriteElement; spriteElement=spriteElement->NextSiblingElement())
//	{
//		loadSprite(spriteElement);
//	}
//}


//void LevelManager::loadSprite(XMLElement* spriteElement)
//{
//	uint16_t entityId;
//	uint16_t spriteType;
//	uint8_t  a;
//	SpriteDef* spriteDef = &SpriteManager::getSingleton()->spriteDef;
//	spriteDef->size.x = -1.0f;
//	spriteDef->size.y = -1.0f;
//
//	loadUShort(spriteElement,tag::ID_VARIABLE,entityId);
//	loadUShort(spriteElement,tag::TYPE_VARIABLE,spriteType);
//
//	spriteDef->spriteType = (SpriteType)(spriteType);
//
//	loadVector2(spriteElement,tag::POSITION_VARIABLE,spriteDef->position);
//	loadFloat(spriteElement,tag::ANGLE_VARIABLE,spriteDef->angle);
//	loadVector2(spriteElement,tag::TEXTURE_SCALE_VARIABLE,spriteDef->textureScale);
//	loadVector2(spriteElement,tag::SIZE_VARIABLE, spriteDef->size);
//	loadUChar(spriteElement,tag::LAYER_VARIABLE,spriteDef->layerId);
//	loadFloat(spriteElement,tag::DEPTH_VARIABLE,spriteDef->depth);
//	loadFloat(spriteElement,tag::ALPHA_VARIABLE,spriteDef->alpha);
//	loadColor(spriteElement,tag::COLOR_VARIABLE,spriteDef->r,spriteDef->g,spriteDef->b,a);
//	loadBool(spriteElement,tag::TEXTURE_REPEAT_VARIABLE,spriteDef->repeatTexture);
//	loadBool(spriteElement,tag::TEXTURE_FILTER_VARIABLE,spriteDef->linearFiltering);
//
//	//loadFloatArray(spriteElement, tag::SPECIAL_VARIABLE, spriteDef->special, setup::graphics::SHADER_SPECIAL_VARIABLES_NUM);
//
//	if(spriteDef->spriteType == SpriteType::SPRITE_ENTITY)
//	{
//		uint16_t textureId;
//		loadUShort(spriteElement,tag::TEXTUREID_VARIABLE,textureId);
//		spriteDef->texture = spriteDef->texture = ResourceManager::getSingleton()->getLevelTexture2dProxy(textureId);
//	}
//
//	if(spriteDef->spriteType == SpriteType::SPRITE_ANIMATOR)
//	{
//		uint16_t animationId;
//		loadUShort(spriteElement,tag::ANIMATIONID_VARIABLE, animationId);
//		spriteDef->animation = ResourceManager::getSingleton()->getLevelAnimationProxy(animationId);
//		loadUShort(spriteElement,tag::SEQUENCEID_VARIABLE,spriteDef->sequenceId);
//		loadDouble(spriteElement,tag::ELAPSED_TIME_VARIABLE,spriteDef->elapsedTime);
//		loadBool(spriteElement,tag::LOOP_VARIABLE,spriteDef->isLoop);
//	}
//
//	if(spriteDef->spriteType == SpriteType::SPRITE_PARTICLES)
//	{
//		uint16_t particleGeneratorId;
//		uint16_t textureId;
//		loadUShort(spriteElement,tag::TEXTUREID_VARIABLE,textureId);
//		spriteDef->texture = spriteDef->texture = ResourceManager::getSingleton()->getLevelTexture2dProxy(textureId);
//		loadUShort(spriteElement,tag::PARTICLEID_VARIABLE,particleGeneratorId);
//
//		spriteDef->particleGenerator = PhysicsManager::getSingleton()->getParticleGenerator(particleGeneratorId);
//	}
//
//	Sprite* sprite = SpriteManager::getSingleton()->createSpriteDef(entityId);
//}

/*void LevelManager::loadEffects(XMLElement* graphicsElement)
{
	XMLElement* effectsElement = graphicsElement->FirstChildElement(tag::EFFECTS_ELEMENT);

	printCurrentTask("Loading layer blenders");
	loadLayerBlenders(effectsElement);
	printFinishedTask("Layer blenders loaded");
	printCurrentTask("Loading layer effects");
	loadLayerEffects(effectsElement);
	printFinishedTask("Layer effects loaded");
	printCurrentTask("Loading shader binders");
	loadShaderBinders(effectsElement);
	printFinishedTask("Shader binders loaded");
	printCurrentTask("Loading FBOS");
	loadFbos(effectsElement);
	printFinishedTask("FBOS loaded");
}*/

/*void LevelManager::loadLayerBlenders(XMLElement* effectsElement)
{
	XMLElement* layerBlendersElement = effectsElement->FirstChildElement(tag::LAYER_BLENDERS_ELEMENT);
	if (layerBlendersElement)
	{
		XMLElement* layerBlenderElement = layerBlendersElement->FirstChildElement(tag::LAYER_BLENDER_VARIABLE);

		int sBlend = 0;
		int dBlend = 0;

		EffectManager* effectManager = EffectManager::getSingleton();
		const char* layerBlenderName;

		for (layerBlenderElement; layerBlenderElement; layerBlenderElement = layerBlenderElement->NextSiblingElement())
		{
			layerBlenderName = layerBlenderElement->Attribute("name");
			if (layerBlenderName == nullptr)
				continue;

			layerBlenderElement->QueryIntAttribute("sBlend", &sBlend);
			layerBlenderElement->QueryIntAttribute("dBlend", &dBlend);

			effectManager->addLayerBlender(layerBlenderName, sBlend, dBlend);
		}
	}
}*/

/*void LevelManager::loadLayerEffects(XMLElement* effectsElement)
{
	XMLElement* layerEffectsElement = effectsElement->FirstChildElement(tag::LAYER_EFFECTS_ELEMENT);
	if (layerEffectsElement)
	{
		XMLElement* layerEffectElement = layerEffectsElement->FirstChildElement(tag::LAYER_EFFECT_ELEMENT);

		for (layerEffectElement; layerEffectElement; layerEffectElement = layerEffectElement->NextSiblingElement())
		{
			loadLayerEffect(layerEffectElement);
		}
	}
}*/

/*void LevelManager::loadShaderBinders(XMLElement* effectsElement)
{
	XMLElement* shaderBindersElement = effectsElement->FirstChildElement(tag::SHADER_BINDERS_ELEMENT);
	if (shaderBindersElement)
	{
		XMLElement* shaderBinderElement = shaderBindersElement->FirstChildElement(tag::SHADER_BINDER_ELEMENT);

		for (shaderBinderElement; shaderBinderElement; shaderBinderElement = shaderBinderElement->NextSiblingElement())
		{
			loadShaderBinder(shaderBinderElement);
		}
	}
}*/

/*void LevelManager::loadFbos(XMLElement* effectsElement)
{
	XMLElement* fbosElement = effectsElement->FirstChildElement(tag::FBOS_ELEMENT);
	if (fbosElement)
	{
		XMLElement* fboElement = fbosElement->FirstChildElement(tag::FBO_ELEMENT);

		for (fboElement; fboElement; fboElement = fboElement->NextSiblingElement())
		{
			loadFbo(fboElement);
		}
	}
}*/

/*void LevelManager::loadResources(XMLElement* levelElement)
{
	XMLElement* resourcesElement = levelElement->FirstChildElement(tag::RESOURCES_ELEMENT);

	if(ResourceManager::getSingleton() != nullptr)
	{
		printCurrentTask("Loading resource groups");
		loadResourceGroups(resourcesElement); // loading resource groups
		printFinishedTask("Resource groups loaded");
		printCurrentTask("Loading textures");
		loadTextures(resourcesElement); // loading texture resources
		printFinishedTask("Textures loaded");
		//printCurrentTask("Loading animations");
		//loadAnimations(resourcesElement);
		//printFinishedTask("Animations loaded");
		printCurrentTask("Loading shaders");
		loadShaders(resourcesElement);
		printFinishedTask("Shaders loaded");
		printCurrentTask("Loading sound effects");
		loadSoundEffects(resourcesElement);
		printFinishedTask("Sound effects loaded");
		printCurrentTask("Loading sound streams");
		loadSoundStreams(resourcesElement);
		printFinishedTask("Sound streams loaded");
		printCurrentTask("Loading scripts");
		loadScripts(resourcesElement);
		printFinishedTask("Scripts loaded");
		loadCustomData(resourcesElement);
		printFinishedTask("Custom data loaded");
	}
}*/

/*void LevelManager::loadTextures(XMLElement* resourcesElement)
{
	XMLElement* texturesElement = resourcesElement->FirstChildElement(tag::TEXTURES_ELEMENT);
	if (texturesElement)
	{
		XMLElement* textureElement = texturesElement->FirstChildElement(tag::TEXTURE_ELEMENT);

		for (textureElement; textureElement; textureElement = textureElement->NextSiblingElement())
		{
			loadTexture(textureElement);
		}
	}
}*/

//void LevelManager::loadAnimations(XMLElement* resourcesElement)
//{
//	XMLElement* animationsElement = resourcesElement->FirstChildElement(tag::ANIMATIONS_ELEMENT);
//	if (animationsElement)
//	{
//		XMLElement* animationElement = animationsElement->FirstChildElement(tag::ANIMATION_ELEMENT);
//
//		for (animationElement; animationElement; animationElement = animationElement->NextSiblingElement())
//		{
//			loadAnimation(animationElement);
//		}
//	}
//}

/*void LevelManager::loadShaders(XMLElement* resourcesElement)
{
	XMLElement* shadersElement = resourcesElement->FirstChildElement(tag::SHADERS_ELEMENT);
	if (shadersElement)
	{
		XMLElement* shaderElement = shadersElement->FirstChildElement(tag::SHADER_ELEMENT);

		for (shaderElement; shaderElement; shaderElement = shaderElement->NextSiblingElement())
		{
			loadShader(shaderElement);
		}
	}
}

void LevelManager::loadScripts(XMLElement* resourcesElement)
{
	XMLElement* scriptsElement = resourcesElement->FirstChildElement(tag::SCRIPTS_ELEMENT);
	if (scriptsElement)
	{
		XMLElement* scriptElement = scriptsElement->FirstChildElement(tag::SCRIPT_ELEMENT);

		for (scriptElement; scriptElement; scriptElement = scriptElement->NextSiblingElement())
		{
			loadScript(scriptElement);
		}
	}
}

void LevelManager::loadSoundEffects(XMLElement* resourcesElement)
{
	XMLElement* soundEffectsElement = resourcesElement->FirstChildElement(tag::SOUND_EFFECTS_ELEMENT);
	if (soundEffectsElement != nullptr)
	{
		XMLElement* soundEffectElement = soundEffectsElement->FirstChildElement(tag::SOUND_EFFECT_ELEMENT);

		for (soundEffectElement; soundEffectElement; soundEffectElement = soundEffectElement->NextSiblingElement())
		{
			loadSoundEffect(soundEffectElement);
		}
	}
}

void LevelManager::loadSoundStreams(XMLElement* resourcesElement)
{
	XMLElement* musicStreamsElement = resourcesElement->FirstChildElement(tag::SOUND_STREAMS_ELEMENT);
	if (musicStreamsElement != nullptr)
	{
		XMLElement* musicStreamElement = musicStreamsElement->FirstChildElement(tag::SOUND_STREAM_ELEMENT);

		for (musicStreamElement; musicStreamElement; musicStreamElement = musicStreamElement->NextSiblingElement())
		{
			loadSoundStream(musicStreamElement);
		}
	}
}*/

/*void LevelManager::loadCustomData(XMLElement* resourcesElement)
{
	XMLElement* customDataElement = resourcesElement->FirstChildElement(tag::CUSTOM_DATA_ELEMENT);
	if (customDataElement != nullptr)
	{
		XMLElement* customTypeElement = customDataElement->FirstChildElement(tag::CUSTOM_TYPE_ELEMENT);

		for (customTypeElement; customTypeElement; customTypeElement = customTypeElement->NextSiblingElement())
		{
			loadCustomType(customTypeElement);
		}
	}
}

void LevelManager::loadTexture(XMLElement* textureElement)
{
	ja::string fileName;
	unsigned short groupId;
	unsigned short id;
	
	loadUShort(textureElement, tag::ID_VARIABLE, id);
	loadSString(textureElement, tag::FILE_VARIABLE, fileName);
	loadUShort(textureElement, tag::GROUP_VARIABLE, groupId);

	printCurrentTask("Loading " + fileName);

	ResourceManager::getSingleton()->changeLevelResourceGroup((unsigned char) groupId);
	
	try
	{
		ResourceManager::getSingleton()->loadLevelTexture2d(fileName, id);
		printFinishedTask(fileName + " loaded");
	}
	catch(Exception e)
	{
		printFailureTask("Can't load " + fileName);
	}
	catch(...)
	{
		printFailureTask("Can't load " + fileName + " undefined exception");
	}

	XMLElement* proxiesElement = textureElement->FirstChildElement(tag::TEXTURE_PROXIES_ELEMENT);

	if(proxiesElement)
	{
		XMLElement* proxyVariable = proxiesElement->FirstChildElement(tag::TEXTURE_PROXY_VARIABLE);

		for(proxyVariable; proxyVariable; proxyVariable = proxyVariable->NextSiblingElement())
		{
			unsigned int proxyId;
			ja::string proxyName;
			
			proxyVariable->QueryUnsignedAttribute("id",&proxyId);
			proxyName = proxyVariable->Attribute("filename");

			try
			{
				ResourceManager::getSingleton()->loadLevelTexture2d(id, proxyId, proxyName);
				printFinishedTask(proxyName + " loaded");
			}
			catch(Exception e)
			{
				printFailureTask("Can't load " + proxyName);
			}
			catch(...)
			{
				printFailureTask("Can't load " + proxyName + " undefined exception");
			}
		}
	}
}

void LevelManager::loadSoundEffect(XMLElement* soundEffectElement)
{
	ja::string fileName;
	unsigned short groupId;
	unsigned short id;

	loadUShort(soundEffectElement, tag::ID_VARIABLE, id);
	loadSString(soundEffectElement, tag::FILE_VARIABLE, fileName);
	loadUShort(soundEffectElement, tag::GROUP_VARIABLE, groupId);

	printCurrentTask("Loading " + fileName);

	ResourceManager::getSingleton()->changeLevelResourceGroup(static_cast<unsigned char>(groupId));

	try
	{
		ResourceManager::getSingleton()->loadLevelSoundEffect(fileName, id);
		printFinishedTask(fileName + " loaded");
	}
	catch(Exception e)
	{
		printFailureTask("Can't load " + fileName);
	}
	catch(...)
	{
		printFailureTask("Can't load " + fileName + " undefined exception");
	}
}

void LevelManager::loadSoundStream(XMLElement* musicStreamElement)
{
	ja::string fileName;
	unsigned short groupId;
	unsigned short id;

	loadUShort(musicStreamElement, tag::ID_VARIABLE, id);
	loadSString(musicStreamElement, tag::FILE_VARIABLE, fileName);
	loadUShort(musicStreamElement, tag::GROUP_VARIABLE, groupId);

	printCurrentTask("Loading " + fileName);

	ResourceManager::getSingleton()->changeLevelResourceGroup(static_cast<unsigned char>(groupId));

	try
	{
		ResourceManager::getSingleton()->loadLevelMusicStream(fileName, id);
		printFinishedTask(fileName + " loaded");
	}
	catch (Exception e)
	{
		printFailureTask("Can't load " + fileName);
	}
	catch (...)
	{
		printFailureTask("Can't load " + fileName + " undefined exception");
	}
}*/

/*void LevelManager::loadShader(XMLElement* shaderElement)
{
	ja::string fileName;
	uint16_t groupId;
	uint16_t id;
	uint8_t  shaderType;
	bool useSpecial;
	
	printCurrentTask("Loading " + fileName);

	loadUShort(shaderElement, tag::ID_VARIABLE, id);
	loadSString(shaderElement, tag::FILE_VARIABLE, fileName);
	loadUShort(shaderElement, tag::GROUP_VARIABLE, groupId);
	loadUChar(shaderElement, tag::SHADER_TYPE_VARIABLE, shaderType);
	loadBool(shaderElement, tag::SHADER_USE_SPECIAL_VARIABLE, useSpecial);

	ResourceManager::getSingleton()->changeLevelResourceGroup((unsigned char) groupId);

	try
	{
		ResourceManager::getSingleton()->loadLevelShader(fileName, (ShaderType)shaderType, id);

		XMLElement* uniforms = shaderElement->FirstChildElement("uniforms");
		if(uniforms)
		{
			const char* uniformName;
			int32_t     uniformType;
			int32_t     uniformSize;

			Resource<Shader>* shader = ResourceManager::getSingleton()->getLevelShader(id);

			XMLElement* uniform = uniforms->FirstChildElement("uniform");
			for(uniform;uniform;uniform = uniform->NextSiblingElement())
			{
				uniformName = uniform->Attribute("name");
				uniform->QueryIntAttribute("type",&uniformType);
				uniform->QueryIntAttribute("size",&uniformSize);

				shader->resource->createUniform(uniformName,(ShaderVariableType)uniformType,uniformSize);
			}
		}

		printFinishedTask(fileName + " loaded");
	}
	catch(Exception e)
	{
		printFailureTask("Can't load " + fileName);
	}
	catch(...)
	{
		printFailureTask("Can't load " + fileName + " undefined exception");
	}
}*/

/*void LevelManager::loadScript(XMLElement* scriptElement)
{
	ja::string fileName;
	ja::string functionName[setup::scripts::MAX_SCRIPT_FUNCTION_NUM];
	unsigned short groupId;
	unsigned short id;
	unsigned int functionNum = 0;

	loadUShort(scriptElement, tag::ID_VARIABLE, id);
	loadSString(scriptElement, tag::FILE_VARIABLE, fileName);
	loadUShort(scriptElement, tag::GROUP_VARIABLE, groupId);

	XMLElement* functionsElement = scriptElement->FirstChildElement(tag::FUNCTIONS_ELEMENT);

	if(functionsElement != nullptr)
	{
		XMLElement* functionElement = functionsElement->FirstChildElement(tag::FUNCTION_VARIABLE);
		for(functionElement;functionElement != nullptr; functionElement = functionElement->NextSiblingElement())
		{
			functionName[functionNum] = functionElement->Attribute(tag::TYPE_VALUE_ATTRIBUTE);
			++functionNum;
		}
	}

	printCurrentTask("Loading " + fileName);

	ResourceManager::getSingleton()->changeLevelResourceGroup((unsigned char) groupId);

	try
	{
		//ResourceManager::getSingleton()->loadLevelScript(fileName, functionName[0], id);
		Resource<Script>* resource = ResourceManager::getSingleton()->loadLevelScript(fileName.c_str());

		//for(uint32_t i=1; i<functionNum; ++i)
		//{
		//	script->addFunction(functionName[i].c_str());
		//}
		printFinishedTask(fileName + " loaded");
	}
	catch(Exception e)
	{
		printFailureTask("Can't load " + fileName);
	}
	catch(...)
	{
		printFailureTask("Can't load " + fileName + " undefined exception");
	}
}*/

//void LevelManager::loadAnimation(XMLElement* animationElement)
//{
//	ja::string fileName;
//	unsigned short groupId;
//	unsigned short id;
//
//	loadUShort(animationElement, tag::ID_VARIABLE, id);
//	loadSString(animationElement, tag::FILE_VARIABLE, fileName);
//	loadUShort(animationElement, tag::GROUP_VARIABLE, groupId);
//
//	printCurrentTask("Loading " + fileName);
//
//	ResourceManager::getSingleton()->changeLevelResourceGroup((unsigned char) groupId);
//
//	try
//	{
//		ResourceManager::getSingleton()->loadLevelAnimation(fileName, id);
//		printFinishedTask(fileName + " loaded");
//	}
//	catch(Exception e)
//	{
//		if(e.exceptionType == ExceptionType::CANT_LOAD_ANIMATION)
//			printFailureTask("Can't load " + fileName);
//		if(e.exceptionType == ExceptionType::CANT_LOAD_TEXTURE)
//			printFailureTask("Can't load " + fileName + " texture " + e.reason);
//	}
//	catch(...)
//	{
//		printFailureTask("Can't load " + fileName + " undefined exception");
//	}
//}

/*void LevelManager::loadCustomType(XMLElement* customDataElement)
{
	ja::string fileName;
	unsigned short groupId;
	unsigned short id;

	loadUShort(customDataElement, tag::ID_VARIABLE, id);
	loadSString(customDataElement, tag::FILE_VARIABLE, fileName);
	loadUShort(customDataElement, tag::GROUP_VARIABLE, groupId);

	printCurrentTask("Loading " + fileName);

	ResourceManager::getSingleton()->changeLevelResourceGroup((unsigned char) groupId);

	try
	{
		ResourceManager::getSingleton()->loadLevelCustomData(fileName, id);
		printFinishedTask(fileName + " loaded");
	}
	catch(Exception e)
	{
		if(e.exceptionType == ExceptionType::CANT_LOAD_CUSTOM_DATA)
			printFailureTask("Can't load " + fileName);
	}
	catch(...)
	{
		printFailureTask("Can't load " + fileName + " undefined exception");
	}
}

void LevelManager::loadResourceGroups(XMLElement* resourcesElement)
{
	XMLElement* resourceGroupsElement = resourcesElement->FirstChildElement(tag::RESOURCE_GROUPS_ELEMENT);
	XMLElement* resourceGroupElement  = resourceGroupsElement->FirstChildElement(tag::RESOURCE_GROUP_ELEMENT);

	for( resourceGroupElement; resourceGroupElement; resourceGroupElement=resourceGroupElement->NextSiblingElement())
	{
		loadResourceGroup(resourceGroupElement);
	}
}

void LevelManager::loadResourceGroup(XMLElement* resourceGroupElement)
{
	ja::string groupName;
	uint16_t groupId;
	uint8_t  resourceType;

	if(resourceGroupElement)
	{
		loadSString(resourceGroupElement, tag::NAME_VARIABLE, groupName);
		loadUShort(resourceGroupElement, tag::ID_VARIABLE, groupId);
		loadUInt8(resourceGroupElement, tag::TYPE_VARIABLE, resourceType);

		ResourceManager::getSingleton()->createLevelResourceGroup(groupName, groupId, resourceType);
	}
}

void LevelManager::loadColor(XMLElement* parent, const char* variableName, unsigned char& variableValueR, unsigned char& variableValueG, unsigned char& variableValueB, unsigned char& variableValueA)
{
	XMLElement* element = parent->FirstChildElement(variableName);

	if(element)
	{
		unsigned int vR,vG,vB,vA;
		element->QueryUnsignedAttribute("r",&vR);
		element->QueryUnsignedAttribute("g",&vG);
		element->QueryUnsignedAttribute("b",&vB);
		element->QueryUnsignedAttribute("a",&vA);
		variableValueR = static_cast<unsigned char>(vR);
		variableValueG = static_cast<unsigned char>(vG);
		variableValueB = static_cast<unsigned char>(vB);
		variableValueA = static_cast<unsigned char>(vA);
	}
}*/

/*void LevelManager::loadDouble(XMLElement* parent, const char* variableName, double& variableValue)
{
	XMLElement* element = parent->FirstChildElement(variableName);

	if(element)
	{
		element->QueryDoubleAttribute(tag::TYPE_VALUE_ATTRIBUTE, &variableValue);
	}
}

void LevelManager::loadBool(XMLElement* parent, const char* variableName, bool& variableValue)
{
	XMLElement* element = parent->FirstChildElement(variableName);

	if(element)
	{
		element->QueryBoolAttribute(tag::TYPE_VALUE_ATTRIBUTE, &variableValue);
	}
}

void LevelManager::loadInt(XMLElement* parent, const char* variableName, int& variableValue)
{
	XMLElement* element = parent->FirstChildElement(variableName);

	if(element)
	{
		element->QueryIntAttribute(tag::TYPE_VALUE_ATTRIBUTE,&variableValue);
	}
}

void LevelManager::loadIntArray(XMLElement* parent, const char* variableName, int* variableValue, unsigned int arraySize)
{
	XMLElement* element = parent->FirstChildElement(variableName);
	//ja::string uniformName;
	const char* valuesBuffer;
	unsigned int readArraySize;
	char valueBuffer[20];
	int readIndex = 0;
	int readNumber = 0;

	element->QueryUnsignedAttribute("size",&readArraySize);
	if(readArraySize < arraySize)
		arraySize = readArraySize;

	valuesBuffer = element->GetText();

	if(valuesBuffer == nullptr)
		return;

	while(*valuesBuffer != '\0')
	{
		if(*valuesBuffer == ';')
		{
			valueBuffer[readIndex] = '\0';

			sscanf_s(valueBuffer,"%d",&variableValue[readNumber]);

			++readNumber;
			readIndex = 0;

			if(readNumber == arraySize)
				break;
		}
		else
		{
			valueBuffer[readIndex] = *valuesBuffer;
			++readIndex;
		}
		++valuesBuffer;
	}
}

void LevelManager::loadUInt(XMLElement* parent, const char* variableName, unsigned int& variableValue)
{
	unsigned int temp;
	XMLElement* element = parent->FirstChildElement(variableName);

	if(element)
	{
		element->QueryUnsignedAttribute(tag::TYPE_VALUE_ATTRIBUTE, &temp);
		variableValue = static_cast<unsigned int>(temp);
	}
}*/

/*void LevelManager::loadUShort(XMLElement* parent, const char* variableName, unsigned short& variableValue)
{
	int temp;
	XMLElement* element = parent->FirstChildElement(variableName);

	if(element)
	{
		element->QueryIntAttribute(tag::TYPE_VALUE_ATTRIBUTE, &temp);
		variableValue = static_cast<unsigned short>(temp);
	}
}

void LevelManager::loadUChar(XMLElement* parent, const char* variableName, unsigned char& variableValue)
{
	unsigned int temp;
	XMLElement* element = parent->FirstChildElement(variableName);

	if(element)
	{
		element->QueryUnsignedAttribute(tag::TYPE_VALUE_ATTRIBUTE, &temp);
		variableValue = static_cast<unsigned char>(temp);
	}
}

void LevelManager::loadUInt8(tinyxml2::XMLElement* parent, const char* variableName, uint8_t& variableValue)
{
	uint32_t temp;
	XMLElement* element = parent->FirstChildElement(variableName);

	if (element)
	{
		element->QueryUnsignedAttribute(tag::TYPE_VALUE_ATTRIBUTE, &temp);
		variableValue = (uint8_t)temp;
	}
}

void LevelManager::loadChar(XMLElement* parent, const char* variableName, char& variableValue)
{
	int temp;
	XMLElement* element = parent->FirstChildElement(variableName);

	if(element)
	{
		element->QueryIntAttribute(tag::TYPE_VALUE_ATTRIBUTE, &temp);
		variableValue = static_cast<char>(temp);
	}	
}

void LevelManager::loadInt8(tinyxml2::XMLElement* parent, const char* variableName, int8_t& variableValue)
{
	int temp;
	XMLElement* element = parent->FirstChildElement(variableName);

	if (element)
	{
		element->QueryIntAttribute(tag::TYPE_VALUE_ATTRIBUTE, &temp);
		variableValue = static_cast<int8_t>(temp);
	}
}

void LevelManager::loadFloat(XMLElement* parent, const char* variableName, float& variableValue)
{
	XMLElement* element = parent->FirstChildElement(variableName);
	if(element)
		element->QueryFloatAttribute(tag::TYPE_VALUE_ATTRIBUTE, &variableValue);
}

void LevelManager::loadFloatArray(XMLElement* parent, const char* variableName, float* variableValue, unsigned int arraySize)
{
	XMLElement* element = parent->FirstChildElement(variableName);
	const char* valuesBuffer;
	unsigned int readArraySize;
	char valueBuffer[20];
	int readIndex = 0;
	int readNumber = 0;

	element->QueryUnsignedAttribute("size",&readArraySize);
	if(readArraySize < arraySize)
		arraySize = readArraySize;

	valuesBuffer = element->GetText();

	if(valuesBuffer == nullptr)
		return;

	while(*valuesBuffer != '\0')
	{
		if(*valuesBuffer == ';')
		{
			valueBuffer[readIndex] = '\0';

			sscanf_s(valueBuffer,"%f",&variableValue[readNumber]);
		
			++readNumber;
			readIndex = 0;

			if(readNumber == arraySize)
				break;
		}
		else
		{
			valueBuffer[readIndex] = *valuesBuffer;
			++readIndex;
		}
		++valuesBuffer;
	}
	
}

void LevelManager::loadShortArray(XMLElement* parent, const char* variableName, short* variableValue, unsigned int& arraySize)
{
	XMLElement* element = parent->FirstChildElement(variableName);

	if(element == nullptr)
	{
		arraySize = 0;
		return;
	}

	const char* valuesBuffer;
	unsigned int readArraySize;
	char valueBuffer[20];
	int readIndex = 0;
	int readNumber = 0;

	element->QueryUnsignedAttribute("size",&readArraySize);
	if(readArraySize < arraySize)
		arraySize = readArraySize;

	valuesBuffer = element->GetText();

	if(valuesBuffer == nullptr)
		return;

	while(*valuesBuffer != '\0')
	{
		if(*valuesBuffer == ';')
		{
			valueBuffer[readIndex] = '\0';

			sscanf_s(valueBuffer,"%hd",&variableValue[readNumber]);

			++readNumber;
			readIndex = 0;

			if(readNumber == arraySize)
				break;
		}
		else
		{
			valueBuffer[readIndex] = *valuesBuffer;
			++readIndex;
		}
		++valuesBuffer;
	}
}*/

/*void LevelManager::loadCharArray(XMLElement* parent, const char* variableName, char* variableValue, unsigned int arraySize)
{
	XMLElement* element = parent->FirstChildElement(variableName);
	const char* valuesBuffer;
	unsigned int readArraySize;
	char valueBuffer[20];
	int readIndex = 0;
	int readNumber = 0;

	element->QueryUnsignedAttribute("size",&readArraySize);
	if(readArraySize < arraySize)
		arraySize = readArraySize;

	valuesBuffer = element->GetText();

	if(valuesBuffer == nullptr)
		return;

	while(*valuesBuffer != '\0')
	{
		if(*valuesBuffer == ';')
		{
			valueBuffer[readIndex] = '\0';

			sscanf_s(valueBuffer,"%hhd",&variableValue[readNumber]);

			++readNumber;
			readIndex = 0;

			if(readNumber == arraySize)
				break;
		}
		else
		{
			valueBuffer[readIndex] = *valuesBuffer;
			++readIndex;
		}
		++valuesBuffer;
	}
}

void LevelManager::loadVector2(XMLElement* parent, const char* variableName, jaVector2& variableValue)
{
	XMLElement* element = parent->FirstChildElement(variableName);
	if(element)
	{
		element->QueryFloatAttribute("x",&variableValue.x);
		element->QueryFloatAttribute("y",&variableValue.y);
	}
}

void LevelManager::loadSString(XMLElement* parent, const char* variableName, ja::string& variableValue)
{
	XMLElement* element = parent->FirstChildElement(variableName);

	if(element)
		variableValue = element->Attribute(tag::TYPE_VALUE_ATTRIBUTE);
}*/

////////////////////////////////////////////



/*void LevelManager::saveResources(XMLDocument* doc, XMLElement* levelElement)
{
	XMLElement* resourcesElement = doc->NewElement(tag::RESOURCES_ELEMENT);
	levelElement->LinkEndChild(resourcesElement);

	if(ResourceManager::getSingleton() != nullptr)
	{
		saveResourceGroups(doc,resourcesElement);
		saveTextures(doc,resourcesElement);
		//saveAnimations(doc,resourcesElement);
		saveShaders(doc,resourcesElement);
		saveSoundEffects(doc,resourcesElement);
		saveScripts(doc,resourcesElement);
		saveCustomData(doc,resourcesElement);
	}
}

void LevelManager::saveTextures(XMLDocument* doc, XMLElement* resourcesElement)
{
	XMLElement* texturesElement = doc->NewElement(tag::TEXTURES_ELEMENT);
	resourcesElement->LinkEndChild(texturesElement);

	std::vector<Resource<Texture2d>*> textures;
	ResourceManager::getSingleton()->getLevelTextures2d(&textures,"");

	std::vector<Resource<Texture2d>*>::iterator it = textures.begin();

	while(it != textures.end())
	{
		saveTexture(doc, texturesElement, (*it));
		it++;
	}
}

void LevelManager::saveSoundEffects(XMLDocument* doc, XMLElement* resourcesElement)
{
	XMLElement* soundEffectsElement = doc->NewElement(tag::SOUND_EFFECTS_ELEMENT);
	resourcesElement->LinkEndChild(soundEffectsElement);

	std::vector<Resource<SoundEffect>*> soundEffects;
	ResourceManager::getSingleton()->getLevelSoundEffects(&soundEffects,"");

	std::vector<Resource<SoundEffect>*>::iterator it = soundEffects.begin();

	while(it != soundEffects.end())
	{
		saveSoundEffect(doc, soundEffectsElement, (*it));
		++it;
	}
}

//void LevelManager::saveAnimations(XMLDocument* doc, XMLElement* resourcesElement)
//{
//	XMLElement* animationsElement = doc->NewElement(tag::ANIMATIONS_ELEMENT);
//	resourcesElement->LinkEndChild(animationsElement);
//
//	std::vector<Resource<Animation>*> animations;
//	ResourceManager::getSingleton()->getLevelAnimations(&animations,"");
//
//	std::vector<Resource<Animation>*>::iterator it = animations.begin();
//
//	while(it !=  animations.end())
//	{
//		saveAnimation(doc, animationsElement, (*it));
//		it++;
//	}
//}

void LevelManager::saveTexture(XMLDocument* doc, XMLElement* texturesElement, Resource<Texture2d>* texture)
{
	XMLElement* textureElement = doc->NewElement(tag::TEXTURE_ELEMENT);
	texturesElement->LinkEndChild(textureElement);

	saveUInt16(doc, textureElement, tag::ID_VARIABLE, texture->resource->resourceId);
	saveSString(doc, textureElement, tag::FILE_VARIABLE, texture->resourceName.c_str());
	saveUInt16(doc, textureElement, tag::GROUP_VARIABLE, texture->resourceGroupId);

	if(texture->resource->hasProxy())
	{
		XMLElement* proxiesElement = doc->NewElement(tag::TEXTURE_PROXIES_ELEMENT);
		textureElement->LinkEndChild(proxiesElement);

		ResourceProxy* textureProxy = ResourceManager::getLevelTexture2dProxies(texture->resource->resourceId);

		for (unsigned char i = 0; i<setup::graphics::MAX_TEXTURE_PROXIES; ++i)
		{
			if (textureProxy->proxyResourceName[i] == "")
				continue;

			XMLElement* proxyVariable = doc->NewElement(tag::TEXTURE_PROXY_VARIABLE);
			proxiesElement->LinkEndChild(proxyVariable);

			proxyVariable->SetAttribute("id",i);
			proxyVariable->SetAttribute("filename",textureProxy->proxyResourceName[i].c_str());
		}
	}
}

void LevelManager::saveSoundEffect(XMLDocument* doc, XMLElement* soundEffectsElement, Resource<SoundEffect>* soundEffect)
{
	XMLElement* soundEffectElement = doc->NewElement(tag::SOUND_EFFECT_ELEMENT);
	soundEffectsElement->LinkEndChild(soundEffectElement);

	saveUInt16(doc, soundEffectElement, tag::ID_VARIABLE, soundEffect->resource->resourceId);
	saveSString(doc, soundEffectElement, tag::FILE_VARIABLE, soundEffect->resourceName.c_str());
	saveUInt16(doc, soundEffectElement, tag::GROUP_VARIABLE, soundEffect->resourceGroupId);

}

void LevelManager::saveShaders(XMLDocument* doc, XMLElement* resourcesElement)
{
	XMLElement* shadersElement = doc->NewElement(tag::SHADERS_ELEMENT);
	resourcesElement->LinkEndChild(shadersElement);

	std::vector<Resource<Shader>*> shaders;
	ResourceManager::getSingleton()->getLevelShaders(&shaders,"");

	std::vector<Resource<Shader>*>::iterator it = shaders.begin();

	while(it != shaders.end())
	{
		saveShader(doc, shadersElement, (*it));
		it++;
	}
}

void LevelManager::saveShader(XMLDocument* doc, XMLElement* shadersElement, Resource<Shader>* shader)
{
	XMLElement* shaderElement = doc->NewElement(tag::SHADER_ELEMENT);
	shadersElement->LinkEndChild(shaderElement);

	saveUInt16(doc, shaderElement, tag::ID_VARIABLE, shader->resource->resourceId);
	saveSString(doc, shaderElement, tag::FILE_VARIABLE, shader->resourceName.c_str());
	saveUInt16(doc, shaderElement, tag::GROUP_VARIABLE, shader->resourceGroupId);
	saveUChar(doc, shaderElement, tag::SHADER_TYPE_VARIABLE, (unsigned char)shader->resource->shaderType);
	//if use special variables save special variables

	XMLElement* uniforms = doc->NewElement("uniforms");
	
	uint32_t size = shader->resource->uniforms.getSize();
	ShaderVariable* shaderVariables = shader->resource->uniforms.getFirst();
	for (uint32_t i = 0; i < size; ++i)
	{
		XMLElement* uniform = doc->NewElement("uniform");
		uniform->SetAttribute("name", shaderVariables[i].nameTag.getName());
		uniform->SetAttribute("type", (int32_t)(shaderVariables[i].variableType));
		uniform->SetAttribute("size", shaderVariables[i].size);
		uniforms->LinkEndChild(uniform);
	}

	shaderElement->LinkEndChild(uniforms);;
}

//void LevelManager::saveAnimation(XMLDocument* doc, XMLElement* animationsElement, Resource<Animation>* animation)
//{
//	XMLElement* animationElement = doc->NewElement(tag::ANIMATION_ELEMENT);
//	animationsElement->LinkEndChild(animationElement);
//
//	saveUInt16(doc, animationElement, tag::ID_VARIABLE, animation->resource->resourceId);
//	saveSString(doc, animationElement, tag::FILE_VARIABLE, animation->resourceName.c_str());
//	saveUInt16(doc, animationElement, tag::GROUP_VARIABLE, animation->resourceGroupId);
//}

void LevelManager::saveScripts(XMLDocument* doc, XMLElement* resourcesElement)
{
	XMLElement* scriptsElement = doc->NewElement(tag::SCRIPTS_ELEMENT);
	resourcesElement->LinkEndChild(scriptsElement);

	std::vector<Resource<Script>*> scripts;
	ResourceManager::getSingleton()->getLevelScripts(&scripts,"");

	std::vector<Resource<Script>*>::iterator it = scripts.begin();

	while(it != scripts.end())
	{
		saveScript(doc, scriptsElement, (*it));
		it++;
	}
}

void LevelManager::saveCustomData(XMLDocument* doc, XMLElement* resourcesElement)
{
	XMLElement* customDataElement = doc->NewElement(tag::CUSTOM_DATA_ELEMENT);
	resourcesElement->LinkEndChild(customDataElement);

	std::vector<Resource<CustomData>*> customData;
	ResourceManager::getSingleton()->getLevelCustomData(&customData,"");

	std::vector<Resource<CustomData>*>::iterator it = customData.begin();

	while(it != customData.end())
	{
		saveCustomType(doc, customDataElement, (*it));
		it++;
	}
}

void LevelManager::saveScript(XMLDocument* doc, XMLElement* scriptsElement, Resource<Script>* script)
{
	XMLElement* scriptElement = doc->NewElement(tag::SCRIPT_ELEMENT);
	scriptsElement->LinkEndChild(scriptElement);

	saveUInt16(doc, scriptElement, tag::ID_VARIABLE, script->resource->resourceId);
	saveSString(doc, scriptElement, tag::FILE_VARIABLE, script->resourceName.c_str());
	
	//if(script->resource->functionNum != 0)
	//{
	//	XMLElement* functionsElement = doc->NewElement(tag::FUNCTIONS_ELEMENT);
	//	scriptElement->LinkEndChild(functionsElement);
	//
	//	for(int i=0; i<script->resource->functionNum; ++i)
	//	{
	//		saveSString(doc, functionsElement, tag::FUNCTION_VARIABLE, script->resource->functionName[i]);
	//	}
	//}

	saveUInt16(doc, scriptElement, tag::GROUP_VARIABLE, script->resourceGroupId);
}

void LevelManager::saveCustomType(XMLDocument* doc, XMLElement* customDataElement, Resource<CustomData>* customType)
{
	XMLElement* customTypeElement = doc->NewElement(tag::CUSTOM_TYPE_ELEMENT);
	customDataElement->LinkEndChild(customTypeElement);

	saveUInt16(doc, customTypeElement, tag::ID_VARIABLE, customType->resource->resourceId);
	saveSString(doc, customTypeElement, tag::FILE_VARIABLE, customType->resourceName.c_str());
	saveUInt16(doc, customTypeElement, tag::GROUP_VARIABLE, customType->resourceGroupId);
}

void LevelManager::saveResourceGroups(XMLDocument* doc, XMLElement* resourcesElement)
{
	XMLElement* resourceGroupsElement = doc->NewElement(tag::RESOURCE_GROUPS_ELEMENT);
	resourcesElement->LinkEndChild(resourceGroupsElement);
	std::vector<ResourceGroupProxy*> resourceGroups;
	ResourceManager::getSingleton()->getLevelResourceGroups(&resourceGroups,(uint8_t)ResourceType::RESOURCE_ALL);

	std::vector<ResourceGroupProxy*>::iterator it = resourceGroups.begin();

	while(it != resourceGroups.end())
	{
		saveResourceGroup(doc, resourceGroupsElement, (*it));
		it++;
	}
}

void LevelManager::saveResourceGroup(XMLDocument* doc, XMLElement* resourceGroupsElement, ResourceGroupProxy* resourceGroup)
{
	XMLElement* resourceGroupElement = doc->NewElement(tag::RESOURCE_GROUP_ELEMENT);
	resourceGroupsElement->LinkEndChild(resourceGroupElement);

	saveUInt16(doc, resourceGroupElement, tag::ID_VARIABLE, resourceGroup->resourceGroupId);
	saveSString(doc, resourceGroupElement, tag::NAME_VARIABLE, resourceGroup->resourceGroupName.c_str());
	saveUInt8(doc, resourceGroupElement, tag::TYPE_VARIABLE, (uint16_t)resourceGroup->resourceType);
}

void LevelManager::saveGraphics(XMLDocument* doc, XMLElement* levelElement)
{
	XMLElement* graphicsElement = doc->NewElement(tag::GRAPHICS_ELEMENT);
	levelElement->LinkEndChild(graphicsElement);

	//if (SpriteManager::getSingleton() != nullptr)
	//	saveGraphicsScene(doc, graphicsElement);
	
	if(EffectManager::getSingleton() != nullptr)
		saveEffects(doc, graphicsElement);

	//if(SpriteManager::getSingleton() != nullptr)
	//	saveSprites(doc, graphicsElement);

	if (LayerObjectManager::getSingleton() != nullptr)
		saveLayerObjects(doc, graphicsElement);
}

void LevelManager::saveGraphicsScene(XMLDocument* doc, XMLElement* graphicsElement)
{
	Scene2d* scene     = LayerObjectManager::getSingleton()->getScene();
	HashGrid* hashGrid = (HashGrid*)(scene);

	uint32_t cellX = hashGrid->getCellWidth();
	uint32_t cellY = hashGrid->getCellHeight();
	float cellSize = hashGrid->getCellSize();

	XMLElement* sceneElement = doc->NewElement(tag::SCENE_ELEMENT);
	sceneElement->SetAttribute("cellX", cellX);
	sceneElement->SetAttribute("cellY", cellY);
	sceneElement->SetAttribute("cellSize", cellSize);
	graphicsElement->LinkEndChild(sceneElement);
}

//void LevelManager::saveSprites(XMLDocument* doc, XMLElement * graphicsElement)
//{
//	XMLElement* spritesElement = doc->NewElement(tag::SPRITE_ENTITIES_ELEMENT);
//
//	graphicsElement->LinkEndChild(spritesElement);
//
//	std::list<Sprite*> spriteEntities;
//	SpriteManager::getSingleton()->getSprites(&spriteEntities);
//	std::list<Sprite*>::iterator it = spriteEntities.begin();
//
//	while(it != spriteEntities.end())
//	{
//		saveSprite(doc, spritesElement, (*it));
//		it++;
//	}
//}

//void LevelManager::saveSprite(XMLDocument* doc, XMLElement* spritesElement, Sprite* spriteEntity)
//{
//	XMLElement* spriteElement = doc->NewElement(tag::SPRITE_ENTITY_ELEMENT);
//	spritesElement->LinkEndChild(spriteElement);
//
//	saveUInt16(doc, spriteElement, tag::ID_VARIABLE, spriteEntity->spriteId);
//	saveVector2(doc, spriteElement, tag::POSITION_VARIABLE, spriteEntity->getPosition());
//	saveFloat(doc, spriteElement, tag::ANGLE_VARIABLE, spriteEntity->getAngleDeg());
//	saveVector2(doc, spriteElement, tag::TEXTURE_SCALE_VARIABLE, spriteEntity->getTextureScale());
//	saveVector2(doc, spriteElement, tag::SIZE_VARIABLE, spriteEntity->getSize());
//	saveUInt16(doc, spriteElement, tag::TYPE_VARIABLE, (uint16_t)spriteEntity->spriteType);
//	saveBool(doc, spriteElement, tag::TEXTURE_REPEAT_VARIABLE, spriteEntity->isRepeatTexture());
//	saveBool(doc, spriteElement, tag::TEXTURE_FILTER_VARIABLE, spriteEntity->isLinearFiltering());
//	saveUInt16(doc, spriteElement, tag::LAYER_VARIABLE, spriteEntity->layerId);
//	if(spriteEntity->texture != nullptr)
//	{
//		saveUInt16(doc, spriteElement, tag::TEXTUREID_VARIABLE, spriteEntity->texture->resourceId);
//	}
//	saveFloat(doc, spriteElement, tag::DEPTH_VARIABLE, spriteEntity->depth);
//	saveFloat(doc, spriteElement, tag::ALPHA_VARIABLE, spriteEntity->alpha);
//	saveColor(doc, spriteElement, tag::COLOR_VARIABLE, spriteEntity->r, spriteEntity->g, spriteEntity->b, 255);
//	//saveFloatArray(doc, spriteElement, tag::SPECIAL_VARIABLE, spriteEntity->specialValues, setup::graphics::SHADER_SPECIAL_VARIABLES_NUM);
//
//	if(spriteEntity->spriteType == SpriteType::SPRITE_ANIMATOR)
//	{
//		SpriteAnimator* animator = (SpriteAnimator*)(spriteEntity);
//		saveUInt16(doc, spriteElement, tag::ANIMATIONID_VARIABLE, animator->currentAnimation->resourceId);
//		saveUInt16(doc, spriteElement, tag::SEQUENCEID_VARIABLE, animator->currentSequence->sequenceId);
//		saveDouble(doc, spriteElement, tag::ELAPSED_TIME_VARIABLE, animator->elapsedSequenceTime);
//		saveBool(doc, spriteElement, tag::LOOP_VARIABLE, animator->isLoop);
//	}
//
//	if(spriteEntity->spriteType == SpriteType::SPRITE_PARTICLES)
//	{
//		SpriteParticles* particles = (SpriteParticles*)(spriteEntity);
//		jaVector2 particleHalfSize = particles->getParticleHalfSize();
//		saveUInt16(doc, spriteElement, tag::PARTICLEID_VARIABLE, particles->getParticleGenerator()->getId());
//	}
//}

void LevelManager::saveLayerObjects(XMLDocument* doc, XMLElement * graphicsElement)
{
	XMLElement* layerObjectsElement = doc->NewElement(tag::LAYER_OBJECTS_ELEMENT);

	graphicsElement->LinkEndChild(layerObjectsElement);

	std::list<LayerObject*> layerObjects;
	LayerObjectManager::getSingleton()->getLayerObjects(&layerObjects);
	std::list<LayerObject*>::iterator it = layerObjects.begin();

	while (it != layerObjects.end())
	{
		saveLayerObject(doc, layerObjectsElement, (*it));
		it++;
	}
}

void LevelManager::saveLayerObject(XMLDocument* doc, XMLElement* layerObjectsElement, LayerObject* layerObject)
{
	XMLElement* layerObjectElement = doc->NewElement(tag::LAYER_OBJECT_ELEMENT);
	layerObjectsElement->LinkEndChild(layerObjectElement);

	saveUInt16(doc, layerObjectElement, tag::ID_VARIABLE, layerObject->getId());
	saveVector2(doc, layerObjectElement, tag::POSITION_VARIABLE, layerObject->getPosition());
	saveFloat(doc, layerObjectElement, tag::ANGLE_VARIABLE, layerObject->getAngleDeg());
	saveVector2(doc, layerObjectElement, tag::TEXTURE_SCALE_VARIABLE, layerObject->getScale());
	//saveVector2(doc, spriteElement, tag::SIZE_VARIABLE, spriteEntity->getSize());
	//saveUShort(doc, spriteElement, tag::TYPE_VARIABLE, (uint16_t)spriteEntity->spriteType);
	//saveBool(doc, spriteElement, tag::TEXTURE_REPEAT_VARIABLE, spriteEntity->isRepeatTexture());
	//saveBool(doc, spriteElement, tag::TEXTURE_FILTER_VARIABLE, spriteEntity->isLinearFiltering());
	saveUInt16(doc, layerObjectElement, tag::LAYER_VARIABLE, layerObject->layerId);
	//if (spriteEntity->texture != nullptr)
	//{
	//	saveUShort(doc, spriteElement, tag::TEXTUREID_VARIABLE, spriteEntity->texture->resourceId);
	//}
	//saveFloat(doc, spriteElement, tag::DEPTH_VARIABLE, spriteEntity->depth);
	//saveFloat(doc, spriteElement, tag::ALPHA_VARIABLE, spriteEntity->alpha);
	//saveColor(doc, spriteElement, tag::COLOR_VARIABLE, spriteEntity->r, spriteEntity->g, spriteEntity->b, 255);
	////saveFloatArray(doc, spriteElement, tag::SPECIAL_VARIABLE, spriteEntity->specialValues, setup::graphics::SHADER_SPECIAL_VARIABLES_NUM);
	//
	//if (spriteEntity->spriteType == SpriteType::SPRITE_ANIMATOR)
	//{
	//	SpriteAnimator* animator = (SpriteAnimator*)(spriteEntity);
	//	saveUShort(doc, spriteElement, tag::ANIMATIONID_VARIABLE, animator->currentAnimation->resourceId);
	//	saveUShort(doc, spriteElement, tag::SEQUENCEID_VARIABLE, animator->currentSequence->sequenceId);
	//	saveDouble(doc, spriteElement, tag::ELAPSED_TIME_VARIABLE, animator->elapsedSequenceTime);
	//	saveBool(doc, spriteElement, tag::LOOP_VARIABLE, animator->isLoop);
	//}
	//
	//if (spriteEntity->spriteType == SpriteType::SPRITE_PARTICLES)
	//{
	//	SpriteParticles* particles = (SpriteParticles*)(spriteEntity);
	//	jaVector2 particleHalfSize = particles->getParticleHalfSize();
	//	saveUShort(doc, spriteElement, tag::PARTICLEID_VARIABLE, particles->getParticleGenerator()->getId());
	//}
}

void LevelManager::saveEffects(XMLDocument* doc, XMLElement* graphicsElement)
{
	XMLElement* effectsElement = doc->NewElement(tag::EFFECTS_ELEMENT);
	graphicsElement->LinkEndChild(effectsElement);

	saveLayerEffects(doc, effectsElement);
	saveShaderBinders(doc, effectsElement);
	saveFbos(doc, effectsElement);
	saveLayerBlenders(doc, effectsElement);
}

void LevelManager::saveLayerBlenders(XMLDocument* doc, XMLElement* effectsElement)
{
	XMLElement* layerBlendersElement = doc->NewElement(tag::LAYER_BLENDERS_ELEMENT);

	effectsElement->LinkEndChild(layerBlendersElement);

	EffectTagMapped* effectMapped    = EffectManager::getSingleton()->layerBlenders.getFirst();
	uint32_t         layerBlenderNum = EffectManager::getSingleton()->layerBlenders.getSize();

	for(uint8_t i=0; i<layerBlenderNum; ++i)
	{
		LayerBlender* layerBlender = (LayerBlender*)(effectMapped[i].pointer);
		saveLayerBlender(doc, layerBlendersElement, effectMapped->effectName.getName(), layerBlender);
	}
}

void LevelManager::saveLayerEffects(XMLDocument* doc, XMLElement* effectsElement)
{
	XMLElement* layerEffectsElement = doc->NewElement(tag::LAYER_EFFECTS_ELEMENT);

	effectsElement->LinkEndChild(layerEffectsElement);

	for (unsigned char i = 0; i<setup::graphics::LAYER_NUM; ++i)
	{
		LayerEffect* layerEffect = EffectManager::getSingleton()->getLayerEffect(i);

		if (layerEffect->renderingScript != nullptr)
			saveLayerEffect(doc, layerEffectsElement, layerEffect, i);
	}
}

void LevelManager::saveShaderBinders(XMLDocument* doc, XMLElement* effectsElement)
{
	XMLElement* shaderBindersElement = doc->NewElement(tag::SHADER_BINDERS_ELEMENT);

	effectsElement->LinkEndChild(shaderBindersElement);

	EffectTagMapped* effectMapped = EffectManager::getSingleton()->shaderBinders.getFirst();
	uint32_t shaderBindersNum = EffectManager::getSingleton()->shaderBinders.getSize();

	for (uint8_t i = 0; i < shaderBindersNum; ++i)
	{
		ShaderBinder* shaderBinder = (ShaderBinder*)(effectMapped[i].pointer);
		saveShaderBinder(doc, shaderBindersElement, effectMapped[i].effectName.getName(), shaderBinder);
	}
}

void LevelManager::saveFbos(XMLDocument* doc, XMLElement* effectsElement)
{
	XMLElement* fbosElement = doc->NewElement(tag::FBOS_ELEMENT);

	effectsElement->LinkEndChild(fbosElement);

	EffectTagMapped* effectMapped = EffectManager::getSingleton()->fbos.getFirst();
	uint32_t              fbosNum = EffectManager::getSingleton()->fbos.getSize();

	for (uint8_t i = 0; i < fbosNum; ++i)
	{
		Fbo* fbo = (Fbo*)( effectMapped[i].pointer );
		saveFbo(doc, fbosElement, effectMapped[i].effectName.getName(), fbo);
	}
}

void LevelManager::savePhysics(XMLDocument* doc, XMLElement* levelElement)
{
	XMLElement* physicsElement = doc->NewElement(tag::PHYSICS_ELEMENT);
	levelElement->LinkEndChild(physicsElement);

	savePhysicsScene(doc, physicsElement);
	saveBodies(doc, physicsElement);
	saveParticles(doc, physicsElement);
}

void LevelManager::savePhysicsScene(XMLDocument* doc, XMLElement* physicsElement)
{
	Scene2d* scene = PhysicsManager::getSingleton()->getScene();
	HashGrid* hashGrid = static_cast<HashGrid*>(scene);

	unsigned int cellX = hashGrid->getCellWidth();
	unsigned int cellY = hashGrid->getCellHeight();
	float cellSize = hashGrid->getCellSize();

	XMLElement* sceneElement = doc->NewElement(tag::SCENE_ELEMENT);
	sceneElement->SetAttribute("cellX", cellX);
	sceneElement->SetAttribute("cellY", cellY);
	sceneElement->SetAttribute("cellSize", cellSize);
	physicsElement->LinkEndChild(sceneElement);
}

void LevelManager::saveBodies(XMLDocument* doc, XMLElement* physicsElement)
{
	XMLElement* bodiesElement = doc->NewElement(tag::BODIES_ELEMENT);

	physicsElement->LinkEndChild(bodiesElement);

	std::list<RigidBody2d*> bodyEntities;
	PhysicsManager::getSingleton()->getBodies(&bodyEntities);

	std::list<RigidBody2d*>::iterator it = bodyEntities.begin();

	while(it != bodyEntities.end())
	{
		saveBody(doc, bodiesElement, (*it));
		it++;
	}
}

void LevelManager::saveEntities(XMLDocument* doc, XMLElement* levelElement)
{
	XMLElement* entitiesElement = doc->NewElement(tag::ENTITIES_ELEMENT);
	levelElement->LinkEndChild(entitiesElement);

	if(EntityManager::getSingleton() != nullptr)
	{
		//saveHashes(entitiesElement);

		std::list<Entity*> entities;
		EntityManager::getSingleton()->getEntities(&entities);

		std::list<Entity*>::iterator it = entities.begin();

		while(it != entities.end())
		{
			saveEntity(doc, entitiesElement, (*it));
			it++;
		}
	}
}

//void jaLevelManager::saveHashes(XMLElement* entitiesElement)
//{
//	XMLElement* hashTagsElement = new XMLElement(JA_HASH_TAGS_ELEMENT);
//	entitiesElement->LinkEndChild(hashTagsElement);
//	std::vector<jaHashTagValue> hashTags;
//	jaScriptManager::getSingleton()->getHashTags(hashTags);
//
//	std::vector<jaHashTagValue>::iterator itHash = hashTags.begin();
//	for(itHash; itHash != hashTags.end(); ++itHash)
//	{
//		saveSString(hashTagsElement,JA_TAG_VARIABLE,itHash->hashTagName);
//	}
//}

void LevelManager::saveParticles(XMLDocument* doc, XMLElement* physicsElement)
{
	XMLElement* particlesElement = doc->NewElement(tag::PARTICLES_ELEMENT);

	physicsElement->LinkEndChild(particlesElement);

	std::list<ParticleGenerator2d*> particleEntities;
	PhysicsManager::getSingleton()->getParticles(&particleEntities);

	std::list<ParticleGenerator2d*>::iterator it = particleEntities.begin();

	while(it != particleEntities.end())
	{
		saveParticle(doc, particlesElement, (*it));
		it++;
	}
}

void LevelManager::saveBody(XMLDocument* doc, XMLElement* bodiesElement, RigidBody2d* body)
{
	XMLElement* bodyElement = doc->NewElement(tag::BODY_ELEMENT);
	bodiesElement->LinkEndChild(bodyElement);

	saveUInt16(doc, bodyElement, tag::ID_VARIABLE, body->getId());
	saveVector2(doc, bodyElement, tag::POSITION_VARIABLE, body->getPosition());
	saveFloat(doc, bodyElement, tag::ANGLE_VARIABLE, body->getAngleDeg());
	saveUInt16(doc, bodyElement, tag::BODY_TYPE_VARIABLE, (uint16_t)body->getBodyType());
	saveBool(doc, bodyElement, tag::FIXED_ROTATION_VARIABLE, body->isFixedRotation());
	saveBool(doc, bodyElement, tag::ALLOW_SLEEP_VARIABLE, body->isSleepingAllowed());
	saveBool(doc, bodyElement, tag::IS_SLEEPING_VARIABLE, body->isSleeping());
	saveVector2(doc, bodyElement, tag::LINEAR_VELOCITY_VARIABLE, body->getLinearVelocity());
	saveFloat(doc, bodyElement, tag::ANGLUAR_VELOCITY_VARIABLE, body->getAngularVelocity());
	
	//saveFloat(doc, bodyElement, tag::RESTITUTION_VARIABLE, body->getRestitution());
	//saveFloat(doc, bodyElement, tag::FRICTION_VARIABLE, body->getFriction());

	saveFloat(doc, bodyElement, tag::LINEAR_DAMPING_VARIABLE, body->getLinearDamping());
	saveFloat(doc, bodyElement, tag::ANGLUAR_DAMPING_VARIABLE, body->getAngularDamping());
	saveVector2(doc, bodyElement, tag::GRAVITY_VARIABLE, body->getGravity());

	//saveFloat(doc, bodyElement, tag::DENSITY_VARIABLE, body->getDensity());

	saveFloat(doc, bodyElement, tag::TIME_FACTOR_VARIABLE, body->getTimeFactor());
	saveChar(doc, bodyElement, tag::GROUP_INDEX_VARIABLE, body->getGroupIndex());
	saveUInt16(doc, bodyElement, tag::CATEGORY_MASK_VARIABLE, body->getCategoryMask());
	saveUInt16(doc, bodyElement, tag::MASK_BITS_VARIABLE, body->getMaskBits());
	saveShape(doc, bodyElement, tag::SHAPE_ELEMENT, body->getShape());
}

void LevelManager::saveEntity(XMLDocument* doc, XMLElement* entitiesElement, Entity* entity)
{
	XMLElement* entityElement = doc->NewElement(tag::ENTITY_ELEMENT);
	entitiesElement->LinkEndChild(entityElement);

	saveUInt16(doc, entityElement, tag::ID_VARIABLE, entity->getId());
	PhysicObject2d* physicObject = entity->getPhysicObject();
	if(physicObject != nullptr)
		saveUInt16(doc, entityElement, tag::PHYSIC_OBJECT_ID_VARIABLE, physicObject->getId());
	
	if (entity->getHash() != 0)
	{
		saveSString(doc, entityElement, tag::HASH_TAG_VARIABLE, ScriptManager::getSingleton()->getHashTagName(entity->getHash()));
	}

	if(entity->getFirstComponent() != nullptr)
	{
		XMLElement* componentsElement = doc->NewElement(tag::COMPONENTS_ELEMENT);
		entityElement->LinkEndChild(componentsElement);

		EntityComponent* entityComponentIt = entity->getFirstComponent();
		for(entityComponentIt; entityComponentIt != nullptr; entityComponentIt = entityComponentIt->getNextComponent())
		{
			switch(entityComponentIt->getComponentType())
			{
			case (uint8_t)EntityGroupType::PHYSIC_OBJECT:
				savePhysicComponent(doc, componentsElement, entityComponentIt);
				break;
			//case JA_SPRITE_GROUP_2D:
			//	saveSpriteComponent(doc, componentsElement, entityComponentIt);
			//	break;
			case (uint8_t)EntityGroupType::SCRIPT:
				saveScriptComponent(doc, componentsElement, entityComponentIt);
				break;
			}
		}
	}
}

void LevelManager::savePhysicComponent(XMLDocument* doc, XMLElement* componentElement, EntityComponent* entityComponent)
{
	PhysicComponent* physicComponent = (PhysicComponent*)(entityComponent);

	XMLElement* physicElement = doc->NewElement(tag::PHYSICS_COMPONENT_ELEMENT);
	componentElement->LinkEndChild(physicElement);

	if(physicComponent->getHash() != 0)
		saveSString(doc, physicElement, tag::HASH_TAG_VARIABLE, ScriptManager::getSingleton()->getHashTagName(physicComponent->getHash()));

	saveBool(doc, physicElement, tag::AUTO_DELETE_VARIABLE, physicComponent->autoDelete);
	PhysicObject2d* physicObject = physicComponent->getPhysicObject();
	if(physicObject != nullptr)
		saveUInt16(doc, physicElement, tag::PHYSIC_OBJECT_ID_VARIABLE, physicObject->getId());
}

//void LevelManager::saveSpriteComponent(XMLDocument* doc, XMLElement* componentElement, EntityComponent2d* entityComponent)
//{
//	SpriteComponent2d* spriteComponent = (SpriteComponent2d*)(entityComponent);
//
//	XMLElement* spriteElement = doc->NewElement(tag::SPRITE_COMPONENT_ELEMENT);
//	componentElement->LinkEndChild(spriteElement);
//
//	if(spriteComponent->getHashTag() != 0)
//		saveSString(doc, spriteElement, tag::HASH_TAG_VARIABLE, ScriptManager::getSingleton()->getHashTagName(spriteComponent->getHashTag()));
//
//	saveBool(doc, spriteElement, tag::AUTO_DELETE_VARIABLE, spriteComponent->isAutoDelete());
//	saveBool(doc, spriteElement, tag::FIXED_ROTATION_VARIABLE, spriteComponent->isFixedRotation());
//
//	PhysicObject2d* physicObject = spriteComponent->getPhysicObject();
//	if(physicObject != nullptr)
//	{
//		saveUInt16(doc, spriteElement, tag::PHYSIC_OBJECT_ID_VARIABLE, physicObject->getId());
//	}
//	
//	SpriteElement2d* spriteElementIt = spriteComponent->getSpriteCollection();
//	int16_t spriteIdArray[256];
//	int32_t spriteNum = 0;
//	
//	for(spriteElementIt; spriteElementIt != nullptr; spriteElementIt = spriteElementIt->getNext())
//	{
//		spriteIdArray[spriteNum] = (int16_t)(spriteElementIt->sprite->getId());
//		++spriteNum;
//	}
//
//	if(spriteNum > 0)
//	{
//		saveShortArray(doc, spriteElement, tag::SPRITE_COLLECTION_VARIABLE, spriteIdArray, spriteNum);
//	}
//}

void LevelManager::saveScriptComponent(XMLDocument* doc, XMLElement* componentElement, EntityComponent* entityComponent)
{
	ScriptComponent* scriptComponent = (ScriptComponent*)(entityComponent);

	XMLElement* scriptElement = doc->NewElement(tag::SCRIPT_COMPONENT_ELEMENT);
	componentElement->LinkEndChild(scriptElement);

	if(scriptComponent->getHash() != 0)
		saveSString(doc, scriptElement, tag::HASH_TAG_VARIABLE, ScriptManager::getSingleton()->getHashTagName(scriptComponent->getHash()));

	//if(scriptComponent->script != nullptr)
	//	saveUInt(doc, scriptElement, tag::SCRIPT_ID_VARIABLE, scriptComponent->script->resourceId);
	//saveUChar(doc, scriptElement, tag::FUNCTION_ID_VARIABLE, scriptComponent->functionIndex);
}

void LevelManager::saveLayerEffect(XMLDocument* doc, XMLElement* layerEffectsElement, LayerEffect* effect, uint8_t layerId)
{
	XMLElement* layerEffectElement = doc->NewElement(tag::LAYER_EFFECT_ELEMENT);
	layerEffectsElement->LinkEndChild(layerEffectElement);
	
	saveUChar(doc, layerEffectElement, tag::LAYER_VARIABLE, layerId);
	saveBool(doc, layerEffectElement, tag::ACTIVE_VARIABLE, effect->isEffectActive);
	if (effect->renderingScript != nullptr) {
		saveUChar(doc, layerEffectElement, tag::SCRIPT_ID_VARIABLE, effect->renderingScript->resourceId);
		saveUChar(doc, layerEffectElement, tag::FUNCTION_ID_VARIABLE, effect->renderingScriptFunctionId);
	}
}

void LevelManager::saveFbo(XMLDocument* doc, XMLElement* fbosElement, const char* variableName, Fbo* fbo)
{
	XMLElement* fboElement = doc->NewElement(tag::FBO_ELEMENT);
	fbosElement->LinkEndChild(fboElement);

	fboElement->SetAttribute("name", variableName);
	int32_t resoulutionWidth = DisplayManager::getResolutionWidth();
	int32_t resolutionHeight = DisplayManager::getResolutionHeight();
	float widthPercent  = ((float)(fbo->getTextureWidth())  / (float)(resoulutionWidth)) * 100.0f;
	float heightPercent = ((float)(fbo->getTextureHeight()) / (float)(resolutionHeight)) * 100.0f;

	saveFloat(doc, fboElement, tag::FBO_WIDTH_VARIABLE, widthPercent);
	saveFloat(doc, fboElement, tag::FBO_HEIGHT_VARIABLE, heightPercent);
	saveFloat(doc, fboElement, tag::FBO_LINEAR_VARIABLE, fbo->textureLinear);


	saveBool(doc, fboElement, tag::FBO_COLOR_VARIABLE, fbo->isColorTexture() == 1);
	saveBool(doc, fboElement, tag::FBO_DEPTH_VARIABLE, fbo->isDepthTexture() == 1);
	saveBool(doc, fboElement, tag::FBO_STENCIL_VARIABLE, fbo->isStencilTexture() == 1);
}

void LevelManager::saveShaderBinder(XMLDocument* doc, XMLElement* shaderBindersElement, const char* variableName, ShaderBinder* shaderBinder)
{
	XMLElement* shaderBinderElement = doc->NewElement(tag::SHADER_BINDER_ELEMENT);
	shaderBindersElement->LinkEndChild(shaderBinderElement);

	shaderBinderElement->SetAttribute("name", variableName);
	saveUInt(doc, shaderBinderElement, tag::VERTEX_SHADER_ID_VARIABLE, shaderBinder->vertexShader->resourceId);
	saveUInt(doc, shaderBinderElement, tag::FRAGMENT_SHADER_ID_VARIABLE, shaderBinder->fragmentShader->resourceId);

	//saving uniforms
	XMLElement* uniformsElement = doc->NewElement(tag::UNIFORMS_ELEMENT);
	shaderBinderElement->LinkEndChild(uniformsElement);

	uint32_t uniformsNum = shaderBinder->uniforms.getSize();

	if (uniformsNum != 0)
	{
		ShaderVariable* shaderVariables = shaderBinder->uniforms.getFirst();

		for (uint32_t i=0; i != uniformsNum; ++i)
		{
			ShaderVariable* shaderVariable = &shaderVariables[i];
			ja::string valuesBuffer;
			char valueBuffer[60];

			XMLElement* uniformElement = doc->NewElement(tag::UNIFORM_VARIABLE);
			uniformsElement->LinkEndChild(uniformElement);

			//uniformElement->SetAttribute("name", shaderVariable->hashTag.hashTagName);
			uniformElement->SetAttribute("name", shaderVariable->nameTag.getName());
			uniformElement->SetAttribute("type", (int32_t)( shaderVariable->variableType ) );
			uniformElement->SetAttribute("size", shaderVariable->size);

			switch (shaderVariable->variableType)
			{
			case ShaderVariableType::FLOAT:
			{
				float* valuePointer = (float*)( shaderVariable->pointer );
				for (int32_t p = 0; p < shaderVariable->size; ++p)
				{
					sprintf_s(valueBuffer, 20, "%f", valuePointer[p]);
					valuesBuffer.append(valueBuffer);
					valuesBuffer.append(";");
				}
			}
				break;
			case ShaderVariableType::INT32:
			{
				int32_t* valuePointer = (int*)( shaderVariable->pointer );
				for (int32_t p = 0; p < shaderVariable->size; ++p)
				{
					sprintf_s(valueBuffer, 20, "%d", valuePointer[p]);
					valuesBuffer.append(valueBuffer);
					valuesBuffer.append(";");
				}
			}
				break;
			case ShaderVariableType::VEC3F:
			{
				float* valuePointer = (float*)( shaderVariable->pointer );
				for (int32_t p = 0; p < shaderVariable->size; ++p)
				{
					sprintf_s(valueBuffer, 60, "%f,%f,%f", valuePointer[p]);
					valuesBuffer.append(valueBuffer);
					valuesBuffer.append(";");
				}
			}
				break;
			}

			XMLText * textValue = doc->NewText(valuesBuffer.c_str());
			uniformElement->LinkEndChild(textValue);
		}
	}
}

void LevelManager::saveParticle(XMLDocument* doc, XMLElement* particlesElement, ParticleGenerator2d* particleGenerator)
{
	XMLElement* particleElement = doc->NewElement(tag::PARTICLE_ELEMENT);
	particlesElement->LinkEndChild(particleElement);

	saveUInt16(doc, particleElement, tag::EMITTERID_VARIABLE, particleGenerator->getEmitter()->getId());
	saveUInt16(doc, particleElement, tag::ID_VARIABLE, particleGenerator->getId());
	
	ParticleDef2d* particleDef = &particleGenerator->particleDef;
	saveFloat(doc, particleElement, tag::ADDITIONAL_BOUND_TO_IMAGE_VARIABLE, particleDef->additionalBoundToImage);
	saveBool(doc, particleElement, tag::AFFECTED_BY_EXPLOSION_VARIABLE, particleDef->affectedByExplosion);
	saveFloat(doc, particleElement, tag::ALPHA_OVER_TIME_VARIABLE, particleDef->alphaOverTime);
	saveVector2(doc, particleElement, tag::GRAVITY_VARIABLE, particleDef->gravity);
	saveFloat(doc, particleElement, tag::ALPHA_VARIABLE, particleDef->initAlpha);
	saveFloat(doc, particleElement, tag::ALPHA_OVER_TIME_VARIABLE, particleDef->initAlphaRandom);
	saveFloat(doc, particleElement, tag::ANGLUAR_VELOCITY_VARIABLE, particleDef->initAngluarVelocity);
	saveFloat(doc, particleElement, tag::ANGLUAR_VELOCITY_RANDOM_VARIABLE, particleDef->initAngluarVelocityRandom);
	saveFloat(doc, particleElement, tag::LIFE_VARIABLE, particleDef->initParticleLife);
	saveFloat(doc, particleElement, tag::LIFE_RANDOM_VARIABLE, particleDef->initParticleLifeRandom);
	saveFloat(doc, particleElement, tag::TEXTURE_SCALE_VARIABLE, particleDef->initScale);
	saveFloat(doc, particleElement, tag::SCALE_RANDOM_VARIABLE, particleDef->initScaleRandom);
	saveVector2(doc, particleElement, tag::LINEAR_VELOCITY_VARIABLE, particleDef->initVelocity);
	saveVector2(doc, particleElement, tag::LINEAR_VELOCITY_RANDOM_VARIABLE, particleDef->initVelocityRandom);
	saveBool(doc, particleElement, tag::IS_COLLIDING_VARIABLE, particleDef->isColliding);
	saveFloat(doc, particleElement, tag::ANGLUAR_DAMPING_VARIABLE, particleDef->particleAngluarDamping);
	saveFloat(doc, particleElement, tag::FRICTION_VARIABLE, particleDef->particleFriction);
	saveUChar(doc, particleElement, tag::GROUP_VARIABLE, particleDef->particleGroup);
	saveFloat(doc, particleElement, tag::LINEAR_DAMPING_VARIABLE, particleDef->particleLinearDamping);
	saveUInt16(doc, particleElement, tag::MAX_NUMBER_VARIABLE, particleDef->maxParticlesNum);
	saveFloat(doc, particleElement, tag::RESTITUTION_VARIABLE, particleDef->particleRestitution);
	saveFloat(doc, particleElement, tag::REMOVE_ON_COLLIDE_VARIABLE, particleDef->removeOnCollide);
	saveFloat(doc, particleElement, tag::SCALE_OVER_TIME_VARIABLE, particleDef->scaleOverTime);
	saveFloat(doc, particleElement, tag::SPAWN_DELAY_VARIABLE, particleDef->spawnDelay);
	saveFloat(doc, particleElement, tag::SPAWN_DELAY_RANDOM_VARIABLE, particleDef->spawnDelayRandom);
	saveBool(doc, particleElement, tag::VELOCITY_AFFECTED_BY_EMITTER_VARIABLE, particleDef->velocityAffectedByEmmiter);
	saveBool(doc, particleElement, tag::VELOCITY_AFFECTED_BY_EMITTER_VELOCITY_VARIABLE, particleDef->velocityAffectedByEmmiterVelocity);
	saveFloat(doc, particleElement, tag::TIME_FACTOR_VARIABLE, particleGenerator->getTimeFactor());
	saveChar(doc, particleElement, tag::GROUP_INDEX_VARIABLE, particleGenerator->getGroupIndex());
	saveUInt16(doc, particleElement, tag::CATEGORY_MASK_VARIABLE, particleGenerator->getCategoryMask());
	saveUInt16(doc, particleElement, tag::MASK_BITS_VARIABLE, particleGenerator->getMaskBits());
}

void LevelManager::saveLayerBlender(XMLDocument* doc, XMLElement* parent, const char* variableName, LayerBlender* layerBlender)
{
	XMLElement* layerBlenderElement = doc->NewElement(tag::LAYER_BLENDER_VARIABLE);
	parent->LinkEndChild(layerBlenderElement);
	layerBlenderElement->SetAttribute("name", variableName);
	layerBlenderElement->SetAttribute("sBlend",layerBlender->getSBlendingMode());
	layerBlenderElement->SetAttribute("dBlend",layerBlender->getDBlendingMode());
}

void LevelManager::saveColor(XMLDocument* doc, XMLElement* parent, const char* variableName, uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	XMLElement* colorElement = doc->NewElement(variableName);
	parent->LinkEndChild(colorElement);
	colorElement->SetAttribute("r",r);
	colorElement->SetAttribute("g",g);
	colorElement->SetAttribute("b",b);
	colorElement->SetAttribute("a",a);
}

void LevelManager::saveDouble(XMLDocument* doc, XMLElement* parent, const char* variableName, double variableValue)
{
	XMLElement* doubleElement = doc->NewElement(variableName);
	parent->LinkEndChild(doubleElement);
	doubleElement->SetAttribute(tag::TYPE_VALUE_ATTRIBUTE,variableValue);
}

void LevelManager::saveBool(XMLDocument* doc, XMLElement* parent, const char* variableName, bool variableValue)
{
	XMLElement* boolElement = doc->NewElement(variableName);
	parent->LinkEndChild(boolElement);
	boolElement->SetAttribute(tag::TYPE_VALUE_ATTRIBUTE,variableValue);
}

void LevelManager::saveInt32(XMLDocument* doc, XMLElement* parent, const char* variableName, int32_t variableValue)
{
	XMLElement* intElement = doc->NewElement(variableName);
	parent->LinkEndChild(intElement);
	intElement->SetAttribute(tag::TYPE_VALUE_ATTRIBUTE,variableValue);
}

void LevelManager::saveIntArray(XMLDocument* doc, XMLElement* parent, const char* variableName, int* valuePointer, unsigned int arraySize)
{
	ja::string valuesBuffer;
	char valueBuffer[20];

	XMLElement* intDataElement = doc->NewElement(variableName);
	parent->LinkEndChild(intDataElement);
	intDataElement->SetAttribute("size",arraySize);

	for(unsigned int p=0; p<arraySize; ++p)
	{
		sprintf_s(valueBuffer,20,"%d",valuePointer[p]);
		valuesBuffer.append(valueBuffer);
		valuesBuffer.append(";");
	}
	
	XMLText * textValue = doc->NewText(valuesBuffer.c_str());
	intDataElement->LinkEndChild(textValue);
}

void LevelManager::saveUInt(XMLDocument* doc, XMLElement* parent, const char* variableName, unsigned int variableValue)
{
	XMLElement* uIntElement = doc->NewElement(variableName);
	parent->LinkEndChild(uIntElement);
	uIntElement->SetAttribute(tag::TYPE_VALUE_ATTRIBUTE,variableValue);
}

void LevelManager::saveUInt16(XMLDocument* doc, XMLElement* parent, const char* variableName, uint16_t variableValue)
{
	XMLElement* uShortElement = doc->NewElement(variableName);
	parent->LinkEndChild(uShortElement);
	uShortElement->SetAttribute(tag::TYPE_VALUE_ATTRIBUTE,variableValue);
}

void LevelManager::saveUChar(XMLDocument* doc, XMLElement* parent, const char* variableName, unsigned char variableValue)
{
	XMLElement* uCharElement = doc->NewElement(variableName);
	parent->LinkEndChild(uCharElement);
	uCharElement->SetAttribute(tag::TYPE_VALUE_ATTRIBUTE,variableValue);
}

void LevelManager::saveUInt8(tinyxml2::XMLDocument* doc, tinyxml2::XMLElement* parent, const char* variableName, uint8_t variableValue)
{
	XMLElement* uInt8Element = doc->NewElement(variableName);
	parent->LinkEndChild(uInt8Element);
	uInt8Element->SetAttribute(tag::TYPE_VALUE_ATTRIBUTE, variableValue);
}

void LevelManager::saveChar(XMLDocument* doc, XMLElement* parent, const char* variableName, char variableValue)
{
	XMLElement* charElement = doc->NewElement(variableName);
	parent->LinkEndChild(charElement);
	charElement->SetAttribute(tag::TYPE_VALUE_ATTRIBUTE,variableValue);
}

void LevelManager::saveVector2(XMLDocument* doc, XMLElement* parent, const char* variableName, jaVector2& variableValue)
{
	XMLElement* vector2Element = doc->NewElement(variableName);
	parent->LinkEndChild(vector2Element);
	vector2Element->SetAttribute("x",variableValue.x);
	vector2Element->SetAttribute("y",variableValue.y);
}

void LevelManager::saveFloat(XMLDocument* doc, XMLElement* parent, const char* variableName, float variableValue)
{
	XMLElement* floatElement = doc->NewElement(variableName);
	parent->LinkEndChild(floatElement);
	floatElement->SetAttribute(tag::TYPE_VALUE_ATTRIBUTE,variableValue);
}

void LevelManager::saveFloatArray(XMLDocument* doc, XMLElement* parent, const char* variableName, float* valuePointer, unsigned int arraySize)
{
	ja::string valuesBuffer;
	char valueBuffer[20];

	XMLElement* floatDataElement = doc->NewElement(variableName);
	parent->LinkEndChild(floatDataElement);
	floatDataElement->SetAttribute("size",arraySize);

	for(unsigned int p=0; p<arraySize; ++p)
	{
		sprintf_s(valueBuffer,20,"%f",valuePointer[p]);
		valuesBuffer.append(valueBuffer);
		valuesBuffer.append(";");
	}

	XMLText * textValue = doc->NewText(valuesBuffer.c_str());
	floatDataElement->LinkEndChild(textValue);
}

void LevelManager::saveShortArray(XMLDocument* doc, XMLElement* parent, const char* variableName, short* valuePointer, unsigned int arraySize)
{
	ja::string valuesBuffer;
	char valueBuffer[20];

	XMLElement* shortDataElement = doc->NewElement(variableName);
	parent->LinkEndChild(shortDataElement);
	shortDataElement->SetAttribute("size",arraySize);

	for(unsigned int p=0; p<arraySize; ++p)
	{
		sprintf_s(valueBuffer,20,"%hd",valuePointer[p]);
		valuesBuffer.append(valueBuffer);
		valuesBuffer.append(";");
	}

	XMLText * textValue = doc->NewText(valuesBuffer.c_str());
	shortDataElement->LinkEndChild(textValue);
}

void LevelManager::saveCharArray(XMLDocument* doc, XMLElement* parent, const char* variableName, char* valuePointer, unsigned int arraySize)
{
	ja::string valuesBuffer;
	char valueBuffer[20];

	XMLElement* charDataElement = doc->NewElement(variableName);
	parent->LinkEndChild(charDataElement);
	charDataElement->SetAttribute("size",arraySize);

	for(unsigned int p=0; p<arraySize; ++p)
	{
		sprintf_s(valueBuffer,20,"%hhd",valuePointer[p]);
		valuesBuffer.append(valueBuffer);
		valuesBuffer.append(";");
	}
	
	XMLText * textValue = doc->NewText(valuesBuffer.c_str());
	charDataElement->LinkEndChild(textValue);
}

void LevelManager::saveSString(XMLDocument* doc, XMLElement* parent, const char* variableName, const char* variableValue)
{
	XMLElement* sstringElement = doc->NewElement(variableName);
	parent->LinkEndChild(sstringElement);
	sstringElement->SetAttribute(tag::TYPE_VALUE_ATTRIBUTE,variableValue);
}

void LevelManager::saveShape(XMLDocument* doc, XMLElement* parent, const char* variableName, Shape2d* variableValue)
{
	XMLElement* shapeElement = doc->NewElement(variableName);

	switch(variableValue->getType())
	{
	case (uint8_t)ShapeType::Box:
		shapeElement->SetAttribute("shapeType","box");
		saveBox(doc, shapeElement,variableValue);
		break;
	case (uint8_t)ShapeType::Circle:
		shapeElement->SetAttribute("shapeType","circle");
		saveCircle(doc, shapeElement, variableValue);
		break;
	}

	parent->LinkEndChild(shapeElement);
}

void LevelManager::saveBox(XMLDocument* doc, XMLElement* parent, Shape2d* shape)
{
	Box2d* box = (Box2d*)shape;
	saveFloat(doc, parent, "width", box->getWidth());
	saveFloat(doc, parent, "height", box->getHeight());
}

void LevelManager::saveCircle(XMLDocument* doc, XMLElement* parent, Shape2d* shape)
{
	Circle2d* circle = (Circle2d*)shape;
	saveFloat(doc, parent, "radius", circle->getRadius());
}*/

}


