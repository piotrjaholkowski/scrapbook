#pragma once

#include "../jaSetup.hpp"
#include "../Allocators/StackAllocator.hpp"
#include "../Allocators/HashMap.hpp"
#include "../Graphics/ProjectedShadow.hpp"
#include "../Graphics/Layer.hpp"
#include "../Graphics/Parallax.hpp"
#include "../Graphics/Fbo.hpp"
#include "../Graphics/Light.hpp"
#include "../Graphics/Parallax.hpp"
#include "../Graphics/RenderBuffer.hpp"

#include "../Utility/NameTag.hpp"

namespace ja
{
	typedef NameTag<setup::graphics::EFFECT_TAG_LENGTH> EffectTag;	

	class EffectManager
	{
	private:
		Fbo*      fboMultisampled;
		Fbo*      fbo;
		Fbo*      pingPongFbo;
	
		StackAllocator<Layer>    layers;
		StackAllocator<Parallax> parallaxes;
	
		RenderBuffer    renderBuffer;
		ProjectedShadow projectedShadow;

		ShaderBinder* defaultShaderBinder;
		Material*     defaultMaterial;
	
		uint32_t quadVertexArray;
		uint32_t quadVertexBuffer;

		uint32_t colorTexture;
		uint32_t glowTexture;

		uint32_t colorMultisampledTexture;
		uint32_t glowMultisampledTexture;

		uint32_t pingPongTexture[2];
	private:

		static EffectManager* singleton;
		LayerRenderer         parallaxRenderer; // used for rendering parallaxes

		EffectManager();
		
		void calculateProjectedShadows(LayerRenderer* layerRenderer);
		void renderQuad();
	public:
		~EffectManager();
		static void init();
		static inline EffectManager* EffectManager::getSingleton()
		{
			return singleton;
		}

		uint8_t       getLayersNum();
		Layer*        getLayer(uint8_t layerId);
		uint8_t       getParallaxesNum();
		Parallax*     getParallax(uint8_t parallaxId);
		uint8_t       createLayer(const char* name);
		uint8_t       createParallax(const char* name, Camera* camera, bool isActiveCamera);
		void          deleteLayer(Layer* layer);
		void          deleteParallax(Parallax* parallax);
		uint8_t       getLayerId(Layer* layer);
		uint8_t       getParallaxId(Parallax* parallax);

		void renderEffects(LayerRenderer* layerRenderer);

		ShaderBinder* getDefaultShaderBinder();
		Material*     getDefaultMaterial();

		void clearEffects();
		static void cleanUp();

		friend class LevelManager;
	};

}