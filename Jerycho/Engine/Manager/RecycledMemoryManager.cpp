#include "RecycledMemoryManager.hpp"

RecycledMemoryManager* RecycledMemoryManager::singleton = nullptr;

void RecycledMemoryManager::initRecycledMemoryManager()
{
	if (singleton == nullptr)
	{
		singleton = new RecycledMemoryManager();
	}
}

void RecycledMemoryManager::cleanUp()
{
	if (singleton != nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

RecycledMemoryManager::RecycledMemoryManager()
{
	
}

RecycledMemoryManager::~RecycledMemoryManager()
{
	RecycledMemoryBlock** itMemoryBlock = memoryBlocks.getFirst();
	uint32_t size = memoryBlocks.getSize();

	for (uint32_t i = 0; i < size; ++i) {
		free(itMemoryBlock[i]);
	}
}

void* RecycledMemoryManager::getMemoryBlock(uint32_t memoryBlockSize)
{
	RecycledMemoryBlock** itMemoryBlock = memoryBlocks.getFirst();
	uint32_t size = memoryBlocks.getSize();

	for (uint32_t i = 0; i < size; ++i) {
		if ((itMemoryBlock[i]->memoryBlockSize >= memoryBlockSize) && (!itMemoryBlock[i]->isInUse))
		{
			itMemoryBlock[i]->isInUse = true;
			return itMemoryBlock[i]->memoryPointer;
		}
	}

	uint32_t biggestBlock = 0;
	int32_t recycledMemoryBlock = -1;
	for (uint32_t i = 0; i < size; ++i) {
		if ((itMemoryBlock[i]->memoryBlockSize > biggestBlock) && (!itMemoryBlock[i]->isInUse)) {
			recycledMemoryBlock = i;
			biggestBlock = itMemoryBlock[i]->memoryBlockSize;
		}
	}

	int8_t* allocatedMemory = (int8_t*)(malloc(memoryBlockSize + sizeof(RecycledMemoryBlock)));
	RecycledMemoryBlock* newMemoryBlock = new(allocatedMemory)RecycledMemoryBlock();
	newMemoryBlock->memoryBlockSize = memoryBlockSize;
	newMemoryBlock->memoryPointer = allocatedMemory + sizeof(RecycledMemoryBlock);
	newMemoryBlock->isInUse = true;

	if (recycledMemoryBlock != -1) {
		free(itMemoryBlock[recycledMemoryBlock]);
		itMemoryBlock[recycledMemoryBlock] = newMemoryBlock;
	}
	else {
		memoryBlocks.push(newMemoryBlock);
	}

	assert(memoryBlocks.getSize() <= 16); // simple assert to find places where memory blocks are not released

	return newMemoryBlock->memoryPointer;
}

void RecycledMemoryManager::releaseMemoryBlock(void* recycledMemoryBlock)
{
	RecycledMemoryBlock* memoryBlock = (RecycledMemoryBlock*)( ((int8_t*)(recycledMemoryBlock) - sizeof(RecycledMemoryBlock)) );
	memoryBlock->isInUse = false;
}