#include "LayerObjectManager.hpp"

#include "DisplayManager.hpp"
#include "../Gilgamesh/DebugDraw.hpp"
#include "../Graphics/Polygon.hpp"
#include "../Tests/TimeStamp.hpp"

namespace ja
{
	
LayerObjectManager* LayerObjectManager::singleton = nullptr;

LayerObjectManager::LayerObjectManager(LayerObjectManagerConfiguration& configuration)
{
	this->scene2d = configuration.scene2d;

	this->layerObjectAllocator = configuration.commonAllocator;
	this->drawElementAllocator = configuration.drawElementAllocator;

	memcpy(this->layerObjectCreators, configuration.layerObjectCreator, sizeof(this->layerObjectCreators));

	memset(layerObjects, 0, setup::graphics::MAX_LAYER_OBJECTS * sizeof(LayerObject*));
	memset(layerObjectsFlags, 0, setup::graphics::MAX_LAYER_OBJECTS * sizeof(uint8_t));

	this->layerObjectDef.init(this);
}

LayerObjectManager::~LayerObjectManager()
{
	delete scene2d; // this doesn't delete common allocator
	delete layerObjectAllocator; // delete common allocator
	delete drawElementAllocator;
}

void LayerObjectManager::init(LayerObjectManagerConfiguration& configuration)
{
	if (singleton == nullptr)
	{
		singleton = new LayerObjectManager(configuration);
	}
}

void LayerObjectManager::cleanUp()
{
	if (singleton != nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void LayerObjectManager::getLayerObjects(StackAllocator<gil::ObjectHandle2d*>* objectList, CameraInterface* camera)
{
	gil::AABB2d camAABB;
	camera->getBoundingBox(&camAABB);
	scene2d->getObjectsAtRectangle(objectList, camAABB);
}

void LayerObjectManager::drawHashGrid(CameraInterface* camera)
{
	gil::Debug::drawHashGrid(camera, (gil::HashGrid*)(scene2d), false);
}

void LayerObjectManager::getLayerObjects(StackAllocator<LayerObject*>& layerObjects) const
{
	for (uint32_t i = 0; i< setup::graphics::MAX_LAYER_OBJECTS; i++)
	{
		if (this->layerObjects[i] != nullptr)
		{
			layerObjects.push(this->layerObjects[i]);
		}
	}
}

void LayerObjectManager::clearMem()
{
	this->layerObjectAllocator->clear();
	this->drawElementAllocator->clear();

	this->layerObjectDef.init(this);
}

void LayerObjectManager::clearLayerObjects()
{
	for (uint32_t i = 0; i< setup::graphics::MAX_LAYER_OBJECTS; i++)
	{
		if (this->layerObjects[i] != nullptr)
		{
			deleteLayerObjectImmediate(this->layerObjects[i]);
		}
	}

	scene2d->clearIdGenerator();

	clearMem();

	LayerObjectMorph::clearDictionary();
}

void LayerObjectManager::getLayerObjectsAtPoint(const jaVector2& point, StackAllocator<gil::ObjectHandle2d*>* polygonList)
{
	scene2d->getObjectsAtPoint(polygonList, point);
}

void LayerObjectManager::getLayerObjectsAtAABB(const gil::AABB2d& aabb, StackAllocator<gil::ObjectHandle2d*>* objectList)
{
	scene2d->getObjectsAtRectangle(objectList, aabb);
}

void LayerObjectManager::getLayerObjectsAtShape(const jaTransform2& transform, gil::Shape2d* shape, StackAllocator<gil::ObjectHandle2d*>* objectList)
{
	scene2d->getObjectsAtShape(objectList, transform, shape);
}

void LayerObjectManager::update(float deltaTime)
{
	PROFILER_FUN;

	for (uint32_t i = 0; i < setup::graphics::MAX_LAYER_OBJECTS; ++i)
	{
		if (0 != layerObjectsFlags[i])
		{
			if (layerObjectsFlags[i] & (uint8_t)LayerObjectFlag::DELETE_OBJECT)
			{
				deleteLayerObjectImmediate(layerObjects[i]);
				continue;
			}

			if (layerObjectsFlags[i] & (uint8_t)LayerObjectFlag::UPDATE_TRANSFORM)
			{
				updateLayerObjectImmediate(layerObjects[i]);
				//layerObjectsFlags[i] ^= (uint8_t)LayerObjectFlag::UPDATE_TRANSFORM;
			}

			layerObjectsFlags[i] = 0;
		}
	}
}


LayerObject* LayerObjectManager::createLayerObjectDef(uint16_t layerObjectId)
{
	LayerObject* layerObject = nullptr;

	layerObject = layerObjectCreators[(uint8_t)(layerObjectDef.layerObjectType)](this->layerObjectAllocator, this->layerObjectDef, this);

	if (layerObjectId == setup::graphics::ID_UNASSIGNED)
	{
		layerObjectId = layerObject->getId();
	}

	layerObject->id = layerObjectId;
	this->layerObjects[layerObjectId] = layerObject;

	return layerObject;
}

}