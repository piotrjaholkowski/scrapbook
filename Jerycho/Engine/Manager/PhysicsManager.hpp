#pragma once

#include <stdio.h>
#include "../jaSetup.hpp"
#include "../Physics/RigidBody2d.hpp"
#include "../Physics/ParticleGenerator2d.hpp"

#include "../Physics/Constraints2d.hpp"
#include "../Physics/RigidContact2d.hpp"
#include "../Physics/RevoluteJoint2d.hpp"
#include "../Physics/DistanceJoint2d.hpp"
#include "../Physics/LinearMotor2d.hpp"

#include "../Gilgamesh/Broad/HashGrid.hpp"
#include "../Allocators/StackAllocator.hpp"
#include "../Allocators/CacheLineAllocator.hpp"
#include "../Interface/CameraInterface.hpp"

#include "../Physics/PhysicObjectDef2d.hpp"

namespace ja
{

	typedef Constraint2d*(*ConstraintCreatorT)(CacheLineAllocator*, ConstraintDef2d*);
	typedef  void(*DrawUserPhysicsPtr)(PhysicObject2d*);

	class PhysicsManagerConfiguration
	{
	public:
		CacheLineAllocator*		sceneAllocator;
		CacheLineAllocator*     physicBodyAllocator;
		CacheLineAllocator*		constraintAllocator;
		CacheLineAllocator*     temporaryShapeAllocator; // This allocator holds temporary shapes of multi shape
		ConstraintCreatorT		constraintCreator[setup::physics::MAX_CONSTRAINT_CREATOR];
		gil::Scene2d*		    scene2d;

		PhysicsManagerConfiguration()	{
			for (uint32_t i = 0; i < setup::physics::MAX_CONSTRAINT_CREATOR; ++i)	{
				constraintCreator[i] = nullptr;
			}
		}
	};

	class PhysicsDefaultConfiguration : public PhysicsManagerConfiguration
	{
	public:
		PhysicsDefaultConfiguration() : PhysicsManagerConfiguration()	{
		
			physicBodyAllocator     = new CacheLineAllocator(setup::physics::PHYSICS_OBJECT_ALLOCATOR_SIZE,4);
			sceneAllocator          = new CacheLineAllocator(setup::gilgamesh::SCENE_ALLOCATOR_SIZE, 16);
			temporaryShapeAllocator = new CacheLineAllocator(setup::physics::MULTI_SHAPE_ALLOCATOR_SIZE, 16);

			const int16_t bucketWidth  = 10;
			const int16_t bucketHeight = 10;
			const jaFloat pixelsPerCell = 256.0f;
			const jaFloat cellSize = pixelsPerCell / (jaFloat)setup::graphics::PIXELS_PER_UNIT;

			scene2d = new HashGrid(bucketWidth, bucketHeight, cellSize, sceneAllocator);
			scene2d->setNarrowConfiguration(gilDefault2dConfiguration());
			constraintAllocator = new CacheLineAllocator(setup::physics::MAX_PHYSICS_CONSTRAINT_ALLOCATOR, 4);
			constraintCreator[JA_REVOLUTE_JOINT2D] = RevoluteJoint2d::allocateConstraint;
			constraintCreator[JA_LINEAR_MOTOR2D]   = LinearMotor2d::allocateConstraint;
			constraintCreator[JA_DISTANCE_JOINT2D] = DistanceJoint2d::allocateConstraint;
		}
	};

	class PhysicsManager	{
	private:
		PhysicObject2d*				physicObjects[setup::physics::MAX_PHYSIC_OBJECT];
		Constraint2d*				constraints[setup::physics::MAX_PHYSIC_OBJECT];
		ConstraintCreatorT			constraintCreator[setup::physics::MAX_CONSTRAINT_CREATOR];
	public:
		bool						isWarmStart;
		bool						isShockPropagation;
	private:
		uint32_t     				velocityIterations;
		uint32_t     				positionIterations;
		uint8_t     				constraintSynchronizer; // This field is used during constraint island generation
		Scene2d*					scene2d;

		ConstraintIslandCache2d		islandToSleep[setup::physics::MAX_ISLAND_TO_SLEEP_CACHE]; // Copy of island id, cache and sleep time
		uint32_t    				sleepIslandNum;

		CacheLineAllocator*			constraintAllocator;
		CacheLineAllocator*         islandAllocator;
		CacheLineAllocator*	        physicsBodyAllocator;    
		CacheLineAllocator*         sceneAllocator;          // This is a common allocator from gilScene2d configuration
		CacheLineAllocator*         temporaryShapeAllocator; // This is allocator which holds temporaty shapes of multi shape

		static PhysicsManager*		singleton;

		uint16_t    				      constraintIslandCounter;
		ConstraintIsland2d*			      constraintIsland;
		StackAllocator<gil::Contact2d>    contactList;
		StackAllocator<ja::Constraint2d*> constraintsToDelete;

		void getBodies(std::list<RigidBody2d*>* bodiesList);
		void getParticles(std::list<ParticleGenerator2d*>* particlesList);
		void clearPhysicObjects();
		void clearIslandsData();
		void clearConstraints();
		void clearMem();

		void integrateSingleThread(jaFloat deltaTime);
		void integrateMultipleThread(jaFloat deltaTime);
		void integrate(jaFloat deltaTime);
		void computeForcesSingleThread(jaFloat deltaTime);
		void computeForcesMultipleThread(jaFloat deltaTime);
		void computeForces(jaFloat deltaTime);
		void generateContactsSingleThread();
		void generateContactsMultipleThread();
		void generateContacts();
		void generateConstraintIslandsSingleThread();
		void generateConstraintIslands();
		void removeOldConstraints();
		void shockPropagation();

		ConstraintIsland2d* addToConstraintIsland(Constraint2d* constraint, ConstraintIsland2d* island);

		void resolveConstraints(jaFloat deltaTime);
			void preStepPhase(jaFloat deltaTime);
			void resolveVelocityConstraints(jaFloat deltaTime);
			void resolvePositionConstraints(jaFloat deltaTime);
			void resolveConstraintsSingleThread(jaFloat deltaTime);
			void resolveConstraintsMultipleThread(jaFloat deltaTime);
	public:
		void makePhysicObjectSleep(PhysicObject2d* physicObject);

		void wakeUpPhysicObject(PhysicObject2d* physicObject);
	private:
		void makeIslandSleep(ConstraintIsland2d* island);
		void makeIslandSleep(jaFloat deltaTime);
		void wakeUpConnectedConstraints(Constraint2d* constraint);
		void clearForcesSingleThread();
		void clearForcesMultipleThread();
		void clearForces();
		void addConstraint(Constraint2d* constraint);
		inline ConstraintIsland2d* getUnitedIsland(Constraint2d* constraint, uint8_t constraintSynchronizer);
		inline ConstraintIsland2d* makeUnion(ConstraintIsland2d* islandA, ConstraintIsland2d* islandB);

		PhysicsManager(PhysicsManagerConfiguration& configuration);
		void deleteParticleGeneratorImmediate(ParticleGenerator2d* particleGenerator);
		void deleteBodyImmediate(RigidBody2d* body);

		inline ObjectHandle2d* createObjectOfType(BodyType bodyType, const Shape2d* shape, const jaTransform2& tr, bool isSleeping);
	public:
		PhysicObjectDef2d physicObjectDef;
		ConstraintDef2d   constraintDef;
		ParticleDef2d     particleDef;

		//debug
		StackAllocator<ObjectHandle2d*> debugList;
		//bool debugConstraints;
		bool fixConstraintPosition;
		bool drawShapes;
		bool drawAABB;
		bool drawVelocities;
		bool drawConstraints;
		bool drawHashCells;
		bool drawId;
		DrawUserPhysicsPtr drawUser;
		//debug

		~PhysicsManager();
		static void init(PhysicsManagerConfiguration& configuration);

		static inline PhysicsManager* getSingleton()
		{
			return singleton;
		}

		inline CacheLineAllocator* getPhysicsAllocator()
		{
			return this->physicsBodyAllocator;
		}

		inline CacheLineAllocator* getTemporaryShapeAllocator()
		{
			return this->temporaryShapeAllocator;
		}

		inline gil::Scene2d* getScene();
		PhysicObject2d*      getLastAwakePhysicObject();
		PhysicObject2d*      getPhysicObject(uint16_t objectId);
		RigidBody2d*	     getBody(uint16_t bodyId);
		ParticleGenerator2d* getParticleGenerator(uint16_t bodyId);
		
		void step(jaFloat deltaTime);
		void debugStep(jaFloat deltaTime);

		RigidBody2d* createBody(Shape2d* shape, BodyType bodyType, const jaTransform2& transform, bool isFixedRotation, bool isSleeping, uint16_t bodyId = setup::physics::ID_UNASSIGNED);

		void deletePhysicObjectImmediate(PhysicObject2d* physicObject);

		Constraint2d* getConstraints(PhysicObject2d* physicObject);
	private:
		void deleteConstraint(Constraint2d* constraint);
		void removeConstraintLinks(Constraint2d* constraint);
		void deleteConstraints(PhysicObject2d* physicObject);
	public:
		void removeJoint(Constraint2d* joint);
		void changeBodyShape(RigidBody2d* body, Shape2d* shape);
		void changeBodyType(PhysicObject2d* physicObject, BodyType bodyType);

		void updatePhysicObjectImmediate(PhysicObject2d* physicObject)
		{
			if (physicObject->isSleeping()) {
				// physicObject is a dynamic object, as Kinematic and Static objects CANNOT be in a sleep state
				scene2d->updateStaticObject(physicObject->shapeHandle);
			}
			else {
				if (physicObject->bodyType == (uint8_t)BodyType::STATIC_BODY) {
					scene2d->updateStaticObject(physicObject->shapeHandle);
				}
				else {
					scene2d->updateDynamicObject(physicObject->shapeHandle, true);
				}
			}
		}

		void drawHashGrid(CameraInterface* camera);
		void drawDebug(CameraInterface* camera);
		void printIsland(ConstraintIsland2d* island);
		void printHashes();
		void printConstraintIslands();
		void printConstraints();
		static void cleanUp();

		void clearWorld();

		void getPhysicObjectsAtPoint(const jaVector2& point, StackAllocator<ObjectHandle2d*>* objectList);
		void getPhysicObjectsAtAABB(const AABB2d& aabb, StackAllocator<ObjectHandle2d*>* objectList);
		void getPhysicObjectsAtShape(const jaTransform2& transform, Shape2d* shape, StackAllocator<ObjectHandle2d*>* objectList);
		RigidBody2d* createBodyDef(uint16_t bodyId = setup::physics::ID_UNASSIGNED);
		Constraint2d* createConstraintDef();
		ParticleGenerator2d* createParticleDef(uint16_t particleId = setup::physics::ID_UNASSIGNED);
		void rayIntersection(jaVector2& ray0, jaVector2& ray1, StackAllocator<RayContact2d>* rayContacts);

		friend class LevelManager;
		friend class PhysicThreadWorkers;
		friend class ::PhysicsTests;
		friend class PhysicObjectDef2d;
	};

	Scene2d* PhysicsManager::getScene()
	{
		return scene2d;
	}

	ConstraintIsland2d* PhysicsManager::makeUnion(ConstraintIsland2d* islandA, ConstraintIsland2d* islandB)
	{
		if (islandA == islandB)
			return islandA;

		//islandA must be older (i.e. have a lower ID) than islandB
		if (islandA->id > islandB->id)
		{
			//NO, islandA is younger, so we need to swap them
			ConstraintIsland2d* temp = islandB;
			islandB = islandA;
			islandA = temp;
		}

#pragma region
		if (islandB->previous == nullptr) // possibility that island is linked as first island
		{
			if (islandB->next != nullptr) // island linked as first need to switch islands
			{
				constraintIsland = islandB->next;
				constraintIsland->previous = nullptr;
			}
			// if next==NULL and previous==NULL then island B is not linked because
			// island A has lower id
		}
		else // island is linked but not as first
		{
			islandB->previous->next = islandB->next;

			if (islandB->next != nullptr)
			{
				islandB->next->previous = islandB->previous;
			}
		}
#pragma endregion relinkIslands

#pragma region
		for (ConstraintList2d* itList = islandB->firstElement; itList != nullptr; itList = itList->next)
		{
			itList->constraint->objectA->island = islandA;
			itList->constraint->objectB->island = islandA;
		}
#pragma endregion assignBodiesToIsland

#pragma region
		islandA->lastElement->next = islandB->firstElement;
		islandB->firstElement->previous = islandA->lastElement;
		islandA->lastElement = islandB->lastElement;
#pragma endregion joinEndingPoints

		islandA->hash += islandB->hash;

		islandA->contactsNum += islandB->contactsNum;
		islandA->jointsNum   += islandB->jointsNum;

		return islandA;
	}

	ConstraintIsland2d* PhysicsManager::getUnitedIsland(Constraint2d* constraint, uint8_t constraintSynchronizer)
	{
		ConstraintIsland2d* island = nullptr;

		if (constraint->isEndpoint())
		{
#pragma region
			//Endpoint by assumption contains at least one DYNAMIC_BODY
			if (constraint->objectA->bodyType == (uint8_t)BodyType::DYNAMIC_BODY)
			{
				if (constraint->objectA->constraintSynchronizer == constraintSynchronizer)
				{
					return constraint->objectA->island;
				}
			}
			else
			{
				// ... so objectB must be a DYNAMIC_BODY and we don't need to check for it
				if (constraint->objectB->constraintSynchronizer == constraintSynchronizer)
				{
					return constraint->objectB->island;
				}
			}
#pragma endregion Island with exactly one DYNAMIC_BODY
		}
		else
		{
#pragma region
			if (constraint->objectA->constraintSynchronizer == constraintSynchronizer)
			{
				island = constraint->objectA->island;
			}

			if (constraint->objectB->constraintSynchronizer == constraintSynchronizer)
			{
				if (island == nullptr)
					return constraint->objectB->island;

				return makeUnion(constraint->objectA->island, constraint->objectB->island);
			}
#pragma endregion Island with two objects which are DYNAMIC_BODY
		}

		return island; // If it returns null it means objectA and objectB is not added to any island
	}

	ObjectHandle2d* PhysicsManager::createObjectOfType(const BodyType bodyType, const Shape2d* shape, const jaTransform2& tr, bool isSleeping)
	{
		if ((bodyType == BodyType::STATIC_BODY) || isSleeping)
		{
			return scene2d->createStaticObject(shape, tr);
		}

		return scene2d->createDynamicObject(shape, tr);
	}

}
