#include "ResourceManager.hpp"
#include "../Graphics/Texture2dLoader.hpp"
#include "../Graphics/ShaderLoader.hpp"
#include "../Sounds/SoundLoader.hpp"
#include "../Videos/VideoLoader.hpp"
#include "../Scripts/ScriptLoader.hpp"
#include "../Gui/GuiLoader.hpp"
#include "../Utility/Exception.hpp"

#include <thread>
#include <chrono>

namespace ja
{

ResourceManager* ResourceManager::singleton    = nullptr;
const char ResourceManager::scriptsPath[]      = "Data/Scripts/";
const char ResourceManager::fontsPath[]        = "Data/Fonts/";
const char ResourceManager::soundStreamsPath[] = "Data/Music/";
const char ResourceManager::shadersPath[]      = "Data/Shaders/";
const char ResourceManager::soundEffectsPath[] = "Data/Sounds/";
const char ResourceManager::texturesPath[]     = "Data/Textures/";

ResourceLoader::ResourceLoader()
{
	this->isRunning = false;
}

void ResourceLoader::run(ResourceManager* pResourceManager)
{
	this->isRunning = true;
	this->pResourceManager = pResourceManager;
	this->resourceLoaderThread = std::thread(resourceLoaderFunction, this);
}

void ResourceLoader::execute()
{
	//while (isRunning)
	{
		//std::cout << "resource loader is running" << std::endl;
		//std::this_thread::sleep_for(std::chrono_literals::2s);
		//std::this_thread::sleep_for(std::chrono::seconds(5));
	}

	//std::cout << "resource loader quit" << std::endl;
}

void ResourceLoader::stop()
{
	this->isRunning = false;
	this->resourceLoaderThread.join();
}

ResourceManager::ResourceManager()
{
	Texture2dLoader::init(); 
	ShaderLoader::init();
	SoundLoader::init();
	ScriptLoader::init();
	GuiLoader::init();

	this->resourceAllocator = new CacheLineAllocator(setup::resources::RESOURCE_ALLOCATOR_SIZE, 16);

	setResourcePath("Data\\");

	this->resourceLoader.run(this);
}

ResourceManager::~ResourceManager()
{
	this->resourceLoader.stop();

	Texture2dLoader::cleanUp();
	ShaderLoader::cleanUp();
	SoundLoader::cleanUp();
	ScriptLoader::cleanUp();
	GuiLoader::cleanUp();
	
	//level
	clearResources();
	//level

	clearResourcesMap(&eternalFonts);
	clearResourcesMap(&eternalFontPolygons);
	clearResourcesMap(&eternalTextures2d);

	clearResourcesMap(&eternalSoundEffects);
	clearResourcesMap(&eternalSoundStreams);

	delete this->resourceAllocator;
}

void ResourceManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void ResourceManager::init()
{
	if(singleton==nullptr)
	{ 
		singleton = new ResourceManager();
	}	
}

Resource<Font>* ResourceManager::loadEternalFont(const char* fileName, uint32_t size, FontEncode encode)
{
	ja::string fullPath(getFontsPath());
	fullPath.append(fileName);

	const uint32_t  hash     = HashMap<Resource<Font>>::getHash(fileName) * size;
	Resource<Font>* resource = eternalFonts.find(hash);

	Font* font = nullptr;

	if (resource == nullptr)
	{
		font = new Font(hash);
	}
	else
	{
		font = resource->resource;
		font->clear();
	}

	bool result = FontLoader::loadFont(fullPath.c_str(), size, encode, font);

	if (false == result)
	{
		if (nullptr != resource)
		{
			delete resource;
			eternalFonts.remove(hash);
			return nullptr;
		}

		delete font;
	
		return nullptr;
	}
	
	if (nullptr == resource)
	{
		resource = new Resource<Font>(font, fileName);
		eternalFonts.add(hash, resource);
	}

	return resource;
}

Resource<FontPolygon>* ResourceManager::loadEternalFontPolygon(const char* fileName, FontEncode encode)
{
	ja::string fullPath(getFontsPath());
	fullPath.append(fileName);

	const uint32_t  hash = HashMap<Resource<FontPolygon>>::getHash(fileName);
	Resource<FontPolygon>* resource = eternalFontPolygons.find(hash);

	FontPolygon* font = nullptr;

	if (resource == nullptr)
	{
		font = new FontPolygon(hash);
	}
	else
	{
		font = resource->resource;
		font->clear();
	}

	bool result = FontLoader::loadFontPolygon(fullPath.c_str(), encode, font);

	if (false == result)
	{
		if (nullptr != resource)
		{
			delete resource;
			eternalFontPolygons.remove(hash);
			return nullptr;
		}

		delete font;

		return nullptr;
	}

	if (nullptr == resource)
	{
		resource = new Resource<FontPolygon>(font, fileName);
		eternalFontPolygons.add(hash, resource);
	}

	return resource;
}

Resource<Texture2d>* ResourceManager::loadEternalTexture2d(const char* fileName)
{
	ja::string fullPath(getTexturesPath());
	fullPath.append(fileName);

	const uint32_t  hash = HashMap<Resource<Texture2d>>::getHash(fileName);
	Resource<Texture2d>* resource = eternalTextures2d.find(hash);

	Texture2d* texture = nullptr;

	if (resource == nullptr)
	{
		texture = new Texture2d(hash);
	}
	else
	{
		texture = resource->resource;
		texture->clear();
	}

	
	bool result = Texture2dLoader::loadTexture(fullPath.c_str(), texture);

	if (false == result)
	{
		if (nullptr != resource)
		{
			delete resource;
			eternalTextures2d.remove(hash);
			return nullptr;
		}

		delete texture;
		
		return nullptr;
	}

	if (nullptr == resource)
	{
		resource = new Resource<Texture2d>(texture, fileName);
		eternalTextures2d.add(hash, resource);
	}

	return resource;
}

Resource<SoundEffect>* ResourceManager::loadEternalSoundEffect(const char* fileName)
{
	/*ja::string fullPath(getSoundEffectsPath());
	fullPath.append(fileName);
	SoundEffect* sound = SoundLoader::loadSoundEffect(fullPath, resourceId);
	Resource<SoundEffect>* proxy = new Resource<SoundEffect>(sound, resourceName, activeResourceGroup);
	sounds[resourceId] = proxy;*/
	return nullptr;
}

Resource<SoundStream>* ResourceManager::loadEternalSoundStream(const char* fileName)
{
	/*ja::string fullPath(getSoundStreamPath());
	fullPath.append(fileName);

	SoundStream* music = SoundLoader::loadSoundStream(fullPath, resourceId);
	Resource<SoundStream>* proxy = new Resource<SoundStream>(music, resourceName, activeResourceGroup);
	soundStreams[resourceId] = proxy;*/
	return nullptr;
}

Resource<Font>* ResourceManager::getEternalFont(const char* resourceName, uint32_t size)
{
	const uint32_t hash = HashMap<Resource<Texture2d>>::getHash(resourceName) * size;
	return eternalFonts.find(hash);
}

Resource<FontPolygon>* ResourceManager::getEternalFontPolygon(const char* resourceName)
{
	return eternalFontPolygons.find(resourceName);
}

Resource<Texture2d>* ResourceManager::getEternalTexture2d(uint32_t resourceId)
{
	return eternalTextures2d.find(resourceId);
}

Resource<Texture2d>* ResourceManager::getEternalTexture2d(const char* fileName)
{
	return eternalTextures2d.find(fileName);
}

Resource<SoundEffect>* ResourceManager::getEternalSoundEffect(uint32_t resourceId)
{
	return eternalSoundEffects.find(resourceId);
}

Resource<SoundStream>* ResourceManager::getEternalSoundStream(uint32_t resourceId)
{
	return eternalSoundStreams.find(resourceId);
}

void ResourceManager::setResourcePath(const char* resourcePath )
{
	this->resourcePath = resourcePath;
}

//level

Resource<Texture2d>* ResourceManager::loadTexture2d(const char* fileName)
{
	/*ja::string fullPath(getTexturesPath());
	fullPath.append(fileName);

	Texture2d* texture = Texture2dLoader::loadTexture(fullPath, resourceId);
	if(texture == nullptr)
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_TEXTURE,fileName);

	Resource<Texture2d>* proxy = new Resource<Texture2d>(texture, fileName, activeLevelResourceGroup);
	levelTextures2d[resourceId] = proxy;*/
	return nullptr;
}


Resource<Shader>* ResourceManager::loadShader(const char* fileName, ShaderType shaderType)
{
	ja::string fullPath(getShadersPath());
	fullPath.append(fileName);

	const uint32_t    hash     = HashMap<Resource<Shader>>::getHash(fileName);
	Resource<Shader>* resource = shaders.find(hash);

	Shader* shader = nullptr;

	if (resource == nullptr)
	{
		shader = new Shader(hash);
	}
	else
	{
		shader = resource->resource;
		shader->clear();
	}

	bool result = ShaderLoader::loadShader(fullPath.c_str(), shaderType, shader);

	if (false == result)
	{
		if (nullptr != resource)
		{
			delete resource;
			shaders.remove(hash);
			return nullptr;
		}

		delete shader;

		return nullptr;
	}

	if (nullptr == resource)
	{
		resource = new Resource<Shader>(shader, fileName);
		shaders.add(hash, resource);
	}

	return resource;
}

Resource<ShaderBinder>* ResourceManager::createShaderBinder(const char* shaderBinderName, const char* vertexShaderName, const char* fragmentShaderName, const char* geometryShaderName)
{
	const uint32_t    vertexHash     = HashMap<Resource<Shader>>::getHash(vertexShaderName);
	Resource<Shader>* vertexResource = shaders.find(vertexHash);

	const uint32_t    fragmentHash     = HashMap<Resource<Shader>>::getHash(fragmentShaderName);
	Resource<Shader>* fragmentResource = shaders.find(fragmentHash);

	const uint32_t    geometryHash     = HashMap<Resource<Shader>>::getHash(geometryShaderName);
	Resource<Shader>* geometryResource = shaders.find(geometryHash);

	const uint32_t          binderHash = HashMap<Resource<ShaderBinder>>::getHash(shaderBinderName);
	Resource<ShaderBinder>* resource   = shaderBinders.find(binderHash);

	ShaderBinder* shaderBinder = nullptr;

	if (resource == nullptr)
	{
		shaderBinder = new ShaderBinder(binderHash);
	}
	else
	{
		shaderBinder = resource->resource;
		shaderBinder->clear();
	}

	Shader* vertexShader   = nullptr;
	Shader* fragmentShader = nullptr;
	Shader* geometryShader = nullptr;

	if (nullptr != vertexResource)
	{
		vertexShader = vertexResource->resource;
	}

	if (nullptr != fragmentResource)
	{
		fragmentShader = fragmentResource->resource;
	}

	if (nullptr != geometryResource)
	{
		geometryShader = geometryResource->resource;
	}

	bool result = ShaderLoader::createShaderBinder(shaderBinderName, vertexShader, fragmentShader, geometryShader, shaderBinder);

	if (false == result)
	{
		if (nullptr != resource)
		{
			delete resource;
			shaderBinders.remove(binderHash);
			return nullptr;
		}

		delete shaderBinder;

		return nullptr;
	}

	if (nullptr == resource)
	{
		resource = new Resource<ShaderBinder>(shaderBinder, shaderBinderName);
		shaderBinders.add(binderHash, resource);
	}

	return resource;
}

Resource<Script>* ResourceManager::addScript(const char* fileName) 
{ 
	const uint32_t hash = HashMap<Resource<Script>>::getHash(fileName);
	Resource<Script>* resource = scripts.find(hash);

	if (nullptr == resource)
	{
		Script* script = new Script(hash);
		resource = new Resource<Script>(script, fileName);

		scripts.add(hash, resource);
	}

	return resource;
}

Resource<DataModel>* ResourceManager::addDataModel(const char* dataModelName, DataModel* dataModel)
{
	const uint32_t       hash     = HashMap<Resource<Script>>::getHash(dataModelName);
	Resource<DataModel>* resource = dataModels.find(hash);

	if (nullptr == resource)
	{
		resource = new Resource<DataModel>(dataModel, dataModelName);

		dataModels.add(hash, resource);
	}

	return resource;
}

Resource<ShaderStructureDef>* ResourceManager::addMaterialShaderDef(const char* materialCoreName, ShaderStructureDef* materialStructure)
{
	const uint32_t                hash     = HashMap<Resource<ShaderStructureDef>>::getHash(materialCoreName);
	Resource<ShaderStructureDef>* resource = materialShaderDefs.find(hash);

	if (nullptr == resource)
	{
		resource = new Resource<ShaderStructureDef>(materialStructure, materialCoreName);

		materialShaderDefs.add(hash, resource);
	}

	return resource;
}

Resource<ShaderStructureDef>* ResourceManager::addShaderObjectDef(const char* shaderObjectDefName, ShaderStructureDef* shaderObjectDef)
{
	const uint32_t                    hash = HashMap<Resource<ShaderStructureDef>>::getHash(shaderObjectDefName);
	Resource<ShaderStructureDef>* resource = shaderObjectDefs.find(hash);

	if (nullptr == resource)
	{
		resource = new Resource<ShaderStructureDef>(shaderObjectDef, shaderObjectDefName);

		shaderObjectDefs.add(hash, resource);

		ShaderBinder* pShaderBinder = shaderObjectDef->getShaderBinder();
		pShaderBinder->shaderObjectDef = shaderObjectDef;
	}

	return resource;
}

Resource<Material>* ResourceManager::createMaterial(const char* materialName, ShaderStructureDef* materialShaderDef)
{
	const uint32_t      hash     = HashMap<Resource<Material>>::getHash(materialName);
	Resource<Material>* resource = materials.find(hash);

	if (nullptr == resource)
	{
		Material* material = new Material(materialName, materialShaderDef);

		resource = new Resource<Material>(material, materialName);

		materials.add(hash, resource);
	}

	return resource;
}

Resource<SoundEffect>* ResourceManager::loadSoundEffect(const char* fileName)
{
	/*ja::string fullPath(getSoundEffectsPath());
	fullPath.append(fileName);

	SoundEffect* soundEffect = SoundLoader::loadSoundEffect(fullPath, resourceId);
	if (soundEffect == nullptr)
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_SOUND_EFFECT, fileName);
	Resource<SoundEffect>* proxy = new Resource<SoundEffect>(soundEffect, fileName, activeLevelResourceGroup);
	levelSounds[resourceId] = proxy;*/

	return nullptr;
}

Resource<SoundStream>* ResourceManager::loadSoundStream(const char* fileName)
{
	/*ja::string fullPath(getMusicStreamPath());
	fullPath.append(fileName);

	SoundStream* musicStream = SoundLoader::loadSoundStream(fullPath, resourceId);
	Resource<SoundStream>* proxy = new Resource<SoundStream>(musicStream, fileName, activeLevelResourceGroup);
	levelSoundStreams[resourceId] = proxy;*/

	return nullptr;
}

Resource<Script>* ResourceManager::isLevelScriptLoaded(const char* fileName)
{
	return scripts.find(fileName);
}

void ResourceManager::getInScriptPath(ja::string& fileInScriptPath, bool& isInScriptPath)
{
	const char* filePath = fileInScriptPath.c_str();

	replaceChar((char*)filePath, '\\', '/');

	const char* fileDirPath = strstr(filePath, getScriptsPath());

	if (fileDirPath == nullptr)
	{
		isInScriptPath = false;
		return;
	}

	isInScriptPath = true;

	fileInScriptPath = (fileDirPath + sizeof(scriptsPath) - 1);
}

Resource<Texture2d>* ResourceManager::getTexture2d(uint32_t resourceId)
{
	return textures2d.find(resourceId);
}

Resource<Shader>* ResourceManager::getShader(const char* fileName)
{
	return shaders.find(fileName);
}

Resource<Shader>* ResourceManager::getShader(uint32_t resourceId)
{
	return shaders.find(resourceId);
}

Resource<ShaderBinder>* ResourceManager::getShaderBinder(const char* shaderBinderName)
{
	return shaderBinders.find(shaderBinderName);
}

Resource<ShaderBinder>* ResourceManager::getShaderBinder(uint32_t resourceId)
{
	return shaderBinders.find(resourceId);
}

Resource<SoundEffect>* ResourceManager::getSoundEffect(uint32_t resourceId)
{
	return soundEffects.find(resourceId);
}

Resource<Script>* ResourceManager::getScript(uint32_t resourceId)
{
	return scripts.find(resourceId);
}

Resource<SoundStream>* ResourceManager::getSoundStream(uint32_t resourceId)
{
	return soundStreams.find(resourceId);
}

Resource<DataModel>* ResourceManager::getDataModel(uint32_t resourceId)
{
	return dataModels.find(resourceId);
}

Resource<DataModel>* ResourceManager::getDataModel(const char* dataModelName)
{
	return dataModels.find(dataModelName);
}

Resource<Material>* ResourceManager::getMaterial(uint32_t resourceId)
{
	return materials.find(resourceId);
}

Resource<Material>* ResourceManager::getMaterial(const char* materialName)
{
	return materials.find(materialName);
}

Resource<ShaderStructureDef>* ResourceManager::getMaterialShaderDef(uint32_t resourceId)
{
	return materialShaderDefs.find(resourceId);
}

Resource<ShaderStructureDef>* ResourceManager::getMaterialShaderDef(const char* materialDefName)
{
	return materialShaderDefs.find(materialDefName);
}

Resource<ShaderStructureDef>* ResourceManager::getShaderObjectDef(uint32_t resourceId)
{
	return shaderObjectDefs.find(resourceId);
}

Resource<ShaderStructureDef>* ResourceManager::getShaderObjectDef(const char* shaderObjectDefName)
{
	return shaderObjectDefs.find(shaderObjectDefName);
}

HashMap<Resource<Texture2d>>* ResourceManager::getTextures2d()
{
	return &textures2d;
}

HashMap<Resource<SoundEffect>>* ResourceManager::getSoundEffects()
{
	return &soundEffects;
}

HashMap<Resource<SoundStream>>* ResourceManager::getSoundStreams()
{
	return &soundStreams;
}

HashMap<Resource<Shader>>* ResourceManager::getShaders()
{
	return &shaders;
}

HashMap<Resource<ShaderBinder>>* ResourceManager::getShaderBinders()
{
	return &shaderBinders;
}

HashMap<Resource<Script>>* ResourceManager::getScripts()
{
	return &scripts;
}

HashMap<Resource<DataModel>>* ResourceManager::getDataModels()
{
	return &dataModels;
}

ja::string ResourceManager::getResourcesPath()
{
	return resourcePath;
}

const char* ResourceManager::getTexturesPath()
{
	return texturesPath;
}

const char* ResourceManager::getSoundEffectsPath()
{
	return soundEffectsPath;
}

const char* ResourceManager::getFontsPath()
{
	return fontsPath;
}

const char* ResourceManager::getShadersPath()
{
	return shadersPath;
}

const char* ResourceManager::getSoundStreamPath()
{
	return soundStreamsPath;
}

const char* ResourceManager::getScriptsPath()
{
	return scriptsPath;
}

void ResourceManager::clearResources()
{
	clearResourcesMap(&textures2d);
	clearResourcesMap(&shaderBinders);
	clearResourcesMap(&shaders);
	clearResourcesMap(&soundEffects);
	clearResourcesMap(&soundStreams);
	clearResourcesMap(&dataModels);
	clearResourcesMap(&scripts);
	clearResourcesMap(&materialShaderDefs);
	clearResourcesMap(&materials);
	clearResourcesMap(&shaderObjectDefs);

	this->resourceAllocator->clear();
}

}
