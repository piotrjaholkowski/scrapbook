#pragma once

#include "../jaSetup.hpp"

#include <cstdarg>
#include "../Interface/CameraInterface.hpp"

#include "../Graphics/LayerRenderer.hpp"
#include "../Gilgamesh/Narrow/Narrow2d.hpp"

namespace ja
{
	class Camera;

	class GameManager
	{
	private:
		static bool         initializedGame;
		static GameManager* singleton;
		bool   isLoading;
		float  deltaTime;

		CameraInterface cameraInterface;

		StackAllocator<gil::ObjectHandle2d*>* tempObjectHandleList;
		LayerRenderer layerRenderer;

		GameManager();
	public:
		//debug
		bool    renderDebug;
		bool    renderBounds;
		bool    renderAABB;

		~GameManager();

		static inline GameManager* GameManager::getSingleton()
		{
			return singleton;
		}

		static void initGameManager(const char* title);
		static void cleanUpAll();
		void update(float deltaTime);
		void allowDropEvent(bool state);
		void showMouse(bool state);
		void grabInput(bool state);
		void blockMouseEvents(bool state);
		void getWindowPosition(float worldX, float worldY, int32_t& windowX, int32_t& windowY);

		inline float getDeltaTime()
		{
			return deltaTime;
		}

		inline StackAllocator<gil::ObjectHandle2d*>* getTempObjectList()
		{
			return tempObjectHandleList;
		}

		CameraInterface* getActiveCamera();

		void beginRender();
		void renderScene();

	};

}