#include "EntityManager.hpp"
#include "DisplayManager.hpp"
#include "../Gilgamesh/DebugDraw.hpp"
#include "../Tests/TimeStamp.hpp"

// Entity groups
#include "../Entity/SoundGroup.hpp"
#include "../Entity/ScriptGroup.hpp"
#include "../Entity/PhysicGroup.hpp"
#include "../Entity/LayerObjectGroup.hpp"
#include "../Entity/DataGroup.hpp"
// Entity groups

namespace ja
{ 

EntityManager* EntityManager::singleton = nullptr;

EntityManager::EntityManager() : nextId(0)
{
	entityAllocator               = new CacheLineAllocator(setup::entities::ENTITY_ALLOCATOR_SIZE, 16);
	layerObjectComponentAllocator = new CacheLineAllocator(setup::entities::ENTITY_ALLOCATOR_SIZE, 16);
	physicComponentAllocator      = new CacheLineAllocator(setup::entities::ENTITY_ALLOCATOR_SIZE, 16);
	scriptComponentAllocator      = new CacheLineAllocator(setup::entities::ENTITY_ALLOCATOR_SIZE, 16);
	dataComponentAllocator        = new CacheLineAllocator(setup::entities::ENTITY_ALLOCATOR_SIZE, 16);

	memset(entityGroups, 0, setup::entities::MAX_COMPONENT_GROUPS * sizeof(void*));

	entityGroups[(uint8_t)EntityGroupType::PHYSIC_OBJECT] = new PhysicGroup(entityAllocator);
	entityGroups[(uint8_t)EntityGroupType::SCRIPT]        = new ScriptGroup(entityAllocator);
	entityGroups[(uint8_t)EntityGroupType::LAYER_OBJECT]  = new LayerObjectGroup(entityAllocator);
	entityGroups[(uint8_t)EntityGroupType::DATA]          = new DataGroup(entityAllocator);

	componentGroupNum = 8;

	for (int32_t i = componentGroupNum; i<setup::entities::MAX_COMPONENT_GROUPS; ++i)
	{
		entityGroups[i] = nullptr;
	}

	memset(entities, 0, setup::entities::MAX_ENTITIES * sizeof(Entity*));
}

EntityManager::~EntityManager()
{
	deinitEntityGroups();
	delete entityAllocator;
	delete layerObjectComponentAllocator;
	delete physicComponentAllocator;
	delete scriptComponentAllocator;
	delete dataComponentAllocator;
}		   

void EntityManager::deinitEntityGroups()
{
	for(uint32_t i=0; i<componentGroupNum; ++i)
	{
		delete entityGroups[i];
		entityGroups[i] = nullptr;
	}
}

void EntityManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void EntityManager::init()
{
	if(singleton==nullptr)
	{ 
		singleton = new EntityManager();	
	}
}

Entity* EntityManager::createEntity(uint16_t entityId)
{
	entities[entityId] = new (entityAllocator->allocate(sizeof(Entity))) Entity();
	Entity* entity = entities[entityId];

	entityIds.remove(entityId);

	entity->id = entityId;
	return entity;
}

Entity* EntityManager::createEntity()
{
	return getNewEntity();
}

Entity* EntityManager::getEntity(uint16_t entityId)
{
	return entities[entityId];
}

EntityGroup* EntityManager::getComponentGroup(uint8_t groupEntity)
{
	return entityGroups[groupEntity];
}

void EntityManager::deleteEntityImmediate(Entity* entity)
{
	entity->deleteComponents();

	entityIds.push_back(entity->id);
	entities[entity->id] = nullptr;

	entityAllocator->free(entity,sizeof(Entity));
}

void EntityManager::update(float deltaTime)
{
	PROFILER_FUN;

	for(uint32_t i=0; i<componentGroupNum; i++)
	{
		if (nullptr != entityGroups[i])
			entityGroups[i]->update(deltaTime);
	}
}

void EntityManager::getEntities( std::list<Entity*>* entitiesList )
{
	for (uint32_t i = 0; i< setup::entities::MAX_ENTITIES; ++i)
	{
		if(entities[i] != nullptr)
		{
			entitiesList->push_back(entities[i]);
		}
	}
}

void EntityManager::clearEntities()
{
	for(uint32_t i=0; i< setup::entities::MAX_ENTITIES; ++i)
	{
		if(entities[i] != nullptr)
		{
			deleteEntityImmediate(entities[i]);
		}
	}

	this->entityIds.clear();
	nextId = 0;

	entityAllocator->clear();
	layerObjectComponentAllocator->clear();
	physicComponentAllocator->clear();
	scriptComponentAllocator->clear();
	dataComponentAllocator->clear();

	Entity::clearDictionary();
	EntityComponent::clearDictionary();
}

uint16_t EntityManager::getNextId()
{
	return nextId;
}

}