#include "SoundManager.hpp"
#include "ResourceManager.hpp"
#include "GameManager.hpp"
#include "LogManager.hpp"
#include "../Interface/CameraInterface.hpp"
#include "../Math/Math.hpp"
#include "../Utility/StringStream.hpp"
#include "../Tests/TimeStamp.hpp"

#include <iostream>
#include <sstream>

#ifndef JA_SWAP
	#define JA_SWAP(a,b)tempr=(a);(a)=(b);(b)=static_cast<float>(tempr)
	//tempr is a variable from our FFT function
#endif JA_SWAP

namespace ja
{ 

SoundManager* SoundManager::singleton = nullptr;

int32_t readSpectrumThreadFunction(void* data)
{
	return 0;
}

SoundManager::SoundManager()
{
	/* Open and initialize a device with default settings */
	device = alcOpenDevice(nullptr);

	LogManager* logManager = LogManager::getSingleton();

	if(!device)
	{
		logManager->addLogMessage(LogType::ERROR_LOG, "Could not open sound device!");
	}

	ctx = alcCreateContext(device, nullptr);
	if(ctx == nullptr || alcMakeContextCurrent(ctx) == ALC_FALSE)
	{
		if(ctx != nullptr)
			alcDestroyContext(ctx);
		alcCloseDevice(device);

		logManager->addLogMessage(LogType::ERROR_LOG, "Could not make sound device context!");
	}

	logDevicesInfo();
	logExtensionsInfo();
	
	captureDevice         = nullptr;
	activeRecordingDevice = 0;

	//openCaptureDevice(0);
	//startCapturing();

	//AL_INVERSE_DISTANCE
	//AL_INVERSE_DISTANCE_CLAMPED
	//AL_LINEAR_DISTANCE 
	//AL_LINEAR_DISTANCE_CLAMPED
	//AL_EXPONENT_DISTANCE
	//AL_EXPONENT_DISTANCE_CLAMPED
	//AL_NONE

	//alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED); //AL_INVERSE_DISTANCE_CLAMPED is default
	alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED);

	setListenerPosition(0,0,0);
	setListenerVelocity(0,0,0);
	setListenerOrientation(0,0,1,0,1,0);

	soundGroups = nullptr;
	soundGroupsNum = 1;

	soundAllocator = new CacheLineAllocator(setup::entities::ENTITY_ALLOCATOR_SIZE,32);
}

SoundManager::~SoundManager()
{
	SoundEmitter* soundEmitter;
	for(uint32_t i=0; i<soundPlayers.size(); ++i)
	{
		soundEmitter = &soundPlayers[i];
		soundEmitter->stop();

		alDeleteSources(1, &soundEmitter->soundEmitterId);		
	}
	soundPlayers.clear();


	alcMakeContextCurrent(nullptr);
	alcDestroyContext(ctx);

	if(device)
		alcCloseDevice(device);
	
	closeCaptureDevice();
	delete soundAllocator;
}



void SoundManager::openCaptureDevice(uint32_t deviceId)
{
	//const char* playbackDevices = alcGetString(NULL, ALC_DEVICE_SPECIFIER);
	
	//AL_FORMAT_MONO8 //represented as unsigned char (0,255) where 128 is audio output level of 0
	//AL_FORMAT_MONO16 // represented as signed short (-32768 to 32767) where 0 is audio output level of 0
	//AL_FORMAT_STEREO8 // same as above but left channel firt right second
	//AL_FORMAT_STEREO16 // same as above but left channel firt right second

	//captureDevice = alcCaptureOpenDevice(playbackDevices, 128, AL_FORMAT_MONO8, 128);
	//8000 is frequency of telephone capturing sound
	//256 is minimal sample get something from it

	closeCaptureDevice();
	//invDiv = 1.0f / 128.0f;

	activeRecordingDevice = deviceId;

	if(deviceId == 0)
	{
		//captureDevice = alcLoopbackOpenDeviceSOFT(device);
	}
	else
	{
		sampleFormat = AL_FORMAT_MONO8;
		captureDevice = alcCaptureOpenDevice(deviceNames[deviceId].c_str(),  setup::sound::CAPTURE_SAMPLE_RATE, sampleFormat, setup::sound::CAPTURE_BUFFER_SIZE);
	}	

	/*
	8-bit PCM data is expressed as an unsigned value over the range 0 to 255, 128 being an audio output level of zero.
	16-bit PCM data is expressed as a signed value over the range -32768 to 32767, 0 being an audio output level of zero.
	Stereo data is expressed in interleaved format, left channel first.
	Buffers containing more than one channel of data will be played without 3D spatialization.*/
	if(!captureDevice)
	{
		std::cout << "Could not open capture device!" << std::endl;
	}
}

void SoundManager::closeCaptureDevice()
{
	if(captureDevice)
	{
		stopCapturing();
		alcCaptureCloseDevice(captureDevice);
		std::cout << "Capture device closed" << std::endl;
	}
}

void SoundManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void SoundManager::init()
{
	if(singleton==nullptr)
	{ 
		singleton = new SoundManager();
	}
}

void SoundManager::setListenerPosition(jaFloat x, jaFloat y, jaFloat z)
{
	alListener3f(AL_POSITION, x, y, z);
}

// at, orientation
void SoundManager::setListenerOrientation(jaFloat facingX, jaFloat facingY, jaFloat facingZ, jaFloat upX, jaFloat upY, jaFloat upZ)
{
	float directionvect[6];
	directionvect[0] = facingX;
	directionvect[1] = facingY;
	directionvect[2] = facingZ;
	directionvect[3] = upX;
	directionvect[4] = upY;
	directionvect[5] = upZ;
	alListenerfv(AL_ORIENTATION, directionvect);
	//default {(0.0f, 0.0f, -1.0f), (0.0f, 1.0f, 0.0f)}
}

void SoundManager::setListenerVelocity(jaFloat x, jaFloat y, jaFloat z)
{
	alListener3f(AL_VELOCITY, x, y, z);
}

void SoundManager::update(jaFloat deltaTime)
{
	PROFILER_FUN;

	CameraInterface* activeCamera = GameManager::getSingleton()->getActiveCamera();

	jaVector2 position = activeCamera->position;
	jaFloat   angle    = activeCamera->angleRad;
	jaVector2 velocity = activeCamera->velocity;

	setListenerPosition(position.x,position.y,0);
	setListenerOrientation(0,0,-1,0,1,0);
	setListenerVelocity(velocity.x,velocity.y,0);

	updateMusicStreams();
	updateCaptureBuffer();
}

void SoundManager::updateMusicStreams()
{
	/*
	ALint state;
	ALint processed;
	ALuint bufs[JA_STREAM_BUFFS_NUM];

	for(int i=0; i<JA_MAX_MUSIC_EMITTERS; i++)
	{
		if(musicStreams[i] == NULL)
			continue;
		processed = 0;

		alGetSourcei(musicEmitters[i], AL_SOURCE_STATE, &state);
		alGetSourcei(musicEmitters[i], AL_BUFFERS_PROCESSED, &processed);
		if(processed > 0)
		{
			alSourceUnqueueBuffers(musicEmitters[i], processed, bufs); 
			//processed = alureBufferDataFromStream(musicStreams[i]->stream, processed, bufs);
			processed = musicStreams[i]->getBufferDataFromStream(processed, bufs);
			if(processed <= 0)
			{
				//alureRewindStream(musicStreams[i]->stream);
				musicStreams[i] = NULL;
				continue;
			}
			alSourceQueueBuffers(musicEmitters[i], processed, bufs);
		}
	
		if(state != AL_PLAYING)
			alSourcePlay(musicEmitters[i]);
	}*/
}

void SoundManager::setSoundGroup(SoundEmitter* soundEmitter, uint8_t soundGroup)
{
	soundGroup = soundGroup % soundGroupsNum;
	SoundEmitterDef* emitterDef = &soundGroups[soundGroup];

	soundEmitter->setRelativeToListener(emitterDef->relativeToListener);
	soundEmitter->setPosition(emitterDef->position[0],emitterDef->position[1],emitterDef->position[2]);
	soundEmitter->setRolloffFactor(emitterDef->rollofFactor);
	soundEmitter->setDirection(emitterDef->direction[0],emitterDef->direction[1],emitterDef->direction[2]);
	soundEmitter->setGain(emitterDef->gain);
	soundEmitter->setPitch(emitterDef->pitch);
	soundEmitter->setMaxDistance(emitterDef->maxDistance);
	soundEmitter->setReferenceDistance(emitterDef->referenceDistance);
	soundEmitter->setVelocity(emitterDef->velocity[0],emitterDef->velocity[1],emitterDef->velocity[2]);
}

ja::string& SoundManager::getSoundGroupInfo()
{
	return soundGroupInfo;
}

SoundEmitter* SoundManager::createSoundPlayer()
{
	SoundEmitter* soundEmitter;
	soundPlayers.push_back(SoundEmitter());
	soundEmitter = &(soundPlayers[soundPlayers.size() - 1]);

	alGenSources(1, &soundEmitter->soundEmitterId);

	soundEmitter->setRelativeToListener(true);
	soundEmitter->setPosition(0,0,0);
	soundEmitter->setRolloffFactor(1);

	soundEmitter->setGain(1);
	soundEmitter->setPitch(1);
	soundEmitter->setMaxDistance(25.0);
	soundEmitter->setReferenceDistance(15.0);
	soundEmitter->setVelocity(0,0,0);

	return soundEmitter;
}

void SoundManager::logDevicesInfo()
{
	LogManager* logManager = LogManager::getSingleton();

	logManager->addLogMessage(LogType::INFO_LOG, "Available sound playback devices:");
	const char* playbackDevices = alcGetString(nullptr, ALC_DEVICE_SPECIFIER); 

	deviceNames.push_back("ALC_SOFT_loopback");
	
	const char* playbackDevicesIt = playbackDevices;
	while (*playbackDevicesIt)
	{ 
		deviceNames.push_back(playbackDevicesIt);
		logManager->addLogMessage(LogType::INFO_LOG, playbackDevicesIt);
		playbackDevicesIt += strlen(playbackDevicesIt) + 1;		
	} 

	logManager->addLogMessage(LogType::INFO_LOG, "Available sound capture devices:");
	const char* recordDevices = alcGetString(nullptr, ALC_CAPTURE_DEVICE_SPECIFIER); 

	const char* recordDevicesIt = recordDevices;
	while (*recordDevicesIt)
	{ 
		deviceNames.push_back(recordDevicesIt);
		logManager->addLogMessage(LogType::INFO_LOG, recordDevicesIt);
		recordDevicesIt += strlen(recordDevicesIt) + 1; 
	} 
}

void SoundManager::logExtensionsInfo()
{
	LogManager* logManager = LogManager::getSingleton();

	logManager->addLogMessage(LogType::INFO_LOG, "Available sound extensions:");
	const char* soundExtensions   = alGetString(AL_EXTENSIONS);
	const char* soundExtensionsIt = soundExtensions;

	char        soundExtensionBuffer[1024];
	int32_t     nextStringPos     = 0;

	while (getDelimSeparatedString(soundExtensionsIt, ' ', soundExtensionBuffer, nextStringPos))
	{
		logManager->addLogMessage(LogType::INFO_LOG, soundExtensionBuffer);
		soundExtensionsIt += nextStringPos;
	}

	logManager->addLogMessage(LogType::INFO_LOG, soundExtensionBuffer);
}

void SoundManager::startCapturing()
{
	if(captureDevice)
		alcCaptureStart(captureDevice);
}

void SoundManager::stopCapturing()
{
	if(captureDevice)
		alcCaptureStop(captureDevice);
}

void SoundManager::updateCaptureBuffer()
{
	if((activeRecordingDevice != 0) && (captureDevice != nullptr))
	{
		// Get the number of samples available 
		alcGetIntegerv(captureDevice, ALC_CAPTURE_SAMPLES, 1, &samplesAvailable); 

		if (samplesAvailable == setup::sound::CAPTURE_BUFFER_SIZE)  // when overrides buffer it will simply return size of the buffer
		{ 
			alcCaptureSamples(captureDevice, capturedSamples, samplesAvailable);
			uint32_t tempI = 0;
			float normalized = 0;
			float normDiff = 0.0f;

			switch(sampleFormat)
			{
			case AL_FORMAT_MONO8:
					normDiff = 128.0f;
				break;
			case AL_FORMAT_MONO16:
					normDiff = 0.0f;
				break;
			}

			for(int32_t i=0; i<samplesAvailable; ++i)
			{
				normalized = (float)(capturedSamples[i]);
				fftData.data[tempI++] = normalized * invDiv;
				fftData.data[tempI++] = 0.0f;
			}

			calculateFft();
		} 
	}
}

void SoundManager::calculateFft()
{
	double  pi   = 3.1415926535;
	int32_t sign = 1; // //sign -> 1 to calculate FFT and -1 to calculate Reverse FFT
	//variables for trigonometric recurrences
	uint32_t n,mmax,m,j,istep,i;
	double wtemp,wr,wpr,wpi,wi,theta,tempr,tempi;

	//the complex array is real+complex so the array 
	//as a size n = 2* number of complex samples
	// real part is the data[index] and the complex part is the data[index+1]
	n = setup::sound::CAPTURE_BUFFER_SIZE * 2; 

	//binary inversion (note that the indexes 
	//start from 0 witch means that the
	//real part of the complex is on the even-indexes 
	//and the complex part is on the odd-indexes
	j=0;
	for (i=0;i<n/2;i+=2) {
		if (j > i) {
			//swap the real part
			JA_SWAP(fftData.data[j],fftData.data[i]);
			//swap the complex part
			JA_SWAP(fftData.data[j+1],fftData.data[i+1]);
			// checks if the changes occurs in the first half
			// and use the mirrored effect on the second half
			if((j/2)<(n/4)){
				//swap the real part
				JA_SWAP(fftData.data[(n-(i+2))],fftData.data[(n-(j+2))]);
				//swap the complex part
				JA_SWAP(fftData.data[(n-(i+2))+1],fftData.data[(n-(j+2))+1]);
			}
		}
		m=n/2;
		while (m >= 2 && j >= m) {
			j -= m;
			m = m/2;
		}
		j += m;
	}

	//Danielson-Lanzcos routine 
	mmax=2;
	//external loop
	while (n > mmax)
	{
		istep = mmax<<  1;
		theta=sign*(2*pi/mmax);
		wtemp=sin(0.5*theta);
		wpr = -2.0*wtemp*wtemp;
		wpi=sin(theta);
		wr=1.0;
		wi=0.0;
		//internal loops
		for (m=1;m<mmax;m+=2) {
			for (i= m;i<=n;i+=istep) {
				j=i+mmax;
				tempr=wr*fftData.data[j-1]-wi*fftData.data[j];
				tempi=wr*fftData.data[j]+wi*fftData.data[j-1];
				fftData.data[j-1]= fftData.data[i-1] - (float)(tempr);
				fftData.data[j]= fftData.data[i] - (float)(tempi);
				fftData.data[i-1] += (float)(tempr);
				fftData.data[i] += (float)(tempi);
			}
			wr=(wtemp=wr)*wpr-wi*wpi+wr;
			wi=wi*wpr+wtemp*wpi+wi;
		}
		mmax=istep;
	}
	//end of the algorithm

	//determine the fundamental frequency
	//look for the maximum absolute value in the complex array
	fftData.frequency = 0;
	uint32_t fundamental_frequency = 0;
	uint32_t spectrumIndex = 0;
	
	switch(sampleFormat)
	{
	case AL_FORMAT_MONO8:
			invDiv = 1.0f / 256.0f;	
		break;
	case AL_FORMAT_MONO16:
			invDiv = 1.0f / 32767.0f;
		break;
	}

	for(i=2; i<setup::sound::CAPTURE_BUFFER_SIZE; i+=2)
	{
		fftData.spectrum[spectrumIndex] = (fftData.data[i] * fftData.data[i]) + (fftData.data[i + 1] * fftData.data[i + 1]); // real^2 + imaginay ^ 2
		if(fftData.spectrum[spectrumIndex] > fftData.spectrum[fundamental_frequency])
			fundamental_frequency = spectrumIndex;

		++spectrumIndex;
	}

	fftData.spectrum[spectrumIndex] = 0.0f;

	float biggestValue = fftData.spectrum[fundamental_frequency];
	for(int32_t x = 0; x <setup::sound::CAPTURE_BUFFER_SIZE / 2; ++x)
	{
		fftData.normalized_spectrum[x] = fftData.spectrum[x] / biggestValue;
	}

	//because the array of complex has the format [real][complex]=>[absolute value]
	//the maximum absolute value must be ajusted to half
	fftData.biggestIndex = fundamental_frequency;
	fundamental_frequency=(long)floor((float)fundamental_frequency/2);

	float deltaOffset = (float)setup::sound::CAPTURE_SAMPLE_RATE / (float)setup::sound::CAPTURE_BUFFER_SIZE;
	fftData.frequency = deltaOffset * fftData.biggestIndex;
}

}