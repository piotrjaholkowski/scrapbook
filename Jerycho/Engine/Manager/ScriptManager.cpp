#include "ScriptManager.hpp"
#include "LogManager.hpp"
#include "../Scripts/ScriptFunctions.hpp"

#include "../Scripts/Entity.hpp"
#include "../Scripts/Component.hpp"
#include "../Scripts/PhysicComponent.hpp"
#include "../Scripts/LayerObjectComponent.hpp"
#include "../Scripts/ScriptComponent.hpp"
#include "../Scripts/DataComponent.hpp"
#include "../Scripts/Sprite.hpp"
#include "../Scripts/Vector2.hpp"
#include "../Scripts/Transform2.hpp"

#include "../Scripts/LayerObjectDef.hpp"
#include "../Scripts/LayerObject.hpp"
#include "../Scripts/SpriteDef.hpp"
#include "../Scripts/WidgetDef.hpp"
#include "../Scripts/PhysicObjectDef.hpp"
#include "../Scripts/DrawElements.hpp"
#include "../Scripts/MorphElements.hpp"
#include "../Scripts/PolygonMorphElements.hpp"
#include "../Scripts/CameraMorphElements.hpp"
#include "../Scripts/PolygonMorph.hpp"
#include "../Scripts/CameraMorph.hpp"
#include "../Scripts/Material.hpp"

#include "../Scripts/ShaderBinder.hpp"
#include "../Scripts/Fbo.hpp"

#include "../Scripts/Menu.hpp"
#include "../Scripts/Widget.hpp"
#include "../Scripts/Scrollbar.hpp"
#include "../Scripts/Button.hpp"
#include "../Scripts/Editbox.hpp"
#include "../Scripts/GridLayout.hpp"

#include "../Scripts/InputManager.hpp"
#include "../Scripts/DisplayManager.hpp"
#include "../Scripts/LayerObjectManager.hpp"
#include "../Scripts/EntityManager.hpp"
#include "../Scripts/PhysicsManager.hpp"
#include "../Scripts/SpriteManager.hpp"
#include "../Scripts/EffectManager.hpp"
#include "../Scripts/ThreadManager.hpp"
#include "../Scripts/UIManager.hpp"
#include "../Scripts/LevelManager.hpp"
#include "../Scripts/ResourceManager.hpp"
#include "../Scripts/SoundManager.hpp"
#include "../Scripts/GameManager.hpp"
#include "../Scripts/ScriptManager.hpp"
#include "../Scripts/PhysicObject.hpp"
#include "../Scripts/RigidBody.hpp"
#include "../Scripts/ParticleGenerator.hpp"
#include "../Scripts/Matrix2.hpp"
#include "../Scripts/Constraint.hpp"
#include "../Scripts/LayerRenderer.hpp"

#include "../Scripts/DataFieldInfo.hpp"

#include "../Scripts/Shape.hpp"
#include "../Scripts/Box.hpp"
#include "../Scripts/Circle.hpp"
#include "../Scripts/TriangleMesh.hpp"
#include "../Scripts/ConvexPolygon.hpp"
#include "../Scripts/MultiShape.hpp"

#include "../Scripts/EnumTypes.hpp"

#include "../Scripts/CameraInterface.hpp"

#include "../Scripts/Toolset.hpp"

namespace ja
{ 

ScriptManager* ScriptManager::singleton = nullptr;


int32_t ScriptManager::panicHandler(lua_State* L)
{
	ja::string errorInfo = lua_tostring(L, 1);
	LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, errorInfo.c_str());
	LogManager::getSingleton()->saveCrashReport();
	return 0;
}

ScriptManager::ScriptManager()
{
	L = luaL_newstate();

	/* load Lua base libraries */
	luaL_openlibs(L);
	registerEngineFunctions();
	lua_atpanic(L, ScriptManager::panicHandler);
}

ScriptManager::~ScriptManager()
{
	lua_close(L);
}

void ScriptManager::registerEngineFunctions()
{
	lua_register(L, "runScript",runScriptFileI);

	setScriptPath("Data/Scripts/");

	jas::Entity::registerClass(L); 
	jas::Component::registerClass(L);
	jas::PhysicComponent::registerClass(L);
	jas::LayerObjectComponent::registerClass(L);
	jas::ScriptComponent::registerClass(L);
	jas::DataComponent::registerClass(L);

	jas::LayerRenderer::registerClass(L);

	jas::PhysicObjectDef::registerClass(L);
	jas::PhysicObject::registerClass(L);
	jas::RigidBody::registerClass(L);
	jas::ParticleGenerator::registerClass(L);
	jas::Constraint::registerClass(L);

	jas::LayerObjectDef::registerClass(L);
	jas::LayerObject::registerClass(L);
	jas::DrawElements::registerClass(L);
	jas::MorphElements::registerClass(L);
	jas::PolygonMorphElements::registerClass(L);
	jas::CameraMorphElements::registerClass(L);
	jas::PolygonMorph::registerClass(L);
	jas::CameraMorph::registerClass(L);

	jas::LayerRenderer::registerClass(L);
	jas::ShaderBinder::registerClass(L);
	jas::Material::registerClass(L);
	jas::Fbo::registerClass(L);
	jas::CameraInterface::registerClass(L);

	jas::WidgetDef::registerClass(L); 

	jas::Widget::registerClass(L);
	jas::Menu::registerClass(L); 
	jas::Button::registerClass(L); 
	jas::Editbox::registerClass(L);
	jas::Scrollbar::registerClass(L);
	jas::GridLayout::registerClass(L); 

	jas::Vector2::registerClass(L);
	jas::Matrix2::registerClass(L);
	jas::Transform2::registerClass(L);

	jas::InputManager::registerClass(L);
	jas::DisplayManager::registerClass(L); 
	jas::LayerObjectManager::registerClass(L);
	jas::PhysicsManager::registerClass(L);
	jas::EntityManager::registerClass(L);
	jas::UIManager::registerClass(L);
	jas::LevelManager::registerClass(L); 
	jas::ResourceManager::registerClass(L); 
	jas::EffectManager::registerClass(L); 
	jas::SoundManager::registerClass(L);
	jas::GameManager::registerClass(L);
	jas::ThreadManager::registerClass(L);
	jas::ScriptManager::registerClass(L);

	jas::Shape::registerClass(L);
	jas::Box::registerClass(L);
	jas::Circle::registerClass(L);
	jas::ConvexPolygon::registerClass(L);
	jas::TriangleMesh::registerClass(L);
	jas::MultiShape::registerClass(L);

	jas::DataFieldInfo::registerClass(L);

	jas::Toolset::registerClass(L);
	jas::EnumTypes::registerClass(L);
}

void ScriptManager::setScriptPath(const ja::string& scriptPath)
{
	this->scriptPath = scriptPath;
}

bool ScriptManager::runScriptFile(const ja::string& scriptFile, bool useAbsolutePath)
{
	int32_t status = 0;

	if (useAbsolutePath)
	{	
		status = luaL_dofile(L, scriptFile.c_str()); 
	}
	else
	{
		fullPath = scriptPath;
		fullPath.append(scriptFile);
		status = luaL_dofile(L, fullPath.c_str()); 
	}
	
	if (status)
	{
		/* If something went wrong, error message is at the top of */
		/* the stack */
		lastError = lua_tostring(L, -1);
		//std::cout << "Couldn't compile file " << scriptFile << ": " << lua_tostring(L, -1) << std::endl;
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Couldn't compile file %s: %s\n", scriptFile.c_str(), lastError.c_str());

		lua_pop(L, 1);

		return false;
	}

	return true;
}

void ScriptManager::runScript(const ja::string& script)
{
	int32_t status = luaL_dostring(L, script.c_str()); // Wykonaj skrypt w stringu

	if (status)
	{
		/* If something went wrong, error message is at the top of */
		/* the stack */
		lastError = lua_tostring(L, -1);
		//std::cout << "Couldn't run script: " << lua_tostring(L, -1) << std::endl;
		//LogManager::getSingleton()->addLog("Couldn't run script: %s\n", lastError.c_str());
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Couldn't run script: %s\n", lastError.c_str());
		lua_pop(L, 1);
	}
}

void ScriptManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}

}

void ScriptManager::init()
{
	if(singleton==nullptr)
	{ 
		singleton = new ScriptManager();
	}

}

void ScriptManager::cleanEnviroment()
{
	lua_close(L);
	L = luaL_newstate();

	initEntityFunctions.clear();
	updateFunctions.clear();
	constraintFunctions.clear();
	initDataFunctions.clear();
	accessDataFunctions.clear();
	//hashTagDictionary.clear();

	/* load Lua base libraries */
	luaL_openlibs(L);
	registerEngineFunctions();
	lua_atpanic(L, ScriptManager::panicHandler);
}

void ScriptManager::dumpStack()
{
	int32_t i;
	int32_t top = lua_gettop(L);
	for (i = 1; i <= top; i++) {  //repeat for each level 
		int32_t t = lua_type(L, i);
		switch (t) {

		  case LUA_TSTRING:  // strings 
			  std::cout << lua_tostring(L, i);
			  break;

		  case LUA_TBOOLEAN:  // booleans 
			  std::cout << (lua_toboolean(L, i) ? "true" : "false");
			  break;

		  case LUA_TNUMBER:  // numbers
			  std::cout << lua_tonumber(L, i);
			  break;

		  default:  // other values 
			  std::cout << lua_typename(L, t);
			  break;

		}
		std::cout << "  ";  // put a separator 
	}
	std::cout << std::endl;
}

lua_State* ScriptManager::getLuaState()
{
	return L;
}

void ScriptManager::printGlobals()
{
	printTable("_G");
}

void ScriptManager::printTable(const char* tableName)
{
	lua_getglobal(L, tableName);

	if (LUA_TTABLE == lua_type(L, -1))
	{
		lua_pushnil(L);                // put a nil key on stack
		while (lua_next(L, -2) != 0) { // key(-1) is replaced by the next key(-1) in table(-2)
			const char* name = lua_tostring(L, -2);  // Get key(-2) name
			std::cout << name << " " << luaL_typename(L, -1) << std::endl;

			lua_pop(L, 1);               // remove value(-1), now key on top at(-1)
		}
	}

}

const StackAllocator<ja::ScriptFunction>* ScriptManager::getScriptFunctions(ScriptFunctionType functionType)
{
	StackAllocator<ScriptFunction>* scriptFunctions = nullptr;

	switch (functionType)
	{
	case ScriptFunctionType::INIT_ENTITY:
		scriptFunctions = &initEntityFunctions;
		break;
	case ScriptFunctionType::UPDATE_ENTITY:
		scriptFunctions = &updateFunctions;
		break;
	case ScriptFunctionType::CONSTRAINT_NOTIFIER:
		scriptFunctions = &constraintFunctions;
		break;
	case ScriptFunctionType::INIT_DATA:
		scriptFunctions = &initDataFunctions;
		break;
	case ScriptFunctionType::ACCESS_DATA:
		scriptFunctions = &accessDataFunctions;
		break;
	}

	return scriptFunctions;
}

const ScriptFunction* ScriptManager::getScriptFunction(ja::ScriptFunctionType functionType, uint32_t fileHash, uint32_t functionHash)
{
	const StackAllocator<ScriptFunction>* functions = getScriptFunctions(functionType);

	ScriptFunction* scriptFunction = functions->getFirst();
	uint32_t        functionsNum   = functions->getSize();

	for (uint32_t i = 0; i < functionsNum; ++i)
	{
		if ((scriptFunction[i].functionHash == functionHash) && (scriptFunction[i].fileHash == fileHash))
		{
			return &scriptFunction[i];
		}
	}

	return nullptr;
}

void ScriptManager::registerFunction( const char* functionName, lua_CFunction func )
{
	lua_register(L,functionName,func);
}

void ScriptManager::registerFunction(const ScriptFunction& scriptFunction)
{
	StackAllocator<ScriptFunction>* scriptFunctions = nullptr;
	EntityGroup*                    entityGroup     = nullptr;

	ResourceManager::getSingleton()->addScript(scriptFunction.fileName);

	switch (scriptFunction.functionType)
	{
	case (uint8_t)ScriptFunctionType::INIT_ENTITY:
		scriptFunctions = &initEntityFunctions;
		entityGroup     = nullptr;
		break;
	case (uint8_t)ScriptFunctionType::UPDATE_ENTITY:
		scriptFunctions = &updateFunctions;
		entityGroup     = EntityManager::getSingleton()->getComponentGroup((uint8_t)EntityGroupType::SCRIPT);
		break;
	case (uint8_t)ScriptFunctionType::CONSTRAINT_NOTIFIER:
		scriptFunctions = &constraintFunctions;
		entityGroup     = EntityManager::getSingleton()->getComponentGroup((uint8_t)EntityGroupType::PHYSIC_OBJECT);
		break;
	case (uint8_t)ScriptFunctionType::INIT_DATA:
		scriptFunctions = &initDataFunctions;
		entityGroup     = EntityManager::getSingleton()->getComponentGroup((uint8_t)EntityGroupType::DATA);
		break;
	case (uint8_t)ScriptFunctionType::ACCESS_DATA:
		scriptFunctions = &accessDataFunctions;
		entityGroup     = EntityManager::getSingleton()->getComponentGroup((uint8_t)EntityGroupType::DATA);
	}

	{
		//Check if already added
		const uint32_t  functionsNum     = scriptFunctions->getSize();
		ScriptFunction* scriptFunctionIt = scriptFunctions->getFirst();

		for (uint32_t i = 0; i < functionsNum; ++i)
		{
			if ((scriptFunctionIt[i].fileHash == scriptFunction.fileHash) && (scriptFunctionIt[i].functionHash == scriptFunction.functionHash))
			{
				if (nullptr != entityGroup)
				{
					entityGroup->updateScriptFunctionReference(scriptFunctionIt->functionRef, scriptFunction.functionRef);
				}

				luaL_unref(L, LUA_REGISTRYINDEX, scriptFunctionIt->functionRef);
				scriptFunctionIt->functionRef = scriptFunction.functionRef;

				return;
			}
		}
	}

	scriptFunctions->push(scriptFunction);
}

void ScriptManager::setGlobalVariable(const char* variableName, uint16_t variableValue)
{
	lua_pushinteger(L, variableValue);
	lua_setglobal(L, variableName);
}

void ScriptManager::setTable(const char* tableName)
{
	lua_newtable(L);
	lua_setglobal(L, tableName);
}

}