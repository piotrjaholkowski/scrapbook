#pragma once

#include "../Allocators/StackAllocator.hpp"
#include "../Utility/NameTag.hpp"
#include "../Entity/Entity.hpp"
#include "../Utility/String.hpp"

#include "../Scripts/Script.hpp"

#include <stdio.h>

extern "C" {
#include <lua/lua.h>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
}

namespace ja
{
	typedef NameTag<setup::TAG_NAME_LENGTH> ScriptHashTagName;

	class HashTagMap
	{
	public:
		uint32_t hashTag;
		void*    pointer;

		HashTagMap(uint32_t hashTag, void* pointer) : hashTag(hashTag), pointer(pointer)
		{

		}
	};

	class ScriptManager
	{
	private:
		lua_State* L;
		static ScriptManager* singleton;
		ja::string scriptPath;
		ja::string fullPath;
		ja::string lastError;

		StackAllocator<ScriptFunction> updateFunctions;
		StackAllocator<ScriptFunction> initEntityFunctions;
		StackAllocator<ScriptFunction> constraintFunctions;
		StackAllocator<ScriptFunction> initDataFunctions;
		StackAllocator<ScriptFunction> accessDataFunctions;

		void registerEngineFunctions();
		ScriptManager();

		static int32_t runScriptFileI(lua_State* L)
		{
			const char* levelName = lua_tostring(L, 1);

			singleton->runScriptFile(levelName);

			return 0;
		}

		static int32_t panicHandler(lua_State* L);

	public:
		~ScriptManager();
		static void init();
		void setScriptPath(const ja::string& scriptPath);
		bool runScriptFile(const ja::string& scriptFile, bool useAbsolutePath = false);
		void runScript(const ja::string& script);
		void setGlobalVariable(const char* variableName, uint16_t variableValue);
		void setTable(const char* tableName);

		void registerFunction(const char* functionName, lua_CFunction func);
		void registerFunction(const ScriptFunction& scriptFunction);

		void cleanEnviroment(); // clean all function and global variables
		void dumpStack();
		lua_State* getLuaState();

		void printGlobals();
		void printTable(const char* tableName);

		const StackAllocator<ja::ScriptFunction>* getScriptFunctions(ja::ScriptFunctionType functionType);
		const ScriptFunction* getScriptFunction(ja::ScriptFunctionType functionType, uint32_t fileHash, uint32_t functionHash);
		
		ja::string getLastError()
		{
			return lastError; 
		}

		static inline uint32_t convertToHash(const char* tag)
		{
			return ja::math::convertToHash(tag);
		}

		static inline ScriptManager* getSingleton()
		{
			return singleton;
		}
		static void cleanUp();
	};

}
