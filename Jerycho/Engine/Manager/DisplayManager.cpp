#include "../jaSetup.hpp"
#include "DisplayManager.hpp"
#include "LogManager.hpp"
#include "../Utility/String.hpp"
#include "../Graphics/Material.hpp"
#include "../Graphics/RenderBuffer.hpp"
#include "../Font/Font.hpp"
#include "../Gui/Margin.hpp"

#include <iostream>
#include <vector>

#ifdef JA_SDL2_VERSION
#include <SDL2/SDL_syswm.h>
#else
#include <SDL/SDL_syswm.h>
#endif

glGetStringiFunc     glGetStringi = nullptr;
glGenerateMipmapFunc glGenerateMipmap = nullptr;
glActiveTextureFunc  glActiveTexture = nullptr;
glBlendEquationFunc  glBlendEquation = nullptr;
glCreateShaderFunc   glCreateShader = nullptr;
glShaderSourceFunc   glShaderSource = nullptr;
glCompileShaderFunc  glCompileShader = nullptr;
glUseProgramFunc     glUseProgram = nullptr;
glLinkProgramFunc    glLinkProgram = nullptr;
glAttachShaderFunc   glAttachShader = nullptr;
glCreateProgramFunc  glCreateProgram = nullptr;	
glDeleteShaderFunc   glDeleteShader = nullptr;
glDeleteProgramFunc  glDeleteProgram = nullptr;
glDetachShaderFunc   glDetachShader = nullptr;

glGetUniformLocationFunc        glGetUniformLocation = nullptr;
glGetActiveUniformFunc          glGetActiveUniform = nullptr;
glGetActiveUniformBlockNameFunc glGetActiveUniformBlockName = nullptr;
glGetActiveUniformsivFunc       glGetActiveUniformsiv = nullptr;
glGetUniformBlockIndexFunc      glGetUniformBlockIndex = nullptr;
glGetActiveUniformBlockivFunc   glGetActiveUniformBlockiv= nullptr;
glBindBufferBaseFunc            glBindBufferBase = nullptr;
glUniformBlockBindingFunc       glUniformBlockBinding = nullptr;
glUniform1iFunc                 glUniform1i = nullptr;
glUniform3fFunc                 glUniform3f = nullptr;
glUniform1fvFunc                glUniform1fv = nullptr;
glUniform1ivFunc                glUniform1iv = nullptr;
glUniform4fvFunc                glUniform4fv = nullptr;
glUniform3fvFunc                glUniform3fv = nullptr;
glUniformMatrix4fvFunc          glUniformMatrix4fv = nullptr;

glGenVertexArraysFunc    glGenVertexArrays = nullptr;
glDeleteVertexArraysFunc glDeleteVertexArrays = nullptr;
glBindVertexArrayFunc    glBindVertexArray;
glEnableVertexAttribArrayFunc  glEnableVertexAttribArray = nullptr;
glVertexAttribPointerFunc      glVertexAttribPointer = nullptr;
glVertexAttribIPointerFunc     glVertexAttribIPointer = nullptr;
glDisableVertexAttribArrayFunc glDisableVertexAttribArray = nullptr;

// opengl 2.0 standard
glGetShaderivFunc       glGetShaderiv = nullptr;
glGetProgramivFunc      glGetProgramiv = nullptr;
glGetShaderInfoLogFunc  glGetShaderInfoLog = nullptr;
glGetProgramInfoLogFunc glGetProgramInfoLog = nullptr;
// only with ARB
glGetObjectParameterivARBFunc glGetObjectParameterivARB = nullptr;
glGetInfoLogARBFunc           glGetInfoLogARB = nullptr;
//

glTexImage2DMultisampleFunc  glTexImage2DMultisample = nullptr;
glGenFramebuffersFunc        glGenFramebuffers = nullptr;
glBindFramebufferFunc        glBindFramebuffer = nullptr;
glFramebufferTexture2DFunc   glFramebufferTexture2D = nullptr;
glDrawBuffersFunc            glDrawBuffers = nullptr;
glBlitFramebufferFunc        glBlitFramebuffer = nullptr;
glCheckFramebufferStatusFunc glCheckFramebufferStatus = nullptr;
glDeleteFramebuffersFunc     glDeleteFramebuffers = nullptr;

//vbo
glGenBuffersFunc    glGenBuffers = nullptr;
glDeleteBuffersFunc glDeleteBuffers = nullptr;
glBindBufferFunc    glBindBuffer = nullptr;
glBufferDataFunc    glBufferData = nullptr;
glBufferSubDataFunc glBufferSubData = nullptr;
//vbo

extern "C" {
	_declspec(dllexport) int32_t NvOptimusEnablement = 0x00000001;
}

namespace ja
{

DisplayManager* DisplayManager::singleton = nullptr;

bool     DisplayManager::initializedDisplay = false;
double   DisplayManager::aspectRatio = 0;
double   DisplayManager::xUnits = 0;
double   DisplayManager::yUnits = 0;
float    DisplayManager::widthMultiplier = 0;
float    DisplayManager::heightMultiplier = 0;
int32_t  DisplayManager::colorR = 0;
int32_t  DisplayManager::colorG = 0;
int32_t  DisplayManager::colorB = 0;
int32_t  DisplayManager::resolutionWidth = 0;
int32_t  DisplayManager::resolutionHeight = 0;
uint32_t DisplayManager::renderingThreadId = 0;

jaMatrix44 DisplayManager::projectionMatrix;
jaMatrix44 DisplayManager::viewMatrix;
jaMatrix44 DisplayManager::projectionViewMatrix;

DisplayManager::DisplayManager()
{
	initializedDisplay = false;
	initializedResolution = false;
	isFullscreen = false;
	isWideScreen = true;
	clearR = 0;
	clearG = 0;
	clearB = 0;
	size = 1;
	colorR = 0;
	colorG = 0;
	colorB = 0;
}

DisplayManager::~DisplayManager()
{
}

void DisplayManager::initDisplayManager()
{
	LogManager* logManager = LogManager::getSingleton();

	if(!initializedDisplay)
	{
		singleton = new DisplayManager();
		renderingThreadId = SDL_ThreadID();
#ifdef JA_SDL_OPENGL
		if(!SDL_WasInit(SDL_INIT_VIDEO))
		{
			if(SDL_Init(SDL_INIT_VIDEO)==0)
			{
				logManager->addLogMessage(LogType::SUCCESS_LOG, "Video initialized");
			}
			else
			{
				logManager->addLogMessage(LogType::ERROR_LOG, "Video could not be initialized");
			}
		}

#ifdef JA_SDL2_VERSION
		SDL_GL_SetSwapInterval(1);
#else
		SDL_GL_SetAttribute( SDL_GL_SWAP_CONTROL, 0);
#endif
		
		initializedDisplay = true;
#endif JA_SDL_OPENGL
	}
}

void DisplayManager::initExtensions()
{
	singleton->openGLVersion = atof((char*)glGetString(GL_VERSION));

	if (singleton->openGLVersion >= 3.0)
	{
		glGetStringi = (glGetStringiFunc)SDL_GL_GetProcAddress("glGetStringi");
	}

	LogManager* logManager = LogManager::getSingleton();

	logManager->addLogMessage(LogType::INFO_LOG, "OpenGL version %f", getOpenglVersion());

	const char* openGlExt = (const char*)glGetString(GL_EXTENSIONS);
	logManager->addLogMessage(LogType::INFO_LOG, "Vendor Id: %s", glGetString(GL_VENDOR));
	logManager->addLogMessage(LogType::INFO_LOG, "Renderer: %s", glGetString(GL_RENDERER));
	logManager->addLogMessage(LogType::INFO_LOG, "Shading language version:  %s", glGetString(GL_SHADING_LANGUAGE_VERSION));

	const char* itNext = openGlExt;
	char        extNameBuffer[1024];
	int32_t     nextStringPos = 0;

	logManager->printLogsToCMD = false;

	if (nullptr == glGetStringi)
	{
		while (getDelimSeparatedString(itNext, ' ', extNameBuffer, nextStringPos))
		{
			logManager->addLogMessage(LogType::INFO_LOG, extNameBuffer);
			itNext = itNext + nextStringPos;
		}

		logManager->addLogMessage(LogType::INFO_LOG, extNameBuffer);
	}
	else
	{
		int32_t extensionsNum;
		
		glGetIntegerv(GL_NUM_EXTENSIONS, &extensionsNum);

		for (int32_t i = 0; i < extensionsNum; ++i)
		{
			const char* extensionName = (const char*)glGetStringi(GL_EXTENSIONS, i);
			logManager->addLogMessage(LogType::INFO_LOG, extensionName);
		}
	}

	logManager->printLogsToCMD = true;

	logManager->addLogMessage(LogType::INFO_LOG, "Available graphic card extensions saved to Config.log");

	//To render to fbo opengl 3.0 < x is needed
	if(getOpenglVersion() >= 3.0)
	{
		glBlendEquation = (glBlendEquationFunc)SDL_GL_GetProcAddress("glBlendEquation");
		glGenerateMipmap = (glGenerateMipmapFunc)SDL_GL_GetProcAddress("glGenerateMipmap");
		glActiveTexture = (glActiveTextureFunc)SDL_GL_GetProcAddress("glActiveTexture");

		glCreateShader = (glCreateShaderFunc)SDL_GL_GetProcAddress("glCreateShader");
		glShaderSource = (glShaderSourceFunc)SDL_GL_GetProcAddress("glShaderSource");
		glCompileShader = (glCompileShaderFunc)SDL_GL_GetProcAddress("glCompileShader");
		glUseProgram = (glUseProgramFunc)SDL_GL_GetProcAddress("glUseProgram");
		glLinkProgram = (glLinkProgramFunc)SDL_GL_GetProcAddress("glLinkProgram");
		glAttachShader = (glAttachShaderFunc)SDL_GL_GetProcAddress("glAttachShader");
		glCreateProgram = (glCreateProgramFunc)SDL_GL_GetProcAddress("glCreateProgram");
		glDeleteShader = (glDeleteShaderFunc)SDL_GL_GetProcAddress("glDeleteShader");
		glDeleteProgram = (glDeleteProgramFunc)SDL_GL_GetProcAddress("glDeleteProgram");
		glDetachShader = (glDetachShaderFunc)SDL_GL_GetProcAddress("glDetachShader");
		// opengl 2.0 standard
		glGetShaderiv = (glGetShaderivFunc)SDL_GL_GetProcAddress("glGetShaderiv");
		glGetProgramiv = (glGetProgramivFunc)SDL_GL_GetProcAddress("glGetProgramiv");
		glGetShaderInfoLog = (glGetShaderInfoLogFunc)SDL_GL_GetProcAddress("glGetShaderInfoLog");
		glGetProgramInfoLog = (glGetProgramInfoLogFunc)SDL_GL_GetProcAddress("glGetProgramInfoLog");

		glGetUniformLocation = (glGetUniformLocationFunc)SDL_GL_GetProcAddress("glGetUniformLocation");
		glGetActiveUniform = (glGetActiveUniformFunc)SDL_GL_GetProcAddress("glGetActiveUniform");
		glGetActiveUniformBlockName = (glGetActiveUniformBlockNameFunc)SDL_GL_GetProcAddress("glGetActiveUniformBlockName");
		glGetActiveUniformsiv = (glGetActiveUniformsivFunc)SDL_GL_GetProcAddress("glGetActiveUniformsiv");
		glGetUniformBlockIndex = (glGetUniformBlockIndexFunc)SDL_GL_GetProcAddress("glGetUniformBlockIndex");
		glGetActiveUniformBlockiv = (glGetActiveUniformBlockivFunc)SDL_GL_GetProcAddress("glGetActiveUniformBlockiv");
		glBindBufferBase = (glBindBufferBaseFunc)SDL_GL_GetProcAddress("glBindBufferBase");
		glUniformBlockBinding = (glUniformBlockBindingFunc)SDL_GL_GetProcAddress("glUniformBlockBinding");
		glUniform1i = (glUniform1iFunc)SDL_GL_GetProcAddress("glUniform1i");
		glUniform3f = (glUniform3fFunc)SDL_GL_GetProcAddress("glUniform3f");
		glUniform1fv = (glUniform1fvFunc)SDL_GL_GetProcAddress("glUniform1fv");
		glUniform1iv = (glUniform1ivFunc)SDL_GL_GetProcAddress("glUniform1iv");
		glUniform3fv = (glUniform3fvFunc)SDL_GL_GetProcAddress("glUniform3fv");
		glUniform4fv = (glUniform4fvFunc)SDL_GL_GetProcAddress("glUniform4fv");
		glUniformMatrix4fv = (glUniformMatrix4fvFunc)SDL_GL_GetProcAddress("glUniformMatrix4fv");

		glGenFramebuffers = (glGenFramebuffersFunc)SDL_GL_GetProcAddress("glGenFramebuffers");
		glBindFramebuffer = (glBindFramebufferFunc)SDL_GL_GetProcAddress("glBindFramebuffer");
		glFramebufferTexture2D = (glFramebufferTexture2DFunc)SDL_GL_GetProcAddress("glFramebufferTexture2D");
		glDrawBuffers = (glDrawBuffersFunc)SDL_GL_GetProcAddress("glDrawBuffers");
		glBlitFramebuffer = (glBlitFramebufferFunc)SDL_GL_GetProcAddress("glBlitFramebuffer");
		glCheckFramebufferStatus = (glCheckFramebufferStatusFunc)SDL_GL_GetProcAddress("glCheckFramebufferStatus");
		glDeleteFramebuffers = (glDeleteFramebuffersFunc)SDL_GL_GetProcAddress("glDeleteFramebuffers");

		//vbo
		glGenBuffers = (glGenBuffersFunc)SDL_GL_GetProcAddress("glGenBuffers");
		glDeleteBuffers = (glDeleteBuffersFunc)SDL_GL_GetProcAddress("glDeleteBuffers");
		glBindBuffer = (glBindBufferFunc)SDL_GL_GetProcAddress("glBindBuffer");
		glBufferData = (glBufferDataFunc)SDL_GL_GetProcAddress("glBufferData");
		glBufferSubData = (glBufferSubDataFunc)SDL_GL_GetProcAddress("glBufferSubData");

		glGenVertexArrays    = (glGenVertexArraysFunc)SDL_GL_GetProcAddress("glGenVertexArrays");
		glDeleteVertexArrays = (glDeleteVertexArraysFunc)SDL_GL_GetProcAddress("glDeleteVertexArrays");
		glBindVertexArray          = (glBindVertexArrayFunc)SDL_GL_GetProcAddress("glBindVertexArray");
		glEnableVertexAttribArray  = (glEnableVertexAttribArrayFunc)SDL_GL_GetProcAddress("glEnableVertexAttribArray");
		glVertexAttribPointer      = (glVertexAttribPointerFunc)SDL_GL_GetProcAddress("glVertexAttribPointer");
		glVertexAttribIPointer     = (glVertexAttribIPointerFunc)SDL_GL_GetProcAddress("glVertexAttribIPointer");
		glDisableVertexAttribArray = (glDisableVertexAttribArrayFunc)SDL_GL_GetProcAddress("glDisableVertexAttribArray");

		glTexImage2DMultisample = (glTexImage2DMultisampleFunc)SDL_GL_GetProcAddress("glTexImage2DMultisample");
	}
	else
	{
		logManager->addLogMessage(LogType::ERROR_LOG, "OpenGL 3.0 is not supported");
	}
	
	{
		int32_t intVar;
		//GL_MAX_COMPUTE_SHADER_STORAGE_BLOCKS
		//GL_MAX_COMBINED_SHADER_STORAGE_BLOCKS
		//GL_MAX_COMPUTE_UNIFORM_BLOCKS
		//GL_MAX_COMPUTE_WORK_GROUP_COUNT
		glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_SIZE, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_COMPUTE_WORK_GROUP_SIZE = %d", intVar);
		//GL_MAX_ARRAY_TEXTURE_LAYERS
		//GL_MAX_COLOR_TEXTURE_SAMPLES
		glGetIntegerv(GL_MAX_COMBINED_UNIFORM_BLOCKS, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_COMBINED_UNIFORM_BLOCKS = %d", intVar);
		glGetIntegerv(GL_MAX_ELEMENTS_INDICES, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_ELEMENTS_INDICES = %d", intVar);
		glGetIntegerv(GL_MAX_ELEMENTS_VERTICES, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_ELEMENTS_VERTICES = %d", intVar);
		
		//GL_MAX_FRAGMENT_SHADER_STORAGE_BLOCKS
		//GL_MAX_FRAGMENT_INPUT_COMPONENTS
		//GL_MAX_FRAGMENT_UNIFORM_COMPONENTS
		//GL_MAX_FRAGMENT_UNIFORM_VECTORS
		glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_BLOCKS, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_FRAGMENT_UNIFORM_BLOCKS = %d", intVar);
		glGetIntegerv(GL_MAX_FRAMEBUFFER_WIDTH, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_FRAMEBUFFER_WIDTH = %d", intVar);
		glGetIntegerv(GL_MAX_FRAMEBUFFER_HEIGHT, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_FRAMEBUFFER_HEIGHT = %d", intVar);
		glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_COLOR_ATTACHMENTS = %d", intVar);
		//GL_MAX_FRAMEBUFFER_LAYERS
		//GL_MAX_FRAMEBUFFER_SAMPLES
		//GL_MAX_GEOMETRY_SHADER_STORAGE_BLOCKS
		//GL_MAX_GEOMETRY_INPUT_COMPONENTS
		//GL_MAX_GEOMETRY_OUTPUT_COMPONENTS
		//GL_MAX_GEOMETRY_TEXTURE_IMAGE_UNITS
		glGetIntegerv(GL_MAX_GEOMETRY_UNIFORM_BLOCKS, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_GEOMETRY_UNIFORM_BLOCKS = %d", intVar);
		//GL_MAX_GEOMETRY_UNIFORM_COMPONENTS
		glGetIntegerv(GL_MAX_RECTANGLE_TEXTURE_SIZE, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_RECTANGLE_TEXTURE_SIZE = %d", intVar);
		//GL_MAX_RENDERBUFFER_SIZE
		//GL_MAX_TEXTURE_BUFFER_SIZE
		//GL_MAX_TEXTURE_IMAGE_UNITS
		glGetIntegerv(GL_MAX_TEXTURE_SIZE, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_TEXTURE_SIZE = %d", intVar);
		glGetIntegerv(GL_MAX_UNIFORM_BUFFER_BINDINGS, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_UNIFORM_BUFFER_BINDINGS = %d", intVar);
		glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_UNIFORM_BLOCK_SIZE = %d", intVar);
		glGetIntegerv(GL_MAX_UNIFORM_LOCATIONS, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_UNIFORM_LOCATIONS = %d", intVar);
		glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT = %d", intVar);
		//GL_MAX_VARYING_COMPONENTS
		//GL_MAX_VERTEX_SHADER_STORAGE_BLOCKS
		//GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS
		glGetIntegerv(GL_MAX_VERTEX_UNIFORM_COMPONENTS, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_VERTEX_UNIFORM_COMPONENTS = %d", intVar);
		//GL_MAX_VERTEX_OUTPUT_COMPONENTS
		glGetIntegerv(GL_MAX_VERTEX_UNIFORM_BLOCKS, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_VERTEX_UNIFORM_BLOCKS = %d", intVar);
		glGetIntegerv(GL_UNIFORM_BUFFER_BINDING, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_UNIFORM_BUFFER_BINDING = %d", intVar);
		glGetIntegerv(GL_UNIFORM_BUFFER_SIZE, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_UNIFORM_BUFFER_SIZE = %d", intVar);
		glGetIntegerv(GL_MAX_ELEMENT_INDEX, &intVar);
		logManager->addLogMessage(LogType::INFO_LOG, "GL_MAX_ELEMENT_INDEX = %d", intVar);
	}

}

void DisplayManager::setSmoothGraphics()
{
	//GL_FRAGMENT_SHADER_DERIVATIVE_HINT
	//GL_GENERATE_MIPMAP_HINT
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glEnable (GL_POLYGON_SMOOTH);
	glHint (GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
}

void DisplayManager::setWindowTitle(const char* windowName)
{
#ifdef JA_SDL_API
	
#ifdef JA_SDL2_VERSION
	SDL_SetWindowTitle(openglWindow, windowName);
#else
	SDL_WM_SetCaption(windowName, windowName);
#endif
	
#endif
}

void DisplayManager::setWindowPosition(int32_t posX, int32_t posY)
{
#ifdef JA_SDL_API

	#ifdef JA_SDL2_VERSION
		SDL_SetWindowPosition(openglWindow,posX,posY);
	#else
		SDL_SysWMinfo windowInfo;
		SDL_VERSION(&windowInfo.version);

		if (SDL_GetWMInfo(&windowInfo))
		{
			//Then, we retrieve the Windows specific 'handle' to the SDL window.
			HWND handle = windowInfo.window;

			//Finally, we can set the position of the window, using the Win32 function, 'SetWindowPos'.
			BOOL result = SetWindowPos(handle, nullptr, posX, posY, 0, 0,
				SWP_NOREPOSITION | SWP_NOZORDER | SWP_NOSIZE | SWP_NOACTIVATE);

			if (result == FALSE)
			{
				//Error occurred with 'SetWindowPos'
			}
		}
		else
		{
			//Error occurred with 'SDL_GetWMInfo' 
		}
	#endif
	
#endif
}

void DisplayManager::setMousePositionXY(int32_t x, int32_t y)
{
#ifdef JA_SDL_API
#ifdef JA_SDL2_VERSION
	SDL_WarpMouseInWindow(openglWindow, x, y);
#else
	SDL_EventState(SDL_MOUSEMOTION, SDL_IGNORE);
	SDL_WarpMouse(x, y);
	SDL_EventState(SDL_MOUSEMOTION, SDL_ENABLE);
#endif
#endif
}

void DisplayManager::setResolution(ScreenResolution resolution, int32_t red, int32_t green, int32_t blue, int32_t alpha, bool isFullscreen, bool isVSync, int32_t multisampleSamples)
{
	this->isFullscreen       = isFullscreen;
	this->multisampleSamples = 0;

	switch(resolution)
	{
	case res800x600:
		this->isWideScreen     = false;
		this->resolutionWidth  = 800;
		this->resolutionHeight = 600;
		this->aspectRatio      = 800.0 / 600.0;
		this->xUnits           = 800.0 / (double)setup::graphics::PIXELS_PER_UNIT;
		this->yUnits           = 600.0 / (double)setup::graphics::PIXELS_PER_UNIT;
		this->widthMultiplier  = this->xUnits / (float)this->resolutionWidth;
		this->heightMultiplier = this->yUnits / (float)this->resolutionHeight;
		break;
	case res1024x768:
		this->isWideScreen     = false;
		this->resolutionWidth  = 1024;
		this->resolutionHeight = 768;
		this->aspectRatio      = 1024.0 / 768.0;
		this->xUnits           = 1024.0 / (double)setup::graphics::PIXELS_PER_UNIT;
		this->yUnits           = 768.0  / (double)setup::graphics::PIXELS_PER_UNIT;
		this->widthMultiplier  = this->xUnits / (float)resolutionWidth;
		this->heightMultiplier = this->yUnits / (float)resolutionHeight;
		break;
	case res1280x720:
		this->isWideScreen     = true;
		this->resolutionWidth  = 1280;
		this->resolutionHeight = 720;
		this->aspectRatio      = 1280.0 / 720.0;
		this->xUnits           = 1280.0 / (double)setup::graphics::PIXELS_PER_UNIT;
		this->yUnits           = 720.0  / (double)setup::graphics::PIXELS_PER_UNIT;
		this->widthMultiplier  = this->xUnits / (float)this->resolutionWidth;
		this->heightMultiplier = this->yUnits / (float)this->resolutionHeight;
		break;
	case res1366x768:
		this->isWideScreen     = true;
		this->resolutionWidth  = 1366;
		this->resolutionHeight = 768;
		this->aspectRatio      = 1366.0 / 768.0;
		this->xUnits           = 1366.0 / (double)setup::graphics::PIXELS_PER_UNIT;
		this->yUnits           = 768.0  / (double)setup::graphics::PIXELS_PER_UNIT;
		this->widthMultiplier  = this->xUnits / (float)this->resolutionWidth;
		this->heightMultiplier = this->yUnits / (float)this->resolutionHeight;
		break;
	}

#ifdef JA_SDL_OPENGL

#ifdef JA_SDL2_VERSION
	//Use OpenGL 2.1
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1 );
	//SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	//SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	//SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY); //SDL_GL_CONTEXT_PROFILE_CORE,SDL_GL_CONTEXT_PROFILE_COMPATIBILITY,SDL_GL_CONTEXT_PROFILE_ES
	//SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // SDL_GL_CONTEXT_DEBUG_FLAG, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG, SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG, SDL_GL_CONTEXT_RESET_ISOLATION_FLAG


	if (multisampleSamples > 0)
	{
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, multisampleSamples);
	}

	ja::LogManager* logManager = LogManager::getSingleton();
	
	//Create window
	this->openglWindow = SDL_CreateWindow( "Jerycho", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, this->resolutionWidth, this->resolutionHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if( this->openglWindow == nullptr )
	{
		logManager->addLogMessage(LogType::ERROR_LOG, "Window could not be created! SDL Error: %s", SDL_GetError());

		return;
	}
	else
	{
		logManager->addLogMessage(LogType::INFO_LOG, "Created window resolution x:%d y:%d fullscreen:%d", this->resolutionWidth, this->resolutionHeight, this->isFullscreen);

		this->openglContext = SDL_GL_CreateContext( this->openglWindow );
		if( this->openglContext == nullptr )
		{
			logManager->addLogMessage(LogType::ERROR_LOG, "OpenGL context could not be created! SDL Error: %s", SDL_GetError());
			return;
		}
		else
		{
			//Use Vsync
			if (isVSync)
			{
				SDL_GL_SetSwapInterval(-1);

				int32_t swapIntervalResult = SDL_GL_GetSwapInterval();
				if (-1 != swapIntervalResult)
				{
					SDL_GL_SetSwapInterval(1);
					logManager->addLogMessage(LogType::INFO_LOG, "Full VSync has been set");
				}
				else
				{
					logManager->addLogMessage(LogType::INFO_LOG, "Half VSync has been set");
				}
			}
			else
			{
				SDL_GL_SetSwapInterval(0);
				logManager->addLogMessage(LogType::INFO_LOG, "No VSync has been set");
			}
		}
	}

	
	if (multisampleSamples > 0)
	{
		SDL_GL_GetAttribute(SDL_GL_MULTISAMPLESAMPLES, &this->multisampleSamples);

		if (this->multisampleSamples > 0)
		{
			glEnable(GL_MULTISAMPLE);
			logManager->addLogMessage(LogType::INFO_LOG, "Multisampling enabled");
		}		
	}

	
	
#else
	if(initializedResolution)
	{
		SDL_FreeSurface(screen);
	}
	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, red );
	SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, green );
	SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, blue );
	SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, alpha );

	//SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	//SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	//
	//SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 8 );
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 16);
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

	if(isVSync) // Podobna dzia�a na windowsach z sdl 1.2 < x
		SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, TRUE); // turn v-sync
	else
		SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, FALSE); // turn v-sync

	if (multisampleSamples > 0)
	{
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, multisampleSamples);
	}
	
	if(isFullscreen)
	{
		screen = SDL_SetVideoMode(resolutionWidth, resolutionHeight, red+green+blue+alpha, SDL_DOUBLEBUF | SDL_HWSURFACE | SDL_OPENGL | SDL_FULLSCREEN);
	}
	else
	{
		screen = SDL_SetVideoMode(resolutionWidth, resolutionHeight, red+green+blue+alpha, SDL_DOUBLEBUF | SDL_HWSURFACE | SDL_OPENGL);
	}

	if (multisampleSamples > 0)
	{
		SDL_GL_GetAttribute(SDL_GL_MULTISAMPLESAMPLES, &this->multisampleSamples);

		if (this->multisampleSamples > 0)
		{
			glEnable(GL_MULTISAMPLE);
		}
	}
#endif
	
	glViewport(0,0,resolutionWidth,resolutionHeight);

	glMatrixMode(GL_PROJECTION);        
	glLoadIdentity();
	gluPerspective(60,aspectRatio,0.01f,100.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity(); 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDepthFunc(GL_LEQUAL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING); 
	glDisable(GL_CULL_FACE);
	glDisable(GL_STENCIL_TEST);

	glShadeModel(GL_SMOOTH);
	//glShadeModel(GL_SMOOTH);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	gluLookAt (0.0f, 0.0f, -5.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

#endif JA_SDL_OPENGL

	initializedResolution = true;
}


void DisplayManager::setBackgroundColor( uint8_t r, uint8_t g, uint8_t b)
{
	this->clearR = r;
	this->clearG = g;
	this->clearB = b;
#ifdef JA_SDL_OPENGL
	glClearColor((float)(clearR) / 256.0f, (float)(clearG) / 256.0f, (float)(clearB) / 256.0f, 0.0f);
#endif JA_SDL_OPENGL
}

void DisplayManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		if(getSingleton()->initializedDisplay)
		{
#ifdef JA_SDL_OPENGL
			if(SDL_WasInit(SDL_INIT_VIDEO))
			{
#ifdef JA_SDL2_VERSION
				SDL_DestroyWindow(singleton->openglWindow);
				singleton->openglWindow = nullptr;
#else
				SDL_FreeSurface(getSingleton()->screen);
#endif
				std::cout << "Video deinitialized" << std::endl;
			}

		initializedDisplay = false;
#endif JA_SDL_OPENGL
		}
		delete singleton;
		singleton = nullptr;
	}
}

void DisplayManager::getWorldResolution(double* widthUnits, double* heightUnits)
{
	*widthUnits = xUnits;
	*heightUnits = yUnits;
}

void DisplayManager::getWorldResolution(float* widthUnits, float* heightUnits)
{
	*widthUnits = (float)xUnits;
	*heightUnits = (float)yUnits;
}

void DisplayManager::getResolution( int32_t* width, int32_t* height )
{
	*width = resolutionWidth;
	*height = resolutionHeight;
}

void DisplayManager::RenderText( Font* font, float x, float y, const char* text, uint32_t marginStyle, uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	glPushMatrix(); 

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glLoadIdentity();

	glColor4ub(r,g,b,a);
	glListBase(font->getListBase());

	CharLine<char,uint32_t> line;
	StackAllocator<CharLine<char,uint32_t>> lines; 
	line.start    = text;
	line.charsNum = 0;
	line.width    = 0;

	for(const char *c=text;*c;c++)
	{
		if(*c=='\n')
		{
			lines.push(line); 
			line.charsNum = 0;
			line.width    = 0;
			line.start    = c + 1;

		}
		else
		{
			line.charsNum++;
			line.width += font->letter[*c].advanceX;
		}
	}
	lines.push(line); 

	const uint32_t size = lines.getSize(); 
	CharLine<char,uint32_t>* pLines = lines.getFirst();
	int32_t     textHeight = size * font->getHighestCharacterSize();  
	uint32_t        iflags = marginStyle & 4294967288;

	if(iflags != (uint32_t)Margin::NORMAL)
	{
		switch(iflags)
		{
			case (uint32_t)Margin::TOP:
				y = y - font->getHighestCharacterSize();  
				break;
			case (uint32_t)Margin::DOWN:
				y = y + textHeight - font->getHighestCharacterSize(); 
				break;
			case (uint32_t)Margin::MIDDLE:
				y = y - font->getHighestCharacterSize() + (textHeight / 2); 
				break;
		}
	}

	iflags = marginStyle & 7;
	for(uint32_t i=0; i<size; i++)
	{
		glLoadIdentity();
		switch(iflags)
		{
		case (uint32_t)Margin::LEFT:
			glTranslatef(x, y - i*font->getHighestCharacterSize(), 0);
			break;
		case (uint32_t)Margin::CENTER:
			glTranslatef(x - (pLines[i].width / 2), y - i*font->getHighestCharacterSize(), 0);
			break;
		case (uint32_t)Margin::RIGHT:
			glTranslatef(x - pLines[i].width, y - i*font->getHighestCharacterSize(), 0);
			break;
		default:
			glTranslatef(x, y - i*font->getHighestCharacterSize(), 0);
			break;
		}
		glCallLists(pLines[i].charsNum,GL_BYTE,pLines[i].start);
	}


	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
}

void DisplayManager::RenderTextWorld(Font* font, float x, float y, float scale, const char* text, uint32_t marginStyle, uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	glPushMatrix();

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glColor4ub(r,g,b,a);
	glListBase(font->getListBase());

	CharLine<char,uint32_t> line;
	StackAllocator<CharLine<char,uint32_t>> lines; 
	line.start    = text;
	line.charsNum = 0;
	line.width    = 0;

	for(const char *textIt=text;*textIt;textIt++)
	{
		if(*textIt=='\n')
		{
			lines.push(line); 
			line.charsNum = 0;
			line.width    = 0;
			line.start    = textIt + 1;

		}
		else
		{
			line.charsNum++;
			line.width += font->letter[*textIt].advanceX;
		}
	}
	
	lines.push(line); 

	uint32_t                   size = lines.getSize(); 
	CharLine<char,uint32_t>* pLines = lines.getFirst();
	int32_t              textHeight = size * font->getHighestCharacterSize(); 
	uint32_t                 iflags = marginStyle & 4294967288;

	if(iflags != (uint32_t)Margin::NORMAL)
	{
		switch(iflags)
		{
		case (uint32_t)Margin::TOP:
			y = y + scale * font->getHighestCharacterSize(); 
			break;
		case (uint32_t)Margin::DOWN:
			y = y - scale * textHeight;
			break;
		case (uint32_t)Margin::MIDDLE:
			y = y + scale * (font->getHighestCharacterSize() + (textHeight / 2));
			break;
		}
	}

	iflags = marginStyle & 7;
	for(uint32_t i=0;i<size;i++)
	{
		glPushMatrix();

		switch(iflags)
		{
		case (uint32_t)Margin::LEFT:
			glTranslatef(x, y - scale*(i*font->getHighestCharacterSize()), 0); 
			break;
		case (uint32_t)Margin::CENTER:
			glTranslatef(x - scale* (pLines[i].width / 2), y - scale*(i*font->getHighestCharacterSize()), 0); 
			break;
		case (uint32_t)Margin::RIGHT:
			glTranslatef(x - scale* pLines[i].width, y - scale * (i*font->getHighestCharacterSize()), 0); 
			break;
		default:
			glTranslatef(x,y-scale*(i*font->getHighestCharacterSize()),0);
			break;
		}
		glScalef(scale,scale,1.0f);
		glCallLists(pLines[i].charsNum, GL_BYTE, pLines[i].start); 

		glPopMatrix(); 
	}

	glPopAttrib();
	glPopMatrix();
}

void DisplayManager::RenderText(Font* font, float x, float y, const wchar_t* text, uint32_t marginStyle, uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	glPushMatrix();

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glLoadIdentity();

	glColor4ub(r,g,b,a);
	glListBase(font->getListBase());

	CharLine<wchar_t,uint32_t> line;
	StackAllocator<CharLine<wchar_t, uint32_t>> lines; 
	line.start    = text;
	line.charsNum = 0;
	line.width    = 0;

	for(const wchar_t *c=text;*c;c++)
	{
		if(*c=='\n')
		{
			lines.push(line); 
			line.charsNum = 0;
			line.width    = 0;
			line.start    = c + 1;

		}
		else
		{
			line.charsNum++;
			line.width += font->letter[*c].advanceX; 
		}
	}
	lines.push(line); //lines.push_back(line);

	uint32_t  size = lines.getSize();
	CharLine<wchar_t, uint32_t>* pLines = lines.getFirst(); 
	int32_t  textHeight = size * font->getHighestCharacterSize(); 
	uint32_t iflags     = marginStyle & 4294967288;

	if(iflags != (uint32_t)Margin::NORMAL)
	{
		switch(iflags)
		{
		case (uint32_t)Margin::TOP:
			y = y - font->getHighestCharacterSize(); 
			break;
		case (uint32_t)Margin::DOWN:
			y = y + textHeight - font->getHighestCharacterSize(); 
			break;
		case (uint32_t)Margin::MIDDLE:
			y = y - font->getHighestCharacterSize() + (textHeight / 2); 
			break;
		}
	}

	iflags = marginStyle & 7;
	for(uint32_t i=0;i<size;i++)
	{
		glLoadIdentity();
		switch(iflags)
		{
		case (uint32_t)Margin::LEFT:
			glTranslatef(x, y - i*font->getHighestCharacterSize(), 0); 
			break;
		case (uint32_t)Margin::CENTER:
			glTranslatef(x - (pLines[i].width / 2), y - i*font->getHighestCharacterSize(), 0); 
			break;
		case (uint32_t)Margin::RIGHT:
			glTranslatef(x - pLines[i].width, y - i*font->getHighestCharacterSize(), 0); 
			break;
		default:
			glTranslatef(x, y - i*font->getHighestCharacterSize(), 0);
			break;
		}
		glCallLists(pLines[i].charsNum,GL_SHORT,pLines[i].start);
	}

	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);

	glPopMatrix();
}

void DisplayManager::RenderText(RenderBuffer& renderBuffer, const Material* const material, const uint8_t* const shaderObjectData, const jaMatrix44& transformMatrix, const FontPolygon* const font, const char* const text, uint32_t marginStyle, const Color4f& fontColor)
{

	if (renderBuffer.checkIfNewRenderCommandNeeded(material))
	{
		renderBuffer.addRenderCommand(material);
	}

	CharLine<char,float> line;
	StackAllocator<CharLine<char,float>> lines;

	line.start    = text;
	line.charsNum = 0;
	line.width    = 0;

	uint32_t totalVerticesNum = 0;
	uint32_t totalIndicesNum  = 0;

	for (const char *textIt = text; *textIt; textIt++)
	{
		if (*textIt == '\n')
		{
			lines.push(line); 
			line.charsNum = 0;
			line.width    = 0.0f;
			line.start    = textIt + 1;
		}
		else
		{
			line.charsNum++;
			line.width += font->letter[*textIt].advanceX;

			totalVerticesNum += font->letter[*textIt].verticesNum;
			totalIndicesNum  += font->letter[*textIt].indicesNum;
		}
	}
	lines.push(line); //lines.push_back(line);

	jaVector2* pVerticesArray       = renderBuffer.verticesPositions.pushArray(totalVerticesNum);
	uint32_t*  pIndicesArray        = renderBuffer.indices.pushArray(totalIndicesNum);
	Color4f*   pColorsArray         = renderBuffer.colors.pushArray(totalVerticesNum);
	jaVector4* pNormalsArray        = renderBuffer.normals.pushArray(totalVerticesNum);
	Color4f*   pSecondaryColorArray = renderBuffer.secondaryColors.pushArray(totalVerticesNum);

	{
		jaMatrix44* modelMatrix = renderBuffer.modelMatrices.push();
		jaMatrix44* rotMatrix = renderBuffer.rotMatrices.push();

		*modelMatrix = transformMatrix;
		*rotMatrix = transformMatrix;
	}

	{
		const int32_t materialId = material->getMaterialId();
		int32_t* pMaterialsArray = renderBuffer.materialIds.pushArray(totalVerticesNum);

		memsetInt32_SSE2(pMaterialsArray, materialId, totalVerticesNum);
	}

	{
		const int32_t  objectId  = renderBuffer.currentRenderCommand->getObjectsNum();
		int32_t* pObjectIdsArray = renderBuffer.objectIds.pushArray(totalVerticesNum);

		memsetInt32_SSE2(pObjectIdsArray, objectId, totalVerticesNum);
	}

	{
		ShaderBinder*  shaderBinder      = material->getShaderBinder();
		const uint32_t shaderObjectSize  = shaderBinder->getShaderObjectSize();
		uint8_t*       pShaderObjectData = renderBuffer.shaderObjectData.pushArray(shaderObjectSize);

		memcpy(pShaderObjectData, shaderObjectData, shaderObjectSize);
	}

	const uint32_t          size = lines.getSize(); //int32_t  size        = lines.size();
	CharLine<char,float>* pLines = lines.getFirst();
	const float       textHeight = (float)size * font->getHighestCharacterSize();
	uint32_t              iflags = marginStyle & 4294967288;

	float x = 0.0f;
	float y = 0.0f;

	if (iflags != (uint32_t)Margin::NORMAL)
	{
		switch (iflags)
		{
		case (uint32_t)Margin::TOP:
			y = y - font->getHighestCharacterSize();
			break;
		case (uint32_t)Margin::DOWN:
			y = y + textHeight - font->getHighestCharacterSize();
			break;
		case (uint32_t)Margin::MIDDLE:
			y = y - font->getHighestCharacterSize() + (textHeight * 0.5f);
			break;
		}
	}

	uint32_t letterVerticesIt = 0;
	uint32_t letterIndicesIt  = 0;

	iflags = marginStyle & 7;
	for (uint32_t i = 0; i<size; i++)
	{
		float advanceX = 0.0f;
		float advanceY = y - (float)i * font->getHighestCharacterSize();

		switch (iflags)
		{
		case (uint32_t)Margin::LEFT:
			break;
		case (uint32_t)Margin::CENTER:
			advanceX = x - (pLines[i].width * 0.5f);
			break;
		case (uint32_t)Margin::RIGHT:
			advanceX = x - pLines[i].width;
			break;
		}

		const uint32_t renderBufferVerticesNum = renderBuffer.getTotalVerticesNum();
		
		const char* pCharIt = pLines[i].start;

		for(uint32_t z=0; z<pLines[i].charsNum; ++z, ++pCharIt)
		{
			uint32_t   letterVerticesNum = font->letter[*pCharIt].verticesNum;
			jaVector2* pLetterVertices   = font->letter[*pCharIt].vertices;
			uint32_t   letterIndicesNum  = font->letter[*pCharIt].indicesNum;
			uint32_t*  pLetterIndices    = font->letter[*pCharIt].indices;

			jaVectormath2::copyVector2WithTranslation_SSE(&pVerticesArray[letterVerticesIt], pLetterVertices, jaVector2((float)advanceX, (float)advanceY), letterVerticesNum);
			memcpyUint32ArrayWithOffset_SSE2(&pIndicesArray[letterIndicesIt], pLetterIndices, letterIndicesNum, letterVerticesIt + renderBufferVerticesNum);
			memcpyFloat4_SSE2_aligned16((float*)&pColorsArray[letterVerticesIt], (float*)&Color4f(1.0f, 0.0f, 0.0f, 1.0f), letterVerticesNum);

			letterVerticesIt += letterVerticesNum;
			letterIndicesIt  += letterIndicesNum;
			advanceX         += font->letter[*pCharIt].advanceX;
		}
	}

	renderBuffer.addObject();
	renderBuffer.addIndices(totalIndicesNum);
	renderBuffer.addVertices(totalVerticesNum);
}

void DisplayManager::enableLighting()
{
	glEnable(GL_LIGHTING);
}

void DisplayManager::disableLighting()
{
	glDisable(GL_LIGHTING);
}

void DisplayManager::unbindAllTextures()
{
	for(char i=setup::graphics::MAX_TEXTURE_PROXIES - 1; i >= 0; --i)
	{
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}


}