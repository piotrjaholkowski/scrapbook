#pragma once

#include "../jaSetup.hpp"
#include "../Gui/Widget.hpp"
#include "../Gui/Interface.hpp"
#include "../Gui/WidgetDef.hpp"
#include <map>
#include <string>

#ifdef JA_SDL2_VERSION
	#include <SDL2/SDL_mutex.h>
#else
	#include <SDL/SDL_mutex.h>
#endif


#define JA_GUI_BUTTON_MARGIN 2

namespace ja
{

	class UIManager
	{
	private:
		static UIManager* singleton;
		static std::map<ja::string, Interface*> interfaces;
		static std::map<ja::string, ScriptDelegate*> scriptDelegates;

		static bool IsMouseOverGui;
		static Widget* lastSelected;
		static Widget* lastDragged;
		static Widget* lastClicked;
		static Widget* mouseOverWidget; // Temporary data
		static Widget* topWidget;

		float deltaTime;

		static SDL_mutex* guiMutex;

		UIManager();
	public:
		static int32_t mouseX;
		static int32_t mouseY;
		static int32_t draggedRelPosX;
		static int32_t draggedRelPosY;
		WidgetDef widgetDef;
		~UIManager();

		static inline UIManager* UIManager::getSingleton()
		{
			return singleton;
		}

		static void init();
		void setDefaultParameters();
		static void cleanUp();

		static void setMouseOverGui(bool state);
		static bool isMouseOverGui();
		static bool isTextInputSelected();
		static void setSelected(Widget* widget);
		static void setLastClicked(Widget* widget);
		static void setDragged(Widget* widget);
		static bool isMouseOver(Widget* widget);
		static void setTopWidget(Widget* widget);
		static void setUnselected();
		static Widget* getLastSelected();
		static Widget* getLastClicked();
		static Widget* getLastDragged();
		static Widget* getTopWidget();

		Widget* createWidgetDef();

		void addInterface(ja::string name, Interface* newInterface);
		void addInterfaceSafe(ja::string name, Interface* newInterface);

		Interface* getInterface(ja::string& name);

		ScriptDelegate* registerDelegate(ja::string& delegateName);

		void changeInterface(ja::string name);
		void update(float deltaTime);
		void updateSafe(float deltaTime);
		float getDeltaTime();
		void draw();
		void drawSafe();
		Widget* getWidget(uint32_t id);
	};

}
