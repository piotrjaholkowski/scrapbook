#pragma once

#include "../Utility/String.hpp"
#include "../Allocators/StackAllocator.hpp"
#include <stdint.h>

namespace ja
{

enum class LogType
{
	SUCCESS_LOG  = 0,
	WARNNING_LOG = 1,
	INFO_LOG     = 2,
	ERROR_LOG    = 3
};

class LogMessage
{
public:
	LogType  logType;
	uint32_t logMessageOffset;
	uint32_t logSize;
	char*    message;
};

class LogManager
{
private:
	static LogManager* singleton;
	
	StackAllocator<LogMessage> logMessages;
	char*      logBuffer;
	uint32_t   logBufferSize;

	uint32_t   logBufferAllocationSize = 8192;

	char*      logBufferCurrentPos;
	char       logLineBuffer[8192];
	ja::string lastLog;

public:
	bool       printLogsToCMD;
private:

	LogManager();
	~LogManager();

	/////Exception handlers
	static void SigabrtHandler(int);
	static void SigintHandler(int);
	static void SigtermHandler(int);

	static void SigfpeHandler(int /*code*/, int subcode);
	static void SigillHandler(int);
	static void SigsegvHandler(int);
	////////////////////
	void logDevices(); // Save information about devices like graphic card, processor
public:
	static void initLogManager();

	void setProcessExceptionHandlers(); //Sets exception handler for current process
	void setThreadExceptionHandlers(); // Sets exception handlers for current thread
	void saveCrashReport();
	void clearLogs();
	void        addLogMessage(LogType logType, const char* fieldValue, ...);
	LogMessage* getLogMessage(uint32_t id);
	uint32_t    getLogsNum();
	void        saveLog(const char* fileName);
	
	uint32_t getCurrentLogFileOffsetPosition();

	inline void setPrintOnCMD(bool state)
	{
		this->printLogsToCMD = state;
	}

	static inline LogManager* LogManager::getSingleton()
	{
		return singleton;
	}

	static void cleanUp();

	friend class jaCyclicFunctionLog;
};

}

