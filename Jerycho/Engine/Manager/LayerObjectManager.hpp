#pragma once

#include "../jaSetup.hpp"
#include "../Graphics/LayerObjectDef.hpp"
#include "../Graphics/DrawElements.hpp"
#include "../Graphics/Polygon.hpp"
#include "../Graphics/Light.hpp"
#include "../Graphics/Camera.hpp"

#include "../Math/VectorMath2d.hpp"
#include "../Gilgamesh/Scene2d.hpp"
#include "../Gilgamesh/Broad/HashGrid.hpp"
#include "../Gilgamesh/Narrow/Narrow2d.hpp"

#include "../Interface/CameraInterface.hpp"


#include "../Allocators/CacheLineAllocator.hpp"

#include <stdint.h>

namespace ja
{
	class LayerObjectManager;

	typedef LayerObject*(*LayerObjectCreatorT)(CacheLineAllocator*, const LayerObjectDef&, LayerObjectManager*);
	
	class LayerObjectManagerConfiguration
	{
	public:
		CacheLineAllocator* commonAllocator;
		CacheLineAllocator* drawElementAllocator; 
		LayerObjectCreatorT layerObjectCreator[setup::graphics::LAYER_OBJECT_CREATOR_NUM];
		gil::Scene2d*       scene2d;

		LayerObjectManagerConfiguration()
		{
			memset(this->layerObjectCreator, 0, setup::graphics::LAYER_OBJECT_CREATOR_NUM * sizeof(LayerObject*));
		}
	};

	class LayerObjectDefaultConfiguration : public LayerObjectManagerConfiguration
	{
	public:
		LayerObjectDefaultConfiguration() : LayerObjectManagerConfiguration()
		{

			//create allocator
			this->commonAllocator      = new CacheLineAllocator(setup::graphics::POLYGON_ALLOCATOR_SIZE, 16); // 16 * 4KB
			this->drawElementAllocator = new CacheLineAllocator(setup::graphics::DRAW_ELEMENT_ALLOCATOR_SIZE, 64); // 16 * 4KB
			//this->drawElementAllocator->log = true;
			const int16_t bucketWidth  = 10;
			const int16_t bucketHeight = 10;
			const jaFloat cellSize     = 256.0f / (jaFloat)setup::graphics::PIXELS_PER_UNIT;
			scene2d = new gil::HashGrid(bucketWidth, bucketHeight, cellSize, commonAllocator); // 64 * 12 //uses cacheManager if Init
			scene2d->setNarrowConfiguration(gil::gilDefault2dConfiguration());

			layerObjectCreator[(uint8_t)LayerObjectType::POLYGON] = Polygon::createPolygon;
			layerObjectCreator[(uint8_t)LayerObjectType::LIGHT]   = Light::createLight;
			layerObjectCreator[(uint8_t)LayerObjectType::CAMERA]  = Camera::createCamera;
		}
	};


	class LayerObjectManager
	{
	private:
		static LayerObjectManager* singleton;
		gil::Scene2d*              scene2d;

		CacheLineAllocator*    layerObjectAllocator;
		CacheLineAllocator*    drawElementAllocator;

	public:
		LayerObjectDef layerObjectDef;

	private:
		LayerObjectCreatorT layerObjectCreators[setup::graphics::LAYER_OBJECT_CREATOR_NUM];

		LayerObject* layerObjects[setup::graphics::MAX_LAYER_OBJECTS];
		uint8_t      layerObjectsFlags[setup::graphics::MAX_LAYER_OBJECTS];

		LayerObjectManager(LayerObjectManagerConfiguration& configuration);
	
	public:

		static void init(LayerObjectManagerConfiguration& configuration);
		static void cleanUp();

		static inline LayerObjectManager* getSingleton()
		{
			return singleton;
		}
		~LayerObjectManager();

		void clearMem();
		void clearLayerObjects();

		inline LayerObject* getLayerObject(uint16_t layerObjectId)
		{
			return layerObjects[layerObjectId];
		}

		inline uint8_t* getLayerObjectFlag(uint16_t layerObjectId)
		{
			return &layerObjectsFlags[layerObjectId];
		}

		inline CacheLineAllocator* getLayerObjectAllocator()
		{
			return this->layerObjectAllocator;
		}

		inline CacheLineAllocator* getDrawElementAllocator()
		{
			return this->drawElementAllocator;
		}

	private:

		inline void updateLayerObjectImmediate(LayerObject *layerObject)
		{
			scene2d->updateDynamicObject(layerObject->handle, true);
		}

		inline void deleteLayerObjectImmediate(LayerObject* layerObject)
		{
			layerObject->freeAllocatedSpace(this);
			scene2d->removeObject(layerObject->handle);

			layerObjects[layerObject->id] = nullptr;
			layerObjectsFlags[layerObject->id] = 0;
			layerObjectAllocator->free(layerObject, layerObject->getSizeOf());
		}

	public:

		inline gil::Scene2d* getScene()
		{
			return this->scene2d;
		}

		void         update(float deltaTime);
		void         getLayerObjects(StackAllocator<gil::ObjectHandle2d*>* objectList, CameraInterface* camera);
		void         getLayerObjects(StackAllocator<LayerObject*>& layerObjects) const;
		void         drawHashGrid(CameraInterface* camera);
		void         getLayerObjectsAtPoint(const jaVector2& point, StackAllocator<gil::ObjectHandle2d*>* polygonList);
		void         getLayerObjectsAtAABB(const gil::AABB2d& aabb, StackAllocator<gil::ObjectHandle2d*>* objectList);
		void         getLayerObjectsAtShape(const jaTransform2& transform, gil::Shape2d* shape, StackAllocator<gil::ObjectHandle2d*>* objectList);
		LayerObject* createLayerObjectDef(uint16_t layerObjectId = setup::graphics::ID_UNASSIGNED);

		friend class LevelManager;
		friend class LayerObjectDef;
	};

}

