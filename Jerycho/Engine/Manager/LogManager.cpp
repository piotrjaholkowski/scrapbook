#include "LogManager.hpp"
#include "LevelManager.hpp"
#include "../Tests/Test.hpp"
#include <string>
#include <iostream>


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <tchar.h>
#include <windows.h>
#include <signal.h>
#include <psapi.h>
#include <rtcapi.h>
#include <new.h>
#include <exception>
#include <sys/stat.h>
#include <Shellapi.h>
#include <dbghelp.h>

namespace ja
{

LogManager* LogManager::singleton = nullptr;

#ifndef _AddressOfReturnAddress

// Taken from: http://msdn.microsoft.com/en-us/library/s975zw7k(VS.71).aspx
#ifdef __cplusplus
#define EXTERNC extern "C"
#else
#define EXTERNC
#endif

// _ReturnAddress and _AddressOfReturnAddress should be prototyped before use 
EXTERNC void * _AddressOfReturnAddress(void);
EXTERNC void * _ReturnAddress(void);

#endif 


// Collects current process state.
static void GetExceptionPointers(DWORD dwExceptionCode, 
								 EXCEPTION_POINTERS** ppExceptionPointers)
{
	// The following code was taken from VC++ 8.0 CRT (invarg.c: line 104)
	EXCEPTION_RECORD ExceptionRecord;
	CONTEXT ContextRecord;
	memset(&ContextRecord, 0, sizeof(CONTEXT));

#ifdef _X86_

	__asm {
		mov dword ptr [ContextRecord.Eax], eax
			mov dword ptr [ContextRecord.Ecx], ecx
			mov dword ptr [ContextRecord.Edx], edx
			mov dword ptr [ContextRecord.Ebx], ebx
			mov dword ptr [ContextRecord.Esi], esi
			mov dword ptr [ContextRecord.Edi], edi
			mov word ptr [ContextRecord.SegSs], ss
			mov word ptr [ContextRecord.SegCs], cs
			mov word ptr [ContextRecord.SegDs], ds
			mov word ptr [ContextRecord.SegEs], es
			mov word ptr [ContextRecord.SegFs], fs
			mov word ptr [ContextRecord.SegGs], gs
			pushfd
			pop [ContextRecord.EFlags]
	}

	ContextRecord.ContextFlags = CONTEXT_CONTROL;
#pragma warning(push)
#pragma warning(disable:4311)
	ContextRecord.Eip = (ULONG)_ReturnAddress();
	ContextRecord.Esp = (ULONG)_AddressOfReturnAddress();
#pragma warning(pop)
	ContextRecord.Ebp = *((ULONG *)_AddressOfReturnAddress()-1);


#elif defined (_IA64_) || defined (_AMD64_)

	/* Need to fill up the Context in IA64 and AMD64. */
	RtlCaptureContext(&ContextRecord);

#else  /* defined (_IA64_) || defined (_AMD64_) */

	ZeroMemory(&ContextRecord, sizeof(ContextRecord));

#endif  /* defined (_IA64_) || defined (_AMD64_) */

	ZeroMemory(&ExceptionRecord, sizeof(EXCEPTION_RECORD));

	ExceptionRecord.ExceptionCode = dwExceptionCode;
	ExceptionRecord.ExceptionAddress = _ReturnAddress();

	EXCEPTION_RECORD* pExceptionRecord = new EXCEPTION_RECORD;
	memcpy(pExceptionRecord, &ExceptionRecord, sizeof(EXCEPTION_RECORD));
	CONTEXT* pContextRecord = new CONTEXT;
	memcpy(pContextRecord, &ContextRecord, sizeof(CONTEXT));

	*ppExceptionPointers = new EXCEPTION_POINTERS;
	(*ppExceptionPointers)->ExceptionRecord = pExceptionRecord;
	(*ppExceptionPointers)->ContextRecord = pContextRecord;  
}

// This method creates minidump of the process
static void CreateMiniDump(EXCEPTION_POINTERS* pExcPtrs)
{
	HMODULE hDbgHelp = nullptr;
	HANDLE hFile = nullptr;
	MINIDUMP_EXCEPTION_INFORMATION mei;
	MINIDUMP_CALLBACK_INFORMATION mci;

	// Load dbghelp.dll
	hDbgHelp = LoadLibrary(_T("dbghelp.dll"));
	if(hDbgHelp==nullptr)
	{
		// Error - couldn't load dbghelp.dll
		return;
	}

	// Create the minidump file
	hFile = CreateFile(
		_T("crashdump.dmp"),
		GENERIC_WRITE,
		0,
		nullptr,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		nullptr);

	if(hFile==INVALID_HANDLE_VALUE)
	{
		// Couldn't create file
		return;
	}

	// Write minidump to the file
	mei.ThreadId = GetCurrentThreadId();
	mei.ExceptionPointers = pExcPtrs;
	mei.ClientPointers = FALSE;
	mci.CallbackRoutine = nullptr;
	mci.CallbackParam = nullptr;

	typedef BOOL (WINAPI *LPMINIDUMPWRITEDUMP)(
		HANDLE hProcess, 
		DWORD ProcessId, 
		HANDLE hFile, 
		MINIDUMP_TYPE DumpType, 
		CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam, 
		CONST PMINIDUMP_USER_STREAM_INFORMATION UserEncoderParam, 
		CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam);

	LPMINIDUMPWRITEDUMP pfnMiniDumpWriteDump = 
		(LPMINIDUMPWRITEDUMP)GetProcAddress(hDbgHelp, "MiniDumpWriteDump");
	if(!pfnMiniDumpWriteDump)
	{    
		// Bad MiniDumpWriteDump function
		return;
	}

	HANDLE hProcess = GetCurrentProcess();
	DWORD dwProcessId = GetCurrentProcessId();

	BOOL bWriteDump = pfnMiniDumpWriteDump(
		hProcess,
		dwProcessId,
		hFile,
		MiniDumpWithFullMemory, //MiniDumpNormal,
		&mei,
		nullptr,
		&mci);

	if(!bWriteDump)
	{    
		// Error writing dump.
		return;
	}

	// Close file
	CloseHandle(hFile);

	// Unload dbghelp.dll
	FreeLibrary(hDbgHelp);
}

//////////////////////////////////////////////////////////
//Exception handlers

// Structured exception handler
static LONG WINAPI SehHandler(PEXCEPTION_POINTERS pExceptionPtrs)
{ 
	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1);    

	// Unreacheable code  
	return EXCEPTION_EXECUTE_HANDLER;
}

static void __cdecl PureCallHandler()
{
	// Pure virtual function call

	// Retrieve exception information
	EXCEPTION_POINTERS* pExceptionPtrs = nullptr;
	GetExceptionPointers(0, &pExceptionPtrs);

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);
	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1);    
}

static int __cdecl NewHandler(size_t)
{
	// 'new' operator memory allocation exception

	// Retrieve exception information
	EXCEPTION_POINTERS* pExceptionPtrs = nullptr;
	GetExceptionPointers(0, &pExceptionPtrs);

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();
	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1);

	// Unreacheable code
	return 0;
}

static void __cdecl InvalidParameterHandler(const wchar_t* expression, 
											const wchar_t* function, const wchar_t* file, unsigned int line, uintptr_t pReserved)
{
	// Invalid parameter exception

	// Retrieve exception information
	EXCEPTION_POINTERS* pExceptionPtrs = nullptr;
	GetExceptionPointers(0, &pExceptionPtrs);

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1);  
}

// CRT terminate() call handler
static void __cdecl TerminateHandler()
{
	// Abnormal program termination (terminate() function was called)

	// Retrieve exception information
	EXCEPTION_POINTERS* pExceptionPtrs = nullptr;
	GetExceptionPointers(0, &pExceptionPtrs);

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1);    
}

// CRT unexpected() call handler
static void __cdecl UnexpectedHandler()
{
	// Unexpected error (unexpected() function was called)

	// Retrieve exception information
	EXCEPTION_POINTERS* pExceptionPtrs = nullptr;
	GetExceptionPointers(0, &pExceptionPtrs);

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1);    	    
}

// CRT SIGABRT signal handler
void LogManager::SigabrtHandler(int)
{
	// Caught SIGABRT C++ signal

	// Retrieve exception information
	EXCEPTION_POINTERS* pExceptionPtrs = nullptr;
	GetExceptionPointers(0, &pExceptionPtrs);

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1); 
}

// CRT SIGTERM signal handler
void LogManager::SigtermHandler(int)
{
	// Termination request (SIGTERM)

	// Retrieve exception information
	EXCEPTION_POINTERS* pExceptionPtrs = nullptr;
	GetExceptionPointers(0, &pExceptionPtrs);

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1); 
}

// CRT sigint signal handler
void LogManager::SigintHandler(int)
{
	// Interruption (SIGINT)

	// Retrieve exception information
	EXCEPTION_POINTERS* pExceptionPtrs = nullptr;
	GetExceptionPointers(0, &pExceptionPtrs);

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1);
}

// CRT SIGFPE signal handler
void LogManager::SigfpeHandler(int /*code*/, int subcode)
{
	// Floating point exception (SIGFPE)

	EXCEPTION_POINTERS* pExceptionPtrs = (PEXCEPTION_POINTERS)_pxcptinfoptrs;

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1); 
}

// CRT sigill signal handler
void LogManager::SigillHandler(int)
{
	// Illegal instruction (SIGILL)

	// Retrieve exception information
	EXCEPTION_POINTERS* pExceptionPtrs = nullptr;
	GetExceptionPointers(0, &pExceptionPtrs);

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1);   
}

// CRT SIGSEGV signal handler
void LogManager::SigsegvHandler(int)
{
	// Invalid storage access (SIGSEGV)

	PEXCEPTION_POINTERS pExceptionPtrs = (PEXCEPTION_POINTERS)_pxcptinfoptrs;

	// Write minidump file
	CreateMiniDump(pExceptionPtrs);

	LogManager::getSingleton()->saveCrashReport();

	// Terminate process
	TerminateProcess(GetCurrentProcess(), 1);  
}

///////////////////////////////////////////////////////
//////////////////////////////////////////////////////

void LogManager::initLogManager()
{
	if(singleton==nullptr)
	{ 
		singleton = new LogManager();
	}
}

void LogManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

LogManager::LogManager()
{
	logDevices();
	setProcessExceptionHandlers();
	setThreadExceptionHandlers();

	this->logBuffer               = new char[this->logBufferAllocationSize];
	this->logBufferSize           = 0;
	this->logBufferCurrentPos     = this->logBuffer;
	this->printLogsToCMD          = true;
}

LogManager::~LogManager()
{
	saveLog("Run.log");
	delete[] this->logBuffer;
}

void LogManager::clearLogs()
{
	this->logBufferCurrentPos = this->logBuffer;
	this->logBufferSize       = 0;
	this->logMessages.clear();
}

void LogManager::saveLog(const char* fileName)
{
	FILE* logFile = fopen(fileName, "w");
	fwrite(this->logBuffer, this->logBufferSize, 1, logFile);
	fclose(logFile);
}

void LogManager::addLogMessage(LogType logType, const char* fieldValue, ...)
{
	va_list	args;

	if (fieldValue == nullptr)
		*logLineBuffer = 0;
	else
	{
		va_start(args, fieldValue);
#pragma warning(disable : 4996)
		vsprintf(logLineBuffer, fieldValue, args);
#pragma warning(default : 4996)
		va_end(args);
	}

	lastLog = logLineBuffer;

	if (printLogsToCMD)
		std::cout << lastLog << std::endl;

	const uint32_t logLength    = lastLog.length() + 1;
	const uint32_t wholeLogSize = logLength + this->logBufferSize + 1; // + new line

	if (wholeLogSize > this->logBufferAllocationSize)
	{
		char* tempBuffer = new char[wholeLogSize * 2];
		memcpy(tempBuffer, this->logBuffer, logBufferSize);
		delete[] this->logBuffer;

		this->logBuffer = tempBuffer;
		this->logBufferCurrentPos     = this->logBuffer + this->logBufferSize;
		this->logBufferAllocationSize = wholeLogSize * 2;
	}

	LogMessage logMessage;
	logMessage.logType          = logType;
	logMessage.logSize          = logLength - 1; //remove new line char
	logMessage.logMessageOffset = (uintptr_t)this->logBufferCurrentPos - (uintptr_t)this->logBuffer;

	memcpy(this->logBufferCurrentPos, lastLog.c_str(), logLength);

	this->logBufferCurrentPos    = (this->logBufferCurrentPos + (logLength - 1));
	this->logBufferCurrentPos[0] = '\n';
	this->logBufferCurrentPos[1] = '\0';
	++this->logBufferCurrentPos;
	this->logBufferSize = wholeLogSize - 1;

	logMessages.push(logMessage);
}

LogMessage* LogManager::getLogMessage(uint32_t logId)
{
	const uint32_t logsNum = logMessages.getSize();
	
	LogMessage* pMessage = logMessages.getFirst();

	if (logId < logsNum)
	{
		pMessage = &pMessage[logId];
		memcpy(this->logLineBuffer, this->logBuffer + pMessage->logMessageOffset, pMessage->logSize);
		this->logLineBuffer[pMessage->logSize] = '\0';
		pMessage->message = logLineBuffer;
		return pMessage;
	}
	
	return nullptr;
}

uint32_t LogManager::getLogsNum()
{
	return logMessages.getSize();
}

uint32_t LogManager::getCurrentLogFileOffsetPosition()
{
	const uint32_t logOffsetPostion = (uintptr_t)logBufferCurrentPos - (uintptr_t)logBuffer;

	return logOffsetPostion;
}

void LogManager::setProcessExceptionHandlers()
{
	// Install top-level SEH handler
	SetUnhandledExceptionFilter(&SehHandler);    

	// Catch pure virtual function calls.
	// Because there is one _purecall_handler for the whole process, 
	// calling this function immediately impacts all threads. The last 
	// caller on any thread sets the handler. 
	// http://msdn.microsoft.com/en-us/library/t296ys27.aspx
	_set_purecall_handler(PureCallHandler);    

	// Catch new operator memory allocation exceptions
	_set_new_handler(NewHandler);

	// Catch invalid parameter exceptions.
	_set_invalid_parameter_handler(InvalidParameterHandler); 

	// Set up C++ signal handlers

	_set_abort_behavior(_CALL_REPORTFAULT, _CALL_REPORTFAULT);

	// Catch an abnormal program termination
	signal(SIGABRT, SigabrtHandler);  

	// Catch illegal instruction handler
	signal(SIGINT, SigintHandler);     

	// Catch a termination request
	signal(SIGTERM, SigtermHandler);  
}

void LogManager::setThreadExceptionHandlers()
{
	// Catch terminate() calls. 
	// In a multithreaded environment, terminate functions are maintained 
	// separately for each thread. Each new thread needs to install its own 
	// terminate function. Thus, each thread is in charge of its own termination handling.
	// http://msdn.microsoft.com/en-us/library/t6fk7h29.aspx
	set_terminate(TerminateHandler);       

	// Catch unexpected() calls.
	// In a multithreaded environment, unexpected functions are maintained 
	// separately for each thread. Each new thread needs to install its own 
	// unexpected function. Thus, each thread is in charge of its own unexpected handling.
	// http://msdn.microsoft.com/en-us/library/h46t5b69.aspx  
	set_unexpected(UnexpectedHandler);    

	// Catch a floating point error
	typedef void (*sigh)(int);
	signal(SIGFPE, (sigh)SigfpeHandler);     

	// Catch an illegal instruction
	signal(SIGILL, SigillHandler);     

	// Catch illegal storage access errors
	signal(SIGSEGV, SigsegvHandler);   

}

void LogManager::saveCrashReport()
{
	saveLog("CrashLog.log");
	//LevelManager::getSingleton()->saveLevel("crashDump.xml");
	Test::cleanUp();
}

void LogManager::logDevices()
{

}

}
