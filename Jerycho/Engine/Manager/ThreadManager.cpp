#include "ThreadManager.hpp"
#include "LogManager.hpp"
#include <new>

#ifdef _WIN32
	#include <windows.h>
#endif
#include <thread>

namespace ja
{

ThreadManager* ThreadManager::singleton = nullptr;

inline void spinForAWhile(int32_t& spinCount) {
	++spinCount;

	if (spinCount < 10) {
		_mm_pause();
		return;
	}

	if (spinCount < 20) {
		for (int32_t i = 0; i != 10; i += 1)
			_mm_pause();
		return;
	}

	if (spinCount < 22) {
		SwitchToThread();
		return;
	}

	/*if (spinCount < 24) {
		Sleep(0);
		return;
	}

	if (spinCount < 26) {
		Sleep(1);
		return;
	}*/
	SwitchToThread();

	if (spinCount > 20000)
		spinCount = 22;
}

void ja::emptyCallback(WorkingThread* workingThread)
{

}

void ja::stopAllWorkersCallback(WorkingThread* workingThread) {
	ThreadManager* threadManager = ThreadManager::getSingleton();
	std::atomic<int>* atomicConter0 = threadManager->getAtomicCounter0();

	int32_t spinCounter = 0;
	while (atomicConter0->load() != 0) {
		spinForAWhile(spinCounter);
	}
}

int32_t ja::workingThreadFunction(void* data)
{
	ThreadManager* threadManager = ThreadManager::getSingleton();
	WorkingThread* workingThread = (WorkingThread*)(data);
	LogManager::getSingleton()->setThreadExceptionHandlers(); // Set exception handlers for this thread

#ifdef JA_SDL2_VERSION
	#ifdef _WIN32
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
	#else
		SDL_SetThreadPriority(SDL_THREAD_PRIORITY_HIGH);
	#endif
#else
	#ifdef _WIN32
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
		//SetThreadAffinityMask(GetCurrentThread(), (1UL << (workingThread->threadId - 1))); 
		/*if (workingThread->threadId == 0) {
			if (0 == SetThreadAffinityMask(GetCurrentThread(), 3UL)) {
				std::cout << "FAIL" << std::endl;
			}
		}
		*/

	#endif
#endif

	while(workingThread->isInit)
	{
		workingThread->initTaskParameters();	
		workingThread->taskArrayCallback(workingThread);
		threadManager->taskArrayFinished(); // It puts current thread to sleep waitining for task signal
	}

	return 0;
}

void ThreadManager::init()
{
	if(singleton==nullptr)
	{ 
		singleton = new ThreadManager();
#ifdef JA_SDL2_VERSION
	#ifdef _WIN32
		SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
	#else
		SDL_SetThreadPriority(SDL_THREAD_PRIORITY_HIGH);
	#endif
#else
#ifdef _WIN32
		/*SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
		if (0 == SetThreadAffinityMask(GetCurrentThread(), (1UL << (workingThread->threadId - 1)))) {
			std::cout << "FAIL" << std::endl;
		}*/
#endif
#endif
	}
	
}

void ThreadManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

ThreadManager::ThreadManager( )
{
	threadsNum = 1;
	std::atomic_init(&taskCounter, 0);
	std::atomic_init(&mainThreadSleepAtomicMutex, 0);
	std::atomic_init(&threadsCounter, 0);
	std::atomic_init(&atomicCounter0, 0);
	std::atomic_init(&spinWorkers, 0);

	for (int32_t i = 0; i < setup::threads::MAX_THREADS; ++i) {
		workingThreads[i].threadId = i;
		workingThreads[i].taskParameters = &taskParameters; 
	}

	sendSignalAfterFinish = false;
	warmUpMode = false;
	useAtomicBarriers = false;
	workingThreads[0].initThread();
}

ThreadManager::~ThreadManager()
{
	for(int32_t i=0; i<threadsNum; ++i)
	{
		workingThreads[i].deinitThread();
	}

	if (warmUpMode) {
		spinWorkers.store(0);
	}
	else {
		wakeUpThreads.notify_all();
	}

	for (int32_t i = 0; i < threadsNum; ++i) {
		if (workingThreads[i].workingThread.joinable())
			workingThreads[i].workingThread.join();
	}

	singleton = nullptr;
}

void ThreadManager::taskArrayFinished()
{
	if (warmUpMode) {
		threadsCounter.fetch_add(1,std::memory_order_relaxed);

		int32_t spinCounter = 0;
		while(true)
		{
			if (threadsCounter.load(std::memory_order_relaxed) == 0)
			{
				spinWorkers.fetch_add(1, std::memory_order_relaxed);
				spinCounter = 0;
				while (true) {
					
					if (spinWorkers.load(std::memory_order_relaxed) == 0) {
						//threadsCounter.store(-threadsNum); set by main thread
						return;
					}
					spinForAWhile(spinCounter);
				}
			}
			spinForAWhile(spinCounter);
		}
	}
	else {
		int32_t spinCounter = 0;
		while (mainThreadSleepMutex.try_lock() == false) {
			spinForAWhile(spinCounter);
		}

		--threadsWorking;

		if (threadsWorking == 1)
		{
			if (sendSignalAfterFinish)
				wakeUpMainThread.notify_one();
			if (useAtomicBarriers)
				mainThreadSleepAtomicMutex.store(1, std::memory_order_relaxed);
			//mainThreadSleepAtomicMutex.store(1);
		}

		wakeUpThreads.wait(mainThreadSleepMutex);
		mainThreadSleepMutex.unlock();
	}
}

void ThreadManager::waitUntilFinish()
{
	if (warmUpMode) {
		threadsCounter.store(-threadsNum + 1);
		spinWorkers.store(0);

		workingThreads[0].initTaskParameters();
		workingThreads[0].taskArrayCallback(&workingThreads[0]);

		int32_t spinCount = 0;
		while (spinWorkers.load(std::memory_order_relaxed) != (threadsNum - 1)) {
			spinForAWhile(spinCount);
		}
	}
	else {
		threadsWorking = threadsNum;
		mainThreadSleepAtomicMutex.store(1);
		wakeUpThreads.notify_all();

		workingThreads[0].initTaskParameters();
		workingThreads[0].taskArrayCallback(&workingThreads[0]);

		if (useAtomicBarriers) {
			int32_t spinCounter = 0;
			while (mainThreadSleepAtomicMutex.load(std::memory_order_relaxed) == 0) { 
				spinForAWhile(spinCounter);
			}
		}

		while (true)
		{
			int32_t spinCounter = 0;
			while (mainThreadSleepMutex.try_lock() == false) {
				spinForAWhile(spinCounter);
			}

			if (threadsWorking == 1) {
				mainThreadSleepMutex.unlock();
				break;
			}

			mainThreadSleepMutex.unlock();
		}
	}
}

void ThreadManager::createTaskArray(void* taskArrayData, uint32_t taskArraySize, void* threadDataMem, uint32_t threadDataMemSize, TaskArrayFunction taskArrayFunction, void* taskData)
{
	taskParameters.threadDataMemSizeByThread = threadDataMemSize / threadsNum;
	taskParameters.taskNum = taskArraySize;
	taskParameters.threadDataMem = threadDataMem;
	taskParameters.taskData = taskData;
	taskParameters.taskArrayData = taskArrayData;
	taskParameters.taskArrayCallback = taskArrayFunction;
}

void ThreadManager::setThreadsNum(int32_t threadNum)
{
	int32_t newThreads = threadNum - this->threadsNum;	
	assert(threadNum <= ja::setup::threads::MAX_THREADS); 

	int32_t i = 0;
	bool removeThreads = false;
	if (newThreads < 0) // set starting point to keep threads one after another
	{
		i = threadsNum + newThreads;
		removeThreads = true;
	}
	else
	{
		i = this->threadsNum;
	}
	taskParameters.taskArrayCallback = emptyCallback;

	if (warmUpMode) {
		
		threadsCounter.store(-newThreads);

		for (i; i<setup::threads::MAX_THREADS; ++i)
		{
			if (newThreads == 0)
				break;

			if (newThreads > 0) // create new thread
			{
				workingThreads[i].initThread();
				--newThreads;
			}
			else // remove one thread
			{
				workingThreads[i].deinitThread();
				++newThreads;
			}
		}

		if (removeThreads) {
			int32_t threadsToRemove = this->threadsNum - threadNum - 1;
			if (threadsToRemove == 0)
				return;

			taskParameters.taskArrayCallback = stopAllWorkersCallback;
			threadsCounter.store(-threadsToRemove);
			atomicCounter0.store(1);
			spinWorkers.store(0);

			for (i = 0; i < setup::threads::MAX_THREADS; ++i)
			{
				if ((workingThreads[i].isInit == false) && (workingThreads[i].workingThread.joinable()))
				{
					workingThreads[i].workingThread.join();
				}
			}

			atomicCounter0.store(0);

			int32_t spinCounter = 0;
			while (spinWorkers.load() != (threadNum - 1)) {
				spinForAWhile(spinCounter);
			}
		}
		else {
			int32_t spinCounter = 0;
			
			while (spinWorkers.load() != (threadNum - 1)) {
				spinForAWhile(spinCounter);
			}
		}
	}
	else
	{
		if (!removeThreads) {
			sendSignalAfterFinish = true;
			mainThreadSleepMutex.lock();
		}

		for (i; i<setup::threads::MAX_THREADS; ++i)
		{
			if (newThreads == 0)
				break;

			if (newThreads > 0) // create new thread
			{
				workingThreads[i].initThread();
				--newThreads;
			}
			else // remove one thread
			{
				workingThreads[i].deinitThread();
				++newThreads;
			}
		}

		if (removeThreads) {
			wakeUpThreads.notify_all();
			for (i = 0; i < setup::threads::MAX_THREADS; ++i)
			{
				if ((workingThreads[i].isInit == false) && (workingThreads[i].workingThread.joinable()))
				{
					workingThreads[i].workingThread.join();
				}
			}
		}
		else {
			threadsWorking = (threadNum - this->threadsNum) + 1;

			if (threadsWorking != 1) {
				wakeUpMainThread.wait(mainThreadSleepMutex);
			}
			mainThreadSleepMutex.unlock();
			sendSignalAfterFinish = false;
		}		
	}

	threadsNum = threadNum;
	if (threadsNum <= 0)
		threadsNum = 1;

	for (i = 0; i < threadsNum; ++i)
	{
		workingThreads[i].threadsNum = threadsNum;
	}
}

void ThreadManager::setWarmUpMode(bool isWarmUpMode)
{
	if (warmUpMode == isWarmUpMode)
		return;

	if (threadsNum == 1) {
		warmUpMode = isWarmUpMode;
		return;
	}
		

	taskParameters.taskArrayCallback = emptyCallback;
	
	if (warmUpMode)
	{
		warmUpMode = isWarmUpMode;
		threadsWorking = threadsNum;
		sendSignalAfterFinish = true;

		mainThreadSleepMutex.lock();
		spinWorkers.store(0);
		wakeUpMainThread.wait(mainThreadSleepMutex);
		mainThreadSleepMutex.unlock();
		sendSignalAfterFinish = false;
	}
	else
	{
		warmUpMode = isWarmUpMode;
		spinWorkers.store(0);
		threadsCounter.store(-threadsNum + 1);
		wakeUpThreads.notify_all();

		int spinCounter = 0;
		while (spinWorkers.load() != (threadsNum - 1)) {
			spinForAWhile(spinCounter);
		}
	}
}

uint32_t ThreadManager::getThreadsAllocatedMemorySize()
{
	uint32_t threadsAllocatedMemory = 0;

	for (uint8_t i = 0; i < threadsNum; ++i)
	{
		threadsAllocatedMemory += workingThreads[i].memoryAllocatedByThread;
	}

	return threadsAllocatedMemory;
}

/*const char* ThreadManager::getOpenClErrorInfo(cl_int error)
{
	switch (error) {
	case CL_SUCCESS:                            return "Success!";
	case CL_DEVICE_NOT_FOUND:                   return "Device not found.";
	case CL_DEVICE_NOT_AVAILABLE:               return "Device not available";
	case CL_COMPILER_NOT_AVAILABLE:             return "Compiler not available";
	case CL_MEM_OBJECT_ALLOCATION_FAILURE:      return "Memory object allocation failure";
	case CL_OUT_OF_RESOURCES:                   return "Out of resources";
	case CL_OUT_OF_HOST_MEMORY:                 return "Out of host memory";
	case CL_PROFILING_INFO_NOT_AVAILABLE:       return "Profiling information not available";
	case CL_MEM_COPY_OVERLAP:                   return "Memory copy overlap";
	case CL_IMAGE_FORMAT_MISMATCH:              return "Image format mismatch";
	case CL_IMAGE_FORMAT_NOT_SUPPORTED:         return "Image format not supported";
	case CL_BUILD_PROGRAM_FAILURE:              return "Program build failure";
	case CL_MAP_FAILURE:                        return "Map failure";
	case CL_INVALID_VALUE:                      return "Invalid value";
	case CL_INVALID_DEVICE_TYPE:                return "Invalid device type";
	case CL_INVALID_PLATFORM:                   return "Invalid platform";
	case CL_INVALID_DEVICE:                     return "Invalid device";
	case CL_INVALID_CONTEXT:                    return "Invalid context";
	case CL_INVALID_QUEUE_PROPERTIES:           return "Invalid queue properties";
	case CL_INVALID_COMMAND_QUEUE:              return "Invalid command queue";
	case CL_INVALID_HOST_PTR:                   return "Invalid host pointer";
	case CL_INVALID_MEM_OBJECT:                 return "Invalid memory object";
	case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:    return "Invalid image format descriptor";
	case CL_INVALID_IMAGE_SIZE:                 return "Invalid image size";
	case CL_INVALID_SAMPLER:                    return "Invalid sampler";
	case CL_INVALID_BINARY:                     return "Invalid binary";
	case CL_INVALID_BUILD_OPTIONS:              return "Invalid build options";
	case CL_INVALID_PROGRAM:                    return "Invalid program";
	case CL_INVALID_PROGRAM_EXECUTABLE:         return "Invalid program executable";
	case CL_INVALID_KERNEL_NAME:                return "Invalid kernel name";
	case CL_INVALID_KERNEL_DEFINITION:          return "Invalid kernel definition";
	case CL_INVALID_KERNEL:                     return "Invalid kernel";
	case CL_INVALID_ARG_INDEX:                  return "Invalid argument index";
	case CL_INVALID_ARG_VALUE:                  return "Invalid argument value";
	case CL_INVALID_ARG_SIZE:                   return "Invalid argument size";
	case CL_INVALID_KERNEL_ARGS:                return "Invalid kernel arguments";
	case CL_INVALID_WORK_DIMENSION:             return "Invalid work dimension";
	case CL_INVALID_WORK_GROUP_SIZE:            return "Invalid work group size";
	case CL_INVALID_WORK_ITEM_SIZE:             return "Invalid work item size";
	case CL_INVALID_GLOBAL_OFFSET:              return "Invalid global offset";
	case CL_INVALID_EVENT_WAIT_LIST:            return "Invalid event wait list";
	case CL_INVALID_EVENT:                      return "Invalid event";
	case CL_INVALID_OPERATION:                  return "Invalid operation";
	case CL_INVALID_GL_OBJECT:                  return "Invalid OpenGL object";
	case CL_INVALID_BUFFER_SIZE:                return "Invalid buffer size";
	case CL_INVALID_MIP_LEVEL:                  return "Invalid mip-map level";
	default: return "Unknown";
	}
}*/

/*void ThreadManager::displayOpenClPlatformInfo(cl_platform_id id, cl_platform_info param_name, const char* paramNameAsStr)
{
	cl_int error = 0;
	size_t paramSize = 0;
	error = clGetPlatformInfo(id, param_name, 0, nullptr, &paramSize);
	char* moreInfo = static_cast<char*>( alloca(sizeof(char)* paramSize) );
	error = clGetPlatformInfo(id, param_name, paramSize, moreInfo, nullptr);
	if (error != CL_SUCCESS) {
		std::cout << "Unable to find any OpenCL platform information" << std::endl;
		std::cout << "Error code: " << error << std::endl;
		std::cout << getOpenClErrorInfo(error) << std::endl;
			return;
	}
	std::cout << paramNameAsStr << ": " << moreInfo << std::endl;
}*/

/*void ThreadManager::displayOpenClDeviceDetails(cl_device_id id, cl_device_info param_name, const char* paramNameAsStr)
{
	cl_int error = 0;
	size_t paramSize = 0;
	error = clGetDeviceInfo(id, param_name, 0, nullptr, &paramSize);
	if (error != CL_SUCCESS) {
		std::cout << "Unable to obtain device info for param" << std::endl;
		return;
	}

	
	//The cl_device_info are preprocessor directives defined in cl.h
	switch (param_name)
	{
	case CL_DEVICE_TYPE: {

		cl_device_type* devType = static_cast<cl_device_type*>( alloca(sizeof(cl_device_type)* paramSize) );
		error = clGetDeviceInfo(id, param_name, paramSize, devType, nullptr);
		if (error != CL_SUCCESS) {
			std::cout << "Unable to obtain device info for param" << std::endl;
			return;
		}

		switch (*devType) {
		case CL_DEVICE_TYPE_CPU:
			std::cout << "CPU detected" << std::endl;
			break;
		case CL_DEVICE_TYPE_GPU:
			std::cout << "GPU detected" << std::endl;
			break;
		case CL_DEVICE_TYPE_DEFAULT:
			std::cout << "default detected" << std::endl;
			break;
		}
	}
		break;
	case CL_DEVICE_VENDOR_ID: {
		cl_uint vendorId;
		error = clGetDeviceInfo(id, param_name, paramSize, &vendorId, nullptr);
		if (error != CL_SUCCESS) {
			std::cout << "Unable to obtain " << paramNameAsStr << std::endl;
			return;
		}
		std::cout << paramNameAsStr << ": " << vendorId << std::endl;
	}
		break;
	case CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS: {
		cl_uint maxWorkItemsDimmension;
		error = clGetDeviceInfo(id, param_name, paramSize, &maxWorkItemsDimmension, nullptr);
		if (error != CL_SUCCESS) {
			std::cout << "Unable to obtain " << paramNameAsStr << std::endl;
			return;
		}
		std::cout << paramNameAsStr << ": " << maxWorkItemsDimmension << std::endl;
	}
		break;
	case CL_DEVICE_MAX_WORK_GROUP_SIZE: {
		size_t maxWorkGroupSize;
		error = clGetDeviceInfo(id, param_name, paramSize, &maxWorkGroupSize, nullptr);
		if (error != CL_SUCCESS) {
			std::cout << "Unable to obtain " << paramNameAsStr << std::endl;
			return;
		}
		std::cout << paramNameAsStr << ": " << maxWorkGroupSize << std::endl;
	}
		break;
	case CL_DEVICE_MAX_COMPUTE_UNITS: {
		cl_uint maxComputeUnits;
		error = clGetDeviceInfo(id, param_name, paramSize, &maxComputeUnits, nullptr);
		if (error != CL_SUCCESS) {
			std::cout << "Unable to obtain " << paramNameAsStr << std::endl;
			return;
		}
		std::cout << paramNameAsStr << ": " << maxComputeUnits << std::endl;
	}
		break;
	case CL_DEVICE_MAX_WORK_ITEM_SIZES: {
		std::cout << "Unable to obtain " << paramNameAsStr << std::endl;
	}
		break;
	case CL_DEVICE_GLOBAL_MEM_SIZE: { 
		cl_ulong memSize;
		error = clGetDeviceInfo(id, param_name, paramSize, &memSize, nullptr);
		if (error != CL_SUCCESS) {
			std::cout << "Unable to obtain " << paramNameAsStr << std::endl;
			return;
		}
		std::cout << paramNameAsStr << ": " << memSize << std::endl;
	}
	case CL_DEVICE_LOCAL_MEM_SIZE: {
		cl_ulong memSize;
		error = clGetDeviceInfo(id, param_name, paramSize, &memSize, nullptr);
		if (error != CL_SUCCESS) {
			std::cout << "Unable to obtain " << paramNameAsStr << std::endl;
			return;
		}
		std::cout << paramNameAsStr << ": " << memSize << std::endl;
	}
		break;
	case CL_DEVICE_MAX_CLOCK_FREQUENCY: {
		cl_uint clockFrequency;
		error = clGetDeviceInfo(id, param_name, paramSize, &clockFrequency, nullptr);
		if (error != CL_SUCCESS) {
			std::cout << "Unable to obtain " << paramNameAsStr << std::endl;
			return;
		}
		std::cout << paramNameAsStr << ": " << clockFrequency << std::endl;
	}
		break;

	} //end of switch
}*/

/*void ThreadManager::displayOpenClDeviceInfo(cl_platform_id id, cl_device_type dev_type)
{
	//OpenCL 1.1 device types 
	cl_int error = 0;
	cl_uint numOfDevices = 0;
	//Determine how many devices are connected to your
	//platform 

	error = clGetDeviceIDs(id, dev_type, 0, nullptr, &numOfDevices);
	if (error != CL_SUCCESS) {
		std::cout << "Unable to obtain any OpenCL compliant device info" << std::endl;
		return;
	}

	cl_device_id* devices = static_cast<cl_device_id*>( alloca(sizeof(cl_device_id)* numOfDevices) );

	//Load the information about your devices into the
	//variable 'devices'
	error = clGetDeviceIDs(id, dev_type, numOfDevices, devices, nullptr);
	if (error != CL_SUCCESS)
	{
		std::cout << "Unable to obtain any OpenCL compliant device info" << std::endl;
		return;
	}

	std::cout << "Number of detected OpenCL devices: " << numOfDevices << std::endl;
	//We attempt to retrieve some information about the
	//devices.

	for (unsigned int i = 0; i < numOfDevices; ++i)
	{
		displayOpenClDeviceDetails(devices[i], CL_DEVICE_TYPE, "CL_DEVICE_TYPE" );
		displayOpenClDeviceDetails(devices[i], CL_DEVICE_VENDOR_ID, "CL_DEVICE_VENDOR_ID" );
		displayOpenClDeviceDetails(devices[i], CL_DEVICE_MAX_COMPUTE_UNITS, "CL_DEVICE_MAX_COMPUTE_UNITS");
		displayOpenClDeviceDetails(devices[i], CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS, "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS");
		displayOpenClDeviceDetails(devices[i], CL_DEVICE_MAX_WORK_ITEM_SIZES, "CL_DEVICE_MAX_WORK_ITEM_SIZES");
		displayOpenClDeviceDetails(devices[i], CL_DEVICE_MAX_WORK_GROUP_SIZE, "CL_DEVICE_MAX_WORK_GROUP_SIZE");
		displayOpenClDeviceDetails(devices[i], CL_DEVICE_GLOBAL_MEM_SIZE, "CL_DEVICE_GLOBAL_MEM_SIZE");
		displayOpenClDeviceDetails(devices[i], CL_DEVICE_LOCAL_MEM_SIZE, "CL_DEVICE_LOCAL_MEM_SIZE");
		displayOpenClDeviceDetails(devices[i], CL_DEVICE_MAX_CLOCK_FREQUENCY, "CL_DEVICE_MAX_CLOCK_FREQUENCY");
	}
}*/

/*void ThreadManager::displayOpenClInfo()
{
	//OpenCL 1.2 data structures
	cl_platform_id* platforms = nullptr;
	//OpenCL 1.1 scalar data types
	cl_uint numOfPlatforms;
	cl_int error;

	//Get the number of platforms
	//Remember that for each vendor's SDK installed on the
	//Computer, the number of available platform also
	//increased.
	error = clGetPlatformIDs(0, nullptr, &numOfPlatforms);
	if (error < 0) {
		std::cout << "Unable to find any OpenCL platforms" << std::endl;
		return;
	}

	// Allocate memory for the number of installed platforms.
	// alloca(...) occupies some stack space but is
	// automatically freed on return
	platforms = static_cast<cl_platform_id*>( alloca(sizeof(cl_platform_id) * numOfPlatforms) );
	std::cout << "Number of OpenCL platforms found: " << numOfPlatforms << std::endl;

	error = clGetPlatformIDs(numOfPlatforms, platforms, nullptr);
	if (error < 0) {
		std::cout << "Unable to get OpenCL platform IDs" << std::endl;
		return;
	}

	// We invoke the API 'clPlatformInfo' twice for each
	// parameter we're trying to extract
	// and we use the return value to create temporary data
	// structures (on the stack) to store
	// the returned information on the second invocation.
	for (cl_uint i = 0; i < numOfPlatforms; ++i) {
		displayOpenClPlatformInfo(platforms[i], CL_PLATFORM_PROFILE, "CL_PLATFORM_PROFILE");
		displayOpenClPlatformInfo(platforms[i], CL_PLATFORM_VERSION, "CL_PLATFORM_VERSION");
		displayOpenClPlatformInfo(platforms[i], CL_PLATFORM_NAME, "CL_PLATFORM_NAME");
		displayOpenClPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, "CL_PLATFORM_VENDOR");
		displayOpenClPlatformInfo(platforms[i], CL_PLATFORM_EXTENSIONS, "CL_PLATFORM_EXTENSIONS");
		displayOpenClDeviceInfo(platforms[i], CL_DEVICE_TYPE_DEFAULT);
	}

}*/

void ThreadManager::lockGate0()
{
	gate0.lock();
}

void ThreadManager::unlockGate0() {
	gate0.unlock();
}

}

