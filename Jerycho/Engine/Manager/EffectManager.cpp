#include "EffectManager.hpp"
#include "DisplayManager.hpp"
#include "LayerObjectManager.hpp"
#include "GameManager.hpp"
#include "InputManager.hpp"
#include "ScriptManager.hpp"
#include "LogManager.hpp"

#include "../Tests/TimeStamp.hpp"
#include "../Graphics/Material.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 

namespace ja
{ 

EffectManager* EffectManager::singleton            = nullptr;

EffectManager::EffectManager() : renderBuffer(setup::graphics::MAX_OBJECTS_PER_RENDER_COMMAND)
{
	{
		//Creating framebuffer
		int32_t resolutionWidth, resolutionHeight;
		float   halfWidth, halfHeight;
		DisplayManager::getResolution(&resolutionWidth, &resolutionHeight);
		int32_t multisampleSamplesNum = DisplayManager::getMultisampleSamples();
		halfWidth  = (float)(resolutionWidth)* 0.5f;
		halfHeight = (float)(resolutionHeight)* 0.5f;

		this->fboMultisampled = new Fbo();
		this->fbo             = new Fbo();
		this->pingPongFbo     = new Fbo[2];

		{
			glGenTextures(1, &this->colorMultisampledTexture);

			glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, this->colorMultisampledTexture);
			glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, multisampleSamplesNum, GL_RGBA, resolutionWidth, resolutionHeight, GL_TRUE);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		}

		{
			glGenTextures(1, &this->colorTexture);

			glBindTexture(GL_TEXTURE_2D, colorTexture);
			//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, resolutionWidth, resolutionHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, resolutionWidth, resolutionHeight, 0, GL_RGBA, GL_FLOAT, nullptr);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		}

		{
			glGenTextures(1, &this->glowMultisampledTexture);

			glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, this->glowMultisampledTexture);
			glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, multisampleSamplesNum, GL_RGBA, resolutionWidth, resolutionHeight, GL_TRUE);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		}

		{
			glGenTextures(1, &this->glowTexture);

			glBindTexture(GL_TEXTURE_2D, this->glowTexture);
			//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, resolutionWidth, resolutionHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, resolutionWidth, resolutionHeight, 0, GL_RGBA, GL_FLOAT, nullptr);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		}

		{
			for (int32_t i = 0; i < 2; ++i)
			{
				glGenTextures(1, &this->pingPongTexture[i]);

				glBindTexture(GL_TEXTURE_2D, this->pingPongTexture[i]);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, resolutionWidth, resolutionHeight, 0, GL_RGBA, GL_FLOAT, NULL);
				//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, resolutionWidth / 2, resolutionHeight / 2, 0, GL_RGBA, GL_FLOAT, NULL);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			}			
		}

		{
			glBindTexture(GL_TEXTURE_2D, 0);
			glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);

			fboMultisampled->bind();
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, this->colorMultisampledTexture, 0);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D_MULTISAMPLE, this->glowMultisampledTexture, 0);
			fboMultisampled->unbind();

			GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER_EXT);
			if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
			{
				LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Could not attach textures multisampled frame buffer error: %d", status);
			}

			fbo->bind();
			glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->colorTexture, 0);
			glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, this->glowTexture, 0);
			fbo->unbind();

			status = glCheckFramebufferStatus(GL_FRAMEBUFFER_EXT);
			if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
			{
				LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Could not attach textures to frame buffer error: %d", status);
			}

			pingPongFbo[0].bind();
			glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->pingPongTexture[0], 0);
			pingPongFbo[0].unbind();

			status = glCheckFramebufferStatus(GL_FRAMEBUFFER_EXT);
			if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
			{
				LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Could not attach textures to ping pong frame buffer error: %d", status);
			}

			pingPongFbo[1].bind();
			glFramebufferTexture2D(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->pingPongTexture[1], 0);
			pingPongFbo[1].unbind();

			status = glCheckFramebufferStatus(GL_FRAMEBUFFER_EXT);
			if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
			{
				LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Could not attach textures to ping pong frame buffer error: %d", status);
			}
		}
		
	}
	
	{
		// The fullscreen quad's FBO
		const GLfloat quadVertexBufferData[] = {
			-1.0f, -1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
			-1.0f, 1.0f, 0.0f,
			-1.0f, 1.0f, 0.0f,
			1.0f, -1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
		};

		/*const GLfloat quadVertexBufferData[] = {
			-0.5f, -0.5f, 0.0f,
			0.5f, -0.5f, 0.0f,
			-0.5f, 0.5f, 0.0f,
			-0.5f, 0.5f, 0.0f,
			0.5f, -0.5f, 0.0f,
			0.5f, 0.5f, 0.0f,
		};*/

		glGenBuffers(1, &this->quadVertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, this->quadVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertexBufferData), quadVertexBufferData, GL_STATIC_DRAW);

		glGenVertexArrays(1, &this->quadVertexArray);
	}
}

EffectManager::~EffectManager()
{
	delete   this->fbo;
	delete   this->fboMultisampled;
	delete[] this->pingPongFbo;

	glDeleteBuffers(1, &this->quadVertexBuffer);
	glDeleteVertexArrays(1, &this->quadVertexArray);

	glDeleteTextures(1, &this->colorTexture);
	glDeleteTextures(1, &this->glowTexture);

	glDeleteTextures(1, &this->colorMultisampledTexture);
	glDeleteTextures(1, &this->glowMultisampledTexture);

	for (int32_t i = 0; i < 2; ++i)
		glDeleteTextures(1, &this->pingPongTexture[i]);
}

void EffectManager::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void EffectManager::init()
{
	if(singleton==nullptr)
	{ 
		singleton = new EffectManager();
	}

}

void EffectManager::calculateProjectedShadows(LayerRenderer* layerRenderer)
{
	projectedShadow.generateShadows(layerRenderer);
}

void EffectManager::renderEffects(LayerRenderer* layerRenderer)
{
	PROFILER_FUN;

	ResourceManager* pResourceManager = ResourceManager::getSingleton();

	CameraInterface* activeCamera = GameManager::getSingleton()->getActiveCamera();
	
	renderBuffer.clear();

	this->defaultShaderBinder = pResourceManager->getShaderBinder("DEFAULT")->resource;
	//this->fboShaderBinder     = ResourceManager::getSingleton()->getShaderBinder("FBO")->resource;
	this->defaultMaterial = pResourceManager->getMaterial("DEFAULT")->resource;	

	ShaderBinder* fboShaderBinder    = pResourceManager->getShaderBinder("FBO_DEBUG")->resource;
	ShaderBinder* gaussianBlurBinder = pResourceManager->getShaderBinder("GAUSSIAN_BLUR")->resource;

	uint8_t   parallaxesNum = parallaxes.getSize();
	Parallax* pParallax     = parallaxes.getFirst();

	const uint32_t layersNum = layers.getSize();
	const Layer*   pLayers   = layers.getFirst();

	LayerRenderer* pCurrentLayerRenderer = nullptr;

	CameraInterface parallaxCameraInterface;

	for (uint8_t parallaxIt = 0; parallaxIt < parallaxesNum; ++parallaxIt)
	{
		if (!pParallax[parallaxIt].isActive)
			continue;

		if (pParallax[parallaxIt].isActiveCamera)
		{
			DisplayManager::setCameraMatrix(activeCamera);
			pCurrentLayerRenderer = layerRenderer;
		}
		else
		{
			if (nullptr == pParallax[parallaxIt].camera)
				continue;

			Camera* parallaxCamera = pParallax[parallaxIt].camera;

			parallaxCameraInterface.position = parallaxCamera->getPosition();
			parallaxCameraInterface.scale    = parallaxCamera->getScale();
			parallaxCameraInterface.angleRad = parallaxCamera->getAngleRad();

			StackAllocator<gil::ObjectHandle2d*>* tempObjectHandleList = GameManager::getSingleton()->getTempObjectList();

			LayerObjectManager::getSingleton()->getLayerObjects(tempObjectHandleList, &parallaxCameraInterface);
			parallaxRenderer.refresh(tempObjectHandleList);
			tempObjectHandleList->clear();

			DisplayManager::setCameraMatrix(&parallaxCameraInterface);

			pCurrentLayerRenderer = &parallaxRenderer;
		}

		//calculateProjectedShadows(pCurrentLayerRenderer);

		for (uint32_t i = 0; i < layersNum; ++i)
		{
			if (true == pLayers[i].isActive)
				//pCurrentLayerRenderer->renderLayer(i);
				pCurrentLayerRenderer->renderLayer(i, renderBuffer);
		}

	}

	renderBuffer.applyViewMatricesToMvpMatrices();

	{
		//Rendering buffer part
		glPushAttrib(GL_ENABLE_BIT);

		
		for (uint32_t i = 0; i < 2; ++i)
		{
			glActiveTexture(GL_TEXTURE0 + i);
			glBindTexture(GL_TEXTURE_2D, 0);
			glEnable(GL_TEXTURE_2D);
		}
		
		glActiveTexture(GL_TEXTURE0);

		glDisable(GL_DEPTH_TEST);
		//glDepthFunc(GL_ALWAYS);
		glDisable(GL_STENCIL_TEST);
		glEnable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glShadeModel(GL_SMOOTH);

		glEnableClientState(GL_VERTEX_ARRAY); //removed from opengl 3.3
		glEnableClientState(GL_COLOR_ARRAY); //removed from opengl 3.3
		glDisableClientState(GL_TEXTURE_COORD_ARRAY); //removed from opengl 3.3

		{
			PROFILER_REG(RenderToMultisampleFBO);
			//Render to fbo
			//this->fbo->bind();
			this->fboMultisampled->bind();
			
			glClear(GL_COLOR_BUFFER_BIT);

			GLuint attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
			glDrawBuffers(2, attachments);

			renderBuffer.sendDataToGPU();

			{
				const UniformBlockInfo* pShaderSetting   = this->defaultShaderBinder->getSettingUniformBlock();
				const UniformField*     pResolutionField = pShaderSetting->getUniformField("resolution");
			
				float   resolution[] = { 1280.0f, 720.0f };
				pShaderSetting->setFloat(pResolutionField, resolution, 2);

				pShaderSetting->sendBufferBlockData();
			}

			renderBuffer.render();
			//this->fbo->unbind();
			this->fboMultisampled->unbind();
		}

		{
			PROFILER_REG(Multisampling);
			//Copy multisampled render buffer to render buffer
			int32_t resWidth  = DisplayManager::getSingleton()->getResolutionWidth();
			int32_t resHeight = DisplayManager::getSingleton()->getResolutionHeight();
			
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this->fbo->getFboHandle());
			glBindFramebuffer(GL_READ_FRAMEBUFFER, this->fboMultisampled->getFboHandle());
		
			glReadBuffer(GL_COLOR_ATTACHMENT0);
			glDrawBuffer(GL_COLOR_ATTACHMENT0);
			glBlitFramebuffer(0,0,resWidth,resHeight,0,0,resWidth,resHeight,GL_COLOR_BUFFER_BIT,GL_NEAREST);
		
			glReadBuffer(GL_COLOR_ATTACHMENT1);
			glDrawBuffer(GL_COLOR_ATTACHMENT1);
			glBlitFramebuffer(0, 0, resWidth, resHeight, 0, 0, resWidth, resHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
		
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
			glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
		}
		
		
		glDisable(GL_ALPHA_TEST);
		glDisable(GL_BLEND);
		
		{
			PROFILER_REG(GaussianBlur);
			gaussianBlurBinder->bind();
			
			int32_t horizontal = true;
		
			bool    firstIteration = true;
			int32_t amount         = 10;
		
			int32_t horizontalUniformLocation;
			{
				PROFILER_REG(GetUniformLocation);
				horizontalUniformLocation = glGetUniformLocation(gaussianBlurBinder->getShaderProgramId(), "horizontal");
			}
		
			for (int32_t i = 0; i < amount; i++)
			{
				pingPongFbo[horizontal].bind();
				glUniform1i(horizontalUniformLocation, horizontal);
				glBindTexture(GL_TEXTURE_2D, firstIteration ? this->colorTexture : pingPongTexture[!horizontal]);
				
				renderQuad();
				
				horizontal = !horizontal;
				
				if (firstIteration)
					firstIteration = false;
			}
		
			glBindFramebuffer(GL_FRAMEBUFFER, 0);	
		}
		
		{
			PROFILER_REG(RenderFbo);
			//Render fbo	
			fboShaderBinder->bind();
		
			{
				//FBO debugging when FBO_DEBUG is bind
				static int32_t debugTextureId = 0;
				if (InputManager::getSingleton()->isKeyPressed(Key::JA_k))
				{
					debugTextureId = (debugTextureId + 1) % 4;
				}
		
				GLint colorTextureId = glGetUniformLocation(fboShaderBinder->getShaderProgramId(), "debugTexture");
		
				switch (debugTextureId)
				{
				case 0:
					glBindTexture(GL_TEXTURE_2D, colorTexture);
					break;
				case 1:
					glBindTexture(GL_TEXTURE_2D, glowTexture);
					break;
				case 2:
					glBindTexture(GL_TEXTURE_2D, pingPongTexture[0]);
					break;
				case 3:
					glBindTexture(GL_TEXTURE_2D, pingPongTexture[1]);
					break;
				}
		
				glUniform1i(colorTextureId, 0);
			}	
		
			renderQuad();
		}

		defaultShaderBinder->unbind();

		glActiveTexture(GL_TEXTURE0);

		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		//glDisableClientState(GL_INDEX_ARRAY);
		//glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glPopAttrib();
	}
}

void EffectManager::renderQuad()
{
	glBindVertexArray(this->quadVertexArray);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, this->quadVertexBuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(0);
	glBindVertexArray(0);
}

void EffectManager::clearEffects()
{
	layers.clear();
	parallaxes.clear();
}

uint8_t EffectManager::getLayersNum()
{
	return (uint8_t)layers.getSize();
}

Layer* EffectManager::getLayer(uint8_t layerId)
{
	Layer* pLayers = layers.getFirst();
	return &pLayers[layerId];
}

uint8_t EffectManager::getParallaxesNum()
{
	return (uint8_t)parallaxes.getSize();
}

Parallax* EffectManager::getParallax(uint8_t parallaxId)
{
	Parallax* pParallax = parallaxes.getFirst();
	return &pParallax[parallaxId];
}

uint8_t EffectManager::createLayer(const char* name)
{
	uint32_t layerId = layers.getSize();

	Layer layer(name);
	layers.push(layer);

	return layerId;
}

uint8_t EffectManager::createParallax(const char* name, Camera* camera, bool isActiveCamera)
{
	uint32_t parallaxId = parallaxes.getSize();

	Parallax parallax(name, camera, isActiveCamera);
	parallaxes.push(parallax);

	return parallaxId;
}

void EffectManager::deleteLayer(Layer* layer)
{
	layers.erase(layer);
}

void EffectManager::deleteParallax(Parallax* parallax)
{
	parallaxes.erase(parallax);
}

uint8_t EffectManager::getLayerId(Layer* layer)
{
	uint8_t layerId = ((uintptr_t)layer - (uintptr_t)layers.getFirst()) / sizeof(Layer);

	return layerId;
}

uint8_t EffectManager::getParallaxId(Parallax* parallax)
{
	uint8_t parallaxId = ((uintptr_t)parallax - (uintptr_t)parallaxes.getFirst()) / sizeof(Parallax);

	return parallaxId;
}

ShaderBinder* EffectManager::getDefaultShaderBinder()
{
	return this->defaultShaderBinder;
}

Material* EffectManager::getDefaultMaterial()
{
	return this->defaultMaterial;
}

}

