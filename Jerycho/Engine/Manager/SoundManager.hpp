#pragma once

#include "../jaSetup.hpp"

#include "../Utility/String.hpp"
#include "../Math/VectorMath2d.hpp"

#include "../Sounds/SoundEffect.hpp"
#include "../Sounds/SoundStream.hpp"
#include "../Sounds/SoundEmitter.hpp"
#include "../Sounds/SoundEmitterDef.hpp"

#include "../Allocators/CacheLineAllocator.hpp"

#include <AL/al.h>
#include <AL/alc.h>

#include <vector>
#include <stdio.h>

int32_t readSpectrumThreadFunction(void* data);

namespace ja
{

	struct FourierTransform
	{
	public:
		float    data[setup::sound::CAPTURE_BUFFER_SIZE * 2]; // real and complex 2 because of imaginary number first real, second complex
		float    spectrum[setup::sound::CAPTURE_BUFFER_SIZE / 2]; // calculated spectrum from fft
		float    normalized_spectrum[setup::sound::CAPTURE_BUFFER_SIZE / 2];
		float    frequency;
		uint32_t biggestIndex;
	};

	class SoundManager
	{
	private:
		static SoundManager* singleton;
		ALCdevice *device;
		ALCdevice* captureDevice;
		ALCcontext *ctx;

		uint8_t capturedSamples[setup::sound::CAPTURE_BUFFER_SIZE];

		float   invDiv; // used for normalization 
		int32_t samplesAvailable;
		int32_t sampleFormat; //mono8, mono16
		int32_t currentSample;

		CacheLineAllocator* soundAllocator;
		std::vector<SoundEmitter> soundPlayers; // it is not cleared after level loaded

		SoundEmitterDef* soundGroups;
		uint8_t soundGroupsNum;

		SoundManager();
		void closeCaptureDevice();
		void updateMusicStreams();
		void logDevicesInfo();
		void logExtensionsInfo();
		void updateCaptureBuffer();
		void calculateFft();
	public:
		uint32_t activeRecordingDevice;
		ja::string soundGroupInfo;
		std::vector<ja::string> deviceNames;
		FourierTransform fftData;

		~SoundManager();
		static void init();
		static inline SoundManager* SoundManager::getSingleton()
		{
			return singleton;
		}
		static void cleanUp();

		void openCaptureDevice(uint32_t deviceId);
		void startCapturing();
		void stopCapturing();
		void setListenerPosition(jaFloat x, jaFloat y, jaFloat z);
		void setListenerOrientation(jaFloat facingX, jaFloat facingY, jaFloat facingZ, jaFloat upX, jaFloat upY, jaFloat upZ);
		void setListenerVelocity(jaFloat x, jaFloat y, jaFloat z);
		//void playMusic(unsigned char musicChannel, jaMusicStream* musicStream);
		void update(jaFloat deltaTime);

		void setSoundGroup(SoundEmitter* soundEmitter, uint8_t soundGroup);
		ja::string& getSoundGroupInfo();
		SoundEmitter* createSoundPlayer(); // It is for gui button purposes not game objects
	};

}
