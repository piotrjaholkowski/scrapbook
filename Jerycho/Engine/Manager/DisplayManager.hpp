#pragma once

#include "../jaSetup.hpp"
#include "../Interface/CameraInterface.hpp"
#include "../Graphics/Texture2d.hpp"
#include "../Math/VectorMath2d.hpp"

#ifdef JA_SDL_OPENGL
	#ifdef JA_SDL2_VERSION
		#include<SDL2/SDL.h>
		#include<SDL2/SDL_opengl.h>
		#include <GL/GLU.h>
	#else
		#include<SDL/SDL.h>
		#include<SDL/SDL_opengl.h>
	#endif

#include "../Graphics/GLExtensions.hpp"

extern glGetStringiFunc glGetStringi;

extern glGenerateMipmapFunc glGenerateMipmap;
extern glActiveTextureFunc  glActiveTexture;
extern glBlendEquationFunc  glBlendEquation;

extern glCreateShaderFunc  glCreateShader;
extern glShaderSourceFunc  glShaderSource;
extern glCompileShaderFunc glCompileShader;
extern glUseProgramFunc    glUseProgram;
extern glLinkProgramFunc   glLinkProgram;
extern glAttachShaderFunc  glAttachShader;
extern glCreateProgramFunc glCreateProgram;
extern glDeleteShaderFunc  glDeleteShader;
extern glDeleteProgramFunc glDeleteProgram;
extern glDetachShaderFunc  glDetachShader;

extern glGetUniformLocationFunc        glGetUniformLocation;
extern glGetActiveUniformFunc          glGetActiveUniform;
extern glGetActiveUniformBlockNameFunc glGetActiveUniformBlockName;
extern glGetActiveUniformsivFunc       glGetActiveUniformsiv;
extern glGetUniformBlockIndexFunc      glGetUniformBlockIndex;
extern glGetActiveUniformBlockivFunc   glGetActiveUniformBlockiv;
extern glBindBufferBaseFunc            glBindBufferBase;
extern glUniformBlockBindingFunc       glUniformBlockBinding;
extern glUniform1iFunc                 glUniform1i;
extern glUniform3fFunc                 glUniform3f;
extern glUniform1fvFunc                glUniform1fv;
extern glUniform1ivFunc                glUniform1iv;
extern glUniform3fvFunc                glUniform3fv;
extern glUniform4fvFunc                glUniform4fv;
extern glUniformMatrix4fvFunc          glUniformMatrix4fv;

extern glGenVertexArraysFunc    glGenVertexArrays;
extern glDeleteVertexArraysFunc glDeleteVertexArrays;
extern glBindVertexArrayFunc    glBindVertexArray;
extern glEnableVertexAttribArrayFunc  glEnableVertexAttribArray;
extern glVertexAttribPointerFunc      glVertexAttribPointer;
extern glVertexAttribIPointerFunc     glVertexAttribIPointer;
extern glDisableVertexAttribArrayFunc glDisableVertexAttribArray;

// opengl 2.0 standard
extern glGetShaderivFunc       glGetShaderiv;
extern glGetProgramivFunc      glGetProgramiv;
extern glGetShaderInfoLogFunc  glGetShaderInfoLog;
extern glGetProgramInfoLogFunc glGetProgramInfoLog;
// only with ARB
extern glGetObjectParameterivARBFunc glGetObjectParameterivARB;
extern glGetInfoLogARBFunc           glGetInfoLogARB;
//

extern glTexImage2DMultisampleFunc  glTexImage2DMultisample;
extern glGenFramebuffersFunc        glGenFramebuffers;
extern glBindFramebufferFunc        glBindFramebuffer;
extern glFramebufferTexture2DFunc   glFramebufferTexture2D;
extern glDrawBuffersFunc            glDrawBuffers;
extern glBlitFramebufferFunc        glBlitFramebuffer;
extern glCheckFramebufferStatusFunc glCheckFramebufferStatus;
extern glDeleteFramebuffersFunc     glDeleteFramebuffers;

extern glGenBuffersFunc    glGenBuffers;
extern glDeleteBuffersFunc glDeleteBuffers;
extern glBindBufferFunc    glBindBuffer;
extern glBufferDataFunc    glBufferData;
extern glBufferSubDataFunc glBufferSubData;



#endif

#pragma warning( disable : 4244 )

namespace ja
{

	enum ScreenResolution
	{
		res800x600  = 0,
		res1024x768 = 1,
		res1280x720 = 2,
		res1366x768 = 3
	};

	class Color4f;
	class RenderBuffer;
	class Font;
	class FontPolygon;
	class Material;

	class DisplayManager
	{
	private:
#ifdef JA_SDL_OPENGL
		SDL_Surface* screen;
#ifdef JA_SDL2_VERSION
		SDL_Window*   openglWindow;
		SDL_GLContext openglContext;
#endif
		float openGLVersion;
#endif

		static DisplayManager* singleton;

	public:
		static jaMatrix44 projectionMatrix;
		static jaMatrix44 viewMatrix;
		static jaMatrix44 projectionViewMatrix;
	private:

		static uint32_t renderingThreadId;
		static double xUnits, yUnits;
		static float widthMultiplier, heightMultiplier;
		static double aspectRatio;
		bool   isFullscreen;
		static bool initializedDisplay;
		bool initializedResolution;
		uint8_t clearR, clearG, clearB;
		static int32_t colorR, colorG, colorB;
		int32_t size;
		static int32_t resolutionWidth, resolutionHeight;
		bool isWideScreen;
		int32_t multisampleSamples;

		DisplayManager();
	public:
		~DisplayManager();

		static inline DisplayManager* getSingleton()
		{
			return singleton;
		}
		static void initDisplayManager();
		static void initExtensions();
		static inline uint32_t getRenderingThreadId();

		static void unbindAllTextures();
		static inline int32_t getMultisampleSamples();
		static inline int32_t getResolutionHeight();
		static inline int32_t getResolutionWidth();
		static inline int32_t getResolutionWidth(float widthPercent);
		static inline int32_t getResolutionHeight(float heightPercent);
		static inline float getWidthMultiplier();
		static inline float getHeightMultiplier();
		void getWorldResolution(double* widthUnits, double* heightUnits);
		void getWorldResolution(float* widthUnits, float* heightUnits);
		static void getResolution(int32_t* width, int32_t* height);
		void setBackgroundColor(uint8_t r, uint8_t g, uint8_t b);
		static inline void clearDisplay();
		static inline void clearColorDisplay();
		static inline void clearStencilDisplay();
		static inline void clearDepthDisplay();
		void setResolution(ScreenResolution resolution, int32_t red, int32_t green, int32_t blue, int32_t alpha, bool isFullscreen, bool isVSync, int32_t multisampleSamples = 0);
		void setWindowTitle(const char* windowName);
		void setWindowPosition(int32_t posX, int32_t posY);
		void setMousePositionXY(int32_t x, int32_t y);
		static void cleanUp();
		static inline float getOpenglVersion();
		static void setSmoothGraphics();
		static inline void beginDrawingSprites();
		static inline void endDrawingSprites();
		static inline void drawSprite(float x, float y, float halfWidth, float halfHeight, bool filled);
		static inline void drawSprite(Texture2d* texture, float x, float y, float rotation);
		static inline void drawSprite(Texture2d* texture, float x, float y, float halfWidth, float halfHeight, float rotation);
		static inline void clearWorldMatrix();
		static inline void pushModelMatrix();
		static inline void popModelMatrix();
		static inline void pushProjectionMatrix();
		static inline void popProjectionMatrix();
		static inline void setModelMatrixMode();
		static inline void setProjectionMatrixMode();
		static inline void setCameraMatrix(const CameraInterface* camera);
		static inline void pushWorldMatrix();
		static inline void popWorldMatrix();
		inline void swapBuffers();
		inline void enableDepthBuffer();
		inline void disableDepthBuffer();
		inline void enableStencilTest();
		inline void disableStencilTest();
		inline void enableLighting();
		inline void disableLighting();
		inline void enableTexturing();
		inline void disableTexturing();
		static inline void enableAlphaBlending();
		static inline void setAlphaBlendingFunc();
		static inline void disableAlphaBlending();
		static inline void getWindowPosition(double xWorld, double yWorld, double zWorld, int32_t* x, int32_t* y);
		static inline void getWorldPosition(int32_t x, int32_t y, double* xWorld, double* yWorld, double* zWorld);
		static inline void setScreenCoordinate();
		static inline void pushScreenCoordinate();
		static inline void popWorldCoordinate();

		static inline void setOrthoView(double left, double right, double bottom, double top, double nearVal, double farVal);
		static inline void setOrtho2DView(double left, double right, double bottom, double top);
		static inline void setPerspectiveView(double fovy, double aspect, double zNear, double zFar);

		//void RenderTextChar(Font* font, float x, float y, char* text);
		static void RenderText(Font* font, float x, float y, const char* text, uint32_t marginStyle, uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		static void RenderTextWorld(Font* font, float x, float y, float scale, const char* text, uint32_t marginStyle, uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void RenderText(Font* font, float x, float y, const wchar_t* text, uint32_t marginStyle, uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		void RenderText(RenderBuffer& renderBuffer, const Material* const material, const uint8_t* const shaderObjectData, const jaMatrix44& transformMatrix, const FontPolygon* const font, const char* const text, uint32_t marginStyle, const Color4f& fontColor);

		static inline void translateWorldMatrix(const float& x, const float& y, const float& z);
		static inline void rotateWorldMatrix(float angle, float x, float y, float z);
		static inline void rotateWorldMatrix(const jaMatrix2& rotationMatrix);
		static inline void setColorF(float r, float g, float b);
		static inline void setColorF(float r, float g, float b, float a);
		static inline void setColorUb(uint8_t r, uint8_t g, uint8_t b);
		static inline void setColorUb(uint8_t r, uint8_t g, uint8_t b, uint8_t a);
		static inline void setPointSize(float size);
		static inline void setLineWidth(float width);
		static inline void drawPoint(float x, float y, float z);
		static inline void drawPoint(float x, float y);
		static inline void drawLine(float x, float y, float z, float x1, float y1, float z1);
		static inline void drawLine(float x, float y, float x1, float y1);
		static inline void drawLine(int32_t x, int32_t y, int32_t x1, int32_t y1);
		static inline void drawRectangle(float x, float y, float x1, float y2, bool filled);
		static inline void drawRectangle(int32_t x, int32_t y, int32_t x1, int32_t y1, bool filled);
		static inline void flushRenderingCommands();
		static inline void finishRenderingCommands();

	};

	inline void DisplayManager::flushRenderingCommands()
	{
		glFlush();
	}

	inline void DisplayManager::finishRenderingCommands()
	{
		glFinish();
	}

	inline uint32_t DisplayManager::getRenderingThreadId()
	{
		return singleton->renderingThreadId;
	}

	inline float DisplayManager::getOpenglVersion()
	{
		return singleton->openGLVersion;
	}

	inline void DisplayManager::beginDrawingSprites()
	{
		glPushAttrib(GL_ENABLE_BIT);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, 0);
		glEnable(GL_TEXTURE_2D);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);
		glEnable(GL_TEXTURE_2D);

		glEnable(GL_ALPHA_TEST);
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glShadeModel(GL_SMOOTH);
		/////////////////// new rendering
		//glDisable(GL_TEXTURE_2D);
		glEnableClientState(GL_VERTEX_ARRAY); //removed from opengl 3.3
		glEnableClientState(GL_COLOR_ARRAY); //removed from opengl 3.3
		//glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY); //removed from opengl 3.3
		//glEnableClientState(GL_INDEX_ARRAY);
		
	}

	inline void DisplayManager::endDrawingSprites()
	{
		glActiveTexture(GL_TEXTURE1);
		glDisable(GL_TEXTURE_2D);
		glActiveTexture(GL_TEXTURE0);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		//glDisableClientState(GL_INDEX_ARRAY);
		//glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glPopAttrib();
	}

	inline void DisplayManager::drawSprite(Texture2d* texture, float x, float y, float rotation)
	{
		glPushMatrix();
		glTranslatef(x, y, 0);
		glRotatef(rotation, 0, 0, 1);
		glBindTexture(GL_TEXTURE_2D, texture->getTexture());
		const jaFloat pixelsPerUnit = (jaFloat)setup::graphics::PIXELS_PER_UNIT * GIL_REAL(0.5);
		float halfWidth = (jaFloat)texture->getWidth() / pixelsPerUnit;
		float halfHeight = (jaFloat)texture->getHeight() / pixelsPerUnit;

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBegin(GL_QUADS);

		glTexCoord2d(0, 0);
		glVertex2f(-halfWidth, -halfHeight);

		glTexCoord2d(1, 0);
		glVertex2f(halfWidth, -halfHeight);

		glTexCoord2d(1, 1);
		glVertex2f(halfWidth, halfHeight);

		glTexCoord2d(0, 1);
		glVertex2f(-halfWidth, halfHeight);

		glEnd();

		glPopMatrix();
	}

	inline void DisplayManager::drawSprite(Texture2d* texture, float x, float y, float halfWidth, float halfHeight, float rotation)
	{
		glPushMatrix();
		glTranslatef(x, y, 0);
		glRotatef(rotation, 0, 0, 1);
		glBindTexture(GL_TEXTURE_2D, texture->getTexture());

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBegin(GL_QUADS);

		glTexCoord2d(0, 0);
		glVertex2f(-halfWidth, -halfHeight);

		glTexCoord2d(1, 0);
		glVertex2f(halfWidth, -halfHeight);

		glTexCoord2d(1, 1);
		glVertex2f(halfWidth, halfHeight);

		glTexCoord2d(0, 1);
		glVertex2f(-halfWidth, halfHeight);

		glEnd();

		glPopMatrix();
	}

	inline int32_t DisplayManager::getMultisampleSamples()
	{
		return singleton->multisampleSamples;
	}

	inline int32_t DisplayManager::getResolutionHeight()
	{
		return resolutionHeight;
	}

	inline int32_t DisplayManager::getResolutionWidth()
	{
		return resolutionWidth;
	}


	inline int32_t DisplayManager::getResolutionWidth(float widthPercent)
	{
		return (int32_t)(widthPercent * resolutionWidth);
	}

	inline int32_t DisplayManager::getResolutionHeight(float heightPercent)
	{
		return (int32_t)(heightPercent * resolutionHeight);
	}


	inline float DisplayManager::getWidthMultiplier()
	{
		return widthMultiplier;
	}

	inline float DisplayManager::getHeightMultiplier()
	{
		return heightMultiplier;
	}

	inline void DisplayManager::getWindowPosition(double xWorld, double yWorld, double zWorld, int32_t* x, int32_t* y)
	{
		GLint    viewport[4];
		GLdouble modelview[16];
		GLdouble projection[16];
		GLdouble winX, winY, winZ;

		glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
		glGetDoublev(GL_PROJECTION_MATRIX, projection);
		glGetIntegerv(GL_VIEWPORT, viewport);

		//winX = (float)x;
		//winY = (float)viewport[3] - (float)y;
		//glReadPixels( x, int(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ );

		gluProject(xWorld, yWorld, zWorld, modelview, projection, viewport, &winX, &winY, &winZ);
		*x = (int32_t)winX;
		*y = (int32_t)winY;
	}

	inline void DisplayManager::getWorldPosition(int32_t x, int32_t y, double* xWorld, double* yWorld, double* zWorld)
	{
#ifdef JA_SDL_OPENGL
		GLint    viewport[4];
		GLdouble modelview[16];
		GLdouble projection[16];
		GLfloat  winX, winY, winZ;

		glGetDoublev(GL_MODELVIEW_MATRIX, modelview);
		glGetDoublev(GL_PROJECTION_MATRIX, projection);
		glGetIntegerv(GL_VIEWPORT, viewport);

		winX = (float)x;
		winY = (float)viewport[3] - (float)y;
		glReadPixels(x, int32_t(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

		gluUnProject(winX, winY, winZ, modelview, projection, viewport, xWorld, yWorld, zWorld);
#endif
	}

	inline void DisplayManager::pushWorldMatrix()
	{
#ifdef JA_SDL_OPENGL
		glPushMatrix();
#endif
	}

	inline void DisplayManager::popWorldMatrix()
	{
#ifdef JA_SDL_OPENGL
		glPopMatrix();
#endif
	}

	inline void DisplayManager::pushModelMatrix()
	{
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
	}

	inline void DisplayManager::popModelMatrix()
	{
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
	}

	inline void DisplayManager::pushProjectionMatrix()
	{
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
	}

	inline void DisplayManager::popProjectionMatrix()
	{
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
	}

	inline void DisplayManager::setModelMatrixMode()
	{
		glMatrixMode(GL_MODELVIEW);
	}

	inline void DisplayManager::setProjectionMatrixMode()
	{
		glMatrixMode(GL_PROJECTION);
	}

	inline void DisplayManager::setCameraMatrix(const CameraInterface* camera)
	{
#ifdef JA_SDL_OPENGL
		jaVector2 eye   = camera->position;
		jaFloat   angle = jaVectormath2::radiansToDegrees(camera->angleRad);
		jaVector2 scale = camera->scale;

		setOrtho2DView(-xUnits * 0.5, xUnits * 0.5, -yUnits * 0.5, yUnits * 0.5);

		glLoadIdentity();
		glRotatef(angle, 0.0f, 0.0f, 1.0f);
		glScalef(scale.x, scale.y, 1.0f);	
		glTranslatef(-eye.x, -eye.y, 0.0f);

		jaMatrix44 rotMatrix;
		jaMatrix44 scaleMatrix;
		jaMatrix44 translateMatrix;
		jaMatrix44 rotScaleMatrix;
	
		jaVectormath2::setRotMatrix44_SSE(jaMatrix2(camera->angleRad), rotMatrix);
		jaVectormath2::setScaleMatrix44(jaVector2(scale.x, scale.y), scaleMatrix);
		jaVectormath2::setTranslateMatrix44(jaVector2(-eye.x, -eye.y), translateMatrix);

		jaVectormath2::mulMatrix44_SSE(rotMatrix, scaleMatrix, rotScaleMatrix);
		jaVectormath2::mulMatrix44_SSE(rotScaleMatrix, translateMatrix, DisplayManager::viewMatrix);

		jaVectormath2::setOrthoFrustumMatrix44(-xUnits * 0.5, xUnits * 0.5, -yUnits * 0.5, yUnits * 0.5, -1.0f, 1.0f, DisplayManager::projectionMatrix);
		jaVectormath2::mulMatrix44_SSE(DisplayManager::projectionMatrix, DisplayManager::viewMatrix, DisplayManager::projectionViewMatrix);
#endif
	}

	inline void DisplayManager::swapBuffers()
	{
#ifdef JA_SDL_OPENGL
#ifdef JA_SDL2_VERSION
		SDL_GL_SwapWindow(openglWindow);
#else
		SDL_GL_SwapBuffers();
#endif
#endif
	}

	inline void DisplayManager::enableTexturing()
	{
#ifdef JA_SDL_OPENGL
		glEnable(GL_TEXTURE_2D);
#endif
	}

	inline void DisplayManager::disableTexturing()
	{
#ifdef JA_SDL_OPENGL
		glDisable(GL_TEXTURE_2D);
#endif
	}

	inline void DisplayManager::enableDepthBuffer()
	{
#ifdef JA_SDL_OPENGL
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
#endif
	}

	inline void DisplayManager::disableDepthBuffer()
	{
#ifdef JA_SDL_OPENGL
		glDisable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);
#endif
	}

	inline void DisplayManager::enableStencilTest()
	{
#ifdef JA_SDL_OPENGL
		glEnable(GL_STENCIL_TEST);
#endif
	}

	inline void DisplayManager::disableStencilTest()
	{
#ifdef JA_SDL_OPENGL
		glDisable(GL_STENCIL_TEST);
#endif
	}

	inline void DisplayManager::enableAlphaBlending()
	{
#ifdef JA_SDL_OPENGL
		glEnable(GL_ALPHA);
		glEnable(GL_BLEND);
		glEnable(GL_ALPHA_TEST);
		glDisable(GL_CULL_FACE);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
#endif
	}

	inline void DisplayManager::setAlphaBlendingFunc()
	{
#ifdef JA_SDL_OPENGL
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
#endif
	}

	inline void DisplayManager::disableAlphaBlending()
	{
#ifdef JA_SDL_OPENGL
		glDisable(GL_ALPHA);
		glDisable(GL_BLEND);
		glDisable(GL_ALPHA_TEST);
#endif
	}

	inline void DisplayManager::clearDisplay()
	{
#ifdef JA_SDL_OPENGL
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
#endif
	}

	inline void DisplayManager::clearColorDisplay()
	{
#ifdef JA_SDL_OPENGL
		glClear(GL_COLOR_BUFFER_BIT);
#endif
	}

	inline void DisplayManager::clearStencilDisplay()
	{
#ifdef JA_SDL_OPENGL
		glClear(GL_STENCIL_BUFFER_BIT);
#endif
	}

	inline void DisplayManager::clearDepthDisplay()
	{
#ifdef JA_SDL_OPENGL
		glClear(GL_DEPTH_BUFFER_BIT);
#endif
	}

	inline void DisplayManager::clearWorldMatrix()
	{
#ifdef JA_SDL_OPENGL
		glLoadIdentity();
#endif
	}

	inline void DisplayManager::setColorF(float r, float g, float b)
	{
#ifdef JA_SDL_OPENGL
		glColor3f(r, g, b);
#endif
	}

	inline void DisplayManager::setColorF(float r, float g, float b, float a)
	{
#ifdef JA_SDL_OPENGL
		glColor4f(r, g, b, a);
#endif
	}

	inline void DisplayManager::setColorUb(uint8_t r, uint8_t g, uint8_t b)
	{
#ifdef JA_SDL_OPENGL
		glColor3ub(r, g, b);
#endif
	}

	inline void DisplayManager::setColorUb(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
	{
#ifdef JA_SDL_OPENGL
		glColor4ub(r, g, b, a);
#endif
	}

	inline void DisplayManager::setPointSize(float size)
	{
#ifdef JA_SDL_OPENGL
		glPointSize(size);
#endif
	}

	inline void DisplayManager::setLineWidth(float width)
	{
#ifdef JA_SDL_OPENGL
		glLineWidth(width);
#endif
	}

	inline void DisplayManager::drawPoint(float x, float y, float z)
	{
#ifdef JA_SDL_OPENGL
		glBegin(GL_POINTS);
		glVertex3f(x, y, z);
		glEnd();
#endif
	}

	inline void DisplayManager::drawPoint(float x, float y)
	{
#ifdef JA_SDL_OPENGL
		glBegin(GL_POINTS);
		glVertex2f(x, y);
		glEnd();
#endif
	}

	inline void DisplayManager::drawLine(float x, float y, float z, float x1, float y1, float z1)
	{
#ifdef JA_SDL_OPENGL
		glBegin(GL_LINES);
		glVertex3f(x, y, z);
		glVertex3f(x1, y1, z1);
		glEnd();
#endif
	}

	inline void DisplayManager::drawLine(float x, float y, float x1, float y1)
	{
#ifdef JA_SDL_OPENGL
		glBegin(GL_LINES);
		glVertex2f(x, y);
		glVertex2f(x1, y1);
		glEnd();
#endif
	}

	inline void DisplayManager::drawLine(int32_t x, int32_t y, int32_t x1, int32_t y1)
	{
#ifdef JA_SDL_OPENGL
		glBegin(GL_LINES);
		glVertex2i(x, y);
		glVertex2i(x1, y1);
		glEnd();
#endif
	}

	inline void DisplayManager::drawSprite(float x, float y, float halfWidth, float halfHeight, bool filled)
	{
#ifdef JA_SDL_OPENGL
		glPushMatrix();
		glTranslatef(x, y, 0);
		if (filled)
		{
			glBegin(GL_QUADS);
			glVertex2f(halfWidth, halfHeight);
			glVertex2f(halfWidth, -halfHeight);
			glVertex2f(-halfWidth, -halfHeight);
			glVertex2f(-halfWidth, halfHeight);
			glEnd();
		}
		else
		{
			glBegin(GL_LINE_LOOP);
			glVertex2f(halfWidth, halfHeight);
			glVertex2f(halfWidth, -halfHeight);
			glVertex2f(-halfWidth, -halfHeight);
			glVertex2f(-halfWidth, halfHeight);
			glEnd();
		}
		glPopMatrix();
#endif
	}

	inline void DisplayManager::drawRectangle(float x, float y, float x1, float y1, bool filled)
	{
		GLenum drawType;
		if (filled)
			drawType = GL_QUADS;
		else
			drawType = GL_LINE_LOOP;

		glBegin(drawType);
		glVertex2f(x, y);
		glVertex2f(x1, y);
		glVertex2f(x1, y1);
		glVertex2f(x, y1);
		glEnd();
	}

	inline void DisplayManager::drawRectangle(int32_t x, int32_t y, int32_t x1, int32_t y1, bool filled)
	{
		GLenum drawType;
		if (filled)
			drawType = GL_QUADS;
		else
			drawType = GL_LINE_LOOP;
		
		glBegin(drawType);
		glVertex2i(x, y);
		glVertex2i(x1, y);
		glVertex2i(x1, y1);
		glVertex2i(x, y1);
		glEnd();
	}

	static inline void setScreenCoordinate()
	{
#ifdef JA_SDL_OPENGL
		GLint	viewport[4];
		glGetIntegerv(GL_VIEWPORT, viewport);
		glMatrixMode(GL_PROJECTION);

		glLoadIdentity();
		gluOrtho2D(viewport[0], viewport[2], viewport[1], viewport[3]);

		glMatrixMode(GL_MODELVIEW);
#endif
	}

	inline void DisplayManager::pushScreenCoordinate()
	{
#ifdef JA_SDL_OPENGL
		glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT | GL_TRANSFORM_BIT);
		GLint	viewport[4];
		glGetIntegerv(GL_VIEWPORT, viewport);
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		gluOrtho2D(viewport[0], viewport[2], viewport[1], viewport[3]);

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
#endif
	}

	inline void DisplayManager::popWorldCoordinate()
	{
#ifdef JA_SDL_OPENGL
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glPopAttrib();
		glMatrixMode(GL_MODELVIEW);
#endif
	}

	inline void DisplayManager::setOrthoView(double left, double right, double bottom, double top, double nearVal, double farVal)
	{
#ifdef JA_SDL_OPENGL
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(left, right, bottom, top, nearVal, farVal);
		glMatrixMode(GL_MODELVIEW);
#endif
	}

	inline void DisplayManager::setOrtho2DView(double left, double right, double bottom, double top)
	{
#ifdef JA_SDL_OPENGL
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(left, right, bottom, top);
		glMatrixMode(GL_MODELVIEW);
#endif
	}

	inline void DisplayManager::setPerspectiveView(double fovy, double aspect, double zNear, double zFar)
	{
#ifdef JA_SDL_OPENGL
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(fovy, aspect, zNear, zFar);
		glMatrixMode(GL_MODELVIEW);
#endif
	}

	inline void DisplayManager::translateWorldMatrix(const float& x, const float& y, const float& z)
	{
#ifdef JA_SDL_OPENGL
		glTranslatef(x, y, z);
#endif
	}

	inline void DisplayManager::rotateWorldMatrix(float angle, float x, float y, float z)
	{
#ifdef JA_SDL_OPENGL
		glRotatef(angle, x, y, z);
#endif
	}

	inline void DisplayManager::rotateWorldMatrix(const jaMatrix2& rotationMatrix)
	{
#ifdef JA_SDL_OPENGL
		jaMatrix44 rotMatrix;
		jaVectormath2::setRotMatrix44(rotationMatrix, rotMatrix);
		glMultMatrixf(rotMatrix.element);
#endif
	}

}
