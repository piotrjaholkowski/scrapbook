#pragma once

#include "../jaSetup.hpp"

#include "../Allocators/LinearAllocator.hpp"
#include <iostream>
#include <thread>
#include <atomic>
#include <mutex>
#include <condition_variable> 
//#include <CL/cl.h>

namespace ja
{

	class WorkingThread;

	typedef void(*TaskArrayFunction)(WorkingThread*);
	int32_t workingThreadFunction(void* data);

	class ThreadTaskParameters
	{
	public:
		uint32_t threadDataMemSizeByThread;
		uint32_t taskNum;
		void*    threadDataMem;
		void*    taskData;
		void*    taskArrayData;
		TaskArrayFunction taskArrayCallback;
	};

	void emptyCallback(WorkingThread* workingThread);
	void stopAllWorkersCallback(WorkingThread* workingThread);

	class WorkingThread
	{
	public:
		bool isInit;
		std::thread workingThread;
		ThreadTaskParameters* taskParameters;

		uint32_t threadAllocatedMemSize;
		TaskArrayFunction taskArrayCallback;
		uint32_t threadId;
		uint32_t taskNum;
		void* taskArrayData; // pointer to task array
		void* taskData; // pointer to arguments
		void* threadAllocatedMem; // pointer to memory which can be used by thread while running
		uint32_t memoryAllocatedByThread; // size of memory which was allocated by thread while running
		uint32_t threadsNum;

		WorkingThread()
		{
			isInit = false;
			taskArrayCallback = nullptr;
		}

		void initTaskParameters()
		{
			threadAllocatedMemSize = taskParameters->threadDataMemSizeByThread;
			threadAllocatedMem = static_cast<void*>(static_cast<char*>(taskParameters->threadDataMem) + (threadId * taskParameters->threadDataMemSizeByThread));
			taskArrayData = taskParameters->taskArrayData;
			taskArrayCallback = taskParameters->taskArrayCallback;
			taskData = taskParameters->taskData;
			memoryAllocatedByThread = 0;
			taskNum = taskParameters->taskNum;
		}

		void initThread()
		{
			isInit = true;
			if (threadId != 0) {
				workingThread = std::thread(workingThreadFunction, this);
			}
		}

		void deinitThread()
		{
			isInit = false;
		}

		~WorkingThread()
		{

		}
	};

	class ThreadManager
	{
	private:
		static ThreadManager* singleton;
		WorkingThread workingThreads[setup::threads::MAX_THREADS];

		int32_t          threadsWorking;
		volatile int32_t threadsNum;
		bool warmUpMode;
	public:
		bool useAtomicBarriers;
		bool sendSignalAfterFinish;
	private:

		ThreadTaskParameters taskParameters;

		std::mutex threadsSleepMutex; // This mutex is used to put threads to sleep
		std::mutex mainThreadSleepMutex; // This mutex is used by main thread to put it to sleep and wait until all tasks are finished
		std::atomic<int32_t> mainThreadSleepAtomicMutex;
		std::condition_variable_any wakeUpThreads;
		std::condition_variable_any wakeUpMainThread;
		std::mutex gate0;

		std::atomic<uint32_t> taskCounter;
		std::atomic<int32_t> threadsCounter;
		std::atomic<int32_t> spinWorkers;
		std::atomic<int32_t> atomicCounter0;

		ThreadManager();

		//This method is called after thread in task array mode finishes its job
		void taskArrayFinished();

	public:
		~ThreadManager();
		static void init();

		//static void displayOpenClPlatformInfo(cl_platform_id id, cl_platform_info param_name, const char* paramNameAsStr);
		//static void displayOpenClDeviceDetails(cl_device_id id, cl_device_info param_name, const char* paramNameAsStr);
		//static void displayOpenClDeviceInfo(cl_platform_id id, cl_device_type dev_type);
		//static void displayOpenClInfo();
		//static const char* getOpenClErrorInfo(cl_int error);

		static inline ThreadManager* ThreadManager::getSingleton()
		{
			return singleton;
		}

		inline WorkingThread* getThreadArray() {
			return workingThreads;
		}

		void setThreadsNum(int32_t threadNum);
		void setWarmUpMode(bool isWarmUpMode);
		inline bool isWarmUpMode() {
			return warmUpMode;
		}
		uint32_t getThreadsAllocatedMemorySize();

		//Function called by main thread
		//This threading model is similar to GPU threading model
		//treadDataPointer - Array of pointers to thread data
		//taskNum - Length of array
		void createTaskArray(void* taskArrayData, uint32_t taskArraySize, void* threadDataMem, uint32_t threadDataMemSize, TaskArrayFunction taskArrayFunction, void* taskData);

		void lockGate0();
		void unlockGate0();
		std::atomic<int32_t>* getAtomicCounter0() {
			return &atomicCounter0;
		}

		inline void zeroTaskCounter()
		{
			taskCounter.store(0);
		}

		inline uint32_t ThreadManager::getTaskIdAndIncreaseTaskSolvedNumber(uint32_t taskSolveIncreaseNumber)
		{
			return taskCounter.fetch_add(taskSolveIncreaseNumber, std::memory_order_relaxed);
		}

		uint32_t getThreadsNum()
		{
			return threadsNum;
		}

		//This method stops calling thread until all worker threads finish their job
		void waitUntilFinish();
		static void cleanUp();

		friend int32_t workingThreadFunction(void* data);
	};

}
