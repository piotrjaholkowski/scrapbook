#pragma once

#include "../Allocators/StackAllocator.hpp"
#include <iostream>

class RecycledMemoryBlock {
public:
	bool isInUse;
	uint32_t memoryBlockSize;
	void* memoryPointer;
};

class RecycledMemoryManager {
private:
	static RecycledMemoryManager* singleton;
	StackAllocator<RecycledMemoryBlock*> memoryBlocks;
	RecycledMemoryManager();
public:
	static void initRecycledMemoryManager();
	~RecycledMemoryManager();
	static inline RecycledMemoryManager* getSingleton();

	void* getMemoryBlock(uint32_t memoryBlockSize);
	void releaseMemoryBlock(void* recycledMemoryBlock);
	
	static void cleanUp();
};

inline RecycledMemoryManager* RecycledMemoryManager::getSingleton()
{
	if (singleton == nullptr) {
		initRecycledMemoryManager();
	}
	return singleton;
}
