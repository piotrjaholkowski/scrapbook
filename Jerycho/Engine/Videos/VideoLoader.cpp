#include "VideoLoader.hpp"

/*#include "../Utility/Exception.hpp"

extern "C" {
#include <libavformat/avformat.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/imgutils.h>
}

namespace ja
{
VideoLoader* VideoLoader::pSingleton = nullptr;

VideoLoader::VideoLoader()
{

}

void VideoLoader::init()
{
	if (pSingleton == nullptr)
	{
		//register available codecs
		av_register_all();
		pSingleton = new VideoLoader();
	}
}

void VideoLoader::cleanUp()
{
	if (pSingleton != nullptr)
	{
		delete pSingleton;
		pSingleton = nullptr;
	}
}

VideoStream* VideoLoader::loadVideoStream(const ja::string& fileName, uint32_t resourceId)
{
	VideoStream* pVideoStream = new VideoStream(resourceId);

	if (avformat_open_input(&pVideoStream->pAvFormatContext, fileName.c_str(), nullptr, nullptr) < 0)
	{
		delete pVideoStream;
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_VIDEO_STREAM, fileName);
	}

	if (avformat_find_stream_info(pVideoStream->pAvFormatContext, nullptr) < 0)
	{
		delete pVideoStream;
		ja::string exceptionReason = fileName;
		exceptionReason.append(": Could not find stream information");
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_VIDEO_STREAM, exceptionReason);
	}

	// Dump information about file onto standard error
	av_dump_format(pVideoStream->pAvFormatContext, 0, fileName.c_str(), 0);

	
	int32_t videoStreamId = -1;

	// Find the first video stream
	
	for (uint32_t i = 0; i<pVideoStream->pAvFormatContext->nb_streams; i++)
	{ 
		if (pVideoStream->pAvFormatContext->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
			videoStreamId= i;
			break;
		}
	}

	if (videoStreamId == -1)
	{
		delete pVideoStream;
		ja::string exceptionReason = fileName;
		exceptionReason.append(": Could not find video stream");
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_VIDEO_STREAM, exceptionReason);
	}
		
	// Get a pointer to the codec context for the video stream
	//pCodecCtx = ;
	AVCodecContext *pCodecCtxOrig = pVideoStream->pAvFormatContext->streams[videoStreamId]->codec;

	if (pCodecCtxOrig == nullptr) {
		delete pVideoStream;
		ja::string exceptionReason = fileName;
		exceptionReason.append(": Unsupported codec");
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_VIDEO_STREAM, exceptionReason);
	}

	
	AVCodecContext *pCodecCtx = nullptr;

	// Copy context
	pCodecCtx = avcodec_alloc_context3(pCodecCtxOrig->codec);
	if (avcodec_copy_context(pCodecCtx, pCodecCtxOrig) != 0)
	{
		delete pVideoStream;
		ja::string exceptionReason = fileName;
		exceptionReason.append(": Couldn't copy codec context");
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_VIDEO_STREAM, exceptionReason);
	}

	AVCodec *pCodec = nullptr;
	// Find the decoder for the video stream
	pCodec = avcodec_find_decoder(pCodecCtx->codec_id); 

	// Open codec
	if (avcodec_open2(pCodecCtx, pCodec, nullptr) < 0)
	{
		delete pVideoStream;
		ja::string exceptionReason = fileName;
		exceptionReason.append(": Could not open codec");
		throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_VIDEO_STREAM, exceptionReason);
	}
	
	// Allocate video frame
	AVFrame* pFrame = av_frame_alloc();

	// Allocate an AVFrame structure
	AVFrame* pFrameRGB = av_frame_alloc();

	uint8_t *buffer = nullptr;
	int32_t  numBytes;
	// Determine required buffer size and allocate buffer
	//AVPixelFormat
	numBytes = avpicture_get_size(AV_PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
	buffer = (uint8_t *)av_malloc(numBytes*sizeof(uint8_t));

	// Assign appropriate parts of buffer to image planes in pFrameRGB
	// Note that pFrameRGB is an AVFrame, but AVFrame is a superset
	// of AVPicture
	avpicture_fill((AVPicture *)pFrameRGB, buffer, AV_PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);

	SwsContext *sws_ctx = NULL;
	int32_t frameFinished;
	AVPacket packet;
	// initialize SWS context for software scaling
	sws_ctx = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, pCodecCtx->width, pCodecCtx->height, AV_PIX_FMT_RGB24, SWS_BILINEAR, nullptr, nullptr, nullptr);
	
	while (av_read_frame(pVideoStream->pAvFormatContext, &packet) >= 0)
	{
		// Is this a packet from the video stream?
		if (packet.stream_index == videoStreamId) {
			// Decode video frame
			avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);

			// Did we get a video frame?
			if (frameFinished)
			{
				// Convert the image from its native format to RGB
				sws_scale(sws_ctx, (uint8_t const * const *)pFrame->data, pFrame->linesize, 0, pCodecCtx->height, pFrameRGB->data, pFrameRGB->linesize);

				//SaveFrame(pFrameRGB, pCodecCtx->width, pCodecCtx->height, i);
			}
		}

		// Free the packet that was allocated by av_read_frame
		av_free_packet(&packet);
	}

	av_free(buffer);
	av_free(pFrameRGB);

	// Free the YUV frame
	av_free(pFrame);

	// Close the codecs
	avcodec_close(pCodecCtx);
	avcodec_close(pCodecCtxOrig);

	// Close the video file
	avformat_close_input(&pVideoStream->pAvFormatContext);

	return pVideoStream;
}

}*/