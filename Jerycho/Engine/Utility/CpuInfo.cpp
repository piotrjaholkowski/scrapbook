#include "CpuInfo.hpp"

#include "../Manager/LogManager.hpp"

#include <stdint.h>

namespace ja
{

CpuInfo::CpuInfo()
{
	int32_t cpuInfo[4];
	__cpuid(cpuInfo, 0); // serialization instruction

	// __cpuid with an InfoType argument of 0 returns the number of
	// valid Ids in CPUInfo[0] and the CPU identification string in
	// the other three array elements. The CPU identification string is
	// not in linear order. The code below arranges the information 
	// in a human readable form.
	__cpuid(CPUInfo, 0);
	nIds = CPUInfo[0];
	memset(CPUString, 0, sizeof(CPUString));
	*((int32_t*)CPUString) = CPUInfo[1];
	*((int32_t*)(CPUString + 4)) = CPUInfo[3];
	*((int32_t*)(CPUString + 8)) = CPUInfo[2];

	// Get the information associated with each valid Id
	for (i = 0; i <= nIds; ++i)
	{
		__cpuid(CPUInfo, i);
		//printf_s("\nFor InfoType %d\n", i);
		//printf_s("CPUInfo[0] = 0x%x\n", CPUInfo[0]);
		//printf_s("CPUInfo[1] = 0x%x\n", CPUInfo[1]);
		//printf_s("CPUInfo[2] = 0x%x\n", CPUInfo[2]);
		//printf_s("CPUInfo[3] = 0x%x\n", CPUInfo[3]);

		// Interpret CPU feature information.
		if (i == 1)
		{
			nSteppingID = CPUInfo[0] & 0xf;
			nModel = (CPUInfo[0] >> 4) & 0xf;
			nFamily = (CPUInfo[0] >> 8) & 0xf;
			nProcessorType = (CPUInfo[0] >> 12) & 0x3;
			nExtendedmodel = (CPUInfo[0] >> 16) & 0xf;
			nExtendedfamily = (CPUInfo[0] >> 20) & 0xff;
			nBrandIndex = CPUInfo[1] & 0xff;
			nCLFLUSHcachelinesize = ((CPUInfo[1] >> 8) & 0xff) * 8;
			nLogicalProcessors = ((CPUInfo[1] >> 16) & 0xff);
			nAPICPhysicalID = (CPUInfo[1] >> 24) & 0xff;
			bSSE3Instructions = (CPUInfo[2] & 0x1) || false;
			bMONITOR_MWAIT = (CPUInfo[2] & 0x8) || false;
			bCPLQualifiedDebugStore = (CPUInfo[2] & 0x10) || false;
			bVirtualMachineExtensions = (CPUInfo[2] & 0x20) || false;
			bEnhancedIntelSpeedStepTechnology = (CPUInfo[2] & 0x80) || false;
			bThermalMonitor2 = (CPUInfo[2] & 0x100) || false;
			bSupplementalSSE3 = (CPUInfo[2] & 0x200) || false;
			bL1ContextID = (CPUInfo[2] & 0x300) || false;
			bCMPXCHG16B = (CPUInfo[2] & 0x2000) || false;
			bxTPRUpdateControl = (CPUInfo[2] & 0x4000) || false;
			bPerfDebugCapabilityMSR = (CPUInfo[2] & 0x8000) || false;
			bSSE41Extensions = (CPUInfo[2] & 0x80000) || false;
			bSSE42Extensions = (CPUInfo[2] & 0x100000) || false;
			bPOPCNT = (CPUInfo[2] & 0x800000) || false;
			bAVX = (CPUInfo[2] & 0x10000000) || false;
			nFeatureInfo = CPUInfo[3];
			bMultithreading = (nFeatureInfo & (1 << 28)) || false;
		}

		if (i == 7)
		{
			bAVX2 = (CPUInfo[1] & 0x20) || false;
		}
	}

	// Calling __cpuid with 0x80000000 as the InfoType argument
	// gets the number of valid extended IDs.
	__cpuid(CPUInfo, 0x80000000);
	nExIds = CPUInfo[0];
	memset(CPUBrandString, 0, sizeof(CPUBrandString));

	// Get the information associated with each extended ID.
	for (i = 0x80000000; i <= nExIds; ++i)
	{
		__cpuid(CPUInfo, i);
		//printf_s("\nFor InfoType %x\n", i);
		//printf_s("CPUInfo[0] = 0x%x\n", CPUInfo[0]);
		//printf_s("CPUInfo[1] = 0x%x\n", CPUInfo[1]);
		//printf_s("CPUInfo[2] = 0x%x\n", CPUInfo[2]);
		//printf_s("CPUInfo[3] = 0x%x\n", CPUInfo[3]);

		if (i == 0x80000001)
		{
			bLAHF_SAHFAvailable = (CPUInfo[2] & 0x1) || false;
			bCmpLegacy = (CPUInfo[2] & 0x2) || false;
			bSVM = (CPUInfo[2] & 0x4) || false;
			bExtApicSpace = (CPUInfo[2] & 0x8) || false;
			bAltMovCr8 = (CPUInfo[2] & 0x10) || false;
			bLZCNT = (CPUInfo[2] & 0x20) || false;
			bSSE4A = (CPUInfo[2] & 0x40) || false;
			bMisalignedSSE = (CPUInfo[2] & 0x80) || false;
			bPREFETCH = (CPUInfo[2] & 0x100) || false;
			bSKINITandDEV = (CPUInfo[2] & 0x1000) || false;
			bSYSCALL_SYSRETAvailable = (CPUInfo[3] & 0x800) || false;
			bExecuteDisableBitAvailable = (CPUInfo[3] & 0x10000) || false;
			bMMXExtensions = (CPUInfo[3] & 0x40000) || false;
			bFFXSR = (CPUInfo[3] & 0x200000) || false;
			b1GBSupport = (CPUInfo[3] & 0x400000) || false;
			bRDTSCP = (CPUInfo[3] & 0x8000000) || false;
			b64Available = (CPUInfo[3] & 0x20000000) || false;
			b3DNowExt = (CPUInfo[3] & 0x40000000) || false;
			b3DNow = (CPUInfo[3] & 0x80000000) || false;
		}

		// Interpret CPU brand string and cache information.
		if (i == 0x80000002)
			memcpy(CPUBrandString, CPUInfo, sizeof(CPUInfo));
		else if (i == 0x80000003)
			memcpy(CPUBrandString + 16, CPUInfo, sizeof(CPUInfo));
		else if (i == 0x80000004)
			memcpy(CPUBrandString + 32, CPUInfo, sizeof(CPUInfo));
		else if (i == 0x80000006)
		{
			nCacheLineSize = CPUInfo[2] & 0xff;
			nL2Associativity = (CPUInfo[2] >> 12) & 0xf;
			nCacheSizeK = (CPUInfo[2] >> 16) & 0xffff;
		}
		else if (i == 0x80000008)
		{
			nPhysicalAddress = CPUInfo[0] & 0xff;
			nVirtualAddress = (CPUInfo[0] >> 8) & 0xff;
		}
		else if (i == 0x8000000A)
		{
			bNestedPaging = (CPUInfo[3] & 0x1) || false;
			bLBRVisualization = (CPUInfo[3] & 0x2) || false;
		}
		else if (i == 0x8000001A)
		{
			bFP128 = (CPUInfo[0] & 0x1) || false;
			bMOVOptimization = (CPUInfo[0] & 0x2) || false;
		}
	}
}

void CpuInfo::printInfo()
{
	// Display all the information in user-friendly format.

	printf_s("CPU String: %s\n", CPUString);

	if (nIds >= 1)
	{
		if (nSteppingID)
			printf_s("Stepping ID = %d\n", nSteppingID);
		if (nModel)
			printf_s("Model = %d\n", nModel);
		if (nFamily)
			printf_s("Family = %d\n", nFamily);
		if (nProcessorType)
			printf_s("Processor Type = %d\n", nProcessorType);
		if (nExtendedmodel)
			printf_s("Extended model = %d\n", nExtendedmodel);
		if (nExtendedfamily)
			printf_s("Extended family = %d\n", nExtendedfamily);
		if (nBrandIndex)
			printf_s("Brand Index = %d\n", nBrandIndex);
		if (nCLFLUSHcachelinesize)
			printf_s("CLFLUSH cache line size = %d\n",
			nCLFLUSHcachelinesize);
		if (bMultithreading && (nLogicalProcessors > 0))
			printf_s("Logical Processor Count = %d\n", nLogicalProcessors);
		if (nAPICPhysicalID)
			printf_s("APIC Physical ID = %d\n", nAPICPhysicalID);

		if (nFeatureInfo || bSSE3Instructions ||
			bMONITOR_MWAIT || bCPLQualifiedDebugStore ||
			bVirtualMachineExtensions || bEnhancedIntelSpeedStepTechnology ||
			bThermalMonitor2 || bSupplementalSSE3 || bL1ContextID ||
			bCMPXCHG16B || bxTPRUpdateControl || bPerfDebugCapabilityMSR ||
			bSSE41Extensions || bSSE42Extensions || bPOPCNT ||
			bLAHF_SAHFAvailable || bCmpLegacy || bSVM ||
			bExtApicSpace || bAltMovCr8 ||
			bLZCNT || bSSE4A || bMisalignedSSE ||
			bPREFETCH || bSKINITandDEV || bSYSCALL_SYSRETAvailable ||
			bExecuteDisableBitAvailable || bMMXExtensions || bFFXSR || b1GBSupport ||
			bRDTSCP || b64Available || b3DNowExt || b3DNow || bNestedPaging ||
			bLBRVisualization || bFP128 || bMOVOptimization)
		{
			printf_s("\nThe following features are supported:\n");

			if (bSSE3Instructions)
				printf_s("\tSSE3\n");
			if (bMONITOR_MWAIT)
				printf_s("\tMONITOR/MWAIT\n");
			if (bCPLQualifiedDebugStore)
				printf_s("\tCPL Qualified Debug Store\n");
			if (bVirtualMachineExtensions)
				printf_s("\tVirtual Machine Extensions\n");
			if (bEnhancedIntelSpeedStepTechnology)
				printf_s("\tEnhanced Intel SpeedStep Technology\n");
			if (bThermalMonitor2)
				printf_s("\tThermal Monitor 2\n");
			if (bSupplementalSSE3)
				printf_s("\tSupplemental Streaming SIMD Extensions 3\n");
			if (bL1ContextID)
				printf_s("\tL1 Context ID\n");
			if (bCMPXCHG16B)
				printf_s("\tCMPXCHG16B Instruction\n");
			if (bxTPRUpdateControl)
				printf_s("\txTPR Update Control\n");
			if (bPerfDebugCapabilityMSR)
				printf_s("\tPerf\\Debug Capability MSR\n");
			if (bSSE41Extensions)
				printf_s("\tSSE4.1 Extensions\n");
			if (bSSE42Extensions)
				printf_s("\tSSE4.2 Extensions\n");
			if (bPOPCNT)
				printf_s("\tPPOPCNT Instruction\n");
			if (bAVX)
				printf_s("\tAVX Extensions\n");
			if (bAVX2)
				printf_s("\tAVX2 Extensions\n");

			i = 0;
			nIds = 1;
			while (i < (sizeof(szCpuFeatures) / sizeof(const char*)))
			{
				if (nFeatureInfo & nIds)
				{
					printf_s("\t");
					printf_s(szCpuFeatures[i]);
					printf_s("\n");
				}

				nIds <<= 1;
				++i;
			}

			if (bLAHF_SAHFAvailable)
				printf_s("\tLAHF/SAHF in 64-bit mode\n");
			if (bCmpLegacy)
				printf_s("\tCore multi-processing legacy mode\n");
			if (bSVM)
				printf_s("\tSecure Virtual Machine\n");
			if (bExtApicSpace)
				printf_s("\tExtended APIC Register Space\n");
			if (bAltMovCr8)
				printf_s("\tAltMovCr8\n");
			if (bLZCNT)
				printf_s("\tLZCNT instruction\n");
			if (bSSE4A)
				printf_s("\tSSE4A (EXTRQ, INSERTQ, MOVNTSD, MOVNTSS)\n");
			if (bMisalignedSSE)
				printf_s("\tMisaligned SSE mode\n");
			if (bPREFETCH)
				printf_s("\tPREFETCH and PREFETCHW Instructions\n");
			if (bSKINITandDEV)
				printf_s("\tSKINIT and DEV support\n");
			if (bSYSCALL_SYSRETAvailable)
				printf_s("\tSYSCALL/SYSRET in 64-bit mode\n");
			if (bExecuteDisableBitAvailable)
				printf_s("\tExecute Disable Bit\n");
			if (bMMXExtensions)
				printf_s("\tExtensions to MMX Instructions\n");
			if (bFFXSR)
				printf_s("\tFFXSR\n");
			if (b1GBSupport)
				printf_s("\t1GB page support\n");
			if (bRDTSCP)
				printf_s("\tRDTSCP instruction\n");
			if (b64Available)
				printf_s("\t64 bit Technology\n");
			if (b3DNowExt)
				printf_s("\t3Dnow Ext\n");
			if (b3DNow)
				printf_s("\t3Dnow! instructions\n");
			if (bNestedPaging)
				printf_s("\tNested Paging\n");
			if (bLBRVisualization)
				printf_s("\tLBR Visualization\n");
			if (bFP128)
				printf_s("\tFP128 optimization\n");
			if (bMOVOptimization)
				printf_s("\tMOVU Optimization\n");
		}
	}

	if (nExIds >= 0x80000004)
		printf_s("\nCPU Brand String: %s\n", CPUBrandString);

	if (nExIds >= 0x80000006)
	{
		printf_s("Cache Line Size = %d\n", nCacheLineSize);
		printf_s("L2 Associativity = %d\n", nL2Associativity);
		printf_s("Cache Size = %dK\n", nCacheSizeK);
	}

	for (i = 0;; i++)
	{
		__cpuidex(CPUInfo, 0x4, i);
		if (!(CPUInfo[0] & 0xf0)) break;

		if (i == 0)
		{
			nCores = CPUInfo[0] >> 26;
			printf_s("\n\nNumber of Cores = %d\n", nCores + 1);
		}

		nCacheType = (CPUInfo[0] & 0x1f);
		nCacheLevel = (CPUInfo[0] & 0xe0) >> 5;
		bSelfInit = (0 != ((CPUInfo[0] & 0x100) >> 8));
		bFullyAssociative = (0 != ((CPUInfo[0] & 0x200) >> 9));
		nMaxThread = (CPUInfo[0] & 0x03ffc000) >> 14;
		nSysLineSize = (CPUInfo[1] & 0x0fff);
		nPhysicalLinePartitions = (CPUInfo[1] & 0x03ff000) >> 12;
		nWaysAssociativity = (CPUInfo[1]) >> 22;
		nNumberSets = CPUInfo[2];

		printf_s("\n");

		printf_s("ECX Index %d\n", i);
		switch (nCacheType)
		{
		case 0:
			printf_s("   Type: Null\n");
			break;
		case 1:
			printf_s("   Type: Data Cache\n");
			break;
		case 2:
			printf_s("   Type: Instruction Cache\n");
			break;
		case 3:
			printf_s("   Type: Unified Cache\n");
			break;
		default:
			printf_s("   Type: Unknown\n");
		}

		printf_s("   Level = %d\n", nCacheLevel + 1);
		if (bSelfInit)
		{
			printf_s("   Self Initializing\n");
		}
		else
		{
			printf_s("   Not Self Initializing\n");
		}
		if (bFullyAssociative)
		{
			printf_s("   Is Fully Associatve\n");
		}
		else
		{
			printf_s("   Is Not Fully Associatve\n");
		}
		printf_s("   Max Threads = %d\n",
			nMaxThread + 1);
		printf_s("   System Line Size = %d\n",
			nSysLineSize + 1);
		printf_s("   Physical Line Partions = %d\n",
			nPhysicalLinePartitions + 1);
		printf_s("   Ways of Associativity = %d\n",
			nWaysAssociativity + 1);
		printf_s("   Number of Sets = %d\n",
			nNumberSets + 1);
	}
}

void CpuInfo::logInfo()
{
	LogManager* logManager = LogManager::getSingleton();

	if (nullptr == logManager)
		return;

	//printf_s("CPU String: %s\n", CPUString);
	logManager->addLogMessage(LogType::INFO_LOG, "CPU String: %s", CPUString);

	if (nIds >= 1)
	{
		if (nSteppingID)
			logManager->addLogMessage(LogType::INFO_LOG, "Stepping ID = %d", nSteppingID);
		if (nModel)
			logManager->addLogMessage(LogType::INFO_LOG, "Model = %d", nModel);
		if (nFamily)
			logManager->addLogMessage(LogType::INFO_LOG, "Family = %d", nFamily);
		if (nProcessorType)
			logManager->addLogMessage(LogType::INFO_LOG, "Processor Type = %d", nProcessorType);
		if (nExtendedmodel)
			logManager->addLogMessage(LogType::INFO_LOG, "Processor Type = %d", nProcessorType);
		if (nExtendedfamily)
			logManager->addLogMessage(LogType::INFO_LOG, "Extended family = %d", nExtendedfamily);
		if (nBrandIndex)
			logManager->addLogMessage(LogType::INFO_LOG, "Brand Index = %d", nBrandIndex);
		if (nCLFLUSHcachelinesize)
			logManager->addLogMessage(LogType::INFO_LOG, "CLFLUSH cache line size = %d", nCLFLUSHcachelinesize);
		if (bMultithreading && (nLogicalProcessors > 0))
			logManager->addLogMessage(LogType::INFO_LOG, "Logical Processor Count = %d", nLogicalProcessors);
		if (nAPICPhysicalID)
			logManager->addLogMessage(LogType::INFO_LOG, "APIC Physical ID = %d", nAPICPhysicalID);

		if (nFeatureInfo || bSSE3Instructions ||
			bMONITOR_MWAIT || bCPLQualifiedDebugStore ||
			bVirtualMachineExtensions || bEnhancedIntelSpeedStepTechnology ||
			bThermalMonitor2 || bSupplementalSSE3 || bL1ContextID ||
			bCMPXCHG16B || bxTPRUpdateControl || bPerfDebugCapabilityMSR ||
			bSSE41Extensions || bSSE42Extensions || bPOPCNT ||
			bLAHF_SAHFAvailable || bCmpLegacy || bSVM ||
			bExtApicSpace || bAltMovCr8 ||
			bLZCNT || bSSE4A || bMisalignedSSE ||
			bPREFETCH || bSKINITandDEV || bSYSCALL_SYSRETAvailable ||
			bExecuteDisableBitAvailable || bMMXExtensions || bFFXSR || b1GBSupport ||
			bRDTSCP || b64Available || b3DNowExt || b3DNow || bNestedPaging ||
			bLBRVisualization || bFP128 || bMOVOptimization)
		{
			logManager->addLogMessage(LogType::INFO_LOG, "\nThe following features are supported:");

			if (bSSE3Instructions)
				logManager->addLogMessage(LogType::INFO_LOG, "\tSSE3");
			if (bMONITOR_MWAIT)
				logManager->addLogMessage(LogType::INFO_LOG, "\tMONITOR/MWAIT");
			if (bCPLQualifiedDebugStore)
				logManager->addLogMessage(LogType::INFO_LOG, "\tCPL Qualified Debug Store");
			if (bVirtualMachineExtensions)
				logManager->addLogMessage(LogType::INFO_LOG, "\tVirtual Machine Extensions");
			if (bEnhancedIntelSpeedStepTechnology)
				logManager->addLogMessage(LogType::INFO_LOG, "\tEnhanced Intel SpeedStep Technology");
			if (bThermalMonitor2)
				logManager->addLogMessage(LogType::INFO_LOG, "\tThermal Monitor 2");
			if (bSupplementalSSE3)
				logManager->addLogMessage(LogType::INFO_LOG, "\tSupplemental Streaming SIMD Extensions 3");
			if (bL1ContextID)
				logManager->addLogMessage(LogType::INFO_LOG, "\tL1 Context ID");
			if (bCMPXCHG16B)
				logManager->addLogMessage(LogType::INFO_LOG, "\tCMPXCHG16B Instruction");
			if (bxTPRUpdateControl)
				logManager->addLogMessage(LogType::INFO_LOG, "\txTPR Update Control");
			if (bPerfDebugCapabilityMSR)
				logManager->addLogMessage(LogType::INFO_LOG, "\tPerf\\Debug Capability MSR");
			if (bSSE41Extensions)
				logManager->addLogMessage(LogType::INFO_LOG, "\tSSE4.1 Extensions");
			if (bSSE42Extensions)
				logManager->addLogMessage(LogType::INFO_LOG, "\tSSE4.2 Extensions");
			if (bPOPCNT)
				logManager->addLogMessage(LogType::INFO_LOG, "\tPPOPCNT Instruction");
			if (bAVX)
				logManager->addLogMessage(LogType::INFO_LOG, "\tAVX Extensions");
			if (bAVX2)
				logManager->addLogMessage(LogType::INFO_LOG, "\tAVX2 Extensions");

			i = 0;
			nIds = 1;
			while (i < (sizeof(szCpuFeatures) / sizeof(const char*)))
			{
				if (nFeatureInfo & nIds)
				{
					logManager->addLogMessage(LogType::INFO_LOG, "\t%s", szCpuFeatures[i]);
				}

				nIds <<= 1;
				++i;
			}

			if (bLAHF_SAHFAvailable)
				logManager->addLogMessage(LogType::INFO_LOG, "\tLAHF/SAHF in 64-bit mode");
			if (bCmpLegacy)
				logManager->addLogMessage(LogType::INFO_LOG, "\tCore multi-processing legacy mode");
			if (bSVM)
				logManager->addLogMessage(LogType::INFO_LOG, "\tSecure Virtual Machine");
			if (bExtApicSpace)
				logManager->addLogMessage(LogType::INFO_LOG, "\tExtended APIC Register Space");
			if (bAltMovCr8)
				logManager->addLogMessage(LogType::INFO_LOG, "\tAltMovCr8");
			if (bLZCNT)
				logManager->addLogMessage(LogType::INFO_LOG, "\tLZCNT instruction");
			if (bSSE4A)
				logManager->addLogMessage(LogType::INFO_LOG, "\tSSE4A (EXTRQ, INSERTQ, MOVNTSD, MOVNTSS)");
			if (bMisalignedSSE)
				logManager->addLogMessage(LogType::INFO_LOG, "\tMisaligned SSE mode");
			if (bPREFETCH)
				logManager->addLogMessage(LogType::INFO_LOG, "\tPREFETCH and PREFETCHW Instructions");
			if (bSKINITandDEV)
				logManager->addLogMessage(LogType::INFO_LOG, "\tSKINIT and DEV support");
			if (bSYSCALL_SYSRETAvailable)
				logManager->addLogMessage(LogType::INFO_LOG, "\tSYSCALL/SYSRET in 64-bit mode");
			if (bExecuteDisableBitAvailable)
				logManager->addLogMessage(LogType::INFO_LOG, "\tExecute Disable Bit");
			if (bMMXExtensions)
				logManager->addLogMessage(LogType::INFO_LOG, "\tExtensions to MMX Instructions");
			if (bFFXSR)
				logManager->addLogMessage(LogType::INFO_LOG, "\tFFXSR");
			if (b1GBSupport)
				logManager->addLogMessage(LogType::INFO_LOG, "\t1GB page support");
			if (bRDTSCP)
				logManager->addLogMessage(LogType::INFO_LOG, "\tRDTSCP instruction");
			if (b64Available)
				logManager->addLogMessage(LogType::INFO_LOG, "\t64 bit Technology");
			if (b3DNowExt)
				logManager->addLogMessage(LogType::INFO_LOG, "\t3Dnow Ext");
			if (b3DNow)
				logManager->addLogMessage(LogType::INFO_LOG, "\t3Dnow! instructions");
			if (bNestedPaging)
				logManager->addLogMessage(LogType::INFO_LOG, "\tNested Paging");
			if (bLBRVisualization)
				logManager->addLogMessage(LogType::INFO_LOG, "\tLBR Visualization");
			if (bFP128)
				logManager->addLogMessage(LogType::INFO_LOG, "\tFP128 optimization");
			if (bMOVOptimization)
				logManager->addLogMessage(LogType::INFO_LOG, "\tMOVU Optimization");
		}
	}

	if (nExIds >= 0x80000004)
		logManager->addLogMessage(LogType::INFO_LOG, "\nCPU Brand String: %s", CPUBrandString);

	if (nExIds >= 0x80000006)
	{
		logManager->addLogMessage(LogType::INFO_LOG, "Cache Line Size = %d", nCacheLineSize);
		logManager->addLogMessage(LogType::INFO_LOG, "L2 Associativity = %d", nL2Associativity);
		logManager->addLogMessage(LogType::INFO_LOG, "Cache Size = %dK", nCacheSizeK);
	}

	for (i = 0;; i++)
	{
		__cpuidex(CPUInfo, 0x4, i);
		if (!(CPUInfo[0] & 0xf0)) break;

		if (i == 0)
		{
			nCores = CPUInfo[0] >> 26;
			logManager->addLogMessage(LogType::INFO_LOG, "Number of Cores = %d", nCores + 1);
		}

		nCacheType = (CPUInfo[0] & 0x1f);
		nCacheLevel = (CPUInfo[0] & 0xe0) >> 5;
		bSelfInit = (0 != ((CPUInfo[0] & 0x100) >> 8));
		bFullyAssociative = (0 != ((CPUInfo[0] & 0x200) >> 9));
		nMaxThread = (CPUInfo[0] & 0x03ffc000) >> 14;
		nSysLineSize = (CPUInfo[1] & 0x0fff);
		nPhysicalLinePartitions = (CPUInfo[1] & 0x03ff000) >> 12;
		nWaysAssociativity = (CPUInfo[1]) >> 22;
		nNumberSets = CPUInfo[2];

		//logManager->addLogMessage(LogType::INFO_LOG, "\n");

		logManager->addLogMessage(LogType::INFO_LOG, "ECX Index %d", i);
		switch (nCacheType)
		{
		case 0:
			logManager->addLogMessage(LogType::INFO_LOG, "   Type: Null");
			break;
		case 1:
			logManager->addLogMessage(LogType::INFO_LOG, "   Type: Data Cache");
			break;
		case 2:
			logManager->addLogMessage(LogType::INFO_LOG, "   Type: Instruction Cache");
			break;
		case 3:
			logManager->addLogMessage(LogType::INFO_LOG, "   Type: Unified Cache");
			break;
		default:
			logManager->addLogMessage(LogType::INFO_LOG, "   Type: Unknown");
		}

		logManager->addLogMessage(LogType::INFO_LOG, "   Level = %d", nCacheLevel + 1);
		if (bSelfInit)
		{
			logManager->addLogMessage(LogType::INFO_LOG, "   Self Initializing");
		}
		else
		{
			logManager->addLogMessage(LogType::INFO_LOG, "   Not Self Initializing");
		}
		if (bFullyAssociative)
		{
			logManager->addLogMessage(LogType::INFO_LOG, "   Is Fully Associatve");
		}
		else
		{
			logManager->addLogMessage(LogType::INFO_LOG, "   Is Not Fully Associatve");
		}
		logManager->addLogMessage(LogType::INFO_LOG, "   Max Threads = %d", nMaxThread + 1);
		logManager->addLogMessage(LogType::INFO_LOG, "   System Line Size = %d", nSysLineSize + 1);
		logManager->addLogMessage(LogType::INFO_LOG, "   Physical Line Partions = %d", nPhysicalLinePartitions + 1);
		logManager->addLogMessage(LogType::INFO_LOG, "   Ways of Associativity = %d", nWaysAssociativity + 1);
		logManager->addLogMessage(LogType::INFO_LOG, "   Number of Sets = %d",nNumberSets + 1);
	}
}


}

