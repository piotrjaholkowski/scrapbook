#pragma once

struct Color
{
public:
	uint8_t r;
	uint8_t g;
	uint8_t b;
};