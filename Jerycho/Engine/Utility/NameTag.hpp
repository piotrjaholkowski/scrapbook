#pragma once

#include "../jaSetup.hpp"
#include "../Math/Math.hpp"
#include <string>


namespace ja
{
	template <int32_t nameLengthT>
	class NameTag
	{
	private:
		uint32_t hash;
		char     name[nameLengthT];
	public:

		uint32_t getHash() const
		{
			return this->hash;
		}

		void setName(const char* name)
		{
			size_t nameLength = strlen(name) + 1;

			if (nameLength > nameLengthT)
			{
				nameLength = nameLengthT;
				memcpy(this->name, name, nameLength);
				this->name[nameLength - 1] = '\0';
			}
			else
			{
				memcpy(this->name, name, nameLength);
			}

			this->hash = ja::math::convertToHash(this->name);
		}

		static uint32_t getHash(const char* name)
		{
			return ja::math::convertToHash(name);
		}

		const char* getName() const
		{
			return this->name;
		}
	};

	typedef NameTag<setup::TAG_NAME_LENGTH> TagName;
}