#pragma once

#include "../Allocators/FastAllocatorSTL.hpp"
#include <sstream>

namespace ja
{
	//typedef std::basic_stringstream<char, std::char_traits<char>, FastAllocatorSTL<char>> stringstream;
	typedef std::basic_stringstream<char, std::char_traits<char>, std::allocator<char>> stringstream;
}

 