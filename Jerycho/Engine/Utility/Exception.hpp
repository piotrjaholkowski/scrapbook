#pragma once

#include "String.hpp"
#include <iostream>

#define JA_EXCEPTION(c) ja::Exception(c, __FUNCTION__ ,__LINE__, __FILE__)
#define JA_EXCEPTIONR(c,r) ja::Exception(c, r, __FUNCTION__ ,__LINE__, __FILE__)
#define JA_CATCH_EXCEPTION catch(ja::Exception ex){ex.Msg();system("pause");terminate();}

namespace ja
{

	enum class ExceptionType
	{
		OUT_OF_MEMORY = 1001,
		FILE_NOT_FOUND = 1002,
		CANT_LOAD_FONT = 1003,
		CANT_LOAD_TEXTURE = 1004,
		CANT_LOAD_ANIMATION = 1005,
		CANT_LOAD_SHADER = 1006,
		CANT_LOAD_SOUND_EFFECT = 1007,
		CANT_LOAD_SCRIPT = 1008,
		CANT_LOAD_CUSTOM_DATA = 1009,
		GUI = 1010,
		CANT_LOAD_MODEL3D = 1011,
		TEST = 1012,
		CANT_LOAD_VIDEO_STREAM = 1013,
		CANT_LOAD_SOUND_STREAM = 1014,
		CANT_LOAD_LEVEL_RESOURCE = 1015
	};

	class Exception
	{
	public:
		ExceptionType exceptionType;
		int32_t       lineNumber;
		ja::string    fileName;
		ja::string    functionName;
		ja::string    reason;

		Exception(ExceptionType exceptionType, ja::string functionName, int lineNumber, ja::string fileName)
		{
			this->exceptionType = exceptionType;
			this->lineNumber = lineNumber;
			this->fileName = fileName;
			this->functionName = functionName;
		}

		Exception(ExceptionType exceptionType, ja::string reason, ja::string functionName, int lineNumber, ja::string fileName)
		{
			this->exceptionType = exceptionType;
			this->lineNumber = lineNumber;
			this->fileName = fileName;
			this->functionName = functionName;
			this->reason = reason;
		}

		/*ja::string getMessage()
		{
		return "E:" + itoa(static_cast<int>(exceptionType)) + "L:" + itoa(static_cast<int>(lineNumber)) + "FN:" + fileName + "F:" + functionName + "R: " + reason;
		}*/
	};

}

