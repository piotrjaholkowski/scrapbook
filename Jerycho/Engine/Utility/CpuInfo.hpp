#pragma once

#include <intrin.h>
#include <stdint.h>

#include <string>

namespace ja
{
	static const char* szCpuFeatures[] = {
		"x87 FPU On Chip",
		"Virtual-8086 Mode Enhancement",
		"Debugging Extensions",
		"Page Size Extensions",
		"Time Stamp Counter",
		"RDMSR and WRMSR Support",
		"Physical Address Extensions",
		"Machine Check Exception",
		"CMPXCHG8B Instruction",
		"APIC On Chip",
		"Unknown1",
		"SYSENTER and SYSEXIT",
		"Memory Type Range Registers",
		"PTE Global Bit",
		"Machine Check Architecture",
		"Conditional Move/Compare Instruction",
		"Page Attribute Table",
		"36-bit Page Size Extension",
		"Processor Serial Number",
		"CFLUSH Extension",
		"Unknown2",
		"Debug Store",
		"Thermal Monitor and Clock Ctrl",
		"MMX Technology",
		"FXSAVE/FXRSTOR",
		"SSE Extensions",
		"SSE2 Extensions",
		"Self Snoop",
		"Multithreading Technology",
		"Thermal Monitor",
		"Unknown4",
		"Pending Break Enable"
	};

	class CpuInfo
	{
	public:

		char    CPUString[0x20];
		char    CPUBrandString[0x40];
		int32_t CPUInfo[4];
		int32_t nSteppingID = 0;
		int32_t nModel = 0;
		int32_t nFamily = 0;
		int32_t nProcessorType = 0;
		int32_t nExtendedmodel = 0;
		int32_t nExtendedfamily = 0;
		int32_t nBrandIndex = 0;
		int32_t nCLFLUSHcachelinesize = 0;
		int32_t nLogicalProcessors = 0;
		int32_t nAPICPhysicalID = 0;
		int32_t nFeatureInfo = 0;
		int32_t nCacheLineSize = 0;
		int32_t nL2Associativity = 0;
		int32_t nCacheSizeK = 0;
		int32_t nPhysicalAddress = 0;
		int32_t nVirtualAddress = 0;
		int32_t nRet = 0;

		int32_t nCores = 0;
		int32_t nCacheType = 0;
		int32_t nCacheLevel = 0;
		int32_t nMaxThread = 0;
		int32_t nSysLineSize = 0;
		int32_t nPhysicalLinePartitions = 0;
		int32_t nWaysAssociativity = 0;
		int32_t nNumberSets = 0;

		uint32_t nIds, nExIds, i;

		bool    bSSE3Instructions = false;
		bool    bMONITOR_MWAIT = false;
		bool    bCPLQualifiedDebugStore = false;
		bool    bVirtualMachineExtensions = false;
		bool    bEnhancedIntelSpeedStepTechnology = false;
		bool    bThermalMonitor2 = false;
		bool    bSupplementalSSE3 = false;
		bool    bL1ContextID = false;
		bool    bCMPXCHG16B = false;
		bool    bxTPRUpdateControl = false;
		bool    bPerfDebugCapabilityMSR = false;
		bool    bSSE41Extensions = false;
		bool    bSSE42Extensions = false;
		bool    bPOPCNT = false;
		bool    bAVX = false;

		bool    bAVX2 = false;

		bool    bMultithreading = false;

		bool    bLAHF_SAHFAvailable = false;
		bool    bCmpLegacy = false;
		bool    bSVM = false;
		bool    bExtApicSpace = false;
		bool    bAltMovCr8 = false;
		bool    bLZCNT = false;
		bool    bSSE4A = false;
		bool    bMisalignedSSE = false;
		bool    bPREFETCH = false;
		bool    bSKINITandDEV = false;
		bool    bSYSCALL_SYSRETAvailable = false;
		bool    bExecuteDisableBitAvailable = false;
		bool    bMMXExtensions = false;
		bool    bFFXSR = false;
		bool    b1GBSupport = false;
		bool    bRDTSCP = false;
		bool    b64Available = false;
		bool    b3DNowExt = false;
		bool    b3DNow = false;
		bool    bNestedPaging = false;
		bool    bLBRVisualization = false;
		bool    bFP128 = false;
		bool    bMOVOptimization = false;

		bool    bSelfInit = false;
		bool    bFullyAssociative = false;

	private:
		CpuInfo();	

	public:
		void printInfo();
		void logInfo();

		static inline CpuInfo* getSingleton()
		{
			static CpuInfo cpuInfo;
			return &cpuInfo;
		}
	};
}