#pragma once

namespace ja
{
	template<class T>
	class ListElement
	{
	public:
		T data;
		ListElement* previousElement;
		ListElement* nextElement;

		ListElement() : nextElement(nullptr), previousElement(nullptr)
		{

		}

		void pushBefore(ListElement<T>* newElement)
		{
			if (previousElement != nullptr)
			{
				previousElement->nextElement = newElement;
				newElement->previousElement  = previousElement;
				previousElement              = newElement;
			}
			else
			{
				previousElement = newElement;
			}

			newElement->nextElement = this;
		}

		void pushAfter(ListElement<T>* newElement)
		{
			if (nextElement != nullptr)
			{
				nextElement->previousElement = newElement;
				newElement->nextElement      = nextElement;
				nextElement                  = newElement;
			}
			else
			{
				nextElement = newElement;
			}

			newElement->previousElement = this;
		}
	};
}

