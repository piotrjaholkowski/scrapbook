#pragma once

#include "../Allocators/FastAllocatorSTL.hpp"
#include <string>

#include "emmintrin.h"
#include "immintrin.h"

namespace ja
{
	//typedef std::basic_string<char, std::char_traits<char>, FastAllocatorSTL<char> >          string;
	//typedef std::basic_string<wchar_t, std::char_traits<wchar_t>, FastAllocatorSTL<wchar_t> > wstring;
	typedef std::basic_string<char, std::char_traits<char>, std::allocator<char> >          string;
	typedef std::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > wstring;

	inline const char* getBoolString(bool value)
	{
		return value ? "true" : "false";
	}

	inline const char *getBinaryCode(int32_t x)
	{
		static char b[33];
		b[32] = '\0';

		uint32_t z = 0x80000000;
		uint8_t  i = 0;

		for (z; z > 0; z >>= 1)
		{
			b[i++] = (z & x) ? '1' : '0';
		}

		return b;
	}

	inline void replaceChar(char* src, char toReplace, char replaceWith)
	{
		uint32_t strLength = strlen(src);

		for (uint32_t i = 0; i < strLength; ++i)
		{
			if (src[i] == toReplace)
				src[i] = replaceWith;
		}
	}

	inline bool getDelimSeparatedString(const char* src, char mark, char* dest, int32_t& nextStringPos)
	{
		nextStringPos = 0;

		while (*src != '\0')
		{
			if (*src == mark)
			{
				dest[nextStringPos++] = '\0';
				return true;
			}

			dest[nextStringPos] = *src;

			++src;
			++nextStringPos;
		}

		dest[nextStringPos] = '\0';
		return false;
	}

	inline void memcpyUint16ArrayWithOffset_SSE2(uint16_t* dstIndices, const uint16_t* srcIndices, uint32_t dataCount, uint16_t dataOffset)
	{
		const uint32_t dataIndexCount  = dataCount / 8;
		const uint32_t dataIndexCount2 = dataCount % 8;

		const uint16_t  addressOffset      = 8 * dataIndexCount;
		uint16_t*       const afterSIMDDst = dstIndices + addressOffset;
		const uint16_t* const afterSIMDSrc = srcIndices + addressOffset;

		__m128i* dstIndices128 = (__m128i*)dstIndices;
		__m128i* srcIndices128 = (__m128i*)srcIndices;

		register const __m128i addOffset = _mm_set1_epi16(dataOffset);

		for (uint32_t dataIndex = 0; dataIndex < dataIndexCount; ++dataIndex)
		{
			dstIndices128[dataIndex] = _mm_add_epi16(srcIndices128[dataIndex], addOffset);
		}

		for (uint32_t dataIndex = 0; dataIndex < dataIndexCount2; ++dataIndex)
		{
			afterSIMDDst[dataIndex] = afterSIMDSrc[dataIndex] + dataOffset;
		}
	}

	inline void memcpyUint32ArrayWithOffset_SSE2(uint32_t* const dstIndices, const uint32_t* const srcIndices, const uint32_t dataCount, const uint32_t dataOffset)
	{	
		{
			register __m128i*       const dstIndices128   = (__m128i*)dstIndices;
			register const __m128i* const srcIndices128   = (__m128i*)srcIndices;
			register const __m128i        addOffset       = _mm_set1_epi32(dataOffset);
			register const uint32_t       dataIndexCount  = dataCount / 4;

			for (register uint32_t dataIndex = 0; dataIndex < dataIndexCount; ++dataIndex)
			{
				dstIndices128[dataIndex] = _mm_add_epi32(srcIndices128[dataIndex], addOffset);
			}
		}		

		{
			register const uint32_t  addressOffset = 4 * (dataCount / 4);
			register uint32_t*       const afterSIMDDst = dstIndices + addressOffset;
			register const uint32_t* const afterSIMDSrc = srcIndices + addressOffset;
			register const uint32_t dataIndexCount2 = dataCount % 4;
			

			for (register uint32_t dataIndex = 0; dataIndex < dataIndexCount2; ++dataIndex)
			{
				afterSIMDDst[dataIndex] = afterSIMDSrc[dataIndex] + dataOffset;
			}
		}
		
	}

	inline void addUint16ArrayOffset_SSE2(uint16_t* dstIndices, uint32_t indicesNum, uint16_t indicesOffset)
	{
		const uint32_t dataIndexCount  = indicesNum / 8;
		const uint32_t dataIndexCount2 = indicesNum % 8;

		const uint32_t  addressOffset = 8 * dataIndexCount;
		uint16_t*       const afterSIMDDst = dstIndices + addressOffset;

		__m128i* dstIndices128 = (__m128i*)dstIndices;

		register const __m128i addOffset = _mm_set1_epi16(indicesOffset);

		for (uint32_t dataIndex = 0; dataIndex < dataIndexCount; ++dataIndex)
		{
			dstIndices128[dataIndex] = _mm_add_epi16(dstIndices128[dataIndex], addOffset);
		}

		for (uint32_t dataIndex = 0; dataIndex < dataIndexCount2; ++dataIndex)
		{
			afterSIMDDst[dataIndex] = afterSIMDDst[dataIndex] + indicesOffset;
		}
	}

	inline void addUint32ArrayOffset_SSE2(uint32_t* dstIndices, uint32_t indicesNum, uint32_t indicesOffset)
	{
		const uint32_t dataIndexCount  = indicesNum / 4;
		const uint32_t dataIndexCount2 = indicesNum % 4;

		const uint32_t addressOffset      = 4 * dataIndexCount;
		uint32_t*      const afterSIMDDst = dstIndices + addressOffset;

		__m128i* dstIndices128 = (__m128i*)dstIndices;

		register const __m128i addOffset = _mm_set1_epi32(indicesOffset);

		for (uint32_t dataIndex = 0; dataIndex < dataIndexCount; ++dataIndex)
		{
			dstIndices128[dataIndex] = _mm_add_epi32(dstIndices128[dataIndex], addOffset);
		}

		for (uint32_t dataIndex = 0; dataIndex < dataIndexCount2; ++dataIndex)
		{
			afterSIMDDst[dataIndex] = afterSIMDDst[dataIndex] + indicesOffset;
		}
	}

	inline void memsetInt32_SSE2_aligned16(int32_t * dstData, int32_t val, uint32_t dataCount)
	{
		register int32_t* const firstLastAddress  = dstData +  (4 * (dataCount / 4));
		
		register const __m128i intBroadcast = _mm_set1_epi32(val);
		
		for (register int32_t* dstDataIt = dstData; dstDataIt < firstLastAddress; dstDataIt += 4)
		{
			_mm_stream_si128((__m128i*)dstDataIt, intBroadcast);
		}
		
		register int32_t* const secondLastAddress = dstData + dataCount;

		for (register int32_t* dstDataIt = firstLastAddress; dstDataIt < secondLastAddress; ++dstDataIt)
		{
			*dstDataIt = val;
		}
		
	}

	inline void memsetInt32_SSE2(int32_t * const dstData, const int32_t val, uint32_t dataCount)
	{	
		const uint32_t toAlign16Bytes = ((uintptr_t)16 - ((uintptr_t)dstData % 16));//(uintptr_t)dstData % 16;

		{
			register const uint32_t toAlign16Ints = toAlign16Bytes / sizeof(uint32_t);
			register const int32_t  intVal = val;

			for (register uint32_t i = 0; i < toAlign16Ints; ++i)
			{
				dstData[i] = intVal;
			}

			dataCount -= toAlign16Ints;
		}	

		int32_t* const dstDataAfterAlignTo16 = (int32_t*)((int8_t*)dstData + toAlign16Bytes);
		int32_t* const lastSIMDAddress       = dstDataAfterAlignTo16 + (4 * (dataCount / 4));

		{
			register const __m128i        intBroadcast = _mm_set1_epi32(val);
			register const int32_t* const lastAddress  = lastSIMDAddress;

			for (register int32_t* dstDataIt = dstDataAfterAlignTo16; dstDataIt < lastAddress; dstDataIt += 4)
			{
				_mm_stream_si128((__m128i*)dstDataIt, intBroadcast);
			}
		}		
		
		{
			register const int32_t* const lastAddress = dstDataAfterAlignTo16 + dataCount;
			register const int32_t  intVal = val;

			for (register int32_t* dstDataIt = lastSIMDAddress; dstDataIt < lastAddress; ++dstDataIt)
			{
				*dstDataIt = intVal;
			}
		}	
	}

	inline void memcpyFloat4_SSE2_aligned16(float* const dstData, const float* const srcData, const uint32_t dataCount)
	{
		register const __m128   floatBroadcast = _mm_set_ps(srcData[3], srcData[2], srcData[1], srcData[0]);
		register const uint32_t itNum          = dataCount * 4;

		for (register uint32_t dataIt = 0; dataIt < itNum; dataIt += 4)
		{
			_mm_stream_ps(&dstData[dataIt], floatBroadcast);
		}	
	}

	inline void memsetInt32(register int32_t* dstData, register const int32_t val, register const uint32_t dataCount)
	{
		for (register uint32_t i = 0; i < dataCount; ++i)
		{
			dstData[i] = val;
		}
	}

	inline void memsetInt32_AVX_aligned32(int32_t * dstData, int32_t val, uint32_t dataCount)
	{
		int32_t* const firstLastAddress = dstData + (8 * (dataCount / 8));
		int32_t* const secondLastAddress = dstData + dataCount;

		const __m256i intBroadcast = _mm256_set1_epi32(val);

		for (int32_t* dstDataIt = dstData; dstDataIt < firstLastAddress; dstDataIt += 8)
		{
			_mm256_stream_si256((__m256i*)dstDataIt, intBroadcast);
		}

		for (int32_t* dstDataIt = firstLastAddress; dstDataIt < secondLastAddress; ++dstDataIt)
		{
			*dstDataIt = val;
		}
	}

}
