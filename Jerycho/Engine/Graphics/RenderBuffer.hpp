#pragma once

#include "../Allocators/StackAllocator.hpp"
#include "../Math/VectorMath2d.hpp"
#include "../Graphics/Color4f.hpp"
#include "../Graphics/ShaderBinder.hpp"
#include "../Graphics/Material.hpp"

namespace ja
{

class RenderCommand
{
public:

private:
	const Material* material;
	uint32_t        objectsNumPerRenderCommand;
	jaMatrix44      viewProjectionMatrix;
	uint32_t        indexRangeStart;
	uint32_t        indicesNum; 

	RenderCommand() : objectsNumPerRenderCommand(0)
	{

	}

	RenderCommand(const Material* material) : material(material)
	{

	}

public:

	inline uint32_t getObjectsNum() const
	{
		return this->objectsNumPerRenderCommand;
	}

	friend class RenderBuffer;
};

class RenderBuffer
{
public:
	RenderCommand* currentRenderCommand;
	uint32_t       maxObjectsPerRenderCommand;
	uint32_t       totalIndicesNum;
	uint32_t       totalVerticesNum;

	StackAllocator<jaVector2>     verticesPositions;
	StackAllocator<Color4f>       colors;
	StackAllocator<jaVector4>     normals;
	StackAllocator<Color4f>       secondaryColors;
	StackAllocator<uint32_t>      indices;

	StackAllocator<jaMatrix44>    modelMatrices; // per object
	StackAllocator<jaMatrix44>    rotMatrices;   // per object
	
	StackAllocator<int32_t>       materialIds;
	StackAllocator<int32_t>       objectIds;

	StackAllocator<uint8_t>       shaderObjectData;

	StackAllocator<jaMatrix44>    mvpMatrices; // per object

	

	StackAllocator<RenderCommand> renderCommands;

	uint32_t vertexArrayID;
	uint32_t vertexPositionBuffer; // layout 0
	uint32_t colorBuffer;          // layout 1
	uint32_t normalBuffer;         // layout 2
	uint32_t secondaryColorBuffer; // layout 3
	uint32_t materialIdBuffer;     // layout 4
	uint32_t objectIdBuffer;       // layout 5
	uint32_t indicesBuffer;
	uint32_t rotMatricesBuffer;
	uint32_t mvpMatrixBuffer;

	RenderBuffer(uint32_t maxObjectsPerRenderCommand);

public:

	RenderCommand* addRenderCommand(const jaMatrix44& viewProjectionMatrix, const Material* material);
	RenderCommand* addRenderCommand(const Material* material);

	~RenderBuffer();
	void applyViewMatricesToMvpMatrices();

	inline RenderCommand* getCurrentRenderCommand()
	{
		return this->currentRenderCommand;
	}

	inline uint32_t getTotalVerticesNum() const
	{
		return this->totalVerticesNum;
	}

	inline void addVertices(uint32_t verticesNum)
	{
		this->totalVerticesNum += verticesNum;
	}

	inline void addIndices(uint32_t indicesNum)
	{
		this->currentRenderCommand->indicesNum += indicesNum;
		this->totalIndicesNum += indicesNum;
	}

	inline void addObject()
	{
		++this->currentRenderCommand->objectsNumPerRenderCommand;
	}

	inline bool checkIfCurrentRenderCommandCanBeUsedWithMaterial(const Material* material)
	{
		const Material* currentMaterial = currentRenderCommand->material;

		if (currentMaterial->getShaderBinder() != material->getShaderBinder())
			return false;

		if (currentMaterial->blendEquation != material->blendEquation)
			return false;

		if ((currentMaterial->sourceFactor != material->sourceFactor) || (currentMaterial->destinationFactor != material->destinationFactor))
			return false;

		return true;
	}

	inline bool checkIfNewRenderCommandNeeded(const Material* material)
	{
		if (currentRenderCommand->objectsNumPerRenderCommand >= this->maxObjectsPerRenderCommand)
		{
			return true;
		}

		if (currentRenderCommand->material != material)
		{
			return !checkIfCurrentRenderCommandCanBeUsedWithMaterial(material);
		}

		return false;
	}

	void render();
	void sendDataToGPU();
	void sendMaterialsToGPU();

private:
	void bindBuffers();
	void unbindBuffers();
public:

	void clear();
};

}