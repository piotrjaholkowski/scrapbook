#pragma once

#include "Color4f.hpp"
#include "DrawElements.hpp"
#include "ShaderObjectData.hpp"
#include "Material.hpp"
#include "LayerObjectMorph.hpp"
#include "../Math/VectorMath2d.hpp"
#include "../Utility/NameTag.hpp"

#include "Polygon.hpp"
#include "Camera.hpp"

#include <stdint.h>

namespace ja
{
	class LayerObjectManager;
	class PolygonMorphElements;

	class LayerObjectDef
	{
	public:
		uint8_t  layerObjectType;
		uint8_t  layerId;
		uint16_t depth;

		Color4f color;

		jaTransform2 transform;
		jaVector2    scale;

		DrawElements*         drawElements;
		ShaderObjectData*     shaderObjectData;
		Material*             material;
		PolygonMorphElements* polygonMorphElements;
		CameraMorphElements*  cameraMorphElements;

		uint16_t polygonFlag;

		uint8_t lightType;

		TagName cameraName;

		LayerObjectDef();

		void init(LayerObjectManager* layerObjectManager);

		~LayerObjectDef();
	};
}