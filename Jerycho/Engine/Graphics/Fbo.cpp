#include "Fbo.hpp"
#include "../Manager/LogManager.hpp"

namespace ja
{ 

Fbo::Fbo()
{

	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glGenFramebuffers(1, &fboHandle); // Generate one frame buffer and store the ID in fbo  
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, fboHandle); // Bind our frame buffer  

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER_EXT);
	if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
	{
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Could not create frame buffer error: %d", status);
	}

	glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0); // Unbind our frame buffer

	return;
}

Fbo::~Fbo()
{
	//Bind 0, which means render to back buffer, as a result, fb is unbound
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);
	glDeleteFramebuffers(1, &fboHandle);
	
}

void Fbo::bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, fboHandle);
}

void Fbo::unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER_EXT, 0);
}

}