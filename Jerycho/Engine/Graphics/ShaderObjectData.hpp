#pragma once

#include "../Graphics/ShaderStructureDef.hpp"

namespace ja
{


class ShaderObjectData
{
private:
	void*               shaderObjectData;
	uint32_t            shaderObjectSize;
	ShaderStructureDef* shaderObjectDef;
	CacheLineAllocator* const pAllocator;
public:

	ShaderObjectData(CacheLineAllocator* pAllocator) : pAllocator(pAllocator), shaderObjectData(nullptr), shaderObjectSize(0)
	{

	}

	inline uint32_t getSize() const
	{
		return this->shaderObjectSize;
	}

	inline void* getData() const
	{
		return this->shaderObjectData;
	}

	inline ShaderStructureDef* getShaderObjectDef() const
	{
		return this->shaderObjectDef;
	}

	inline void setShaderBinder(ShaderBinder* shaderBinder)
	{
		this->shaderObjectDef= shaderBinder->getShaderObjectDef();

		if (this->shaderObjectSize != this->shaderObjectDef->getShaderStructureSize())
		{
			this->pAllocator->free(this->shaderObjectData, this->shaderObjectSize);
			this->shaderObjectSize = shaderObjectDef->getShaderStructureSize();
			this->shaderObjectData = this->pAllocator->allocate(this->shaderObjectSize);
		}
	}

	inline void copy(ShaderObjectData& shaderObjectData)
	{
		if (this->shaderObjectSize != shaderObjectData.shaderObjectSize)
		{
			pAllocator->free(this->shaderObjectData, this->shaderObjectSize);
			this->shaderObjectSize = shaderObjectData.shaderObjectSize;
			this->shaderObjectData = pAllocator->allocate(this->shaderObjectSize);
		}

		this->shaderObjectDef = shaderObjectData.shaderObjectDef;

		memcpy(this->shaderObjectData, shaderObjectData.shaderObjectData, this->shaderObjectSize);
	}

	void freeAllocatedSpace()
	{
		pAllocator->free(shaderObjectData, shaderObjectSize);
	}
};

}