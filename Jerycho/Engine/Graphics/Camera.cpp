#include "Camera.hpp"
#include "LayerObjectDef.hpp"

#include "../Manager/GameManager.hpp"
#include "../Manager/EffectManager.hpp"
#include "../Manager/LayerObjectManager.hpp"

namespace ja
{

CameraMorphElements::CameraMorphElements(CacheLineAllocator* pAllocator) : MorphElements(pAllocator)
{

}

LayerObjectMorph* CameraMorphElements::createMorph(const char* morphName)
{
	CameraMorph* cameraMorph = new ((CameraMorph*)pAllocator->allocate(sizeof(CameraMorph))) CameraMorph(morphName, this->pAllocator);

	addMorph(cameraMorph);

	return cameraMorph;
}

LayerObjectMorph* CameraMorphElements::createMorph(uint32_t hash)
{
	CameraMorph* cameraMorph = new ((CameraMorph*)pAllocator->allocate(sizeof(CameraMorph))) CameraMorph(hash, this->pAllocator);

	addMorph(cameraMorph);

	return cameraMorph;
}

CameraMorph::CameraMorph(const char* morphName, CacheLineAllocator* pAllocator) : LayerObjectMorph(morphName)
{

}

CameraMorph::CameraMorph(uint32_t hash, CacheLineAllocator* pAllocator) : LayerObjectMorph(hash)
{

}

void CameraMorph::setMorph(LayerObject* layerObject)
{
	Camera* camera = (Camera*)layerObject;

	this->scale     = camera->scale;
	this->transform = camera->getHandle()->getTransform();

	//this->drawElements.copy(polygon->drawElements);
}

void* CameraMorph::copy(CacheLineAllocator* pAllocator)
{
	CameraMorph* cameraMorph = new ((CameraMorph*)pAllocator->allocate(sizeof(CameraMorph))) CameraMorph(getHash(), pAllocator);

	cameraMorph->scale     = this->scale;
	cameraMorph->transform = this->transform;

	cameraMorph->activeFeatures = this->activeFeatures;

	return cameraMorph;
}

uint8_t CameraMorph::getFeaturesNum() const
{
	return 3;
}

const char* CameraMorph::getFeatureName(uint8_t featureId) const
{
	switch (featureId)
	{
	case (uint8_t)PolygonMorphFeature::TRANSLATE:
		return "Translate";
		break;
	case (uint8_t)PolygonMorphFeature::ROTATE:
		return "Rotate";
		break;
	case (uint8_t)PolygonMorphFeature::SCALE:
		return "Scale";
		break;
	}

	return "Undefined";
}

uint32_t CameraMorph::getSizeOf() const
{
	return sizeof(CameraMorph);
}

void CameraMorph::freeAllocatedSpace()
{
	//this->drawElements.freeAllocatedSpace();
}

Camera::Camera(gil::ObjectHandle2d* handle, uint8_t* layerObjectFlag, CacheLineAllocator* pAllocator) : LayerObject((uint8_t)LayerObjectType::CAMERA, handle, layerObjectFlag), morphElements(pAllocator)
{

}

void Camera::morph(LayerObjectMorph* morphStart, LayerObjectMorph* morphEnd, float morph)
{
	CameraMorph* cameraMorphStart = (CameraMorph*)morphStart;
	CameraMorph* cameraMorphEnd   = (CameraMorph*)morphEnd;

	if (cameraMorphStart->isFeatureActive((uint8_t)CameraMorphFeature::TRANSLATE))
	{
		jaVector2 translateRel = cameraMorphEnd->transform.position - cameraMorphStart->transform.position;
		//this->handle->transform.position = polygonMorphStart->transform.position + (morph * translateRel);
		const jaVector2 endTranslate = cameraMorphStart->transform.position + (morph * translateRel);
		setPosition(endTranslate);
	}

	if (cameraMorphStart->isFeatureActive((uint8_t)CameraMorphFeature::ROTATE))
	{
		float rotateRel = cameraMorphEnd->transform.rotationMatrix.getRadians() - cameraMorphStart->transform.rotationMatrix.getRadians();
		//this->handle->transform.rotationMatrix = polygonMorphStart->transform.rotationMatrix * jaMatrix2(rotateRel * morph);
		const jaMatrix2 rotMat = cameraMorphStart->transform.rotationMatrix * jaMatrix2(rotateRel * morph);
		setAngleRot(rotMat);
	}

	if (cameraMorphStart->isFeatureActive((uint8_t)CameraMorphFeature::SCALE))
	{
		jaVector2 scaleRel = cameraMorphEnd->scale - cameraMorphStart->scale;
		jaVector2 scale    = cameraMorphStart->scale + (morph * scaleRel);
		setScale(scale);
	}

}

void Camera::getBoundingBox(gil::AABB2d* boundingBox)
{

}

LayerObject* Camera::createCamera(CacheLineAllocator* layerObjectAllocator, const LayerObjectDef& layerObjectDef, LayerObjectManager* layerObjectManager)
{
	//jaFloat maxScale = std::max(abs(layerObjectDef.scale.x), abs(layerObjectDef.scale.y));

	gil::Scene2d* scene2d = layerObjectManager->getScene();

	int32_t widthRes,    heightRes;
	jaFloat camWidthRes, camHeightRes;
	DisplayManager::getResolution(&widthRes, &heightRes);
	const jaFloat pixelsPerUnit = (jaFloat)setup::graphics::PIXELS_PER_UNIT;
	camWidthRes  = ((jaFloat)widthRes  / pixelsPerUnit);
	camHeightRes = ((jaFloat)heightRes / pixelsPerUnit);

	camWidthRes  *= layerObjectDef.scale.x;
	camHeightRes *= layerObjectDef.scale.y;

	//gil::Circle2d        circleShape(maxScale * layerObjectDef.drawElements->getBoundingVolumeRadius());
	//gil::ObjectHandle2d* handle = scene2d->createDynamicObject(&circleShape, layerObjectDef.transform);

	gil::Box2d           boxShape = gil::Box2d(camWidthRes, camHeightRes);
	gil::ObjectHandle2d* handle   = scene2d->createDynamicObject(&boxShape, layerObjectDef.transform);

	uint8_t* layerObjectFlag = layerObjectManager->getLayerObjectFlag(handle->id);

	Camera* camera = new (layerObjectAllocator->allocate(sizeof(Camera))) Camera(handle, layerObjectFlag, layerObjectManager->getDrawElementAllocator());

	//GameManager::getSingleton()->addCamera(camera);
	
	camera->setLayerObjectDef(layerObjectDef, false);
	//camera->morphElements.copy(*layerObjectDef.cameraMorphElements);

	handle->setUserData(camera);


	return camera;
}

void Camera::setLayerObjectDef(const LayerObjectDef& layerObjectDef, bool updateScene)
{
	gil::ObjectHandle2d* handle = getHandle();

	handle->transform = layerObjectDef.transform;

	this->layerId       = layerObjectDef.layerId;;
	this->scale         = layerObjectDef.scale;
	this->cameraScale.x = 1.0f / layerObjectDef.scale.x;
	this->cameraScale.y = 1.0f / layerObjectDef.scale.y;
	this->depth         = layerObjectDef.depth;

	this->morphElements.copy(*layerObjectDef.cameraMorphElements);

	//gil::Circle2d* circle = (gil::Circle2d*)handle->getShape();

	//jaFloat maxScale = std::max(abs(layerObjectDef.scale.x), abs(layerObjectDef.scale.y));
	//circle->radius = maxScale * layerObjectDef.drawElements->getBoundingVolumeRadius();

	int32_t widthRes,    heightRes;
	jaFloat camWidthRes, camHeightRes;
	DisplayManager::getResolution(&widthRes, &heightRes);
	const jaFloat pixelsPerUnit = (jaFloat)setup::graphics::PIXELS_PER_UNIT;
	camWidthRes  = ((jaFloat)widthRes  / pixelsPerUnit);
	camHeightRes = ((jaFloat)heightRes / pixelsPerUnit);

	camWidthRes  *= layerObjectDef.scale.x;
	camHeightRes *= layerObjectDef.scale.y;

	gil::Box2d* box = (gil::Box2d*)handle->getShape();
	box->setWidth(camWidthRes);
	box->setHeight(camHeightRes);

	cameraName.setName(layerObjectDef.cameraName.getName());

	if (updateScene)
		updateTransform();
}

void Camera::draw()
{
	*this->layerObjectFlag |= (uint8_t)LayerObjectFlag::RENDERED_OBJECT;

	if (!GameManager::getSingleton()->renderDebug)
		return;
	//if (nullptr != material)
	//	material->setShaderProperties();

	glPushMatrix();
	//glColor4f(this->color.r, this->color.g, this->color.b, this->color.a); set as blending in shader properties

	//glTranslatef(handle->transform.position.x, handle->transform.position.y, 0.0f);
	//glRotatef(handle->transform.rotationMatrix.getDegrees(), 0.0f, 0.0f, 1.0f);
	//glScalef(scale.x, scale.y, 1.0f);

	jaMatrix44 modelMatrix;
	//float rotMatrix[16];

	//jaVectormath2::translateToMatrix4(handle->transform.position, modelMatrix);
	//glMultMatrixf(modelMatrix);
	//
	//jaVectormath2::matrix2toMatrix4(handle->transform.rotationMatrix, modelMatrix);
	//glMultMatrixf(modelMatrix);
	//jaVectormath2::rotScale2toMatrix4(handle->transform.rotationMatrix, scale, modelMatrix);
	const gil::ObjectHandle2d* handle = getHandle();

	jaVectormath2::setTranslateRotScaleMatrix44(handle->transform.position, handle->transform.rotationMatrix, this->scale, modelMatrix);
	glMultMatrixf(modelMatrix.element);


	//gil::Box2d* box  = (gil::Box2d*)handle->getShape();
	//float halfWidth  = box->getHalfWidth();
	//float halfHeight = box->getHalfHeight();
	int32_t widthRes, heightRes;
	jaFloat camWidthRes, camHeightRes;
	DisplayManager::getResolution(&widthRes, &heightRes);
	const jaFloat pixelsPerUnit = (jaFloat)setup::graphics::PIXELS_PER_UNIT;
	camWidthRes  = ((jaFloat)widthRes / pixelsPerUnit);
	camHeightRes = ((jaFloat)heightRes / pixelsPerUnit);

	//camWidthRes  /= this->scale.x;
	//camHeightRes /= this->scale.y;

	const float camHalfWidth  = camWidthRes  * 0.5f;
	const float camHalfHeight = camHeightRes * 0.5f;
	const float crossLineWidth  = 0.2f;
	const float crossLineLength = 1.0f;

	glColor4f(1.0f, 1.0f, 1.0f, 0.4f);
	glBegin(GL_QUADS);
	glVertex2f(-camHalfWidth, -camHalfHeight);
	glVertex2f(camHalfWidth,  -camHalfHeight);
	glVertex2f(camHalfWidth,  camHalfHeight);
	glVertex2f(-camHalfWidth, camHalfHeight);

	glVertex2f(-crossLineWidth, -crossLineLength);
	glVertex2f(crossLineWidth,  -crossLineLength);
	glVertex2f(crossLineWidth,  crossLineLength);
	glVertex2f(-crossLineWidth, crossLineLength);

	glVertex2f(-crossLineLength, -crossLineWidth);
	glVertex2f(-crossLineLength, crossLineWidth);
	glVertex2f(crossLineLength,  crossLineWidth);
	glVertex2f(crossLineLength, -crossLineWidth);

	glEnd();

	//Resource<Font>* fontResource = ResourceManager::getSingleton()->getEternalFont("debug.ttf");
	//DisplayManager::getSingleton()->RenderTextWorld(fontResource->resource, 0.0f, 0.0f, 1.0f, cameraName.getName(), 0, 0, 255, 0, 128);


	//this->drawElements.bindVertices();
	//this->drawElements.drawFaces();

	glPopMatrix();
}

void Camera::draw(RenderBuffer& renderBuffer)
{
	*this->layerObjectFlag |= (uint8_t)LayerObjectFlag::RENDERED_OBJECT;

	Material* material = this->getMaterial();

	if (renderBuffer.checkIfNewRenderCommandNeeded(material))
	{
		renderBuffer.addRenderCommand(material);
	}

	const gil::ObjectHandle2d* handle = getHandle();

	int32_t widthRes, heightRes;
	jaFloat camWidthRes, camHeightRes;
	DisplayManager::getResolution(&widthRes, &heightRes);
	const jaFloat pixelsPerUnit = (jaFloat)setup::graphics::PIXELS_PER_UNIT;
	camWidthRes  = ((jaFloat)widthRes  / pixelsPerUnit);
	camHeightRes = ((jaFloat)heightRes / pixelsPerUnit);

	const float camHalfWidth    = camWidthRes  * 0.5f;
	const float camHalfHeight   = camHeightRes * 0.5f;
	const float crossLineWidth  = 0.2f;
	const float crossLineLength = 1.0f;

 	Resource<FontPolygon>* fontPolygon = ResourceManager::getSingleton()->getEternalFontPolygon("lucon.ttf");
	//Resource<FontPolygon>* fontPolygon = ResourceManager::getSingleton()->getEternalFontPolygon("debug.ttf");

	uint32_t totalVerticesNum = 0;
	uint32_t totalIndicesNum  = 0;


	const uint32_t cameraFrameVerticesNum = 12;
	const uint32_t cameraFrameIndicesNum  = 18;

	totalIndicesNum                += cameraFrameIndicesNum;
	totalVerticesNum               += cameraFrameVerticesNum;
	jaVector2* pVerticesArray       = renderBuffer.verticesPositions.pushArray(totalVerticesNum);
	uint32_t*  pIndicesArray        = renderBuffer.indices.pushArray(totalIndicesNum);
	Color4f*   pColorsArray         = renderBuffer.colors.pushArray(totalVerticesNum);
	jaVector4* pNormalsArray        = renderBuffer.normals.pushArray(totalVerticesNum);
	Color4f*   pSecondaryColorArray = renderBuffer.secondaryColors.pushArray(totalVerticesNum);

	{
		pVerticesArray[0].x = -camHalfWidth; pVerticesArray[0].y = -camHalfHeight;
		pVerticesArray[1].x = camHalfWidth;  pVerticesArray[1].y = -camHalfHeight;
		pVerticesArray[2].x = camHalfWidth;  pVerticesArray[2].y = camHalfHeight;
		pVerticesArray[3].x = -camHalfWidth; pVerticesArray[3].y = camHalfHeight;

		pVerticesArray[4].x = -crossLineWidth; pVerticesArray[4].y = -crossLineLength;
		pVerticesArray[5].x = crossLineWidth;  pVerticesArray[5].y = -crossLineLength;
		pVerticesArray[6].x = crossLineWidth;  pVerticesArray[6].y = crossLineLength;
		pVerticesArray[7].x = -crossLineWidth; pVerticesArray[7].y = crossLineLength;

		pVerticesArray[8].x  = -crossLineLength; pVerticesArray[8].y  = -crossLineWidth;
		pVerticesArray[9].x  = -crossLineLength; pVerticesArray[9].y  = crossLineWidth;
		pVerticesArray[10].x = crossLineLength;  pVerticesArray[10].y = crossLineWidth;
		pVerticesArray[11].x = crossLineLength;  pVerticesArray[11].y = -crossLineWidth;
	}

	{
		pColorsArray[0].set(1.0f, 1.0f, 1.0f, 0.4f);
		pColorsArray[1].set(1.0f, 1.0f, 1.0f, 0.4f);
		pColorsArray[2].set(1.0f, 1.0f, 1.0f, 0.4f);
		pColorsArray[3].set(1.0f, 1.0f, 1.0f, 0.4f);
					   
		pColorsArray[4].set(1.0f, 1.0f, 1.0f, 0.4f);
		pColorsArray[5].set(1.0f, 1.0f, 1.0f, 0.4f);
		pColorsArray[6].set(1.0f, 1.0f, 1.0f, 0.4f);
		pColorsArray[7].set(1.0f, 1.0f, 1.0f, 0.4f);

		pColorsArray[8].set(1.0f, 1.0f, 1.0f, 0.4f);
		pColorsArray[9].set(1.0f, 1.0f, 1.0f, 0.4f);
		pColorsArray[10].set(1.0f, 1.0f, 1.0f, 0.4f);
		pColorsArray[11].set(1.0f, 1.0f, 1.0f, 0.4f);
	}

	{
		pIndicesArray[0] = 0; pIndicesArray[1] = 1; pIndicesArray[2] = 2;
		pIndicesArray[3] = 0; pIndicesArray[4] = 2; pIndicesArray[5] = 3;

		pIndicesArray[6] = 4; pIndicesArray[7] = 5; pIndicesArray[8] = 6;
		pIndicesArray[9] = 4; pIndicesArray[10] = 6; pIndicesArray[11] = 7;

		pIndicesArray[12] = 8; pIndicesArray[13] = 9;  pIndicesArray[14] = 10;
		pIndicesArray[15] = 8; pIndicesArray[16] = 10; pIndicesArray[17] = 11;

		addUint32ArrayOffset_SSE2(pIndicesArray, cameraFrameIndicesNum, renderBuffer.getTotalVerticesNum());
	}

	{
		jaMatrix44* modelMatrix = renderBuffer.modelMatrices.push();
		jaMatrix44* rotMatrix   = renderBuffer.rotMatrices.push();

		jaVectormath2::setWorldAndRotateMatrix44_SSE(handle->transform.position, handle->transform.rotationMatrix, this->scale, *modelMatrix, *rotMatrix);
	}

	{
		const int32_t materialId = material->getMaterialId();
		int32_t* pMaterialsArray = renderBuffer.materialIds.pushArray(totalVerticesNum);

		memsetInt32_SSE2(pMaterialsArray, materialId, totalVerticesNum);
	}

	{
		const int32_t  objectId  = renderBuffer.currentRenderCommand->getObjectsNum();
		int32_t* pObjectIdsArray = renderBuffer.objectIds.pushArray(totalVerticesNum);

		memsetInt32_SSE2(pObjectIdsArray, objectId, totalVerticesNum);
	}

	
	{
		ShaderBinder* shaderBinder      = material->getShaderBinder();
		uint8_t*      pShaderObjectData = renderBuffer.shaderObjectData.pushArray(shaderBinder->getShaderObjectSize());
		memset(pShaderObjectData, 0, shaderBinder->getShaderObjectSize());
	}

	renderBuffer.addObject(); 
	renderBuffer.addIndices(totalIndicesNum); 
	renderBuffer.addVertices(totalVerticesNum);

	if (nullptr != fontPolygon)
	{
		FontPolygon* font = fontPolygon->resource;		

		jaMatrix44 transformMatrix;
		jaVector2  textPosition(0.0f, camHalfHeight * this->scale.y);
		jaVector2  textScale(3.0f * this->scale.x, 3.0f * this->scale.y);
		
		textPosition  = handle->transform.rotationMatrix * textPosition;
		textPosition += handle->transform.position;

		jaVectormath2::setTranslateRotScaleMatrix44(textPosition, handle->transform.rotationMatrix, textScale, transformMatrix);

		uint8_t shaderObjectData[256];
		memset(shaderObjectData, 0, sizeof(shaderObjectData));

		DisplayManager::getSingleton()->RenderText(renderBuffer, material, shaderObjectData, transformMatrix, font, this->cameraName.getName(), (uint32_t)((uint32_t)Margin::TOP | (uint32_t)Margin::CENTER), Color4f(1.0f, 1.0f, 1.0f, 1.0f));
	}
	
}

uint32_t Camera::getSizeOf() const
{
	return sizeof(Camera);
}

void Camera::fillLayerObjectDef(LayerObjectDef& layerObjectDef)
{
	layerObjectDef.layerObjectType = (uint8_t)LayerObjectType::POLYGON;

	layerObjectDef.transform = this->getTransform();
	//layerObjectDef.drawElements->copy(this->drawElements);
	//layerObjectDef.polygonMorphElements->copy(this->morphElements);

	layerObjectDef.layerId = this->layerId;
	//layerObjectDef.color = this->color;
	layerObjectDef.scale = this->scale;
	//layerObjectDef.material = this->material;


	layerObjectDef.depth = this->depth;
	layerObjectDef.cameraMorphElements->copy(this->morphElements);

	layerObjectDef.cameraName.setName(cameraName.getName());
}

bool Camera::isIntersecting(const gil::AABB2d& aabb)
{
	//jaVector2 aabbCenter = aabb.getCenter();
	//
	//float width, height;
	//aabb.getSize(width, height);
	//
	//gil::Box2d boxShape(width, height);
	//
	//const gil::ObjectHandle2d* handle = getHandle();
	//
	//jaVector2 positionDiff = aabbCenter - handle->transform.position;
	//jaMatrix2 invRot = jaVectormath2::inverse(handle->transform.rotationMatrix);
	//jaVector2 aabbLocalPosition = invRot * positionDiff;
	//
	//jaTransform2 aabbTransform(aabbLocalPosition, invRot);

	//return drawElements.isIntersecting(aabbTransform, boxShape, this->scale);
	return true;
}

void Camera::freeAllocatedSpace(LayerObjectManager* layerObjectManager)
{
	//drawElements.freeAllocatedSpace();
	//morphElements.freeAllocatedSpace();
}

Material* Camera::getMaterial() const
{
	return EffectManager::getSingleton()->getDefaultMaterial();
}

}