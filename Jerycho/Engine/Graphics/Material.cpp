#include "Material.hpp"

#include "../Manager/ScriptManager.hpp"
#include "../Manager/ResourceManager.hpp"

namespace ja
{

uint32_t Material::blendEquations[5] = { GL_FUNC_ADD, GL_FUNC_SUBTRACT, GL_FUNC_REVERSE_SUBTRACT, GL_MIN, GL_MAX }; 
uint32_t Material::blendFactors[19]  = { GL_ZERO, GL_ONE, GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR, GL_DST_COLOR, GL_ONE_MINUS_DST_COLOR, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA, GL_CONSTANT_COLOR, GL_ONE_MINUS_CONSTANT_COLOR, GL_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA, GL_SRC_ALPHA_SATURATE, GL_SRC1_COLOR, GL_ONE_MINUS_SRC1_COLOR, GL_SRC1_ALPHA, GL_ONE_MINUS_SRC1_ALPHA };

Material::Material(const char* materialName, ShaderStructureDef* shaderStructureDef) : blendEquation(GL_FUNC_ADD), sourceFactor(GL_SRC_ALPHA), destinationFactor(GL_ONE_MINUS_SRC_ALPHA), shaderBinder(nullptr),
materialData(nullptr)
{
	this->shaderStructureDef = shaderStructureDef;
	this->shaderBinder       = this->shaderStructureDef->getShaderBinder();

	allocateShaderStructureData(this->shaderStructureDef);
	
	this->shaderStructureDef->getShaderBinder()->addMaterial(this); // fills material id
	
	setMaterialName(materialName);
}

Material::~Material()
{
	if (nullptr != this->materialData)
	{
		CacheLineAllocator* resourceAllocator = ResourceManager::getSingleton()->getResourceAllocator();
		resourceAllocator->free(this->materialData, this->materialStructureSize);
	}
}

void Material::setMaterialName(const char* materialName)
{
	this->materialName.setName(materialName);
}

void Material::allocateShaderStructureData(const ShaderStructureDef* shaderStructureDef)
{
	this->materialStructureSize = shaderStructureDef->getShaderStructureSize();
	
	if (0 != this->materialStructureSize)
	{
		CacheLineAllocator* resourceAllocator = ResourceManager::getSingleton()->getResourceAllocator();
		this->materialData = resourceAllocator->allocate(this->materialStructureSize);
	}	
}

char* Material::getBlendingFactorName(uint32_t blendingFactor)
{
	switch (blendingFactor)
	{
	case GL_ZERO:
		return "GL_ZERO";
		break;
	case GL_ONE:
		return "GL_ONE";
		break;
	case GL_SRC_COLOR:
		return "GL_SRC_COLOR";
		break;
	case GL_ONE_MINUS_SRC_COLOR:
		return "GL_ONE_MINUS_SRC_COLOR";
		break;
	case GL_DST_COLOR:
		return "GL_DST_COLOR";
		break;
	case GL_ONE_MINUS_DST_COLOR:
		return "GL_ONE_MINUS_DST_COLOR";
		break;
	case GL_SRC_ALPHA:
		return "GL_SRC_ALPHA";
		break;
	case GL_ONE_MINUS_SRC_ALPHA:
		return "GL_ONE_MINUS_SRC_ALPHA";
		break;
	case GL_DST_ALPHA:
		return "GL_DST_ALPHA";
		break;
	case GL_ONE_MINUS_DST_ALPHA:
		return "GL_ONE_MINUS_DST_ALPHA";
		break;
	case  GL_CONSTANT_COLOR:
		return "GL_CONSTANT_COLOR";
		break;
	case GL_ONE_MINUS_CONSTANT_COLOR:
		return "GL_ONE_MINUS_CONSTANT_COLOR";
		break;
	case GL_CONSTANT_ALPHA:
		return "GL_CONSTANT_ALPHA";
		break;
	case GL_ONE_MINUS_CONSTANT_ALPHA:
		return "GL_ONE_MINUS_CONSTANT_ALPHA";
		break;
	case GL_SRC_ALPHA_SATURATE:
		return "GL_SRC_ALPHA_SATURATE";
		break;
	case GL_SRC1_COLOR:
		return "GL_SRC1_COLOR";
		break;
	case GL_ONE_MINUS_SRC1_COLOR:
		return "GL_ONE_MINUS_SRC1_COLOR";
		break;
	case GL_SRC1_ALPHA:
		return "GL_SRC1_ALPHA";
		break;
	case GL_ONE_MINUS_SRC1_ALPHA:
		return "GL_ONE_MINUS_SRC1_ALPHA";
		break;
	}

	return "UNDEFINED";
}

const char* Material::getBlendEquationName()
{
	switch (this->blendEquation)
	{
	case GL_FUNC_ADD:
		return "GL_FUNC_ADD";
		break;
	case GL_FUNC_SUBTRACT:
		return "GL_FUNC_SUBTRACT";
		break;
	case GL_FUNC_REVERSE_SUBTRACT:
		return "GL_FUNC_REVERSE_SUBTRACT";
		break;
	case GL_MIN:
		return "GL_MIN";
		break;
	case GL_MAX:
		return "GL_MAX";
		break;
	}

	return "UNDEFINED";
}

uint32_t Material::getBlendEquationId(uint32_t blendEquation)
{
	uint32_t blendEquationsNum = getBlendEquationsNum();

	for (uint32_t i = 0; i < blendEquationsNum; ++i)
	{
		if (blendEquation == Material::blendEquations[i])
			return i;
	}

	return 0;
}

uint32_t Material::getBlendFactorId(uint32_t blendFactor)
{
	uint32_t blendFactorsNum = getBlendFactorsNum();

	for (uint32_t i = 0; i < blendFactorsNum; ++i)
	{
		if (blendFactor == Material::blendFactors[i])
			return i;
	}

	return 0;
}

uint32_t* Material::getBlendEquations()
{
	return Material::blendEquations;
}

uint32_t* Material::getBlendFactors()
{
	return Material::blendFactors;
}

uint32_t  Material::getBlendEquationsNum()
{
	return sizeof(Material::blendEquations) / sizeof(uint32_t);
}

uint32_t  Material::getBlendFactorsNum()
{
	return sizeof(Material::blendFactors) / sizeof(uint32_t);
}

void Material::setFloat(const DataFieldInfo* dataFieldInfo, const int32_t elementId, const float dataValue)
{
	float* dataPtr = (float*)((uint8_t*)this->materialData + (dataFieldInfo->dataFieldOffset + (elementId * sizeof(float))));
	*dataPtr = dataValue;
}

void Material::setBool(const DataFieldInfo* dataFieldInfo, const int32_t elementId, const int32_t dataValue)
{
	int32_t* dataPtr = (int32_t*)((uint8_t*)this->materialData + (dataFieldInfo->dataFieldOffset + (elementId * sizeof(int32_t))));
	*dataPtr = dataValue;
}

}