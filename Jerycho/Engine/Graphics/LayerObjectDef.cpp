#include "LayerObjectDef.hpp"

#include "../Manager/LayerObjectManager.hpp"
#include "../Manager/EffectManager.hpp"

namespace ja
{
	LayerObjectDef::LayerObjectDef() : drawElements(nullptr), material(nullptr)
	{
		this->lightType = (uint8_t)LightType::POINT_LIGHT;
	}

	void LayerObjectDef::init(LayerObjectManager* layerObjectManager)
	{
		this->layerObjectType      = (uint8_t)LayerObjectType::POLYGON;
		this->drawElements         = new (layerObjectManager->drawElementAllocator->allocate(sizeof(DrawElements))) DrawElements(layerObjectManager->drawElementAllocator);
		this->shaderObjectData     = new (layerObjectManager->drawElementAllocator->allocate(sizeof(ShaderObjectData))) ShaderObjectData(layerObjectManager->drawElementAllocator);
		this->polygonMorphElements = new (layerObjectManager->drawElementAllocator->allocate(sizeof(PolygonMorphElements))) PolygonMorphElements(layerObjectManager->drawElementAllocator);
		this->cameraMorphElements  = new (layerObjectManager->drawElementAllocator->allocate(sizeof(CameraMorphElements))) CameraMorphElements(layerObjectManager->drawElementAllocator);
		this->material             = nullptr;
	}

	LayerObjectDef::~LayerObjectDef()
	{

	}

}