#pragma once

#include "../Gilgamesh/ObjectHandle2d.hpp"
#include "../Gilgamesh/DebugDraw.hpp"

#include <stdint.h>

namespace ja
{
	enum class LayerObjectType
	{
		POLYGON = 0,
		LIGHT   = 1,
		CAMERA  = 2
	};

	enum class LayerObjectFlag
	{
		UPDATE_TRANSFORM = 1,
		DELETE_OBJECT    = 2,
		RENDERED_OBJECT  = 4
	};

	class LayerObjectManager;
	class LayerObjectDef;
	class MorphElements;
	class LayerObjectMorph;
	class RenderBuffer;

	class LayerObject
	{
	private:
		uint16_t             id;
	public:
		uint16_t             depth;

	public:
		uint8_t layerId;
	private:
		uint8_t objectType;

		gil::ObjectHandle2d* handle;
	
		
	public:
		uint8_t* layerObjectFlag;
		void*    componentData;

		LayerObject(uint8_t objectType, gil::ObjectHandle2d* handle, uint8_t* layerObjectFlag) : objectType(objectType), handle(handle), id(handle->getId()), componentData(nullptr), layerObjectFlag(layerObjectFlag)
		{

		}

		virtual MorphElements* getMorphElements()
		{
			return nullptr;
		}		

		inline void updateTransform()
		{
			*this->layerObjectFlag |= (uint8_t)LayerObjectFlag::UPDATE_TRANSFORM;
		}

		inline void deleteLayerObject()
		{
			*this->layerObjectFlag |= (uint8_t)LayerObjectFlag::DELETE_OBJECT;
		}

		inline bool isDeleted()
		{
			return  (0 != (*this->layerObjectFlag & (uint8_t)LayerObjectFlag::DELETE_OBJECT));
		}

		inline jaTransform2 getTransform()
		{
			return handle->transform;
		}

		virtual void morph(LayerObjectMorph* morphStart, LayerObjectMorph* morphEnd, float morph)
		{
			
		}
		
		inline uint16_t getId() const
		{
			return id;
		}

		inline gil::ObjectHandle2d* getHandle() const
		{
			return handle;
		}

		virtual void draw() = 0;
		virtual void draw(RenderBuffer& renderBuffer) = 0;
		
		inline void drawBounds()
		{
			gil::Debug::drawObjectHandle2d(handle, handle->transform);
		}
		
		inline void drawAABB()
		{
			gil::Debug::drawAABB2d(handle->volume);
		}

		inline jaVector2 getPosition() const
		{
			return this->handle->transform.position;
		}

		inline jaMatrix2 getRotationMatrix() const
		{
			return this->handle->transform.rotationMatrix;
		}

		inline jaFloat getAngleDeg() const
		{
			return this->handle->transform.rotationMatrix.getDegrees();
		}

		inline jaFloat getAngleRad() const
		{
			return this->handle->transform.rotationMatrix.getRadians();
		}

		inline void setPosition(const jaVector2& position)
		{
			this->handle->transform.position = position;
			updateTransform();
		}

		inline void setAngleDeg(const jaFloat angleDeg)
		{
			this->handle->transform.rotationMatrix.setDegrees(angleDeg);
			updateTransform();
		}

		inline void setAngleRad(const jaFloat angleRad)
		{
			this->handle->transform.rotationMatrix.setRadians(angleRad);
			updateTransform();
		}

		inline void setAngleRot(const jaMatrix2& rotMat)
		{
			this->handle->transform.rotationMatrix = rotMat;
			updateTransform();
		}

		inline uint8_t getObjectType() const
		{
			return this->objectType;
		}

		inline void* getComponentData() const
		{
			return componentData;
		}

		void updateComponentData();

		virtual void        setScale(const jaVector2& scale) = 0;
		virtual jaVector2   getScale() const = 0;
		virtual uint32_t    getSizeOf() const = 0;
		virtual bool        isIntersecting(const gil::AABB2d& aabb) = 0;
		virtual void        freeAllocatedSpace(LayerObjectManager* layerObjectManager) = 0;
		virtual void        setLayerObjectDef(const LayerObjectDef& layerObjectDef, bool updateScene = true) = 0;
		virtual void        fillLayerObjectDef(LayerObjectDef& layerObjectDef) = 0;
		virtual void        updateBoundingVolume() = 0;
		virtual const char* getName() const = 0;
		virtual Material*   getMaterial() const= 0;

		friend class LayerObjectManager;
	};
}