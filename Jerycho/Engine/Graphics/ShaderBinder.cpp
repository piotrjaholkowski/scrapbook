#include "ShaderBinder.hpp"
#include "Shader.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Graphics/ShaderLoader.hpp"

namespace ja
{

const UniformField* UniformBlockInfo::getUniformField(uint32_t nameHash) const
{
	const uint32_t      fieldsNum     = this->fields.getSize();
	const UniformField* pUniformField = this->fields.getFirst();

	for (uint32_t i = 0; i < fieldsNum; ++i)
	{
		if (pUniformField[i].tagName.getHash() == nameHash)
			return &pUniformField[i];
	}

	return nullptr;
}

const UniformField* UniformBlockInfo::getUniformField(const char* name) const
{
	const uint32_t      fieldsNum     = this->fields.getSize();
	const UniformField* pUniformField = this->fields.getFirst();

	uint32_t nameHash = ja::math::convertToHash(name);

	for (uint32_t i = 0; i < fieldsNum; ++i)
	{
		if (pUniformField[i].tagName.getHash() == nameHash)
			return &pUniformField[i];
	}

	return nullptr;
}

ShaderBinder::ShaderBinder(uint32_t resourceId) : resourceId(resourceId), vertexShader(nullptr), fragmentShader(nullptr), geometryShader(nullptr), shaderProgramId(0),
shaderObjectSize(0), materialSize(0)
{
	
}

ShaderBinder::~ShaderBinder()
{
	clear();
}

void ShaderBinder::clear()
{
	if (0 != shaderProgramId)
	{
		ShaderLoader::deleteShaderBinder(this);
	}	

	if (nullptr != this->materialUniformBlock.uniformBuffer)
		delete[] this->materialUniformBlock.uniformBuffer;

	if (nullptr != this->settingUniformBlock.uniformBuffer)
		delete[] this->settingUniformBlock.uniformBuffer;

	if (nullptr != this->shaderObjectUniformBlock.uniformBuffer)
		delete[] this->shaderObjectUniformBlock.uniformBuffer;
}


void ShaderBinder::addMaterial(Material* material)
{
	material->materialId = (uint8_t)materials.getSize();
	materials.push(material);
}

void ShaderBinder::bind() const
{
	glUseProgram(this->shaderProgramId);
}

void ShaderBinder::unbind()
{
	glUseProgram(0);
}

}