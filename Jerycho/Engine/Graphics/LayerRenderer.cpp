#include "LayerRenderer.hpp"

#include "RenderBuffer.hpp"

#include <algorithm>

namespace ja {

bool layerObjectCompare(LayerObject* entityA, LayerObject* entityB) 
{
	return (entityA->depth < entityB->depth); 
}

LayerRenderer::LayerRenderer()
{

}

void LayerRenderer::refresh(StackAllocator<gil::ObjectHandle2d*>* objectList)
{
	gil::ObjectHandle2d** layerObjectList = objectList->getFirst();
	uint32_t     num;
	uint8_t      layerId;
	LayerObject* layerObject;

	memset(entitiesNum, 0, sizeof(entitiesNum));

	uint32_t size = objectList->getSize();
	for (uint32_t i = 0; i<size; ++i)
	{
		layerObject = (LayerObject*)layerObjectList[i]->getUserData();
		layerId     = layerObject->layerId;
		num         = entitiesNum[layerId];
		layerObjects[layerId][num] = layerObject;
		entitiesNum[layerId]++;
	}

	for (uint8_t i = 0; i<255; ++i)
	{
		if (0 != entitiesNum[i])
			std::sort(layerObjects[i], layerObjects[i] + entitiesNum[i], layerObjectCompare);
	}
}

void LayerRenderer::renderLayer(uint8_t layerId)
{
	uint32_t size = entitiesNum[layerId];

	for (uint32_t i = 0; i<size; ++i)
	{
		layerObjects[layerId][i]->draw();
	}
}

void LayerRenderer::renderLayer(uint8_t layerId, RenderBuffer& renderBuffer)
{
	uint32_t size = entitiesNum[layerId];

	if (0 != size)
	{
		renderBuffer.addRenderCommand(DisplayManager::projectionViewMatrix, layerObjects[layerId][0]->getMaterial());
	}

	for (uint32_t i = 0; i < size; ++i)
	{
		layerObjects[layerId][i]->draw(renderBuffer);
	}
}

void LayerRenderer::renderLayerObjectBounds(uint8_t layerId)
{
	uint32_t size = entitiesNum[layerId];

	for (uint32_t i = 0; i<size; ++i)
	{
		layerObjects[layerId][i]->drawBounds();
	}
}

void LayerRenderer::renderLayerObjectAABB(uint8_t layerId)
{
	uint32_t size = entitiesNum[layerId];

	for (uint32_t i = 0; i<size; ++i)
	{
		layerObjects[layerId][i]->drawAABB();
	}
}

void LayerRenderer::getLights(StackAllocator<Light*>& lights, uint8_t layerId)
{
	uint32_t size = entitiesNum[layerId];

	LayerObject** currentLayerObjects = layerObjects[layerId];

	for (uint32_t i = 0; i<size; ++i)
	{
		if ((uint8_t)LayerObjectType::LIGHT == currentLayerObjects[i]->getObjectType())
			lights.push((Light*)currentLayerObjects[i]);
	}
}

}