#pragma once

#include "../jaSetup.hpp"
//#include "../Manager/ScriptManager.hpp"
#include "../Allocators/StackAllocator.hpp"
#include "../Utility/NameTag.hpp"
#include <iostream>
#include <vector>

namespace ja
{

	enum class ShaderType
	{
		VERTEX_SHADER   = 0,
		FRAGMENT_SHADER = 1,
		GEOMETRY_SHADER = 2
	};

	class Shader
	{
	public:
		uint32_t   resourceId;
		uint32_t   shaderProcId;
		ShaderType shaderType;

		Shader(uint32_t resourceId);
		~Shader();

		void clear();
	};

}
