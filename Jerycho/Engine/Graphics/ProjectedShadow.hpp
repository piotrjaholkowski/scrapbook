#pragma once

#include "../Allocators/StackAllocator.hpp"
#include "../Math/VectorMath2d.hpp"
#include "../Gilgamesh/ObjectHandle2d.hpp"
#include "Light.hpp"

namespace ja
{
	class LayerRenderer;

class ProjectedShadow
{
private:
	StackAllocator<jaVector2> shadowVertices;
	StackAllocator<uint16_t>  shadowIndices;

	StackAllocator<Light*>               lights[255];
	StackAllocator<gil::ObjectHandle2d*> temporaryObjectList;

	void projectPointLightShadow(gil::ObjectHandle2d* lightHandle, gil::Circle2d* pointShape, Polygon* polygon);

public:
	
	ProjectedShadow()
	{

	}

	void getLights(LayerRenderer* layerRenderer);


	void generateShadows(LayerRenderer* layerRenderer);
};

}