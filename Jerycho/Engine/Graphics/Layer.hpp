#pragma once

#include "ShaderBinder.hpp"
#include "LayerRenderer.hpp"

#include "../Utility/NameTag.hpp"

namespace ja
{

typedef NameTag<setup::TAG_NAME_LENGTH> LayerTag;

class Layer
{
private:
	LayerTag layerTag;
public:
	
	bool  isActive;

	Layer(const char* layerName);
	void renderEffect(LayerRenderer* layerRenderer, uint8_t layerId);

	inline void setName(const char* name)
	{
		layerTag.setName(name);
	}

	inline const char* getName() const
	{
		return layerTag.getName();
	}

	inline uint32_t getHash() const
	{
		return layerTag.getHash();
	}
};

}