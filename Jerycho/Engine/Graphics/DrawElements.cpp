#include "DrawElements.hpp"

namespace ja
{

DrawElements::DrawElements(CacheLineAllocator* pCacheLineAllocator) : pAllocator(pCacheLineAllocator), verticesPositions(nullptr), verticesColors(nullptr), verticesNormals(nullptr), verticesNum(0),
indicesNum(0), indices(nullptr)
{

}

void DrawElements::setVerticesPositions(const jaVector2* const pVerticesPositions, uint32_t verticesNum)
{
	if (this->verticesNum != verticesNum)
	{
		if (nullptr != this->verticesPositions)
		{
			this->pAllocator->free(this->verticesPositions, (this->verticesNum * sizeof(jaVector2)));
		}

		this->verticesPositions = (jaVector2*)this->pAllocator->allocate(verticesNum * sizeof(jaVector2));
		Color4f* newColors = (Color4f*)this->pAllocator->allocate(verticesNum * sizeof(Color4f));

		if (nullptr != this->verticesColors)
		{
			uint32_t verticesToCopyNum = (this->verticesNum < verticesNum) ? this->verticesNum : verticesNum;

			memcpy(newColors, this->verticesColors, verticesToCopyNum * sizeof(Color4f));
			this->pAllocator->free(this->verticesColors, this->verticesNum * sizeof(Color4f));
		}

		this->verticesColors = newColors;
		this->verticesNum    = verticesNum;
	}
	
	memcpy(this->verticesPositions, pVerticesPositions, verticesNum * sizeof(jaVector2));	

	updateBoundingVolume();
}

jaVector2* DrawElements::allocateVerticesPositions(uint32_t verticesNum)
{
	if (this->verticesNum != verticesNum)
	{
		if (nullptr != this->verticesPositions)
		{
			this->pAllocator->free(this->verticesPositions, (this->verticesNum * sizeof(jaVector2)));
			this->verticesPositions = nullptr;
		}

		this->verticesPositions = (jaVector2*)this->pAllocator->allocate(verticesNum * sizeof(jaVector2));
		Color4f* newColors      = (Color4f*)this->pAllocator->allocate(verticesNum * sizeof(Color4f));

		if (nullptr != this->verticesColors)
		{
			uint32_t verticesToCopyNum = (this->verticesNum < verticesNum) ? this->verticesNum : verticesNum;

			memcpy(newColors, this->verticesColors, verticesToCopyNum * sizeof(Color4f));
			this->pAllocator->free(this->verticesColors, this->verticesNum * sizeof(Color4f));
		}

		this->verticesColors = newColors;
		this->verticesNum = verticesNum;
	}

	return this->verticesPositions;
}

void DrawElements::setVerticesColors(const Color4f* const pColorVertices, uint32_t verticesNum)
{
	if (this->verticesNum != verticesNum)
	{
		if (nullptr != this->verticesColors)
		{
			this->pAllocator->free(this->verticesColors, (this->verticesNum * sizeof(Color4f)));
		}

		this->verticesNum    = verticesNum;
		this->verticesColors = (Color4f*)this->pAllocator->allocate(this->verticesNum * sizeof(Color4f));
	}

	memcpy(this->verticesColors, pColorVertices, verticesNum * sizeof(Color4f));
}

void DrawElements::setVerticesColor(const Color4f& color, uint32_t verticesNum)
{
	if (this->verticesNum != verticesNum)
	{
		if (nullptr != this->verticesColors)
		{
			this->pAllocator->free(this->verticesColors, (this->verticesNum * sizeof(Color4f)));
		}

		this->verticesColors = (Color4f*)this->pAllocator->allocate(verticesNum * sizeof(Color4f));
		this->verticesNum    = verticesNum;
	}

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		this->verticesColors[i] = color;
	}

}

void DrawElements::setTriangleIndices(const uint32_t* const pTriangleIndices, uint32_t indicesNum)
{
	if (this->indicesNum != indicesNum)
	{
		if (nullptr != this->indices)
		{
			this->pAllocator->free(this->indices, this->indicesNum * sizeof(uint32_t));
		}

		this->indicesNum = indicesNum;
		this->indices    = (uint32_t*)this->pAllocator->allocate(this->indicesNum * sizeof(uint32_t));
	}

	memcpy(this->indices, pTriangleIndices, this->indicesNum * sizeof(uint32_t));
}

uint32_t* DrawElements::allocateTriangleIndices(uint32_t indicesNum)
{
	if (this->indicesNum != indicesNum)
	{
		if (nullptr != this->indices)
		{
			this->pAllocator->free(this->indices, this->indicesNum * sizeof(uint32_t));
		}

		this->indicesNum = indicesNum;
		this->indices = (uint32_t*)this->pAllocator->allocate(this->indicesNum * sizeof(uint32_t));
	}

	return this->indices;
}

uint16_t findSupportPointIndex(const jaVector2* triangleVertices, const jaVector2& supportDir)
{
	const float proj0 = jaVectormath2::projection(triangleVertices[0], supportDir);
	const float proj1 = jaVectormath2::projection(triangleVertices[1], supportDir);
	const float proj2 = jaVectormath2::projection(triangleVertices[2], supportDir);

	if (proj0 > proj1)
	{
		if (proj0 > proj2)
			return 0;
		else
			return 2;
	}
	else
	{
		if (proj1 > proj2)
			return 1;
		else
			return 2;
	}
}

void getSupportTriangle(const jaVector2* triangleVertices, uint32_t vertexIndex, jaVector2* supportTriangle)
{
	uint32_t lowerIndex;
	uint32_t higherIndex = (vertexIndex + 1) % 3;

	lowerIndex = (vertexIndex == 0) ? 2 : (vertexIndex - 1);

	supportTriangle[0] = triangleVertices[lowerIndex];
	supportTriangle[1] = triangleVertices[vertexIndex];
	supportTriangle[2] = triangleVertices[higherIndex];
}

bool DrawElements::isIntersecting(jaTransform2& transformBoxToDrawElementSpace, const gil::Box2d& box, const jaVector2& drawElementScale)
{
	const uint32_t  trianglesNum = this->indicesNum / 3;
	const uint32_t* pIndices     = this->indices;

	for (uint32_t triangleIt = 0; triangleIt < trianglesNum; ++triangleIt)
	{
		const uint32_t triangleI = triangleIt * 3;

		const uint32_t index0 = pIndices[triangleI];
		const uint32_t index1 = pIndices[triangleI + 1];
		const uint32_t index2 = pIndices[triangleI + 2];

		const jaVector2 triangleVertices[3] = { drawElementScale * this->verticesPositions[index0], drawElementScale * this->verticesPositions[index1], drawElementScale * this->verticesPositions[index2] };
		const jaVector2 triangleCenter = ((triangleVertices[0] + triangleVertices[1] + triangleVertices[2]) / 3.0f);

		jaVector2 supportDir = transformBoxToDrawElementSpace.position - triangleCenter;

		jaVector2 vA[3];
		uint32_t  supportTrianglePointIndex = findSupportPointIndex(triangleVertices, supportDir);
		getSupportTriangle(triangleVertices, supportTrianglePointIndex, vA);

		jaVector2 vB[3];
		jaVector2 supportDirA = jaVectormath2::inverse(transformBoxToDrawElementSpace.rotationMatrix) * -supportDir;
		uint32_t  supportBoxPointIndex = box.findSupportPointIndex(supportDirA);
		box.getSupportTriangle(supportBoxPointIndex, vB);

		vB[0] = transformBoxToDrawElementSpace * vB[0];
		vB[1] = transformBoxToDrawElementSpace * vB[1];
		vB[2] = transformBoxToDrawElementSpace * vB[2];

		jaVector2 normals[4];
		//Make normals from support trangles edges
		normals[0] = jaVectormath2::perpendicular(vA[0] - vA[1]); //normal on the left of vector 
		normals[1] = jaVectormath2::perpendicular(vA[1] - vA[2]); //normal on the left of vector
		normals[2] = jaVectormath2::perpendicular(vB[0] - vB[1]); //normal on the left of vector
		normals[3] = jaVectormath2::perpendicular(vB[1] - vB[2]); //normal on the left of vector

		//Find best normal
		for (uint8_t i = 0; i<2; ++i) //A trinagle normals
		{
			normals[i].normalize(); // Can be deleted ?

			jaFloat aProj = jaVectormath2::projection(normals[i], vA[1]);
			jaFloat bProj = GILMAXFLOAT;

			for (uint8_t z = 0; z<3; ++z) // Project all three points from B and pick best
			{
				jaFloat tempBProj = jaVectormath2::projection(normals[i], vB[z]);
				if (tempBProj < bProj)
				{
					bProj = tempBProj;
				}
			}

			jaFloat depth = aProj - bProj;

			if (depth < GIL_ZERO)
				goto NO_TRIANGLE_INTERSECTION;
		}

		for (uint8_t i = 2; i<4; ++i) //B triangle normals
		{
			normals[i].normalize(); // Sqrt

			jaFloat aProj = GILMAXFLOAT;
			jaFloat bProj = jaVectormath2::projection(normals[i], vB[1]);

			for (uint8_t z = 0; z<3; ++z) // Project all three points from B and pick best
			{
				jaFloat tempAProj = jaVectormath2::projection(normals[i], vA[z]);
				if (tempAProj < aProj)
				{
					aProj = tempAProj;
				}
			}

			jaFloat depth = bProj - aProj;

			if (depth < GIL_ZERO)
				goto NO_TRIANGLE_INTERSECTION;
		}

		return true;
		break;

	NO_TRIANGLE_INTERSECTION:
		continue;
	}

	return false;
}

}