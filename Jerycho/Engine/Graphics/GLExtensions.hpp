#pragma once
//extension for shaders
#define GL_FRAGMENT_SHADER 0x8B30
#define GL_VERTEX_SHADER 0x8B31
typedef unsigned int GLhandleARB;
/*typedef GLhandleARB (APIENTRY * glCreateShaderObjectARBFunc)(GLenum shaderType);
typedef void (APIENTRY * glShaderSourceARBFunc)(GLhandleARB shaderObj, GLsizei count, const GLcharARB* *string, const GLint *length);
typedef void (APIENTRY * glCompileShaderARBFunc)(GLhandleARB shader);
typedef void (APIENTRY * glAttachObjectARBFunc)(GLhandleARB containerObj, GLhandleARB obj);
*/
typedef GLubyte* (APIENTRY * glGetStringiFunc)(GLenum name, GLuint index);
typedef GLuint (APIENTRY * glCreateShaderFunc)(GLenum shaderType);
typedef void (APIENTRY *   glShaderSourceFunc)(GLuint shader, int numOfStrings, const char **strings, int *lenOfStrings);
typedef void (APIENTRY *   glCompileShaderFunc)(GLuint shader);
typedef void (APIENTRY *   glUseProgramFunc)(GLuint prog);
typedef void (APIENTRY *   glLinkProgramFunc)(GLuint program);
typedef void (APIENTRY *   glAttachShaderFunc)(GLuint program, GLuint shader);
typedef GLuint (APIENTRY * glCreateProgramFunc)(void);	
typedef void (APIENTRY *   glDeleteShaderFunc)(GLuint shader);
typedef void (APIENTRY *   glDeleteProgramFunc)(GLuint shader);
typedef void (APIENTRY *   glDetachShaderFunc)(GLuint program, GLuint shader);

typedef GLint (APIENTRY *  glGetUniformLocationFunc)(GLuint program, const char *name);
typedef void (APIENTRY *  glGetActiveUniformFunc)(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name);
typedef void (APIENTRY * glGetActiveUniformBlockNameFunc)(GLuint program, GLuint uniformBlockIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformBlockName);
typedef GLuint (APIENTRY * glGetUniformBlockIndexFunc)(GLuint program, const GLchar *uniformBlockName);
typedef void (APIENTRY * glGetActiveUniformsivFunc)(GLuint program, GLsizei uniformCount, const GLuint *uniformIndices, GLenum pname, GLint *params);
typedef void (APIENTRY * glGetActiveUniformBlockivFunc)(GLuint program, GLuint uniformBlockIndex, GLenum pname, GLint *params);
typedef void (APIENTRY * glBindBufferBaseFunc)(GLenum target, GLuint index, GLuint buffer);
typedef void (APIENTRY * glUniformBlockBindingFunc)(GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding);
typedef GLint(APIENTRY * glUniform1iFunc)(GLint location, GLint v0);
typedef GLint (APIENTRY * glUniform3fFunc)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
typedef GLint (APIENTRY * glUniform1fvFunc)(GLint location, GLsizei count, GLfloat *v);
typedef GLint (APIENTRY * glUniform1ivFunc)(GLint location, GLsizei count, GLint *v);
typedef GLint (APIENTRY * glUniform3fvFunc)(GLint location, GLsizei count, GLfloat *v);
typedef GLint (APIENTRY * glUniform4fvFunc)(GLint location, GLsizei count, GLfloat *v);
typedef GLint (APIENTRY * glUniformMatrix4fvFunc)(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

typedef GLint(APIENTRY * glGenVertexArraysFunc)(GLsizei n, GLuint *arrays);
typedef GLint(APIENTRY * glDeleteVertexArraysFunc)(GLsizei n, const GLuint *arrays);
typedef GLint(APIENTRY * glBindVertexArrayFunc)(GLuint arrayId);
typedef GLint(APIENTRY * glEnableVertexAttribArrayFunc)(GLuint index);
typedef GLint(APIENTRY * glVertexAttribPointerFunc)(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid * pointer);
typedef GLint(APIENTRY * glVertexAttribIPointerFunc)(GLuint index, GLint size, GLenum type, GLsizei stride, const GLvoid * pointer);
typedef GLint(APIENTRY * glDisableVertexAttribArrayFunc)(GLuint index);

// in opengl 2.0 standard for debugging
typedef void (APIENTRY * glGetShaderivFunc)(GLuint object, GLenum type, int *param);
typedef void (APIENTRY * glGetProgramivFunc)(GLuint object, GLenum type, int *param);
typedef void (APIENTRY * glGetShaderInfoLogFunc)(GLuint object, int maxLen, int *len, char *log);
typedef void (APIENTRY * glGetProgramInfoLogFunc)(GLuint object, int maxLen, int *len, char *log);
// only with ARB
typedef void (APIENTRY * glGetObjectParameterivARBFunc)(GLhandleARB object, GLenum type, int *param);
typedef void (APIENTRY * glGetInfoLogARBFunc)(GLhandleARB object, int maxLen, int *len, char *log);
//


//extension for shaders

//extensions for framebuffer
typedef void (APIENTRY * glTexImage2DMultisampleFunc)(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations);
typedef void (APIENTRY * glGenFramebuffersFunc)(GLsizei n, GLuint *ids);
typedef void (APIENTRY * glBindFramebufferFunc)(GLenum target, GLuint framebuffer);
typedef void (APIENTRY * glFramebufferTexture2DFunc)(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
typedef void (APIENTRY * glDrawBuffersFunc)(GLsizei n,const GLenum *bufs);
typedef void (APIENTRY * glBlitFramebufferFunc)(GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter);
typedef GLenum (APIENTRY * glCheckFramebufferStatusFunc)(GLenum target);
typedef void (APIENTRY * glDeleteFramebuffersFunc)(GLsizei n, const GLuint * framebuffers);
//extensions for framebuffer

//vbo
typedef void (APIENTRY * glGenBuffersFunc)(GLsizei n, GLuint* ids);
typedef void (APIENTRY * glDeleteBuffersFunc)(GLsizei n, GLuint* buffers);
typedef void (APIENTRY * glBindBufferFunc)(GLenum target, GLuint id);
typedef void (APIENTRY * glBufferDataFunc)(GLenum target, GLsizei size, const void* data, GLenum usage);
typedef void (APIENTRY * glBufferSubDataFunc)(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid * data);
//vbo

#define GL_GENERATE_MIPMAP 0x8191
typedef void (APIENTRY * glGenerateMipmapFunc)(GLenum target); 
typedef void (APIENTRY * glActiveTextureFunc)(GLenum texture);
typedef void (APIENTRY * glBlendEquationFunc)(GLenum mode);
