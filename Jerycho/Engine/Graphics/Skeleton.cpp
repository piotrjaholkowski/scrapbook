#include "Skeleton.hpp"
#include "../Gilgamesh/Mid/AABB2d.hpp"

namespace ja
{

	Skeleton::Skeleton(CacheLineAllocator* boneAllocator) : boneAllocator(boneAllocator), next(nullptr), prev(nullptr)
	{
		this->bones = (Bone*)boneAllocator->allocate(sizeof(Bone) + sizeof(TransformedBone) + sizeof(BoneName));
		this->transformedBones = (TransformedBone*)(this->bones + 1);
		this->boneNames = (BoneName*)(this->transformedBones + 1);

		this->bones[0].transformation.length = 0.0f;
		this->bones[0].transformation.rotMat.setDegrees(0.0f);
		this->bones[0].parentId = -1;
		this->bones[0].childNum = 0;

		this->boneNames[0].setName("root");

		this->bonesNum = 1;

		this->skeletalAnimations = nullptr;
	}


	Skeleton::~Skeleton()
	{
		this->boneAllocator->free(this->bones, this->bonesNum * (sizeof(Bone) + sizeof(TransformedBone) + sizeof(BoneName)));

		if (nullptr != this->skeletalAnimations)
		{
			this->skeletalAnimations->removeReference();

			if (this->skeletonId == this->skeletalAnimations->getOwnerId())
			{
				if (this->skeletalAnimations->getReferencesNum() <= 0)
				{
					delete this->skeletalAnimations;
					this->boneAllocator->free(this->skeletalAnimations, sizeof(SkeletalAnimations));
					this->skeletalAnimations = nullptr;
				}
				else
				{
					this->skeletalAnimations->rebindOwner();
				}
			}
		}
	}

	void Skeleton::addBone(int16_t parentBoneId, const jaVector2& boneTransformedPosition)
	{
		calculateTransformedBoneStates();

		uint16_t newBonesNum = this->bonesNum + 1;
		uint8_t* newAllocatedMem = (uint8_t*)this->boneAllocator->allocate(newBonesNum * (sizeof(Bone) + sizeof(TransformedBone) + sizeof(BoneName)));

		Bone*            newBones = (Bone*)newAllocatedMem;
		TransformedBone* newTransformedBones = (TransformedBone*)(newBones + newBonesNum);
		BoneName*        newBoneNames = (BoneName*)(newTransformedBones + newBonesNum);

		this->bones[parentBoneId].childNum++;

		memcpy(newBones, this->bones, bonesNum * sizeof(Bone));
		memcpy(newBoneNames, this->boneNames, bonesNum * sizeof(BoneName));

		TransformedBone& parentTransformed = this->transformedBones[parentBoneId];
		Bone&     childBone = newBones[newBonesNum - 1];
		BoneName& childBoneName = newBoneNames[newBonesNum - 1];

		childBone.childNum = 0;
		childBone.parentId = parentBoneId;

		jaVector2 positionDiff = boneTransformedPosition - parentTransformed.position;
		jaVector2 parentDiff = parentTransformed.rotMat * jaVector2(1.0f, 0.0f);

		childBone.transformation.length = jaVectormath2::length(positionDiff);
		childBone.transformation.rotMat.setRadians(jaVectormath2::getAngle(positionDiff, parentDiff));

		char boneName[setup::graphics::BONE_NAME_LENGTH];
		sprintf(boneName, "bone %d", (int32_t)(this->bonesNum));
		childBoneName.setName(boneName);

		{
			//release old mem
			this->boneAllocator->free(this->bones, bonesNum * (sizeof(Bone) + sizeof(TransformedBone) + sizeof(BoneName)));
		}

		{
			//assingn new data
			this->bonesNum = newBonesNum;
			this->bones = newBones;
			this->transformedBones = newTransformedBones;
			this->boneNames = newBoneNames;
		}

		calculateTransformedBoneStates();
	}

	void Skeleton::calculateTransformedBoneStates()
	{
		transformedBones[0].position = position;
		transformedBones[0].rotMat   = bones[0].transformation.rotMat;

		for (uint16_t i = 1; i < bonesNum; ++i)
		{
			Bone& itBone = this->bones[i];
			const TransformedBone& parent = this->transformedBones[itBone.parentId];

			TransformedBone& itTransformedBone = this->transformedBones[i];

			itTransformedBone.rotMat = parent.rotMat * itBone.transformation.rotMat;
			itTransformedBone.position = parent.position + itTransformedBone.rotMat * jaVector2(itBone.transformation.length, 0.0f);
		}
	}

	void Skeleton::setBonePosition(int16_t boneId, const jaVector2& boneTransformedPosition)
	{
		ja::Bone* pBone = &this->bones[boneId];

		TransformedBone& parentTransformed = this->transformedBones[pBone->parentId];

		jaVector2 positionDiff = boneTransformedPosition - parentTransformed.position;
		jaVector2 parentDiff = parentTransformed.rotMat * jaVector2(1.0f, 0.0f);

		pBone->transformation.length = jaVectormath2::length(positionDiff);
		pBone->transformation.rotMat.setRadians(jaVectormath2::getAngle(positionDiff, parentDiff));

		calculateTransformedBoneStates();
	}

	void Skeleton::setBoneWordRotation(int16_t boneId, float angleRadians)
	{
		const float     worldBoneRot = transformedBones[boneId].rotMat.getRadians();
		const float     deltaRot = angleRadians - worldBoneRot;
		const jaMatrix2 deltaRotMat(deltaRot);

		bones[boneId].transformation.rotMat = bones[boneId].transformation.rotMat * deltaRotMat;
		calculateTransformedBoneStates();
	}

	SkeletonAnimation* Skeleton::createAnimation(const char* animationName)
	{
		if (nullptr == this->skeletalAnimations)
		{
			this->skeletalAnimations = new (this->boneAllocator->allocate(sizeof(SkeletalAnimations))) SkeletalAnimations(this->skeletonId, this->boneAllocator);
		}

		return this->skeletalAnimations->createAnimation(animationName, this->bonesNum);
	}

	void Skeleton::setSkeletonPose(uint16_t animationNameHashCode, float normalizedTime)
	{
		if (nullptr != this->skeletalAnimations)
		{
			SkeletonAnimation* skeletonAnimation = skeletalAnimations->getAnimation(animationNameHashCode);

			if (nullptr != skeletonAnimation)
				skeletonAnimation->setAnimationPose(normalizedTime, this->bones);

			calculateTransformedBoneStates();
		}
	}

	void Skeleton::setSkeletonPose(const char* animationName, float normalizedTime)
	{
		AnimationName animationNameTag;

		animationNameTag.setName(animationName);

		setSkeletonPose(animationNameTag.getHash(), normalizedTime);
	}

	void Skeleton::draw()
	{
		glPushMatrix();

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glLineWidth(0.1f);

		glBegin(GL_LINES);
		for (int32_t i = 1; i < bonesNum; ++i)
		{
			int16_t parentId = this->bones[i].parentId;

			glVertex2f(transformedBones[parentId].position.x, transformedBones[parentId].position.y);
			glVertex2f(transformedBones[i].position.x, transformedBones[i].position.y);
		}
		glEnd();

		glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
		glPointSize(3.0f);

		glBegin(GL_POINTS);
		for (int32_t i = 0; i < bonesNum; ++i)
		{
			glVertex2f(transformedBones[i].position.x, transformedBones[i].position.y);
		}
		glEnd();

		glPopMatrix();
	}

	void Skeleton::getSelectedBones(const gil::AABB2d& selection, StackAllocator<uint16_t>& selectedBoneIds)
	{
		for (uint16_t i = 0; i < this->bonesNum; ++i)
		{
			if (selection.isPointInside(this->transformedBones[i].position))
			{
				selectedBoneIds.push(i);
			}
		}
	}

}