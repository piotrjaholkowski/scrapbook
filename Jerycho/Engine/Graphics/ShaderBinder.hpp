#pragma once

#include "../jaSetup.hpp"
#include "../Manager/DisplayManager.hpp"
#include "Shader.hpp"
#include "../Allocators/StackAllocator.hpp"

namespace ja
{
	class Material;
	class ShaderStructureDef;

	typedef NameTag<setup::TAG_NAME_LENGTH> UniformFieldTag;
	typedef NameTag<setup::TAG_NAME_LENGTH> UniformBlockTag;

	class UniformField
	{
	public:
		uint32_t        fieldOffset;
		uint32_t        elementsNum;
		uint32_t        arrayStride;
		uint32_t        fieldType;
		UniformFieldTag tagName;

		UniformField()
		{

		}

		UniformField(const char* name, uint32_t fieldOffset, uint32_t elementsNum, uint32_t arrayStride, uint32_t fieldType) : fieldOffset(fieldOffset), elementsNum(elementsNum),
			arrayStride(arrayStride), fieldType(fieldType)
		{
			this->tagName.setName(name);
		}
	};

	class UniformBlockInfo
	{
	private:
		uint32_t uniformBlockIndex;
		int32_t  uniformBlockSize;
		uint32_t uniformBlockHandle;

		void* uniformBuffer;

		StackAllocator<UniformField> fields;

		UniformBlockTag tagName;
	public:

		UniformBlockInfo() : uniformBlockSize(0), uniformBlockIndex(0), uniformBlockHandle(0), uniformBuffer(nullptr)
		{

		}

		inline const StackAllocator<UniformField>* getDataFields() const
		{
			return &fields;
		}

		inline int32_t getBlockSize() const
		{
			return this->uniformBlockSize;
		}

		inline void* getBufferData() const
		{
			return this->uniformBuffer;
		}

		inline void setFloat(const UniformField* uniformField, float* floatValue, uint32_t valuesNum) const
		{
			memcpy((uint8_t*)this->uniformBuffer + uniformField->fieldOffset, floatValue, valuesNum * sizeof(float));
		}

		inline void setBool(const UniformField* uniformField, int32_t* boolValue, uint32_t valuesNum) const
		{
			memcpy((uint8_t*)this->uniformBuffer + uniformField->fieldOffset, boolValue, valuesNum * sizeof(int32_t));
		}

		inline void sendBufferBlockData() const
		{
			glBindBuffer(GL_UNIFORM_BUFFER, this->uniformBlockHandle);
			glBufferData(GL_UNIFORM_BUFFER, this->uniformBlockSize, this->uniformBuffer, GL_STATIC_DRAW);
			glBindBuffer(GL_UNIFORM_BUFFER, 0);
		}
		
		const UniformField* getUniformField(uint32_t nameHash) const;
		const UniformField* getUniformField(const char* name) const;

		friend class ShaderLoader;
		friend class ShaderBinder;
	};


	class ShaderBinder
	{
	private:
		uint32_t resourceId;
		uint32_t shaderProgramId;

		int32_t  mvpUniformLocation;
		int32_t  mvpMatricesUniformLocation;
		int32_t  rotMatricesUniformLocation;
		uint32_t materialSize;
		uint32_t shaderObjectSize;

		uint32_t materialUniformBlockHandle;
		uint32_t settingUniformBlockHandle;
		uint32_t shaderObjectUniformBlockHandle;

		UniformBlockInfo materialUniformBlock;
		UniformBlockInfo settingUniformBlock;
		UniformBlockInfo shaderObjectUniformBlock;

		ShaderStructureDef* shaderObjectDef;

		Shader* vertexShader;
		Shader* fragmentShader;
		Shader* geometryShader;		
	
		StackAllocator<Material*> materials;
	public:

		ShaderBinder(uint32_t resourceId);
		~ShaderBinder();

	private:
		void addMaterial(Material* material);
	public:
		void    bind() const;
		static  void unbind();

		inline uint32_t getResourceId() const
		{
			return this->resourceId;
		}

		inline uint32_t getShaderObjectSize() const
		{
			return this->shaderObjectSize;
		}

		inline uint32_t getMaterialSize() const
		{
			return this->materialSize;
		}

		inline ShaderStructureDef* getShaderObjectDef() const
		{
			return this->shaderObjectDef;
		}

		inline int32_t getMvpMatricesUniformLocation() const
		{
			return this->mvpMatricesUniformLocation;
		}

		inline int32_t getRotMatricesUniformLocation() const
		{
			return this->rotMatricesUniformLocation;
		}

		inline const StackAllocator<Material*>* getMaterials() const
		{
			return &this->materials;
		}

		inline uint32_t getMaterialUniformBlockHandle() const
		{
			return this->materialUniformBlockHandle;
		}

		inline uint32_t getSettingUniformBlockHandle() const
		{
			return this->settingUniformBlockHandle;
		}

		inline uint32_t getShaderObjectUniformBlockHandle() const
		{
			return this->shaderObjectUniformBlockHandle;
		}

		inline const UniformBlockInfo* getMaterialUniformBlock() const
		{
			return &this->materialUniformBlock;
		}

		inline const UniformBlockInfo* getSettingUniformBlock() const
		{
			return &this->settingUniformBlock;
		}
		
		inline const UniformBlockInfo* getShaderObjectUniformBlock() const
		{
			return &this->shaderObjectUniformBlock;
		}

		inline uint32_t getShaderProgramId() const
		{
			return this->shaderProgramId;
		}

		void clear();

		friend class Material;
		friend class ShaderLoader;
		friend class ResourceManager;
	};

}