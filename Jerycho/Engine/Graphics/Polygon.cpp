#include "Polygon.hpp"

#include "Material.hpp"
#include "LayerObjectDef.hpp"

#include "../Manager/LayerObjectManager.hpp"
#include "../Manager/EffectManager.hpp"

#include "../Gilgamesh/DebugDraw.hpp"
#include "LayerObjectMorph.hpp"

#include "../Allocators/CacheLineAllocator.hpp"

namespace ja
{

PolygonMorphElements::PolygonMorphElements(CacheLineAllocator* pAllocator) : MorphElements(pAllocator)
{

}

LayerObjectMorph* PolygonMorphElements::createMorph(const char* morphName)
{
	PolygonMorph* polygonMorph = new ((PolygonMorph*)pAllocator->allocate(sizeof(PolygonMorph))) PolygonMorph(morphName, this->pAllocator);

	addMorph(polygonMorph);

	return polygonMorph;
}

LayerObjectMorph* PolygonMorphElements::createMorph(uint32_t hash)
{
	PolygonMorph* polygonMorph = new ((PolygonMorph*)pAllocator->allocate(sizeof(PolygonMorph))) PolygonMorph(hash, pAllocator);

	addMorph(polygonMorph);

	return polygonMorph;
}

PolygonMorph::PolygonMorph(const char* morphName, CacheLineAllocator* pAllocator) : LayerObjectMorph(morphName), drawElements(pAllocator)
{

}

PolygonMorph::PolygonMorph(uint32_t hash, CacheLineAllocator* pAllocator) : LayerObjectMorph(hash), drawElements(pAllocator)
{

}

void PolygonMorph::setMorph(LayerObject* layerObject)
{
	Polygon* polygon = (Polygon*)layerObject;

	this->scale     = polygon->scale;
	this->transform = polygon->getHandle()->getTransform();

	this->drawElements.copy(polygon->drawElements);
}

void* PolygonMorph::copy(CacheLineAllocator* pAllocator)
{
	PolygonMorph* polygonMorph = new ((PolygonMorph*)pAllocator->allocate(sizeof(PolygonMorph))) PolygonMorph(getHash(), pAllocator);

	polygonMorph->scale     = this->scale;
	polygonMorph->transform = this->transform;

	polygonMorph->drawElements.copy(this->drawElements);
	polygonMorph->activeFeatures = this->activeFeatures;

	return polygonMorph;
}

uint8_t PolygonMorph::getFeaturesNum() const
{
	return 5;
}

const char* PolygonMorph::getFeatureName(uint8_t featureId) const
{
	switch (featureId)
	{
	case (uint8_t)PolygonMorphFeature::TRANSLATE:
		return "Translate";
		break;
	case (uint8_t)PolygonMorphFeature::ROTATE:
		return "Rotate";
		break;
	case (uint8_t)PolygonMorphFeature::SCALE:
		return "Scale";
		break;
	case (uint8_t)PolygonMorphFeature::COLOR:
		return "Color";
		break;
	case (uint8_t)PolygonMorphFeature::VERTICES:
		return "Vertices";
		break;
	}

	return "Undefined";
}

uint32_t PolygonMorph::getSizeOf() const
{
	return sizeof(PolygonMorph);
}

void PolygonMorph::freeAllocatedSpace()
{
	this->drawElements.freeAllocatedSpace();
}

Polygon::Polygon(gil::ObjectHandle2d* handle, uint8_t* layerObjectFlag, CacheLineAllocator* pDrawElementsAllocator) : LayerObject((uint8_t)LayerObjectType::POLYGON, handle, layerObjectFlag), drawElements(pDrawElementsAllocator),
morphElements(pDrawElementsAllocator),  material(nullptr), shaderObjectData(pDrawElementsAllocator)
{

}

void Polygon::morph(LayerObjectMorph* morphStart, LayerObjectMorph* morphEnd, float morph)
{
	PolygonMorph* polygonMorphStart = (PolygonMorph*)morphStart;
	PolygonMorph* polygonMorphEnd   = (PolygonMorph*)morphEnd;
	
	if (polygonMorphStart->isFeatureActive((uint8_t)PolygonMorphFeature::TRANSLATE))
	{
		jaVector2 translateRel = polygonMorphEnd->transform.position - polygonMorphStart->transform.position;
		const jaVector2 endTranslate = polygonMorphStart->transform.position + (morph * translateRel);
		setPosition(endTranslate);
	}
	
	if (polygonMorphStart->isFeatureActive((uint8_t)PolygonMorphFeature::ROTATE))
	{
		float rotateRel = polygonMorphEnd->transform.rotationMatrix.getRadians() - polygonMorphStart->transform.rotationMatrix.getRadians();
		const jaMatrix2 rotMat = polygonMorphStart->transform.rotationMatrix * jaMatrix2(rotateRel * morph);
		setAngleRot(rotMat);
	}
	
	if (polygonMorphStart->isFeatureActive((uint8_t)PolygonMorphFeature::SCALE))
	{
		jaVector2 scaleRel = polygonMorphEnd->scale - polygonMorphStart->scale;
		jaVector2 scale = polygonMorphStart->scale + (morph * scaleRel);
		setScale(scale);
	}

	if (polygonMorphStart->isFeatureActive((uint8_t)PolygonMorphFeature::COLOR))
	{
		const uint32_t verticesNumStart = polygonMorphStart->drawElements.getVerticesNum();
		const uint32_t verticesNumEnd   = polygonMorphEnd->drawElements.getVerticesNum();
	
		const Color4f* pVerticesStart   = polygonMorphStart->drawElements.getVerticesColors();
		const Color4f* pVerticesEnd     = polygonMorphEnd->drawElements.getVerticesColors();
		Color4f*       pCurrentVertex   = (Color4f*)drawElements.getVerticesColors();
	
		const uint32_t verticesNum      = (verticesNumStart > verticesNumEnd) ? verticesNumStart : verticesNumEnd;
	
		//for (uint32_t i = 0; i < verticesNum; ++i)
		//{
		//	pCurrentVertex[i].r = pVerticesStart[i].r + ((pVerticesEnd[i].r - pVerticesStart[i].r) * morph);
		//	pCurrentVertex[i].g = pVerticesStart[i].g + ((pVerticesEnd[i].g - pVerticesStart[i].g) * morph);
		//	pCurrentVertex[i].b = pVerticesStart[i].b + ((pVerticesEnd[i].b - pVerticesStart[i].b) * morph);
		//	pCurrentVertex[i].a = pVerticesStart[i].a + ((pVerticesEnd[i].a - pVerticesStart[i].a) * morph);
		//}

		//DrawElement allocator allings to 64 bytes
		jaVectormath2::morphDataStream_SSE((__m128*)pVerticesStart, (__m128*)pVerticesEnd, morph, verticesNum, (__m128*)pCurrentVertex);
	}
	
	if (polygonMorphStart->isFeatureActive((uint8_t)PolygonMorphFeature::VERTICES))
	{
		const uint32_t verticesNumStart = polygonMorphStart->drawElements.getVerticesNum();
		const uint32_t verticesNumEnd   = polygonMorphEnd->drawElements.getVerticesNum();
	
		const jaVector2* pVerticesStart = polygonMorphStart->drawElements.getVerticesPositions();
		const jaVector2* pVerticesEnd   = polygonMorphEnd->drawElements.getVerticesPositions();
		jaVector2*       pCurrentVertex = (jaVector2*)drawElements.getVerticesPositions();
	
		const uint32_t verticesNum = (verticesNumStart > verticesNumEnd) ? verticesNumStart : verticesNumEnd;
	
		//for (uint32_t i = 0; i < verticesNum; ++i)
		//{
		//	pCurrentVertex[i] = pVerticesStart[i] + ((pVerticesEnd[i] - pVerticesStart[i]) * morph);
		//}
		jaVectormath2::morphDataStream_SSE((__m128*)pVerticesStart, (__m128*)pVerticesEnd, morph, verticesNum, (__m128*)pCurrentVertex);
	}
	
}

void Polygon::setLayerObjectDef(const LayerObjectDef& layerObjectDef, bool updateScene)
{
	gil::ObjectHandle2d* handle = getHandle();
	
	handle->transform = layerObjectDef.transform;

	this->drawElements.copy(*layerObjectDef.drawElements);
	this->shaderObjectData.copy(*layerObjectDef.shaderObjectData);
	this->morphElements.copy(*layerObjectDef.polygonMorphElements);

	this->layerId     = layerObjectDef.layerId;
	this->scale       = layerObjectDef.scale;
	this->material    = layerObjectDef.material;
	this->depth       = layerObjectDef.depth;
	
	gil::Circle2d* circle = (gil::Circle2d*)handle->getShape();

	jaFloat maxScale = std::max(abs(layerObjectDef.scale.x), abs(layerObjectDef.scale.y));
	circle->radius = maxScale * layerObjectDef.drawElements->getBoundingVolumeRadius();

	if (updateScene)
		updateTransform();
}

void Polygon::fillLayerObjectDef(LayerObjectDef& layerObjectDef)
{
	layerObjectDef.layerObjectType = (uint8_t)LayerObjectType::POLYGON;

	layerObjectDef.transform = this->getTransform();
	layerObjectDef.drawElements->copy(this->drawElements);
	layerObjectDef.shaderObjectData->copy(this->shaderObjectData);
	layerObjectDef.polygonMorphElements->copy(this->morphElements);

	layerObjectDef.layerId     = this->layerId;
	layerObjectDef.scale       = this->scale;
	layerObjectDef.material    = this->material;


	layerObjectDef.depth = this->depth;
}

void Polygon::setText(FontPolygon* font, const char* text, const Color4f& color)
{	
	uint32_t totalVerticesNum = 0;
	uint32_t totalIndicesNum  = 0;

	const char* textIt = text;

	for (textIt = text; *textIt != 0; ++textIt)
	{
		totalVerticesNum += font->letter[*textIt].verticesNum;
		totalIndicesNum  += font->letter[*textIt].indicesNum;
	}

	jaVector2* pVerticesDst = this->drawElements.allocateVerticesPositions(totalVerticesNum);
	uint32_t*  pIndicesDst  = this->drawElements.allocateTriangleIndices(totalIndicesNum);

	float    advanceX   = 0;
	uint32_t verticesIt = 0;
	uint32_t indicesIt  = 0;

	for (textIt = text; *textIt != 0; ++textIt)
	{
		uint32_t   verticesNum = font->letter[*textIt].verticesNum;
		jaVector2* pVertices   = font->letter[*textIt].vertices;
		uint32_t   indicesNum  = font->letter[*textIt].indicesNum;
		uint32_t*  pIndices    = font->letter[*textIt].indices;
		float      advanceY    = font->letter[*textIt].advanceY;

		//memcpy(&pVerticesDst[verticesIt], pVertices, verticesNum * sizeof(jaVector2));
		jaVectormath2::copyVector2WithTranslation_SSE(&pVerticesDst[verticesIt], pVertices, jaVector2((float)advanceX, (float)advanceY), verticesNum);
		//memcpy(&pIndices[indicesIt], pIndices, indicesNum * sizeof(uint32_t));
		memcpyUint32ArrayWithOffset_SSE2(&pIndicesDst[indicesIt], pIndices, indicesNum, verticesIt);

		verticesIt += verticesNum;
		indicesIt  += indicesNum;
		advanceX   += font->letter[*textIt].advanceX;
	}

	this->drawElements.setVerticesColor(color , totalVerticesNum);	
}

void Polygon::drawBounds()
{
	const gil::ObjectHandle2d* handle = this->getHandle();
	gil::Debug::drawObjectHandle2d(handle, handle->transform);
}

void Polygon::drawAABB()
{
	gil::Debug::drawAABB2d(this->getHandle()->volume);
}

bool Polygon::isIntersecting(const gil::AABB2d& aabb)
{
	jaVector2 aabbCenter = aabb.getCenter();
	
	float width, height;
	aabb.getSize(width, height);

	gil::Box2d boxShape(width, height);

	const gil::ObjectHandle2d* handle = getHandle();

	jaVector2 positionDiff      = aabbCenter - handle->transform.position;
	jaMatrix2 invRot            = jaVectormath2::inverse(handle->transform.rotationMatrix);
	jaVector2 aabbLocalPosition = invRot * positionDiff;

	jaTransform2 aabbTransform(aabbLocalPosition, invRot);

	return drawElements.isIntersecting(aabbTransform, boxShape, this->scale);
}

void Polygon::draw()
{
	*this->layerObjectFlag |= (uint8_t)LayerObjectFlag::RENDERED_OBJECT;

	glPushMatrix();

	jaMatrix44 modelMatrix;


	const gil::ObjectHandle2d* handle = getHandle();

	jaVectormath2::setTranslateRotScaleMatrix44(handle->transform.position, handle->transform.rotationMatrix, this->scale, modelMatrix);
	
	glMultMatrixf(modelMatrix.element);


	this->drawElements.bindVertices();

	this->drawElements.drawFaces();

	glPopMatrix();
}

void Polygon::draw(RenderBuffer& renderBuffer)
{
	*this->layerObjectFlag |= (uint8_t)LayerObjectFlag::RENDERED_OBJECT;

	if (renderBuffer.checkIfNewRenderCommandNeeded(material))
	{
		renderBuffer.addRenderCommand(material);
	}

	const uint32_t verticesNum = this->drawElements.getVerticesNum();
	const uint32_t indicesNum  = drawElements.getTriangleIndicesNum();

	{
		jaVector2*       pVerticesArray = renderBuffer.verticesPositions.pushArray(verticesNum);
		const jaVector2* pVertices      = drawElements.getVerticesPositions();

		memcpy(pVerticesArray, pVertices, verticesNum * sizeof(jaVector2));
	}

	{
		Color4f*       pColorsArray = renderBuffer.colors.pushArray(verticesNum);
		const Color4f* pColors      = drawElements.getVerticesColors();

		memcpy(pColorsArray, pColors, verticesNum * sizeof(Color4f));
	}

	{
		jaVector4* pNormalsArray = renderBuffer.normals.pushArray(verticesNum);
	}

	{
		Color4f*  pSecondaryColorArray = renderBuffer.secondaryColors.pushArray(verticesNum);
	}

	{
		const uint32_t* pIndices = drawElements.getTriangleIndices();
		uint32_t*       pIndicesArray = renderBuffer.indices.pushArray(indicesNum);

		memcpyUint32ArrayWithOffset_SSE2(pIndicesArray, pIndices, indicesNum, renderBuffer.getTotalVerticesNum());
	}
	
	{
		jaMatrix44* modelMatrix = renderBuffer.modelMatrices.push();
		jaMatrix44* rotMatrix   = renderBuffer.rotMatrices.push();

		const gil::ObjectHandle2d* handle = getHandle();

		jaVectormath2::setWorldAndRotateMatrix44_SSE(handle->transform.position, handle->transform.rotationMatrix, this->scale, *modelMatrix, *rotMatrix);
	}

	{
		const int32_t materialId = material->getMaterialId();
		int32_t* pMaterialsArray = renderBuffer.materialIds.pushArray(verticesNum);
	
		memsetInt32_SSE2(pMaterialsArray, materialId, verticesNum);
	}

	{
		const int32_t  objectId  = renderBuffer.currentRenderCommand->getObjectsNum();
		int32_t* pObjectIdsArray = renderBuffer.objectIds.pushArray(verticesNum);

		memsetInt32_SSE2(pObjectIdsArray, objectId, verticesNum);
	}

	{
		const uint32_t shaderObjectSize = shaderObjectData.getSize();
		uint8_t*      pShaderObjectData = renderBuffer.shaderObjectData.pushArray(shaderObjectSize);

		memcpy(pShaderObjectData, shaderObjectData.getData(), shaderObjectSize);
	}

	renderBuffer.addObject(); 
	renderBuffer.addIndices(indicesNum); 
	renderBuffer.addVertices(verticesNum);
}

void Polygon::freeAllocatedSpace(LayerObjectManager* layerObjectManager)
{
	drawElements.freeAllocatedSpace();
	morphElements.freeAllocatedSpace();
	shaderObjectData.freeAllocatedSpace();
}

LayerObject* Polygon::createPolygon(CacheLineAllocator* layerObjectAllocator, const LayerObjectDef& layerObjectDef, LayerObjectManager* layerObjectManager)
{
	jaFloat maxScale = std::max(abs(layerObjectDef.scale.x), abs(layerObjectDef.scale.y));

	gil::Scene2d* scene2d = layerObjectManager->getScene();

	gil::Circle2d        circleShape(maxScale * layerObjectDef.drawElements->getBoundingVolumeRadius());
	gil::ObjectHandle2d* handle = scene2d->createDynamicObject(&circleShape, layerObjectDef.transform);
	uint8_t*             layerObjectFlag = layerObjectManager->getLayerObjectFlag(handle->id);

	Polygon* polygon = new (layerObjectAllocator->allocate(sizeof(Polygon))) Polygon(handle, layerObjectFlag, layerObjectManager->getDrawElementAllocator());

	polygon->setLayerObjectDef(layerObjectDef, false);

	handle->setUserData(polygon);

	return polygon;
}



}