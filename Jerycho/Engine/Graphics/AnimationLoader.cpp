#include "Texture2dLoader.hpp"
#include "../Manager/ResourceManager.hpp"
#include "../Utility/Exception.hpp"
#include "../Math/Math.hpp"

/*using namespace tinyxml2;

namespace ja
{

	AnimationLoader* AnimationLoader::singleton = nullptr;

	AnimationLoader::AnimationLoader()
	{

	}

	AnimationLoader::~AnimationLoader()
	{

	}

	AnimationLoader* AnimationLoader::getSingleton()
	{
		return singleton;
	}

	void AnimationLoader::init()
	{
		if (singleton == nullptr)
		{
			singleton = new AnimationLoader();
		}
	}

	void AnimationLoader::cleanUp()
	{
		if (singleton != nullptr)
		{
			delete singleton;
			singleton = nullptr;
		}
	}

	Animation* AnimationLoader::loadAnimation(const ja::string& fileName, uint32_t resourceId)
	{
		XMLDocument doc; 
		XMLError status = doc.LoadFile(fileName.c_str());

		Animation* animation = nullptr;

		animation = new Animation(resourceId);

		if (status == XML_NO_ERROR)
		{
			XMLElement* animationElement = doc.FirstChildElement();

			loadImages(animationElement, animation);
			loadSequences(animationElement, animation);
		}
		else
		{
			std::cout << "Parsing file " << fileName.c_str() << " error: " << status << std::endl;
			std::cout << "Error string 1: " << doc.GetErrorStr1() << std::endl;
			std::cout << "Error string 2: " << doc.GetErrorStr2() << std::endl;

			delete animation;

			throw JA_EXCEPTION(ExceptionType::CANT_LOAD_ANIMATION);
		}

		return animation;
	}

	void AnimationLoader::loadImages(tinyxml2::XMLElement* animationElement, Animation* animation)
	{
		XMLElement* frames = animationElement->FirstChildElement("frames");
		ja::string animationPath = ResourceManager::getSingleton()->getAnimationsPath();
		ja::string imageName;
		int32_t    imagesCount = 0;
		Texture2d* frameImage;

		if (frames)
		{
			frames->QueryIntAttribute("number", &imagesCount);

			animation->animationFrames = new ImageFrame[imagesCount];

			animation->imagesCount = imagesCount;

			XMLElement* frame = frames->FirstChildElement("image");
			for (int i = 0; frame; frame = frame->NextSiblingElement())
			{
				imageName = frame->Attribute("file");
				frameImage = Texture2dLoader::getSingleton()->loadTexture(animationPath + imageName, setup::ID_UNASSIGNED);
				if (frameImage == nullptr)
					throw JA_EXCEPTIONR(ExceptionType::CANT_LOAD_TEXTURE, imageName);
				animation->animationFrames[i].frameId = i;
				animation->animationFrames[i].frameTexture = frameImage;
				i++;
			}

			animation->width = frameImage->getWidth();
			animation->height = frameImage->getHeight();
		}

	}

	void AnimationLoader::loadSequences(tinyxml2::XMLElement* animationElement, Animation* animation)
	{
		XMLElement* sequences = animationElement->FirstChildElement("sequences");
		XMLElement* frame;
		int32_t     sequencesNum = 0;
		int32_t     framesNum = 0;
		int32_t     time = 0;
		double      deltaTime;
		int32_t     frameId = 0;
		double      elapsedTime = 0;

		if (sequences)
		{
			sequences->QueryIntAttribute("number", &sequencesNum);
			animation->sequencesCount = sequencesNum;


			animation->animationSequences = new AnimationSequence[sequencesNum];

			XMLElement* sequence = sequences->FirstChildElement("sequence");
			ja::string sequenceName;

			for (int i = 0; sequence; sequence = sequence->NextSiblingElement())
			{
				sequence->QueryIntAttribute("frames", &framesNum);
				
				sequenceName = sequence->Attribute("name");

				animation->animationSequences[i].sequenceId = i;
				animation->animationSequences[i].frameCount = framesNum;
				if (sequenceName.empty()) {
					HashTagValue hashTag;
					animation->animationSequences[i].hashTag = hashTag;
				}
				else {
					HashTagValue hashTag(sequenceName.c_str());
					animation->animationSequences[i].hashTag = hashTag;
				}

				animation->animationSequences[i].frames = new SequenceFrame[framesNum];

				elapsedTime = 0;

				frame = sequence->FirstChildElement("frame");
				for (int32_t z = 0; frame; frame = frame->NextSiblingElement())
				{
					frame->QueryIntAttribute("time", &time);
					frame->QueryIntAttribute("id", &frameId);

					animation->animationSequences[i].frames[z].imageId = frameId;
					animation->animationSequences[i].frames[z].imageTexture = animation->animationFrames[frameId].frameTexture;
					animation->animationSequences[i].frames[z].sequenceFrameId = z;
					animation->animationSequences[i].frames[z].startTime = elapsedTime;
					deltaTime = static_cast<double>(time) / 1000.0;
					animation->animationSequences[i].frames[z].frameTime = deltaTime;
					elapsedTime += deltaTime;
					animation->animationSequences[i].frames[z].endTime = elapsedTime;
					z++;
				}
		
				animation->animationSequences[i].sequenceTime = elapsedTime;
				i++;
			}
		}
	}

}*/