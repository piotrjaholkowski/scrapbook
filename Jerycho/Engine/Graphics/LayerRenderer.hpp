#pragma once

#include "../jaSetup.hpp"
#include "../Allocators/StackAllocator.hpp"
#include "LayerObject.hpp"

namespace ja
{
	class Light;
	class RenderBuffer;

	bool layerObjectCompare(LayerObject* entityA, LayerObject* entityB);

	class LayerRenderer
	{
	private:
		LayerObject*  layerObjects[255][setup::graphics::MAX_LAYER_OBJECTS];

		uint32_t entitiesNum[255];
	public:
		LayerRenderer();
		void refresh(StackAllocator<gil::ObjectHandle2d*>* objectList);
		void renderLayer(uint8_t layerId);
		void renderLayer(uint8_t layerId, RenderBuffer& renderBuffer);

		void renderLayerObjectBounds(uint8_t layerId);
		void renderLayerObjectAABB(uint8_t layerId);

		void getLights(StackAllocator<Light*>& lights, uint8_t layerId);
	};

}
