#include "Texture2dLoader.hpp"
#include <IL/il.h>
#include "../Manager/DisplayManager.hpp"
#include "../Manager/LogManager.hpp"
#include "../Tests/TimeStamp.hpp"

namespace ja
{

Texture2dLoader*        Texture2dLoader::singleton = nullptr;
SDL_mutex*              Texture2dLoader::loadingMutex = nullptr;
SDL_cond*               Texture2dLoader::alreadyLoadedCondition = nullptr;
ScheduleTexture2dLoad   Texture2dLoader::scheduleTexture2dLoad;
ScheduleTexture2dDelete Texture2dLoader::scheduleTexture2dDelete;

Texture2dLoader::Texture2dLoader()
{
	ilInit();
	//ilutRenderer(ILUT_OPENGL);
	ilEnable(IL_ORIGIN_SET);
	
	loadingMutex           = SDL_CreateMutex();
	alreadyLoadedCondition = SDL_CreateCond();
}

Texture2dLoader::~Texture2dLoader()
{
	ilShutDown();

	SDL_DestroyMutex(loadingMutex);
	SDL_DestroyCond(alreadyLoadedCondition);
}

Texture2dLoader* Texture2dLoader::getSingleton()
{
	return singleton;
}

void Texture2dLoader::init()
{
	if(singleton==nullptr)
	{
		singleton = new Texture2dLoader();
	}
}

void Texture2dLoader::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

bool Texture2dLoader::loadTexture(const char* fileName, Texture2d* texture)
{
	if(SDL_ThreadID() != DisplayManager::getRenderingThreadId())
	{
		return scheduleLoadTexture2d(fileName, texture);
	}

	GLuint     textureId;
	uint32_t   handle,width,height;
	int32_t    format;
	int32_t    bpp;
	uint8_t*   data;
	float      glSupport;

	glSupport = DisplayManager::getSingleton()->getOpenglVersion();
	handle = ilGenImage();
	ilBindImage(handle);
	
	if(ilLoadImage((wchar_t*)fileName))
	{
		if(!ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE))
		{
			ilDeleteImage(handle);
			LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't load %s ilConvertImage fail", fileName);
			return false;
		}
		// mo�e b�dzie trzeba zrobi� skalowanie na power of 2
		width  = ilGetInteger(IL_IMAGE_WIDTH); 
		height = ilGetInteger(IL_IMAGE_HEIGHT);
		format = ilGetInteger(IL_FORMAT_MODE);
		//bytePerPixel = ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);
		bpp  = ilGetInteger(IL_IMAGE_BPP);
		data = ilGetData();

		glGetError();
		#ifdef JA_SDL_OPENGL
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glGenTextures(1, &textureId);
		
		glBindTexture(GL_TEXTURE_2D, textureId );
		//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
		//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE); // zastosuj efekty �wiat�a na teksturze
		//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL); // nie stosuj efekt�w �wiat�a
		
		/*glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_COMBINE);
		glTexEnvf(GL_TEXTURE_ENV,GL_COMBINE_RGB,GL_INTERPOLATE);
		glTexEnvf(GL_TEXTURE_ENV,GL_SOURCE0_RGB,GL_TEXTURE);
		glTexEnvf(GL_TEXTURE_ENV,GL_OPERAND0_RGB,GL_SRC_COLOR);
		glTexEnvf(GL_TEXTURE_ENV,GL_SOURCE1_RGB,GL_PREVIOUS);
		glTexEnvf(GL_TEXTURE_ENV,GL_OPERAND1_RGB,GL_SRC_COLOR);
		glTexEnvf(GL_TEXTURE_ENV,GL_SOURCE2_RGB,GL_CONSTANT);
		glTexEnvf(GL_TEXTURE_ENV,GL_OPERAND2_RGB,GL_SRC_ALPHA);*/

		//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST_MIPMAP_LINEAR); // Linear Filtering
		//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); 
	
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST); // it repairs seams
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST); 
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		//glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // powtarzaj

		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP); // przytnij jak powy�ej jeden
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP); // depracated in opengl 3.0 removed from 3.1

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); // przytnij jak powy�ej jeden
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


		/*if((glSupport >= 1.4) && (glSupport < 3.0))
		{
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE); // wymaga openGL 1.4
		}*/

		//It's for max avaliable mipmaps
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);

		glTexImage2D(GL_TEXTURE_2D,0,bpp,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE,data);
		
		//glGenerateMipmap(GL_TEXTURE_2D);
		//glTexImage2D(GL_TEXTURE_2D,1,bpp,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE,data);
		//glTexImage2D(GL_TEXTURE_2D,2,bpp,width,height,0,GL_RGBA,GL_UNSIGNED_BYTE,data);
		//gluBuild2DMipmaps() tego nie zaleca si� u�ywa�
		/*if(glSupport >= 3.0)
		{
		// to ustawi� przed texImage z ilo�ci� mipmap
		// w przypadku sterownik�w ati zrobi� glEnable(texutre_2d)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
		//////////////
			glGenerateMipmap(GL_TEXTURE_2D); //na razie obejdzie si� bez mipmap
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // wtedy najmiejszy trzeba zmieni� na to filtrowanie
			
		}*/
		#endif JA_SDL_OPENGL

		GLenum errorStatus = glGetError();
		if (errorStatus != GL_NO_ERROR)
		{
			//std::cout << "Error creating texture of " << fileName << " error status code: " << errorStatus << std::endl;
			LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't load %s error status code %d", fileName, errorStatus);
			ilDeleteImage(handle);
			return false;
		}
		
		//texture = new Texture2d(textureId, width, height, resourceId);
		texture->texture = textureId;
		texture->width   = width;
		texture->height  = height;

		ilDeleteImage(handle);
		
		return true;
	}
	else
	{
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't load %s lLoadImage fail\n", fileName);
		ilDeleteImage(handle);
	}

	return false;
}

bool Texture2dLoader::deleteTexture(Texture2d* texture)
{
	if (SDL_ThreadID() != DisplayManager::getRenderingThreadId())
	{
		return scheduleDeleteTexture2d(texture);
	}

	glDeleteTextures(1, &texture->texture);

	return true;
}

bool Texture2dLoader::scheduleLoadTexture2d(const char* fileName, Texture2d* texture)
{
	SDL_mutexP(loadingMutex);
		scheduleTexture2dLoad.fileName          = fileName;
		scheduleTexture2dLoad.schedludedTexture = texture;
		scheduleTexture2dLoad.scheduleOn        = true;
		SDL_CondWait(alreadyLoadedCondition,loadingMutex);
		if(scheduleTexture2dLoad.scheduleOn == true)
			SDL_CondWait(alreadyLoadedCondition,loadingMutex); // To be 100% sure that task is finalized
	SDL_mutexV(loadingMutex);

	return scheduleTexture2dLoad.result;
}

bool Texture2dLoader::scheduleDeleteTexture2d(Texture2d* texture)
{
	SDL_mutexP(loadingMutex);
	scheduleTexture2dDelete.schedludedTexture = texture;
	scheduleTexture2dDelete.scheduleOn = true;
	SDL_CondWait(alreadyLoadedCondition, loadingMutex);
	if (scheduleTexture2dDelete.scheduleOn == true)
		SDL_CondWait(alreadyLoadedCondition, loadingMutex); // To be 100% sure that task is finalized
	SDL_mutexV(loadingMutex);

	return scheduleTexture2dDelete.result;
}

void Texture2dLoader::queryScheduleLoading()
{
	PROFILER_FUN;

	SDL_mutexP(loadingMutex);

	if(scheduleTexture2dLoad.scheduleOn)
	{
		scheduleTexture2dLoad.result = loadTexture(scheduleTexture2dLoad.fileName.c_str(), scheduleTexture2dLoad.schedludedTexture);
		scheduleTexture2dLoad.scheduleOn = false;
	}

	if (scheduleTexture2dDelete.scheduleOn)
	{
		scheduleTexture2dDelete.result = deleteTexture(scheduleTexture2dDelete.schedludedTexture);
		scheduleTexture2dDelete.scheduleOn = false;
	}

	SDL_mutexV(loadingMutex);

	SDL_CondSignal(alreadyLoadedCondition);
}

}