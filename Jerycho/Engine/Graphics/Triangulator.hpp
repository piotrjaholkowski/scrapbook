#pragma once

#include "../Allocators/StackAllocator.hpp"
#include "../Allocators/LinearAllocator.hpp"
#include "../Math/VectorMath2d.hpp"

#include "../Manager/DisplayManager.hpp"
#include <gl\glu.h>

#include <list>

namespace ja
{

enum class TessWindingRule
{
	WINDING_ODD = GLU_TESS_WINDING_ODD, //Fill odd numbers, default setting
	WINDING_NONZERO = GLU_TESS_WINDING_NONZERO,
	WINDING_POSITIVE = GLU_TESS_WINDING_POSITIVE,
	WINDING_NEGATIVE = GLU_TESS_WINDING_NEGATIVE,
	WINDING_TESS_WINDING_ABS_GEQ_TWO = GLU_TESS_WINDING_ABS_GEQ_TWO
};

// helper for triangulator (context of triangulator)
class TessContext
{
public:
	// temporal list to store data for triangulation
	StackAllocator<jaVector2> vertices;
	StackAllocator<uint32_t>  indices;
	
	LinearAllocator* dataAllocator;

	// triangulation runtime data
	uint32_t currentPrimitiveType;
	uint32_t currentIndex;
	bool     isFirstVertex;
	uint32_t firstVertexIndex;
	bool     isSecondVertex;
	uint32_t secondVertexIndex;

	uint32_t windingRule;

	TessContext()
		: currentPrimitiveType(GL_NONE)
		, currentIndex(0)
		, isFirstVertex(false)
		, isSecondVertex(false)
		, windingRule((uint32_t)TessWindingRule::WINDING_ODD)
	{
		const uint32_t verticesPerBlock = 128;
		dataAllocator = new LinearAllocator(128 * 32, 32);
	}

	void clearContext()
	{
		this->currentPrimitiveType = GL_NONE;
		this->currentIndex   = 0;
		this->isFirstVertex  = false;
		this->isSecondVertex = false;

		dataAllocator->clear();
		vertices.clear();
		indices.clear();
	}
};

class Triangulator
{
public:
	Triangulator();
	~Triangulator();

	void beginTriangulation();
	void beginContour();
	void addVertex(const jaVector2& vertex);
	void endContour();
	void endTriangulation();

	inline void setWindingRule(TessWindingRule windingRule)
	{
		this->context.windingRule = (uint32_t)windingRule;
	}

	inline StackAllocator<jaVector2>* getVertices()
	{
		return &context.vertices;
	}

	inline StackAllocator<uint32_t>* getIndices()
	{
		return &context.indices;
	}

private:
	GLUtesselator * gluTesselator;
	TessContext     context;
};

}