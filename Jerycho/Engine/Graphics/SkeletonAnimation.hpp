#pragma once

#include <stdint.h>

#include "../jaSetup.hpp"
#include "Bone.hpp"

#include "../Utility/NameTag.hpp"
#include "../Allocators/CacheLineAllocator.hpp"

namespace ja
{
	class AnimationKeyFrame
	{
	public:
		Bone*  bones;		
		float  normalizedTime;
	};

	typedef NameTag<setup::TAG_NAME_LENGTH> AnimationName;

	class SkeletonAnimation
	{
	private:

	public:
		AnimationKeyFrame* keyFrames;
		uint16_t           keyFramesNum;
		uint16_t           bonesNum;
		float              animationTime;
		
		CacheLineAllocator* animationAllocator;

		AnimationName animationName;

		SkeletonAnimation(const char* animationName, uint16_t bonesNum, CacheLineAllocator* animationAllocator) : animationAllocator(animationAllocator), bonesNum(bonesNum)
		{
			this->animationName.setName(animationName);
			this->keyFrames     = nullptr;
			this->keyFramesNum  = 0;		
			this->animationTime = 0.0f;
		}

		~SkeletonAnimation()
		{
			if (nullptr != keyFrames)
			{
				for (int32_t i = 0; i < keyFramesNum; ++i)
				{
					this->animationAllocator->free(this->keyFrames[i].bones, this->bonesNum * sizeof(Bone));
				}

				this->animationAllocator->free(this->keyFrames, this->keyFramesNum * sizeof(AnimationKeyFrame));
			}
		}

		void interpolateFrames(const AnimationKeyFrame& frame0, const AnimationKeyFrame& frame1, float normalizedTime, Bone* bones)
		{
			float interpolationLength = frame1.normalizedTime - frame0.normalizedTime;

			float interpolation = normalizedTime / interpolationLength;

			Bone* bones0 = frame0.bones;
			Bone* bones1 = frame1.bones;

			for (uint16_t boneId = 0; boneId < this->bonesNum; ++boneId)
			{
				const float boneLengthDiff = bones1[boneId].transformation.length - bones0[boneId].transformation.length;

				const float boneRot0 = bones0[boneId].transformation.rotMat.getRadians();
				const float boneRot1 = bones1[boneId].transformation.rotMat.getRadians();

				const float boneAngleDiff  =  boneRot1 - boneRot0;

				const float boneDistance = bones0[boneId].transformation.length + (interpolation * boneLengthDiff);
				const float boneRot      = boneRot0 + (interpolation * boneAngleDiff);

				bones[boneId].transformation.length = boneDistance;
				bones[boneId].transformation.rotMat.setRadians(boneRot);
			}
		}

		void setAnimationPose(float normalizedTime, Bone* bones)
		{
			uint16_t i = 0;

			if (this->keyFramesNum < 2)
				return;

			for (i; i < this->keyFramesNum; ++i)
			{
				if (this->keyFrames[i].normalizedTime >= normalizedTime)
				{
					break;
				}
			}

			if (0 == i)
			{
				memcpy(bones, this->keyFrames[0].bones, this->bonesNum * sizeof(Bone));
				return;
			}

			if (this->keyFramesNum == i)
			{
				memcpy(bones, this->keyFrames[this->keyFramesNum - 1].bones, this->bonesNum * sizeof(Bone));
				return;
			}
			
			interpolateFrames(this->keyFrames[i - 1], this->keyFrames[i], normalizedTime, bones);
		}

		inline const char* getName()
		{
			return this->animationName.getName();
		}

		inline uint16_t getNameHashCode()
		{
			return this->animationName.getHash();
		}

		inline void setName(const char* name)
		{
			this->animationName.setName(name);
		}

		inline float getAnimationTime()
		{
			return this->animationTime;
		}

		inline void setAnimationTime(float time)
		{
			this->animationTime = time;
		}

		inline uint16_t getKeyFramesNum()
		{
			return this->keyFramesNum;
		}

		inline AnimationKeyFrame* getKeyFrames()
		{
			return this->keyFrames;
		}

		void addKeyFrame(float normalizedTime, const Bone* bones)
		{
			uint16_t i = 0;

			for (i = 0; i < this->keyFramesNum; ++i)
			{
				if (this->keyFrames[i].normalizedTime > normalizedTime)
				{
					break;
				}
			}

			AnimationKeyFrame* newKeyFrames = (AnimationKeyFrame*)this->animationAllocator->allocate( (this->keyFramesNum + 1) * sizeof(AnimationKeyFrame) );
			memcpy(newKeyFrames, this->keyFrames, i * sizeof(AnimationKeyFrame));
			memcpy(&newKeyFrames[i + 1], &this->keyFrames[i], (this->keyFramesNum - i) * sizeof(AnimationKeyFrame) );

			if (nullptr != this->keyFrames)
			{
				this->animationAllocator->free(this->keyFrames, this->keyFramesNum * sizeof(AnimationKeyFrame));
			}

			newKeyFrames[i].normalizedTime = normalizedTime;
			uint32_t boneAllocationSize = bonesNum * sizeof(Bone);
			newKeyFrames[i].bones = (Bone*)this->animationAllocator->allocate(boneAllocationSize);
			memcpy(newKeyFrames[i].bones, bones, boneAllocationSize);		

			this->keyFrames = newKeyFrames;

			++this->keyFramesNum;
		}

		void deleteKeyFrame(uint16_t keyFrameId)
		{

		}
	};

	class SkeletalAnimations
	{
	private:
		uint16_t            skeletonOwnerId;
		SkeletonAnimation** skeletonAnimations;
		uint16_t            animationsNum;
		uint16_t            animationReferencesNum;

		CacheLineAllocator* animationAllocator;
	public:

		SkeletalAnimations(uint16_t skeletonOwnerId, CacheLineAllocator* animationAllocator);
		~SkeletalAnimations();

		SkeletonAnimation* createAnimation(const char* animationName, uint16_t bonesNum)
		{
			if (nullptr == this->skeletonAnimations)
			{
				this->skeletonAnimations = (SkeletonAnimation**)this->animationAllocator->allocate(sizeof(SkeletonAnimation*));
			}
			else
			{
				SkeletonAnimation** tempMem = (SkeletonAnimation**)this->animationAllocator->allocate((animationsNum + 1) * sizeof(SkeletonAnimation*));
				memcpy(tempMem, this->skeletonAnimations, animationsNum * sizeof(SkeletonAnimation*));
				this->animationAllocator->free(this->skeletonAnimations, animationsNum * sizeof(SkeletonAnimation*));
				this->skeletonAnimations = tempMem;
			}

			SkeletonAnimation* newAnimation = new (this->animationAllocator->allocate(sizeof(SkeletonAnimation))) SkeletonAnimation(animationName, bonesNum, this->animationAllocator);
			this->skeletonAnimations[animationsNum] = newAnimation;

			++this->animationsNum;

			return newAnimation;
		}

		bool removeAnimation(SkeletonAnimation* animation)
		{
			bool     animationFound = false;
			uint16_t animationIt    = 0;

			for (animationIt; animationIt < animationsNum; ++animationIt)
			{
				if (skeletonAnimations[animationIt] == animation)
				{
					animationFound = true;
					break;
				}
			}

			if (!animationFound)
				return false;

			SkeletonAnimation** newAnimations = (SkeletonAnimation**)this->animationAllocator->allocate((animationsNum - 1) * sizeof(SkeletonAnimation*));

			{
				//copy old data
				memcpy(newAnimations, this->skeletonAnimations, animationIt * sizeof(SkeletonAnimation*));
				memcpy(&newAnimations[animationIt], &this->skeletonAnimations[animationIt + 1], (animationsNum - 1 - animationIt) * sizeof(SkeletonAnimation*));
			}

			animation->~SkeletonAnimation();
			this->animationAllocator->free(animation, sizeof(SkeletonAnimation));

			this->animationAllocator->free(skeletonAnimations, this->animationsNum * sizeof(SkeletonAnimation*));
			this->skeletonAnimations = newAnimations;

			--this->animationsNum;
			return true;
		}

		inline SkeletonAnimation* getAnimation(uint16_t hashCode)
		{
			for (uint16_t i = 0; i < animationsNum; ++i)
			{
				if (hashCode == skeletonAnimations[i]->getNameHashCode())
				{
					return skeletonAnimations[i];
				}
			}

			return nullptr;
		}

		inline SkeletonAnimation* getAnimation(const char* animationName)
		{
			NameTag<setup::TAG_NAME_LENGTH> nameTag;
			nameTag.setName(animationName);

			return getAnimation(nameTag.getHash());	
		}

		inline SkeletonAnimation** getSkeletonAnimations()
		{
			return this->skeletonAnimations;
		}

		inline uint16_t getAnimationsNum()
		{
			return this->animationsNum;
		}

		uint16_t getOwnerId()
		{
			return this->skeletonOwnerId;
		}

		void removeReference()
		{
			--animationReferencesNum;
		}

		uint16_t getReferencesNum()
		{
			return this->animationReferencesNum;
		}

		void rebindOwner();
	};
}