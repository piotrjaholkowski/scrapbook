/*#include "Animation.hpp"

using namespace ja;

ImageFrame::ImageFrame()
{
	this->frameTexture = nullptr;
}

ImageFrame::~ImageFrame()
{
	if(frameTexture != nullptr)
	{
		delete frameTexture;
		frameTexture = nullptr;
	}
}

void ImageFrame::clear()
{
	if(frameTexture != nullptr)
	{
		frameTexture->~Texture2d();
		frameTexture = nullptr;
	}
}

AnimationSequence::AnimationSequence()
{
	frames = nullptr;
}

AnimationSequence::~AnimationSequence()
{
	if(frames != nullptr)
	{
		delete[] frames;
		frames = nullptr;
	}
}

void AnimationSequence::clear()
{
	if(frames != nullptr)
	{
		frames = nullptr;
	}
}

Animation::Animation(unsigned int resourceId, bool allocatedByAllocator) : resourceId(resourceId), allocatedByAllocator(allocatedByAllocator)
{
	animationSequences = nullptr;
	imagesCount = 0;
	sequencesCount = 0;
	animationFrames = nullptr;
}

Animation::~Animation()
{
	if(animationFrames != nullptr)
	{
		if(allocatedByAllocator)
		{
			for(unsigned short i=0; i<imagesCount; ++i)
			{
				animationFrames[i].clear();
			}
		}
		else
		{
			delete[] animationFrames;
		}
		
		animationFrames = nullptr;
	}
	
	if(animationSequences != nullptr)
	{
		if(allocatedByAllocator)
		{
			for(unsigned short i=0; i<sequencesCount; ++i)
			{
				animationSequences[i].clear();
			}
		}
		else
		{
			delete[] animationSequences;
		}
		
		animationSequences = nullptr;
	}
}*/
