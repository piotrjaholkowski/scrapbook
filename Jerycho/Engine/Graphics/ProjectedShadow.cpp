#include "ProjectedShadow.hpp"
#include "LayerRenderer.hpp"

#include "../Manager/LayerObjectManager.hpp"

using namespace gil;

namespace ja
{

void ProjectedShadow::getLights(LayerRenderer* layerRenderer)
{
	for (uint8_t i = 0; i<255; i++)
	{
		lights[i].clear();
		layerRenderer->getLights(lights[i], i);
	}
}

void ProjectedShadow::projectPointLightShadow(gil::ObjectHandle2d* lightHandle, gil::Circle2d* pointShape, Polygon* polygon)
{
	

	
}

void ProjectedShadow::generateShadows(LayerRenderer* layerRenderer)
{
	shadowVertices.clear();
	shadowIndices.clear();

	getLights(layerRenderer);

	LayerObjectManager* layerObjectManager = LayerObjectManager::getSingleton();

	for (uint8_t i = 0; i < 255; ++i)
	{
		const Light**  layerLights    = (const Light**)lights[i].getFirst();
		const uint16_t layerLightsNum = lights[i].getSize();

		for (uint16_t lightIt = 0; lightIt < layerLightsNum; ++lightIt)
		{
			const Light* layerLight = layerLights[lightIt];
			if (layerLight->isShadowProjector())
			{
				temporaryObjectList.clear();

				gil::ObjectHandle2d* lightHandle = layerLight->getHandle();
				gil::Shape2d*        lightShape  = lightHandle->getShape();
				
				gil::AABB2d lightAABB = lightHandle->getAABB2d();
				layerObjectManager->getLayerObjectsAtAABB(lightAABB, &temporaryObjectList);

				const uint32_t objectsNum = temporaryObjectList.getSize();
				gil::ObjectHandle2d** objects = temporaryObjectList.getFirst();

				if((uint8_t)ShapeType::Circle == lightShape->getType())
				{
					for (uint32_t objectIt = 0; objectIt < objectsNum; ++objectIt)
					{
						LayerObject* layerObject = (LayerObject*)objects[objectIt]->getUserData();

						if ((uint8_t)LayerObjectType::POLYGON == layerObject->getObjectType())
						{
							projectPointLightShadow(lightHandle, (gil::Circle2d*)lightShape, (Polygon*)layerObject);
						}
					}
				}				
			}
		}
	}
}

}