#include "Shader.hpp"
#include "../Graphics/ShaderLoader.hpp"

namespace ja
{ 

Shader::Shader(uint32_t resourceId) : resourceId(resourceId), shaderProcId(0)
{
	
}

Shader::~Shader()
{
	clear();
}

void Shader::clear()
{
	if (0 != shaderProcId)
	{
		
		ShaderLoader::deleteShader(this);
	}
}

}