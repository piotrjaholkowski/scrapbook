#include "Parallax.hpp"

namespace ja
{

	Parallax::Parallax(const char* layerName, Camera* camera, bool isActiveCamera) : isActiveCamera(isActiveCamera), camera(camera)
	{
		this->parallaxTag.setName(layerName);
	}
}