#pragma once

#include "../Utility/NameTag.hpp"

namespace ja
{
	typedef NameTag<setup::TAG_NAME_LENGTH> ParallaxTag;

	class Camera;

	class Parallax
	{
	private:
		ParallaxTag parallaxTag;
	public:

		Camera* camera;
		bool    isActive;
		bool    isActiveCamera;

		Parallax(const char* layerName, Camera* camera, bool isActiveCamera);

		inline void setName(const char* name)
		{
			parallaxTag.setName(name);
		}

		inline const char* getName() const
		{
			return parallaxTag.getName();
		}

		inline uint32_t getHash() const
		{
			return parallaxTag.getHash();
		}
	};

}