#include "ShaderLoader.hpp"
#include "Shader.hpp"
#include "../Manager/DisplayManager.hpp"
#include "../Manager/EffectManager.hpp"
#include "../Manager/LogManager.hpp"
#include "../Utility/Exception.hpp"
#include "../Tests/TimeStamp.hpp"

#include <iostream>
#include <fstream>

namespace ja
{

ShaderLoader*              ShaderLoader::singleton = nullptr;
SDL_mutex*                 ShaderLoader::loadingMutex = nullptr;
SDL_cond*                  ShaderLoader::alreadyLoadedCondition = nullptr;
ScheduleShaderLoad         ShaderLoader::scheduleShaderLoad;
ScheduleShaderDelete       ShaderLoader::scheduleShaderDelete;
//ScheduleFbo               ShaderLoader::scheduleFbo;
ScheduleShaderBinderCreate ShaderLoader::scheduleShaderBinderCreate;
ScheduleShaderBinderDelete ShaderLoader::scheduleShaderBinderDelete;

ShaderLoader::ShaderLoader()
{
	loadingMutex = SDL_CreateMutex();
	alreadyLoadedCondition = SDL_CreateCond();
}

ShaderLoader::~ShaderLoader()
{
	SDL_DestroyMutex(loadingMutex);
	SDL_DestroyCond(alreadyLoadedCondition);
}

ShaderLoader* ShaderLoader::getSingleton()
{
	return singleton;
}

void ShaderLoader::init()
{
	if(singleton==nullptr)
	{
		singleton = new ShaderLoader();
	}
}

void ShaderLoader::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

bool ShaderLoader::loadShader(const char* fileName, ShaderType shaderType, Shader* shader)
{
	if(SDL_ThreadID() != DisplayManager::getRenderingThreadId())
	{
		return scheduleLoadShader(fileName,shaderType,shader);
	}

	shader->shaderType = shaderType;

	switch(shaderType)
	{
	case ShaderType::FRAGMENT_SHADER:
		shader->shaderProcId = glCreateShader(GL_FRAGMENT_SHADER);
		break;
	case ShaderType::VERTEX_SHADER:
		shader->shaderProcId = glCreateShader(GL_VERTEX_SHADER);
		break;
	case ShaderType::GEOMETRY_SHADER:
		shader->shaderProcId = glCreateShader(GL_GEOMETRY_SHADER);
	}
	
	std::ifstream ifs(fileName, std::ios::in | std::ios::binary | std::ios::ate);

	if(ifs.is_open())
	{
		uint32_t size = (uint32_t)(ifs.tellg());
	
		char* memblock = new char [size + 1];
		ifs.seekg (0, std::ios::beg);
		ifs.read (memblock, size);
		memblock[size] = '\0';
		const char* textPointer = memblock;
		ifs.close();

		glShaderSource(shader->shaderProcId, 1, &textPointer, 0); // 0 if null terminated
		glCompileShader(shader->shaderProcId);

		delete[] memblock;

		int32_t status;
		if(DisplayManager::getOpenglVersion() >= 2.0)
		{
			glGetShaderiv(shader->shaderProcId, GL_COMPILE_STATUS, &status);
			if(status == GL_FALSE)
			{
				glGetShaderInfoLog(shader->shaderProcId, 1024, &singleton->length, singleton->debugInfo);

				LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Shader %s could not be compiled: %s", fileName, singleton->debugInfo);

				return false;
			}
		}
		else
		{
			glGetObjectParameterivARB(shader->shaderProcId, GL_OBJECT_COMPILE_STATUS_ARB, &status);
			if(status == GL_FALSE)
			{
				glGetInfoLogARB(shader->shaderProcId, 1024, &singleton->length, singleton->debugInfo);

				LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Shader %s could not be compiled: %s", fileName, singleton->debugInfo);
				
				return false;
			}
		}	

		return true;
	}
	else
	{
		LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Can't open %s\n", fileName);
	}

	return false; 
}

bool ShaderLoader::deleteShader(Shader* shader)
{
	if (SDL_ThreadID() != DisplayManager::getRenderingThreadId())
	{
		return scheduleDeleteShader(shader);
	}

	glDeleteShader(shader->shaderProcId);

	return true;
}

/*void ShaderLoader::createFbo( const char* fboName, float widthPercent, float heightPercent, int32_t textureFlag, bool linear )
{
	if(SDL_ThreadID() != DisplayManager::getRenderingThreadId())
	{
		return scheduleCreateFbo(fboName,widthPercent,heightPercent,textureFlag,linear);
	}

	EffectManager::getSingleton()->addFbo(fboName, widthPercent, heightPercent, textureFlag, linear);
}*/

const char* getGLSLTypeName(uint32_t type)
{

#define CASE_GL_TYPE_NAME(CASE_TYPE) \
	case CASE_TYPE: return #CASE_TYPE; break;

	switch (type)
	{
		CASE_GL_TYPE_NAME(GL_FLOAT);
		CASE_GL_TYPE_NAME(GL_FLOAT_VEC2);
		CASE_GL_TYPE_NAME(GL_FLOAT_VEC3);
		CASE_GL_TYPE_NAME(GL_FLOAT_VEC4);
		CASE_GL_TYPE_NAME(GL_INT);
		CASE_GL_TYPE_NAME(GL_INT_VEC2);
		CASE_GL_TYPE_NAME(GL_INT_VEC3);
		CASE_GL_TYPE_NAME(GL_INT_VEC4);
		CASE_GL_TYPE_NAME(GL_BOOL);
		CASE_GL_TYPE_NAME(GL_BOOL_VEC2);
		CASE_GL_TYPE_NAME(GL_BOOL_VEC3);
		CASE_GL_TYPE_NAME(GL_BOOL_VEC4);
		CASE_GL_TYPE_NAME(GL_FLOAT_MAT2);
		CASE_GL_TYPE_NAME(GL_FLOAT_MAT3);
		CASE_GL_TYPE_NAME(GL_FLOAT_MAT4);
		CASE_GL_TYPE_NAME(GL_SAMPLER_2D);
		CASE_GL_TYPE_NAME(GL_SAMPLER_CUBE);
	}

	return "UNDEFINED";
}

GLuint getGLSLUniformSize(GLint type)
{
	GLuint s;

#define CASE_GL_TYPE_SIZE(type, numElementsInType, elementType) \
      case type : s = numElementsInType * sizeof(elementType); break;

	switch (type)
	{
		CASE_GL_TYPE_SIZE(GL_FLOAT, 1, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_VEC2, 2, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_VEC3, 3, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_VEC4, 4, GLfloat);
		CASE_GL_TYPE_SIZE(GL_INT, 1, GLint);
		CASE_GL_TYPE_SIZE(GL_INT_VEC2, 2, GLint);
		CASE_GL_TYPE_SIZE(GL_INT_VEC3, 3, GLint);
		CASE_GL_TYPE_SIZE(GL_INT_VEC4, 4, GLint);
		CASE_GL_TYPE_SIZE(GL_UNSIGNED_INT, 1, GLuint);
		CASE_GL_TYPE_SIZE(GL_UNSIGNED_INT_VEC2, 2, GLuint);
		CASE_GL_TYPE_SIZE(GL_UNSIGNED_INT_VEC3, 3, GLuint);
		CASE_GL_TYPE_SIZE(GL_UNSIGNED_INT_VEC4, 4, GLuint);
		CASE_GL_TYPE_SIZE(GL_BOOL, 1, sizeof(int32_t));           //CASE_GL_TYPE_SIZE(GL_BOOL, 1, GLboolean); 
		CASE_GL_TYPE_SIZE(GL_BOOL_VEC2, 2, sizeof(int32_t));      //CASE_GL_TYPE_SIZE(GL_BOOL_VEC2, 2, GLboolean);
		CASE_GL_TYPE_SIZE(GL_BOOL_VEC3, 3, sizeof(int32_t));      //CASE_GL_TYPE_SIZE(GL_BOOL_VEC3, 3, GLboolean);
		CASE_GL_TYPE_SIZE(GL_BOOL_VEC4, 4, sizeof(int32_t)); //CASE_GL_TYPE_SIZE(GL_BOOL_VEC4, 4, GLboolean);
		CASE_GL_TYPE_SIZE(GL_FLOAT_MAT2, 4, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_MAT3, 9, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_MAT4, 16, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_MAT2x3, 6, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_MAT2x4, 8, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_MAT3x2, 6, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_MAT3x4, 12, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_MAT4x2, 8, GLfloat);
		CASE_GL_TYPE_SIZE(GL_FLOAT_MAT4x3, 12, GLfloat);
	default: s = 0; break;
	}
	return s;
}

void ShaderLoader::logActiveUniforms(const ShaderBinder* shaderBinder)
{
	LogManager* pLogManager = LogManager::getSingleton();

	int32_t activeUniformsNum;

	//GL_ACTIVE_ATTRIBUTES
	glGetProgramiv(shaderBinder->shaderProgramId, GL_ACTIVE_UNIFORMS, &activeUniformsNum);

	const int32_t maxUniformNameLength = 512;
	char     uniformName[maxUniformNameLength];
	int32_t  actualNameLength = 0;
	int32_t  uniformSize;
	uint32_t uniformType;

	pLogManager->addLogMessage(LogType::INFO_LOG, "Uniforms:");

	for (int32_t i = 0; i < activeUniformsNum; ++i)
	{
		glGetActiveUniform(shaderBinder->shaderProgramId, i, maxUniformNameLength, &actualNameLength, &uniformSize, &uniformType, uniformName);

		const char* typeName = getGLSLTypeName(uniformType);

		pLogManager->addLogMessage(LogType::INFO_LOG, "name = %s, type = %s, size = %d", uniformName, typeName, uniformSize);
	}
}

void ShaderLoader::logActiveUniformBlocks(const ShaderBinder* shaderBinder)
{
	LogManager* pLogManager = LogManager::getSingleton();

	// get number of uniform blocks in shader
	int32_t activeUniformBlocksNum;
	glGetProgramiv(shaderBinder->shaderProgramId, GL_ACTIVE_UNIFORM_BLOCKS, &activeUniformBlocksNum);

	const int32_t maxUniformBlockNameLength = 512;
	const int32_t maxUniformNameLength      = 512;
	const int32_t maxUniformIndices         = 1024;
	
	int32_t uniformBlockNameLength;
	int32_t uniformBlockSize;
	int32_t numberOfUniformsInBlock;

	int32_t  uniformOffset, uniformSize;
	uint32_t uniformType;
	int32_t  arrayStride, matrixStride;
	int32_t  actualUniformNameLength = 0;

	char    uniformBlockName[maxUniformBlockNameLength];
	char    uniformName[maxUniformNameLength];
	int32_t uniformIndices[maxUniformIndices];

	pLogManager->addLogMessage(LogType::INFO_LOG, "Uniform blocks:");

	for (int32_t uniformBlockIt = 0; uniformBlockIt < activeUniformBlocksNum; ++uniformBlockIt)
	{
		glGetActiveUniformBlockName(shaderBinder->shaderProgramId, uniformBlockIt, maxUniformBlockNameLength, &uniformBlockNameLength, uniformBlockName);
		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId,   uniformBlockIt, GL_UNIFORM_BLOCK_DATA_SIZE, &uniformBlockSize);
		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId,   uniformBlockIt, GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS, &numberOfUniformsInBlock);

		pLogManager->addLogMessage(LogType::INFO_LOG, "block name = %s, size = %d", uniformBlockName, uniformBlockSize);

		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId, uniformBlockIt, GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES, uniformIndices);

		pLogManager->addLogMessage(LogType::INFO_LOG, "block uniforms:");
		for (int32_t uniformMemberIt = 0; uniformMemberIt<numberOfUniformsInBlock; uniformMemberIt++)
		{
			if (uniformIndices[uniformMemberIt] >= 0)
			{
				uint32_t uniformIndex = (uint32_t)uniformIndices[uniformMemberIt];

				// get length of name of uniform variable
				glGetActiveUniform(shaderBinder->shaderProgramId, uniformIndex, maxUniformNameLength, &actualUniformNameLength, &uniformSize, &uniformType, uniformName);

				// get offset of uniform variable related to start of uniform block
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_OFFSET, &uniformOffset);
				// get size of uniform variable (number of elements)
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_SIZE, &uniformSize);
				// get type of uniform variable (size depends on this value)
				//glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_TYPE, &uniformType);
				// offset between two elements of the array
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_ARRAY_STRIDE, &arrayStride);
				// offset between two vectors in matrix
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_MATRIX_STRIDE, &matrixStride);

				// Size of uniform variable in bytes
				const uint32_t sizeInBytes = uniformSize * getGLSLUniformSize(uniformType);
				const char*    uniformTypeName = getGLSLTypeName(uniformType);

				pLogManager->addLogMessage(LogType::INFO_LOG, "name = %s, size = %u, offset = %d, type = %s, arrayStride = %d, matrixStride = %d", uniformName, sizeInBytes, uniformOffset, uniformTypeName, arrayStride, matrixStride);
			}
			else
			{
				pLogManager->addLogMessage(LogType::INFO_LOG, "Bad uniform");
			}
		}
	}	
}

void ShaderLoader::logUniformBlockInfo(const ShaderBinder* shaderBinder, UniformBlockInfo* pUniformBlock)
{
	LogManager* pLogManager = LogManager::getSingleton();

	pLogManager->addLogMessage(LogType::INFO_LOG, "Uniform block: %s", pUniformBlock->tagName.getName());

	const UniformField* pUniformField = pUniformBlock->fields.getFirst();
	const uint32_t      fieldsNum     = pUniformBlock->fields.getSize();

	if (GL_INVALID_INDEX == pUniformBlock->uniformBlockIndex)
	{
		pLogManager->addLogMessage(LogType::INFO_LOG, "Uniform block not defined");
		return;
	}

	pLogManager->addLogMessage(LogType::INFO_LOG, "size = %u", pUniformBlock->uniformBlockSize);
	pLogManager->addLogMessage(LogType::INFO_LOG, "uniform fields:");

	for (uint32_t i = 0; i < fieldsNum; ++i)
	{
		const char* fieldTypeName = getGLSLTypeName(pUniformField[i].fieldType);
		pLogManager->addLogMessage(LogType::INFO_LOG, "name = %s, type = %s, offset = %u, elementsNum = %u, arrayStride = %u", pUniformField[i].tagName.getName(), fieldTypeName, pUniformField[i].fieldOffset, pUniformField[i].elementsNum, pUniformField[i].arrayStride);
	}
}

void ShaderLoader::getMaterialUniformBlock(ShaderBinder* shaderBinder, const char* uniformBlockName)
{
	const int32_t maxUniformNameLength = 512;
	const int32_t maxUniformIndices    = 1024;

	int32_t numberOfUniformsInBlock;

	int32_t  uniformOffset, uniformSize;
	uint32_t uniformType;
	int32_t  arrayStride, matrixStride;
	int32_t  actualUniformNameLength = 0;

	char    uniformName[maxUniformNameLength];
	int32_t uniformIndices[maxUniformIndices];

	const char materialBeginStruct[] = "materials[0].";
	const char materialEndStruct[]   = "materials[1].";

	int32_t uniformStructureSize = INT_MAX;

	UniformBlockInfo* pUniformBlockInfo = &shaderBinder->materialUniformBlock;

	{
		pUniformBlockInfo->uniformBlockIndex = glGetUniformBlockIndex(shaderBinder->shaderProgramId, uniformBlockName);
		pUniformBlockInfo->tagName.setName(uniformBlockName);

		glGenBuffers(1, &shaderBinder->materialUniformBlockHandle);
		pUniformBlockInfo->uniformBlockHandle = shaderBinder->materialUniformBlockHandle;

		if (GL_INVALID_INDEX == pUniformBlockInfo->uniformBlockIndex)
		{
			return;
		}

		glUniformBlockBinding(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, setup::graphics::MATERIAL_UNIFORM_BLOCK_BINDING_POINT);
		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &pUniformBlockInfo->uniformBlockSize);

		if (pUniformBlockInfo->uniformBlockSize > 0)
		{
			pUniformBlockInfo->uniformBuffer = new uint8_t[pUniformBlockInfo->uniformBlockSize];
			memset(pUniformBlockInfo->uniformBuffer , 0, pUniformBlockInfo->uniformBlockSize);
		}
		
		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS, &numberOfUniformsInBlock);
		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES, uniformIndices);

		for (int32_t uniformMemberIt = 0; uniformMemberIt<numberOfUniformsInBlock; uniformMemberIt++)
		{
			if (uniformIndices[uniformMemberIt] >= 0)
			{
				uint32_t uniformIndex = (uint32_t)uniformIndices[uniformMemberIt];

				// get length of name of uniform variable
				glGetActiveUniform(shaderBinder->shaderProgramId, uniformIndex, maxUniformNameLength, &actualUniformNameLength, &uniformSize, &uniformType, uniformName);

				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_OFFSET, &uniformOffset);
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_SIZE, &uniformSize);
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_ARRAY_STRIDE, &arrayStride);
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_MATRIX_STRIDE, &matrixStride);


				char* pMaterialBeginFound = strstr(uniformName, materialBeginStruct);
				
				if (nullptr != pMaterialBeginFound)
				{
					char* fieldName = (pMaterialBeginFound + sizeof(materialBeginStruct) - 1);
					
					if (uniformSize > 1)
						replaceChar(fieldName, '[', '\0');

					new(pUniformBlockInfo->fields.push()) UniformField(fieldName, uniformOffset, uniformSize, arrayStride, uniformType);
				}
				else
				{
					const char* pMaterialEndFound = strstr(uniformName, materialEndStruct);
					if (nullptr != pMaterialEndFound)
					{
						if (uniformStructureSize > uniformOffset)
							uniformStructureSize = uniformOffset;
					}
					else
					{
						break;
					}
				}
			}
		}
	}

	//pUniformBlockInfo->uniformBlockSize = uniformStructureSize;
	shaderBinder->materialSize = uniformStructureSize;
}

void ShaderLoader::getSettingUniformBlock(ShaderBinder* shaderBinder, const char* uniformBlockName)
{
	const int32_t maxUniformNameLength = 512;
	const int32_t maxUniformIndices    = 1024;

	int32_t numberOfUniformsInBlock;

	int32_t  uniformOffset, uniformSize;
	uint32_t uniformType;
	int32_t  arrayStride, matrixStride;
	int32_t  actualUniformNameLength = 0;

	char    uniformName[maxUniformNameLength];
	int32_t uniformIndices[maxUniformIndices];

	UniformBlockInfo* pUniformBlockInfo = &shaderBinder->settingUniformBlock;

	{
		pUniformBlockInfo->uniformBlockIndex = glGetUniformBlockIndex(shaderBinder->shaderProgramId, uniformBlockName);
		pUniformBlockInfo->tagName.setName(uniformBlockName);

		glGenBuffers(1, &shaderBinder->settingUniformBlockHandle);
		pUniformBlockInfo->uniformBlockHandle = shaderBinder->settingUniformBlockHandle;

		if (GL_INVALID_INDEX == pUniformBlockInfo->uniformBlockIndex)
		{
			return;
		}

		glUniformBlockBinding(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, setup::graphics::SETTING_UNIFORM_BLOCK_BINDING_POINT);
		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &pUniformBlockInfo->uniformBlockSize);

		if (pUniformBlockInfo->uniformBlockSize > 0)
		{
			pUniformBlockInfo->uniformBuffer = new uint8_t[pUniformBlockInfo->uniformBlockSize];
			memset(pUniformBlockInfo->uniformBuffer, 0, pUniformBlockInfo->uniformBlockSize);
		}

		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS, &numberOfUniformsInBlock);
		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES, uniformIndices);

		for (int32_t uniformMemberIt = 0; uniformMemberIt<numberOfUniformsInBlock; uniformMemberIt++)
		{
			if (uniformIndices[uniformMemberIt] >= 0)
			{
				uint32_t uniformIndex = (uint32_t)uniformIndices[uniformMemberIt];

				// get length of name of uniform variable
				glGetActiveUniform(shaderBinder->shaderProgramId, uniformIndex, maxUniformNameLength, &actualUniformNameLength, &uniformSize, &uniformType, uniformName);

				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_OFFSET, &uniformOffset);
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_SIZE, &uniformSize);
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_ARRAY_STRIDE, &arrayStride);
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_MATRIX_STRIDE, &matrixStride);

				new(pUniformBlockInfo->fields.push()) UniformField(uniformName, uniformOffset, uniformSize, arrayStride, uniformType);	
			}
		}
	}
}

void ShaderLoader::getShaderObjectUniformBlock(ShaderBinder* shaderBinder, const char* uniformBlockName)
{
	const int32_t maxUniformNameLength = 512;
	const int32_t maxUniformIndices    = 1024;

	int32_t numberOfUniformsInBlock;

	int32_t  uniformOffset, uniformSize;
	uint32_t uniformType;
	int32_t  arrayStride, matrixStride;
	int32_t  actualUniformNameLength = 0;

	char    uniformName[maxUniformNameLength];
	int32_t uniformIndices[maxUniformIndices];

	const char objectBeginStruct[] = "objects[0].";
	const char objectEndStruct[]   = "objects[1].";

	int32_t uniformStructureSize = INT_MAX;

	UniformBlockInfo* pUniformBlockInfo = &shaderBinder->shaderObjectUniformBlock;

	{
		pUniformBlockInfo->uniformBlockIndex = glGetUniformBlockIndex(shaderBinder->shaderProgramId, uniformBlockName);
		pUniformBlockInfo->tagName.setName(uniformBlockName);

		glGenBuffers(1, &shaderBinder->shaderObjectUniformBlockHandle);
		pUniformBlockInfo->uniformBlockHandle = shaderBinder->shaderObjectUniformBlockHandle;

		if (GL_INVALID_INDEX == pUniformBlockInfo->uniformBlockIndex)
		{
			return;
		}

		glUniformBlockBinding(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, setup::graphics::SHADER_OBJECT_UNIFORM_BLOCK_BINDING_POINT);
		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &pUniformBlockInfo->uniformBlockSize);

		if (pUniformBlockInfo->uniformBlockSize > 0)
		{
			pUniformBlockInfo->uniformBuffer = new uint8_t[pUniformBlockInfo->uniformBlockSize];
			memset(pUniformBlockInfo->uniformBuffer, 0, pUniformBlockInfo->uniformBlockSize);
		}

		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS, &numberOfUniformsInBlock);
		glGetActiveUniformBlockiv(shaderBinder->shaderProgramId, pUniformBlockInfo->uniformBlockIndex, GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES, uniformIndices);

		for (int32_t uniformMemberIt = 0; uniformMemberIt<numberOfUniformsInBlock; uniformMemberIt++)
		{
			if (uniformIndices[uniformMemberIt] >= 0)
			{
				uint32_t uniformIndex = (uint32_t)uniformIndices[uniformMemberIt];

				// get length of name of uniform variable
				glGetActiveUniform(shaderBinder->shaderProgramId, uniformIndex, maxUniformNameLength, &actualUniformNameLength, &uniformSize, &uniformType, uniformName);

				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_OFFSET, &uniformOffset);
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_SIZE, &uniformSize);
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_ARRAY_STRIDE, &arrayStride);
				glGetActiveUniformsiv(shaderBinder->shaderProgramId, 1, &uniformIndex, GL_UNIFORM_MATRIX_STRIDE, &matrixStride);


				char* pObjectBeginFound = strstr(uniformName, objectBeginStruct);

				if (nullptr != pObjectBeginFound)
				{
					char* fieldName = (pObjectBeginFound + sizeof(objectBeginStruct) - 1);

					if (uniformSize > 1)
						replaceChar(fieldName, '[', '\0');

					new(pUniformBlockInfo->fields.push()) UniformField(fieldName, uniformOffset, uniformSize, arrayStride, uniformType);
				}
				else
				{
					const char* pObjectEndFound = strstr(uniformName, objectEndStruct);
					if (nullptr != pObjectEndFound)
					{
						if (uniformStructureSize > uniformOffset)
							uniformStructureSize = uniformOffset;
					}
					else
					{
						break;
					}
				}
			}
		}
	}

	glBindBuffer(GL_UNIFORM_BUFFER, shaderBinder->shaderObjectUniformBlockHandle);
	glBufferData(GL_UNIFORM_BUFFER, pUniformBlockInfo->uniformBlockSize, nullptr, GL_STATIC_DRAW);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);

	//pUniformBlockInfo->uniformBlockSize = uniformStructureSize; wymazac to
	shaderBinder->shaderObjectSize      = uniformStructureSize;
}

bool ShaderLoader::createShaderBinder(const char* binderName, const Shader* vertexShader, const Shader* fragmentShader, const Shader* geometryShader, ShaderBinder* shaderBinder)
{
	if (SDL_ThreadID() != DisplayManager::getRenderingThreadId())
	{
		return scheduleCreateShaderBinder(binderName, vertexShader, fragmentShader, geometryShader, shaderBinder);
	}

	if (0 == shaderBinder->shaderProgramId)
	{
		shaderBinder->shaderProgramId = glCreateProgram();

		if (0 == shaderBinder->shaderProgramId)
		{
			LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Shader program could not be created");
			return false;
		}
	}

	if (nullptr != shaderBinder->vertexShader)
	{
		glDetachShader(shaderBinder->shaderProgramId, vertexShader->shaderProcId);
		shaderBinder->vertexShader = nullptr;
	}

	if (nullptr != shaderBinder->fragmentShader)
	{
		glDetachShader(shaderBinder->shaderProgramId, fragmentShader->shaderProcId);
		shaderBinder->fragmentShader = nullptr;
	}	

	if (nullptr != shaderBinder->geometryShader)
	{
		glDetachShader(shaderBinder->shaderProgramId, geometryShader->shaderProcId);
		shaderBinder->geometryShader = nullptr;
	}

	if (nullptr != vertexShader)
	{
		if (vertexShader->shaderType == ShaderType::VERTEX_SHADER)
		{
			shaderBinder->vertexShader = (ja::Shader*)vertexShader;
			glAttachShader(shaderBinder->shaderProgramId, vertexShader->shaderProcId);
		}
	}
	
	if (nullptr != fragmentShader)
	{
		if (fragmentShader->shaderType == ShaderType::FRAGMENT_SHADER)
		{
			shaderBinder->fragmentShader = (ja::Shader*)fragmentShader;
			glAttachShader(shaderBinder->shaderProgramId, fragmentShader->shaderProcId);
		}
	}
	
	if (nullptr != geometryShader)
	{
		if (geometryShader->shaderType == ShaderType::GEOMETRY_SHADER)
		{
			shaderBinder->geometryShader = (ja::Shader*)geometryShader;
			glAttachShader(shaderBinder->shaderProgramId, geometryShader->shaderProcId);
		}
	}
	
	glLinkProgram(shaderBinder->shaderProgramId);

	int32_t linkStatus;
	int32_t length;

	if (DisplayManager::getOpenglVersion() >= 2.0)
	{
		glGetProgramiv(shaderBinder->shaderProgramId, GL_LINK_STATUS, &linkStatus);
		if (linkStatus == GL_FALSE)
		{
			glGetProgramInfoLog(shaderBinder->shaderProgramId, 1024, &length, singleton->debugInfo);
			LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Shader binder %s could not be linked: %s", binderName, singleton->debugInfo);
			return false;
		}
	}
	else
	{
		glGetObjectParameterivARB(shaderBinder->shaderProgramId, GL_OBJECT_LINK_STATUS_ARB, &linkStatus);
		if (linkStatus == GL_FALSE)
		{
			glGetInfoLogARB(shaderBinder->shaderProgramId, 1024, &length, singleton->debugInfo);
			LogManager::getSingleton()->addLogMessage(LogType::ERROR_LOG, "Shader binder %s could not be linked: %s", binderName, singleton->debugInfo);
			return false;
		}
	}
	
	shaderBinder->mvpUniformLocation         = glGetUniformLocation(shaderBinder->shaderProgramId, "mvpMatrix");
 	shaderBinder->mvpMatricesUniformLocation = glGetUniformLocation(shaderBinder->shaderProgramId, "mvpMatrices");
	shaderBinder->rotMatricesUniformLocation = glGetUniformLocation(shaderBinder->shaderProgramId, "rotMatrices");
 	getMaterialUniformBlock(shaderBinder,     "Materials");
	getSettingUniformBlock(shaderBinder,      "Settings");
	getShaderObjectUniformBlock(shaderBinder, "Objects");

	LogManager* pLogManager = LogManager::getSingleton();

	const bool printLogsToCMD   = pLogManager->printLogsToCMD;
	pLogManager->printLogsToCMD = false;
	logActiveUniforms(shaderBinder);
	logActiveUniformBlocks(shaderBinder);
	logUniformBlockInfo(shaderBinder, &shaderBinder->materialUniformBlock);
	logUniformBlockInfo(shaderBinder, &shaderBinder->settingUniformBlock);
	logUniformBlockInfo(shaderBinder, &shaderBinder->shaderObjectUniformBlock);
	
	pLogManager->printLogsToCMD = printLogsToCMD;

	return true;
}

bool ShaderLoader::deleteShaderBinder(ShaderBinder* shaderBinder)
{
	if (SDL_ThreadID() != DisplayManager::getRenderingThreadId())
	{
		return scheduleDeleteShaderBinder(shaderBinder);
	}

	if (nullptr != shaderBinder->vertexShader)
	{
		glDetachShader(shaderBinder->shaderProgramId, shaderBinder->vertexShader->shaderProcId);
		shaderBinder->vertexShader = nullptr;
	}
		
	if (nullptr != shaderBinder->fragmentShader)
	{
		glDetachShader(shaderBinder->shaderProgramId, shaderBinder->fragmentShader->shaderProcId);
		shaderBinder->fragmentShader = nullptr;
	}

	if (nullptr != shaderBinder->geometryShader)
	{
		glDetachShader(shaderBinder->shaderProgramId, shaderBinder->geometryShader->shaderProcId);
		shaderBinder->geometryShader = nullptr;
	}

	glDeleteProgram(shaderBinder->shaderProgramId);
	glDeleteBuffers(1, &shaderBinder->materialUniformBlockHandle);
	glDeleteBuffers(1, &shaderBinder->settingUniformBlockHandle);
	glDeleteBuffers(1, &shaderBinder->shaderObjectUniformBlockHandle);
	
	return true;
}

bool ShaderLoader::scheduleLoadShader(const char* fileName, ShaderType shaderType , Shader* shader)
{
	SDL_mutexP(loadingMutex);
	scheduleShaderLoad.fileName         = fileName;
	scheduleShaderLoad.schedludedShader = shader;
	scheduleShaderLoad.shaderType       = shaderType;
	scheduleShaderLoad.scheduleOn       = true;
	SDL_CondWait(alreadyLoadedCondition,loadingMutex);
	if(scheduleShaderLoad.scheduleOn == true)
		SDL_CondWait(alreadyLoadedCondition,loadingMutex); // To be 100% sure that task is finalized
	SDL_mutexV(loadingMutex);

	return scheduleShaderLoad.result;
}

bool ShaderLoader::scheduleDeleteShader(Shader* shader)
{
	SDL_mutexP(loadingMutex);
	scheduleShaderDelete.schedludedShader = shader;
	scheduleShaderDelete.scheduleOn = true;
	SDL_CondWait(alreadyLoadedCondition, loadingMutex);
	if (scheduleShaderDelete.scheduleOn == true)
		SDL_CondWait(alreadyLoadedCondition, loadingMutex); // To be 100% sure that task is finalized
	SDL_mutexV(loadingMutex);

	return scheduleShaderDelete.result;
}

bool ShaderLoader::scheduleCreateShaderBinder(const char* binderName, const Shader* vertexShader, const Shader* fragmentShader, const Shader* geometryShader, ShaderBinder* shaderBinder)
{
	SDL_mutexP(loadingMutex);
	scheduleShaderBinderCreate.binderName     = binderName;
	scheduleShaderBinderCreate.vertexShader   = (Shader*)vertexShader;
	scheduleShaderBinderCreate.fragmentShader = (Shader*)fragmentShader;
	scheduleShaderBinderCreate.geometryShader = (Shader*)geometryShader;
	scheduleShaderBinderCreate.scheduleOn     = true;
	scheduleShaderBinderCreate.scheduledShaderBinder = shaderBinder;

	SDL_CondWait(alreadyLoadedCondition, loadingMutex);
	if (scheduleShaderBinderCreate.scheduleOn == true)
		SDL_CondWait(alreadyLoadedCondition, loadingMutex); // To be 100% sure that task is finalized
	SDL_mutexV(loadingMutex);

	return scheduleShaderBinderCreate.result;
}

bool ShaderLoader::scheduleDeleteShaderBinder(ShaderBinder* shaderBinder)
{
	SDL_mutexP(loadingMutex);
	scheduleShaderBinderDelete.scheduleOn = true;
	scheduleShaderBinderDelete.scheduledShaderBinder = shaderBinder;

	SDL_CondWait(alreadyLoadedCondition, loadingMutex);
	if (scheduleShaderBinderDelete.scheduleOn == true)
		SDL_CondWait(alreadyLoadedCondition, loadingMutex); // To be 100% sure that task is finalized
	SDL_mutexV(loadingMutex);

	return scheduleShaderBinderDelete.result;
}

/*void ShaderLoader::scheduleCreateFbo(const char* fboName, float widthPercent, float heightPercent, int32_t textureFlag, bool linear)
{
	SDL_mutexP(loadingMutex);
	scheduleFbo.name = fboName;
	scheduleFbo.bufferWidth = widthPercent;
	scheduleFbo.bufferHeight = heightPercent;
	scheduleFbo.textureFlag = textureFlag;
	scheduleFbo.linear = linear;
	scheduleFbo.scheduleOn = true;

	SDL_CondWait(alreadyLoadedCondition,loadingMutex);
	if(scheduleFbo.scheduleOn == true)
		SDL_CondWait(alreadyLoadedCondition,loadingMutex); // To be 100% sure that task is finalized
	SDL_mutexV(loadingMutex);

}*/

void ShaderLoader::queryScheduleLoading()
{
	PROFILER_FUN;

	SDL_mutexP(loadingMutex);

	if(scheduleShaderLoad.scheduleOn)
	{
		scheduleShaderLoad.result = loadShader(scheduleShaderLoad.fileName.c_str(), scheduleShaderLoad.shaderType, scheduleShaderLoad.schedludedShader);
		scheduleShaderLoad.scheduleOn = false;
	}

	/*if(scheduleFbo.scheduleOn)
	{
		createFbo(scheduleFbo.name , scheduleFbo.bufferWidth, scheduleFbo.bufferHeight, scheduleFbo.textureFlag, scheduleFbo.linear);
		scheduleFbo.scheduleOn = false;
	}*/

	if (scheduleShaderBinderCreate.scheduleOn)
	{
		scheduleShaderBinderCreate.result = createShaderBinder(scheduleShaderBinderCreate.binderName.c_str(), scheduleShaderBinderCreate.vertexShader, scheduleShaderBinderCreate.fragmentShader, scheduleShaderBinderCreate.geometryShader, scheduleShaderBinderCreate.scheduledShaderBinder);
		scheduleShaderBinderCreate.scheduleOn = false;
	}

	SDL_mutexV(loadingMutex);

	SDL_CondSignal(alreadyLoadedCondition);
}

}