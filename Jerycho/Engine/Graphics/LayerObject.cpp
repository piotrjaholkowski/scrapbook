#include "LayerObject.hpp"
#include "../Entity/LayerObjectGroup.hpp"

namespace ja
{

void LayerObject::updateComponentData()
{
	if (componentData != nullptr)
	{
		LayerObjectComponent* layerObjectComponent = (LayerObjectComponent*)(componentData);
		layerObjectComponent->updateLocationData();
	}
}

}