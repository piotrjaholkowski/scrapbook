#pragma once

//#include "LayerObject.hpp"
#include "../Math/VectorMath2d.hpp"

#include "Bone.hpp"
#include "SkeletonAnimation.hpp"

#include "../Manager/DisplayManager.hpp"

#include "../Allocators/CacheLineAllocator.hpp"
#include "../Allocators/StackAllocator.hpp"
#include "../Utility/NameTag.hpp"

#include <stdint.h>

namespace ja
{
	class BoneElement
	{
	public:
		//Morph or not
	};

	class SkeletonDef
	{
	public:
		jaVector2 position;
		jaMatrix2 rotMat;
	};

	typedef NameTag<setup::TAG_NAME_LENGTH> SkeletonName;

	

	class Skeleton
	{
		uint16_t skeletonId;

		Skeleton* next;
		Skeleton* prev;

		Bone*            bones; 
		TransformedBone* transformedBones;
		BoneName*        boneNames;
		uint16_t         bonesNum;

		SkeletalAnimations* skeletalAnimations;

		jaVector2 position;

		CacheLineAllocator* boneAllocator;

		SkeletonName skeletonName;
		

		Skeleton(CacheLineAllocator* boneAllocator);
		~Skeleton();

	public:

		void calculateTransformedBoneStates();

		inline SkeletalAnimations* getSkeletalAnimations()
		{
			return this->skeletalAnimations;
		}

		SkeletonAnimation* createAnimation(const char* animationName);
		void setSkeletonPose(uint16_t animationNameHashCode, float normalizedTime);
		void setSkeletonPose(const char* animationName, float normalizedTime);

		inline int16_t getBoneId(uint16_t boneNameHashCode) const
		{
			for (uint16_t i = 0; i < this->bonesNum; ++i)
			{
				if (this->boneNames[i].getHash() == boneNameHashCode)
				{
					return i;
				}
			}

			return -1;
		}

		inline uint16_t getBoneId(const char* boneName) const
		{
			BoneName boneNameTag;

			boneNameTag.setName(boneName);
			return getBoneId(boneNameTag.getHash());
		}

		inline uint16_t getBoneId(Bone* bone) const
		{
			return ( ((uintptr_t)bone - (uintptr_t)this->bones) /sizeof(Bone));
		}

		void setName(const char* skeletonName)
		{
			this->skeletonName.setName(skeletonName);
		}

		const char* getName()
		{
			return this->skeletonName.getName();
		}

		void getSelectedBones(const gil::AABB2d& selection, StackAllocator<uint16_t>& selectedBoneIds);

		uint16_t getBonesNum() const
		{
			return this->bonesNum;
		}

		Bone* getBones() const
		{
			return this->bones;
		}

		const TransformedBone* const getTransformedBones() const
		{
			return this->transformedBones;
		}

		BoneName* getBoneNames() const
		{
			return this->boneNames;
		}

		void setBonePosition(int16_t boneId, const jaVector2& boneTransformedPosition);
		void setBoneWordRotation(int16_t boneId, float angleRadians);
		void addBone(int16_t parentBoneId, const jaVector2& boneTransformedPosition);

		uint16_t getId()
		{
			return skeletonId;
		}

		void setPosition(const jaVector2& position)
		{
			this->position = position;
		}
	
		jaVector2 getPosition()
		{
			return this->position;
		}

		void setAngleDeg(jaFloat angleDeg)
		{
			this->bones[0].transformation.rotMat.setDegrees(angleDeg);
		}

		void setSkeletonDef(SkeletonDef& skeletonDef)
		{
			this->position = skeletonDef.position;
		}

		void draw();

		friend class AnimationManager;
	};

	//class AnimationPlayer
	//{
		//Animation* animation;
		//Skeleton* skeleton;

		//AbsoluteSkeleton absoluteSkeleton;

		//float elapsedTime;
	//};

	//class AnimationIKPlayer
	//{
		//AbsoluteSkeleton absoluteSkeleton;
	//};

	//class AnimationBlender
	//{
	//	AnimationPlayer* animation0;
	//	AnimationPlayer* animation1;
	//	AnimationPlayer* animation2;
	//};

	//make machine state

	//class AnimationState
	//{
	//	char animationName[20];
	//  later do that in script
	//};

	//class Skeleton
	//{
	//
	//};

	

	
}