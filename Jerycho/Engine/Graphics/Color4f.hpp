#pragma once

#include "../Math/Math.hpp"

namespace ja
{
	class Color4f
	{
	public:
		jaFloat r;
		jaFloat g;
		jaFloat b;
		jaFloat a;

		inline Color4f() : r(1.0f), g(1.0f), b(1.0f), a(1.0f)
		{

		}

		inline Color4f(jaFloat r, jaFloat g, jaFloat b, jaFloat a) : r(r), g(g), b(b), a(a)
		{

		}

		inline void set(const jaFloat r, const jaFloat g, const jaFloat b, const jaFloat a)
		{
			this->r = r;
			this->g = g;
			this->b = b;
			this->a = a;
		}
	};
}