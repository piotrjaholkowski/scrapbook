#include "Light.hpp"

#include "../Manager/LayerObjectManager.hpp"
#include "LayerObjectDef.hpp"

namespace ja
{
	Light::Light(gil::ObjectHandle2d* handle, uint8_t* layerObjectFlag, CacheLineAllocator* pDrawElementsAllocator) : LayerObject((uint8_t)LayerObjectType::LIGHT, handle, layerObjectFlag), drawElements(pDrawElementsAllocator)
	{

	}

	void Light::setLayerObjectDef(const LayerObjectDef& layerObjectDef, bool updateScene)
	{
		gil::ObjectHandle2d* handle = getHandle();
		handle->transform = layerObjectDef.transform;

		this->drawElements.copy(*layerObjectDef.drawElements);

		this->layerId = layerObjectDef.layerId;
		this->scale = layerObjectDef.scale;

		this->depth = layerObjectDef.depth;

		gil::Circle2d* circle = (gil::Circle2d*)handle->getShape();

		jaFloat maxScale = std::max(abs(layerObjectDef.scale.x), abs(layerObjectDef.scale.y));
		circle->radius = maxScale * layerObjectDef.drawElements->getBoundingVolumeRadius();

		if (updateScene)
			updateTransform();
	}

	void Light::fillLayerObjectDef(LayerObjectDef& layerObjectDef)
	{
		layerObjectDef.layerObjectType = (uint8_t)LayerObjectType::LIGHT;
		layerObjectDef.transform = getHandle()->getTransform();
		layerObjectDef.drawElements->copy(this->drawElements);

		layerObjectDef.layerId     = this->layerId;
		layerObjectDef.scale       = this->scale;


		layerObjectDef.depth = this->depth;
	}

	void Light::drawBounds()
	{
		const gil::ObjectHandle2d* handle = getHandle();
		gil::Debug::drawObjectHandle2d(handle, handle->transform);
	}

	void Light::drawAABB()
	{
		gil::Debug::drawAABB2d(getHandle()->volume);
	}

	bool Light::isIntersecting(const gil::AABB2d& aabb)
	{
		jaVector2 aabbCenter = aabb.getCenter();

		float width, height;
		aabb.getSize(width, height);

		gil::Box2d boxShape(width, height);

		const gil::ObjectHandle2d* handle = getHandle();
		jaVector2 positionDiff = aabbCenter - handle->transform.position;
		jaMatrix2 invRot = jaVectormath2::inverse(handle->transform.rotationMatrix);
		jaVector2 aabbLocalPosition = invRot * positionDiff;

		jaTransform2 aabbTransform(aabbLocalPosition, invRot);

		return drawElements.isIntersecting(aabbTransform, boxShape, this->scale);
	}

	void Light::draw()
	{
		*this->layerObjectFlag |= (uint8_t)LayerObjectFlag::RENDERED_OBJECT;
		//set shader properties to normal

		glPushMatrix();

		jaMatrix44 modelMatrix;

		const gil::ObjectHandle2d* handle = getHandle();
		jaVectormath2::setTranslateRotScaleMatrix44(handle->transform.position, handle->transform.rotationMatrix, scale, modelMatrix);
		glMultMatrixf(modelMatrix.element);

		this->drawElements.bindVertices();
		this->drawElements.drawFaces();

		glPopMatrix();
	}

	uint32_t Light::getSizeOf()
	{
		return sizeof(Polygon);
	}

	void Light::freeAllocatedSpace(LayerObjectManager* layerObjectManager)
	{
		drawElements.freeAllocatedSpace();
	}

	LayerObject* Light::createLight(CacheLineAllocator* layerObjectAllocator, const LayerObjectDef& layerObjectDef, LayerObjectManager* layerObjectManager)
	{
		jaFloat maxScale = std::max(abs(layerObjectDef.scale.x), abs(layerObjectDef.scale.y));

		gil::Scene2d* scene2d = layerObjectManager->getScene();

		gil::Circle2d        circleShape(maxScale * layerObjectDef.drawElements->getBoundingVolumeRadius());
		gil::ObjectHandle2d* handle = scene2d->createDynamicObject(&circleShape, layerObjectDef.transform);

		return nullptr;
	}

}