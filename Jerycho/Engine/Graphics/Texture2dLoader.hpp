#pragma once

#include "../jaSetup.hpp"
#include "../Allocators/LinearAllocator.hpp"
#include "../Utility/String.hpp"
#include "Texture2d.hpp"

#ifdef JA_SDL2_VERSION
#include <SDL2/SDL_mutex.h>
#else
#include <SDL/SDL_mutex.h>
#endif

#include <stdint.h>

namespace ja
{

	class ScheduleTexture2dLoad
	{
	public:
		ja::string  fileName;
		Texture2d*  schedludedTexture;
		bool        scheduleOn;
		bool        result;
	};

	class ScheduleTexture2dDelete
	{
	public:
		Texture2d*  schedludedTexture;
		bool        scheduleOn;
		bool        result;
	};

	class Texture2dLoader
	{
	private:
		static Texture2dLoader* singleton;
		Texture2dLoader();

		static ScheduleTexture2dLoad   scheduleTexture2dLoad;
		static ScheduleTexture2dDelete scheduleTexture2dDelete;
		static SDL_mutex*              loadingMutex;
		static SDL_cond*               alreadyLoadedCondition;


	public:
		static void init();
		static Texture2dLoader* getSingleton();

	
		static bool loadTexture(const char* fileName, Texture2d* texture);
		static bool deleteTexture(Texture2d* texture);

		static bool scheduleLoadTexture2d(const char* fileName, Texture2d* texture);
		static bool scheduleDeleteTexture2d(Texture2d* texture);

		static void queryScheduleLoading(); // called by main thread

		~Texture2dLoader();
		static void cleanUp();
	};

}