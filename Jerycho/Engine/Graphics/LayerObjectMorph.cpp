#include "LayerObjectMorph.hpp"

namespace ja
{

HashMap<MorphHashTagName> LayerObjectMorph::hashTagDictionary;

LayerObjectMorph::LayerObjectMorph(const char* morphName) : activeFeatures(0)
{	
	this->hash = createHashTag(morphName);
}

LayerObjectMorph::LayerObjectMorph(uint32_t hash) : activeFeatures(0)
{
	this->hash = hash;
}

const char* LayerObjectMorph::getName(uint32_t hash)
{
	uint32_t                     hashNum  = hashTagDictionary.getSize();
	HashEntry<MorphHashTagName>* hashTags = hashTagDictionary.getFirst();

	for (uint32_t i = 0; i < hashNum; ++i)
	{
		if (hash == hashTags[i].hash)
		{
			return hashTags[i].data->getName();
		}
	}

	return nullptr;
}

uint32_t LayerObjectMorph::createHashTag(const char* name)
{
	MorphHashTagName hashTag;
	hashTag.setName(name);

	const uint32_t               hashNum  = hashTagDictionary.getSize();
	HashEntry<MorphHashTagName>* hashTags = hashTagDictionary.getFirst();

	for (uint32_t i = 0; i < hashNum; ++i)
	{
		if (hashTags[i].data->getHash() == hashTag.getHash())
		{
			return hashTag.getHash();
		}
	}

	hashTagDictionary.add(hashTag.getHash(), new MorphHashTagName(hashTag));

	return hashTag.getHash();
}

uint32_t LayerObjectMorph::rename(uint32_t oldHash, const char* newName)
{
	return createHashTag(newName);
}

const char* LayerObjectMorph::getName() const
{
	const char* name = LayerObjectMorph::getName(this->hash);
	
	if (nullptr == name)
		return "";
	
	return name;
}

void LayerObjectMorph::setName(const char* name)
{
	this->hash = LayerObjectMorph::rename(this->hash, name);
}

void LayerObjectMorph::clearDictionary()
{
	uint32_t                     hashNum  = hashTagDictionary.getSize();
	HashEntry<MorphHashTagName>* hashTags = hashTagDictionary.getFirst();

	for (uint32_t i = 0; i < hashNum; ++i)
	{
		delete hashTags[i].data;
	}

	hashTagDictionary.clear();
}

MorphElements::MorphElements(CacheLineAllocator* pCacheLineAllocator) : pAllocator(pCacheLineAllocator), morphEntries(nullptr), morphsNum(0)
{

}

void MorphElements::freeAllocatedSpace()
{
	if (nullptr != this->morphEntries)
	{
		for (uint16_t i = 0; i < this->morphsNum; ++i)
		{
			morphEntries[i].layerObjectMorph->freeAllocatedSpace();
			pAllocator->free(morphEntries[i].layerObjectMorph, morphEntries[i].layerObjectMorph->getSizeOf());
		}

		pAllocator->free(this->morphEntries, this->morphsNum * sizeof(MorphEntry));
		this->morphEntries = nullptr;
		this->morphsNum    = 0;
	}
}


void MorphElements::copy(const MorphElements& morphElementsSource)
{
	this->freeAllocatedSpace();

	this->morphsNum         = morphElementsSource.getMorphsNum();
	const int32_t arraySize = morphsNum * sizeof(MorphEntry);

	if (0 != morphsNum)
		this->morphEntries = (MorphEntry*)pAllocator->allocate(arraySize);

	MorphEntry* morphCopy = morphElementsSource.getMorphEntries();

	for (uint16_t i = 0; i < morphsNum; ++i)
	{
		this->morphEntries[i].layerObjectMorph = (LayerObjectMorph*)morphCopy[i].layerObjectMorph->copy(this->pAllocator);
		this->morphEntries[i].hash             = this->morphEntries[i].layerObjectMorph->getHash();
	}
}

void MorphElements::addMorph(LayerObjectMorph* layerObjectMorph)
{
	const uint32_t newSize = sizeof(MorphEntry) * (morphsNum + 1);
	const uint32_t oldSize = sizeof(MorphEntry) * morphsNum;

	MorphEntry* newEntries = (MorphEntry*)pAllocator->allocate(newSize);

	memcpy(newEntries, this->morphEntries, oldSize);

	if (oldSize > 0)
		pAllocator->free(this->morphEntries, oldSize);

	newEntries[this->morphsNum].hash             = layerObjectMorph->getHash();
	newEntries[this->morphsNum].layerObjectMorph = layerObjectMorph;

	this->morphsNum    = this->morphsNum + 1;
	this->morphEntries = newEntries;
}

bool MorphElements::removeMorph(LayerObjectMorph* layerObjectMorph)
{
	
	for (uint32_t i = 0; i < this->morphsNum; ++i)
	{
		if (this->morphEntries[i].layerObjectMorph == layerObjectMorph)
		{
			const uint32_t toCopySize = (this->morphsNum - (i+1)) * sizeof(MorphEntry);

			morphEntries[i].layerObjectMorph->freeAllocatedSpace();
			pAllocator->free(morphEntries[i].layerObjectMorph, morphEntries[i].layerObjectMorph->getSizeOf());

			memcpy(&morphEntries[i], &morphEntries[i + 1], toCopySize);

			--this->morphsNum;

			return true;
		}
	}

	return false;
}

void MorphElements::clear()
{
	freeAllocatedSpace();
}

}