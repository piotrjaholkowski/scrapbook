#pragma once

#include <stdint.h>

#include "../Math/VectorMath2d.hpp"
#include "../Utility/NameTag.hpp"


namespace ja
{
	typedef NameTag<setup::graphics::BONE_NAME_LENGTH> BoneName;

	class BoneState
	{
	public:
		jaFloat   length;
		jaMatrix2 rotMat;
	};

	class Bone
	{
	public:
		int16_t   parentId;
		uint16_t  childNum;

		BoneState transformation;

		Bone() : childNum(0)
		{

		}
	};

	class TransformedBone
	{
	public:
		jaVector2 position;  //position of bone end
		jaMatrix2 rotMat; //rotation transformation of bone end
	};
}