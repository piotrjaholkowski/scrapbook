#pragma once

#include "DrawElements.hpp"
#include "LayerObject.hpp"

#include "../Gilgamesh/ObjectHandle2d.hpp"
#include "../Math/VectorMath2d.hpp"

enum class LightType
{
	POINT_LIGHT       = 0,
	DIRECTIONAL_LIGHT = 1 
};

namespace ja
{
	class LayerObjectDef;
	class LayerObjectManager;

	class Light : public LayerObject
	{
	private:
		uint8_t   lightType;
		bool      shadowProjector;
		jaVector2 scale;

	public:
		DrawElements  drawElements;

		Light(gil::ObjectHandle2d* handle, uint8_t* polygonFlag, CacheLineAllocator* pCacheLineAllocator);

		void setLayerObjectDef(const LayerObjectDef& layerObjectDef, bool updateScene = true);
		void fillLayerObjectDef(LayerObjectDef& layerObjectDef);

		bool isShadowProjector() const
		{
			return shadowProjector;
		}

		inline uint8_t getLightType()
		{
			return lightType;
		}

		inline jaVector2 getPosition()
		{
			return getHandle()->transform.position;
		}

		inline jaMatrix2 getRotationMatrix()
		{
			return getHandle()->transform.rotationMatrix;
		}

		inline jaFloat getAngleDeg()
		{
			return getHandle()->transform.rotationMatrix.getDegrees();
		}

		inline jaVector2 getScale()
		{
			return this->scale;
		}

		inline void setPosition(const jaVector2& position)
		{
			getHandle()->transform.position = position;
			updateTransform();
		}

		inline void setAngleDeg(const jaFloat angleDeg)
		{
			getHandle()->transform.rotationMatrix.setDegrees(angleDeg);
			updateTransform();
		}

		inline void setScale(const jaVector2& scale)
		{
			this->scale = scale;

			gil::Circle2d* circle = (gil::Circle2d*)getHandle()->getShape();
	
			jaFloat maxScale = std::max(abs(scale.x), abs(scale.y));
			circle->radius = maxScale * drawElements.getBoundingVolumeRadius();

			updateTransform();
		}

		bool isIntersecting(const gil::AABB2d& aabb);

		void draw();
		void drawBounds();
		void drawAABB();

		static  LayerObject* createLight(CacheLineAllocator* layerObjectAllocator, const LayerObjectDef& layerObjectDef, LayerObjectManager* layerObjectManager);
		virtual uint32_t     getSizeOf() = 0;

	private:
		virtual void freeAllocatedSpace(LayerObjectManager* layerObjectManager);

		friend class LayerObjectManager;
	};

	class PointLight : public Light
	{
	private:
		jaFloat circleRadius;

	public:
	};

	class DirectionalLight : public Light
	{
	private:

	public:
	};

}