#pragma once

#include "../Manager/DisplayManager.hpp"

namespace ja
{

	class Fbo
	{
	private:
		GLuint fboHandle;

	public:
	
		Fbo();
		void bind();
		void unbind();

		inline uint32_t getFboHandle()
		{
			return fboHandle;
		}

		~Fbo();
	};

}