#pragma once


#include "Color4f.hpp"
#include "DrawElements.hpp"
#include "LayerObject.hpp"
#include "LayerObjectMorph.hpp"
#include "ShaderObjectData.hpp"

#include "../Gilgamesh/ObjectHandle2d.hpp"
#include "../Math/VectorMath2d.hpp"

namespace ja
{

class LayerObjectDef;
class LayerObjectManager;
class Material;
class RenderBuffer;

class PolygonMorphElements : public MorphElements
{
public:
	PolygonMorphElements(CacheLineAllocator* pCacheLineAllocator);

	LayerObjectMorph* createMorph(const char* morphName);
	LayerObjectMorph* createMorph(uint32_t hash);
};

enum class PolygonMorphFeature
{
	TRANSLATE = 1,
	ROTATE    = 2,
	SCALE     = 3,
	COLOR     = 4,
	VERTICES  = 5
};

class PolygonMorph : public LayerObjectMorph
{	
public:
	jaTransform2 transform;
	jaVector2    scale;

	DrawElements drawElements;

	PolygonMorph(const char* morphName, CacheLineAllocator* pDrawElementsAllocator);
	PolygonMorph(uint32_t hash, CacheLineAllocator* pDrawElementsAllocator);

	void  setMorph(LayerObject* layerObject);
	void* copy(CacheLineAllocator* pAllocator);
	
	uint8_t     getFeaturesNum() const;
	const char* getFeatureName(uint8_t featureId) const;

	void     freeAllocatedSpace();
	uint32_t getSizeOf() const;
};



class Polygon : public LayerObject
{
private:
	jaVector2 scale;

public:
	DrawElements         drawElements;
	ShaderObjectData     shaderObjectData; 
	PolygonMorphElements morphElements;
private:
	Material*         material;
	

public:
	Polygon(gil::ObjectHandle2d* handle, uint8_t* layerObjectFlag, CacheLineAllocator* pCacheLineAllocator);

	void morph(LayerObjectMorph* morphStart, LayerObjectMorph* morphEnd, float morph);

	inline MorphElements* getMorphElements() 
	{
		return &morphElements;
	}

	void setLayerObjectDef(const LayerObjectDef& layerObjectDef, bool updateScene = true);
	void fillLayerObjectDef(LayerObjectDef& layerObjectDef);

	void setText(FontPolygon* font, const char* text, const Color4f& color);

	inline jaVector2 getScale() const
	{
		return this->scale;
	}

	inline void setScale(const jaVector2& scale)
	{
		this->scale = scale;

		updateBoundingVolume();
		updateTransform();
	}

	inline void updateBoundingVolume()
	{
		gil::Circle2d* circle = (gil::Circle2d*)getHandle()->getShape();
		jaFloat maxScale      = std::max(abs(scale.x), abs(scale.y));
		circle->radius        = maxScale * drawElements.getBoundingVolumeRadius();
	}

	inline ShaderObjectData* getShaderObjectData() 
	{
		return &this->shaderObjectData;
	}

	bool isIntersecting(const gil::AABB2d& aabb);

	void draw();
	void draw(RenderBuffer& renderBuffer);
	void drawBounds();
	void drawAABB();

	inline ShaderBinder* getShaderBinder() const
	{
		return this->material->getShaderBinder();
	}

	inline Material* getMaterial()  const
	{
		return this->material;
	}

	inline uint32_t getSizeOf() const
	{
		return sizeof(Polygon);
	}

	static LayerObject* createPolygon(CacheLineAllocator* layerObjectAllocator, const LayerObjectDef& layerObjectDef, LayerObjectManager* layerObjectManager);

	inline const char* getName() const
	{
		return "Polygon";
	}

private:
	virtual void freeAllocatedSpace(LayerObjectManager* layerObjectManager);

	friend class LayerObjectManager;
	friend class PolygonMorph;
};


}