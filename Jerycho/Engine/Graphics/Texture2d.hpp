#pragma once

#include "../jaSetup.hpp"

#ifdef JA_SDL_OPENGL
	#ifdef JA_SDL2_VERSION
		#include<SDL2/SDL_opengl.h>
	#else
		#include<SDL/SDL_opengl.h>
	#endif
#endif

namespace ja
{

	class Texture2d
	{
	public:
		uint32_t resourceId;
		uint32_t width, height;
		GLuint   texture;
		
		Texture2d(uint32_t resourceId) : resourceId(resourceId), texture(0)
		{
		
		}

		~Texture2d()
		{
			clear();
		}

		void clear();

		inline GLuint getTexture()
		{
			return texture;
		}

		inline uint32_t getWidth()
		{
			return width;
		}

		inline uint32_t getHeight()
		{
			return height;
		}

	};

}
