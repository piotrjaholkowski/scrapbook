#include "SkeletonAnimation.hpp"


#include "../Manager/AnimationManager.hpp"

namespace ja
{
	SkeletalAnimations::SkeletalAnimations(uint16_t skeletonOwnerId, CacheLineAllocator* animationAllocator) : skeletonOwnerId(skeletonOwnerId), animationReferencesNum(1), animationAllocator(animationAllocator),
		skeletonAnimations(nullptr), animationsNum(0)
	{

	}

	SkeletalAnimations::~SkeletalAnimations()
	{
		if (nullptr != this->skeletonAnimations)
		{
			for (uint16_t i = 0; i < animationsNum; ++i)
			{
				delete skeletonAnimations[i];
				skeletonAnimations[i] = nullptr;
			}

			animationAllocator->free(this->skeletonAnimations, this->animationsNum * sizeof(SkeletonAnimation));
			this->skeletonAnimations = nullptr;
		}
	}

	void SkeletalAnimations::rebindOwner()
	{
		this->skeletonOwnerId = AnimationManager::getSingleton()->getNextOwnerId(this);
	}
}