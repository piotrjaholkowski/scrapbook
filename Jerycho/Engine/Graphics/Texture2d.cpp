#include "Texture2d.hpp"
#include "../Graphics/Texture2dLoader.hpp"

namespace ja
{


void Texture2d::clear()
{
	Texture2dLoader::deleteTexture(this);
}

}
