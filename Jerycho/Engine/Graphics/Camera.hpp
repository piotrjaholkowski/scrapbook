#pragma once

#include "../jaSetup.hpp"
#include "LayerObject.hpp"
#include "LayerObjectMorph.hpp"

#include "../Utility/NameTag.hpp"

#include <stdint.h>

namespace ja
{

class LayerObjectDef;
class LayerObjectManager;
class RenderBuffer;

class CameraMorphElements : public MorphElements
{
public:
	CameraMorphElements(CacheLineAllocator* pCacheLineAllocator);

	LayerObjectMorph* createMorph(const char* morphName);
	LayerObjectMorph* createMorph(uint32_t hash);
};

enum class CameraMorphFeature
{
	TRANSLATE = 1,
	ROTATE    = 2,
	SCALE     = 3
};

class CameraMorph : public LayerObjectMorph
{
public:
	jaTransform2 transform;
	jaVector2    scale;

	CameraMorph(const char* morphName, CacheLineAllocator* pDrawElementsAllocator);
	CameraMorph(uint32_t hash, CacheLineAllocator* pDrawElementsAllocator);

	void  setMorph(LayerObject* layerObject);
	void* copy(CacheLineAllocator* pAllocator);

	uint8_t     getFeaturesNum() const;
	const char* getFeatureName(uint8_t featureId) const;

	void     freeAllocatedSpace();
	uint32_t getSizeOf() const;
};

class Camera : public LayerObject
{
private:
	jaVector2 scale;
	jaVector2 cameraScale;

	TagName cameraName;
public:
	CameraMorphElements morphElements;

	Camera(gil::ObjectHandle2d* handle, uint8_t* layerObjectFlag, CacheLineAllocator* pAllocator);

	void morph(LayerObjectMorph* morphStart, LayerObjectMorph* morphEnd, float morph);

	inline MorphElements* getMorphElements() 
	{
		return &morphElements;
	}

	void setLayerObjectDef(const LayerObjectDef& layerObjectDef, bool updateScene = true);
	void fillLayerObjectDef(LayerObjectDef& layerObjectDef);

	inline void setCameraName(const char* name)
	{
		this->cameraName.setName(name);
	}

	inline const char* getCameraName() const
	{
		return this->cameraName.getName();
	}

	inline jaVector2 getScale() const
	{
		return this->scale;
	}

	inline jaVector2 getCameraScale() const
	{
		return this->cameraScale;
	}

	inline void setScale(const jaVector2& scale)
	{
		this->scale       = scale;
		this->cameraScale.x = 1.0f / scale.x;
		this->cameraScale.y = 1.0f / scale.y;

		int32_t widthRes, heightRes;
		jaFloat camWidthRes, camHeightRes;
		DisplayManager::getResolution(&widthRes, &heightRes);
		const jaFloat pixelsPerUnit = (jaFloat)setup::graphics::PIXELS_PER_UNIT;
		camWidthRes  = ((jaFloat)widthRes / pixelsPerUnit);
		camHeightRes = ((jaFloat)heightRes / pixelsPerUnit);

		camWidthRes  *= this->scale.x;
		camHeightRes *= this->scale.y;

		gil::Box2d* box = (gil::Box2d*)getHandle()->getShape();
		box->setWidth(camWidthRes);
		box->setHeight(camHeightRes);

		updateTransform();
	}

	inline void updateBoundingVolume()
	{
		//gil::Box2d* circle = (gil::Circle2d*)handle->getShape();
		//jaFloat maxScale = std::max(abs(scale.x), abs(scale.y));
		
	}

	bool isIntersecting(const gil::AABB2d& aabb);

	void draw();
	void draw(RenderBuffer& renderBuffer);
	void drawBounds();
	void drawAABB();

	Material* getMaterial() const;

	void getBoundingBox(gil::AABB2d* boundingBox);

	static  LayerObject* createCamera(CacheLineAllocator* layerObjectAllocator, const LayerObjectDef& layerObjectDef, LayerObjectManager* layerObjectManager);
	virtual uint32_t     getSizeOf() const;

	const char* getName() const
	{
		return "Camera";
	}

private:
	virtual void freeAllocatedSpace(LayerObjectManager* layerObjectManager);

	friend class CameraMorph;
	friend class LayerObjectManager;
};


}