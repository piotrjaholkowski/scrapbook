#pragma once

#include "../jaSetup.hpp"
#include "../Utility/NameTag.hpp"

#include <stdint.h>

namespace jas
{
	class ResourceManager;
}

namespace ja
{
	class ShaderBinder;
	class ShaderStructureDef; 
	class DataFieldInfo;

	typedef NameTag<setup::TAG_NAME_LENGTH> MaterialNameTag;

	class Material
	{
	private:
		ShaderBinder* shaderBinder;
		uint8_t       materialId;
	public:
		uint32_t      blendEquation;
		uint32_t      sourceFactor;
		uint32_t      destinationFactor;
	private:
		void*         materialData;
		uint32_t      materialStructureSize;
		
		ShaderStructureDef* shaderStructureDef;
	
		uint32_t resourceId;

	public:
		MaterialNameTag materialName;

	private:
		static uint32_t blendEquations[5];
		static uint32_t blendFactors[19];

	private:

		Material(const char* materialName, ShaderStructureDef* shaderStructureDef);
	public:

		~Material();

		inline uint8_t getMaterialId() const
		{
			return materialId;
		}

		inline ShaderBinder* getShaderBinder() const
		{
			return this->shaderBinder;
		}

		inline const ShaderStructureDef* getMaterialShaderDef()
		{
			return this->shaderStructureDef;
		}

		inline void* getMaterialData()
		{
			return this->materialData;
		}

		inline uint32_t getMaterialStructureSize()
		{
			return this->materialStructureSize;
		}

		const char* getMaterialName()
		{
			return this->materialName.getName();
		}

		const char* getSourceFactorName()
		{
			return getBlendingFactorName(this->sourceFactor);
		}

		const char* getDestinationFactorName()
		{
			return getBlendingFactorName(this->destinationFactor);
		}

		const char* getBlendEquationName();
		
		static uint32_t getBlendEquationId(uint32_t blendEquation);
		static uint32_t getBlendFactorId(uint32_t blendFactor);

		static uint32_t* getBlendEquations();
		static uint32_t* getBlendFactors();
		static uint32_t  getBlendEquationsNum();
		static uint32_t  getBlendFactorsNum();

		void setFloat(const DataFieldInfo* dataFieldInfo, const int32_t elementId, const float dataValue);
		void setBool(const DataFieldInfo* dataFieldInfo, const int32_t elementId, const int32_t dataValue);

	private:
		
		static char* getBlendingFactorName(uint32_t blendingFactor);
		void setMaterialName(const char* materialName);;
		void allocateShaderStructureData(const ShaderStructureDef* shaderStructureDef);
		
		friend class ResourceManager;
		friend class ShaderBinder;
	};
}