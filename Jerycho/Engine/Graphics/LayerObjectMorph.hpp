#pragma once

#include "../jaSetup.hpp"
#include "../Allocators/CacheLineAllocator.hpp"
#include "../Allocators/StackAllocator.hpp"
#include "../Allocators/HashMap.hpp"
#include "../Utility/NameTag.hpp"
//#include "../Utility/String.hpp"
#include "../Math/Math.hpp"


#include <stdint.h>

namespace ja
{

class   LayerObject;
typedef NameTag<setup::TAG_NAME_LENGTH> MorphHashTagName;

class LayerObjectMorph
{
private:
	static   HashMap<MorphHashTagName> hashTagDictionary;
	uint32_t hash;
public:
	uint32_t activeFeatures;
private:

	static void        clearDictionary();
	static const char* getName(uint32_t hash);
	static uint32_t    createHashTag(const char* name);
	static uint32_t    rename(uint32_t oldHash, const char* newName);
public:
	LayerObjectMorph(const char* morphName);
	LayerObjectMorph(uint32_t hash);

	inline uint32_t getHash()
	{
		return hash;
	}

	inline uint32_t getActiveFeatures() const
	{
		return activeFeatures;
	}

	inline void setActiveFeatures(uint32_t activeFeatures)
	{
		this->activeFeatures = activeFeatures;
	}

	inline bool isFeatureActive(uint8_t featureId) const
	{
		uint32_t featureFlag = 1 << featureId;

		if (featureFlag & this->activeFeatures)
			return true;

		return false;
	}

	inline void setFeature(uint8_t featureId, bool state)
	{
		uint32_t featureFlag = 1 << featureId;

		this->activeFeatures |= featureFlag;

		if (!state)
		{
			this->activeFeatures ^= featureFlag;
		}
	}

	virtual void        setMorph(LayerObject* layerObject) = 0;
	virtual const char* getFeatureName(uint8_t featureId) const = 0;
	virtual void*       copy(CacheLineAllocator* pAllocator) = 0;

	virtual void     freeAllocatedSpace() = 0;
	virtual uint32_t getSizeOf() const = 0;

	const char*     getName() const;
	void            setName(const char* name);
	virtual uint8_t getFeaturesNum() const = 0;

	friend class LayerObjectManager;
};

class MorphEntry
{
public:
	uint32_t          hash;
	LayerObjectMorph* layerObjectMorph;

};

class MorphElements
{
protected:
	uint16_t            morphsNum;
	MorphEntry*         morphEntries;

	CacheLineAllocator* const pAllocator;

public:
	MorphElements(CacheLineAllocator* pAllocator);

	inline uint16_t getMorphsNum() const
	{
		return morphsNum;
	}

	inline MorphEntry* getMorphEntries() const
	{
		return morphEntries;
	}

	inline LayerObjectMorph* getMorph(const char* name) const
	{
		uint32_t hash = math::convertToHash(name);

		for (uint16_t i = 0; i < morphsNum; ++i)
		{
			if (hash == morphEntries[i].hash)
			{
				return morphEntries[i].layerObjectMorph;
			}
		}

		return nullptr;
	}

protected:
	void addMorph(LayerObjectMorph* layerObjectMorph);
public:

	virtual LayerObjectMorph* createMorph(const char* morphName) = 0;
	virtual LayerObjectMorph* createMorph(uint32_t hash) = 0;
	void copy(const MorphElements& copy);
	bool removeMorph(LayerObjectMorph* layerObjectMorph);
	void clear();

	void freeAllocatedSpace();
};

}