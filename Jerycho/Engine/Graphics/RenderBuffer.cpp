#include "RenderBuffer.hpp"

#include "../Manager/DisplayManager.hpp"
#include "../Manager/ResourceManager.hpp"

namespace ja
{

RenderBuffer::RenderBuffer(uint32_t maxObjectsPerRenderCommand) : maxObjectsPerRenderCommand(setup::graphics::MAX_OBJECTS_PER_RENDER_COMMAND)
{
	glGenVertexArrays(1, &vertexArrayID);
	glGenBuffers(1, &vertexPositionBuffer);
	glGenBuffers(1, &colorBuffer);
	glGenBuffers(1, &normalBuffer);
	glGenBuffers(1, &secondaryColorBuffer);
	glGenBuffers(1, &indicesBuffer);
	glGenBuffers(1, &rotMatricesBuffer);
	glGenBuffers(1, &materialIdBuffer);
	glGenBuffers(1, &objectIdBuffer);
	glGenBuffers(1, &mvpMatrixBuffer);
}

RenderBuffer::~RenderBuffer()
{
	glDeleteBuffers(1, &vertexPositionBuffer);
	glDeleteBuffers(1, &colorBuffer);
	glDeleteBuffers(1, &normalBuffer);
	glDeleteBuffers(1, &secondaryColorBuffer);
	glDeleteBuffers(1, &indicesBuffer);
	glDeleteBuffers(1, &rotMatricesBuffer);
	glDeleteBuffers(1, &materialIdBuffer);
	glDeleteBuffers(1, &objectIdBuffer);
	glDeleteBuffers(1, &mvpMatrixBuffer);

	glDeleteVertexArrays(1, &vertexArrayID);
}

void RenderBuffer::sendDataToGPU()
{
	uint32_t verticesNum = verticesPositions.getSize();

	{
		jaVector2* pVerticesPositions = verticesPositions.getFirst();
		glBindBuffer(GL_ARRAY_BUFFER, this->vertexPositionBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(jaVector2) * verticesNum, pVerticesPositions, GL_STATIC_DRAW);
	}

	{
		Color4f* pColors = colors.getFirst();
		glBindBuffer(GL_ARRAY_BUFFER, this->colorBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Color4f) * verticesNum, pColors, GL_STATIC_DRAW);
	}
	
	{
		jaVector4* pNormals = normals.getFirst();
		glBindBuffer(GL_ARRAY_BUFFER, this->normalBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(jaVector4) * verticesNum, pNormals, GL_STATIC_DRAW);
	}
	
	{
		Color4f* pSecondaryColors = secondaryColors.getFirst();
		glBindBuffer(GL_ARRAY_BUFFER, this->secondaryColorBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Color4f) * verticesNum, pSecondaryColors, GL_STATIC_DRAW);
	}

	{
		uint32_t*      pIndices   = indices.getFirst();
		const uint32_t indicesNum = indices.getSize();
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->indicesBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * indicesNum, pIndices, GL_STATIC_DRAW);	
	}

	{
		int32_t* pMaterialIds = materialIds.getFirst();
		glBindBuffer(GL_ARRAY_BUFFER, this->materialIdBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(int32_t) * verticesNum, pMaterialIds, GL_STATIC_DRAW);
	}

	{
		int32_t* pObjectIds = objectIds.getFirst();
		glBindBuffer(GL_ARRAY_BUFFER, this->objectIdBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(int32_t) * verticesNum, pObjectIds, GL_STATIC_DRAW);
	}

	//modelMatrices;   // send while rendering
	//rotMatrices      // send while rendering
	//mvpMatrices      // send while rendering
	//shaderObjectData // send while rendering

	sendMaterialsToGPU();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void RenderBuffer::sendMaterialsToGPU()
{
	HashMap<Resource<ShaderBinder>>* shaderBinders = ResourceManager::getSingleton()->getShaderBinders();
	const uint32_t shaderBindersNum = shaderBinders->getSize();
	HashEntry<Resource<ShaderBinder>>* pShaderBinderEntry = shaderBinders->getFirst();

	for (uint32_t i = 0; i < shaderBindersNum; ++i)
	{
		ShaderBinder* pShaderBinderIt = pShaderBinderEntry->data->resource;

		const StackAllocator<Material*>* materials    = pShaderBinderIt->getMaterials();
		Material**                       pMaterials   = materials->getFirst();
		const uint32_t                   materialsNum = materials->getSize();

		if (0 == materialsNum)
			continue;

		const UniformBlockInfo* pUniformBlockInfo = pShaderBinderIt->getMaterialUniformBlock();

		const int32_t materialSize = pShaderBinderIt->getMaterialSize();

		uint8_t* pMemBlockBuffer   = (uint8_t*)pUniformBlockInfo->getBufferData();
		uint8_t* pMemBlockBufferIt = (uint8_t*)pMemBlockBuffer;

		for (uint32_t z = 0; z < materialsNum; ++z)
		{
			memcpy(pMemBlockBufferIt, pMaterials[z]->getMaterialData(), materialSize);
			pMemBlockBufferIt += materialSize;
		}

		const int32_t materialBlockSize = materialSize * (int32_t)materialsNum;

		glBindBuffer(GL_UNIFORM_BUFFER, pShaderBinderIt->getMaterialUniformBlockHandle());
		glBufferData(GL_UNIFORM_BUFFER, materialBlockSize, pMemBlockBuffer, GL_STATIC_DRAW);
		glBindBuffer(GL_UNIFORM_BUFFER, 0);
	}
}

void RenderBuffer::clear()
{
	this->totalIndicesNum  = 0;
	this->totalVerticesNum = 0;

	this->verticesPositions.clear();
	this->colors.clear();
	this->normals.clear();
	this->secondaryColors.clear();
	this->modelMatrices.clear();
	this->rotMatrices.clear();
	this->indices.clear();
	this->materialIds.clear();
	this->objectIds.clear();
	this->shaderObjectData.clear();
	this->mvpMatrices.clear();

	renderCommands.clear();

	this->currentRenderCommand = nullptr;
}

RenderCommand* RenderBuffer::addRenderCommand(const jaMatrix44& viewProjectionMatrix, const Material* material)
{
	RenderCommand* renderCommand = new (renderCommands.push()) RenderCommand(material);

	renderCommand->viewProjectionMatrix = viewProjectionMatrix;

	if (nullptr == this->currentRenderCommand) {
		renderCommand->indexRangeStart = 0;
	} else {
		renderCommand->indexRangeStart = totalIndicesNum; 
	}
	
	renderCommand->objectsNumPerRenderCommand = 0;
	renderCommand->indicesNum = 0; 

	this->currentRenderCommand = renderCommand;

	return this->currentRenderCommand;
}

RenderCommand* RenderBuffer::addRenderCommand(const Material* material)
{
	return addRenderCommand(this->currentRenderCommand->viewProjectionMatrix, material);
}

void RenderBuffer::applyViewMatricesToMvpMatrices()
{
	RenderCommand* pRenderCommand    = renderCommands.getFirst();
	uint32_t       renderCommandsNum = (uint32_t)renderCommands.getSize();

	const uint32_t    objectsNum     = modelMatrices.getSize();
	const jaMatrix44* pModelMatrices = modelMatrices.getFirst();
	jaMatrix44*       pMvpMatrices   = mvpMatrices.pushArray(objectsNum);

	uint32_t matricesIndex = 0;

	for (uint32_t i = 0; i < renderCommandsNum; ++i)
	{
		jaVectormath2::mulMatrix44_SSE(pRenderCommand[i].viewProjectionMatrix, &pModelMatrices[matricesIndex], &pMvpMatrices[matricesIndex], pRenderCommand[i].objectsNumPerRenderCommand);

		matricesIndex += pRenderCommand[i].objectsNumPerRenderCommand;
	}
}

void RenderBuffer::bindBuffers()
{
	glBindVertexArray(vertexArrayID);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexPositionBuffer); // layout 0
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer); // layout 1
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer); // layout 2
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, secondaryColorBuffer); // layout 3
	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(4);
	glBindBuffer(GL_ARRAY_BUFFER, materialIdBuffer); // layout 4
	glVertexAttribIPointer(4, 1, GL_INT, 0, (void*)0);

	glEnableVertexAttribArray(5);
	glBindBuffer(GL_ARRAY_BUFFER, objectIdBuffer); // layout 5
	glVertexAttribIPointer(5, 1, GL_INT, 0, (void*)0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer);
}

void RenderBuffer::unbindBuffers()
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
	glDisableVertexAttribArray(5);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
	glBindVertexArray(0);
}

void RenderBuffer::render()
{
	const RenderCommand* const  pRenderCommand    = renderCommands.getFirst();
	const uint32_t              renderCommandsNum = renderCommands.getSize();

	this->bindBuffers();

	const jaMatrix44* const pRotMatrices        = rotMatrices.getFirst();
	const jaMatrix44* const pMvpMatrices        = mvpMatrices.getFirst();
	const uint8_t*    const pShaderObjectData   = shaderObjectData.getFirst();
	const uint8_t*          pShaderObjectDataIt = pShaderObjectData;

	const Material* pPreviousMaterial = nullptr;
	uint32_t        objectIndex       = 0;
	
	if (renderCommandsNum > 0)
	{
		pPreviousMaterial = pRenderCommand[0].material;
		ShaderBinder* pShaderBinder = pPreviousMaterial->getShaderBinder();
		pShaderBinder->bind();
		glBindBufferBase(GL_UNIFORM_BUFFER, setup::graphics::MATERIAL_UNIFORM_BLOCK_BINDING_POINT, pShaderBinder->getMaterialUniformBlockHandle());
		glBindBufferBase(GL_UNIFORM_BUFFER, setup::graphics::SETTING_UNIFORM_BLOCK_BINDING_POINT, pShaderBinder->getSettingUniformBlockHandle());
		glBindBufferBase(GL_UNIFORM_BUFFER, setup::graphics::SHADER_OBJECT_UNIFORM_BLOCK_BINDING_POINT, pShaderBinder->getShaderObjectUniformBlockHandle());
		glBlendEquation(pPreviousMaterial->blendEquation);
		glBlendFunc(pPreviousMaterial->sourceFactor, pPreviousMaterial->destinationFactor);
	}
	
	for (uint32_t i = 0; i < renderCommandsNum; ++i)
	{
		const RenderCommand* const pCurrentRenderCommand = &pRenderCommand[i];
		const Material*      const pCurrentMaterial      = pCurrentRenderCommand->material;
		const ShaderBinder*  const pCurrentShaderBinder  = pCurrentMaterial->getShaderBinder();

		if (pPreviousMaterial != pCurrentMaterial)
		{
			if (pPreviousMaterial->getShaderBinder() != pCurrentShaderBinder)
			{
				pCurrentShaderBinder->bind();
				glBindBufferBase(GL_UNIFORM_BUFFER, setup::graphics::MATERIAL_UNIFORM_BLOCK_BINDING_POINT, pCurrentShaderBinder->getMaterialUniformBlockHandle());
				glBindBufferBase(GL_UNIFORM_BUFFER, setup::graphics::SETTING_UNIFORM_BLOCK_BINDING_POINT, pCurrentShaderBinder->getSettingUniformBlockHandle());
				glBindBufferBase(GL_UNIFORM_BUFFER, setup::graphics::SHADER_OBJECT_UNIFORM_BLOCK_BINDING_POINT, pCurrentShaderBinder->getShaderObjectUniformBlockHandle());
			}
		
			if (pPreviousMaterial->blendEquation != pCurrentMaterial->blendEquation)
				glBlendEquation(pCurrentMaterial->blendEquation);
			
			if ((pPreviousMaterial->sourceFactor != pCurrentMaterial->sourceFactor) || (pPreviousMaterial->destinationFactor != pCurrentMaterial->destinationFactor))
				glBlendFunc(pCurrentMaterial->sourceFactor, pCurrentMaterial->destinationFactor);
		}
			

		if (-1 != pCurrentShaderBinder->getRotMatricesUniformLocation())
		{
			glUniformMatrix4fv(pCurrentShaderBinder->getRotMatricesUniformLocation(), pCurrentRenderCommand->objectsNumPerRenderCommand, GL_FALSE, pRotMatrices[objectIndex].element);
		}

		if (-1 != pCurrentShaderBinder->getMvpMatricesUniformLocation())
		{
			glUniformMatrix4fv(pCurrentShaderBinder->getMvpMatricesUniformLocation(), pCurrentRenderCommand->objectsNumPerRenderCommand, GL_FALSE, pMvpMatrices[objectIndex].element);
		}

		//Sending shader object uniform buffer
		{
			const uint32_t shaderObjectsUpdateSize = pCurrentRenderCommand->objectsNumPerRenderCommand * pCurrentShaderBinder->getShaderObjectSize();

			glBindBuffer(GL_UNIFORM_BUFFER, pCurrentShaderBinder->getShaderObjectUniformBlockHandle());
			glBufferSubData(GL_UNIFORM_BUFFER, 0, shaderObjectsUpdateSize, pShaderObjectDataIt);

			pShaderObjectDataIt += shaderObjectsUpdateSize;
		}

		glDrawElements(GL_TRIANGLES, pCurrentRenderCommand->indicesNum, GL_UNSIGNED_INT, (void*)(pCurrentRenderCommand->indexRangeStart * sizeof(uint32_t)));

		objectIndex       += pCurrentRenderCommand->objectsNumPerRenderCommand;
		pPreviousMaterial  = pCurrentMaterial;
	}

	this->unbindBuffers();

	glUseProgram(0);
}



}