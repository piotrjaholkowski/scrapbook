#pragma once

#include "Shader.hpp"
#include "Layer.hpp"

#ifdef JA_SDL2_VERSION
#include <SDL2/SDL_mutex.h>
#else
#include <SDL/SDL_mutex.h>
#endif


namespace ja
{

	class ScheduleShaderLoad
	{
	public:
		ja::string fileName;
		ShaderType shaderType;
		Shader*    schedludedShader;

		bool       scheduleOn;
		bool       result;

		ScheduleShaderLoad() {
			scheduleOn = false;
		}
	};

	class ScheduleShaderBinderCreate
	{
	public:
		std::string binderName;
		Shader*     fragmentShader;
		Shader*     vertexShader;
		Shader*     geometryShader;
		ShaderBinder* scheduledShaderBinder;

		bool scheduleOn;
		bool result;

		ScheduleShaderBinderCreate()
		{
			scheduleOn = false;
		}
	};

	class ScheduleShaderDelete
	{
	public:
		Shader*    schedludedShader;

		bool       scheduleOn;
		bool       result;

		ScheduleShaderDelete() {
			scheduleOn = false;
		}
	};

	class ScheduleShaderBinderDelete
	{
	public:
		ShaderBinder* scheduledShaderBinder;

		bool scheduleOn;
		bool result;

		ScheduleShaderBinderDelete()
		{
			scheduleOn = false;
		}
	};

	/*struct ScheduleFbo
	{
	public:
		const char* name;
		float   bufferWidth;
		float   bufferHeight;
		int32_t textureFlag;
		bool    linear;
		bool    scheduleOn;

		ScheduleFbo() {
			scheduleOn = false;
		}
	};*/

	
	class ShaderLoader
	{
	private:
		static  ShaderLoader* singleton;
		char    debugInfo[1024];
		int32_t length;
		ShaderLoader();

		static ScheduleShaderLoad   scheduleShaderLoad;
		static ScheduleShaderDelete scheduleShaderDelete;
		//static ScheduleFbo scheduleFbo;
		static ScheduleShaderBinderCreate scheduleShaderBinderCreate;
		static ScheduleShaderBinderDelete scheduleShaderBinderDelete;
		static SDL_mutex* loadingMutex;
		static SDL_cond*  alreadyLoadedCondition;

		static void getMaterialUniformBlock(ShaderBinder* shaderBinder, const char* uniformBlockName);
		static void getSettingUniformBlock(ShaderBinder* shaderBinder, const char* uniformBlockName);
		static void getShaderObjectUniformBlock(ShaderBinder* shaderBinder, const char* uniformBlockName);

		static void logActiveUniforms(const ShaderBinder* shaderBinder);
		static void logActiveUniformBlocks(const ShaderBinder* shaderBinder);
		static void logUniformBlockInfo(const ShaderBinder* shaderBinder, UniformBlockInfo* pUniformBlock);
	public:
		static void init();
		static ShaderLoader* getSingleton();
		static bool loadShader(const char* fileName, ShaderType shaderType, Shader* shader);
		static bool deleteShader(Shader* shader);
		//static void createFbo(const char* fboName, float widthPercent, float heightPercent, int32_t textureFlag, bool linear);
		static bool createShaderBinder(const char* binderName, const Shader* vertexShader, const Shader* fragmentShader, const Shader* geometryShader, ShaderBinder* shaderBinder);
		static bool deleteShaderBinder(ShaderBinder* shaderBinder);
		~ShaderLoader();

		static bool scheduleLoadShader(const char* fileName, ShaderType shaderType, Shader* shader);
		static bool scheduleDeleteShader(Shader* shader);
		static bool scheduleCreateShaderBinder(const char* binderName, const Shader* vertexShader, const Shader* fragmentShader, const Shader* geometryShader, ShaderBinder* shaderBinder);
		static bool scheduleDeleteShaderBinder(ShaderBinder* shaderBinder);
		//static void          scheduleCreateFbo(const char* fboName, float widthPercent, float heightPercent, int32_t textureFlag, bool linear);

		static void queryScheduleLoading(); // called by main thread

		static void cleanUp();
	};

}
