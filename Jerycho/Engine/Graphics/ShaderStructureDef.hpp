#pragma once

#include "../Data/DataModel.hpp"
#include "../Graphics/ShaderBinder.hpp"

namespace ja
{

typedef NameTag<setup::TAG_NAME_LENGTH>  ShaderStructureTag;
typedef NameTag<setup::FILE_NAME_LENGTH> FileNameTag;

class ShaderBinder;

class ShaderStructureDef
{
private:
	ShaderBinder* shaderBinder;
	uint32_t      shaderStructureSize;
public:
	StackAllocator<DataFieldInfo> dataFields;

private:
	ShaderStructureTag shaderStructureName;
	FileNameTag        fileName;
public:

	ShaderStructureDef(const char* shaderStructureName, const char* shaderStructureFileName, ShaderBinder* shaderBinder, uint32_t shaderStructureSize) : shaderBinder(shaderBinder)
	{
		this->shaderStructureName.setName(shaderStructureName);
		this->fileName.setName(shaderStructureFileName);

		this->shaderStructureSize = shaderStructureSize;
	}

	inline ShaderBinder* getShaderBinder() const
	{
		return this->shaderBinder;
	}

	inline const StackAllocator<DataFieldInfo>* getDataFields() const
	{
		return &this->dataFields;
	}

	inline int32_t getShaderStructureSize() const
	{
		return this->shaderStructureSize;

	}
};

}