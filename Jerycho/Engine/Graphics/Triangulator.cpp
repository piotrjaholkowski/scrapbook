#include "Triangulator.hpp"

namespace ja
{

// Constructor
Triangulator::Triangulator() : gluTesselator(nullptr)
{
	
}

// Destructor
Triangulator::~Triangulator()
{
	if (this->gluTesselator)
		gluDeleteTess(this->gluTesselator);
}

// called when triangulator starts new primitive
void __stdcall tessBegin(GLenum type, TessContext * ctx);
// called when triangulator outputs new vertex
void __stdcall tessVertex(GLvoid *, TessContext * ctx);

// PS: previous functions have behavior similar to immediate mode functions glVertex, glBegin, glEnd from older versions of OpenGL

// Initializes triangulation
void Triangulator::beginTriangulation()
{
	if (this->gluTesselator)
		gluDeleteTess(this->gluTesselator);

	this->gluTesselator = gluNewTess();

	// set callback functions for triangulator
	gluTessCallback(this->gluTesselator, GLU_TESS_BEGIN_DATA, (void (__stdcall*)())tessBegin);
	gluTessCallback(this->gluTesselator, GLU_TESS_VERTEX_DATA, (void (__stdcall*)())tessVertex);

	gluTessProperty(this->gluTesselator, GLU_TESS_WINDING_RULE, context.windingRule);
	gluTessProperty(this->gluTesselator, GLU_TESS_TOLERANCE, 0);

	// set normal that is used by default
	// it point in direction of Z axis, as we work in XY plane
	gluTessNormal(this->gluTesselator, 0, 0, 1);

	// create and set context for triangulator - context is object
	// that is visible to triangulator during triangulation
	context.clearContext();
	gluTessBeginPolygon(this->gluTesselator, &context);
}

// starts new contour
void Triangulator::beginContour()
{
	gluTessBeginContour(this->gluTesselator);
}

// adds new vertex to current contour
void Triangulator::addVertex(const jaVector2& vertex)
{
	GLdouble * pInput = (double*)context.dataAllocator->allocate(sizeof(double) * 4);
	pInput[0] = vertex.x;
	pInput[1] = vertex.y;
	pInput[2] = 0;

	gluTessVertex(this->gluTesselator, pInput, pInput);
}

// ends current contour
void Triangulator::endContour()
{
	gluTessEndContour(this->gluTesselator);
}

// triangulates saved contours and removes GLU triangulator
void Triangulator::endTriangulation()
{
	gluTessEndPolygon(this->gluTesselator);

	if (this->gluTesselator)
	{
		gluDeleteTess(this->gluTesselator);
		this->gluTesselator = nullptr;
	}
}

// called when triangulator starts new primitive
// sets type of primitive, and that nor first,
// nor second vertices aren't set
void __stdcall tessBegin(GLenum type, TessContext * ctx)
{
	ctx->currentPrimitiveType = type;
	ctx->isFirstVertex  = false;
	ctx->isSecondVertex = false;
}

// called when triangulator adds new vertex to results
void __stdcall tessVertex(GLvoid * data, TessContext * ctx)
{
	// new vertex
	GLdouble * coord = (GLdouble*)data;

	new (ctx->vertices.push()) jaVector2((float)coord[0], (float)coord[1]);

	// save results - depends on type of primitive
	if (ctx->currentPrimitiveType == GL_TRIANGLES)
	{
		ctx->indices.push(ctx->currentIndex++);
	}
	else if (ctx->currentPrimitiveType == GL_TRIANGLE_FAN)
	{
		if (ctx->isSecondVertex)
		{
			// new triangle
			ctx->indices.push(ctx->firstVertexIndex);
			ctx->indices.push(ctx->secondVertexIndex);
			ctx->indices.push(ctx->currentIndex);

			// index of next vertex
			ctx->secondVertexIndex = ctx->currentIndex++;
		}
		else if (ctx->isFirstVertex)
		{
			// second vertex in triangle
			ctx->isSecondVertex    = true;
			ctx->secondVertexIndex = ctx->currentIndex++;
		}
		else
		{
			// first vertex in triangle
			ctx->isFirstVertex    = true;
			ctx->firstVertexIndex = ctx->currentIndex++;
		}
	}
	else if (ctx->currentPrimitiveType == GL_TRIANGLE_STRIP)
	{
		if (ctx->isSecondVertex)
		{
			// new triangle
			ctx->indices.push(ctx->firstVertexIndex);
			ctx->indices.push(ctx->secondVertexIndex);
			ctx->indices.push(ctx->currentIndex);

			// to next vertex
			ctx->firstVertexIndex  = ctx->secondVertexIndex;
			ctx->secondVertexIndex = ctx->currentIndex++;
		}
		else if (ctx->isFirstVertex)
		{
			// second vertex in triangle
			ctx->isSecondVertex    = true;
			ctx->secondVertexIndex = ctx->currentIndex++;
		}
		else
		{
			// first vertex in triangle
			ctx->isFirstVertex    = true;
			ctx->firstVertexIndex = ctx->currentIndex++;
		}
	}
}

}