#pragma once

#include "Color4f.hpp"

#include "../Math/VectorMath2d.hpp"
#include "../Allocators/CacheLineAllocator.hpp"
#include "../Manager/DisplayManager.hpp"

#include "../Gilgamesh/Shapes/Box2d.hpp"

namespace ja
{
	class Polygon;

	class DrawElements
	{
	private:
		uint32_t   verticesNum;
		jaVector2* verticesPositions;
		Color4f*   verticesColors;
		jaVector2* verticesNormals;
		
		uint32_t  indicesNum;
		uint32_t* indices;

		jaFloat boundingVolumeRadius;


		CacheLineAllocator* const pAllocator;

		inline void deleteVertices()
		{
			if (nullptr != this->verticesPositions)
			{
				this->pAllocator->free(this->verticesPositions, (this->verticesNum * sizeof(jaVector2)));
				this->verticesPositions = nullptr;
			}

			if (nullptr != this->verticesColors)
			{
				this->pAllocator->free(this->verticesColors, (this->verticesNum * sizeof(Color4f)));
				this->verticesColors = nullptr;
			}

			if (nullptr != this->verticesNormals)
			{
				this->pAllocator->free(this->verticesNormals, (this->verticesNum * sizeof(jaVector2)));
				this->verticesNormals = nullptr;
			}

			this->verticesNum = 0;
		}

	public:

		inline void deleteTriangleIndices()
		{
			if (nullptr != this->indices)
			{
				this->pAllocator->free(this->indices, this->indicesNum * sizeof(uint32_t));
				this->indices    = nullptr;
				this->indicesNum = 0;
			}
		}

	public:
		DrawElements(CacheLineAllocator* pCustomAllocator);

		inline void freeAllocatedSpace()
		{
			deleteVertices();
			deleteTriangleIndices();
		}

		inline CacheLineAllocator* getAllocator() const
		{
			return this->pAllocator;
		}

		inline void bindVertices()
		{
			glVertexPointer(2, GL_FLOAT, sizeof(jaVector2), this->verticesPositions);
			glColorPointer(4, GL_FLOAT, sizeof(Color4f), this->verticesColors);

			//if (nullptr != this->verticesNormals)
			//{
			//	glNormalPointer(2, GL_FLOAT, sizeof(jaVector2), this->verticesNormals);
			//}
		}

		inline void drawFaces()
		{
			if (this->indicesNum > 0)
			{
				glDrawElements(GL_TRIANGLES, this->indicesNum, GL_UNSIGNED_INT, this->indices);
			}
		}

		inline jaFloat getBoundingVolumeRadius()
		{
			return this->boundingVolumeRadius;
		}

		inline void updateBoundingVolume()
		{
			jaFloat  biggestSquaredDistance = 0.0f;

			for (uint32_t i = 0; i < verticesNum; ++i)
			{
				jaFloat squaredDistance = (verticesPositions[i].x * verticesPositions[i].x) + (verticesPositions[i].y * verticesPositions[i].y);

				if (squaredDistance > biggestSquaredDistance)
				{
					biggestSquaredDistance = squaredDistance;
				}
			}

			this->boundingVolumeRadius = sqrtf(biggestSquaredDistance);
		}

		void       setVerticesPositions(const jaVector2* const pVertices, uint32_t verticesNum);
		jaVector2* allocateVerticesPositions(uint32_t verticesNum);

		void setVerticesColors(const Color4f* const pColors, uint32_t verticesNum);
		void setVerticesColor(const Color4f& color, uint32_t verticesNum);
		
		void      setTriangleIndices(const uint32_t* const pTriangleIndices, uint32_t indicesNum);
		uint32_t* allocateTriangleIndices(uint32_t indicesNum);

		inline const jaVector2* getVerticesPositions() const
		{
			return this->verticesPositions;
		}

		inline const Color4f* getVerticesColors() const
		{
			return this->verticesColors;
		}

		inline uint32_t getVerticesNum() const
		{
			return this->verticesNum;
		}

		inline const uint32_t* getTriangleIndices() const
		{
			return this->indices;
		}

		inline uint32_t getTriangleIndicesNum() const
		{
			return this->indicesNum;
		}

		inline void copy(const DrawElements& drawElements)
		{
			deleteVertices();
			deleteTriangleIndices();

			if (0 != drawElements.verticesNum)
				setVerticesPositions(drawElements.verticesPositions, drawElements.verticesNum);

			if (0 != drawElements.verticesNum)
				setVerticesColors(drawElements.verticesColors, drawElements.verticesNum);

			if (0 != drawElements.indicesNum)
				setTriangleIndices(drawElements.indices, drawElements.indicesNum);
		}

		bool isIntersecting(jaTransform2& transformBoxToDrawElementSpace, const gil::Box2d& box, const jaVector2& drawElementScale);
	};
}