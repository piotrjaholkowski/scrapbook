#ifndef JA_CYCLIC_FUNCTION_LOG_H
#define JA_CYCLIC_FUNCTION_LOG_H

#include "../jaSetup.h"

class jaCyclicFunctionLog
{
	char message[JA_CYCLIC_FUNCTION_LOG_MESSAGE_SIZE];

public:

	jaCyclicFunctionLog(const char* logMessage)
	{
		message[0] = '+';
		strcpy(&this->message[1],message);
		jaLogManager::getSingleton()->cyclicFunctionLog(message);
	}

	~jaCyclicFunctionLog()
	{
		message[0] = '-';
		jaLogManager::getSingleton()->cyclicFunctionLog(message);
	}

};

#endif JA_CYCLIC_FUNCTION_LOG_H