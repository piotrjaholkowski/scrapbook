#include "DataModel.hpp"

#include "../Gui/Label.hpp"
#include "../Gui/Scrollbar.hpp"
#include "../Gui/Checkbox.hpp"
#include "../Gui/FlowLayout.hpp"
#include "../Gui/Margin.hpp"

#include "../Manager/ScriptManager.hpp"

#include "../Entity/DataGroup.hpp"

namespace ja
{ 

DataModel::DataModel(const char* dataModelName, const char* dataModelFileName, const ScriptFunction* initDataFunction) : initDataFunction(initDataFunction)
{
	this->modelName.setName(dataModelName);
	this->fileName.setName(dataModelFileName);

	this->resourceId = this->modelName.getHash();
}

void DataModel::updateScriptFunctionReference(const int32_t oldReference, const int32_t newReference)
{
	const uint32_t     dataFieldsNum = dataFields.getSize();
	ja::DataFieldInfo* pDataField    = dataFields.getFirst();

	for (uint32_t i = 0; i < dataFieldsNum; ++i)
	{
		if (pDataField->accessDataFunctionRef == oldReference)
		{
			pDataField->accessDataFunctionRef = newReference;
		}
	}
}

void DataModel::createModelTable()
{
	lua_State* L = ScriptManager::getSingleton()->getLuaState();

	lua_newtable(L);

	const uint32_t     dataFieldsNum = this->dataFields.getSize();
	ja::DataFieldInfo* pDataField    = this->dataFields.getFirst();

	for (uint32_t i = 0; i < dataFieldsNum; ++i)
	{
		lua_pushstring(L, pDataField[i].dataFieldName.getName());
		lua_pushlightuserdata(L, &pDataField[i]);
		lua_settable(L, -3);
	}

	lua_setglobal(L, this->modelName.getName());
}

DataNotifier::DataNotifier(void* allocatedData, uint32_t elementId, DataFieldInfo* dataFieldInfo) : dataFieldInfo(dataFieldInfo), elementId(elementId)
{
	int32_t dataType        = (int32_t)this->dataFieldInfo->dataType;
	int32_t dataFieldOffset = this->dataFieldInfo->dataFieldOffset;

	this->dataOffset   = dataFieldOffset + (elementId * DataTypeInfo::typeSize[dataType]);
	this->fieldDataPtr = ((uint8_t*)allocatedData + dataFieldOffset) + (elementId * DataTypeInfo::typeSize[dataType]);
}

void DataNotifier::setValue(void* dataValue)
{
	memcpy(this->fieldDataPtr, dataValue, DataTypeInfo::typeSize[(int32_t)this->dataFieldInfo->dataType]);

	if (this->dataChangeCallback)
		this->dataChangeCallback(this);
}

Widget* DataNotifier::createDataWidget()
{
	return this->dataFieldInfo->createDataWidget(this);
}

void DataFieldInfo::setLabelValue(Label* label, void* dataValue)
{
	switch (this->dataType)
	{
		case DataType::Float:
		{
			float dataFloat = *((float*)dataValue);
			label->setTextPrintC("%f", dataFloat);
		}
			break;
		case DataType::Int32:
		{
			int32_t dataInt = *((int32_t*)dataValue);
			label->setTextPrintC("%d", dataInt);
		}
			break;

		case DataType::Bool:
		{
			int32_t dataBool = *((int32_t*)dataValue);
			const char* valString = (dataBool == 0) ? "false" : "true";

			label->setText(valString);
		}
			break;
	}
}

void DataFieldInfo::setScrollbarValue(Scrollbar* scrollbar, void* dataValue)
{
	switch (this->dataType)
	{
		case DataType::Float:
		{
			float dataFloat = *((float*)dataValue);
			scrollbar->setValue((double)dataFloat);
		}
		break;
		case DataType::Int32:
		{
			int32_t dataInt = *((int32_t*)dataValue);
			scrollbar->setValue((double)dataInt);
		}
		break;
		case DataType::Bool:
		{
			int32_t dataBool = *((int32_t*)dataValue);
			scrollbar->setValue((double)dataBool);
		}
	}
}

void DataFieldInfo::setCheckboxValue(Checkbox* checkbox, void* dataValue)
{
	switch (this->dataType)
	{
	case DataType::Float:
	{
		float dataFloat = *((float*)dataValue);
		bool  isChecked = (dataFloat <= 0.0f) ? false : true;

		checkbox->setCheck(isChecked);
	}
	break;
	case DataType::Int32:
	{
		int32_t dataInt = *((int32_t*)dataValue);
		bool  isChecked = (dataInt <= 0) ? false : true;

		checkbox->setCheck(isChecked);
	}
	break;
	case DataType::Bool:
	{
		int32_t dataBool = *((int32_t*)dataValue);
		checkbox->setCheck(dataBool != 0);
	}
	}
}

void DataFieldInfo::onScrollbarChange(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	Scrollbar* scrollbar = (Scrollbar*)sender;

	double currentValue = scrollbar->getValue();

	DataNotifier* dataNotifier = (DataNotifier*)scrollbar->vUserData;
	Label*        labelValue   = (Label*)dataNotifier->siblingWidget;

	switch (dataType)
	{
		case DataType::Int32:
		{
			int32_t dataValue = (int32_t)currentValue;
			setLabelValue(labelValue, &dataValue);
			dataNotifier->setValue(&dataValue);
		}
		break;
		case DataType::Float:
		{
			float dataValue = (float)currentValue;
			setLabelValue(labelValue, &dataValue);
			dataNotifier->setValue(&dataValue);
		}
		break;
		case DataType::Bool:
		{
			int32_t dataValue = (int32_t)currentValue;
			setLabelValue(labelValue, &dataValue);
			dataNotifier->setValue(&dataValue);
		}
		break;
	}
	
	//dataNotifier->callNotifier();
}

void DataFieldInfo::onCheckboxChange(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	Checkbox* checkbox = (Checkbox*)sender;

	bool currentValue = checkbox->isChecked();

	DataNotifier* dataNotifier = (DataNotifier*)checkbox->vUserData;
	Label*        labelValue = (Label*)dataNotifier->siblingWidget;

	switch (dataType)
	{
	case DataType::Int32:
	{
		int32_t dataValue = (int32_t)currentValue;
		setLabelValue(labelValue, &dataValue);
		dataNotifier->setValue(&dataValue);
	}
	break;
	case DataType::Float:
	{
		float dataValue = (float)currentValue;
		setLabelValue(labelValue, &dataValue);
		dataNotifier->setValue(&dataValue);
	}
	break;
	case DataType::Bool:
	{
		int32_t dataValue = (int32_t)currentValue;
		setLabelValue(labelValue, &dataValue);
		dataNotifier->setValue(&dataValue);
	}
	break;
	}

	//dataNotifier->callNotifier();
}

Widget* DataFieldInfo::createDataWidget(DataNotifier* dataNotifier)
{
	//this->allocatedField = ((uint8_t*)allocatedData + this->dataFieldOffset) + (elementId * DataTypeInfo::typeSize[(int32_t)this->dataType]);

	FlowLayout* flowLayout = new FlowLayout(0, nullptr, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 0, 0);
	flowLayout->setSize(300, 20);
	flowLayout->setColor(255, 0, 0, 128);

	Label* fieldLabelName = new Label(0, nullptr);
	fieldLabelName->setFont("default.ttf",10);
	fieldLabelName->setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);

	if (this->dataFieldSize == DataTypeInfo::typeSize[(int32_t)this->dataType])
	{
		fieldLabelName->setText(this->dataFieldName.getName());
	}
	else
	{
		fieldLabelName->setTextPrintC("%s[%d]", this->dataFieldName, dataNotifier->elementId);
	}

	fieldLabelName->setSize(100, 20);
	fieldLabelName->setColor(255, 255, 255, 255);

	flowLayout->addWidget(fieldLabelName);

	switch (dataWidget)
	{
		case DataWidget::LABEL:
		{
			Label* fieldLabelValue = new Label(1, nullptr);
			fieldLabelValue->setFont("default.ttf",10);
			fieldLabelValue->setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
			
			fieldLabelValue->setColor(255, 255, 255, 255);
			
			setLabelValue(fieldLabelValue, dataNotifier->fieldDataPtr);

			fieldLabelValue->setSize(100, 20);

			flowLayout->addWidget(fieldLabelValue);
		}
		break;
		case DataWidget::SCROLLBAR:
		{
			Label* fieldLabelValue = new Label(1, nullptr);
			fieldLabelValue->setFont("default.ttf",10);
			fieldLabelValue->setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
			
			fieldLabelValue->setColor(255, 255, 255, 255);

			setLabelValue(fieldLabelValue, dataNotifier->fieldDataPtr);
			fieldLabelValue->setSize(100, 20);

			Scrollbar* scrollbar = new Scrollbar(1, nullptr);
			scrollbar->setRange(this->rangeMin, this->rangeMax);
			scrollbar->setColor(20, 20, 20, 150);
			scrollbar->setSize(100, 20);

			dataNotifier->siblingWidget = fieldLabelValue;
			scrollbar->vUserData        = dataNotifier;

			scrollbar->setOnChangeEvent(this, &DataFieldInfo::onScrollbarChange);

			setScrollbarValue(scrollbar, dataNotifier->fieldDataPtr);

			flowLayout->addWidget(fieldLabelValue);
			flowLayout->addWidget(scrollbar);
		}
		break;
		case DataWidget::CHECKBOX:
		{
			Label* fieldLabelValue = new Label(1, nullptr);
			fieldLabelValue->setFont("default.ttf", 10);
			fieldLabelValue->setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);

			fieldLabelValue->setColor(255, 255, 255, 255);

			setLabelValue(fieldLabelValue, dataNotifier->fieldDataPtr);
			fieldLabelValue->setSize(100, 20);

			Checkbox* checkbox = new Checkbox(1, nullptr);
			checkbox->setColor(20, 20, 20, 150);
			checkbox->setSize(18, 18);

			dataNotifier->siblingWidget = fieldLabelValue;
			checkbox->vUserData = dataNotifier;

			checkbox->setOnChangeEvent(this, &DataFieldInfo::onCheckboxChange);

			setCheckboxValue(checkbox, dataNotifier->fieldDataPtr);

			flowLayout->addWidget(fieldLabelValue);
			flowLayout->addWidget(checkbox);
		}
		break;
	}

	return flowLayout;
}




}