#pragma once 

#include "../jaSetup.hpp"

#include "../Allocators/StackAllocator.hpp"
#include "../Math/Math.hpp"

#include "../Gui/Widget.hpp"

#include "../Scripts/Script.hpp"
#include "../Utility/NameTag.hpp"

#include <stdint.h>
#include <FastDelegate.h>

namespace jas
{
	class ResourceManager;
}

namespace ja
{

enum class DataType
{
	Float = 0,
	Int32 = 1,
	Bool  = 2
};

enum class DataWidget
{
	LABEL     = 0,
	SCROLLBAR = 1,
	TEXTFIELD = 2,
	CHECKBOX  = 3
};

class DataTypeInfo
{
public:
	static const uint32_t typeSize[];
	static const char     typeName[][24];// = { "float", "int32", "bool" };
};

class Label;
class Scrollbar;
class Checkbox;
class DataFieldInfo;
class DataNotifier;

typedef fastdelegate::FastDelegate1<DataNotifier*> DataChangeCallback;
typedef NameTag<setup::TAG_NAME_LENGTH>  DataFieldTag;
typedef NameTag<setup::TAG_NAME_LENGTH>  ModelNameTag;
typedef NameTag<setup::FILE_NAME_LENGTH> FileNameTag;

class DataNotifier
{
private:

	uint32_t elementId;
	void*    fieldDataPtr;
	int32_t  dataOffset;

	DataChangeCallback dataChangeCallback;
	DataFieldInfo*     dataFieldInfo;

	Widget* siblingWidget; // filled  by createDataWidget
public:

	DataNotifier(void* allocatedData, uint32_t elementId, DataFieldInfo* dataFieldInfo);

	void setValue(void* dataValue);

	Widget* createDataWidget();

	inline int32_t getDataOffset()
	{
		return this->dataOffset;
	}

	inline int32_t getElementId()
	{
		return this->elementId;
	}

	inline const DataFieldInfo* getDataFieldInfo()
	{
		return this->dataFieldInfo;
	}

	template < class Y, class X >
	void setDataChangeCallback(Y *pthis, void (X::*pmethod)(DataNotifier*))
	{
		dataChangeCallback.bind(pthis, pmethod);
	}

private:

	friend class DataFieldInfo;
};

class DataFieldInfo
{
public:
	int32_t  accessDataFunctionRef;	
	uint32_t dataFieldOffset;
	uint32_t dataFieldSize;
	uint32_t dataNameHash;
	DataType dataType;

	ScriptFunction* accessDataFunction;
	DataWidget      dataWidget;
	float           rangeMin;
	float           rangeMax;

	DataFieldTag dataFieldName;

	static inline uint32_t getHash(const char* dataFieldName)
	{
		return ja::math::convertToHash(dataFieldName);
	}

	inline uint32_t getElementsNum() const
	{
		return (this->dataFieldSize / DataTypeInfo::typeSize[(int32_t)this->dataType]);
	}

	inline void setAccessDataFunction(ScriptFunction* accessDataFunction)
	{
		this->accessDataFunction    = accessDataFunction;

		if (nullptr != this->accessDataFunction)
			this->accessDataFunctionRef = accessDataFunction->functionRef;
		else
			this->accessDataFunctionRef = 0;
	}

	template <typename T>
	void save(FILE* pFile, const char* dataComponentVar, const char* dataModelName, void* allocatedData)
	{
		char* pSetMethod      = nullptr;
		char* pSaveParam      = nullptr;
		char* pSaveCommaParam = nullptr;

		switch (this->dataType)
		{
		case DataType::Int32:
			pSetMethod = "setInt32";
			pSaveParam = "%i";
			pSaveCommaParam = ",%i";
			break;
		case DataType::Float:
			pSetMethod = "setFloat";
			pSaveParam = "%f";
			pSaveCommaParam = ",%f";
			break;
		case DataType::Bool:
			pSetMethod = "setBool";
			pSaveParam = "%i";
			pSaveCommaParam = ",%i";
			break;
		}

		fprintf(pFile, "DataComponent.%s(%s,%s.%s,", pSetMethod, dataComponentVar, dataModelName, this->dataFieldName.getName());

		const uint32_t elementsNum = this->getElementsNum();

		T* dataValue = (T*)((uint8_t*)allocatedData + this->dataFieldOffset);

		if (1 == elementsNum)
		{
			fprintf(pFile, pSaveParam, *dataValue);
			fprintf(pFile, ");\n");
		}
		else
		{
			fprintf(pFile, "{");

			fprintf(pFile, pSaveParam, dataValue[0]);

			for (uint32_t i = 1; i < elementsNum; ++i)
			{
				fprintf(pFile, pSaveCommaParam, dataValue[i]);
			}

			fprintf(pFile, "});\n");
		}
	}

private:
	Widget* createDataWidget(DataNotifier* dataNotifier);

	void    setLabelValue(Label* label, void* dataValue);
	void    setScrollbarValue(Scrollbar* scrollbar, void* dataValue);
	void    setCheckboxValue(Checkbox* checkbox, void* dataValue);

	void    onScrollbarChange(Widget* sender, WidgetEvent action, void* data);
	void    onCheckboxChange(Widget* sender, WidgetEvent action, void* data);

	friend class DataNotifier;
};

class DataModel
{
private:
	uint32_t resourceId;
	uint32_t dataSize;
	
	const ScriptFunction* initDataFunction;

	StackAllocator<DataFieldInfo> dataFields;

	ModelNameTag modelName;
	FileNameTag  fileName;
public:
	DataModel(const char* dataModelName, const char* dataModelFileName, const ScriptFunction* initDataFunction);

	static inline uint32_t DataModel::getHash(const char* modelName)
	{
		return ja::math::convertToHash(modelName);
	}

	inline uint32_t getDataSize()
	{
		return dataSize;
	}

	inline const char* getName() const
	{
		return modelName.getName();
	}

	inline const char* getFileName() const
	{
		return fileName.getName();
	}

	inline StackAllocator<DataFieldInfo>* getDataFields()
	{
		return &dataFields;
	}

	inline const ScriptFunction* getInitDataFunction()
	{
		return initDataFunction;
	}

	void updateScriptFunctionReference(const int32_t oldReference, const int32_t newReference);

	void createModelTable();

	friend class jas::ResourceManager;
};

}