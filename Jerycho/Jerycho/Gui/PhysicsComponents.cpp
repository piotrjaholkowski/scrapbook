#include "PhysicsComponents.hpp"
#include "PhysicsEditor.hpp"
#include "WidgetId.hpp"
#include "Toolset.hpp"


#include "../../Engine/Manager/GameManager.hpp"

#include "../../Engine/Gui/BoxLayout.hpp"
#include "../../Engine/Gui/Margin.hpp"
#include "../../Engine/Gui/Editbox.hpp"
#include "../../Engine/Gui/ProgressButton.hpp"
#include "../../Engine/Gui/GridLayout.hpp"
#include "../../Engine/Gui/FlowLayout.hpp"
#include "../../Engine/Gui/TextField.hpp"
#include "../../Engine/Gui/Scrollbar.hpp"
#include "../../Engine/Gui/Checkbox.hpp"
#include "../../Engine/Gui/RadioButton.hpp"
#include "../../Engine/Gui/GridViewer.hpp"
#include "../../Engine/Gui/SlideWidget.hpp"
#include "../../Engine/Gui/SequenceEditor.hpp"
#include "../../Engine/Gui/ScrollPane.hpp"
#include "../../Engine/Gui/Hider.hpp"
#include "../../Engine/Gui/Magnet.hpp"

using namespace ja;

namespace tr
{

PhysicsComponentMenu* PhysicsComponentMenu::singleton = nullptr;

MaterialEditor*		 MaterialEditor::singleton = nullptr;
VelocityEditor*		 VelocityEditor::singleton = nullptr;
ShapeEditor*		 ShapeEditor::singleton = nullptr;
DampingEditor*		 DampingEditor::singleton = nullptr;
LinearMotorEditor*	 LinearMotorEditor::singleton = nullptr;
DistanceJointEditor* DistanceJointEditor::singleton = nullptr;
BodyTypeEditor*		 BodyTypeEditor::singleton = nullptr;
CollisionMaskEditor* CollisionMaskEditor::singleton = nullptr;
BoxEditor*           BoxEditor::singleton = nullptr;
CircleEditor*        CircleEditor::singleton = nullptr;
ConvexPolygonEditor* ConvexPolygonEditor::singleton = nullptr;
TriangleMeshEditor*  TriangleMeshEditor::singleton = nullptr;
PointEditor*         PointEditor::singleton = nullptr;

PhysicsTimerMenu::PhysicsTimerMenu(uint32_t widgetId, Widget* parent) :  CommonWidget(widgetId, "Timer", parent)
{
	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(180, 20);
	fillBoxLayout(boxLayout);

	FlowLayout* timerFlow = new FlowLayout(0, nullptr, (uint32_t)Margin::LEFT, 0, 0);
	timerFlow->setSize(180, 20);
	fillFlowLayout(timerFlow);

	frameElapsedLabel = new Label(0, nullptr);
	fillLabel(frameElapsedLabel, "0");
	frameElapsedLabel->setSize(70,20);
	timerFlow->addWidget(frameElapsedLabel);

	Label* framesLabel = new Label(1, nullptr);
	fillLabel(framesLabel, " frames");
	timerFlow->addWidget(framesLabel);

	resetButton = new Button(2, nullptr);
	fillButton(resetButton, "Reset");
	resetButton->setOnClickEvent(this, &PhysicsTimerMenu::onClickReset);
	timerFlow->addWidget(resetButton);

	framesPassed = 0;

	boxLayout->addWidget(timerFlow);
	addWidgetToEditor(boxLayout);
}

void PhysicsTimerMenu::onClickReset(Widget* sender, WidgetEvent action, void* data)
{
	frameElapsedLabel->setText("0");
	framesPassed = 0;
}

void PhysicsTimerMenu::increaseFramePassed()
{
	++framesPassed;
	frameElapsedLabel->setTextPrintC("%d", framesPassed);
}

PhysicsSceneMenu::PhysicsSceneMenu(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Scene", parent)
{
	const int32_t widgetWidth = 150;

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, 200);
	fillBoxLayout(boxLayout);

	FlowLayout* cellWidthFlow = new FlowLayout(0, nullptr, (uint32_t)Margin::LEFT, 0, 0);
	cellWidthFlow->setSize(widgetWidth, 20);
	fillFlowLayout(cellWidthFlow);

	Label* cellWidthLabel = new Label(0, nullptr);
	fillLabel(cellWidthLabel, "Width");
	cellWidthFlow->addWidget(cellWidthLabel);

	cellWidthTextfield = new TextField(1, nullptr);
	fillTextField(cellWidthTextfield);
	cellWidthTextfield->setAcceptOnlyNumbers(true);
	cellWidthTextfield->setAllowDynamicBuffering(true);
	cellWidthTextfield->setFieldValue("%f", 0.0f);
	cellWidthTextfield->setSize(70, 20);
	cellWidthTextfield->setOnDeselectEvent(this, &PhysicsSceneMenu::onCellWidthDeselect);
	cellWidthFlow->addWidget(cellWidthTextfield);

	FlowLayout* cellHeightFlow = new FlowLayout(1, nullptr, (uint32_t)Margin::LEFT, 1, 0);
	cellHeightFlow->setSize(widgetWidth, 20);
	fillFlowLayout(cellHeightFlow);

	Label* cellHeightLabel = new Label(0, nullptr);
	fillLabel(cellHeightLabel, "Height");
	cellHeightFlow->addWidget(cellHeightLabel);

	cellHeightTextfield = new TextField(1, nullptr);
	fillTextField(cellHeightTextfield);
	cellHeightTextfield->setAcceptOnlyNumbers(true);
	cellHeightTextfield->setAllowDynamicBuffering(true);
	cellHeightTextfield->setFieldValue("%f", 0.0f);
	cellHeightTextfield->setSize(70, 20);
	cellHeightTextfield->setOnDeselectEvent(this, &PhysicsSceneMenu::onCellHeightDeselect);
	cellHeightFlow->addWidget(cellHeightTextfield);

	FlowLayout* cellSizeFlow = new FlowLayout(2, nullptr, (uint32_t)Margin::LEFT, 1, 0);
	cellSizeFlow->setSize(widgetWidth, 20);
	fillFlowLayout(cellSizeFlow);

	Label* cellSizeLabel = new Label(0, nullptr);
	fillLabel(cellSizeLabel, "Size");
	cellSizeFlow->addWidget(cellSizeLabel);

	cellSizeTextfield = new TextField(1, nullptr);
	fillTextField(cellSizeTextfield);
	cellSizeTextfield->setAcceptOnlyNumbers(true);
	cellSizeTextfield->setAllowDynamicBuffering(true);
	cellSizeTextfield->setFieldValue("%f", 0.0f);
	cellSizeTextfield->setSize(70, 20);
	cellSizeTextfield->setOnDeselectEvent(this, &PhysicsSceneMenu::onCellSizeDeselect);
	cellSizeFlow->addWidget(cellSizeTextfield);

	FlowLayout* cellFlow = new FlowLayout(3, nullptr, (uint32_t)Margin::LEFT, 1, 0);
	cellFlow->setSize(widgetWidth, 20);
	fillFlowLayout(cellFlow);

	cellRebuildButton = new Button(0, nullptr);
	fillButton(cellRebuildButton, "Rebuild");
	cellRebuildButton->setOnClickEvent(this, &PhysicsSceneMenu::onRebuildClick);
	cellFlow->addWidget(cellRebuildButton);

	cellRefreshButton = new Button(1, nullptr);
	fillButton(cellRefreshButton, "Refresh");
	cellRefreshButton->setOnClickEvent(this, &PhysicsSceneMenu::onRefreshClick);
	cellFlow->addWidget(cellRefreshButton);

	boxLayout->addWidget(cellWidthFlow);
	boxLayout->addWidget(cellHeightFlow);
	boxLayout->addWidget(cellSizeFlow);
	boxLayout->addWidget(cellFlow);

	addWidgetToEditor(boxLayout);
}

void PhysicsSceneMenu::reloadResources()
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	Scene2d*  scene    = physicsManager->getScene();
	HashGrid* hashGrid = (HashGrid*)(scene);

	int32_t cellWidth  = hashGrid->getCellWidth();
	int32_t cellHeight = hashGrid->getCellHeight();
	jaFloat cellSize   = hashGrid->getCellSize();

	cellWidthTextfield->setFieldValue("%d", cellWidth);
	cellHeightTextfield->setFieldValue("%d", cellHeight);
	cellSizeTextfield->setFieldValue("%f", cellSize);
}

void PhysicsSceneMenu::onCellWidthDeselect(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	HashGrid* hashGrid = (HashGrid*)physicsManager->getScene();

	int32_t cellWidth;
	int32_t cellHeight;

	if (false == this->cellWidthTextfield->getAsInt32(&cellWidth))
	{
		this->cellWidthTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellWidth());
	}

	if (false == this->cellHeightTextfield->getAsInt32(&cellHeight))
	{
		this->cellHeightTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellHeight());
	}

	int32_t cellsNum = cellWidth * cellHeight;

	if (cellsNum > setup::gilgamesh::MAX_BUCKET_NUM)
	{
		this->cellWidthTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellWidth());
	}
}

void PhysicsSceneMenu::onCellHeightDeselect(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	HashGrid* hashGrid = (HashGrid*)physicsManager->getScene();

	int32_t cellWidth;
	int32_t cellHeight;

	if (false == this->cellWidthTextfield->getAsInt32(&cellWidth))
	{
		this->cellWidthTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellWidth());
	}

	if (false == this->cellHeightTextfield->getAsInt32(&cellHeight))
	{
		this->cellHeightTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellHeight());
	}

	int32_t cellsNum = cellWidth * cellHeight;

	if (cellsNum > setup::gilgamesh::MAX_BUCKET_NUM)
	{
		this->cellWidthTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellWidth());
	}
}

void PhysicsSceneMenu::onCellSizeDeselect(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	HashGrid* hashGrid = (HashGrid*)physicsManager->getScene();

	float cellSize;

	if (false == this->cellSizeTextfield->getAsFloat(&cellSize))
	{
		this->cellSizeTextfield->setFieldValue("%f", hashGrid->getCellSize());
	}

	if (cellSize <= 0.0f)
	{
		this->cellSizeTextfield->setFieldValue("%f", hashGrid->getCellSize());
	}
}

void PhysicsSceneMenu::onRefreshClick(Widget* sender, WidgetEvent action, void* data)
{
	reloadResources();
}

void PhysicsSceneMenu::onRebuildClick(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	Scene2d* scene = physicsManager->getScene();
	HashGrid* hashGrid = (HashGrid*)(scene);

	int16_t cellWidth;
	int16_t cellHeight;
	jaFloat cellSize;

	if (!cellWidthTextfield->getAsInt16(&cellWidth)) {
		cellWidth = hashGrid->getCellWidth();
	}

	if (!cellHeightTextfield->getAsInt16(&cellHeight)) {
		cellHeight = hashGrid->getCellHeight();
	}

	if (!cellSizeTextfield->getAsFloat(&cellSize)) {
		cellSize = hashGrid->getCellSize();
	}

	hashGrid->rebuild(cellWidth, cellHeight, cellSize);
}

PhysicsComponentMenu::PhysicsComponentMenu( uint32_t widgetId, Widget* parent ) : CommonWidget(widgetId,"Menu",parent)
{
	singleton = this;

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(150,250);
	fillBoxLayout(boxLayout);

	Button* materialButton = new Button((int32_t)TR_PHYSICSEDITOR_ID::MATERIAL_EDITOR, nullptr);
	fillButton(materialButton,"Material");
	materialButton->setOnClickEvent(this, &PhysicsComponentMenu::onMenuClick);
	boxLayout->addWidget(materialButton);

	Button* typeButton = new Button((int32_t)TR_PHYSICSEDITOR_ID::PHYSIC_TYPE_EDITOR, nullptr);
	fillButton(typeButton,"Physic type");
	typeButton->setOnClickEvent(this, &PhysicsComponentMenu::onMenuClick);
	boxLayout->addWidget(typeButton);

	Button* shapeButton = new Button((int32_t)TR_PHYSICSEDITOR_ID::SHAPE_EDITOR, nullptr);
	fillButton(shapeButton,"Shape");
	shapeButton->setOnClickEvent(this, &PhysicsComponentMenu::onMenuClick);
	boxLayout->addWidget(shapeButton);

	Button* velocityButton = new Button((int32_t)TR_PHYSICSEDITOR_ID::VELOCITY_EDITOR, nullptr);
	fillButton(velocityButton, "Velocity");
	velocityButton->setOnClickEvent(this, &PhysicsComponentMenu::onMenuClick);
	boxLayout->addWidget(velocityButton);

	Button* dampingButton = new Button((int32_t)TR_PHYSICSEDITOR_ID::DAMPING_EDITOR, nullptr);
	fillButton(dampingButton, "Damping");
	dampingButton->setOnClickEvent(this, &PhysicsComponentMenu::onMenuClick);
	boxLayout->addWidget(dampingButton);

	Button* linearMotorButton = new Button((int32_t)TR_PHYSICSEDITOR_ID::LINEAR_MOTOR_EDITOR, nullptr);
	fillButton(linearMotorButton,"Linear motor");
	linearMotorButton->setOnClickEvent(this, &PhysicsComponentMenu::onMenuClick);
	boxLayout->addWidget(linearMotorButton);

	Button* distanceJointButton = new Button((int32_t)TR_PHYSICSEDITOR_ID::DISTANCE_JOINT_EDITOR, nullptr);
	fillButton(distanceJointButton, "Distance joint");
	distanceJointButton->setOnClickEvent(this, &PhysicsComponentMenu::onMenuClick);
	boxLayout->addWidget(distanceJointButton);

	Button* collisionMaskButton = new Button((int32_t)TR_PHYSICSEDITOR_ID::COLLISION_MASK_EDITOR, nullptr);
	fillButton(collisionMaskButton, "Collision mask");
	collisionMaskButton->setOnClickEvent(this, &PhysicsComponentMenu::onMenuClick);
	boxLayout->addWidget(collisionMaskButton);

	components[(int32_t)TR_PHYSICSEDITOR_ID::MATERIAL_EDITOR] = new MaterialEditor((uint32_t)TR_PHYSICSEDITOR_ID::MATERIAL_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::MATERIAL_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::PHYSIC_TYPE_EDITOR] = new BodyTypeEditor((uint32_t)TR_PHYSICSEDITOR_ID::PHYSIC_TYPE_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::PHYSIC_TYPE_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::SHAPE_EDITOR] = new ShapeEditor((uint32_t)TR_PHYSICSEDITOR_ID::SHAPE_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::SHAPE_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::VELOCITY_EDITOR] = new VelocityEditor((uint32_t)TR_PHYSICSEDITOR_ID::VELOCITY_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::VELOCITY_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::DAMPING_EDITOR] = new DampingEditor((uint32_t)TR_PHYSICSEDITOR_ID::DAMPING_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::DAMPING_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::LINEAR_MOTOR_EDITOR] = new LinearMotorEditor((uint32_t)TR_PHYSICSEDITOR_ID::LINEAR_MOTOR_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::LINEAR_MOTOR_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::DISTANCE_JOINT_EDITOR] = new DistanceJointEditor((uint32_t)TR_PHYSICSEDITOR_ID::DISTANCE_JOINT_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::DISTANCE_JOINT_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::COLLISION_MASK_EDITOR] = new CollisionMaskEditor((uint32_t)TR_PHYSICSEDITOR_ID::COLLISION_MASK_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::COLLISION_MASK_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::BOX_EDITOR] = new BoxEditor((uint32_t)TR_PHYSICSEDITOR_ID::BOX_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::BOX_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::CIRCLE_EDITOR] = new CircleEditor((uint32_t)TR_PHYSICSEDITOR_ID::CIRCLE_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::CIRCLE_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::CONVEX_POLYGON_EDITOR] = new ConvexPolygonEditor((uint32_t)TR_PHYSICSEDITOR_ID::CONVEX_POLYGON_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::CONVEX_POLYGON_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::TRIANGLE_MESH_EDITOR] = new TriangleMeshEditor((uint32_t)TR_PHYSICSEDITOR_ID::TRIANGLE_MESH_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::TRIANGLE_MESH_EDITOR]);
	components[(int32_t)TR_PHYSICSEDITOR_ID::POINT_EDITOR] = new PointEditor((uint32_t)TR_PHYSICSEDITOR_ID::POINT_EDITOR, this);
	addWidget(components[(int32_t)TR_PHYSICSEDITOR_ID::POINT_EDITOR]);

	addWidgetToEditor(boxLayout);
}

void PhysicsComponentMenu::onMenuClick( Widget* sender, WidgetEvent action, void* data )
{
	uint32_t id = sender->getId();
	components[id]->setActive(true);
	components[id]->setRender(true);
}

void PhysicsComponentMenu::updateComponentEditors( PhysicObject2d* physicObject )
{
	components[(int32_t)TR_PHYSICSEDITOR_ID::MATERIAL_EDITOR]->updateWidget(physicObject);
	components[(int32_t)TR_PHYSICSEDITOR_ID::PHYSIC_TYPE_EDITOR]->updateWidget(physicObject);
	components[(int32_t)TR_PHYSICSEDITOR_ID::SHAPE_EDITOR]->updateWidget(physicObject);
	components[(int32_t)TR_PHYSICSEDITOR_ID::LINEAR_MOTOR_EDITOR]->updateWidget(physicObject);
	components[(int32_t)TR_PHYSICSEDITOR_ID::VELOCITY_EDITOR]->updateWidget(physicObject);
	components[(int32_t)TR_PHYSICSEDITOR_ID::DAMPING_EDITOR]->updateWidget(physicObject);
}

MaterialEditor::MaterialEditor( uint32_t widgetId, Widget* parent ) : CommonWidget(widgetId,"Material",parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 310;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth,(20 + 1) * 3);
	fillBoxLayout(boxLayout);

	////////////////////////////////////
	FlowLayout* restitutionFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 0, 0);
	restitutionFlow->setSize(widgetWidth,20);
	fillFlowLayout(restitutionFlow);

	Label* restitutionLabel = new Label(0,nullptr);
	fillLabel(restitutionLabel,"Restitution");
	restitutionLabel->setSize(80,20);

	restitutionScrollbar = new Scrollbar(1,nullptr);
	fillScrollbar(restitutionScrollbar,0.0,1.0);
	restitutionScrollbar->setHorizontal();
	restitutionScrollbar->setSize(100,20);
	restitutionScrollbar->setValue(0.0);
	restitutionScrollbar->setOnChangeEvent(this,&MaterialEditor::onChangeRestitution);
	restitutionScrollbar->setActive(false);

	restitutionTextField = new TextField(2,nullptr);
	fillTextField(restitutionTextField);
	restitutionTextField->setAcceptOnlyNumbers(true);
	restitutionTextField->setAllowDynamicBuffering(true);
	restitutionTextField->setFieldValue("%f",0.0f);
	restitutionTextField->setSize(70,20);
	restitutionTextField->setOnDeselectEvent(this,&MaterialEditor::onChangeRestitution);
	restitutionTextField->setActive(false);

	restitutionFlow->addWidget(restitutionLabel);
	restitutionFlow->addWidget(restitutionScrollbar);
	restitutionFlow->addWidget(restitutionTextField);
	///////////////////////////////////////
	FlowLayout* frictionFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 0, 0);
	frictionFlow->setSize(widgetWidth,20);
	fillFlowLayout(frictionFlow);

	Label* frictionLabel = new Label(0,nullptr);
	fillLabel(frictionLabel,"Friction");
	frictionLabel->setSize(80,20);

	frictionScrollbar = new Scrollbar(1,nullptr);
	fillScrollbar(frictionScrollbar,0.0,1.0);
	frictionScrollbar->setHorizontal();
	frictionScrollbar->setSize(100,20);
	frictionScrollbar->setValue(0.2);
	frictionScrollbar->setOnChangeEvent(this,&MaterialEditor::onChangeFriction);
	frictionScrollbar->setActive(false);

	frictionTextField = new TextField(2,nullptr);
	fillTextField(frictionTextField);
	frictionTextField->setAcceptOnlyNumbers(true);
	frictionTextField->setAllowDynamicBuffering(true);
	frictionTextField->setFieldValue("%f",0.2f);
	frictionTextField->setSize(70,20);
	frictionTextField->setOnDeselectEvent(this,&MaterialEditor::onChangeFriction);
	frictionTextField->setActive(false);

	frictionFlow->addWidget(frictionLabel);
	frictionFlow->addWidget(frictionScrollbar);
	frictionFlow->addWidget(frictionTextField);
	//////////////////////////////////////
	FlowLayout* densityFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 0, 0);
	densityFlow->setSize(widgetWidth,20);
	fillFlowLayout(densityFlow);
	Label* densityLabel = new Label(0,nullptr);
	fillLabel(densityLabel,"Density");
	densityLabel->setSize(80,20);

	densityTextField = new TextField(1,nullptr);
	fillTextField(densityTextField);
	densityTextField->setAcceptOnlyNumbers(true);
	densityTextField->setAllowDynamicBuffering(true);
	densityTextField->setFieldValue("%f",1.0f);
	densityTextField->setSize(70,20);
	densityTextField->setOnDeselectEvent(this, &MaterialEditor::onChangeDensity);
	densityTextField->setActive(false);

	Label* massLabel = new Label(2, nullptr);
	fillLabel(massLabel, "Mass:");
	massLabel->setSize(80, 20);

	massValueLabel = new Label(3, nullptr);
	fillLabel(massValueLabel, "-");
	massValueLabel->setSize(80, 20);

	densityFlow->addWidget(densityLabel);
	densityFlow->addWidget(densityTextField);
	densityFlow->addWidget(massLabel);
	densityFlow->addWidget(massValueLabel);

	boxLayout->addWidget(restitutionFlow);
	boxLayout->addWidget(frictionFlow);
	boxLayout->addWidget(densityFlow);
	addWidgetToEditor(boxLayout);
}

void MaterialEditor::updateWidget( void* data )
{
	PhysicObject2d* physicObject = (PhysicObject2d*)data;

	if (nullptr == physicObject)
	{
		this->restitutionTextField->setText("");
		this->frictionTextField->setText("");
		this->densityTextField->setText("");
		this->massValueLabel->setText("");

		this->restitutionTextField->setActive(false);
		this->frictionTextField->setActive(false);
		this->densityTextField->setActive(false);
		restitutionScrollbar->setActive(false);
		frictionScrollbar->setActive(false);

		return;
	}

	if(physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = static_cast<RigidBody2d*>(physicObject);
	Shape2d*     bodyShape = rigidBody->getShape();

	restitution  = bodyShape->getRestitution();
	friction     = bodyShape->getFriction();
	density      = bodyShape->getDensity();
	jaFloat mass = rigidBody->getMass();

	restitutionScrollbar->setValue(restitution);
	restitutionTextField->setFieldValue("%f", restitution);
	frictionScrollbar->setValue(friction);
	frictionTextField->setFieldValue("%f", friction);
	densityTextField->setFieldValue("%f", density);
	massValueLabel->setTextPrintC("%f", mass);

	this->restitutionTextField->setActive(true);
	this->frictionTextField->setActive(true);
	this->densityTextField->setActive(true);
	restitutionScrollbar->setActive(true);
	frictionScrollbar->setActive(true);
}

void MaterialEditor::onChangeFriction( Widget* sender, WidgetEvent action, void* data )
{
	if(sender == this->frictionScrollbar)
	{
		this->friction = (jaFloat)(this->frictionScrollbar->getValue());
		this->frictionTextField->setFieldValue("%f",this->friction);
	}
	
	if(sender == frictionTextField)
	{
		float frictionVal = 0.0f;
		if (this->frictionTextField->getAsFloat(&frictionVal))
		{
			this->frictionScrollbar->setValue(frictionVal);
			this->friction = frictionVal;
		}
		else
		{
			this->frictionTextField->setFieldValue("%f", this->friction);
		}		
	}
	
	PhysicsEditor::getSingleton()->applyFilterToSelection(changeFrictionFilter);
}

void MaterialEditor::onChangeRestitution( Widget* sender, WidgetEvent action, void* data )
{
	if(sender == this->restitutionScrollbar)
	{
		this->restitution = (jaFloat)(this->restitutionScrollbar->getValue());
		this->restitutionTextField->setFieldValue("%f",this->restitution);
	}
	
	if(sender == this->restitutionTextField)
	{
		float restitutionVal = 0.0f;
		if (this->restitutionTextField->getAsFloat(&restitutionVal))
		{
			this->restitutionScrollbar->setValue(restitutionVal);
			this->restitution = restitutionVal;
		}
		else
		{
			this->restitutionTextField->setFieldValue("%f", this->restitution);
		}
	}
	
	PhysicsEditor::getSingleton()->applyFilterToSelection(changeResitutionFilter);
}

void MaterialEditor::onChangeDensity(Widget* sender, WidgetEvent action, void* data)
{
	if (sender == this->densityTextField)
	{
		float densityVal = 1.0f;

		if (false == this->densityTextField->getAsFloat(&densityVal))
		{
			this->densityTextField->setFieldValue("%f", this->density);
		}
		else
		{
			this->density = densityVal;
		}
	}

	PhysicsEditor::getSingleton()->applyFilterToSelection(changeDensityFilter);
	
	if (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 0)
	{
		PhysicObject2d* physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		updateWidget(physicObject);
		ShapeEditor::getSingleton()->updateWidget(physicObject);
	}
}

void MaterialEditor::changeFrictionFilter( PhysicObject2d* physicObject )
{
	if(physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = (RigidBody2d*)(physicObject);
	Shape2d*     bodyShape = rigidBody->getShape();

	bodyShape->setFriction(singleton->friction);
}

void MaterialEditor::changeResitutionFilter( PhysicObject2d* physicObject )
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = static_cast<RigidBody2d*>(physicObject);
	Shape2d*     bodyShape = rigidBody->getShape();
	
	bodyShape->setRestitution(singleton->restitution);
}

void MaterialEditor::changeDensityFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = static_cast<RigidBody2d*>(physicObject);
	Shape2d*     bodyShape = rigidBody->getShape();

	bodyShape->setDensity(singleton->density);
	rigidBody->resetMassData();
}

BodyTypeEditor::BodyTypeEditor( uint32_t widgetId, Widget* parent ) : CommonWidget(widgetId,"Physic Type",parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 300;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, 2 * (20 + 1));
	fillBoxLayout(boxLayout); 

	FlowLayout* typeLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 1, 0);
	typeLayout->setSize(widgetWidth,20);
	fillFlowLayout(typeLayout);

	this->staticButton = new RadioButton(0,nullptr);
	this->staticButton->setOnClickEvent(this,&BodyTypeEditor::onClickBodyType);
	this->dynamicButton = new RadioButton(1,nullptr);
	this->dynamicButton->setOnClickEvent(this, &BodyTypeEditor::onClickBodyType);
	this->kinematicButton = new RadioButton(2,nullptr);
	this->kinematicButton->setOnClickEvent(this, &BodyTypeEditor::onClickBodyType);

	this->staticButton->setActive(false);
	this->dynamicButton->setActive(false);
	this->kinematicButton->setActive(false);

	fillRadioButton(this->staticButton, "Static");
	fillRadioButton(this->dynamicButton, "Dynamic");
	fillRadioButton(this->kinematicButton, "Kinematic");

	ButtonGroup* buttonGroup = new ButtonGroup(6,nullptr);
	buttonGroup->addButton(this->staticButton);
	buttonGroup->addButton(this->dynamicButton);
	buttonGroup->addButton(this->kinematicButton);

	Label* staticLabel = new Label(0,nullptr);
	Label* dynamicLabel = new Label(0,nullptr);
	Label* kinematicLabel = new Label(0,nullptr);
	fillLabel(staticLabel, "Static");
	fillLabel(dynamicLabel, "Dynamic");
	fillLabel(kinematicLabel, "Kinematic");

	typeLayout->addWidget(buttonGroup);
	typeLayout->addWidget(staticLabel);
	typeLayout->addWidget(this->staticButton);
	typeLayout->addWidget(dynamicLabel);
	typeLayout->addWidget(this->dynamicButton);
	typeLayout->addWidget(kinematicLabel);
	typeLayout->addWidget(this->kinematicButton);

	FlowLayout* sleepLayout = new FlowLayout(1, nullptr, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 1, 0);
	sleepLayout->setSize(widgetWidth, 20);
	fillFlowLayout(sleepLayout);
	Label* sleepLabel = new Label(0, nullptr);
	fillLabel(sleepLabel, "Sleep");
	this->isSleepingCheckbox = new Checkbox(1, nullptr);
	fillCheckbox(this->isSleepingCheckbox);
	this->isSleepingCheckbox->setCheckWithoutEvent(false);
	this->isSleepingCheckbox->setOnChangeEvent(this, &BodyTypeEditor::onChangeSleep);
	this->isSleepingCheckbox->setActive(false);
	sleepLayout->addWidget(sleepLabel);
	sleepLayout->addWidget(this->isSleepingCheckbox);
	Label* allowSleepLabel = new Label(2, nullptr);
	fillLabel(allowSleepLabel, "Allow Sleep");
	this->isSleepingAllowedCheckbox = new Checkbox(3, nullptr);
	fillCheckbox(this->isSleepingAllowedCheckbox);
	this->isSleepingAllowedCheckbox->setCheckWithoutEvent(false);
	this->isSleepingAllowedCheckbox->setOnChangeEvent(this, &BodyTypeEditor::onChangeAllowSleep);
	this->isSleepingAllowedCheckbox->setActive(false);
	sleepLayout->addWidget(allowSleepLabel);
	sleepLayout->addWidget(this->isSleepingAllowedCheckbox);

	bodyType = BodyType::STATIC_BODY;

	boxLayout->addWidget(typeLayout);
	boxLayout->addWidget(sleepLayout);
	addWidgetToEditor(boxLayout);
}

void BodyTypeEditor::onClickBodyType( Widget* sender, WidgetEvent action, void* data )
{
	switch(sender->getId())
	{
	case 0:
		bodyType = BodyType::STATIC_BODY;
		break;
	case 1:
		bodyType = BodyType::DYNAMIC_BODY;
		break;
	case 2:
		bodyType = BodyType::KINEMATIC_BODY;
		break;
	}

	PhysicsEditor::getSingleton()->applyFilterToSelection(bodyTypeFilter);

	if (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 0)
	{
		PhysicObject2d* physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		ShapeEditor::getSingleton()->updateWidget(physicObject);
		MaterialEditor::getSingleton()->updateWidget(physicObject);
	}
}

void BodyTypeEditor::onChangeSleep(Widget* sender, WidgetEvent action, void* data)
{
	isSleeping = isSleepingCheckbox->isChecked();
	PhysicsEditor::getSingleton()->applyFilterToSelection(sleepFilter);
}

void BodyTypeEditor::onChangeAllowSleep(Widget* sender, WidgetEvent action, void* data)
{
	isSleepingAllowed = isSleepingAllowedCheckbox->isChecked();
	PhysicsEditor::getSingleton()->applyFilterToSelection(allowSleepFilter);
}

void BodyTypeEditor::updateWidget(void* data)
{
	PhysicObject2d* physicObject = (PhysicObject2d*)(data);

	if (nullptr == physicObject)
	{
		this->staticButton->setActive(false);
		this->dynamicButton->setActive(false);
		this->kinematicButton->setActive(false);
		this->isSleepingCheckbox->setActive(false);
		this->isSleepingAllowedCheckbox->setActive(false);

		return;
	}

	this->staticButton->setActive(true);
	this->dynamicButton->setActive(true);
	this->kinematicButton->setActive(true);
	this->isSleepingCheckbox->setActive(true);
	this->isSleepingAllowedCheckbox->setActive(true);

	switch (physicObject->getBodyType())
	{
	case (uint8_t)BodyType::STATIC_BODY:
		staticButton->setCheck(true);
		break;
	case (uint8_t)BodyType::DYNAMIC_BODY:
		dynamicButton->setCheck(true);
		break;
	case (uint8_t)BodyType::KINEMATIC_BODY:
		kinematicButton->setCheck(true);
		break;
	}

	this->bodyType = (BodyType)(physicObject->getBodyType());
	this->isSleeping = physicObject->isSleeping();
	this->isSleepingAllowed = physicObject->isSleepingAllowed();
	this->isSleepingCheckbox->setCheckWithoutEvent(isSleeping);
	this->isSleepingAllowedCheckbox->setCheckWithoutEvent(isSleepingAllowed);
}

void BodyTypeEditor::bodyTypeFilter(PhysicObject2d* physicObject)
{
	PhysicsManager::getSingleton()->changeBodyType(physicObject, singleton->bodyType);
}

void BodyTypeEditor::sleepFilter(PhysicObject2d* physicObject)
{
	if (singleton->isSleeping)
		PhysicsManager::getSingleton()->makePhysicObjectSleep(physicObject);
	else
		PhysicsManager::getSingleton()->wakeUpPhysicObject(physicObject);
}

void BodyTypeEditor::allowSleepFilter(PhysicObject2d* physicObject)
{
	physicObject->setAllowSleep(singleton->isSleepingAllowed);
}

BoxEditor::BoxEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Box", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 330;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, 1 * (20 + 1));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* boxSizeLayout = new FlowLayout(5, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		boxSizeLayout->setSize(widgetWidth, 20);
		fillFlowLayout(boxSizeLayout);
		Label* widthLabel = new Label(0, nullptr);
		fillLabel(widthLabel, "Width");
		this->widthTextfield = new TextField(1, nullptr);
		fillTextField(this->widthTextfield);
		this->widthTextfield->setAcceptOnlyNumbers(true);
		this->widthTextfield->setTextSize(12, 20);
		this->widthTextfield->setOnDeselectEvent(this, &BoxEditor::onWidthDeselect);
		Label* heightLabel = new Label(2, nullptr);
		fillLabel(heightLabel, "Height");
		this->heightTextfield = new TextField(3, nullptr);
		fillTextField(this->heightTextfield);
		this->heightTextfield->setAcceptOnlyNumbers(true);
		this->heightTextfield->setTextSize(12, 20);
		this->heightTextfield->setOnDeselectEvent(this, &BoxEditor::onHeightDeselect);
		this->createButton = new Button(4, nullptr);
		fillButton(this->createButton, "Create");
		this->createButton->setOnClickEvent(this, &BoxEditor::onCreateClick);

		boxSizeLayout->addWidget(widthLabel);
		boxSizeLayout->addWidget(this->widthTextfield);
		boxSizeLayout->addWidget(heightLabel);
		boxSizeLayout->addWidget(this->heightTextfield);
		boxSizeLayout->addWidget(this->createButton);
		boxLayout->addWidget(boxSizeLayout);
	}

	this->width  = 1.0f;
	this->height = 1.0f;

	addWidgetToEditor(boxLayout);
}

void BoxEditor::updateWidget(void* data)
{
	PhysicObject2d*  physicObject = (PhysicObject2d*)(data);

	if (nullptr == physicObject)
		return;

	ObjectHandle2d*  objectHandle = physicObject->getShapeHandle();
	Shape2d* shape = objectHandle->getShape();

	if (shape->getType() == (uint8_t)ShapeType::Box) {
		Box2d* box = (Box2d*)(shape);
		widthTextfield->setFieldValue("%f", box->getWidth());
		heightTextfield->setFieldValue("%f", box->getHeight());
	}
}

void BoxEditor::onWidthDeselect(Widget* sender, WidgetEvent action, void* data)
{
	float widthVal = 0.0f;
	if (false == widthTextfield->getAsFloat(&widthVal))
	{
		this->widthTextfield->setFieldValue("%f", this->width);
	}
	else
	{
		this->width = widthVal;
	}

	PhysicsEditor::getSingleton()->applyFilterToSelection(widthFilter);

	if (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 0)
	{
		PhysicObject2d* physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		ShapeEditor::getSingleton()->updateWidget(physicObject);
		MaterialEditor::getSingleton()->updateWidget(physicObject);
	}
}

void BoxEditor::onHeightDeselect(Widget* sender, WidgetEvent action, void* data)
{
	float heightVal = 0.0f;
	if (false == heightTextfield->getAsFloat(&heightVal))
	{
		this->heightTextfield->setFieldValue("%f", this->height);
	}
	else
	{
		this->height = heightVal;
	}
	
	PhysicsEditor::getSingleton()->applyFilterToSelection(heightFilter);

	if (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 0)
	{
		PhysicObject2d* physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		ShapeEditor::getSingleton()->updateWidget(physicObject);
		MaterialEditor::getSingleton()->updateWidget(physicObject);
	}
}

void BoxEditor::onCreateClick(Widget* sender, WidgetEvent action, void* data)
{
	ja::PhysicObjectDef2d* physicObjectDef = &PhysicsManager::getSingleton()->physicObjectDef;

	physicObjectDef->box.setSizeX(this->width);
	physicObjectDef->box.setSizeY(this->height);

	physicObjectDef->setShapeType((int8_t)ShapeType::Box);

	PhysicsEditor* physicsEditor = PhysicsEditor::getSingleton();
	physicObjectDef->transform.position = physicsEditor->getGizmoPosition();

	PhysicsManager::getSingleton()->createBodyDef();
}

void BoxEditor::widthFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = (RigidBody2d*)(physicObject);
	Shape2d* shape = rigidBody->getShape();
	if (shape->getType() != (uint8_t)ShapeType::Box)
		return;

	Box2d* box = (Box2d*)(shape);
	box->setWidth(singleton->width);
	PhysicsManager::getSingleton()->updatePhysicObjectImmediate(rigidBody);

	rigidBody->resetMassData();
}

void BoxEditor::heightFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = (RigidBody2d*)(physicObject);
	Shape2d* shape = rigidBody->getShape();
	if (shape->getType() != (uint8_t)ShapeType::Box)
		return;

	Box2d* box = (Box2d*)(shape);
	box->setHeight(singleton->height);
	PhysicsManager::getSingleton()->updatePhysicObjectImmediate(rigidBody);

	rigidBody->resetMassData();
}

CircleEditor::CircleEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Circle", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 250;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, 1 * (20 + 1));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* circleSizeLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		circleSizeLayout->setSize(300, 20);
		fillFlowLayout(circleSizeLayout);
		Label* radiusLabel = new Label(0, nullptr);
		fillLabel(radiusLabel, "Radius");
		this->radiusTextfield = new TextField(1, nullptr);
		fillTextField(this->radiusTextfield);
		this->radiusTextfield->setAcceptOnlyNumbers(true);
		this->radiusTextfield->setTextSize(12, 20);
		this->radiusTextfield->setOnDeselectEvent(this, &CircleEditor::onRadiusDeselect);
		this->createButton = new Button(2, nullptr);
		fillButton(this->createButton, "Create");
		this->createButton->setOnClickEvent(this, &CircleEditor::onCreateClick);

		circleSizeLayout->addWidget(radiusLabel);
		circleSizeLayout->addWidget(radiusTextfield);
		circleSizeLayout->addWidget(this->createButton);
		boxLayout->addWidget(circleSizeLayout);
	}

	this->radius = 1.0f;

	addWidgetToEditor(boxLayout);
}

void CircleEditor::updateWidget(void* data)
{
	PhysicObject2d* physicObject = (PhysicObject2d*)(data);
	
	if (nullptr == physicObject)
		return;
	
	ObjectHandle2d* objectHandle = physicObject->getShapeHandle();
	Shape2d* shape = objectHandle->getShape();

	if (shape->getType() == (uint8_t)ShapeType::Circle) {
		Circle2d* circle = (Circle2d*)(shape);
		radiusTextfield->setFieldValue("%f", circle->getRadius());
	}
}

void CircleEditor::onRadiusDeselect(Widget* sender, WidgetEvent action, void* data)
{
	float radiusVal = 0.0f;
	if (false == this->radiusTextfield->getAsFloat(&radiusVal))
	{
		this->radiusTextfield->setFieldValue("%f", this->radius);
	}
	else
	{
		this->radius = radiusVal;
	}

	PhysicsEditor::getSingleton()->applyFilterToSelection(radiusFilter);

	if (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 0)
	{
		PhysicObject2d* physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		ShapeEditor::getSingleton()->updateWidget(physicObject);
		MaterialEditor::getSingleton()->updateWidget(physicObject);
	}
}

void CircleEditor::onCreateClick(Widget* sender, WidgetEvent action, void* data)
{
	ja::PhysicObjectDef2d* physicObjectDef = &PhysicsManager::getSingleton()->physicObjectDef;

	physicObjectDef->circle.setSizeX(this->radius);
	physicObjectDef->setShapeType((int8_t)ShapeType::Circle);

	PhysicsEditor* physicsEditor = PhysicsEditor::getSingleton();
	physicObjectDef->transform.position = physicsEditor->getGizmoPosition();

	PhysicsManager::getSingleton()->createBodyDef();
}

void CircleEditor::radiusFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = (RigidBody2d*)(physicObject);
	Shape2d* shape = rigidBody->getShape();
	if (shape->getType() != (uint8_t)ShapeType::Circle)
		return;

	Circle2d* circle = (Circle2d*)(shape);
	circle->radius = singleton->radius;
	PhysicsManager::getSingleton()->updatePhysicObjectImmediate(rigidBody);

	rigidBody->resetMassData();
}

ConvexPolygonEditor::ConvexPolygonEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Convex Polygon", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 300;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, 3 * (20 + 1));

	DisplayManager* display = DisplayManager::getSingleton();
	infoPosX = (float)(display->getResolutionWidth(0.3f));
	infoPosY = (float)(display->getResolutionHeight(0.3f));
	fillBoxLayout(boxLayout);

	this->verticesAllocator = new PoolAllocator<ListElement<jaVector2>>(16*sizeof(ListElement<jaVector2>), 16);
	this->verticesAllocator->clear();
	this->firstVertex = nullptr;

	{
		FlowLayout* convexSizeLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		convexSizeLayout->setSize(widgetWidth, 1 * (20 + 1));
		fillFlowLayout(convexSizeLayout);

		Label* verticesLabel = new Label(0, nullptr);
		fillLabel(verticesLabel, "Vertices: ");
		convexSizeLayout->addWidget(verticesLabel);

		verticesNumLabel = new Label(1, nullptr);
		fillLabel(verticesNumLabel, "0");
		convexSizeLayout->addWidget(verticesNumLabel);

		boxLayout->addWidget(convexSizeLayout);
	}

	{
		FlowLayout* setAsLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		setAsLayout->setSize(widgetWidth, 1 * (20 + 1));
		fillFlowLayout(setAsLayout);

		Label* setAsLabel = new Label(0,nullptr);
		fillLabel(setAsLabel, "Set as: ");
		setAsLayout->addWidget(setAsLabel);

		setAsTriangleButton = new Button(1, nullptr);
		fillButton(setAsTriangleButton, "Triangle");
		setAsTriangleButton->setOnClickEvent(this, &ConvexPolygonEditor::setAsTriangleClick);
		setAsLayout->addWidget(setAsTriangleButton);
		setAsTriangleButton->setActive(false);

		setAsBoxButton = new Button(2, nullptr);
		fillButton(setAsBoxButton, "Box");
		setAsBoxButton->setOnClickEvent(this, &ConvexPolygonEditor::setAsBoxClick);
		setAsLayout->addWidget(setAsBoxButton);
		setAsBoxButton->setActive(false);

		boxLayout->addWidget(setAsLayout);
	}

	{
		FlowLayout* convexEditLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		convexEditLayout->setSize(widgetWidth, 1 * (20 + 1));
		fillFlowLayout(convexEditLayout);

		editModeButton = new Button(0, nullptr);
		fillButton(editModeButton, "Edit");
		editModeButton->setOnClickEvent(this, &ConvexPolygonEditor::editModeClick);
		convexEditLayout->addWidget(editModeButton);

		boxLayout->addWidget(convexEditLayout);
	}


	addWidgetToEditor(boxLayout);
}

ConvexPolygonEditor::~ConvexPolygonEditor()
{
	this->verticesAllocator->clear();
	delete this->verticesAllocator;
}

void ConvexPolygonEditor::render()
{
	if (this->editedPhysicObject == nullptr)
		return;

	DisplayManager::setColorUb(255, 255, 255, 255);
	DisplayManager::setLineWidth(2.0f);

	ListElement<jaVector2>* itVertex = firstVertex;

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		ListElement<jaVector2>* nextVertex = itVertex->nextElement;
		DisplayManager::drawLine(itVertex->data.x, itVertex->data.y, nextVertex->data.x, nextVertex->data.y);
		itVertex = itVertex->nextElement;
	}

	DisplayManager::setColorUb(255, 0, 0, 255);
	DisplayManager::setPointSize(7);
	itVertex = firstVertex;
	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		DisplayManager::drawPoint(itVertex->data.x, itVertex->data.y);
		itVertex = itVertex->nextElement;
	}

	DisplayManager::setColorUb(255, 255, 255, 255);
	DisplayManager::setPointSize(7);
	if (this->editedVertex != nullptr)
	{
		DisplayManager::drawPoint(editedVertex->data.x, editedVertex->data.y);
	}

	DisplayManager::setColorUb(0, 255, 0, 255);
	DisplayManager::setPointSize(3);
	if (this->nearestVertex != nullptr)
	{
		DisplayManager::drawPoint(nearestVertex->data.x, nearestVertex->data.y);
	}

	{
		DisplayManager::setColorUb(0, 255, 0, 255);
		DisplayManager::setPointSize(7);
		DisplayManager::drawPoint(castedPoint.x, castedPoint.y);
	}
}

ListElement<jaVector2>* ConvexPolygonEditor::findNearestVertex(jaVector2& point)
{
	ListElement<jaVector2>* itVertex      = firstVertex;
	ListElement<jaVector2>* closestVertex = firstVertex;

	float closestDistance = FLT_MAX;

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		jaVector2 vertexDiff = itVertex->data - point;
		float     lengthDiff = jaVectormath2::length(vertexDiff);

		if (lengthDiff < closestDistance)
		{
			closestDistance = lengthDiff;
			closestVertex   = itVertex;
		}

		itVertex = itVertex->nextElement;
	}

	return closestVertex;
}

jaVector2 ConvexPolygonEditor::findCastedPoint(const jaVector2& point, ListElement<jaVector2>* closestVertex, ListElement<jaVector2>** insertVertexIt)
{
	*insertVertexIt        = firstVertex;
	jaVector2 diffPosition = point - closestVertex->data;

	jaVector2 edge0 = closestVertex->data - closestVertex->nextElement->data;
	jaVector2 edge1 = closestVertex->previousElement->data - closestVertex->data;

	jaFloat edge0Len = edge0.normalize();
	jaFloat edge1Len = edge1.normalize();

	jaVector2 pEdge0 = jaVectormath2::perpendicular(edge0);
	jaVector2 pEdge1 = jaVectormath2::perpendicular(edge1);

	jaFloat projPEdge0 = jaVectormath2::projection(pEdge0, diffPosition);
	jaFloat projPEdge1 = jaVectormath2::projection(pEdge1, diffPosition);

	jaFloat projSign = projPEdge0 * projPEdge1;

	jaVector2 pointCasted;

	{
		jaVector2 castedEdge;
		jaFloat   castedEdgeLen;

		if (projPEdge0 > projPEdge1)
		{
			castedEdge      = -edge0;
			castedEdgeLen   = edge0Len;
			*insertVertexIt = closestVertex->nextElement;
		}
		else
		{
			castedEdge      = edge1;
			castedEdgeLen   = edge1Len;
			*insertVertexIt = closestVertex;
		}

		jaFloat projEdge = jaVectormath2::projection(castedEdge, diffPosition);

		if (projEdge < 0.0f)
		{
			projEdge = 0.0f;
		}
		else if (projEdge > castedEdgeLen)
		{
			projEdge = castedEdgeLen;
		}

		pointCasted  = projEdge * castedEdge;
		pointCasted += closestVertex->data;
	}

	return pointCasted;
}

void ConvexPolygonEditor::handleInput()
{
	if (InputManager::isKeyPressed(JA_ESCAPE))
	{
		PhysicsEditor::getSingleton()->setEditorMode(PhysicsEditorMode::SELECT_MODE);
	}

	if (InputManager::isKeyPressed(JA_RETURN))
	{
		setShapeInfo(editedPhysicObject);
		PhysicsEditor::getSingleton()->setEditorMode(PhysicsEditorMode::SELECT_MODE);
	}

	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
	ListElement<jaVector2>* insertVertexIt = firstVertex;

	//Find nearest vertex
	{
		this->nearestVertex = findNearestVertex(drawPoint);
		this->castedPoint   = findCastedPoint(drawPoint, this->nearestVertex, &insertVertexIt);
	}

	if (!UIManager::isMouseOverGui())
	{
		if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
		{
			float zoomFactor = 1.0f / ja::GameManager::getSingleton()->getActiveCamera()->scale.x;
			float anchorCircle = zoomFactor * (7.0f / (float)ja::setup::graphics::PIXELS_PER_UNIT);
	
			ListElement<jaVector2>* itVertex = firstVertex;
			this->editedVertex = nullptr;

			for (uint32_t i = 0; i < verticesNum; ++i)
			{
				jaVector2 vertexDiff = itVertex->data - drawPoint;
				float     lengthDiff = jaVectormath2::length(vertexDiff);

				if (lengthDiff < anchorCircle)
				{
					this->editedVertex = itVertex;
				}

				itVertex = itVertex->nextElement;
			}
		}

		if (ja::InputManager::isMouseButtonPress((uint8_t)MouseButton::BUTTON_LEFT))
		{
			if (this->editedVertex != nullptr)
			{
				this->editedVertex->data = drawPoint;
			}
		}

		if (InputManager::isKeyPressed(JA_c))
		{
			ListElement<jaVector2>* newVertex = this->verticesAllocator->allocate();
			newVertex->data = castedPoint;
			insertVertexIt->pushBefore(newVertex);

			++verticesNum;
			updateVerticesNumLabel();
		}

		if (InputManager::isKeyPressed(JA_d))
		{
			if (this->editedVertex == nullptr)
			{
				Toolset::getSingleton()->windowHud->addInfoC((uint16_t)PhysicsEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "First select vertex");
			}
			else
			{
				if (verticesNum <= 3)
				{
					Toolset::getSingleton()->windowHud->addInfoC((uint16_t)PhysicsEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "You can't delete vertex of polygon with 3 vertices");
				}
				else
				{
					if (firstVertex == editedVertex)
						firstVertex = firstVertex->previousElement;
	
					reconnectVertices(editedVertex->previousElement, editedVertex->nextElement);		

					verticesAllocator->free(editedVertex);
					this->editedVertex = nullptr;

					--verticesNum;
				}
			}

			updateVerticesNumLabel();
		}
	}
}

void ConvexPolygonEditor::setActiveMode(bool state)
{
	if (state)
	{
		editModeButton->setColor(0, 255, 0);
		PhysicsEditor::getSingleton()->hud.setFieldValue((uint32_t)PhysicsEditorHud::MODE_HUD, "Mode=Convex polygon editor");
		PhysicsEditor::getSingleton()->hud.setFieldValue((uint32_t)PhysicsEditorHud::HELPER_HUD, "LMB - Select vertex\nd - Delete vertex\nc - Create vertex\nESC - Cancel");
	}
	else
	{
		editModeButton->setColor(255, 0, 0);
	}
}

void ConvexPolygonEditor::updateWidget(void* data)
{
	PhysicObject2d* physicObject = static_cast<PhysicObject2d*>(data);

	if (nullptr == physicObject)
		return;

	ObjectHandle2d* objectHandle = physicObject->getShapeHandle();
	Shape2d*        shape        = objectHandle->getShape();

	if (shape->getType() == (uint8_t)ShapeType::ConvexPolygon) {
		ConvexPolygon2d* convexPolygon = (ConvexPolygon2d*)(shape);
		uint32_t verticesNum = convexPolygon->getVerticesNum();
		verticesNumLabel->setTextPrintC("%u", verticesNum);
	}

}

void ConvexPolygonEditor::editModeClick(Widget* sender, WidgetEvent action, void* data)
{
	CommonWidget* pCommonWidget = PhysicsEditor::getSingleton()->getCurrentEditor();
	if (nullptr != pCommonWidget)
	{
		if (pCommonWidget == this)
			return;
	}

	{
		setAsBoxButton->setActive(true);
		setAsTriangleButton->setActive(true);
	}

	PhysicsManager* physicManager = PhysicsManager::getSingleton();
	std::list<PhysicObject2d*>& selectedPhysicsObjects = PhysicsEditor::getSingleton()->selectedPhysicObjects;

	if (selectedPhysicsObjects.size() == 0)
	{
		Toolset::getSingleton()->windowHud->addInfoC((uint16_t)PhysicsEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "Select physic bodies first");
		return;
	}

	if (selectedPhysicsObjects.size() > 1)
	{
		Toolset::getSingleton()->windowHud->addInfoC((uint16_t)PhysicsEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "You can edit only one convex shape body at once");
		return;
	}

	editedPhysicObject          = *selectedPhysicsObjects.begin();
	Shape2d* convexPolygonShape = editedPhysicObject->getShape();

	if (convexPolygonShape->getType() != (uint8_t)ShapeType::ConvexPolygon)
	{
		Toolset::getSingleton()->windowHud->addInfoC((uint16_t)PhysicsEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "You need to select convex shape body");
		return;
	}

	getShapeInfo(editedPhysicObject);

	PhysicsEditor::getSingleton()->setEditorMode(this);
}

void ConvexPolygonEditor::setAsTriangleClick(Widget* sender, WidgetEvent action, void* data)
{
	jaVector2 vertices[3] = { jaVector2(0.0f, 0.0f), jaVector2(2.0f, 0.0f), jaVector2(2.0f, 2.0f) };
	jaVector2 shapeCenter;
	ConvexPolygon2d::getShapeCenter(vertices, 3, shapeCenter);
		
	ConvexPolygon2d* convexShape = (ConvexPolygon2d*)this->editedPhysicObject->getShape();
	jaTransform2     transform   = this->editedPhysicObject->getShapeHandle()->transform;

	this->verticesNum = 3;

	this->verticesAllocator->clear();
	firstVertex = new (verticesAllocator->allocate()) ListElement<jaVector2>();
	firstVertex->data = transform * (vertices[0] - shapeCenter);

	ListElement<jaVector2>* itVertex = firstVertex;

	for (uint32_t i = 1; i < verticesNum; ++i)
	{
		ListElement<jaVector2>* newElement = new (verticesAllocator->allocate()) ListElement<jaVector2>();

		newElement->data = transform * (vertices[i] - shapeCenter);

		itVertex->pushAfter(newElement);
		itVertex = newElement;
	}

	itVertex->nextElement        = firstVertex;
	firstVertex->previousElement = itVertex;

	this->editedVertex = nullptr;
	this->nearestVertex = nullptr;
}

void ConvexPolygonEditor::setAsBoxClick(Widget* sender, WidgetEvent action, void* data)
{
	jaVector2 vertices[4] = { jaVector2(-1.0f, -1.0f), jaVector2(1.0f, -1.0f), jaVector2(1.0f, 1.0f), jaVector2(-1.0f, 1.0f) };
	jaVector2 shapeCenter;
	ConvexPolygon2d::getShapeCenter(vertices, 4, shapeCenter);

	ConvexPolygon2d* convexShape = (ConvexPolygon2d*)this->editedPhysicObject->getShape();
	jaTransform2     transform = this->editedPhysicObject->getShapeHandle()->transform;

	this->verticesNum = 4;

	this->verticesAllocator->clear();
	firstVertex = new (verticesAllocator->allocate()) ListElement<jaVector2>();
	firstVertex->data = transform * (vertices[0] - shapeCenter);

	ListElement<jaVector2>* itVertex = firstVertex;

	for (uint32_t i = 1; i < verticesNum; ++i)
	{
		ListElement<jaVector2>* newElement = new (verticesAllocator->allocate()) ListElement<jaVector2>();

		newElement->data = transform * (vertices[i] - shapeCenter);

		itVertex->pushAfter(newElement);
		itVertex = newElement;
	}

	itVertex->nextElement = firstVertex;
	firstVertex->previousElement = itVertex;

	this->editedVertex = nullptr;
	this->nearestVertex = nullptr;
}

void ConvexPolygonEditor::updateVerticesNumLabel()
{
	this->verticesNumLabel->setTextPrintC("%u", verticesNum);
}

void ConvexPolygonEditor::getShapeInfo(PhysicObject2d* physicObject)
{
	ConvexPolygon2d* convexShape = (ConvexPolygon2d*)physicObject->getShape();
	jaTransform2     transform   = physicObject->getShapeHandle()->transform;

	this->verticesNum = convexShape->getVerticesNum();

	this->verticesAllocator->clear();
	firstVertex = new (verticesAllocator->allocate()) ListElement<jaVector2>();
	convexShape->getVertex(0, firstVertex->data);
	firstVertex->data = transform * firstVertex->data;

	ListElement<jaVector2>* itVertex = firstVertex;
	
	for (uint32_t i = 1; i < verticesNum; ++i)
	{
		ListElement<jaVector2>* newElement = new (verticesAllocator->allocate()) ListElement<jaVector2>();

		convexShape->getVertex(i, newElement->data);
		newElement->data = transform * newElement->data;
		
		itVertex->pushAfter(newElement);
		itVertex = newElement;
	}

	itVertex->nextElement        = firstVertex;
	firstVertex->previousElement = itVertex;

	this->editedVertex  = nullptr;
	this->nearestVertex = nullptr;
}

void ConvexPolygonEditor::setShapeInfo(PhysicObject2d* physicObject)
{
	//Check if convex or concave
	bool isConcave = false;

	{
		ListElement<jaVector2>* itVertex = firstVertex;

		for (uint32_t i = 0; i < verticesNum; ++i)
		{
			jaVector2 edge0 = itVertex->data - itVertex->previousElement->data;
			jaVector2 edge1 = itVertex->nextElement->data - itVertex->data;
			edge0.normalize();
			edge1.normalize();

			jaFloat detValue = jaVectormath2::det(edge0, edge1);

			if (detValue < 0.0f)
			{
				isConcave = true;
				break;
			}
				
			itVertex = itVertex->nextElement;
		}
	}

	PhysicsManager* pPhysicsManager = PhysicsManager::getSingleton();

	if (false == isConcave)
	{
		jaFloat degrees = this->editedPhysicObject->getShapeHandle()->transform.rotationMatrix.getDegrees();
		this->editedPhysicObject->getShapeHandle()->transform.rotationMatrix.setDegrees(0);

		if (verticesNum < ja::setup::gilgamesh::VERTICES_NUM2D)
		{
			PhysicObjectDef2d* pPhysicObjectDef = &PhysicsManager::getSingleton()->physicObjectDef;
			ConvexPolygon2d* pConvexPolygon = &pPhysicObjectDef->polygon;

			jaVector2 translateOffset;
			jaVector2 shapeCenter = getShapeCenter(firstVertex, verticesNum);

			{
				jaVector2 vertices[setup::gilgamesh::VERTICES_NUM2D];
				
				fillWithLocalCoordinates(firstVertex, verticesNum, shapeCenter, vertices);
				//pConvexPolygon->setVerticesWithoutSettingCentroid(vertices, verticesNum);
				pConvexPolygon->setVertices(vertices, verticesNum);
			}
			
			RigidBody2d* pRigidBody = (RigidBody2d*)physicObject;
			PhysicsManager::getSingleton()->changeBodyShape(pRigidBody, pConvexPolygon);
			pRigidBody->setPosition(shapeCenter.x, shapeCenter.y);
			PhysicsManager::getSingleton()->updatePhysicObjectImmediate(physicObject);
		}
		else
		{
			int32_t verticesLeftNum  = verticesNum;
			int32_t verticesPerSlice = setup::gilgamesh::VERTICES_NUM2D;
			int32_t slicesNum        = 1;

			while (verticesLeftNum > setup::gilgamesh::VERTICES_NUM2D)
			{
				verticesLeftNum -= (setup::gilgamesh::VERTICES_NUM2D - 2);
				++slicesNum;
			}

			PhysicObjectDef2d* pPhysicObjectDef = &PhysicsManager::getSingleton()->physicObjectDef;
			ConvexPolygon2d*   pConvexPolygon   = &pPhysicObjectDef->polygon;
			MultiShape2d*      pMultiShape      = pPhysicObjectDef->multiShape;
			pMultiShape->deleteShapes();

			jaVector2    center = getShapeCenter(firstVertex, verticesNum);

			jaVector2    vertices[setup::gilgamesh::VERTICES_NUM2D];
			jaVector2    shapeCenter    = getShapeCenter(firstVertex, verticesLeftNum);
			jaTransform2 localTransform = jaTransform2(shapeCenter - center, 0);

			ListElement<jaVector2>* itVertex   = firstVertex;
			ListElement<jaVector2>* lastVertex = fillWithLocalCoordinates(firstVertex, verticesLeftNum, shapeCenter, vertices);
			reconnectVertices(firstVertex, lastVertex);

			pConvexPolygon->setVertices(vertices, verticesLeftNum);
			pMultiShape->addShape(localTransform, pConvexPolygon);

			for (int32_t i = 1; i < slicesNum; ++i)
			{
				shapeCenter = getShapeCenter(itVertex, verticesPerSlice);
				localTransform.position = shapeCenter - center;
				lastVertex = fillWithLocalCoordinates(firstVertex, verticesPerSlice, shapeCenter, vertices);
				reconnectVertices(firstVertex, lastVertex);
				pConvexPolygon->setVertices(vertices, verticesPerSlice);
				pMultiShape->addShape(localTransform, pConvexPolygon);
			}

			RigidBody2d* pRigidBody = (RigidBody2d*)physicObject;
			
			PhysicsManager::getSingleton()->changeBodyShape(pRigidBody, pMultiShape);
			pRigidBody->setPosition(center.x, center.y);
			PhysicsManager::getSingleton()->updatePhysicObjectImmediate(physicObject);
		}
	}
	else {
		//divide to convex polygon

		ja::ListElement<jaVector2>* itVertex = firstVertex;
		jaVector2 vertices[setup::gilgamesh::VERTICES_NUM2D];
		jaVector2 center(0.0f, 0.0f);

		PhysicObjectDef2d* pPhysicObjectDef = &PhysicsManager::getSingleton()->physicObjectDef;
		ConvexPolygon2d*   pConvexPolygon = &pPhysicObjectDef->polygon;
		jaTransform2       localTransform;
		MultiShape2d*      pMultiShape = pPhysicObjectDef->multiShape;

		pMultiShape->deleteShapes();
		localTransform.rotationMatrix.setDegrees(0.0f);

		int32_t trianglesNum = 0;

		while (this->verticesNum >= 3)
		{
			jaVector2 edge0 = itVertex->data - itVertex->previousElement->data;
			jaVector2 edge1 = itVertex->nextElement->data - itVertex->data;
			edge0.normalize();
			edge1.normalize();

			jaFloat detValue = jaVectormath2::det(edge0, edge1);

			if (detValue < 0.0f) // concave
			{
				itVertex = itVertex->nextElement;
			}
			else // convex
			{	
				if (true == canMakeTriangle(itVertex->previousElement->data, itVertex->data, itVertex->nextElement->data))
				{
					// make triangle
					jaVector2 triangleCenter = getShapeCenter(itVertex->previousElement, 3);
					localTransform.position  = triangleCenter;
					center += triangleCenter;
					ListElement<jaVector2>* lastVertex = fillWithLocalCoordinates(itVertex->previousElement, 3, triangleCenter, vertices);
					reconnectVertices(itVertex->previousElement, lastVertex);
					pConvexPolygon->setVertices(vertices, 3);
					pMultiShape->addShape(localTransform, pConvexPolygon);
					--this->verticesNum;
					++trianglesNum;
				}
				else
				{
					itVertex = itVertex->nextElement;
				}
			}	
		}

		center.x /= (float)trianglesNum;
		center.y /= (float)trianglesNum;

		for (gil::ObjectHandle2d* shapeProxyIt = pMultiShape->shapes ; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
		{
			shapeProxyIt->transform.position -= center;
		}

		RigidBody2d* pRigidBody = (RigidBody2d*)physicObject;

		PhysicsManager::getSingleton()->changeBodyShape(pRigidBody, pMultiShape);
		pRigidBody->setPosition(center.x, center.y);
		PhysicsManager::getSingleton()->updatePhysicObjectImmediate(physicObject);
	}
}

bool ConvexPolygonEditor::isEdgeIntersectingWithPoly(jaVector2& lineA0, jaVector2& lineA1)
{
	ListElement<jaVector2>* itVertex = firstVertex;
	
	jaVector2 intersectionPoint;
	
	for (uint32_t i=0 ; i < this->verticesNum; ++i)
	{
		if (jaVectormath2::isLineIntersecting(lineA0, lineA1, itVertex->data, itVertex->nextElement->data, intersectionPoint))
			return true;
		itVertex = itVertex->nextElement; 
	}
	
	return false;
}

bool ConvexPolygonEditor::canMakeTriangle(jaVector2& p0, jaVector2& p1, jaVector2& p2)
{
	ListElement<jaVector2>* itVertex = firstVertex;

	// Compute vectors        
	jaVector2 v0 = p2 - p0;
	jaVector2 v1 = p1 - p0;
	
	for (uint32_t i = 0; i < this->verticesNum; ++i)
	{
		
		jaVector2 v2 = itVertex->data - p0;
		
		// Compute dot products
		const float dot00 = jaVectormath2::projection(v0, v0);
		const float dot01 = jaVectormath2::projection(v0, v1);
		const float dot02 = jaVectormath2::projection(v0, v2);
		const float dot11 = jaVectormath2::projection(v1, v1);
		const float dot12 = jaVectormath2::projection(v1, v2);

		// Compute barycentric coordinates
		float invDenom = 1.0f / (dot00 * dot11 - dot01 * dot01);
		float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
		float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

		// Check if point is in triangle
		if ((u > 0.0f) && (v > 0.0f) && ((u + v) < 1.0f))
			return false;

		itVertex = itVertex->nextElement;
	}

	return true;
}

jaVector2 ConvexPolygonEditor::getShapeCenter(ListElement<jaVector2>* firstConvexPolygonVertex, int32_t convexPolygonVerticesNum)
{
	jaVector2 translateOffset;
	ListElement<jaVector2>* itVertex = firstConvexPolygonVertex;

	translateOffset.setZero();
	for (int32_t i = 0; i < convexPolygonVerticesNum; ++i)
	{
		translateOffset.x += itVertex->data.x;
		translateOffset.y += itVertex->data.y;
		itVertex = itVertex->nextElement;
	}

	const float fVerticesNum = (float)convexPolygonVerticesNum;
	translateOffset.x /= fVerticesNum;
	translateOffset.y /= fVerticesNum;

	return translateOffset;
}

ListElement<jaVector2>* ConvexPolygonEditor::fillWithLocalCoordinates(ListElement<jaVector2>* firstConvexVertex, uint32_t convexVerticesNum, const jaVector2& shapeCenter, jaVector2* vertices)
{
	ListElement<jaVector2>* itVertex = firstConvexVertex;

	for (uint32_t i = 0; i < convexVerticesNum; ++i)
	{
		vertices[i] = itVertex->data - shapeCenter;
		itVertex = itVertex->nextElement;
	}

	return itVertex->previousElement;
}

void ConvexPolygonEditor::reconnectVertices(ListElement<jaVector2>* firstConvexVertex, ListElement<jaVector2>* lastConvexVertex)
{
	firstConvexVertex->nextElement    = lastConvexVertex;
	lastConvexVertex->previousElement = firstConvexVertex;
}

TriangleMeshEditor::TriangleMeshEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Triangle Mesh", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 300;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, 3 * (20 + 1));

	DisplayManager* display = DisplayManager::getSingleton();
	infoPosX = (float)(display->getResolutionWidth(0.3f));
	infoPosY = (float)(display->getResolutionHeight(0.3f));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* meshInfoLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		meshInfoLayout->setSize(widgetWidth, 1 * (20 + 1));
		fillFlowLayout(meshInfoLayout);

		Label* verticesLabel = new Label(0, nullptr);
		fillLabel(verticesLabel, "Vertices: ");
		meshInfoLayout->addWidget(verticesLabel);

		this->verticesNumLabel = new Label(1, nullptr);
		fillLabel(this->verticesNumLabel, "0");
		meshInfoLayout->addWidget(this->verticesNumLabel);

		Label* trianglesLabel = new Label(2, nullptr);
		fillLabel(trianglesLabel, "Triangles: ");
		meshInfoLayout->addWidget(trianglesLabel);

		this->trianglesNumLabel = new Label(3, nullptr);
		fillLabel(this->trianglesNumLabel, "0");
		meshInfoLayout->addWidget(this->trianglesNumLabel);

		boxLayout->addWidget(meshInfoLayout);
	}

	{
		FlowLayout* meshEditLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		meshEditLayout->setSize(widgetWidth, 1 * (20 + 1));
		fillFlowLayout(meshEditLayout);

		this->editButton = new Button(0, nullptr);
		fillButton(this->editButton, "Edit");
		this->editButton->setOnClickEvent(this, &TriangleMeshEditor::onEditClick);
		meshEditLayout->addWidget(this->editButton);

		this->makeAsPolygonButton = new Button(1, nullptr);
		fillButton(makeAsPolygonButton, "Make as polygon");
		makeAsPolygonButton->setOnClickEvent(this, &TriangleMeshEditor::onMakeAsPolygonClick);
		meshEditLayout->addWidget(makeAsPolygonButton);

		boxLayout->addWidget(meshEditLayout);
	}

	this->meshEditor = new MeshEditor(5, this);

	addWidgetToEditor(boxLayout);
	addWidget(this->meshEditor);
}

TriangleMeshEditor::~TriangleMeshEditor()
{

}

void TriangleMeshEditor::updateWidget(void* data)
{
	PhysicObject2d* physicObject = (PhysicObject2d*)(data);

	if (nullptr == physicObject)
		return;

	ObjectHandle2d* objectHandle = physicObject->getShapeHandle();
	Shape2d*        shape = objectHandle->getShape();

	if (shape->getType() == (uint8_t)ShapeType::TriangleMesh) {
		TriangleMesh2d* triangleMesh = (TriangleMesh2d*)(shape);

		uint32_t verticesNum = triangleMesh->getVerticesNum();
		this->verticesNumLabel->setTextPrintC("%u", verticesNum);

		uint32_t trianglesNum = triangleMesh->getTrianglesNum();
		this->trianglesNumLabel->setTextPrintC("%u", trianglesNum);
	}
}

void TriangleMeshEditor::onMakeAsPolygonClick(Widget* sender, WidgetEvent action, void* data)
{

}

void TriangleMeshEditor::onEditClick(Widget* sender, WidgetEvent action, void* data)
{
	this->meshEditor->setActive(true);
	this->meshEditor->setRender(true);
}

PointEditor::PointEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Point", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 300;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, 3 * (20 + 1));

	DisplayManager* display = DisplayManager::getSingleton();
	infoPosX = (float)(display->getResolutionWidth(0.5f));
	infoPosY = (float)(display->getResolutionHeight(0.5f));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* meshInfoLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		meshInfoLayout->setSize(widgetWidth, 1 * (20 + 1));
		fillFlowLayout(meshInfoLayout);

		this->createButton = new Button(1, nullptr);
		fillButton(this->createButton, "Create");
		meshInfoLayout->addWidget(this->createButton);
		this->createButton->setOnClickEvent(this, &PointEditor::onCreateClick);

		boxLayout->addWidget(meshInfoLayout);
	}

	{
		FlowLayout* meshEditLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		meshEditLayout->setSize(widgetWidth, 1 * (20 + 1));
		fillFlowLayout(meshEditLayout);

		boxLayout->addWidget(meshEditLayout);
	}

	addWidgetToEditor(boxLayout);
}

PointEditor::~PointEditor()
{

}

void PointEditor::updateWidget(void* data)
{

}

void PointEditor::onCreateClick(Widget* sender, WidgetEvent action, void* data)
{
	ja::PhysicObjectDef2d* physicObjectDef = &PhysicsManager::getSingleton()->physicObjectDef;

	physicObjectDef->setShapeType((int8_t)ShapeType::Point);

	PhysicsEditor* physicsEditor = PhysicsEditor::getSingleton();
	physicObjectDef->transform.position = physicsEditor->getGizmoPosition();

	PhysicsManager::getSingleton()->createBodyDef();
}

ShapeEditor::ShapeEditor( uint32_t widgetId, Widget* parent ) : CommonWidget(widgetId,"Shape",parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 300;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, 11 * (20 + 1));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* posLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		posLayout->setSize(widgetWidth, 20);
		fillFlowLayout(posLayout);
		Label* posLabel = new Label(0, nullptr);
		fillLabel(posLabel, "Transformation");
		posLayout->addWidget(posLabel);
		boxLayout->addWidget(posLayout);
	}
	
	{
		FlowLayout* postLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		postLayout->setSize(widgetWidth, 20);
		fillFlowLayout(postLayout);
		Label* xLabel = new Label(0, nullptr);
		fillLabel(xLabel, "X");
		this->xTextfield = new TextField(1, nullptr);
		fillTextField(this->xTextfield);
		this->xTextfield->setAcceptOnlyNumbers(true);
		this->xTextfield->setTextSize(12, 20);
		this->xTextfield->setOnDeselectEvent(this, &ShapeEditor::xDeselect);
		Label* yLabel = new Label(2, nullptr);
		fillLabel(yLabel, "Y");
		this->yTextfield = new TextField(3, nullptr);
		fillTextField(this->yTextfield);
		this->yTextfield->setAcceptOnlyNumbers(true);
		this->yTextfield->setTextSize(12, 20);
		this->yTextfield->setOnDeselectEvent(this, &ShapeEditor::yDeselect);

		this->xTextfield->setActive(false);
		this->yTextfield->setActive(false);

		postLayout->addWidget(xLabel);
		postLayout->addWidget(this->xTextfield);
		postLayout->addWidget(yLabel);
		postLayout->addWidget(this->yTextfield);
		boxLayout->addWidget(postLayout);
	}
	
	{
		FlowLayout* rotLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		rotLayout->setSize(widgetWidth, 20);
		fillFlowLayout(rotLayout);
		Label* angleLabel = new Label(0, nullptr);
		fillLabel(angleLabel, "Angle");
		rotLayout->addWidget(angleLabel);
		this->angleTextfield = new TextField(1, nullptr);
		fillTextField(this->angleTextfield);
		this->angleTextfield->setAcceptOnlyNumbers(true);
		this->angleTextfield->setTextSize(12, 20);
		this->angleTextfield->setOnDeselectEvent(this, &ShapeEditor::angleDeselect);
		
		this->angleTextfield->setActive(false);

		rotLayout->addWidget(this->angleTextfield);
		boxLayout->addWidget(rotLayout);
	}

	{
		FlowLayout* shapeLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		shapeLayout->setSize(widgetWidth, 20);
		fillFlowLayout(shapeLayout);
		Label* shapesLabel = new Label(0, nullptr);
		fillLabel(shapesLabel, "Shapes");
		shapeLayout->addWidget(shapesLabel);

		FlowLayout* shapesLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		shapesLayout->setSize(widgetWidth, 2 * 20);
		fillFlowLayout(shapesLayout);

		this->boxButton = new Button((uint32_t)TR_PHYSICSEDITOR_ID::BOX_EDITOR, nullptr);
		fillButton(this->boxButton, "Box");
		this->boxButton->setOnClickEvent(PhysicsComponentMenu::getSingleton(), &PhysicsComponentMenu::onMenuClick);

		this->circleButton = new Button((uint32_t)TR_PHYSICSEDITOR_ID::CIRCLE_EDITOR, nullptr);
		fillButton(this->circleButton, "Circle");
		this->circleButton->setOnClickEvent(PhysicsComponentMenu::getSingleton(), &PhysicsComponentMenu::onMenuClick);

		this->convexPolygonButton = new Button((uint32_t)TR_PHYSICSEDITOR_ID::CONVEX_POLYGON_EDITOR, nullptr);
		fillButton(this->convexPolygonButton, "Convex Polygon");
		this->convexPolygonButton->setOnClickEvent(PhysicsComponentMenu::getSingleton(), &PhysicsComponentMenu::onMenuClick);

		this->triangleMeshButton = new Button((uint32_t)TR_PHYSICSEDITOR_ID::TRIANGLE_MESH_EDITOR, nullptr);
		fillButton(this->triangleMeshButton, "Triangle Mesh");
		this->triangleMeshButton->setOnClickEvent(PhysicsComponentMenu::getSingleton(), &PhysicsComponentMenu::onMenuClick);

		this->pointButton = new Button((uint32_t)TR_PHYSICSEDITOR_ID::POINT_EDITOR, nullptr);
		fillButton(this->pointButton, "Point");
		this->pointButton->setOnClickEvent(PhysicsComponentMenu::getSingleton(), &PhysicsComponentMenu::onMenuClick);

		shapesLayout->addWidget(this->boxButton);
		shapesLayout->addWidget(this->circleButton);
		shapesLayout->addWidget(this->convexPolygonButton);
		shapesLayout->addWidget(this->triangleMeshButton);
		shapesLayout->addWidget(this->pointButton);

		boxLayout->addWidget(shapeLayout);
		boxLayout->addWidget(shapesLayout);
	}
	
	{
		FlowLayout* combineLayout = new FlowLayout(6, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		combineLayout->setSize(widgetWidth, 20);
		fillFlowLayout(combineLayout);
		
		Label* shapeGroupLabel = new Label(0, nullptr);
		fillLabel(shapeGroupLabel, "Shape Group:");
		combineLayout->addWidget(shapeGroupLabel);
		this->combineButton = new Button(0, nullptr);
		fillButton(this->combineButton, "Combine");
		this->combineButton->setOnClickEvent(this, &ShapeEditor::combineClick);
		combineLayout->addWidget(this->combineButton);
		this->decomposeButton = new Button(1, nullptr);
		fillButton(this->decomposeButton, "Decompose");
		this->decomposeButton->setOnClickEvent(this, &ShapeEditor::decomposeClick);
		combineLayout->addWidget(this->decomposeButton);
		this->editModeButton = new Button(2, nullptr);
		fillButton(this->editModeButton, "Edit mode");
		combineLayout->addWidget(this->editModeButton);
		boxLayout->addWidget(combineLayout);

		this->combineButton->setActive(false);
		this->decomposeButton->setActive(false);
		this->editModeButton->setActive(false);
	}
	
	{
		FlowLayout* massDataLayout = new FlowLayout(7, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		massDataLayout->setSize(widgetWidth, 20);
		fillFlowLayout(massDataLayout);
		Label* centerOfMassLabel = new Label(0, nullptr);
		fillLabel(centerOfMassLabel, "Center of mass");
		massDataLayout->addWidget(centerOfMassLabel);
		boxLayout->addWidget(massDataLayout);

		FlowLayout* positionLayout = new FlowLayout(8, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		positionLayout->setSize(widgetWidth, 20);
		fillFlowLayout(positionLayout);
		Label* xLabel = new Label(0, nullptr);
		fillLabel(xLabel, "X");
		this->xComTextfield = new TextField(1, nullptr);
		fillTextField(this->xComTextfield);
		this->xComTextfield->setAcceptOnlyNumbers(true);
		this->xComTextfield->setTextSize(12, 20);
		Label* yLabel = new Label(2, nullptr);
		fillLabel(yLabel, "Y");
		this->yComTextfield = new TextField(3, nullptr);
		fillTextField(this->yComTextfield);
		this->yComTextfield->setAcceptOnlyNumbers(true);
		this->yComTextfield->setTextSize(12, 20);
		positionLayout->addWidget(xLabel);
		positionLayout->addWidget(this->xComTextfield);
		positionLayout->addWidget(yLabel);
		positionLayout->addWidget(this->yComTextfield);
		boxLayout->addWidget(positionLayout);

		FlowLayout* combineLayout = new FlowLayout(9, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		combineLayout->setSize(widgetWidth, 20);
		fillFlowLayout(combineLayout);

		
		this->applyCofButton = new Button(1, nullptr);
		fillButton(this->applyCofButton, "Apply");
		combineLayout->addWidget(this->applyCofButton);
		this->editCofButton = new Button(2, nullptr);
		fillButton(this->editCofButton, "Edit");
		combineLayout->addWidget(this->editCofButton);
		this->resetCofButton = new Button(3, nullptr);
		fillButton(this->resetCofButton, "Reset");
		this->resetCofButton->setOnClickEvent(this, &ShapeEditor::resetClick);
		combineLayout->addWidget(this->resetCofButton);
		boxLayout->addWidget(combineLayout);

		this->xComTextfield->setActive(false);
		this->yComTextfield->setActive(false);
		this->applyCofButton->setActive(false);
		this->editCofButton->setActive(false);
		this->resetCofButton->setActive(false);
	}

	{
		FlowLayout* momentOfInteriaLayout = new FlowLayout(10, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		momentOfInteriaLayout->setSize(widgetWidth, 20);
		fillFlowLayout(momentOfInteriaLayout);
		Label* momentOfInteriaLabel = new Label(0, nullptr);
		fillLabel(momentOfInteriaLabel, "Moment of interia");
		momentOfInteriaLayout->addWidget(momentOfInteriaLabel);

		this->massOfInteriaTextfield = new TextField(1, nullptr);
		fillTextField(this->massOfInteriaTextfield);
		this->massOfInteriaTextfield->setAcceptOnlyNumbers(true);
		this->massOfInteriaTextfield->setTextSize(12, 20);
		momentOfInteriaLayout->addWidget(this->massOfInteriaTextfield);

		this->massOfInteriaTextfield->setActive(false);

		boxLayout->addWidget(momentOfInteriaLayout);
	}

	addWidgetToEditor(boxLayout);
}

void ShapeEditor::updateWidget(void* data)
{
	PhysicObject2d* physicObject = (PhysicObject2d*)(data);

	if (nullptr == physicObject)
	{
		this->xTextfield->setText("");
		this->yTextfield->setText("");
		this->angleTextfield->setText("");
		this->massOfInteriaTextfield->setText("");
		this->xComTextfield->setText("");
		this->yComTextfield->setText("");

		this->xTextfield->setActive(false);
		this->yTextfield->setActive(false);
		this->angleTextfield->setActive(false);
		
		this->massOfInteriaTextfield->setActive(false);
		this->xComTextfield->setActive(false);
		this->yComTextfield->setActive(false);
		return;
	}

	ObjectHandle2d* objectHandle = physicObject->getShapeHandle();
	Shape2d*        shape        = objectHandle->getShape();

	this->xTextfield->setActive(true);
	this->yTextfield->setActive(true);
	this->angleTextfield->setActive(true);
	
	this->xTextfield->setFieldValue("%f", objectHandle->transform.position.x);
	this->yTextfield->setFieldValue("%f", objectHandle->transform.position.y);
	this->angleTextfield->setFieldValue("%f", objectHandle->transform.rotationMatrix.getDegrees());

	if (shape->getType() == (uint8_t)ShapeType::Box) 
	{
		BoxEditor::getSingleton()->updateWidget(physicObject);
	}

	if (shape->getType() == (uint8_t)ShapeType::Circle)
	{
		CircleEditor::getSingleton()->updateWidget(physicObject);
	}

	if (shape->getType() == (uint8_t)ShapeType::ConvexPolygon)
	{
		ConvexPolygonEditor::getSingleton()->updateWidget(physicObject);
	}

	if (shape->getType() == (uint8_t)ShapeType::TriangleMesh)
	{
		TriangleMeshEditor::getSingleton()->updateWidget(physicObject);
	}

	bool isDecomposeActive = (shape->getType() == (uint8_t)ShapeType::MultiShape);
	this->decomposeButton->setActive(isDecomposeActive);

	bool isCombineActive = (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 1);
	this->combineButton->setActive(isCombineActive);

	if (physicObject->getObjectType() == (uint8_t)PhysicObjectType::RIGID_BODY)
	{
		RigidBody2d* rigidBody = (RigidBody2d*)physicObject;
		this->massOfInteriaTextfield->setFieldValue("%f", rigidBody->getInteria());
		jaVector2 localCenterOfMass = rigidBody->getLocalCenterOfMass();
		this->xComTextfield->setFieldValue("%f", localCenterOfMass.x);
		this->yComTextfield->setFieldValue("%f", localCenterOfMass.y);
	}
	else {
		this->massOfInteriaTextfield->setFieldValue("%f", 0.0f);
		this->xComTextfield->setFieldValue("%f", 0.0f);
		this->yComTextfield->setFieldValue("%f", 0.0f);
	}
}

void ShapeEditor::xDeselect(Widget* sender, WidgetEvent action, void* data)
{
	xTextfield->getAsFloat(&x);
	PhysicsEditor::getSingleton()->applyFilterToSelection(xFilter);
}

void ShapeEditor::yDeselect(Widget* sender, WidgetEvent action, void* data)
{
	yTextfield->getAsFloat(&y);
	PhysicsEditor::getSingleton()->applyFilterToSelection(yFilter);
}

void ShapeEditor::angleDeselect(Widget* sender, WidgetEvent action, void* data)
{
	angleTextfield->getAsFloat(&angleDeg);
	PhysicsEditor::getSingleton()->applyFilterToSelection(angleFilter);
}

void ShapeEditor::xFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = static_cast<RigidBody2d*>(physicObject);
	jaVector2 position = rigidBody->getPosition();
	rigidBody->setPosition(singleton->x,position.y);
	PhysicsManager::getSingleton()->updatePhysicObjectImmediate(rigidBody);
}

void ShapeEditor::yFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = static_cast<RigidBody2d*>(physicObject);
	jaVector2 position = rigidBody->getPosition();
	rigidBody->setPosition(position.x, singleton->y);
	PhysicsManager::getSingleton()->updatePhysicObjectImmediate(rigidBody);
}

void ShapeEditor::angleFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = static_cast<RigidBody2d*>(physicObject);
	rigidBody->setAngleDeg(singleton->angleDeg);
	PhysicsManager::getSingleton()->updatePhysicObjectImmediate(rigidBody);
}

void ShapeEditor::resetClick(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	std::list<PhysicObject2d*>* selectedPhysicObjects = &PhysicsEditor::getSingleton()->selectedPhysicObjects;

	if (selectedPhysicObjects->size() < 1)
		return;

	std::list<PhysicObject2d*>::iterator itPhysics = selectedPhysicObjects->begin();
	for (itPhysics; itPhysics != selectedPhysicObjects->end(); ++itPhysics) {

		if ((*itPhysics)->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
			continue;
		if ((*itPhysics)->getComponentData() != nullptr)
			continue;

		RigidBody2d* rigidBody = (RigidBody2d*)*itPhysics;
		rigidBody->resetMassData();
	}

	updateWidget(selectedPhysicObjects->back());
}

void ShapeEditor::combineClick(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	std::list<PhysicObject2d*>* selectedPhysicObjects = &PhysicsEditor::getSingleton()->selectedPhysicObjects;

	if (selectedPhysicObjects->size() < 2)
	{
		Toolset::getSingleton()->infoHud->addInfo(255,0,0,"You need select more than one physic objects");
		return;
	}

	MultiShape2d multiShape(PhysicsManager::getSingleton()->getTemporaryShapeAllocator());

	PhysicObject2d* physicObject = selectedPhysicObjects->back();
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;
	if (physicObject->getComponentData() != nullptr)
		return;

	RigidBody2d*    multiShapeBody   = (RigidBody2d*)(physicObject);
	ObjectHandle2d* multiShapeHandle = multiShapeBody->getShapeHandle();
	selectedPhysicObjects->remove(physicObject);

	jaVector2    multiShapeCenter = PhysicsEditor::getSingleton()->getGizmoPosition();
	jaTransform2 shapeTransform;

	std::list<PhysicObject2d*>::iterator itPhysics = selectedPhysicObjects->begin();
	for (itPhysics; itPhysics != selectedPhysicObjects->end(); ++itPhysics) {
		
		if ((*itPhysics)->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
			continue;
		if ((*itPhysics)->getComponentData() != nullptr)
			continue;

		ObjectHandle2d* shapeHandle = (*itPhysics)->getShapeHandle();
		
		shapeTransform.position       = shapeHandle->transform.position - multiShapeCenter;
		shapeTransform.rotationMatrix = shapeHandle->transform.rotationMatrix;

		ObjectHandle2d* addedShape = multiShape.addShape(shapeTransform, shapeHandle->shape);
		addedShape->groupIndex     = shapeHandle->groupIndex;
		addedShape->categoryBits   = shapeHandle->categoryBits;
		addedShape->maskBits       = shapeHandle->maskBits;
		
		physicsManager->deletePhysicObjectImmediate(*itPhysics);
	}

	shapeTransform.position       = multiShapeHandle->transform.position - multiShapeCenter;
	shapeTransform.rotationMatrix = multiShapeHandle->transform.rotationMatrix;

	ObjectHandle2d* addedShape    = multiShape.addShape(shapeTransform, multiShapeHandle->shape);
	addedShape->groupIndex        = multiShapeHandle->groupIndex;
	addedShape->categoryBits      = multiShapeHandle->categoryBits;
	addedShape->maskBits          = multiShapeHandle->maskBits;

	multiShapeHandle->transform.position = multiShapeCenter;
	multiShapeHandle->transform.rotationMatrix.setDegrees(0.0f);
	physicsManager->changeBodyShape(multiShapeBody, &multiShape); 

	physicsManager->updatePhysicObjectImmediate(multiShapeBody);

	selectedPhysicObjects->clear();
}

void ShapeEditor::decomposeClick(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	std::list<PhysicObject2d*>* selectedPhysicObjects = &PhysicsEditor::getSingleton()->selectedPhysicObjects;

	std::list<PhysicObject2d*>::iterator itPhysics = selectedPhysicObjects->begin();
	for (itPhysics; itPhysics != selectedPhysicObjects->end(); ++itPhysics) {

		if ((*itPhysics)->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
			continue;
		if ((*itPhysics)->getComponentData() != nullptr)
			continue;

		ObjectHandle2d* shapeHandle = (*itPhysics)->getShapeHandle();
		if (shapeHandle->getShape()->getType() != (uint8_t)ShapeType::MultiShape)
			continue;

		MultiShape2d*   multiShape    = (MultiShape2d*)shapeHandle->getShape();
		ObjectHandle2d* shapeProxyIt  = multiShape->getShapeHandle();
		RigidBody2d*    rigidBody     = (RigidBody2d*)(*itPhysics);
		jaTransform2    shapeTransform;
		jaTransform2    parentTransform = shapeHandle->getTransform();

		for (shapeProxyIt; shapeProxyIt != nullptr; shapeProxyIt = shapeProxyIt->next)
		{
			shapeTransform.rotationMatrix = parentTransform.rotationMatrix * shapeProxyIt->transform.rotationMatrix;
			shapeTransform.position       = parentTransform.position + (parentTransform.rotationMatrix * shapeProxyIt->transform.position);

			physicsManager->createBody(shapeProxyIt->getShape(), (BodyType)rigidBody->getBodyType(), shapeTransform, rigidBody->isFixedRotation(), rigidBody->isSleeping());
		}

		physicsManager->deletePhysicObjectImmediate(*itPhysics);
	}

	selectedPhysicObjects->clear();
}

VelocityEditor::VelocityEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Velocity", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 220;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, 3 * (20 + 1));
	fillBoxLayout(boxLayout);

	FlowLayout* linLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	linLayout->setSize(widgetWidth, 20);
	fillFlowLayout(linLayout);
	Label* linLabel = new Label(0, nullptr);
	fillLabel(linLabel, "Linear");
	linLayout->addWidget(linLabel);

	FlowLayout* linearLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	linearLayout->setSize(widgetWidth, 20);
	fillFlowLayout(linearLayout);
	Label* linXLabel = new Label(0, nullptr);
	fillLabel(linXLabel, "X");
	this->linXTextfield = new TextField(1, nullptr);
	fillTextField(this->linXTextfield);
	this->linXTextfield->setAcceptOnlyNumbers(true);
	this->linXTextfield->setTextSize(12, 20);
	this->linXTextfield->setOnDeselectEvent(this, &VelocityEditor::linXVelDeselect);
	this->linXTextfield->setActive(false);
	Label* linYLabel = new Label(2, nullptr);
	fillLabel(linYLabel, "Y");
	this->linYTextfield = new TextField(3, nullptr);
	fillTextField(this->linYTextfield);
	this->linYTextfield->setAcceptOnlyNumbers(true);
	this->linYTextfield->setTextSize(12, 20);
	this->linYTextfield->setOnDeselectEvent(this, &VelocityEditor::linYVelDeselect);
	linearLayout->addWidget(linXLabel);
	linearLayout->addWidget(this->linXTextfield);
	linearLayout->addWidget(linYLabel);
	linearLayout->addWidget(this->linYTextfield);

	FlowLayout* angLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	angLayout->setSize(220, 20);
	fillFlowLayout(angLayout);
	Label* angLabel = new Label(0, nullptr);
	fillLabel(angLabel, "Angular");
	angLayout->addWidget(angLabel);

	this->angularTextfield = new TextField(1, nullptr);
	fillTextField(this->angularTextfield);
	this->angularTextfield->setAcceptOnlyNumbers(true);
	this->angularTextfield->setTextSize(12, 20);
	this->angularTextfield->setOnDeselectEvent(this, &VelocityEditor::angVelDeselect);
	angLayout->addWidget(this->angularTextfield);
	this->angularTextfield->setActive(false);

	boxLayout->addWidget(linLayout);
	boxLayout->addWidget(linearLayout);
	boxLayout->addWidget(angLayout);
	addWidgetToEditor(boxLayout);
}

void VelocityEditor::updateWidget(void* data)
{
	PhysicObject2d* physicObject = static_cast<PhysicObject2d*>(data);
	
	if (nullptr == physicObject)
	{
		this->linXTextfield->setText("");
		this->linYTextfield->setText("");
		this->angularTextfield->setText("");

		this->linXTextfield->setActive(false);
		this->linYTextfield->setActive(false);
		this->angularTextfield->setActive(false);
		return;
	}

	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	this->linXTextfield->setActive(true);
	this->linYTextfield->setActive(true);
	this->angularTextfield->setActive(true);

	RigidBody2d* rigidBody = (RigidBody2d*)(physicObject);

	jaVector2 linearVelocity = rigidBody->getLinearVelocity();
	this->linXTextfield->setFieldValue("%f", linearVelocity.x);
	this->linYTextfield->setFieldValue("%f", linearVelocity.y);
	this->angularTextfield->setFieldValue("%f", rigidBody->getAngularVelocity());
}

void VelocityEditor::linXVelDeselect(Widget* sender, WidgetEvent action, void* data)
{
	float linearVelX = 0.0f;
	if (false == linXTextfield->getAsFloat(&linearVelX))
	{
		this->linXTextfield->setFieldValue("%f", this->linearVelocity.x);
	}
	else
	{
		this->linearVelocity.x = linearVelX;
	}
	
	PhysicsEditor::getSingleton()->applyFilterToSelection(linVelocityXFilter);
}

void VelocityEditor::linYVelDeselect(Widget* sender, WidgetEvent action, void* data)
{
	float linearVelY = 0.0f;
	if (false == linYTextfield->getAsFloat(&linearVelY))
	{
		this->linYTextfield->setFieldValue("%f", this->linearVelocity.y);
	}
	else
	{
		this->linearVelocity.y = linearVelY;
	}

	PhysicsEditor::getSingleton()->applyFilterToSelection(linVelocityYFilter);
}

void VelocityEditor::angVelDeselect(Widget* sender, WidgetEvent action, void* data)
{
	float angularVel = 0.0f;
	if (false == angularTextfield->getAsFloat(&angularVel))
	{
		this->angularTextfield->setFieldValue("%f", this->angularVelocity);
	}
	else
	{
		this->angularVelocity = angularVel;
	}
	
	PhysicsEditor::getSingleton()->applyFilterToSelection(angVelocityFilter);
}

void VelocityEditor::linVelocityXFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = (RigidBody2d*)(physicObject);
	jaVector2 linVel = rigidBody->getLinearVelocity();
	linVel.x = singleton->linearVelocity.x;
	rigidBody->setLinearVelocity(linVel);
}

void VelocityEditor::linVelocityYFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = static_cast<RigidBody2d*>(physicObject);
	jaVector2 linVel = rigidBody->getLinearVelocity();
	linVel.y = singleton->linearVelocity.y;
	rigidBody->setLinearVelocity(linVel);
}

void VelocityEditor::angVelocityFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = static_cast<RigidBody2d*>(physicObject);
	rigidBody->setAngularVelocity(singleton->angularVelocity);
}

//////////////////////////////////////////////////////////////////
DampingEditor::DampingEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Damping", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 220;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, 2 * (20 + 1));
	fillBoxLayout(boxLayout);

	FlowLayout* linLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	linLayout->setSize(widgetWidth, 20);
	fillFlowLayout(linLayout);
	Label* linLabel = new Label(0, nullptr);
	fillLabel(linLabel, "Linear");
	this->linDampingTextfield = new TextField(1, nullptr);
	fillTextField(this->linDampingTextfield);
	this->linDampingTextfield->setAcceptOnlyNumbers(true);
	this->linDampingTextfield->setTextSize(12, 20);
	this->linDampingTextfield->setOnDeselectEvent(this, &DampingEditor::linDampingDeselect);
	linLayout->addWidget(linLabel);
	linLayout->addWidget(this->linDampingTextfield);
	this->linDampingTextfield->setActive(false);

	FlowLayout* angLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	angLayout->setSize(widgetWidth, 20);
	fillFlowLayout(angLayout);
	Label* angLabel = new Label(0, nullptr);
	fillLabel(angLabel, "Angular");
	this->angDampingTextfield = new TextField(1, nullptr);
	fillTextField(this->angDampingTextfield);
	this->angDampingTextfield->setAcceptOnlyNumbers(true);
	this->angDampingTextfield->setTextSize(12, 20);
	this->angDampingTextfield->setOnDeselectEvent(this, &DampingEditor::angDampingDeselect);
	angLayout->addWidget(angLabel);
	angLayout->addWidget(this->angDampingTextfield);
	this->angDampingTextfield->setActive(false);

	boxLayout->addWidget(linLayout);
	boxLayout->addWidget(angLayout);
	addWidgetToEditor(boxLayout);
}

void DampingEditor::updateWidget(void* data)
{
	PhysicObject2d* physicObject = (PhysicObject2d*)(data);

	if (nullptr == physicObject)
	{
		this->linDampingTextfield->setText("");
		this->angDampingTextfield->setText("");
		this->linDampingTextfield->setActive(false);
		this->angDampingTextfield->setActive(false);

		return;
	}

	this->linDampingTextfield->setActive(true);
	this->angDampingTextfield->setActive(true);

	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;
	RigidBody2d* rigidBody = (RigidBody2d*)(physicObject);

	linDampingTextfield->setFieldValue("%f", rigidBody->getLinearDamping());
	angDampingTextfield->setFieldValue("%f", rigidBody->getAngularDamping());
}

void DampingEditor::linDampingDeselect(Widget* sender, WidgetEvent action, void* data)
{
	float linearDampingVal = 0.0f;

	if (false == linDampingTextfield->getAsFloat(&linearDampingVal))
	{
		this->linDampingTextfield->setFieldValue("%f",this->linearDamping);
	}
	else
	{
		this->linearDamping = linearDampingVal;
	}
	
	PhysicsEditor::getSingleton()->applyFilterToSelection(linDampingFilter);
}

void DampingEditor::angDampingDeselect(Widget* sender, WidgetEvent action, void* data)
{
	float angularDampingVal = 0.0f;

	if (false == angDampingTextfield->getAsFloat(&angularDampingVal))
	{
		this->angDampingTextfield->setFieldValue("%f", this->angularDamping);
	}
	else
	{
		this->angularDamping = angularDampingVal;
	}
	
	PhysicsEditor::getSingleton()->applyFilterToSelection(angDampingFilter);
}

void DampingEditor::linDampingFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = (RigidBody2d*)(physicObject);
	rigidBody->setLinearDamping(singleton->linearDamping);
}

void DampingEditor::angDampingFilter(PhysicObject2d* physicObject)
{
	if (physicObject->getObjectType() != (uint8_t)PhysicObjectType::RIGID_BODY)
		return;

	RigidBody2d* rigidBody = (RigidBody2d*)(physicObject);
	rigidBody->setAngularDamping(singleton->angularDamping);
}

LinearMotorEditor::LinearMotorEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Linear Motor", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	BoxLayout* boxLayout = new BoxLayout(4,this,(uint32_t)Margin::LEFT,false,0,1);
	boxLayout->setSize(220,7 * (20 + 1));
	fillBoxLayout(boxLayout);

	FlowLayout* posLayout = new FlowLayout(0,parent,(uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE,2,0);
	posLayout->setSize(220, 20);
	fillFlowLayout(posLayout);
	Label* positionLabel = new Label(0,nullptr);
	fillLabel(positionLabel,"Position");
	posLayout->addWidget(positionLabel);

	FlowLayout* positionLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	positionLayout->setSize(220, 20);
	fillFlowLayout(positionLayout);
	Label* posXLabel = new Label(0,nullptr);
	fillLabel(posXLabel,"X");
	posXTextfield = new TextField(1,nullptr);
	fillTextField(posXTextfield);
	posXTextfield->setAcceptOnlyNumbers(true);
	posXTextfield->setTextSize(12,20);
	Label* posYLabel = new Label(2,nullptr);
	fillLabel(posYLabel,"Y");
	posYTextfield = new TextField(3,nullptr);
	fillTextField(posYTextfield);
	posYTextfield->setAcceptOnlyNumbers(true);
	posYTextfield->setTextSize(12,20);
	positionLayout->addWidget(posXLabel);
	positionLayout->addWidget(posXTextfield);
	positionLayout->addWidget(posYLabel);
	positionLayout->addWidget(posYTextfield);

	FlowLayout* normLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	normLayout->setSize(220,20);
	fillFlowLayout(normLayout);
	Label* normalLabel = new Label(0,nullptr);
	fillLabel(normalLabel,"Normal");
	normLayout->addWidget(normalLabel);

	FlowLayout* normalLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	normalLayout->setSize(220,20);
	fillFlowLayout(normalLayout);
	Label* norXLabel = new Label(0,nullptr);
	fillLabel(norXLabel,"X");
	norXTextfield = new TextField(1,nullptr);
	fillTextField(norXTextfield);
	norXTextfield->setAcceptOnlyNumbers(true);
	norXTextfield->setTextSize(12,20);
	Label* norYLabel = new Label(2,nullptr);
	fillLabel(norYLabel,"Y");
	norYTextfield = new TextField(3,nullptr);
	fillTextField(norYTextfield);
	norYTextfield->setAcceptOnlyNumbers(true);
	norYTextfield->setTextSize(12,20);
	normalLayout->addWidget(norXLabel);
	normalLayout->addWidget(norXTextfield);
	normalLayout->addWidget(norYLabel);
	normalLayout->addWidget(norYTextfield);

	FlowLayout* maxImpulseLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	maxImpulseLayout->setSize(220,20);
	fillFlowLayout(maxImpulseLayout);
	Label* maxImpulseLabel = new Label(0,nullptr);
	fillLabel(maxImpulseLabel,"Max velocity");
	maxImpulseTextfield = new TextField(1,nullptr);
	fillTextField(maxImpulseTextfield);
	maxImpulseTextfield->setAcceptOnlyNumbers(true);
	maxImpulseTextfield->setTextSize(12,20);
	maxImpulseLayout->addWidget(maxImpulseLabel);
	maxImpulseLayout->addWidget(maxImpulseTextfield);

	FlowLayout* accelerationLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	accelerationLayout->setSize(220,20);
	fillFlowLayout(accelerationLayout);
	Label* accelerationLabel = new Label(0,nullptr);
	fillLabel(accelerationLabel,"Acceleration");
	accelerationTextfield = new TextField(1,nullptr);
	fillTextField(accelerationTextfield);
	accelerationTextfield->setAcceptOnlyNumbers(true);
	accelerationTextfield->setTextSize(12,20);
	accelerationLayout->addWidget(accelerationLabel);
	accelerationLayout->addWidget(accelerationTextfield);

	FlowLayout* editLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	editLayout->setSize(220, 20);
	fillFlowLayout(editLayout);
	editModeButton = new Button(0, nullptr); 
	editModeButton->setFont("default.ttf",10);
	editModeButton->setColor(0,0,255,128);
	editModeButton->setFontColor(255,255,255,255);
	editModeButton->setText("Edit mode");
	editModeButton->adjustToText(10,20);
	editModeButton->setOnClickEvent(this,&LinearMotorEditor::editModeClick);
	createMotorButton = new Button(1,nullptr);
	fillButton(createMotorButton,"Create");
	createMotorButton->setOnClickEvent(this,&LinearMotorEditor::createMotorClick);
	deleteMotorButton = new Button(2,nullptr);
	fillButton(deleteMotorButton,"Delete");
	deleteMotorButton->setOnClickEvent(this,&LinearMotorEditor::deleteMotorClick);
	editLayout->addWidget(editModeButton);
	editLayout->addWidget(createMotorButton);
	editLayout->addWidget(deleteMotorButton);

	boxLayout->addWidget(posLayout);
	boxLayout->addWidget(positionLayout);
	boxLayout->addWidget(normLayout);
	boxLayout->addWidget(normalLayout);
	boxLayout->addWidget(maxImpulseLayout);
	boxLayout->addWidget(accelerationLayout);
	boxLayout->addWidget(editLayout);

	addWidgetToEditor(boxLayout);
}

void LinearMotorEditor::render()
{
	float anchorX, anchorY, normalX, normalY;
	posXTextfield->getAsFloat(&anchorX);
	posYTextfield->getAsFloat(&anchorY);
	norXTextfield->getAsFloat(&normalX);
	norYTextfield->getAsFloat(&normalY);
	DisplayManager::setColorUb(255,255,255,255);
	DisplayManager::drawLine(anchorX,anchorY,anchorX + normalX,anchorY + normalY);
	DisplayManager::setColorUb(255,0,0,255);
	DisplayManager::setPointSize(2.0f);
	DisplayManager::drawPoint(anchorX,anchorY);
}

void LinearMotorEditor::handleInput()
{
	if(InputManager::isKeyPressed(JA_ESCAPE))
	{
		PhysicsEditor::getSingleton()->setEditorMode(PhysicsEditorMode::SELECT_MODE);
	}

	if(!UIManager::isMouseOverGui())
	{
		if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
		{
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
			posXTextfield->setFieldValue("%f",drawPoint.x);
			posYTextfield->setFieldValue("%f",drawPoint.y);
		}

		if (InputManager::isMouseButtonPress((uint8_t)MouseButton::BUTTON_LEFT))
		{
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
			float anchorX, anchorY, normalX, normalY;
			posXTextfield->getAsFloat(&anchorX);
			posYTextfield->getAsFloat(&anchorY);
			normalX = drawPoint.x - anchorX;
			normalY = drawPoint.y - anchorY;
			norXTextfield->setFieldValue("%f",normalX);
			norYTextfield->setFieldValue("%f",normalY);
		}

		if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
		{

		}

		if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_RIGHT))
		{
			PhysicsManager* physicsManager = PhysicsManager::getSingleton();
			std::list<PhysicObject2d*>& selectedPhysics =  PhysicsEditor::getSingleton()->selectedPhysicObjects;
			if(selectedPhysics.size() != 0)
			{
				
			}
		}
	}
}

void LinearMotorEditor::setActiveMode( bool state )
{
	if(state)
	{
		editModeButton->setColor(0,255,0);
		PhysicsEditor::getSingleton()->hud.setFieldValue((uint32_t)PhysicsEditorHud::MODE_HUD, "Mode=Linear motor");
		PhysicsEditor::getSingleton()->hud.setFieldValue((uint32_t)PhysicsEditorHud::HELPER_HUD, "LMB - Set motor\nRMB - Select nearest\nESC - Cancel");
	}
	else
	{
		editModeButton->setColor(255,0,0);
	}
}

void LinearMotorEditor::editModeClick( Widget* sender, WidgetEvent action, void* data )
{
	PhysicsEditor::getSingleton()->setEditorMode(this);
}

void LinearMotorEditor::createMotorClick( Widget* sender, WidgetEvent action, void* data )
{
	PhysicsManager* physicManager = PhysicsManager::getSingleton();
	physicManager->constraintDef.constraintType = JA_LINEAR_MOTOR2D;
	physicManager->createConstraintDef();
	
}

void LinearMotorEditor::deleteMotorClick( Widget* sender, WidgetEvent action, void* data )
{

}

DistanceJointEditor::DistanceJointEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Distance joint", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(220, 7 * (20 + 1));
	fillBoxLayout(boxLayout);

	DisplayManager* display = DisplayManager::getSingleton();
	infoPosX = (float)(display->getResolutionWidth(0.2f));
	infoPosY = (float)(display->getResolutionHeight(0.2f));

	FlowLayout* editLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	editLayout->setSize(220, 20);
	fillFlowLayout(editLayout);
	editModeButton = new Button(0, nullptr);
	editModeButton->setFont("default.ttf",10);
	editModeButton->setColor(0, 0, 255, 128);
	editModeButton->setFontColor(255, 255, 255, 255);
	editModeButton->setText("Edit mode");
	editModeButton->adjustToText(10, 20);
	editModeButton->setOnClickEvent(this, &DistanceJointEditor::editModeClick);
	createMotorButton = new Button(1, nullptr);
	fillButton(createMotorButton, "Create");
	createMotorButton->setOnClickEvent(this, &DistanceJointEditor::createJointClick);

	editLayout->addWidget(editModeButton);
	editLayout->addWidget(createMotorButton);

	boxLayout->addWidget(editLayout);

	addWidgetToEditor(boxLayout);
}

void DistanceJointEditor::render()
{
	std::list<ja::DistanceJoint2d*>::iterator it = distanceJoints.begin();
	jaVector2 worldAnchorPointA;
	jaVector2 worldAnchorPointB;

	if (this->activeDistanceJoint != nullptr)
	{
		jaVector2 worldPointA;
		jaVector2 worldPointB;
		activeDistanceJoint->getWorldAnchorPoints(worldPointA, worldPointB);
		DisplayManager::setColorUb(0, 255, 0, 255);
		DisplayManager::setLineWidth(2.0f);
		DisplayManager::drawLine(worldPointA.x, worldPointA.y, worldPointB.x, worldPointB.y);

		if (this->anchorPointEdited)
		{
			DisplayManager::setColorUb(0, 255, 0, 255);
			DisplayManager::setLineWidth(2.0f);
			DisplayManager::drawLine(anchorPointA.x, anchorPointA.y, anchorPointB.x, anchorPointB.y);
		}
	}
	
	for (; it != distanceJoints.end(); ++it)
	{
		(*it)->getWorldAnchorPoints(worldAnchorPointA, worldAnchorPointB);

		DisplayManager::setColorUb(255, 0, 0, 255);
		DisplayManager::setPointSize(7);
		DisplayManager::drawPoint(worldAnchorPointA.x, worldAnchorPointA.y);
		DisplayManager::drawPoint(worldAnchorPointB.x, worldAnchorPointB.y);
		
		DisplayManager::setColorUb(255, 255, 255, 255);
		DisplayManager::setPointSize(3);
		DisplayManager::drawPoint(worldAnchorPointA.x, worldAnchorPointA.y);
		DisplayManager::drawPoint(worldAnchorPointB.x, worldAnchorPointB.y);
	}

	
}

void DistanceJointEditor::handleInput()
{
	if (InputManager::isKeyPressed(JA_ESCAPE))
	{
		PhysicsEditor::getSingleton()->setEditorMode(PhysicsEditorMode::SELECT_MODE);
	}

	if (InputManager::isKeyPressed(JA_d))
	{
		if (this->activeDistanceJoint != nullptr)
		{
			this->distanceJoints.remove(this->activeDistanceJoint);
			ja::PhysicsManager::getSingleton()->removeJoint(this->activeDistanceJoint);

			this->activeDistanceJoint = nullptr;
		}
	}

	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

	if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
	{
		std::list<ja::DistanceJoint2d*>::iterator it = distanceJoints.begin();
		float zoomFactor = 1.0f / ja::GameManager::getSingleton()->getActiveCamera()->scale.x;
		float anchorCircle = zoomFactor * (7.0f / (float)ja::setup::graphics::PIXELS_PER_UNIT);
		
		activeDistanceJoint = nullptr;

		for (; it != distanceJoints.end(); ++it)
		{
			(*it)->getWorldAnchorPoints(anchorPointA, anchorPointB);
			jaVector2 anchorA = drawPoint - anchorPointA;
			jaVector2 anchorB = drawPoint - anchorPointB;
			float radiusLength = jaVectormath2::length(anchorA);

			if (radiusLength < anchorCircle)
			{
				this->editedAnchorPointId = 0;
				this->activeDistanceJoint = (*it);
				break;
			}

			radiusLength = jaVectormath2::length(anchorB);

			if (radiusLength < anchorCircle)
			{
				this->editedAnchorPointId = 1;
				this->activeDistanceJoint = (*it);
				break;
			}
		}
	}

	if (ja::InputManager::isMouseButtonPress((uint8_t)MouseButton::BUTTON_LEFT))
	{
		this->anchorPointEdited = true; 
		if (this->editedAnchorPointId == 0)
		{
			anchorPointA = drawPoint;
		}
		else {
			anchorPointB = drawPoint;
		}
	}

	if (ja::InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
	{
		this->anchorPointEdited = false;
		if (this->activeDistanceJoint != nullptr)
		{
			activeDistanceJoint->setConstraint(anchorPointA, anchorPointB);
		}
	}
}

void DistanceJointEditor::setActiveMode(bool state)
{
	if (state)
	{
		editModeButton->setColor(0, 255, 0);
		PhysicsEditor::getSingleton()->hud.setFieldValue((uint32_t)PhysicsEditorHud::MODE_HUD, "Mode=Distance joint");
		PhysicsEditor::getSingleton()->hud.setFieldValue((uint32_t)PhysicsEditorHud::HELPER_HUD, "LMB - Select joint\nd - Delete joint\nESC - Cancel");
	}
	else
	{
		editModeButton->setColor(255, 0, 0);
	}
}

void DistanceJointEditor::editModeClick(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicManager = PhysicsManager::getSingleton();
	std::list<PhysicObject2d*> selectedPhysicsObjects = PhysicsEditor::getSingleton()->selectedPhysicObjects;

	if (selectedPhysicsObjects.size() == 0)
	{
		Toolset::getSingleton()->windowHud->addInfoC((uint16_t)PhysicsEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "Select physic bodies first");
		return;
	}

	PhysicObject2d* physicObjectA;

	this->distanceJoints.clear();
	std::list<PhysicObject2d*>::iterator itSelected = selectedPhysicsObjects.begin();

	for (itSelected; itSelected != selectedPhysicsObjects.end(); ++itSelected)
	{
		physicObjectA = *itSelected;

		ja::Constraint2d* constraint = PhysicsManager::getSingleton()->getConstraints(physicObjectA);

		for (; constraint != nullptr; constraint = constraint->getNext(physicObjectA))
		{
			if (constraint->getConstraintType() == JA_DISTANCE_JOINT2D)
			{
				std::list<ja::DistanceJoint2d*>::iterator it = distanceJoints.begin();
				bool isAlreadyExists = false;

				for (; it != distanceJoints.end(); ++it)
				{
					if ((*it) == constraint)
					{
						isAlreadyExists = true;
						break;
					}
				}

				if (!isAlreadyExists)
				{
					distanceJoints.push_back((ja::DistanceJoint2d*)constraint);
				}
			}
		}
	}

	if (distanceJoints.size() == 0)
	{
		Toolset::getSingleton()->windowHud->addInfoC((uint16_t)PhysicsEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "There is no distance joints in selected bodies");
		return;
	}

	PhysicsEditor::getSingleton()->setEditorMode(this);
}

void DistanceJointEditor::createJointClick(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicManager = PhysicsManager::getSingleton();
	std::list<PhysicObject2d*> selectedPhysicsObjects = PhysicsEditor::getSingleton()->selectedPhysicObjects;

	PhysicObject2d* physicObjectA;
	PhysicObject2d* physicObjectB;

	std::list<PhysicObject2d*>::iterator itSelected = selectedPhysicsObjects.begin();

	for (itSelected; itSelected != selectedPhysicsObjects.end(); ++itSelected)
	{
		physicObjectA = *itSelected;
		std::list<PhysicObject2d*>::iterator itSelected2 = itSelected;
		itSelected2 = itSelected;

		for (++itSelected2; itSelected2 != selectedPhysicsObjects.end(); ++itSelected2)
		{
			physicObjectB = *itSelected2;

			physicManager->constraintDef.constraintType = JA_DISTANCE_JOINT2D;
			physicManager->constraintDef.objectA =		  physicObjectA;
			physicManager->constraintDef.objectB =		  physicObjectB;
			physicManager->constraintDef.relaxationBias = setup::physics::DISTANCE_JOINT2D_RELAXATION_BIAS;
			physicManager->constraintDef.anchorPointA =	  physicObjectA->getShapeHandle()->getTransform().getPosition();
			physicManager->constraintDef.anchorPointB =	  physicObjectB->getShapeHandle()->getTransform().getPosition();

			ja::DistanceJoint2d* distanceJoint = (ja::DistanceJoint2d*)physicManager->createConstraintDef();
			if (distanceJoint == nullptr)
			{
				Toolset::getSingleton()->windowHud->addInfoC((uint16_t)PhysicsEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "You need one dynamic body to create joint");
			}
			this->distanceJoints.push_back(distanceJoint);
		}
	}
}

CollisionMaskEditor::CollisionMaskEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Collision mask", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(220, 7 * (20 + 1));
	fillBoxLayout(boxLayout);

	DisplayManager* display = DisplayManager::getSingleton();
	infoPosX = (float)(display->getResolutionWidth(0.2f));
	infoPosY = (float)(display->getResolutionHeight(0.2f));

	FlowLayout* editLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	editLayout->setSize(220, 20);
	fillFlowLayout(editLayout);
	
	boxLayout->addWidget(editLayout);

	addWidgetToEditor(boxLayout);
}

void CollisionMaskEditor::render()
{
	
}

void CollisionMaskEditor::handleInput()
{
	
}

void CollisionMaskEditor::setActiveMode(bool state)
{
	
}

ShapeAddMenu::ShapeAddMenu(uint32_t widgetId, Widget* parent) : GridLayout(widgetId, parent, 1, 4, 0, 1)
{
	setSelectable(true);
	setSize(80, 5 * (20 + 1));

	circleButton = new Button((uint32_t)ShapeType::Circle, nullptr);
	fillButton(circleButton, "Circle");
	circleButton->setOnClickEvent(this, &ShapeAddMenu::createShapeClick);
	addWidget(circleButton, 0, 0);

	boxButton = new Button((uint32_t)ShapeType::Box, nullptr);
	fillButton(boxButton, "Box");
	boxButton->setOnClickEvent(this, &ShapeAddMenu::createShapeClick);
	addWidget(boxButton, 0, 1);

	polygonButton = new Button((uint32_t)ShapeType::ConvexPolygon, nullptr);
	fillButton(polygonButton, "Convex\nPolygon");
	polygonButton->setOnClickEvent(this, &ShapeAddMenu::createShapeClick);
	addWidget(polygonButton, 0, 2);

	pointButton = new Button((uint32_t)ShapeType::Point, nullptr);
	fillButton(pointButton, "Point");
	pointButton->setOnClickEvent(this, &ShapeAddMenu::createShapeClick);
	addWidget(pointButton, 0, 3);
}

ShapeAddMenu::~ShapeAddMenu()
{

}

void ShapeAddMenu::createShapeClick(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsEditor* pPhysicsEditor = PhysicsEditor::getSingleton();
	pPhysicsEditor->setShape((ShapeType)sender->getId());

	PhysicsManager::getSingleton()->physicObjectDef.transform.position = pPhysicsEditor->getGizmoPosition();
	pPhysicsEditor->createBody();
}

PhysicsSpaceBarMenu::PhysicsSpaceBarMenu(uint32_t widgetId, Widget* parent) : GridLayout(widgetId, parent, 1, 1, 0, 1)
{
	setSize(80, ((1 + 20) * 1));

	addShapeButton = new Button(0, nullptr);
	fillButton(addShapeButton, "Add");
	
	ShapeAddMenu* addShapeMenu  = new ShapeAddMenu(0, nullptr);
	SlideWidget*  addShapeSlide = new SlideWidget(1, addShapeButton, addShapeMenu, nullptr);
	addShapeSlide->setAlign((uint32_t)Margin::TOP | (uint32_t)Margin::RIGHT);

	addWidget(addShapeSlide, 0, 0);
}

PhysicsSpaceBarMenu::~PhysicsSpaceBarMenu()
{

}

void PhysicsSpaceBarMenu::hide()
{
	this->setActive(false);
	this->setRender(false);
}

void PhysicsSpaceBarMenu::show()
{
	this->setActive(true);
	this->setRender(true);

	UIManager::setSelected(this);
}

void PhysicsSpaceBarMenu::update()
{
	Widget* lastSelected = UIManager::getSingleton()->getLastSelected();

	bool isSelected = false;

	if (lastSelected != nullptr)
	{
		Widget* itWidget = lastSelected;

		for (Widget* itWidget = lastSelected; itWidget != nullptr; itWidget = itWidget->getParent())
		{
			if (itWidget == this)
			{
				isSelected = true;
				break;
			}
		}
	} 

	if (isSelected)
	{
		GridLayout::update();

		Widget* lastClicked = UIManager::getLastClicked();
		for (Widget* itWidget = lastClicked; itWidget != nullptr; itWidget = itWidget->getParent())
		{
			if (itWidget == this)
			{
				hide();
			}
		}
	}
	else {
		hide();
	}
}

}