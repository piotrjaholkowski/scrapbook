#include "ScriptConsole.hpp"
#include "../../Engine/Manager/ScriptManager.hpp"
#include "MainMenu.hpp"
#include "WidgetId.hpp"

using namespace tr;

ScriptConsole::ScriptConsole() : Menu((uint32_t)TR_MENU_ID::SCRIPTCONSOLE, nullptr)
{
	int32_t widthR, heightR;
	DisplayManager::getResolution(&widthR,&heightR);
	setText("Console line");
	setPosition(0,heightR/3);

	consoleLine = new Editbox((uint32_t)TR_SCRIPTCONSOLE_ID::CONSOLELINE, this);
	consoleLine->setFont("lucon.ttf",12);
	consoleLine->setFontColor(255,0,0,255);
	consoleLine->setColor(255,255,0,128);
	consoleLine->setTextSize(80,15);
	consoleLine->setPosition(0,0);
	addWidget(consoleLine);
}

ScriptConsole::~ScriptConsole()
{
	
}

void ScriptConsole::init()
{
	if(singleton==nullptr)
	{
		singleton = new ScriptConsole();
	}
}

ScriptConsole* ScriptConsole::getSingleton()
{
	return singleton;
}

void ScriptConsole::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
	}
}

void ScriptConsole::activeConsole(bool active)
{
	if(active)
	{
		//jaInputManager::setUnicode(true);
		UIManager::setSelected(consoleLine);
	}
	else
	{
		//jaInputManager::setUnicode(false);
		UIManager::setUnselected();
	}
}

void ScriptConsole::update()
{
	Interface::update();

	if(InputManager::isKeyPressed(JA_RETURN))
	{ 		
		lastCommand = consoleLine->getText();
		ScriptManager::getSingleton()->runScript(lastCommand);
		consoleLine->clearText();
	}

	if(InputManager::isKeyPress(JA_LSHIFT))
	{
		if(InputManager::isKeyPressed(JA_UP))
		{
			consoleLine->setText(lastCommand.c_str());
		}
	}


	if(InputManager::isKeyPressed(JA_ESCAPE))
	{
		MainMenu::getSingleton()->setGameState(GameState::MAINMENU_MODE);
	}
}

void ScriptConsole::draw()
{
	Interface::draw();
}

ScriptConsole* ScriptConsole::singleton = nullptr;
	