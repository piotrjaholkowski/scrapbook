#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../../Engine/Sounds/SoundEmitter.hpp"
#include "../../Engine/Gui/Menu.hpp"
#include "../../Engine/Gui/Timer.hpp"
#include "../../Engine/Gui/Chart.hpp"
#include "../../Engine/Gui/TestHud.hpp"
#include "../../Engine/Gui/InfoFade.hpp"
#include "../../Engine/Gui/LogFade.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"
#include "../../Engine/Math/VectorMath2d.hpp"
//#include "../../Engine/Gui/jaButton.h"

namespace tr
{

	typedef enum {
		TR_HUD_CAMERAPOSITION = 0,
		TR_HUD_MOUSEWINDOW = 1,
		TR_HUD_MOUSEGUI = 2,
		TR_HUD_CAMERANAME = 3,
		TR_HUD_GAMESTATE = 4,
		TR_HUD_MOUSEWORLD = 5,
		TR_HUD_CAMERASCALE = 6
	} FieldTestHud;

	typedef enum {
		TR_GRID_256 = 256,
		TR_GRID_128 = 128,
		TR_GRID_64 = 64,
		TR_GRID_32 = 32,
		TR_GRID_16 = 16,
		TR_GRID_8 = 8
	} GridDetail;

	enum class RulerAlign {
		ALIGN_X = 1,
		ALIGN_Y = 2,
		NO_ALIGN = 3

	};

	enum class ScaleMode
	{
		SCALE_X  = 0,
		SCALE_Y  = 1,
		SCALE_XY = 2
	};

	class ToolsetRuler
	{
	public:
		RulerAlign   alignTo;
		jaFloat		 angleDeg;
		jaTransform2 transform;
		bool activeRuler;

		ToolsetRuler();

		void setActive(bool active);
		bool isActive();
		void setAlign(RulerAlign allign);
		void setAngle(jaFloat angleDeg);
		void setPosition(jaFloat x, jaFloat y);
		void draw(gil::AABB2d& cameraView);
		jaVector2 transformPoint(jaVector2& point);
	};

	class Toolset : public ja::Menu
	{
	private:
		bool IsGridDrawed;
		bool IsTestHudDrawed;
		bool IsGridAnchor;

		//gui
		ja::Chart* spectrumChart;
		ja::Label* deviceNamesLabel;
		ja::Label* activeDeviceLabel;
		ja::Timer* deviceInfoFadeTimer;
		//gui

		ja::TestHud*    testHud;
		GridDetail      gridDetail;
		static Toolset* singleton;
		jaVector2       drawPoint;
		ToolsetRuler    ruler;

		//
		jaVector2 mouseWorldPosition;
		jaVector2 anchorPoint;
		jaVector2 anchorCamera;
		jaFloat   zoomFactor;
	public:
		//gui sound
		ja::SoundEmitter* selectButtonSound;
		//gui sound
		ja::InfoFade* windowHud;
		ja::InfoFade* worldHud;
		ja::LogFade*  infoHud;

		float worldScaleMultiplier;
		float maxWorldScale;
		float minWorldScale;

		Toolset();
		static void     init();
		static Toolset* getSingleton();
		static void     cleanUp();
		void update();
		void draw();

		void updateCamera(ja::CameraInterface* camera);

		// Toolset ruler
		void setRulerActive(bool state);
		bool isRulerActive();
		ToolsetRuler* getRuler();
		// Toolset ruler

		// Toolset function
		inline void setDrawGrid(bool state);
		inline bool isGridDrawed();
		inline void setGridDetail(GridDetail detail);

		inline void setDrawTestHud(bool state);
		inline bool isTestHudDrawed();

		inline void setGridAnchor(bool state);

		void updateDrawPoint();
		jaVector2 getDrawPoint();

		void drawGrid(ja::CameraInterface* camera);
		void drawDrawPoint();

		//gui
		void onTimer(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onBeginDrawWorldHud(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onEndDrawWorldHud(ja::Widget* sender, ja::WidgetEvent action, void* data);
		//gui

		//gui sound
		void playSound();
		//gui sound

		~Toolset();
	};

	void Toolset::setDrawGrid(bool state)
	{
		IsGridDrawed = state;
	}

	bool Toolset::isGridDrawed()
	{
		return IsGridDrawed;
	}

	void Toolset::setGridDetail(GridDetail detail)
	{
		gridDetail = detail;
	}

	void Toolset::setDrawTestHud(bool state)
	{
		IsTestHudDrawed = state;
	}

	bool Toolset::isTestHudDrawed()
	{
		return IsTestHudDrawed;
	}


}
