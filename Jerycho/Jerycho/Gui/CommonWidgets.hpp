#pragma once

#include "../../Engine/Gui/Interface.hpp"
//#include "../../Engine/Gui/Hider.hpp"
//#include "../../Engine/Gui/Magnet.hpp"
#include "../../Engine/Gui/Label.hpp"


#include "../../Engine/Scripts/Script.hpp"
#include "../../Engine/Manager/ResourceManager.hpp"
#include "../../Engine/Sounds/SoundEmitter.hpp"
#include "../../Engine/Math/VectorMath2d.hpp"

namespace ja
{
	class SequenceEditor;
	class SequenceMarker;
	class TextField;
	class GridViewer;
	class Button;
	class Scrollbar;
	class Checkbox;
	class RadioButton;
	class SlideWidget;
	class Hider;
	class Magnet;
	class ImageButton;
	class Editbox;
	class FlowLayout;
	class ScrollPane;
	class ProgressButton;
	class GridLayout;
	class BoxLayout;
	class FlowLayout;
}

namespace tr
{
	void fillButton(ja::Button* button, const char* text);

	class CommonWidget : public ja::Interface
	{
	protected:
		int32_t dockPositionX;
		int32_t dockPositionY;
		ja::Hider* hider;
		ja::Magnet* magnet;
		ja::Label* editorTitle;
		ja::ImageButton* closeButton;

		ja::WidgetCallback* notifier;
	public:
		CommonWidget(uint32_t widgetId, const char* interfaceName, ja::Widget* parent);
		void setNotifier(ja::WidgetCallback* notifier);
		void addWidgetToEditor(ja::Widget* widget);
		void fillLabel(ja::Label* label, const char* text);
		void fillButton(ja::Button* button, const char* text);
		void fillScrollPane(ja::ScrollPane* scrollPane);
		void fillCheckbox(ja::Checkbox* checkbox);
		void fillRadioButton(ja::RadioButton* radiobutton, const char* text);
		void fillEditbox(ja::Editbox* editbox);
		void fillTextField(ja::TextField* textField);
		void fillProgressButton(ja::ProgressButton* button);
		void fillScrollbar(ja::Scrollbar* scrollbar, double lowestValue, double biggestValue);
		void fillGridLayout(ja::GridLayout* gridLayout);
		void fillBoxLayout(ja::BoxLayout* boxLayout);
		void fillFlowLayout(ja::FlowLayout* flowLayout);
		void fillGridViewer(ja::GridViewer* gridViewer);
		void fillSequenceEditor(ja::SequenceEditor* sequenceEditor);

		virtual void updateWidget(void* data)
		{

		}

		virtual void onCreateNotify(void* data)
		{

		}

		virtual void onDeleteNotify(void* data)
		{

		}

		virtual void handleInput()
		{

		}

		virtual void reloadResources()
		{

		}

		virtual void render() // render contest to editor
		{

		}

		virtual void setActiveMode(bool state) // caled when component editor becomes active
		{

		}

		void onMouseOver(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onMouseLeave(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCloseClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		~CommonWidget();
	};

	class CommonPopup : public ja::Interface
	{
	protected:
		ja::GridLayout* popup;

	public:
		CommonPopup(uint32_t widgetId, ja::Widget* parent);
		void show();
		void hide();
		void update();
		void setEnable(ja::Button* button);
		void setDisable(ja::Button* button);
		void fillLabel(ja::Label* label, const char* text);
		void fillButton(ja::Button* button, const char* text);
		void fillCheckbox(ja::Checkbox* checkbox);
		void fillRadioButton(ja::RadioButton* radiobutton, const char* text);
		void fillEditbox(ja::Editbox* editbox);
		void fillTextField(ja::TextField* textField);
		void fillProgressButton(ja::ProgressButton* button);
		void fillScrollbar(ja::Scrollbar* scrollbar, double lowestValue, double biggestValue);
		void fillGridLayout(ja::GridLayout* gridLayout);
		void fillBoxLayout(ja::BoxLayout* boxLayout);
		void fillFlowLayout(ja::FlowLayout* flowLayout);
		void fillGridViewer(ja::GridViewer* gridViewer);
		void fillSequenceEditor(ja::SequenceEditor* sequenceEditor);

		void onMouseOver(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onMouseLeave(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCloseClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		~CommonPopup();
	};

	class MeshEditor : public CommonWidget
	{
	private:

		ja::TextField* xTextfield;
		ja::TextField* yTextfield;

		//LayerObjectEditorMode        editorMode;
		StackAllocator<jaVector2>  vertices;
		StackAllocator<jaVector2>  verticesTransformedToLocal;
		StackAllocator<jaVector2>  savedVertices;
		StackAllocator<bool>       selectedVertices;
		
		//StackAllocator<ColorVertex*> faceVertices;
		StackAllocator<uint16_t>     savedTriangleIndices;
		StackAllocator<uint16_t>     triangleIndices;
		StackAllocator<uint16_t>     recalculatedTriangleIndices;

	public:
		MeshEditor(uint32_t widgetId, Widget* parent);
		~MeshEditor();
	};

	class ScriptFunctionViewer : public CommonWidget
	{
	private:
		static ScriptFunctionViewer* singleton;

		ja::Label functionLabel;

		ja::GridViewer*    functionsViewer;
		std::vector<void*> viewedFunctions;
		//StackAllocator<ja::ScriptFunction> scriptFunctions;
		ja::ScriptFunctionType lastFunctionType;
	public:
		ja::ScriptFunction* chosenFunction;

		ScriptFunctionViewer(uint32_t widgetId, ja::Widget* parent);
		~ScriptFunctionViewer();

		static inline ScriptFunctionViewer* getSingleton()
		{
			return singleton;
		}

		void refresh(ja::ScriptFunctionType functionType);

		inline void reloadResources()
		{
			refresh(lastFunctionType);
		}

		void onFunctionDraw(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onFunctionClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onClose(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class DataModelViewer : public CommonWidget
	{
	private:
		static DataModelViewer* singleton;

		ja::Label dataModelLabel;

		ja::GridViewer*    dataModelsViewer;
		std::vector<void*> dataModels;

	public:
		ja::DataModel* chosenDataModel;

		DataModelViewer(uint32_t widgetId, ja::Widget* parent);
		~DataModelViewer();

		static inline DataModelViewer* getSingleton()
		{
			return singleton;
		}

		void refresh();

		inline void reloadResources()
		{
			refresh();
		}

		void onDataModelDraw(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDataModelClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onClose(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class SoundResources : public CommonWidget
	{
	private:
		std::vector<void*> sounds;
		std::vector<void*> streams;

		ja::GridViewer* soundViewer;
		ja::Font*       font;
		ja::Label       soundDescriptionLabel;
		ja::Button*     soundTypeButton;

		ja::SoundEmitter*       soundPlayer;
	public:
		ja::SoundEffect* chosenSound;
		ja::SoundStream* chosenStream;

		SoundResources(uint32_t widgetId, ja::Widget* parent);

		void reloadResources();

		void onSoundOver(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onSoundLeave(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onSoundDraw(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onGroupDraw(ja::Widget* sender, ja::WidgetEvent action, void* data);

		void onSoundClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onSoundTypeClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onClose(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

}
