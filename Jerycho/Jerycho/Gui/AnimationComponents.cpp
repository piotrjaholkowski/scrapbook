#include "AnimationComponents.hpp"
#include "AnimationEditor.hpp"
#include "Toolset.hpp"

#include "../../Engine/Manager/AnimationManager.hpp"
#include "../../Engine/Manager/UIManager.hpp"
#include "../../Engine/Gilgamesh/DebugDraw.hpp"

#include "../../Engine/Gui/Margin.hpp"
#include "../../Engine/Gui/Editbox.hpp"
#include "../../Engine/Gui/ProgressButton.hpp"
#include "../../Engine/Gui/GridLayout.hpp"
#include "../../Engine/Gui/FlowLayout.hpp"
#include "../../Engine/Gui/BoxLayout.hpp"
#include "../../Engine/Gui/TextField.hpp"
#include "../../Engine/Gui/BoxLayout.hpp"
#include "../../Engine/Gui/Scrollbar.hpp"
#include "../../Engine/Gui/Checkbox.hpp"
#include "../../Engine/Gui/RadioButton.hpp"
#include "../../Engine/Gui/GridViewer.hpp"
#include "../../Engine/Gui/SlideWidget.hpp"
#include "../../Engine/Gui/SequenceEditor.hpp"
#include "../../Engine/Gui/ScrollPane.hpp"
#include "../../Engine/Gui/Hider.hpp"
#include "../../Engine/Gui/Magnet.hpp"

using namespace ja;

namespace tr
{
	AnimationComponentMenu*  AnimationComponentMenu::singleton  = nullptr;
	AnimationSkeletonEditor* AnimationSkeletonEditor::singleton = nullptr;
	AnimationBoneEditor*     AnimationBoneEditor::singleton     = nullptr;
	AnimationSequenceEditor* AnimationSequenceEditor::singleton = nullptr;

	AnimationComponentMenu::AnimationComponentMenu(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Menu", parent)
	{
		singleton = this;

		BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 1);
		boxLayout->setSize(150, 250);
		fillBoxLayout(boxLayout);

		Button* skeletonButton = new Button((int32_t)TR_ANIMATIONEDITOR_ID::SKELETON_EDITOR, nullptr);
		fillButton(skeletonButton, "Skeleton");
		skeletonButton->setOnClickEvent(this, &AnimationComponentMenu::onMenuClick);
		boxLayout->addWidget(skeletonButton);

		Button* boneButton = new Button((int32_t)TR_ANIMATIONEDITOR_ID::BONE_EDITOR, nullptr);
		fillButton(boneButton, "Bone");
		boneButton->setOnClickEvent(this, &AnimationComponentMenu::onMenuClick);
		boxLayout->addWidget(boneButton);

		Button* animationSequenceButton = new Button((int32_t)TR_ANIMATIONEDITOR_ID::ANIMATION_SEQUENCE_EDITOR, nullptr);
		fillButton(animationSequenceButton, "Animation");
		animationSequenceButton->setOnClickEvent(this, &AnimationComponentMenu::onMenuClick);
		boxLayout->addWidget(animationSequenceButton);

		components[(int32_t)TR_ANIMATIONEDITOR_ID::SKELETON_EDITOR] = new AnimationSkeletonEditor((uint32_t)TR_ANIMATIONEDITOR_ID::SKELETON_EDITOR, this);
		addWidget(components[(int32_t)TR_ANIMATIONEDITOR_ID::SKELETON_EDITOR]);
		components[(int32_t)TR_ANIMATIONEDITOR_ID::BONE_EDITOR] = new AnimationBoneEditor((uint32_t)TR_ANIMATIONEDITOR_ID::BONE_EDITOR, this);
		addWidget(components[(int32_t)TR_ANIMATIONEDITOR_ID::BONE_EDITOR]);
		components[(int32_t)TR_ANIMATIONEDITOR_ID::ANIMATION_SEQUENCE_EDITOR] = new AnimationSequenceEditor((uint32_t)TR_ANIMATIONEDITOR_ID::ANIMATION_SEQUENCE_EDITOR, this);
		addWidget(components[(int32_t)TR_ANIMATIONEDITOR_ID::ANIMATION_SEQUENCE_EDITOR]);

		addWidgetToEditor(boxLayout);
	}

	void AnimationComponentMenu::onMenuClick(Widget* sender, WidgetEvent action, void* data)
	{
		uint32_t id = sender->getId();
		components[id]->setActive(true);
		components[id]->setRender(true);
	}

	void AnimationComponentMenu::updateComponentEditors(void* skeleton)
	{
		components[(int32_t)TR_ANIMATIONEDITOR_ID::SKELETON_EDITOR]->updateWidget(skeleton);
		components[(int32_t)TR_ANIMATIONEDITOR_ID::BONE_EDITOR]->updateWidget(skeleton);
		components[(int32_t)TR_ANIMATIONEDITOR_ID::ANIMATION_SEQUENCE_EDITOR]->updateWidget(skeleton);

		//components[(int32_t)TR_POLYEDITOR_ID::BOX_EDITOR]->updateWidget(polygon);
		//components[(int32_t)TR_POLYEDITOR_ID::CIRCLE_EDITOR]->updateWidget(polygon);
	}

	AnimationSequenceEditor::AnimationSequenceEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Animation", parent)
	{
		singleton = this;
		setRender(false);
		setActive(false);

		BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
		const uint32_t widgetWidth = 400;
		boxLayout->setSize(widgetWidth, (200 + 2) +  3 *(28 + 2));
		fillBoxLayout(boxLayout);

		DisplayManager* display = DisplayManager::getSingleton();
		infoPosX = (float)(display->getResolutionWidth(0.2f));
		infoPosY = (float)(display->getResolutionHeight(0.2f));

		{
			FlowLayout* nameLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			nameLayout->setSize(widgetWidth, 28);
			fillFlowLayout(nameLayout);

			Label* nameLabel = new Label(0, nullptr);
			fillLabel(nameLabel, "Name");
			nameLayout->addWidget(nameLabel);

			this->newAnimationNameTextfield = new TextField(1, nullptr);
			fillTextField(this->newAnimationNameTextfield);
			this->newAnimationNameTextfield->setTextSize(setup::TAG_NAME_LENGTH, 28);
			//this->animationNameTextfield->setOnDeselectEvent(this, &AnimationSequenceEditor::nameDeselect);
			nameLayout->addWidget(this->newAnimationNameTextfield);

			this->createAnimationButton = new Button(2, nullptr);
			fillButton(this->createAnimationButton, "Create");
			this->createAnimationButton->setOnClickEvent(this, &AnimationSequenceEditor::onCreateAnimationClick);
			nameLayout->addWidget(this->createAnimationButton);

			this->animationsNumLabel = new Label(3, nullptr);
			fillLabel(this->animationsNumLabel, "Animations:");
			nameLayout->addWidget(this->animationsNumLabel);

			boxLayout->addWidget(nameLayout);
		}

		{
			this->animationGridViewer = new GridViewer(3, 3, 3, nullptr);
			this->animationGridViewer->setSize(widgetWidth, 100);
			fillGridViewer(this->animationGridViewer);
			this->animationGridViewer->setOnDrawEvent(this,  &AnimationSequenceEditor::onDrawAnimation);
			this->animationGridViewer->setOnClickEvent(this, &AnimationSequenceEditor::onPickAnimation);
			this->animationGridViewer->setGridList(&animations);

			boxLayout->addWidget(this->animationGridViewer);
		}

		{
			FlowLayout* animationLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			animationLayout->setSize(widgetWidth, 28);
			fillFlowLayout(animationLayout);

			Label* nameLabel = new Label(0, nullptr);
			fillLabel(nameLabel, "Name");
			animationLayout->addWidget(nameLabel);

			this->animationTextfield = new TextField(1, nullptr);
			fillTextField(this->animationTextfield);
			this->animationTextfield->setTextSize(setup::TAG_NAME_LENGTH, 28);
			this->animationTextfield->setOnDeselectEvent(this, &AnimationSequenceEditor::onNameDeselect);
			animationLayout->addWidget(this->animationTextfield);

			this->deleteAnimationButton = new Button(2, nullptr);
			fillButton(this->deleteAnimationButton, "Delete");
			this->deleteAnimationButton->setOnClickEvent(this, &AnimationSequenceEditor::onDeleteAnimationClick);
			animationLayout->addWidget(this->deleteAnimationButton);

			this->animationTextfield->setActive(false);
			this->deleteAnimationButton->setActive(false);

			boxLayout->addWidget(animationLayout);
		}

		{
			FlowLayout* timeLayout = new FlowLayout(5, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			timeLayout->setSize(widgetWidth, 28);
			fillFlowLayout(timeLayout);

			Label* timeLabel = new Label(0, nullptr);
			fillLabel(timeLabel, "Time");
			timeLayout->addWidget(timeLabel);

			this->timeTextfield = new TextField(1, nullptr);
			fillTextField(this->timeTextfield);
			this->timeTextfield->setAcceptOnlyNumbers(true);
			this->timeTextfield->setTextSize(setup::TAG_NAME_LENGTH, 28);
			this->timeTextfield->setOnDeselectEvent(this, &AnimationSequenceEditor::onTimeDeselect);
			timeLayout->addWidget(this->timeTextfield);
			
			this->keyFramesLabel = new Label(2, nullptr);
			fillLabel(this->keyFramesLabel, "Frames:");
			timeLayout->addWidget(this->keyFramesLabel);

			this->timeTextfield->setActive(false);

			boxLayout->addWidget(timeLayout);
		}

		{
			FlowLayout* sequenceLayout = new FlowLayout(6, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			sequenceLayout->setSize(widgetWidth, 40);
			fillFlowLayout(sequenceLayout);

			this->animationSequenceEditor = new SequenceEditor(0, nullptr);
			this->animationSequenceEditor->setSize(widgetWidth, 40);
			fillSequenceEditor(this->animationSequenceEditor);

			sequenceLayout->addWidget(this->animationSequenceEditor);

			this->animationSequenceEditor->setOnChangeEvent(this, &AnimationSequenceEditor::onMarkerChange);
			this->animationSequenceEditor->setActive(false);

			boxLayout->addWidget(sequenceLayout);
		}

		{
			FlowLayout* positionLayout = new FlowLayout(7, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			positionLayout->setSize(widgetWidth, 28);
			fillFlowLayout(positionLayout);

			Label* positionLabel = new Label(0, nullptr);
			fillLabel(positionLabel, "Position");
			positionLayout->addWidget(positionLabel);

			this->positionTextfield = new TextField(1, nullptr);
			fillTextField(this->positionTextfield);
			this->positionTextfield->setAcceptOnlyNumbers(true);
			this->positionTextfield->setTextSize(16, 28);
			this->positionTextfield->setOnDeselectEvent(this, &AnimationSequenceEditor::onPositionDeselect);
			positionLayout->addWidget(this->positionTextfield);

			Label* timeElapsedLabel = new Label(2, nullptr);
			fillLabel(timeElapsedLabel, "Elapsed Time");
			positionLayout->addWidget(timeElapsedLabel);

			this->elapsedTimeTextfield = new TextField(3, nullptr);
			fillTextField(this->elapsedTimeTextfield);
			this->elapsedTimeTextfield->setAcceptOnlyNumbers(true);
			this->elapsedTimeTextfield->setTextSize(16, 28);
			this->elapsedTimeTextfield->setOnDeselectEvent(this, &AnimationSequenceEditor::onElapsedTimeDeselect);
			positionLayout->addWidget(this->elapsedTimeTextfield);

			this->positionTextfield->setActive(false);
			this->elapsedTimeTextfield->setActive(false);

			boxLayout->addWidget(positionLayout);
		}

		{
			FlowLayout* keyFrameLayout = new FlowLayout(8, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			keyFrameLayout->setSize(widgetWidth, 28);
			fillFlowLayout(keyFrameLayout);

			Label* keyFrameLabel = new Label(0, nullptr);
			fillLabel(keyFrameLabel, "Key Frame:");
			keyFrameLayout->addWidget(keyFrameLabel);

			this->insertKeyFrameButton = new Button(1, nullptr);
			fillButton(this->insertKeyFrameButton, "Insert");
			this->insertKeyFrameButton->setOnClickEvent(this, &AnimationSequenceEditor::onCreateKeyFrameClick);
			keyFrameLayout->addWidget(this->insertKeyFrameButton);

			this->deleteKeyFrameButton = new Button(2, nullptr);
			fillButton(this->deleteKeyFrameButton, "Delete");
			this->deleteKeyFrameButton->setOnClickEvent(this, &AnimationSequenceEditor::onDeleteKeyFrameClick);
			keyFrameLayout->addWidget(this->deleteKeyFrameButton);

			this->insertKeyFrameButton->setActive(false);
			this->deleteKeyFrameButton->setActive(false);

			boxLayout->addWidget(keyFrameLayout);
		}

		{
			animationLabel.setFont("default.ttf",10);
			animationLabel.setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
			animationLabel.setColor(255, 255, 255, 255);
			animationLabel.setText("");
		}

		this->editedSkeleton  = nullptr;
		this->editedAnimation = nullptr;

		setActiveWidgets(false);

		addWidgetToEditor(boxLayout);
	}

	void AnimationSequenceEditor::setActiveWidgets(bool state)
	{
		//this->setActive(state);

		this->createAnimationButton->setActive(state);
		this->newAnimationNameTextfield->setActive(state);

		
	}

	void AnimationSequenceEditor::updateWidget(void* data)
	{
		this->animations.clear();
	
		ja::Skeleton* skeleton = (ja::Skeleton*)data;

		if (this->editedSkeleton != skeleton)
		{
			setAnimationData(nullptr);
		}

		this->editedSkeleton   = skeleton;
			
		if (nullptr == skeleton)
		{	
			this->animationsNumLabel->setText("Animations:");
			setActiveWidgets(false);
			return;
		}

		setActiveWidgets(true);

		refreshAnimationGridView();
	}

	void AnimationSequenceEditor::refreshAnimationGridView()
	{
		this->animations.clear();

		if (nullptr == this->editedSkeleton)
			return;

		SkeletalAnimations* skeletalAnimations = this->editedSkeleton->getSkeletalAnimations();

		if (nullptr != skeletalAnimations)
		{
			uint16_t            animationsNum = skeletalAnimations->getAnimationsNum();
			SkeletonAnimation** animationArray = skeletalAnimations->getSkeletonAnimations();

			this->animationsNumLabel->setTextPrintC("Animations:%d", (int32_t)animationsNum);

			for (uint16_t i = 0; i < animationsNum; ++i)
			{
				this->animations.push_back(animationArray[i]);
			}
		}
		else
		{
			this->animationsNumLabel->setText("Animations:");
		}

		animationGridViewer->setCellColor(255, 0, 0, 128);
	}

	void AnimationSequenceEditor::setAnimationData(SkeletonAnimation* animation)
	{
		this->editedAnimation = animation;

		if (nullptr == animation)
		{
			//this->timeTextfield->setFieldValue("%f", 0.0f);
		
			this->timeTextfield->setText("");
			this->keyFramesLabel->setText("Frames:");
			this->animationTextfield->setText("");

			this->timeTextfield->setActive(false);
			this->animationSequenceEditor->setActive(false);
			this->positionTextfield->setActive(false);
			this->elapsedTimeTextfield->setActive(false);
			this->animationTextfield->setActive(false);
			this->deleteAnimationButton->setActive(false);
			this->deleteKeyFrameButton->setActive(false);
			this->insertKeyFrameButton->setActive(false);
			this->deleteKeyFrameButton->setActive(false);

			return;
		}

		this->timeTextfield->setActive(true);
		this->animationSequenceEditor->setActive(true);
		this->positionTextfield->setActive(true);
		this->elapsedTimeTextfield->setActive(true);
		this->animationTextfield->setActive(true);
		this->deleteAnimationButton->setActive(true);
		this->deleteKeyFrameButton->setActive(true);
		this->insertKeyFrameButton->setActive(true);
		this->deleteKeyFrameButton->setActive(true);

		this->animationTextfield->setText(animation->getName());
		this->timeTextfield->setFieldValue("%f", animation->getAnimationTime());
		this->keyFramesLabel->setTextPrintC("Frames:%d", (int32_t)animation->getKeyFramesNum());

		{
			this->animationSequenceEditor->clearMarkers();
			
			this->playMarker   = this->animationSequenceEditor->createMarker();
			this->insertMarker = this->animationSequenceEditor->createMarker();

			this->playMarker->r = 255;
			this->playMarker->g = 255;
			this->playMarker->b = 255;
			this->playMarker->isMarkerTop = false;
			this->playMarker->markerSize  = this->markerSize;
			this->playMarker->position    = 0.0f;
		
			this->insertMarker->r = 0;
			this->insertMarker->g = 0;
			this->insertMarker->b = 0;
			this->insertMarker->isMarkerTop = false;
			this->insertMarker->markerSize  = this->markerSize;
			this->insertMarker->position = 0.0f;

			uint16_t           keyFramesNum = animation->getKeyFramesNum();
			AnimationKeyFrame* keyFrames    = animation->getKeyFrames();

			for (uint16_t i = 0; i < keyFramesNum; ++i)
			{
				SequenceMarker* keyFrameMarker = this->animationSequenceEditor->createMarker();
				keyFrameMarker->position       = animation->keyFrames[i].normalizedTime;
				keyFrameMarker->isMarkerTop    = true;
				keyFrameMarker->markerSize     = this->markerSize;
				keyFrameMarker->r = 0;
				keyFrameMarker->g = 0;
				keyFrameMarker->b = 0;

				//keyFrameMarker->userData = &keyFrames[i];
			}
		}
	}

	void  AnimationSequenceEditor::onDrawAnimation(Widget* sender, WidgetEvent action, void* data)
	{
		SkeletonAnimation* animation = (SkeletonAnimation*)data;

		int32_t x, y;

		int32_t halfWidth, halfHeight;
		float   fX, fY;

		sender->getPosition(x, y);
		fX = (float)(x);
		fY = (float)(y);

		halfWidth  = (sender->getWidth() / 2);
		halfHeight = (sender->getHeight() / 2);

		Button* button = (Button*)sender;
		
		if ((editedAnimation == animation) && (nullptr != editedAnimation))
		{
			button->setColor(0, 255, 0);
		}
		else
		{
			button->setColor(255, 0, 0);
		}

		animationLabel.x = x + halfWidth;
		animationLabel.y = y + halfHeight;
		animationLabel.setText(animation->getName());
		animationLabel.draw(0, 0);
	}

	void  AnimationSequenceEditor::onPickAnimation(Widget* sender, WidgetEvent action, void* data)
	{
		Button* button = (Button*)sender;
		SkeletonAnimation* animation = (SkeletonAnimation*)button->vUserData;
		this->editedAnimation = animation;

		setAnimationData(this->editedAnimation);
	}

	void AnimationSequenceEditor::onCreateAnimationClick(Widget* sender, WidgetEvent action, void* data)
	{
		if (this->editedSkeleton == nullptr)
			return;

		const char* animationName = this->newAnimationNameTextfield->getTextPtr();

		SkeletalAnimations* skeletalAnimations = this->editedSkeleton->getSkeletalAnimations();

		if (nullptr != skeletalAnimations)
		{
			SkeletonAnimation* animation = skeletalAnimations->getAnimation(animationName);
			if (nullptr != animation)
			{
				return;
			}
		}

		SkeletonAnimation* skeletonAnimation = this->editedSkeleton->createAnimation(animationName);

		this->animations.push_back(skeletonAnimation);

		int32_t animationsNum = this->animations.size();
		this->animationsNumLabel->setTextPrintC("Animations:%d", (int32_t)animationsNum);
	}

	void AnimationSequenceEditor::onDeleteAnimationClick(Widget* sender, WidgetEvent action, void* data)
	{
		SkeletalAnimations* skeletalAnimations = this->editedSkeleton->getSkeletalAnimations();

		if (nullptr != skeletalAnimations)
		{
			assert(skeletalAnimations->removeAnimation(this->editedAnimation));
		}

		refreshAnimationGridView();

		setAnimationData(nullptr);

		this->deleteAnimationButton->setColor(255, 0, 0);
	}

	void AnimationSequenceEditor::onCreateKeyFrameClick(Widget* sender, WidgetEvent action, void* data)
	{
		float normalizedTime = this->insertMarker->position;

		SequenceMarker* newKeyMarker = this->animationSequenceEditor->createMarker();
		newKeyMarker->position = normalizedTime;
		newKeyMarker->r = 0;
		newKeyMarker->g = 0;
		newKeyMarker->b = 0;
		newKeyMarker->isMarkerTop = true;
		newKeyMarker->markerSize  = this->markerSize;

		const Bone* bones = this->editedSkeleton->getBones();

		this->editedAnimation->addKeyFrame(normalizedTime, bones);

		this->keyFramesLabel->setTextPrintC("Frames:%d", this->editedAnimation->getKeyFramesNum());

		//refreshKeyFrameMarkers();
	}

	void AnimationSequenceEditor::onDeleteKeyFrameClick(Widget* sender, WidgetEvent action, void* data)
	{
		SequenceMarker* selectedMarker = this->animationSequenceEditor->getSelectedMarker();

		if (nullptr == selectedMarker)
			return;

		std::vector<SequenceMarker*>& markers = this->animationSequenceEditor->getMarkers();
		//sort and figure which to delete by checking numbers
	}

	void AnimationSequenceEditor::onMarkerChange(Widget* sender, WidgetEvent action, void* data)
	{
		SequenceMarker* changedMarker = (SequenceMarker*)data;

		float normalizedTime = changedMarker->position;

		this->positionTextfield->setFieldValue("%f", normalizedTime);
		
		float time = 0.0f;
		this->timeTextfield->getAsFloat(&time);

		float elapsedTime = time * normalizedTime;

		this->elapsedTimeTextfield->setFieldValue("%f", elapsedTime);

		if (changedMarker == this->playMarker)
		{
			this->editedSkeleton->setSkeletonPose(this->editedAnimation->getNameHashCode(), this->playMarker->position);
		}
		//else
		//{
		//	refreshKeyFrameMarkers();
		//}
	}

	void AnimationSequenceEditor::onNameDeselect(Widget* sender, WidgetEvent action, void* data)
	{
		const char* animationName = this->animationTextfield->getTextPtr();

		SkeletalAnimations* skeletalAnimations = this->editedSkeleton->getSkeletalAnimations();

		if (nullptr != skeletalAnimations)
		{
			SkeletonAnimation* animation = skeletalAnimations->getAnimation(animationName);

			if (nullptr != animation)
			{
				this->animationTextfield->setText(this->editedAnimation->getName());
				return;
			}
		}

		this->editedAnimation->setName(animationName);
	}

	void AnimationSequenceEditor::onTimeDeselect(Widget* sender, WidgetEvent action, void* data)
	{
		float time = 0.0f;
		this->timeTextfield->getAsFloat(&time);

		if (time < 0)
		{
			time = 0.0f;
			this->timeTextfield->setFieldValue("%f", time);
		}

		float position = 0.0f;
		this->positionTextfield->getAsFloat(&position);
		
		float elapsedTime = position * time;
		this->elapsedTimeTextfield->setFieldValue("%f", elapsedTime);

		this->editedAnimation->setAnimationTime(time);
	}

	void AnimationSequenceEditor::onPositionDeselect(Widget* sender, WidgetEvent action, void* data)
	{
		float position = 0.0f;
		this->positionTextfield->getAsFloat(&position);

		if (position > 1.0f)
		{
			position = 1.0f;
			this->positionTextfield->setFieldValue("%f", 1.0f);
		}

		if (position < 0.0f)
		{
			position = 0.0f;
			this->positionTextfield->setFieldValue("%f", 0.0f);
		}

		float time = 0.0f;
		this->timeTextfield->getAsFloat(&time);

		float elapsedTime = position * time;
		this->elapsedTimeTextfield->setFieldValue("%f", elapsedTime);

		SequenceMarker* selectedMarker = this->animationSequenceEditor->getSelectedMarker();
		if (nullptr != selectedMarker)
		{
			selectedMarker->position = position;
		}
	}

	void AnimationSequenceEditor::onElapsedTimeDeselect(Widget* sender, WidgetEvent action, void* data)
	{
		float time = 0.0f;
		this->timeTextfield->getAsFloat(&time);

		float elapsedTime = 0.0f;
		this->elapsedTimeTextfield->getAsFloat(&elapsedTime);

		if (elapsedTime < 0.0f)
		{
			elapsedTime = 0.0f;
			this->elapsedTimeTextfield->setFieldValue("%f", 0.0f);
		}

		if (elapsedTime > time)
		{
			elapsedTime = time;
			this->elapsedTimeTextfield->setFieldValue("%f", time);
		}

		float position = elapsedTime / time;

		this->positionTextfield->setFieldValue("%f", position);

		SequenceMarker* selectedMarker = this->animationSequenceEditor->getSelectedMarker();
		if (nullptr != selectedMarker)
		{
			selectedMarker->position = position;
		}
	}

	AnimationSkeletonEditor::AnimationSkeletonEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Skeleton", parent)
	{
		singleton = this;
		setRender(false);
		setActive(false);

		BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);

		const int32_t widgetWidth = 400;

		boxLayout->setSize(widgetWidth, (100 + 2) + (3 * (28 + 2)));
		fillBoxLayout(boxLayout);

		DisplayManager* display = DisplayManager::getSingleton();
		infoPosX = (float)(display->getResolutionWidth(0.3f));
		infoPosY = (float)(display->getResolutionHeight(0.3f));

		{
			FlowLayout* nameLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			nameLayout->setSize(widgetWidth, 28);
			fillFlowLayout(nameLayout);

			Label* nameLabel = new Label(0, nullptr);
			fillLabel(nameLabel, "Name");
			nameLayout->addWidget(nameLabel);

			this->skeletonNameTextfield = new TextField(1, nullptr);
			fillTextField(this->skeletonNameTextfield);
			this->skeletonNameTextfield->setTextSize(setup::TAG_NAME_LENGTH, 28);
			this->skeletonNameTextfield->setOnDeselectEvent(this, &AnimationSkeletonEditor::onNameDeselect);
			nameLayout->addWidget(this->skeletonNameTextfield);
			this->skeletonNameTextfield->setActive(false);

			this->bonesNumLabel = new Label(0, nullptr);
			fillLabel(this->bonesNumLabel, "Bones:   ");
			nameLayout->addWidget(this->bonesNumLabel);

			boxLayout->addWidget(nameLayout);
		}

		{
			this->boneGridViewer = new GridViewer(2, 3, 3, nullptr);
			this->boneGridViewer->setSize(widgetWidth, 100);
			fillGridViewer(this->boneGridViewer);
			
			this->boneGridViewer->setOnDrawEvent(this, &AnimationSkeletonEditor::onDrawBone);
			this->boneGridViewer->setOnClickEvent(this, &AnimationSkeletonEditor::onPickBone);
			this->boneGridViewer->setGridList(&bones);

			this->boneGridViewer->setActive(false);

			boxLayout->addWidget(this->boneGridViewer);
		}

		{
			FlowLayout* editLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			editLayout->setSize(widgetWidth, 28);
			fillFlowLayout(editLayout);
			this->editModeButton = new Button(2, nullptr);
			fillButton(this->editModeButton, "Edit mode");
			editModeButton->setOnClickEvent(this, &AnimationSkeletonEditor::onEditModeClick);

			editModeButton->setActive(false);

			editLayout->addWidget(this->editModeButton);
			boxLayout->addWidget(editLayout);
		}

		{
			FlowLayout* gizmoLayout = new FlowLayout(5, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			gizmoLayout->setSize(widgetWidth, 28);
			fillFlowLayout(gizmoLayout);

			Label* nameLabel = new Label(0, nullptr);
			fillLabel(nameLabel, "Gizmo to parent");
			gizmoLayout->addWidget(nameLabel);

			this->gizmoToParentCheckbox = new Checkbox(0, nullptr);
			fillCheckbox(gizmoToParentCheckbox);
			gizmoToParentCheckbox->setCheck(false);
			gizmoLayout->addWidget(gizmoToParentCheckbox);

			boxLayout->addWidget(gizmoLayout);
		}

		{
			boneLabel.setFont("default.ttf",10);
			boneLabel.setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
			boneLabel.setColor(255, 255, 255, 255);
			boneLabel.setText("");
		}

		this->drawBoxSelection = false;

		addWidgetToEditor(boxLayout);
	}

	void AnimationSkeletonEditor::updateWidget(void* data)
	{
		ja::Skeleton* skeleton = (ja::Skeleton*)data;

		if (nullptr == skeleton)
		{
			bones.clear();
			this->skeletonNameTextfield->setText("");
			this->skeletonNameTextfield->setActive(false);
			this->editModeButton->setActive(false);
			return;
		}

		this->skeletonNameTextfield->setActive(true);
		this->editModeButton->setActive(true);

		this->skeletonNameTextfield->setText(skeleton->getName());

		
	}

	void AnimationSkeletonEditor::setEditorMode(AnimationEditorMode editorMode)
	{
		this->editorMode = editorMode;

		switch (this->editorMode)
		{
		case AnimationEditorMode::SELECT_MODE:
			AnimationEditor::getSingleton()->hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_MODE_HUD, "Mode=Bone Select");
			AnimationEditor::getSingleton()->hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_HELPER_HUD, "ESC - Accept\nLMB - Select vertex\nF - Face\nD - Delete vertex\nCtrl+D - Delete face\nShift + D - Duplicate\nE - Extrude\nC - Create bone\nG - Translate\nR - Rotate\nS - Scale");
			break;
		case AnimationEditorMode::TRANSLATE_MODE:
			AnimationEditor::getSingleton()->hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_MODE_HUD, "Mode=Bone Translate");
			AnimationEditor::getSingleton()->hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_HELPER_HUD, "LMB - Accepth\nESC - Cancel");
			break;
		case AnimationEditorMode::ROTATE_MODE:
			AnimationEditor::getSingleton()->hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_MODE_HUD, "Mode=Bone Rotate");
			AnimationEditor::getSingleton()->hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_HELPER_HUD, "LMB - Accepth\nESC - Cancel");
			break;
		case AnimationEditorMode::SCALE_MODE:
			AnimationEditor::getSingleton()->hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_MODE_HUD, "Mode=Bone Scale");
			AnimationEditor::getSingleton()->hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_HELPER_HUD, "LMB - Accepth\nESC - Cancel\nx - scale x\ny - scale y");
			break;
		}
	}

	void AnimationSkeletonEditor::drawGizmo()
	{
		DisplayManager::setColorUb(255, 255, 255);
		DisplayManager::setPointSize(7);
		DisplayManager::drawPoint(gizmo.x, gizmo.y);

		DisplayManager::setColorUb(255, 0, 0);
		DisplayManager::setPointSize(5);
		DisplayManager::drawPoint(gizmo.x, gizmo.y);
	}

	void AnimationSkeletonEditor::drawBoneNames()
	{
		Font* fontRender = ResourceManager::getSingleton()->getEternalFont("default.ttf",10)->resource;
		float scale      = DisplayManager::getHeightMultiplier();

		uint16_t               bonesNum         = this->editedSkeleton->getBonesNum();
		const BoneName*        boneNames        = this->editedSkeleton->getBoneNames();
		const TransformedBone* transformedBones = this->editedSkeleton->getTransformedBones();

		jaVector2 position = this->editedSkeleton->getPosition();

		bool* pSelectedBones = this->selectedBones.getFirst();

		for (uint16_t i = 0; i < bonesNum; ++i)
		{
			if (pSelectedBones[i])
				DisplayManager::RenderTextWorld(fontRender, transformedBones[i].position.x, transformedBones[i].position.y, scale, boneNames[i].getName(), (uint32_t)Margin::TOP, 255, 255, 255, 200);
			else
				DisplayManager::RenderTextWorld(fontRender, transformedBones[i].position.x, transformedBones[i].position.y, scale, boneNames[i].getName(), (uint32_t)Margin::TOP, 128, 128, 128, 200);
		}
	}

	void AnimationSkeletonEditor::drawSelectedBones()
	{
		const TransformedBone* pTransformed = this->editedSkeleton->getTransformedBones();

		uint32_t  selectedNum      = selectedBoneIds.getSize();
		uint16_t* pSelectedBoneIds = selectedBoneIds.getFirst();

		glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		glPointSize(3.0f);

		glBegin(GL_POINTS);
		for (uint32_t i = 0; i < selectedNum; ++i)
		{
			glVertex2f(pTransformed[pSelectedBoneIds[i]].position.x, pTransformed[pSelectedBoneIds[i]].position.y);
		}
		glEnd();
	}

	void AnimationSkeletonEditor::render()
	{
		if (this->editedSkeleton == nullptr)
			return;

		DisplayManager::setColorUb(255, 255, 255, 255);

		const jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

		drawSelectedBones();

		if (this->drawBoxSelection) {
			DisplayManager::setColorUb(255, 0, 128, 255);
			DisplayManager::setLineWidth(3.0f);
			gil::AABB2d boxSelection = gil::AABB2d::make(selectionStartPoint, Toolset::getSingleton()->getDrawPoint());
			gil::Debug::drawAABB2d(boxSelection);

			DisplayManager::setColorUb(255, 255, 255);
			DisplayManager::setPointSize(7);
			DisplayManager::drawPoint(selectionStartPoint.x, selectionStartPoint.y);

			DisplayManager::setColorUb(255, 0, 0);
			DisplayManager::setPointSize(5);
			DisplayManager::drawPoint(selectionStartPoint.x, selectionStartPoint.y);
		}

		if ((this->editorMode == AnimationEditorMode::ROTATE_MODE) || (this->editorMode == AnimationEditorMode::SCALE_MODE))
		{
			DisplayManager::setLineWidth(2.0f);
			DisplayManager::setColorF(1.0f, 1.0f, 1.0f);
			DisplayManager::drawLine(drawPoint.x, drawPoint.y, gizmo.x, gizmo.y);

			//if (AnimationEditorMode::SCALE_MODE == this->editorMode)
			//{
			//	if (this->scaleAxis != ScaleMode::SCALE_XY)
			//		Toolset::getSingleton()->getRuler()->draw();
			//}
		}

		drawBoneNames();

		drawGizmo();
		//drawOriginPoint();
	}

	void AnimationSkeletonEditor::addToSelection(gil::AABB2d& selectionAABB)
	{	
		uint16_t bonesNum = this->editedSkeleton->getBonesNum();
		const TransformedBone* pTransformedBone = this->editedSkeleton->getTransformedBones();

		bool* pSelected = this->selectedBones.getFirst();

		this->selectedBoneIds.clear();
		
		for (int32_t i = 0; i < bonesNum; ++i)
		{
			bool state = selectionAABB.isPointInside(pTransformedBone[i].position);
			pSelected[i] = pSelected[i] || state;

			if (pSelected[i])
				this->selectedBoneIds.push(i);
		}
	}

	void AnimationSkeletonEditor::removeFromSelection(gil::AABB2d& selectionAABB)
	{
		uint16_t bonesNum = this->editedSkeleton->getBonesNum();
		const TransformedBone* pTransformedBone = this->editedSkeleton->getTransformedBones();
		
		bool* pSelected = this->selectedBones.getFirst();

		this->selectedBoneIds.clear();
		
		for (int32_t i = 0; i < bonesNum; ++i)
		{
			bool state = selectionAABB.isPointInside(pTransformedBone[i].position);
		
			if (state)
				pSelected[i] = false;

			if (pSelected[i])
				this->selectedBoneIds.push(i);
		}
	}

	void AnimationSkeletonEditor::saveBoneStates()
	{
		const Bone*            pBones            = this->editedSkeleton->getBones();
		const TransformedBone* pTransformedBones = this->editedSkeleton->getTransformedBones();
		uint16_t               bonesNum          = this->editedSkeleton->getBonesNum();

		this->savedBoneStates.resize(bonesNum * sizeof(Bone));
		this->savedBoneStates.setSize(bonesNum);
		this->savedTransformedBoneStates.resize(bonesNum * sizeof(TransformedBone));
		this->savedTransformedBoneStates.setSize(bonesNum);

		Bone* pSavedBones = this->savedBoneStates.getFirst();
		memcpy(pSavedBones, pBones, bonesNum * sizeof(Bone));

		TransformedBone* pSavedTransformedBones = this->savedTransformedBoneStates.getFirst();
		memcpy(pSavedTransformedBones, pTransformedBones, bonesNum * sizeof(TransformedBone));
	}

	void AnimationSkeletonEditor::makeSelection(gil::AABB2d& selectionAABB)
	{		
		this->selectedBones.clear();
		this->selectedBoneIds.clear();

		this->editedSkeleton->getSelectedBones(selectionAABB, this->selectedBoneIds);

		uint16_t bonesNum = this->editedSkeleton->getBonesNum();

		this->selectedBones.resize(bonesNum * sizeof(bool));
		this->selectedBones.setSize(bonesNum);

		bool* pSelectedBones = this->selectedBones.getFirst();

		memset(pSelectedBones, 0, bonesNum * sizeof(bool));

		uint32_t selectedIdsNum  = this->selectedBoneIds.getSize();
		uint16_t* pSelectedIds   = this->selectedBoneIds.getFirst();

		for (uint32_t i = 0; i < selectedIdsNum; ++i)
		{
			pSelectedBones[pSelectedIds[i]] = true;
		}
	}

	void AnimationSkeletonEditor::calculateSelectionPoint()
	{
		const TransformedBone* pTransformedBones = this->editedSkeleton->getTransformedBones();
		int16_t bonesNum = this->editedSkeleton->getBonesNum();

		bool* pSelected = this->selectedBones.getFirst();
		
		jaVector2 midpoint(0.0f, 0.0f);
		
		uint32_t selectedNum = 0;

		uint16_t lastSelectedBoneId = 0;
		
		for (int16_t i = 0; i < bonesNum; ++i)
		{
			if (pSelected[i])
			{
				midpoint += pTransformedBones[i].position;
				++selectedNum;
				lastSelectedBoneId = i;
			}
		}
		
		if (selectedNum > 0)
		{
			float fSelectedNum = (float)selectedNum;
			this->gizmo = jaVector2(midpoint.x / fSelectedNum, midpoint.y / fSelectedNum);
		}

		if (gizmoToParentCheckbox->isChecked() && (selectedNum == 1))
		{
			const Bone* bones = this->editedSkeleton->getBones();
			uint16_t parentId = bones[lastSelectedBoneId].parentId;

			if (-1 != parentId)
			{
				this->gizmo = pTransformedBones[parentId].position;
			}
		}
	}

	void AnimationSkeletonEditor::handleInput()
	{
		if (AnimationEditorMode::SELECT_MODE == this->editorMode)
		{
			if (InputManager::isKeyPressed(JA_ESCAPE))
			{
				setSkeletonInfo(nullptr);
				AnimationEditor::getSingleton()->setEditorMode(AnimationEditorMode::SELECT_MODE);
			}
		}

		if (!UIManager::isMouseOverGui())
		{
			if (AnimationEditorMode::SELECT_MODE == this->editorMode)
			{
				if (!InputManager::isKeyModPress(JA_KMOD_LALT))
				{
					if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
					{
						this->drawBoxSelection = true;
						this->selectionStartPoint = Toolset::getSingleton()->getDrawPoint();
					}

					if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT) && (this->drawBoxSelection))
					{
						this->drawBoxSelection = false;

						gil::AABB2d selectionAABB = gil::AABB2d::make(selectionStartPoint, Toolset::getSingleton()->getDrawPoint());

						if (InputManager::isKeyModPress(JA_KMOD_LSHIFT) || InputManager::isKeyModPress(JA_KMOD_LCTRL))
						{
							if (InputManager::isKeyModPress(JA_KMOD_LSHIFT))
								addToSelection(selectionAABB);
							else
								removeFromSelection(selectionAABB);
						}
						else
						{
							makeSelection(selectionAABB);
						}

						calculateSelectionPoint();

						uint32_t selectedBonesNum = this->selectedBoneIds.getSize();
						if (selectedBonesNum > 0)
						{
							uint16_t* boneIds = this->selectedBoneIds.getFirst();
							AnimationBoneEditor::getSingleton()->updateBoneId(boneIds[selectedBonesNum - 1]);
						}
						else
						{
							AnimationBoneEditor::getSingleton()->updateBoneId(MAXUINT16);
						}
					}
				}

				if (InputManager::isKeyModPress(JA_KMOD_LALT))
				{
					if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
						this->gizmo = Toolset::getSingleton()->getDrawPoint();
				}

				if (InputManager::isKeyPressed(JA_f))
				{
					//makeFace();
				}

				if (InputManager::isKeyPressed(JA_g))
				{
					saveBoneStates();
					setEditorMode(AnimationEditorMode::TRANSLATE_MODE);
				}

				if (InputManager::isKeyPressed(JA_r))
				{
					saveBoneStates();
					transformationStart = Toolset::getSingleton()->getDrawPoint();
					setEditorMode(AnimationEditorMode::ROTATE_MODE);
				}

				if (InputManager::isKeyPressed(JA_s))
				{
					saveBoneStates();
					transformationStart = Toolset::getSingleton()->getDrawPoint();
					setEditorMode(AnimationEditorMode::SCALE_MODE);
					this->scaleAxis = ScaleMode::SCALE_XY;
				}

				if (InputManager::isKeyPressed(JA_c))
				{
					createBone();
				}

				if (InputManager::isKeyPressed(JA_d))
				{
					//if (InputManager::isKeyModPress(JA_KMOD_LCTRL))
						//deleteFace();

					//if (InputManager::isKeyModPress(JA_KMOD_LSHIFT))
					//	duplicate();
					//else
					//	deleteVertex();
				}
			}
		}

		if (editorMode == AnimationEditorMode::TRANSLATE_MODE)
		{
			if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT)) {
				setEditorMode(AnimationEditorMode::SELECT_MODE);
				calculateSelectionPoint();
			}
		}

		if (editorMode == AnimationEditorMode::ROTATE_MODE)
		{
			if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT)) {
				setEditorMode(AnimationEditorMode::SELECT_MODE);
			}
		}

		if (editorMode == AnimationEditorMode::SCALE_MODE)
		{
			if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT)) {
				setEditorMode(AnimationEditorMode::SELECT_MODE);
			}
		}

		if (editorMode != AnimationEditorMode::SELECT_MODE)
		{
			if (InputManager::getSingleton()->isKeyPressed(JA_ESCAPE))
			{
				Bone*    pBones   = (Bone*)this->editedSkeleton->getBones();
				uint16_t bonesNum = this->editedSkeleton->getBonesNum();

				memcpy(pBones, this->savedBoneStates.getFirst(), bonesNum * sizeof(Bone));

				setEditorMode(AnimationEditorMode::SELECT_MODE);
			}
		}

		switch (editorMode)
		{
		case AnimationEditorMode::TRANSLATE_MODE:
			translateMode();
			break;
		case AnimationEditorMode::SCALE_MODE:
			scaleMode();
			break;
		case AnimationEditorMode::ROTATE_MODE:
			rotateMode();
			break;
		}

	}

	void AnimationSkeletonEditor::setActiveMode(bool state)
	{
		if (state)
		{
			editModeButton->setColor(0, 255, 0);

		}
		else
		{
			editModeButton->setColor(255, 0, 0);
		}

		setEditorMode(AnimationEditorMode::SELECT_MODE);
	}

	void AnimationSkeletonEditor::translateMode()
	{
		TransformedBone* pSavedTransformedBone = this->savedTransformedBoneStates.getFirst();
		uint16_t         bonesNum              = this->editedSkeleton->getBonesNum();
		
		bool* pSelectedBones = selectedBones.getFirst();
		
		jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
		
		uint16_t lastSelectedBoneId = 0;

		for (uint32_t i = 0; i < bonesNum; ++i)
		{
			if (pSelectedBones[i])
			{
				jaVector2 diff            = pSavedTransformedBone[i].position - gizmo;
				jaVector2 newBonePosition = drawPoint + diff;

				this->editedSkeleton->setBonePosition(i, newBonePosition);

				lastSelectedBoneId = i;
			}
		}

		AnimationBoneEditor::getSingleton()->updateBoneId(lastSelectedBoneId);
	}

	void AnimationSkeletonEditor::scaleMode()
	{
		const TransformedBone* pSavedTransformedBones = this->savedTransformedBoneStates.getFirst();
		uint16_t bonesNum = this->editedSkeleton->getBonesNum();

		bool* pSelected = this->selectedBones.getFirst();

		jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

		const jaVector2 startVec   = transformationStart - gizmo;
		const jaVector2 currentVec = drawPoint - gizmo;

		const jaFloat startVecLength = jaVectormath2::length(startVec);
		const jaFloat currentVecLength = jaVectormath2::length(currentVec);

		jaVector2 scale(currentVecLength / startVecLength, currentVecLength / startVecLength);

		if (InputManager::isKeyPressed(JA_x))
		{
			this->scaleAxis = ScaleMode::SCALE_X;
			Toolset::getSingleton()->getRuler()->setPosition(gizmo.x, gizmo.y);
			Toolset::getSingleton()->getRuler()->setAngle(0);
		}

		if (InputManager::isKeyPressed(JA_y))
		{
			this->scaleAxis = ScaleMode::SCALE_Y;

			Toolset::getSingleton()->getRuler()->setPosition(gizmo.x, gizmo.y);
			Toolset::getSingleton()->getRuler()->setAngle(90);
		}

		switch (this->scaleAxis)
		{
		case ScaleMode::SCALE_X:
			scale.y = 1.0f;
			break;
		case ScaleMode::SCALE_Y:
			scale.x = 1.0f;
			break;
		}

		uint16_t lastSelectedBoneId = 0;

		for (uint16_t i = 0; i < bonesNum; ++i)
		{
			if (pSelected[i])
			{
				jaVector2       diff      = pSavedTransformedBones[i].position - gizmo;
				const jaVector2 scaleDiff = scale * diff;
				jaVector2 newPosition = gizmo + scaleDiff;

				this->editedSkeleton->setBonePosition(i, newPosition);

				lastSelectedBoneId = i;
			}
		}

		AnimationBoneEditor::getSingleton()->updateBoneId(lastSelectedBoneId);
	}

	void AnimationSkeletonEditor::rotateMode()
	{			
		TransformedBone* pSavedTransformedBone = this->savedTransformedBoneStates.getFirst();
		uint16_t         bonesNum = this->editedSkeleton->getBonesNum();

		bool* pSelectedBones = selectedBones.getFirst();

		jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
		const jaVector2 startVec = transformationStart - gizmo;
		const jaVector2 currentVec = drawPoint - gizmo;

		const jaFloat   angle = jaVectormath2::getAngle(currentVec, startVec);
		const jaMatrix2 rotMat(angle);

		uint16_t lastSelectedBoneId = 0;

		for (uint16_t i = 0; i < bonesNum; ++i)
		{
			if (pSelectedBones[i])
			{
				jaVector2 diff = pSavedTransformedBone[i].position - gizmo;
				const jaVector2 rotDiff = rotMat * diff;
				jaVector2 newBonePosition = gizmo + rotDiff;

				this->editedSkeleton->setBonePosition(i, newBonePosition);

				lastSelectedBoneId = i;
			}
		}

		AnimationBoneEditor::getSingleton()->updateBoneId(lastSelectedBoneId);
	}

	void AnimationSkeletonEditor::onEditModeClick(Widget* sender, WidgetEvent action, void* data)
	{
		CommonWidget* pCommonWidget = AnimationEditor::getSingleton()->getCurrentEditor();
		if (nullptr != pCommonWidget)
		{
			if (pCommonWidget == this)
				return;
		}

		std::list<ja::Skeleton*>& selectedSkeletons = AnimationEditor::getSingleton()->selectedSkeletons;

		if (selectedSkeletons.size() == 0)
		{
			Toolset::getSingleton()->windowHud->addInfoC((uint16_t)AnimationEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "Select skeleton first");
			return;
		}

		if (selectedSkeletons.size() > 1)
		{
			Toolset::getSingleton()->windowHud->addInfoC((uint16_t)AnimationEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "You can edit only one skeleton at once");
			return;
		}

		Skeleton* skeletonToEdit = *selectedSkeletons.begin();

		setSkeletonInfo(skeletonToEdit);

		AnimationEditor::getSingleton()->setEditorMode(this);
	}

	void AnimationSkeletonEditor::onNameDeselect(Widget* sender, WidgetEvent action, void* data)
	{
		const char* skeletonName = this->skeletonNameTextfield->getTextPtr();

		std::list<ja::Skeleton*>& selectedSkeletons = AnimationEditor::getSingleton()->selectedSkeletons;
		Skeleton* selectedSkeleton = selectedSkeletons.back();

		if (nullptr != selectedSkeleton)
		{
			selectedSkeleton->setName(skeletonName);
		}
	}

	void AnimationSkeletonEditor::onDrawBone(Widget* sender, WidgetEvent action, void* data)
	{
		Bone* bone = (Bone*)data;

		int32_t x, y;

		int32_t halfWidth, halfHeight;


		sender->getPosition(x, y);
	

		halfWidth  = (sender->getWidth() / 2);
		halfHeight = (sender->getHeight() / 2);

		Button* button = (Button*)sender;

		uint16_t boneId = this->editedSkeleton->getBoneId(bone);

		bool* pSelectedBones = this->selectedBones.getFirst();
		
		if (pSelectedBones[boneId])
		{
			button->setColor(0, 255, 0);
		}
		else
		{
			button->setColor(255, 0, 0);
		}

		BoneName* boneNames = this->editedSkeleton->getBoneNames();
	
		boneLabel.x = x + halfWidth;
		boneLabel.y = y + halfHeight;
		boneLabel.setText(boneNames[boneId].getName());
		boneLabel.draw(0, 0);
	}

	void AnimationSkeletonEditor::onPickBone(Widget* sender, WidgetEvent action, void* data)
	{
		Button* button = (Button*)sender;
		Bone*   bone   = (Bone*)button->vUserData;

		if (nullptr == bone)
			return;

		uint16_t boneId = this->editedSkeleton->getBoneId(bone);

		bool*    pSelectedBones   = this->selectedBones.getFirst();
		uint16_t selectedBonesNum = this->selectedBones.getSize();

		this->selectedBoneIds.clear();

		if (InputManager::isKeyModPress(JA_KMOD_LSHIFT) || InputManager::isKeyModPress(JA_KMOD_LCTRL))
		{
			if (InputManager::isKeyModPress(JA_KMOD_LSHIFT)) //add to selection
				pSelectedBones[boneId] = true;
			else
				pSelectedBones[boneId] = false;

			for (uint16_t i = 0; i < selectedBonesNum; ++i)
			{
				if (pSelectedBones[i])
				{
					this->selectedBoneIds.push(i);
				}
			}

			calculateSelectionPoint();

			return;
		}

		memset(pSelectedBones, 0, selectedBonesNum * sizeof(bool));
		pSelectedBones[boneId] = true;
		this->selectedBoneIds.push(boneId);

		calculateSelectionPoint();
	}

	void AnimationSkeletonEditor::setSkeletonInfo(ja::Skeleton* skeleton)
	{
		this->editedSkeleton = skeleton;
		bones.clear();

		if (nullptr == skeleton)
		{
			this->boneGridViewer->setActive(false);
			this->bonesNumLabel->setText("Bones:");
			return;
		}

		this->boneGridViewer->setActive(true);
		
		refreshBoneGridView();

		gizmo = skeleton->getPosition();
	}

	void AnimationSkeletonEditor::refreshBoneGridView()
	{
		this->bones.clear();

		if (nullptr == this->editedSkeleton)
		{
			this->bonesNumLabel->setText("Bones:");
			return;
		}

		Bone*    skeletonBones = this->editedSkeleton->getBones();
		uint16_t bonesNum      = this->editedSkeleton->getBonesNum();

		this->bonesNumLabel->setTextPrintC("Bones:%d", (int32_t)bonesNum);

		for (uint16_t i = 0; i < bonesNum; ++i)
		{
			this->bones.push_back(&skeletonBones[i]);
		}
		
		boneGridViewer->setCellColor(255, 0, 0, 128);
	}

	void AnimationSkeletonEditor::createBone()
	{
		const jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

		uint32_t selectedBoneIdsNum = this->selectedBoneIds.getSize();

		if (selectedBoneIdsNum > 1)
		{
			Toolset::getSingleton()->windowHud->addInfoC((uint16_t)AnimationEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "Bone can only have one parent");
			return;
		}

		if (0 == selectedBoneIdsNum)
		{
			Toolset::getSingleton()->windowHud->addInfoC((uint16_t)AnimationEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "Select parent bone fist");
			return;
		}

		this->editedSkeleton->addBone(*this->selectedBoneIds.getFirst(), drawPoint);
		this->selectedBones.push(false);

		refreshBoneGridView();
	}

	AnimationBoneEditor::AnimationBoneEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Bone", parent)
	{
		singleton = this;
		setRender(false);
		setActive(false);

		BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);

		const uint32_t widgetWidth = 350;
		boxLayout->setSize(widgetWidth, 4 * (28 + 2));
		fillBoxLayout(boxLayout);

		{
			FlowLayout* nameLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			nameLayout->setSize(widgetWidth, 28);
			fillFlowLayout(nameLayout);

			Label* nameLabel = new Label(0, nullptr);
			fillLabel(nameLabel, "Name");
			nameLayout->addWidget(nameLabel);

			this->boneNameTextfield = new TextField(1, nullptr);
			fillTextField(this->boneNameTextfield);
			this->boneNameTextfield->setTextSize(setup::graphics::BONE_NAME_LENGTH, 28);
			this->boneNameTextfield->setOnDeselectEvent(this, &AnimationBoneEditor::onNameDeselect);

			this->boneNameTextfield->setActive(false);

			nameLayout->addWidget(this->boneNameTextfield);
			boxLayout->addWidget(nameLayout);
		}

		{
			FlowLayout* boneLengthLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			boneLengthLayout->setSize(widgetWidth, 28);
			fillFlowLayout(boneLengthLayout);
			
			Label* boneLengthLabel = new Label(0, nullptr);
			fillLabel(boneLengthLabel, "Length");
			boneLengthLayout->addWidget(boneLengthLabel);

			this->boneLengthTextfield = new TextField(1, nullptr);
			fillTextField(this->boneLengthTextfield);
			this->boneLengthTextfield->setTextSize(16, 28);
			this->boneLengthTextfield->setAcceptOnlyNumbers(true);
			this->boneLengthTextfield->setOnDeselectEvent(this, &AnimationBoneEditor::onBoneLengthDeselect);
			boneLengthLayout->addWidget(boneLengthTextfield);

			this->boneLengthTextfield->setActive(false);

			boxLayout->addWidget(boneLengthLayout);
		}

		{
			FlowLayout* bonePositionLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			bonePositionLayout->setSize(widgetWidth, 28);
			fillFlowLayout(bonePositionLayout);

			Label* positionXLabel = new Label(0, nullptr);
			fillLabel(positionXLabel, "X");
			bonePositionLayout->addWidget(positionXLabel);

			this->positionXTextfield = new TextField(1, nullptr);
			fillTextField(this->positionXTextfield);
			this->positionXTextfield->setTextSize(16, 28);
			this->positionXTextfield->setAcceptOnlyNumbers(true);
			this->positionXTextfield->setOnDeselectEvent(this, &AnimationBoneEditor::onPositionXDeselect);
			bonePositionLayout->addWidget(this->positionXTextfield);

			this->positionXTextfield->setActive(false);

			Label* positionYLabel = new Label(2, nullptr);
			fillLabel(positionYLabel, "Y");
			bonePositionLayout->addWidget(positionYLabel);

			this->positionYTextfield = new TextField(3, nullptr);
			fillTextField(this->positionYTextfield);
			this->positionYTextfield->setTextSize(16, 28);
			this->positionYTextfield->setAcceptOnlyNumbers(true);
			this->positionYTextfield->setOnDeselectEvent(this, &AnimationBoneEditor::onPositionYDeselect);
			bonePositionLayout->addWidget(this->positionYTextfield);

			this->positionYTextfield->setActive(false);

			boxLayout->addWidget(bonePositionLayout);
		}

		{
			FlowLayout* angleLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			angleLayout->setSize(widgetWidth, 28);
			fillFlowLayout(angleLayout);

			Label* angleLabel = new Label(0, nullptr);
			fillLabel(angleLabel, "Angle");
			angleLayout->addWidget(angleLabel);

			this->angleTextfield = new TextField(1, nullptr);
			fillTextField(this->angleTextfield);
			this->angleTextfield->setTextSize(16, 28);
			this->angleTextfield->setAcceptOnlyNumbers(true);
			this->angleTextfield->setOnDeselectEvent(this, &AnimationBoneEditor::onAngleDeselect);
			angleLayout->addWidget(this->angleTextfield);

			this->angleTextfield->setActive(false);

			boxLayout->addWidget(angleLayout);
		}

		addWidgetToEditor(boxLayout);
	}

	void AnimationBoneEditor::updateWidget(void* data)
	{
		this->boneNameTextfield->setText("");
		this->boneNameTextfield->setActive(false);
	}

	void AnimationBoneEditor::updateBoneId(uint16_t boneId)
	{
		this->editedBoneId = boneId;

		if (boneId == MAXUINT16)
		{
			this->boneNameTextfield->setText("");
			this->boneNameTextfield->setActive(false);
			this->boneLengthTextfield->setText("");
			this->boneLengthTextfield->setActive(false);
			this->positionXTextfield->setText("");
			this->positionXTextfield->setActive(false);
			this->positionYTextfield->setText("");
			this->positionYTextfield->setActive(false);
			this->angleTextfield->setText("");
			this->angleTextfield->setActive(false);
			return;
		}


		ja::Skeleton* editedSkeleton = AnimationSkeletonEditor::getSingleton()->getEditedSkeleton();

		this->boneNameTextfield->setActive(true);
		this->boneLengthTextfield->setActive(true);
		this->positionXTextfield->setActive(true);
		this->positionYTextfield->setActive(true);
		this->angleTextfield->setActive(true);

		if (nullptr != editedSkeleton)
		{
			const BoneName* boneNames = editedSkeleton->getBoneNames();
			this->boneNameTextfield->setText(boneNames[boneId].getName());

			const Bone* bones = editedSkeleton->getBones();
			this->boneLengthTextfield->setFieldValue("%f", bones[boneId].transformation.length);

			const TransformedBone* transformedBones = editedSkeleton->getTransformedBones();
			this->positionXTextfield->setFieldValue("%f", transformedBones[boneId].position.x);
			this->positionYTextfield->setFieldValue("%f", transformedBones[boneId].position.y);
			this->angleTextfield->setFieldValue("%f", transformedBones[boneId].rotMat.getDegrees());
			//this->bon transformedBones[boneId].position
		}
	}

	void AnimationBoneEditor::onNameDeselect(Widget* sender, WidgetEvent action, void* data)
	{
		if (this->editedBoneId != MAXUINT16)
		{
			ja::Skeleton* editedSkeleton = AnimationSkeletonEditor::getSingleton()->getEditedSkeleton();

			const char* boneName  = this->boneNameTextfield->getTextPtr();
			BoneName*   boneNames = editedSkeleton->getBoneNames();

			if (-1 != editedSkeleton->getBoneId(boneName))
			{
				this->boneNameTextfield->setText(boneNames[this->editedBoneId].getName());
				return;
			}

			boneNames[this->editedBoneId].setName(boneName);
		}
	}

	void AnimationBoneEditor::onBoneLengthDeselect(Widget* sender, WidgetEvent action, void* data)
	{
		Skeleton* editedSkeleton = AnimationSkeletonEditor::getSingleton()->getEditedSkeleton();

		Bone* bones = editedSkeleton->getBones();

		float boneLength = 0.0f;

		if (false == boneLengthTextfield->getAsFloat(&boneLength))
		{
			this->boneLengthTextfield->setFieldValue("%f", bones[this->editedBoneId].transformation.length);
			return;
		}

		bones[this->editedBoneId].transformation.length = boneLength;

		editedSkeleton->calculateTransformedBoneStates();

		const TransformedBone* transformedBones = editedSkeleton->getTransformedBones();
		this->positionXTextfield->setFieldValue("%f", transformedBones[this->editedBoneId].position.x);
		this->positionYTextfield->setFieldValue("%f", transformedBones[this->editedBoneId].position.y);
	}

	void AnimationBoneEditor::onPositionXDeselect(Widget* sender, WidgetEvent action, void* data)
	{
		Skeleton* editedSkeleton = AnimationSkeletonEditor::getSingleton()->getEditedSkeleton();

		float xPos = 0.0f;
		float yPos = 0.0f;

		if (false == positionXTextfield->getAsFloat(&xPos))
		{
			const TransformedBone* transformedBone = editedSkeleton->getTransformedBones();
			this->positionXTextfield->setFieldValue("%f", transformedBone[this->editedBoneId].position.x);
			return;
		}
			
		if (false == positionYTextfield->getAsFloat(&yPos))
		{
			const TransformedBone* transformedBone = editedSkeleton->getTransformedBones();
			this->positionYTextfield->setFieldValue("%f", transformedBone[this->editedBoneId].position.y);
			return;
		}

		editedSkeleton->setBonePosition(this->editedBoneId, jaVector2(xPos, yPos));

		Bone* bones = editedSkeleton->getBones();
		this->boneLengthTextfield->setFieldValue("%f", bones[this->editedBoneId].transformation.length);

		const TransformedBone* transformedBones = editedSkeleton->getTransformedBones();
		this->angleTextfield->setFieldValue("%f", transformedBones[this->editedBoneId].rotMat.getDegrees());
	}

	void AnimationBoneEditor::onPositionYDeselect(Widget* sender, WidgetEvent action, void* data)
	{
		Skeleton* editedSkeleton = AnimationSkeletonEditor::getSingleton()->getEditedSkeleton();
		
		float xPos = 0.0f;
		float yPos = 0.0f;

		if (false == positionXTextfield->getAsFloat(&xPos))
		{
			const TransformedBone* transformedBone = editedSkeleton->getTransformedBones();
			this->positionXTextfield->setFieldValue("%f", transformedBone[this->editedBoneId].position.x);
			return;
		}

		if (false == positionYTextfield->getAsFloat(&yPos))
		{
			const TransformedBone* transformedBone = editedSkeleton->getTransformedBones();
			this->positionYTextfield->setFieldValue("%f", transformedBone[this->editedBoneId].position.y);
			return;
		}
		
		editedSkeleton->setBonePosition(this->editedBoneId, jaVector2(xPos, yPos));

		Bone* bones = editedSkeleton->getBones();
		this->boneLengthTextfield->setFieldValue("%f", bones[this->editedBoneId].transformation.length);

		const TransformedBone* transformedBones = editedSkeleton->getTransformedBones();
		this->angleTextfield->setFieldValue("%f", transformedBones[this->editedBoneId].rotMat.getDegrees());
	}

	void AnimationBoneEditor::onAngleDeselect(Widget* sender, WidgetEvent action, void* data)
	{
		Skeleton* editedSkeleton = AnimationSkeletonEditor::getSingleton()->getEditedSkeleton();

		float angleWorldRot = 0.0f;

		if (false == angleTextfield->getAsFloat(&angleWorldRot))
		{
			const TransformedBone* transformedBone = editedSkeleton->getTransformedBones();
			this->angleTextfield->setFieldValue("%f", transformedBone[this->editedBoneId].rotMat.getDegrees());
			return;
		}

		angleWorldRot = jaVectormath2::degreesToRadians(angleWorldRot);

		editedSkeleton->setBoneWordRotation(this->editedBoneId, angleWorldRot);

		const TransformedBone* transformedBones = editedSkeleton->getTransformedBones(); 

		positionXTextfield->setFieldValue("%f", transformedBones[this->editedBoneId].position.x);
		positionYTextfield->setFieldValue("%f", transformedBones[this->editedBoneId].position.y);
	}

	AnimationSpaceBarMenu::AnimationSpaceBarMenu(uint32_t widgetId, Widget* parent) : GridLayout(widgetId, parent, 1, 3, 0, 2)
	{
		setSize(80, 200);

		//addShapeButton = new Button(0, nullptr);
		//
		//fillButton(addShapeButton, "Add");
		//
		//ShapeAddMenu* addShapeMenu  = new ShapeAddMenu(0, nullptr);
		//SlideWidget*  addShapeSlide = new SlideWidget(1, addShapeButton, addShapeMenu, nullptr);
		//addShapeSlide->setAlign((uint32_t)Margin::TOP | (uint32_t)Margin::RIGHT);
		//
		//addWidget(addShapeSlide, 0, 0);
	}

	AnimationSpaceBarMenu::~AnimationSpaceBarMenu()
	{

	}

	void AnimationSpaceBarMenu::hide()
	{
		this->setActive(false);
		this->setRender(false);
	}

	void AnimationSpaceBarMenu::show()
	{
		this->setActive(true);
		this->setRender(true);

		UIManager::setSelected(this);
	}

	void AnimationSpaceBarMenu::update()
	{
		Widget* lastSelected = UIManager::getSingleton()->getLastSelected();

		bool isSelected = false;

		if (lastSelected != nullptr)
		{
			Widget* itWidget = lastSelected;

			for (Widget* itWidget = lastSelected; itWidget != nullptr; itWidget = itWidget->getParent())
			{
				if (itWidget == this)
				{
					isSelected = true;
					break;
				}
			}
		}

		if (isSelected)
		{
			GridLayout::update();

			Widget* lastClicked = UIManager::getLastClicked();
			for (Widget* itWidget = lastClicked; itWidget != nullptr; itWidget = itWidget->getParent())
			{
				if (itWidget == this)
				{
					hide();
				}
			}
		}
		else {
			hide();
		}
	}
}