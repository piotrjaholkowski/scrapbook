#include "ComponentEditor.hpp"
#include "CommonWidgets.hpp"
#include "WidgetId.hpp"
#include "EntityEditor.hpp"
#include "LayerObjectEditor.hpp"
#include "Toolset.hpp"
#include "../../Engine/Manager/EntityManager.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"
#include "../../Engine/Manager/GameManager.hpp"
#include "../../Engine/Manager/ResourceManager.hpp"
#include "../../Engine/Entity/ScriptGroup.hpp"
#include "../../Engine/Entity/LayerObjectGroup.hpp"
#include "../../Engine/Entity/PhysicGroup.hpp"
#include "../../Engine/Entity/SoundGroup.hpp"
#include "../../Engine/Gilgamesh/DebugDraw.hpp"

using namespace ja;

namespace tr
{

EntityComponentMenu* EntityComponentMenu::singleton = nullptr;
EntityListEditor*    EntityListEditor::singleton = nullptr;
SoundEditor*         SoundEditor::singleton = nullptr;

char EntityPopup::tagName[setup::TAG_NAME_LENGTH];

ComponentPopup::ComponentPopup(Widget* parent) : CommonPopup((uint32_t)TR_ENTITYEDITOR_ID::COMPONENT_POPUP, parent)
{
	const int32_t widgetWidth = 100;

	popup = new GridLayout((uint32_t)TR_ENTITYEDITOR_ID::COMPONENT_POPUP, nullptr, 1, 2, 0, 0);
	popup->setSelectable(true);
	popup->setSize(widgetWidth, 20 * 2);
	popup->setActive(true);
	popup->setRender(true);

	deleteComponent = new Button(0, nullptr);
	fillButton(deleteComponent, "Delete");
	deleteComponent->setOnClickEvent(this, &ComponentPopup::onDeleteComponent);
	popup->addWidget(deleteComponent, 0, 0);

	editComponent = new Button(1, nullptr);
	fillButton(editComponent, "Edit");
	editComponent->setOnClickEvent(this, &ComponentPopup::onEditComponent);
	popup->addWidget(editComponent, 0, 1);

	addWidget(popup);
	hide();
}

ComponentPopup::~ComponentPopup()
{

}

EntityPopup::EntityPopup(Widget* parent) : CommonPopup((uint32_t)TR_ENTITYEDITOR_ID::ENTITY_POPUP, parent)
{
	popup = new GridLayout((uint32_t)TR_ENTITYEDITOR_ID::ENTITY_POPUP, nullptr, 1, 3, 0, 0);
	popup->setSelectable(true);
	popup->setSize(210, 20 * 3);
	popup->setActive(true);
	popup->setRender(true);

	const int32_t widgetWidth = 210;

	deleteEntity = new Button(0, nullptr);
	fillButton(deleteEntity, "Delete");
	deleteEntity->setOnClickEvent(this, &EntityPopup::onDeleteEntityClick);
	popup->addWidget(deleteEntity, 0, 0);

	createEntity = new Button(1, nullptr);
	fillButton(createEntity, "Create");
	createEntity->setOnClickEvent(this, &EntityPopup::onCreateEntityClick);
	popup->addWidget(createEntity, 0, 1);

	FlowLayout* tagFlow = new FlowLayout(2, nullptr, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	tagFlow->setSize(widgetWidth, 20);
	fillFlowLayout(tagFlow);

	Label* tagLabel = new Label(0, nullptr);
	fillLabel(tagLabel, "Tag");
	tagFlow->addWidget(tagLabel);

	tagTextField = new TextField(1, nullptr);
	fillTextField(tagTextField);
	tagTextField->setTextSize(setup::TAG_NAME_LENGTH, 20);
	tagTextField->setOnDeselectEvent(this, &EntityPopup::onTagEntityDeselect);
	tagFlow->addWidget(tagTextField);

	popup->addWidget(tagFlow, 0, 2);
	addWidget(popup);

	hide();
}

EntityPopup::~EntityPopup()
{

}

void EntityPopup::onDeleteEntityClick(Widget* sender, WidgetEvent widgetEvent, void* data)
{
	EntityEditor*     entityEditor     = EntityEditor::getSingleton();
	EntityListEditor* entityListEditor = EntityListEditor::getSingleton();
	EntityManager*    entityManager    = EntityManager::getSingleton();

	std::list<Entity*>::iterator it = entityEditor->selectedEntities.begin();

	EntityComponentMenu::getSingleton()->updateComponentEditors(nullptr);

	for (it; it != entityEditor->selectedEntities.end(); ++it)
	{
		entityManager->deleteEntityImmediate((*it));
	}

	entityEditor->selectedEntities.clear();
	entityListEditor->refreshEntityList(); // redraw tables
	hide();
}

void EntityPopup::onCreateEntityClick(Widget* sender, WidgetEvent widgetEvent, void* data)
{
	EntityListEditor* entityListEditor = EntityListEditor::getSingleton();

	uint16_t      firstEntityId = (uint16_t)(floor(entityListEditor->getEntityScrollbar()->getValue()));
	uint16_t      biggestId     = EntityManager::getSingleton()->getNextId();

	if (sender->getWidgetType() == JA_BUTTON)
	{
		Button* entityButton = (Button*)(createEntity->vUserData);
		if (entityButton != nullptr)
		{
			Entity* entity = EntityManager::getSingleton()->createEntity(firstEntityId + entityButton->getId());

			entityButton->setTextValue("Entity %hu", entity->getId());
			entityButton->bUserData = true;
			entityButton->vUserData = entity;
		}

		hide();
	}
}

void EntityPopup::onTagEntityDeselect(Widget* sender, WidgetEvent widgetEvent, void* data)
{
	EntityEditor*     entityEditor     = EntityEditor::getSingleton();
	EntityListEditor* entityListEditor = EntityListEditor::getSingleton();

	const char* newTagPtr = tagTextField->getTextPtr();

#pragma warning(disable:4996)
	strncpy(tagName, newTagPtr, setup::TAG_NAME_LENGTH);
#pragma warning(default:4996)

	entityEditor->applyFilterToSelectedEntities(changeTagFilter);
	entityListEditor->refreshEntityList(); // redraw tables
	hide();
}

void EntityPopup::changeTagFilter(Entity* entity)
{
	entity->setTagName(tagName);
}

void ComponentPopup::onDeleteComponent(Widget* sender, WidgetEvent widgetEvent, void* data)
{
	EntityListEditor* entityListEditor = EntityListEditor::getSingleton();

	EntityComponentMenu::getSingleton()->updateComponentEditors(nullptr);
	entityComponent->deleteComponent();

	entityListEditor->refreshComponentList();
	hide();
}

void ComponentPopup::onEditComponent(Widget* sender, WidgetEvent widgetEvent, void* data)
{
	EntityListEditor* entityListEditor = EntityListEditor::getSingleton();

	if (entityComponent != nullptr)
	{
		EntityListEditor::getSingleton()->updateEditorsData(entityComponent);
	}

	hide();
}

EntityComponentMenu::EntityComponentMenu(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Menu", parent)
{
	singleton = this;

	setRender(true);
	setActive(true);

	const int32_t widgetWidth  = 150;
	const int32_t widgetHeight = 250;

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(widgetWidth, widgetHeight);
	fillBoxLayout(boxLayout);

	Button* addComponentButton = new Button((int32_t)TR_ENTITYEDITOR_ID::ADD_COMPONENT_EDITOR, nullptr);
	fillButton(addComponentButton, "Add Component");
	addComponentButton->setOnClickEvent(this, &EntityComponentMenu::onMenuClick);
	boxLayout->addWidget(addComponentButton);

	Button* entityListButton = new Button((int32_t)TR_ENTITYEDITOR_ID::ENTITY_LIST_EDITOR, nullptr);
	fillButton(entityListButton, "Entity List");
	entityListButton->setOnClickEvent(this, &EntityComponentMenu::onMenuClick);
	boxLayout->addWidget(entityListButton);

	components[(int32_t)TR_ENTITYEDITOR_ID::ADD_COMPONENT_EDITOR] = new AddComponentEditor((uint32_t)TR_ENTITYEDITOR_ID::ADD_COMPONENT_EDITOR, this);
	addWidget(components[(int32_t)TR_ENTITYEDITOR_ID::ADD_COMPONENT_EDITOR]);

	components[(int32_t)TR_ENTITYEDITOR_ID::ENTITY_LIST_EDITOR] = new EntityListEditor((uint32_t)TR_ENTITYEDITOR_ID::ENTITY_LIST_EDITOR, this);
	addWidget(components[(int32_t)TR_ENTITYEDITOR_ID::ENTITY_LIST_EDITOR]);

	components[(int32_t)TR_ENTITYEDITOR_ID::PHYSIC_OBJECT_COMPONENT] = new PhysicComponentEditor((uint32_t)TR_ENTITYEDITOR_ID::PHYSIC_OBJECT_COMPONENT, this);
	addWidget(components[(int32_t)TR_ENTITYEDITOR_ID::PHYSIC_OBJECT_COMPONENT]);

	components[(int32_t)TR_ENTITYEDITOR_ID::LAYER_OBJECT_COMPONENT] = new LayerObjectComponentEditor((uint32_t)TR_ENTITYEDITOR_ID::LAYER_OBJECT_COMPONENT, this);
	addWidget(components[(int32_t)TR_ENTITYEDITOR_ID::LAYER_OBJECT_COMPONENT]);

	components[(int32_t)TR_ENTITYEDITOR_ID::SCRIPT_COMPONENT] = new ScriptComponentEditor((uint32_t)TR_ENTITYEDITOR_ID::SCRIPT_COMPONENT, this);
	addWidget(components[(int32_t)TR_ENTITYEDITOR_ID::SCRIPT_COMPONENT]);

	components[(int32_t)TR_ENTITYEDITOR_ID::DATA_COMPONENT] = new DataComponentEditor((uint32_t)TR_ENTITYEDITOR_ID::DATA_COMPONENT, this);
	addWidget(components[(int32_t)TR_ENTITYEDITOR_ID::DATA_COMPONENT]);

	addWidgetToEditor(boxLayout);
}

void EntityComponentMenu::onMenuClick(Widget* sender, WidgetEvent action, void* data)
{
	uint32_t id = sender->getId();
	components[id]->setActive(true);
	components[id]->setRender(true);
}

void EntityComponentMenu::updateComponentEditors(void* component)
{
	components[(int32_t)TR_ENTITYEDITOR_ID::PHYSIC_OBJECT_COMPONENT]->updateWidget(component);
	components[(int32_t)TR_ENTITYEDITOR_ID::LAYER_OBJECT_COMPONENT]->updateWidget(component);
	components[(int32_t)TR_ENTITYEDITOR_ID::SCRIPT_COMPONENT]->updateWidget(component);
	components[(int32_t)TR_ENTITYEDITOR_ID::DATA_COMPONENT]->updateWidget(component);
}

AddComponentEditor::AddComponentEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Add Component", parent)
{
	setActive(true);
	setRender(true);

	const int32_t buttonWidth  = 32;
	const int32_t buttonHeight = 32;

	const int32_t cellsWidth  = 5;
	const int32_t cellsHeight = 5;

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(cellsWidth * buttonWidth, cellsHeight * buttonHeight);
	fillBoxLayout(boxLayout);

	{
		componentPickerGrid = new GridLayout(0, nullptr, cellsWidth, cellsHeight, 0, 0);
		fillGridLayout(componentPickerGrid);
		componentPickerGrid->setSize(cellsWidth * buttonWidth, cellsHeight * buttonHeight);
		componentPickerGrid->setRender(true);
		componentPickerGrid->setActive(true);

		for (int32_t i = 0; i< (cellsWidth * cellsHeight); ++i)
		{
			ImageButton* componentButton = new ImageButton(i, nullptr);
			fillButton(componentButton, "");

			EntityGroup* entityGroup = EntityManager::getSingleton()->getComponentGroup(i);

			if (entityGroup != nullptr)
			{
				componentButton->setImageTexture(entityGroup->getTextureImage());
				componentButton->bUserData = true;
				componentButton->vUserData = entityGroup;
			}
			else
			{
				componentButton->bUserData = false;
				componentButton->vUserData = nullptr;
			}

			componentButton->setOnClickEvent(this, &AddComponentEditor::onComponentPickerClick);
			componentButton->setBorderColor(255, 255, 255, 255);
			componentButton->setRenderBorder(true);
			componentButton->setRender(true);
			componentButton->setActive(true);
			componentPickerGrid->addWidget(componentButton, i % cellsWidth, i / cellsHeight);
		}
		boxLayout->addWidget(componentPickerGrid);
	}

	addWidgetToEditor(boxLayout);
}

AddComponentEditor::~AddComponentEditor()
{

}

void AddComponentEditor::onComponentPickerClick(Widget* sender, WidgetEvent action, void* data)
{
	EntityEditor* entityEditor = EntityEditor::getSingleton();

	if (!entityEditor->selectedEntities.empty())
	{
		uint8_t groupId = sender->getId();

		EntityGroup* componentGroup = EntityManager::getSingleton()->getComponentGroup(groupId);

		std::list<Entity*>::iterator it = entityEditor->selectedEntities.begin();

		for (it; it != entityEditor->selectedEntities.end(); ++it)
		{
			EntityComponent* component = componentGroup->createComponent((*it));
		}

		EntityListEditor::getSingleton()->refreshComponentList();
	}
}

EntityListEditor::EntityListEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Entity List", parent)
{
	singleton = this;

	setActive(false);
	setRender(false);

	const int32_t componentCellHeight = 60;
	const int32_t componentCellWidth  = 150;
	const int32_t entityCellWidth     = 110;

	int32_t widthR, heightR;
	DisplayManager::getResolution(&widthR, &heightR);

	const int32_t scrollbarWidth      = 10;
	const int32_t componentTableWidth = componentCellWidth  * componentNumList;
	const int32_t tableHeight         = (componentCellHeight * entityNumList);
	const int32_t tableWidth          = (entityCellWidth + componentTableWidth) + scrollbarWidth;

	FlowLayout* flowLayout = new FlowLayout(0, nullptr, (uint32_t)Margin::LEFT | (uint32_t)Margin::TOP, 0, 0);
	flowLayout->setSize(tableWidth, tableHeight + scrollbarWidth);
	fillFlowLayout(flowLayout);

	{
		//Entity list View
		entityScrollbar = new Scrollbar(0, nullptr);
		fillScrollbar(entityScrollbar, setup::entities::MAX_ENTITIES - entityNumList, 0.0f);
		entityScrollbar->setVertical();
		entityScrollbar->setSize(10, tableHeight);
		entityScrollbar->setValue(0);
		entityScrollbar->setBorderColor(255, 255, 255, 255);
		entityScrollbar->setRenderBorder(true);
		entityScrollbar->setRender(true);
		entityScrollbar->setActive(true);
		entityScrollbar->setOnChangeEvent(this, &EntityListEditor::onScrollbarChange);
		flowLayout->addWidget(entityScrollbar);

		entityListGrid = new GridLayout(1, nullptr, 1, entityNumList, 0, 0);
		fillGridLayout(entityListGrid);
		entityListGrid->setSize(entityCellWidth, tableHeight);

		for (int32_t i = 0; i < entityNumList; ++i)
		{
			entitiesButtons[i] = new Button(i, nullptr);
			fillButton(entitiesButtons[i], "");
			entitiesButtons[i]->bUserData = false;
			entitiesButtons[i]->vUserData = nullptr;
			entitiesButtons[i]->setRenderBorder(true);
			entitiesButtons[i]->setBorderColor(255, 255, 255, 255);
			entitiesButtons[i]->setTextValue("%hu", i);
			entitiesButtons[i]->setOnClickEvent(this, &EntityListEditor::onEntityClick);
			entityListGrid->addWidget(entitiesButtons[i], 0, i);
		}

		flowLayout->addWidget(entityListGrid);
	}

	{
		//Component list view
		BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 1);
		boxLayout->setSize(componentTableWidth, tableHeight + scrollbarWidth);
		fillBoxLayout(boxLayout);

		componentListGrid = new GridLayout(0, nullptr, componentNumList, entityNumList, 0, 0);
		fillGridLayout(this->componentListGrid);
		componentListGrid->setSize(componentNumList * componentCellWidth, tableHeight);
		boxLayout->addWidget(this->componentListGrid);

		for (int32_t i = 0; i<entityNumList; ++i)
		{
			for (int32_t z = 0; z<componentNumList; ++z)
			{
				ImageButton* imageButton = new ImageButton(i * 10 + z, nullptr);
				fillButton(imageButton,"");
				Label* imageLabel = imageButton->getLabel();
				//imageLabel->setShadowColor(0, 0, 0, 128);
				imageLabel->setShadowColor(0, 0, 0, 255);
				imageLabel->setShadowOffset(-2, -2);
				//imageLabel->setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
				imageButton->setBorderColor(255, 255, 255, 255);
				imageButton->setRenderBorder(true);
				imageButton->setOnClickEvent(this, &EntityListEditor::onComponentListClick);
				imageButton->vUserData = nullptr;
				imageButton->bUserData = false;
				imageButton->setDragable(true);
				componentsList[i][z] = imageButton;
				componentListGrid->addWidget(imageButton, z, i);
				//imageLabel->setSize(imageButton->getWidth(), imageButton->getHeight());
				//imageLabel->setPosition(imageButton->getWidth() / 2, imageButton->getHeight() / 2);
				//imageButton->setTextOffset(0, -2);
			}
		}

		componentScrollbar = new Scrollbar(0, nullptr);
		fillScrollbar(componentScrollbar,0,15);
		componentScrollbar->setHorizontal();
		componentScrollbar->setSize(componentTableWidth, 10);
		componentScrollbar->setValue(0);
		componentScrollbar->setBorderColor(255, 255, 255, 255);
		componentScrollbar->setRenderBorder(true);
		componentScrollbar->setOnChangeEvent(this, &EntityListEditor::onScrollbarChange);
		boxLayout->addWidget(this->componentScrollbar);

		flowLayout->addWidget(boxLayout);
	}

	addWidgetToEditor(flowLayout);

	entityPopup = new EntityPopup(this);
	addWidget(entityPopup);
	entityPopup->setModal(true);
	componentPopup = new ComponentPopup(this);
	addWidget(componentPopup);
	componentPopup->setModal(true);
}

EntityListEditor::~EntityListEditor()
{

}

void EntityListEditor::updateEditorsData(ja::EntityComponent* entityComponent)
{
	EntityComponentMenu::getSingleton()->updateComponentEditors(entityComponent);
}

void EntityListEditor::refresh()
{
	refreshEntityList();
	refreshComponentList();
}

void EntityListEditor::refreshEntityList()
{
	uint16_t       firstId       = (uint16_t)(floor(entityScrollbar->getValue()));
	EntityManager* entityManager = EntityManager::getSingleton();
	EntityEditor*  entityEditor  = EntityEditor::getSingleton();
	Entity*        tempEntity;

	for (uint16_t i = 0; i<entityNumList; ++i)
	{
		tempEntity = entityManager->getEntity(firstId + i);
		Button* entityButton = entitiesButtons[i];

		if (tempEntity == nullptr)
		{
			entityButton->setTextValue("%hu", firstId + i);
			entityButton->bUserData = false;
			entityButton->vUserData = nullptr;
			entityButton->setBorderColor(255, 255, 255, 255);
		}
		else
		{
			if (entityEditor->isInSelection(tempEntity))
			{
				entityButton->setBorderColor(0, 255, 0, 255);
			}
			else
			{
				entityButton->setBorderColor(255, 255, 255, 255);
			}

			if (tempEntity->getHash() == 0)
			{
				entityButton->setTextValue("Entity %hu", tempEntity->getId());
			}
			else
			{
				const char* tagName = tempEntity->getTagName();
				entityButton->setTextValue("%s %hu", tagName, tempEntity->getId());
			}

			entityButton->bUserData = true;
			entityButton->vUserData = tempEntity;
		}

		updateComponentList(i, tempEntity);
	}
}

void EntityListEditor::refreshComponentList()
{
	uint16_t       firstId       = (uint16_t)(floor(entityScrollbar->getValue()));
	EntityManager* entityManager = EntityManager::getSingleton();
	Entity*        tempEntity;

	for (uint16_t i = 0; i<entityNumList; ++i)
	{
		tempEntity = entityManager->getEntity(firstId + i);
		updateComponentList(i, tempEntity);
	}
}

void EntityListEditor::updateComponentList(uint16_t tableId, Entity* tableEntity)
{
	EntityComponent* itComp = nullptr;

	if (tableEntity != nullptr)
	{
		itComp = tableEntity->getFirstComponent();
		uint16_t firstComponentId = (uint16_t)(floor(componentScrollbar->getValue()));

		while ((firstComponentId != 0) && (itComp != nullptr))
		{
			itComp = itComp->nextComponent;
			--firstComponentId;
		}
	}	

	for (int32_t i = 0; i<componentNumList; ++i)
	{
		if (itComp != nullptr)
		{
			uint8_t componentGroup = itComp->getComponentType();
			Texture2d* texImage    = itComp->getComponentGroup()->getTextureImage();

			componentsList[tableId][i]->setImageTexture(texImage);
			componentsList[tableId][i]->setSize(28, 28);
			
			const char* tagName = itComp->getTagName();

			switch (componentGroup)
			{
				case (uint8_t)EntityGroupType::PHYSIC_OBJECT:
				{
					PhysicComponent* physicComponent = (PhysicComponent*)itComp;

					const char* rootText = "root";
					char* isRoot = "";


					if (physicComponent->isRoot())
					{
						isRoot = (char*)rootText;
					}
					
					componentsList[tableId][i]->setTextValue("%s\n%s", tagName, isRoot);
				}
				break;
				case (uint8_t)EntityGroupType::LAYER_OBJECT:
				{
					LayerObjectComponent* layerObjectComponent = (LayerObjectComponent*)itComp;

					PhysicObject2d* physicObject = layerObjectComponent->getPhysicObject();

					char* physicComponentName = nullptr;

					if (physicObject != nullptr)
					{
						PhysicComponent* physicComponent = (PhysicComponent*)physicObject->getComponentData();
						physicComponentName = (char*)physicComponent->getTagName();
					}
					 
					componentsList[tableId][i]->setTextValue("%s\n%s", tagName, physicComponentName);
				}
				break;
				case (uint8_t)EntityGroupType::SCRIPT:
				{
					ScriptComponent* scriptComponent = (ScriptComponent*)itComp;

					char* tagName      = (char*)scriptComponent->getTagName();
					char* fileName     = nullptr;
					char* functionName = nullptr;
				

					if (0 != scriptComponent->getUpdateHash())
					{

						const ScriptFunction* scriptFunction = ScriptManager::getSingleton()->getScriptFunction(ScriptFunctionType::UPDATE_ENTITY, scriptComponent->getUpdateFileHash(), scriptComponent->getUpdateHash());
						
						fileName     = (char*)scriptFunction->fileName;
						functionName = (char*)scriptFunction->functionName;
					}

					componentsList[tableId][i]->setTextValue("%s\n%s\n%s", tagName, fileName, functionName);
				}
				break;
				case (uint8_t)EntityGroupType::DATA:
				{
					DataComponent* dataComponent = (DataComponent*)itComp;

					char* dataModelName = nullptr;
					
					DataModel* dataModel = dataComponent->getDataModel();
					if (nullptr != dataModel)
					{
						dataModelName = (char*)dataModel->getName();
					}

					char* tagName = (char*)dataComponent->getTagName();
					componentsList[tableId][i]->setTextValue("%s\n%s", tagName, dataModelName);
				}
				break;
			}

			componentsList[tableId][i]->vUserData = itComp;
			itComp = itComp->nextComponent;
		}
		else
		{
			componentsList[tableId][i]->setImageTexture(nullptr);
			componentsList[tableId][i]->vUserData = nullptr;
			componentsList[tableId][i]->setText("");
			componentsList[tableId][i]->setBorderColor(255, 255, 255, 255);
		}
	}
}

void EntityListEditor::onScrollbarChange(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	if (sender->getWidgetType() == JA_SCROLLBAR)
	{
		Scrollbar* bar = (Scrollbar*)(sender);

		if (sender == entityScrollbar)
		{
			refreshEntityList();
		}

		if (sender == componentScrollbar)
		{
			refreshComponentList();
		}
	}
}

void EntityListEditor::onEntityClick(Widget* sender, WidgetEvent action, void* data)
{
	uint8_t mouseButtons = *(uint8_t*)(data);

	EntityEditor* entityEditor = EntityEditor::getSingleton();

	if (sender->getWidgetType() == JA_BUTTON)
	{
		uint32_t entityId     = sender->getId();
		uint32_t firstId      = (uint32_t)(floor(entityScrollbar->getValue()));
		Button*  entityButton = (Button*)(sender);
		Entity*  entity       = (Entity*)(entityButton->vUserData);

		if (mouseButtons & 2)
		{
			if (entity != nullptr)
			{
				entityEditor->addToSelection(entity);
				entityPopup->setEnable(entityPopup->deleteEntity);
				entityPopup->setDisable(entityPopup->createEntity);
				entityPopup->tagTextField->setActive(true);

				if (entity->getHash() != 0)
				{
					const char* hashText = entity->getTagName();
					entityPopup->tagTextField->setText(hashText);
				}

				entityPopup->show();
			}
			else
			{
				entityPopup->createEntity->vUserData = entityButton;
				entityPopup->setEnable(entityPopup->createEntity);
				entityPopup->setDisable(entityPopup->deleteEntity);
				entityPopup->tagTextField->setActive(false);
				entityPopup->show();
			}
		}

		if (mouseButtons & 1)
		{
			if (entity != nullptr)
			{
				if (InputManager::isKeyModPress(JA_KMOD_LSHIFT)) // Add to selection
				{
					entityEditor->addToSelection(entity);
				}
				else // Clear selection and add
				{
					entityEditor->selectedEntities.clear();
					entityEditor->addToSelection(entity);
				}

				refreshEntityList();
			}
		}
	}
}

void EntityListEditor::onComponentListClick(Widget* sender, WidgetEvent action, void* data)
{
	uint8_t           mouseButtons     = *(uint8_t*)(data);
	ImageButton*      imageButton      = (ImageButton*)(sender);
	EntityComponent*  entityComponent  = (EntityComponent*)(imageButton->vUserData);

	if (mouseButtons & 2)
	{
		if (entityComponent != nullptr)
		{
			componentPopup->entityComponent = entityComponent;

			componentPopup->show();
		}
	}
}

ScriptComponentEditor::ScriptComponentEditor( uint32_t widgetId, Widget* parent ) : CommonWidget(widgetId,"Script",parent)
{
	this->scriptComponent = nullptr;

	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 350;

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, 2 * (20 + 2));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* tFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 5, 0);
		tFlow->setSize(widgetWidth, 20);
		fillFlowLayout(tFlow);

		Label* tagLabel = new Label(0, nullptr);
		fillLabel(tagLabel, "Tag");

		tagTextField = new TextField(1, nullptr);
		fillTextField(tagTextField);
		tagTextField->setTextSize(setup::TAG_NAME_LENGTH, 20);
		tagTextField->setOnDeselectEvent(this, &ScriptComponentEditor::onTagDeselect);

		tFlow->addWidget(tagLabel);
		tFlow->addWidget(tagTextField);

		boxLayout->addWidget(tFlow);
	}

	{
		FlowLayout* flowLayout = new FlowLayout(4, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		flowLayout->setSize(widgetWidth, 20);
		fillFlowLayout(flowLayout);

		Label* functionLabel = new Label(0, nullptr);
		fillLabel(functionLabel, "Function");

		functionButton = new Button(1, nullptr);
		fillButton(functionButton, "Set");
		functionButton->setSize(250, 20);
		functionButton->setOnClickEvent(this, &ScriptComponentEditor::onScriptClick);

		flowLayout->addWidget(functionLabel);
		flowLayout->addWidget(functionButton);
		boxLayout->addWidget(flowLayout);
	}
	
	notifierCallback.bind(this,&ScriptComponentEditor::onNotify);

	addWidgetToEditor(boxLayout);
}

ScriptComponentEditor::~ScriptComponentEditor()
{

}

void ScriptComponentEditor::onTagDeselect(Widget* sender, WidgetEvent action, void* data)
{
	const char* tagNamePtr = tagTextField->getTextPtr();

	scriptComponent->setTagName(tagNamePtr);
	EntityListEditor::getSingleton()->refreshComponentList();
}

void ScriptComponentEditor::onScriptClick( Widget* sender, WidgetEvent action, void* data )
{
	ScriptFunctionViewer* scriptFunctionViewer = ScriptFunctionViewer::getSingleton();
	scriptFunctionViewer->refresh(ScriptFunctionType::UPDATE_ENTITY);
	scriptFunctionViewer->setNotifier(&notifierCallback);
	scriptFunctionViewer->setActive(true);
	scriptFunctionViewer->setRender(true);
}

void ScriptComponentEditor::onNotify( Widget* sender, WidgetEvent action, void* data )
{
	ScriptFunctionViewer* scriptFunctionViewer = ScriptFunctionViewer::getSingleton();

	if(scriptFunctionViewer->chosenFunction != nullptr)
	{
		ScriptFunction* chosenFunction = scriptFunctionViewer->chosenFunction;
		functionButton->setTextValue("%s\n%s", chosenFunction->fileName, chosenFunction->functionName);
		scriptComponent->setUpdateFunction(*scriptFunctionViewer->chosenFunction);
	}	

	EntityListEditor::getSingleton()->refreshComponentList();
}

void ScriptComponentEditor::updateWidget(void* entityComponent )
{
	EntityComponent* component = (EntityComponent*)entityComponent;

	if (nullptr == component)
	{
		this->scriptComponent = nullptr;
		setRender(false);
		setActive(false);
		return;
	}

	if(component->getComponentType() != (uint8_t)EntityGroupType::SCRIPT)
		return;

	setRender(true);
	setActive(true);
	UIManager::getSingleton()->setTopWidget(this);

	this->scriptComponent = (ScriptComponent*)component;
	tagTextField->setText(scriptComponent->getTagName());

	if(scriptComponent->getUpdateHash() != 0)
	{
		const ja::ScriptFunction* scriptFunction = ScriptManager::getSingleton()->getScriptFunction(ScriptFunctionType::UPDATE_ENTITY, scriptComponent->getUpdateFileHash(), scriptComponent->getUpdateHash());
		functionButton->setTextValue("%s\n%s", scriptFunction->fileName, scriptFunction->functionName);
	}
	else
	{
		functionButton->setText("Set");
	}
}

DataComponentEditor::DataComponentEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Data", parent)
{
	this->dataComponent = nullptr;

	setRender(false);
	setActive(false);

	const int32_t widgetWidth      = 350;
	const int32_t scrollPaneHeight = (7 * (20 + 2) + 10);

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, (1 * (20 + 2)) + (1 * (28 + 2)) + scrollPaneHeight);
	fillBoxLayout(boxLayout);
	
	{
		FlowLayout* tFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 5, 0);
		tFlow->setSize(widgetWidth, 20);
		fillFlowLayout(tFlow);

		Label* tagLabel = new Label(0, nullptr);
		fillLabel(tagLabel, "Tag");

		tagTextField = new TextField(1, nullptr);
		fillTextField(tagTextField);
		tagTextField->setTextSize(setup::TAG_NAME_LENGTH, 20);
		tagTextField->setOnDeselectEvent(this, &DataComponentEditor::onTagDeselect);

		tFlow->addWidget(tagLabel);
		tFlow->addWidget(tagTextField);

		boxLayout->addWidget(tFlow);
	}

	{
		FlowLayout* flowLayout = new FlowLayout(4, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		flowLayout->setSize(widgetWidth, 28);
		fillFlowLayout(flowLayout);

		Label* dataModelLabel = new Label(0, nullptr);
		fillLabel(dataModelLabel, "Data Model");

		dataModelButton = new Button(1, nullptr);
		fillButton(dataModelButton, "Set");
		dataModelButton->setSize(250, 28);
		dataModelButton->setOnClickEvent(this, &DataComponentEditor::onDataModelClick);

		flowLayout->addWidget(dataModelLabel);
		flowLayout->addWidget(dataModelButton);
		boxLayout->addWidget(flowLayout);
	}

	{
		this->scrollPane = new ScrollPane(4, nullptr);
		this->scrollPane->setSize(widgetWidth, scrollPaneHeight);
		fillScrollPane(this->scrollPane);

		this->scrollPane->setColor(0, 255, 0, 128);

		boxLayout->addWidget(this->scrollPane);
	}

	{
		dataLabel.setFont("default.ttf",10);
		dataLabel.setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
		dataLabel.setColor(255, 255, 255, 255);
		dataLabel.setText("");
	}

	notifierCallback.bind(this, &DataComponentEditor::onNotify);

	addWidgetToEditor(boxLayout);
}

DataComponentEditor::~DataComponentEditor()
{

}

void DataComponentEditor::updateWidget(void* entityComponent)
{
	EntityComponent* component = (EntityComponent*)entityComponent;

	if (nullptr == component)
	{
		this->dataComponent = nullptr;
		setRender(false);
		setActive(false);
		return;
	}

	if (component->getComponentType() != (uint8_t)EntityGroupType::DATA)
		return;

	setRender(true);
	setActive(true);
	UIManager::getSingleton()->setTopWidget(this);

	this->dataComponent = (DataComponent*)component;
	tagTextField->setText(dataComponent->getTagName());

	if (dataComponent->getDataModel() != nullptr)
	{
		const ja::DataModel* dataModel = dataComponent->getDataModel();
		dataModelButton->setTextValue("%s\n%s", dataModel->getFileName(), dataModel->getName());

		refreshDataFieldViewer();
	}
	else
	{
		dataModelButton->setText("Set");
	}
}

void DataComponentEditor::onTagDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	const char* tagNamePtr = tagTextField->getTextPtr();

	dataComponent->setTagName(tagNamePtr);
	EntityListEditor::getSingleton()->refreshComponentList();
}

void DataComponentEditor::onDataModelClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	DataModelViewer* dataModelViewer = DataModelViewer::getSingleton();
	dataModelViewer->refresh();
	dataModelViewer->setNotifier(&notifierCallback);
	dataModelViewer->setActive(true);
	dataModelViewer->setRender(true);
}

void DataComponentEditor::onNotify(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	DataModelViewer* dataModelViewer = DataModelViewer::getSingleton();

	if (dataModelViewer->chosenDataModel != nullptr)
	{
		DataModel* chosenDataModel = dataModelViewer->chosenDataModel;
		
		
		if (nullptr == dataComponent->getDataModel())
		{
			dataComponent->setDataModel(chosenDataModel);
			dataComponent->initData();
		}
		else
		{
			dataComponent->setDataModel(chosenDataModel); // copy old data if exist
		}

		dataModelButton->setTextValue("%s\n%s", chosenDataModel->getName(), chosenDataModel->getFileName());
		refreshDataFieldViewer();
	}

	EntityListEditor::getSingleton()->refreshComponentList();
}

void DataComponentEditor::refreshDataFieldViewer()
{
	this->scrollPane->clearWidgets();
	this->dataNotifiers.clear();

	DataModel*                     dataModel       = this->dataComponent->getDataModel();
	StackAllocator<DataFieldInfo>* dataFieldsStack = dataModel->getDataFields();
	const uint32_t                 dataFieldsNum   = dataFieldsStack->getSize();
	DataFieldInfo*                 pDataField      = dataFieldsStack->getFirst();

	uint32_t fieldsNum = 0;

	for (uint32_t i = 0; i < dataFieldsNum; ++i)
	{
		fieldsNum += pDataField[i].getElementsNum();
	}

	const uint32_t gridLayoutHeight = fieldsNum  * (20 + 2);
	const int32_t  scrollPaneWidth  = this->scrollPane->getWidth();

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(scrollPaneWidth, gridLayoutHeight);

	boxLayout->setPosition(0, 0);
	boxLayout->setColor(0, 0, 255, 150);

	this->dataNotifiers.resize(sizeof(DataNotifier) * fieldsNum);
	DataNotifier* pDataNotifier = this->dataNotifiers.getFirst();

	for (uint32_t i = 0; i < dataFieldsNum; ++i)
	{
		const uint32_t elementsNum = pDataField[i].getElementsNum();

		for (uint32_t z = 0; z < elementsNum; ++z)
		{			
			DataNotifier* fieldNotifier = new (&pDataNotifier[i + z]) DataNotifier(dataComponent->getAllocatedData(), z, &pDataField[i]);

			fieldNotifier->setDataChangeCallback(this, &DataComponentEditor::onDataChange);
			Widget* widget = fieldNotifier->createDataWidget();

			boxLayout->addWidget(widget);
		}	
	}

	scrollPane->addWidget(boxLayout);
	scrollPane->setScrollY(INT_MAX);
	scrollPane->setVerticalScrollbarPolicy(ScrollbarPolicy::JA_SCROLLBAR_ALWAYS);
}

void DataComponentEditor::onDataChange(DataNotifier* dataNotifier)
{
	if (nullptr == this->dataComponent)
		return;
	
	this->dataComponent->callDataAccess(dataNotifier->getDataFieldInfo(), dataNotifier->getElementId());
}

PhysicComponentEditor::PhysicComponentEditor( uint32_t widgetId, Widget* parent ) : CommonWidget(widgetId,"Physic Object",parent)
{
	this->physicComponent = nullptr;

	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 360;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, 6 * (28 + 2));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* tFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 5, 0);
		tFlow->setSize(widgetWidth, 28);
		fillFlowLayout(tFlow);

		Label* tagLabel = new Label(0, nullptr);
		fillLabel(tagLabel, "Tag");

		tagTextField = new TextField(1, nullptr);
		fillTextField(tagTextField);
		tagTextField->setTextSize(setup::TAG_NAME_LENGTH, 28);
		tagTextField->setOnDeselectEvent(this, &PhysicComponentEditor::onTagDeselect);

		tFlow->addWidget(tagLabel);
		tFlow->addWidget(tagTextField);

		boxLayout->addWidget(tFlow);
	}
	
	{
		FlowLayout* pFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 5, 0);
		pFlow->setSize(widgetWidth, 28);
		fillFlowLayout(pFlow);

		Label* pickObjectLabel = new Label(0, nullptr);
		fillLabel(pickObjectLabel, "Physic object");

		assignPhysicObjectButton = new Button(1, nullptr);
		fillButton(assignPhysicObjectButton, "Assign");
		assignPhysicObjectButton->setOnClickEvent(this, &PhysicComponentEditor::onAssignPhysicObjectClick);

		selectPhysicObjectButton = new Button(2, nullptr);
		fillButton(selectPhysicObjectButton, "Select");
		selectPhysicObjectButton->setOnClickEvent(this, &PhysicComponentEditor::onSelectPhysicObjectClick);

		pFlow->addWidget(pickObjectLabel);
		pFlow->addWidget(assignPhysicObjectButton);
		pFlow->addWidget(selectPhysicObjectButton);

		boxLayout->addWidget(pFlow);
	}
	
	{
		FlowLayout* rFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 5, 0);
		rFlow->setSize(widgetWidth, 28);
		fillFlowLayout(rFlow);

		Label* rootLabel = new Label(0, nullptr);
		fillLabel(rootLabel, "Root");

		rootCheckbox = new Checkbox(2, nullptr);
		fillCheckbox(rootCheckbox);
		rootCheckbox->setOnClickEvent(this, &PhysicComponentEditor::onRootChange);

		rFlow->addWidget(rootLabel);
		rFlow->addWidget(rootCheckbox);

		boxLayout->addWidget(rFlow);
	}
	
	{
		FlowLayout* aFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 0, 0);
		aFlow->setSize(widgetWidth, 28);
		fillFlowLayout(aFlow);

		Label* autoDeleteLabel = new Label(0, nullptr);
		fillLabel(autoDeleteLabel, "Auto delete");

		autoDeleteCheckbox = new Checkbox(3, nullptr);
		fillCheckbox(autoDeleteCheckbox);
		autoDeleteCheckbox->setOnClickEvent(this, &PhysicComponentEditor::onAutoDeleteChange);

		aFlow->addWidget(autoDeleteLabel);
		aFlow->addWidget(autoDeleteCheckbox);

		boxLayout->addWidget(aFlow);
	}

	{
		FlowLayout* flowLayout = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		flowLayout->setSize(widgetWidth, 28);
		fillFlowLayout(flowLayout);

		Label* functionLabel = new Label(0, nullptr);
		fillLabel(functionLabel, "onBeginContact");

		onBeginContactButton = new Button(1, nullptr);
		fillButton(onBeginContactButton, "Set");
		onBeginContactButton->setSize(250, 28);
		onBeginContactButton->setOnClickEvent(this, &PhysicComponentEditor::onBeginContactClick);

		onBeginContactNotifierCallback.bind(this, &PhysicComponentEditor::onBeginContactNotify);

		flowLayout->addWidget(functionLabel);
		flowLayout->addWidget(onBeginContactButton);
		boxLayout->addWidget(flowLayout);
	}

	{
		FlowLayout* flowLayout = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		flowLayout->setSize(widgetWidth, 28);
		fillFlowLayout(flowLayout);

		Label* functionLabel = new Label(0, nullptr);
		fillLabel(functionLabel, "onEndContact");

		onEndContactButton = new Button(1, nullptr);
		fillButton(onEndContactButton, "Set");
		onEndContactButton->setSize(250, 28);
		onEndContactButton->setOnClickEvent(this, &PhysicComponentEditor::onEndContactClick);

		onEndContactNotifierCallback.bind(this, &PhysicComponentEditor::onEndContactNotify);

		flowLayout->addWidget(functionLabel);
		flowLayout->addWidget(onEndContactButton);
		boxLayout->addWidget(flowLayout);
	}
	
	addWidgetToEditor(boxLayout);
}


void PhysicComponentEditor::updateWidget( void* entityComponent )
{
	if (nullptr == entityComponent)
	{
		physicComponent = nullptr;
		setActive(false);
		setRender(false);
		return;
	}

	EntityComponent* component = (EntityComponent*)entityComponent;

	if (component->getComponentType() != (uint8_t)EntityGroupType::PHYSIC_OBJECT)
	{
		return;
	}
	
	setActive(true);
	setRender(true);
	UIManager::getSingleton()->setTopWidget(this);

	physicComponent = (PhysicComponent*)(entityComponent);

	tagTextField->setText(physicComponent->getTagName());
	
	autoDeleteCheckbox->setCheck(physicComponent->autoDelete);
	rootCheckbox->setCheck(physicComponent->isRoot());

	if (physicComponent->getOnBeginConstraintHash() != 0)
	{
		uint32_t fileHash     = physicComponent->getOnBeginConstraintFileHash();
		uint32_t functionHash = physicComponent->getOnBeginConstraintHash();
		const ja::ScriptFunction* scriptFunction = ScriptManager::getSingleton()->getScriptFunction(ScriptFunctionType::CONSTRAINT_NOTIFIER, fileHash, functionHash);
		this->onBeginContactButton->setTextValue("%s\n%s", scriptFunction->fileName, scriptFunction->functionName);
	}
	else
	{
		this->onBeginContactButton->setText("Set");
	}

	if (physicComponent->getOnEndConstraintHash() != 0)
	{
		uint32_t fileHash     = physicComponent->getOnEndConstraintFileHash();
		uint32_t functionHash = physicComponent->getOnEndConstraintHash();
		const ja::ScriptFunction* scriptFunction = ScriptManager::getSingleton()->getScriptFunction(ScriptFunctionType::CONSTRAINT_NOTIFIER, fileHash, functionHash);
		this->onEndContactButton->setTextValue("%s\n%s", scriptFunction->fileName, scriptFunction->functionName);
	}
	else
	{
		this->onEndContactButton->setText("Set");
	}

	EntityListEditor::getSingleton()->refreshComponentList();
}

void PhysicComponentEditor::onTagDeselect( Widget* sender, WidgetEvent action, void* data )
{
	const char* tagNamePtr = tagTextField->getTextPtr();
	
	physicComponent->setTagName(tagNamePtr);
	EntityListEditor::getSingleton()->refreshComponentList();
}


void PhysicComponentEditor::onAssignPhysicObjectClick( Widget* sender, WidgetEvent action, void* data )
{
	if(physicComponent != nullptr)
	{
		PhysicObject2d* physicObject = nullptr;
		if(PhysicsEditor::getSingleton()->selectedPhysicObjects.size() != 0)
		{
			physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		}

		if(physicObject->getComponentData() == nullptr)
			physicComponent->setPhysicObject(physicObject);
		else
			physicComponent->setPhysicObject(nullptr);
	}
	EntityListEditor::getSingleton()->refreshComponentList();
}

void PhysicComponentEditor::onSelectPhysicObjectClick(Widget* sender, WidgetEvent action, void* data)
{
	if (physicComponent == nullptr)
		return;

	PhysicObject2d* physicObject = physicComponent->getPhysicObject();

	if (physicObject == nullptr)
		return;

	if (InputManager::isKeyModPress(JA_KMOD_LSHIFT))
	{
		PhysicsEditor::getSingleton()->addToSelection(physicObject);
	}
	else
	{
		if (InputManager::isKeyModPress(JA_KMOD_LCTRL))
		{
			PhysicsEditor::getSingleton()->removeFromSelection(physicObject);
		}
		else
		{
			PhysicsEditor::getSingleton()->selectedPhysicObjects.clear();
			PhysicsEditor::getSingleton()->addToSelection(physicObject);
		}
	}
}

void PhysicComponentEditor::onRootChange( Widget* sender, WidgetEvent action, void* data )
{
	if(!this->rootCheckbox->isChecked())
		physicComponent->setRoot();

	EntityListEditor::getSingleton()->refreshComponentList();
}

void PhysicComponentEditor::onAutoDeleteChange( Widget* sender, WidgetEvent action, void* data )
{
	bool autoDelete = autoDeleteCheckbox->isChecked();

	physicComponent->autoDelete = autoDelete;
}

void PhysicComponentEditor::onBeginContactClick(Widget* sender, WidgetEvent action, void* data)
{
	ScriptFunctionViewer* scriptFunctionViewer = ScriptFunctionViewer::getSingleton();
	scriptFunctionViewer->refresh(ScriptFunctionType::CONSTRAINT_NOTIFIER);
	scriptFunctionViewer->setNotifier(&this->onBeginContactNotifierCallback);
	scriptFunctionViewer->setActive(true);
	scriptFunctionViewer->setRender(true);
}

void PhysicComponentEditor::onBeginContactNotify(Widget* sender, WidgetEvent action, void* data)
{
	ScriptFunctionViewer* scriptFunctionViewer = ScriptFunctionViewer::getSingleton();

	if (scriptFunctionViewer->chosenFunction != nullptr)
	{
		ScriptFunction* chosenFunction = scriptFunctionViewer->chosenFunction;
		onBeginContactButton->setTextValue("%s\n%s", chosenFunction->fileName, chosenFunction->functionName);
		physicComponent->setBeginConstraintFunction(*scriptFunctionViewer->chosenFunction);
	}

	EntityListEditor::getSingleton()->refreshComponentList();
}

void PhysicComponentEditor::onEndContactClick(Widget* sender, WidgetEvent action, void* data)
{
	ScriptFunctionViewer* scriptFunctionViewer = ScriptFunctionViewer::getSingleton();
	scriptFunctionViewer->refresh(ScriptFunctionType::CONSTRAINT_NOTIFIER);
	scriptFunctionViewer->setNotifier(&this->onEndContactNotifierCallback);
	scriptFunctionViewer->setActive(true);
	scriptFunctionViewer->setRender(true);
}

void PhysicComponentEditor::onEndContactNotify(Widget* sender, WidgetEvent action, void* data)
{
	ScriptFunctionViewer* scriptFunctionViewer = ScriptFunctionViewer::getSingleton();

	if (scriptFunctionViewer->chosenFunction != nullptr)
	{
		ScriptFunction* chosenFunction = scriptFunctionViewer->chosenFunction;
		onEndContactButton->setTextValue("%s\n%s", chosenFunction->fileName, chosenFunction->functionName);
		physicComponent->setEndConstraintFunction(*scriptFunctionViewer->chosenFunction);
	}

	EntityListEditor::getSingleton()->refreshComponentList();
}

LayerObjectComponentEditor::LayerObjectComponentEditor( uint32_t widgetId, Widget* parent) : CommonWidget(widgetId,"Layer Object",parent)
{
	this->layerObjectComponent = nullptr;

	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 220;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, 5 * (28 + 2));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* tFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 5, 0);
		tFlow->setSize(widgetWidth, 28);
		fillFlowLayout(tFlow);

		Label* tagLabel = new Label(0, nullptr);
		fillLabel(tagLabel, "Tag");

		tagTextField = new TextField(1, nullptr);
		fillTextField(tagTextField);
		tagTextField->setTextSize(setup::TAG_NAME_LENGTH, 28);
		tagTextField->setOnDeselectEvent(this, &LayerObjectComponentEditor::onTagDeselect);

		tFlow->addWidget(tagLabel);
		tFlow->addWidget(tagTextField);

		boxLayout->addWidget(tFlow);
	}

	{
		FlowLayout* sFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 5, 0);
		sFlow->setSize(widgetWidth, 28);
		fillFlowLayout(sFlow);

		Label* spriteLabel = new Label(0, nullptr);
		fillLabel(spriteLabel, "Layer Objects");

		assignLayerObjectsButton = new Button(4, nullptr);
		fillButton(assignLayerObjectsButton, "Assign");
		assignLayerObjectsButton->setOnClickEvent(this, &LayerObjectComponentEditor::onAssignLayerObjectClick);

		selectAssignedLayerObjectsButton = new Button(5, nullptr);
		fillButton(selectAssignedLayerObjectsButton, "Select");
		selectAssignedLayerObjectsButton->setOnClickEvent(this, &LayerObjectComponentEditor::onSelectAssignedLayerObjectsClick);

		sFlow->addWidget(spriteLabel);
		sFlow->addWidget(assignLayerObjectsButton);
		sFlow->addWidget(selectAssignedLayerObjectsButton);

		boxLayout->addWidget(sFlow);
	}
	
	{
		FlowLayout* pFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 0, 0);
		pFlow->setSize(widgetWidth, 28);
		fillFlowLayout(pFlow);

		Label* physicLabel = new Label(0, nullptr);
		fillLabel(physicLabel, "Physic component");

		dropPhysicButton = new Button(5, nullptr);
		fillButton(dropPhysicButton, "Set");
		dropPhysicButton->setOnDropEvent(this, &LayerObjectComponentEditor::onSetPhysicComponentDrop);
		dropPhysicButton->adjustToText(60, 28);

		pFlow->addWidget(physicLabel);
		pFlow->addWidget(dropPhysicButton);

		boxLayout->addWidget(pFlow);
	}
	
	{
		FlowLayout* aFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 0, 0);
		aFlow->setSize(widgetWidth, 28);
		fillFlowLayout(aFlow);

		Label* deleteLabel = new Label(0, nullptr);
		fillLabel(deleteLabel, "Auto delete");

		autoDeleteCheckbox = new Checkbox(6, nullptr);
		autoDeleteCheckbox->setOnClickEvent(this, &LayerObjectComponentEditor::onAutoDeleteChange);
		fillCheckbox(autoDeleteCheckbox);

		aFlow->addWidget(deleteLabel);
		aFlow->addWidget(autoDeleteCheckbox);

		boxLayout->addWidget(aFlow);
	}

	{
		FlowLayout* fFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 0, 0);
		fFlow->setSize(widgetWidth, 28);
		fillFlowLayout(fFlow);

		Label* fixedRotationLabel = new Label(0, nullptr);
		fillLabel(fixedRotationLabel, "Fixed rotation");

		fixedRotationCheckbox = new Checkbox(6, nullptr);
		fixedRotationCheckbox->setOnClickEvent(this, &LayerObjectComponentEditor::onFixedRotationChange);
		fillCheckbox(fixedRotationCheckbox);

		fFlow->addWidget(fixedRotationLabel);
		fFlow->addWidget(fixedRotationCheckbox);

		boxLayout->addWidget(fFlow);
	}	

	addWidgetToEditor(boxLayout);
}

void LayerObjectComponentEditor::updateWidget( void* entityComponent )
{
	if (nullptr == entityComponent)
	{
		layerObjectComponent = nullptr;
		setRender(false);
		setActive(false);

		return;
	}

	EntityComponent* component = (EntityComponent*)entityComponent;

	if (component->getComponentType() != (uint8_t)EntityGroupType::LAYER_OBJECT)
	{
		return;
	}

	this->layerObjectComponent = (LayerObjectComponent*)component;

	setActive(true);
	setRender(true);
	UIManager::getSingleton()->setTopWidget(this);

	tagTextField->setText(layerObjectComponent->getTagName());

	autoDeleteCheckbox->setCheck(layerObjectComponent->isAutoDelete());
	fixedRotationCheckbox->setCheck(layerObjectComponent->isFixedRotation());

	if(layerObjectComponent->getPhysicObject() != nullptr)
	{
		PhysicComponent* physicComponent = (PhysicComponent*)(layerObjectComponent->getPhysicObject()->getComponentData());

		if(physicComponent->getHash() != 0)
		{
			const char* tagName = physicComponent->getTagName();
			dropPhysicButton->setText(tagName);
		}
		else
		{
			PhysicObject2d* physicObject = physicComponent->getPhysicObject();
			if(physicObject == nullptr)
			{
				dropPhysicButton->setText("");
			}
			else
			{
				dropPhysicButton->setTextValue("Physic %hu",physicObject->getId());
			}
		}
	}
	else
	{
		dropPhysicButton->setText("Set");
	}
}

void LayerObjectComponentEditor::onSetPhysicComponentDrop( Widget* sender, WidgetEvent action, void* data )
{
	Widget* onDropWidget = (Widget*)(data);
	if(onDropWidget->getWidgetType() != JA_IMAGE_BUTTON)
		return;

	ImageButton* dropButton = (ImageButton*)(onDropWidget);
	if(dropButton->vUserData == nullptr)
		return;

	EntityComponent* entityComponent = (EntityComponent*)(dropButton->vUserData);
	if(entityComponent->getComponentType() != (uint8_t)EntityGroupType::PHYSIC_OBJECT)
		return;

	PhysicComponent*      physicComponent  = (PhysicComponent*)(entityComponent);
	layerObjectComponent->setPhysicComponent(physicComponent);

	if(physicComponent->getHash() != 0)
	{
		const char* tagName = physicComponent->getTagName();
		dropPhysicButton->setText(tagName);
	}
	else
	{
		PhysicObject2d* physicObject = physicComponent->getPhysicObject();
		if(physicObject != nullptr)
			dropPhysicButton->setTextValue("Physic %hu", physicObject->getId());
		else
			dropPhysicButton->setText("");
	}

	EntityListEditor::getSingleton()->refreshComponentList();
}

void LayerObjectComponentEditor::onTagDeselect(Widget* sender, WidgetEvent action, void* data)
{
	const char* tagNamePtr = tagTextField->getTextPtr();

	layerObjectComponent->setTagName(tagNamePtr);
	EntityListEditor::getSingleton()->refreshComponentList();
}

void LayerObjectComponentEditor::onAssignLayerObjectClick( Widget* sender, WidgetEvent action, void* data )
{
	if(layerObjectComponent != nullptr)
	{
		std::list<LayerObject*>& selectedLayerObjects = LayerObjectEditor::getSingleton()->selectedLayerObjects;

		layerObjectComponent->clearLayerObjects();

		if(selectedLayerObjects.size() != 0)
		{
			std::list<LayerObject*>::iterator itLayerObject = selectedLayerObjects.begin();

			for(itLayerObject; itLayerObject != selectedLayerObjects.end(); ++itLayerObject)
			{
				if((*itLayerObject)->componentData == nullptr)
				{
					layerObjectComponent->addLayerObject(*itLayerObject);
				}
			}
		}
	}
}

void LayerObjectComponentEditor::onSelectAssignedLayerObjectsClick( Widget* sender, WidgetEvent action, void* data )
{
	if (nullptr == layerObjectComponent)
		return;

	LayerObjectElement* layerObjectElement = layerObjectComponent->getLayerObjectCollection();

	if (layerObjectElement == nullptr)
		return;

	if (InputManager::isKeyModPress(JA_KMOD_LSHIFT))
	{
		while (layerObjectElement != nullptr)
		{
			LayerObjectEditor::getSingleton()->addToSelection(layerObjectElement->layerObject);
			layerObjectElement = layerObjectElement->getNext();
		}
	}
	else
	{
		if (InputManager::isKeyModPress(JA_KMOD_LCTRL))
		{
			while (layerObjectElement != nullptr)
			{
				LayerObjectEditor::getSingleton()->removeFromSelection(layerObjectElement->layerObject);
				layerObjectElement = layerObjectElement->getNext();
			}
		}
		else
		{
			LayerObjectEditor::getSingleton()->selectedLayerObjects.clear();
			while (layerObjectElement != nullptr)
			{
				LayerObjectEditor::getSingleton()->addToSelection(layerObjectElement->layerObject);
				layerObjectElement = layerObjectElement->getNext();
			}
		}
	}
}

void LayerObjectComponentEditor::onAutoDeleteChange( Widget* sender, WidgetEvent action, void* data )
{
	bool autoDelete = autoDeleteCheckbox->isChecked();

	layerObjectComponent->setAutoDelete(autoDelete);
}

void LayerObjectComponentEditor::onFixedRotationChange(Widget* sender, WidgetEvent action, void* data)
{
	bool fixedRotation = fixedRotationCheckbox->isChecked();

	layerObjectComponent->setFixedRotation(fixedRotation);
}


SoundEditor::SoundEditor(uint32_t widgetId, Widget* parent ) : CommonWidget(widgetId,"Sound",parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	chosenSound = nullptr;
	chosenStream = nullptr;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(210,4 * (28 + 2)); // 210
	fillBoxLayout(boxLayout);

	FlowLayout* soundLayout = new FlowLayout(4, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	soundLayout->setSize(210,28);
	fillFlowLayout(soundLayout);

	Label* soundLabel = new Label(0,nullptr);
	fillLabel(soundLabel,"Sound");

	pickSoundButton = new Button(1,nullptr);
	fillButton(pickSoundButton,"Set");
	pickSoundButton->setSize(soundLayout->getWidth() - soundLabel->getWidth() - 2,28);
	pickSoundButton->setOnClickEvent(this,&SoundEditor::onSoundClick);

	soundLayout->addWidget(soundLabel);
	soundLayout->addWidget(pickSoundButton);

	FlowLayout* loopLayout = new FlowLayout(5, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	loopLayout->setSize(210,28);
	fillFlowLayout(loopLayout);
	Label* loopLabel = new Label(0,nullptr);
	fillLabel(loopLabel,"Loop");
	loopCheckbox = new Checkbox(2,nullptr);
	fillCheckbox(loopCheckbox);
	loopCheckbox->setOnChangeEvent(this, &SoundEditor::onLoopChange);

	loopLayout->addWidget(loopLabel);
	loopLayout->addWidget(loopCheckbox);

	FlowLayout* playLayout = new FlowLayout(5, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	playLayout->setSize(210, 28);
	fillFlowLayout(playLayout);

	playRadioButton = new RadioButton(0,nullptr);
	fillRadioButton(playRadioButton,"play");
	playRadioButton->setOnClickEvent(this,&SoundEditor::onPlayerStateChange);
	pauseRadioButton = new RadioButton(1,nullptr);
	fillRadioButton(pauseRadioButton,"pause");
	pauseRadioButton->setOnClickEvent(this,&SoundEditor::onPlayerStateChange);
	stopRadioButton = new RadioButton(2,nullptr);
	fillRadioButton(stopRadioButton,"stop");
	stopRadioButton->setOnClickEvent(this,&SoundEditor::onPlayerStateChange);

	Label* playLabel = new Label();
	fillLabel(playLabel,"Play");
	Label* pauseLabel = new Label();
	fillLabel(pauseLabel,"Pause");
	Label* stopLabel = new Label();
	fillLabel(stopLabel,"Stop");

	ButtonGroup* buttonGroup = new ButtonGroup(6,nullptr);
	buttonGroup->addButton(playRadioButton);
	buttonGroup->addButton(pauseRadioButton);
	buttonGroup->addButton(stopRadioButton);
	
	playLayout->addWidget(buttonGroup);
	playLayout->addWidget(playLabel);
	playLayout->addWidget(playRadioButton);
	playLayout->addWidget(pauseLabel);
	playLayout->addWidget(pauseRadioButton);
	playLayout->addWidget(stopLabel);
	playLayout->addWidget(stopRadioButton);
	
	FlowLayout* physicLayout = new FlowLayout(6, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
	physicLayout->setSize(210,28);
	fillFlowLayout(physicLayout);

	Label* physicLabel = new Label(0,nullptr);
	fillLabel(physicLabel,"Physic component");

	dropPhysicButton = new Button(5,nullptr);
	fillButton(dropPhysicButton,"Set");
	dropPhysicButton->setOnDropEvent(this,&SoundEditor::onSetPhysicComponentDrop);
	dropPhysicButton->adjustToText(60,28);

	physicLayout->addWidget(physicLabel);
	physicLayout->addWidget(dropPhysicButton);

	notifierCallback.bind(this,&SoundEditor::onNotify);

	boxLayout->addWidget(soundLayout);
	boxLayout->addWidget(loopLayout);
	boxLayout->addWidget(playLayout);
	boxLayout->addWidget(physicLayout);

	addWidgetToEditor(boxLayout);
}

void SoundEditor::onNotify( Widget* sender, WidgetEvent action, void* data )
{
	Interface* parentInterface = (Interface*)(parent);
	SoundResources* soundResources = (SoundResources*)(parentInterface->getWidget((uint32_t)TR_COMMON_ID::SOUND_RESOURCES));

	if(soundResources->chosenSound != nullptr)
	{
		Resource<SoundEffect>* soundEffect = ResourceManager::getSingleton()->getSoundEffect(soundResources->chosenSound->resourceId);
		chosenSound = soundResources->chosenSound;
		pickSoundButton->setText(soundEffect->resourceName.c_str());
	}
	
	if (soundResources->chosenStream != nullptr)
	{
		Resource<SoundStream>* soundStream = ResourceManager::getSingleton()->getSoundStream(soundResources->chosenStream->resourceId);
		chosenStream = soundResources->chosenStream;
		pickSoundButton->setText(soundStream->resourceName.c_str());
	}
}

void SoundEditor::updateWidget(void* entityComponent )
{
	EntityComponent* component = (EntityComponent*)entityComponent;

	if(component->getComponentType() != (uint8_t)EntityGroupType::SOUND)
		return;

	Interface*      parentInterface = (Interface*)(parent);
	SoundResources* soundResources  = (SoundResources*)(parentInterface->getWidget((uint32_t)TR_COMMON_ID::SOUND_RESOURCES));
	SoundComponent* soundComponent  = (SoundComponent*)(entityComponent);

	chosenStream = soundComponent->getSoundStream();
	chosenSound = soundComponent->getSoundEffect();
	soundResources->chosenSound = chosenSound;
	soundResources->chosenStream = chosenStream;

	if(chosenSound != nullptr)
	{
		Resource<SoundEffect>* resourceProxy = ResourceManager::getSingleton()->getSoundEffect(chosenSound->resourceId);
		pickSoundButton->setText(resourceProxy->resourceName.c_str());
	}
	else
	{
		if (chosenStream != nullptr) {
			Resource<SoundStream>* resourceProxy = ResourceManager::getSingleton()->getSoundStream(chosenStream->resourceId);
		} else
		pickSoundButton->setText("Set");
	}

	switch(soundComponent->getState())
	{
	case AL_PAUSED:
		pauseRadioButton->setCheck(true);
		break;
	case AL_PLAYING:
		playRadioButton->setCheck(true);
		break;
	case AL_STOPPED:
		stopRadioButton->setCheck(true);
		break;
	}

	loopCheckbox->setCheck(soundComponent->isLoop());
}

void SoundEditor::onSoundClick( Widget* sender, WidgetEvent action, void* data )
{
	Interface* parentInterface = (Interface*)(parent);

	SoundResources* soundResources = (SoundResources*)(parentInterface->getWidget((uint32_t)TR_COMMON_ID::SOUND_RESOURCES));
	soundResources->reloadResources();
	soundResources->setNotifier(&this->notifierCallback);
	soundResources->setActive(true);
	soundResources->setRender(true);
}

void SoundEditor::onLoopChange( Widget* sender, WidgetEvent action, void* data )
{
	/*EntityEditor* entityEditor = EntityEditor::getSingleton();
	std::list<EntityComponent*>::iterator it = entityEditor->selectedComponents.begin();

	bool isLoop = loopCheckbox->isChecked();

	for(it; it != entityEditor->selectedComponents.end(); ++it)
	{
		if((*it)->getComponentType() == (uint8_t)EntityGroupType::SOUND)
		{
			SoundComponent* soundComponent = (SoundComponent*)((*it));
			soundComponent->setLoop(isLoop);
		}
	}*/
}

void SoundEditor::onPlayerStateChange( Widget* sender, WidgetEvent action, void* data )
{
	/*EntityEditor* entityEditor = EntityEditor::getSingleton();
	std::list<EntityComponent*>::iterator it = entityEditor->selectedComponents.begin();

	for(it; it != entityEditor->selectedComponents.end(); ++it)
	{
		if ((*it)->getComponentType() == (uint8_t)EntityGroupType::SOUND)
		{
			SoundComponent* soundComponent = (SoundComponent*)((*it));

			switch(sender->getId())
			{
			case 0:
				soundComponent->play();
				break;
			case 1:
				soundComponent->pause();
				break;
			case 2:
				soundComponent->stop();
				break;
			}
		}
	}*/
}

void SoundEditor::onSetPhysicComponentDrop( Widget* sender, WidgetEvent action, void* data )
{
	/*Widget* onDropWidget = (Widget*)(data);
	if(onDropWidget->getWidgetType() != JA_IMAGE_BUTTON)
		return;

	ImageButton* dropButton = (ImageButton*)(onDropWidget);
	if(dropButton->vUserData == nullptr)
		return;

	EntityComponent* entityComponent = (EntityComponent*)(dropButton->vUserData);
	if(entityComponent->getComponentType() != (uint8_t)EntityGroupType::PHYSIC_OBJECT)
		return;

	PhysicComponent* physicComponent = (PhysicComponent*)(entityComponent);
	SoundComponent*  soundComponent  = (SoundComponent*)(EntityEditor::getSingleton()->getSelectedComponent((uint8_t)EntityGroupType::SOUND));

	if(soundComponent->getEntity() != physicComponent->getEntity())
		return;

	soundComponent->setPhysicComponent(physicComponent);

	if(physicComponent->getHashTag() != 0)
	{
		const char* tagName = ScriptManager::getSingleton()->getHashTagName(physicComponent->getHashTag());
		dropPhysicButton->setText(tagName);
	}
	else
	{
		PhysicObject2d* physicObject = physicComponent->getPhysicObject();
		if(physicObject != nullptr)
			dropPhysicButton->setTextValue("Body %hu", physicObject->getId());
		else
			dropPhysicButton->setText("");
	}*/
}

void SoundEditor::soundEffectFilter(EntityComponent* entityComponent)
{
	if (entityComponent->getComponentType() != (uint8_t)EntityGroupType::SOUND)
		return;
	
	SoundComponent* soundComponent = (SoundComponent*)(entityComponent);
	soundComponent->setSound(singleton->chosenSound);
}

void SoundEditor::soundStreamFilter(EntityComponent* entityComponent)
{
	if (entityComponent->getComponentType() != (uint8_t)EntityGroupType::SOUND)
		return;

	SoundComponent* soundComponent = (SoundComponent*)(entityComponent);
	soundComponent->setSoundStream(singleton->chosenStream);
}

SensorEditor::SensorEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Sensor", parent)
{
	setRender(false);
	setActive(false);
}

void SensorEditor::updateWidget(void* entityComponent)
{

}

SensorEditor::~SensorEditor()
{

}


}