#pragma once

#include "../../Engine/jaSetup.hpp"
#include "../../Engine/Gui/DebugHud.hpp"
#include "../../Engine/Gui/Menu.hpp"
#include "../../Engine/Gui/Label.hpp"
#include "../../Engine/Gui/Editbox.hpp"
#include "../../Engine/Gui/ImageButton.hpp"
#include "../../Engine/Gui/ProgressButton.hpp"
#include "../../Engine/Gui/Button.hpp"
#include "../../Engine/Gui/GridLayout.hpp"
#include "../../Engine/Gui/Magnet.hpp"
#include "../../Engine/Gui/Hider.hpp"
#include "../../Engine/Entity/Entity.hpp"

#include "PhysicsEditor.hpp"
#include "ComponentEditor.hpp"
#include "CommonWidgets.hpp"
#include <vector>

namespace tr
{

	enum class EntityEditorMode
	{
		ENTITY_EDIT_MODE = 0
	};

	enum class EntityEditorHud
	{
		ENTITY_ID_HUD = 0,
		ENTITY_EDITOR_MODE_HUD = 1,
		ENTITY_EDITOR_HELPER_HUD = 2, // Editor helper for informtion show
		ENTITY_EDITOR_STATIC_HELPER_HUD = 3
	};

	typedef void(*ComponentFilterPtr)(ja::EntityComponent*);
	typedef void(*EntityFilterPtr)(ja::Entity*);

	class EntityEditor : public ja::Menu
	{
	private:
		ja::DebugHud hud;
		static EntityEditor* singleton;
		EntityEditorMode editMode;

	public:

		std::list<ja::Entity*>          selectedEntities;
	private:

		//Debug drawing
		ja::Font* fontDebugRender;

		//Gui
		EntityComponentMenu* componentMenu;

		//Script window
		ScriptFunctionViewer* scriptFunctionViewer;
		DataModelViewer*      dataModelViewer;
		SoundResources*       soundResources;

	public:

	private:
		//Gui

		EntityEditor();
		void createGUI();

	public:
		static void init();
		static EntityEditor* getSingleton();
		static void cleanUp();
		~EntityEditor();

		void addToSelection(ja::Entity* entity);
		bool isInSelection(ja::Entity* entity);

		void setEditorMode(EntityEditorMode mode);
		void setDefaultParameters();

		void update();
		void draw();

		void gatherResources();

		//gui
		void refresh();

		void applyFilterToSelectedEntities(EntityFilterPtr entityFilter);

		static void drawPhysicObject(ja::PhysicObject2d* physicObject);

		friend class ComponentPopup;
		friend class EntityPopup;
		friend class ScriptEditor;
		friend class TimerEditor;
		friend class PhysicObjectEditor;
		friend class SpriteComponentEditor;
		friend class SoundEditor;
	};

}