#include "PhysicsEditor.hpp"
#include "WidgetId.hpp"
#include "../../Engine/Manager/GameManager.hpp"
#include "../../Engine/Gilgamesh/DebugDraw.hpp"
#include "MainMenu.hpp"

using namespace tr;

PhysicsEditor::PhysicsEditor() : Menu((uint32_t)TR_MENU_ID::PHYSICSEDITOR, nullptr)
{
	this->drawBoxSelection = false;
	this->drawEntity       = true;
	this->drawHud          = true;
	shapeTypeId            = (int8_t)ShapeType::Box;

	hud.setFont("default.ttf",10,jaLeftLowerCorner,255,255,255,255);
	hud.setFont("default.ttf",10,jaRightLowerCorner,255,255,255,255);
	hud.setFont("default.ttf",10,jaLeftUpperCorner,255,255,255,255);
	hud.setFont("default.ttf",10,jaRightUpperCorner,255,255,255,255);

	hud.addField(jaLeftUpperCorner , (uint32_t)PhysicsEditorHud::BODY_ID_HUD, "Id=");
	hud.addField(jaLeftUpperCorner , (uint32_t)PhysicsEditorHud::BODY_TYPE_HUD, "Type=");
	hud.addField(jaRightUpperCorner, (uint32_t)PhysicsEditorHud::MODE_HUD, "Mode=");

	hud.addField(jaRightLowerCorner, (uint32_t)PhysicsEditorHud::HELPER_HUD, "");

	hud.addField(jaLeftLowerCorner, (uint32_t)PhysicsEditorHud::GRID_SIZE_HUD,"Grid = 32");
	hud.addField(jaLeftLowerCorner, (uint32_t)PhysicsEditorHud::WARM_START_HUD,"W - Warm start = On");
	hud.addField(jaLeftLowerCorner, (uint32_t)PhysicsEditorHud::FIX_CONSTRAINTS_HUD,"Tab - Fix constraints = Off");
	hud.addField(jaLeftLowerCorner, (uint32_t)PhysicsEditorHud::STATIC_HELPER_HUD,"L - Ruler\nX - X axis\nY - Y axis\nO - Object space\nA - Draw AABB\nB - Draw bound\nN - Draw id\nE - Draw entity\nH - Draw hashgrid\nI - Print hashes\nU - Print constraint islands\nN - Single step\nM - Simulate\nShift + R - Rotate 90\nShift + Ctrl + R - Rotate 45\nSPACE - Menu");

	selectedPhysicObjectsTemp = new StackAllocator<ObjectHandle2d*>(10,16);

	int32_t resolutionWidth;
	int32_t resolutionHeight;
	DisplayManager::getSingleton()->getResolution(&resolutionWidth, &resolutionHeight);
	setText("Physics Editor");
	setPosition(0,0);
	setSize(resolutionWidth, resolutionHeight);

	setDefaultParameters();
	createGUI();
	gatherResources();

	setEditorMode(PhysicsEditorMode::SELECT_MODE);
}

void PhysicsEditor::createGUI()
{
	physicsBar  = new SlideBox(0);
	physicsMenu = new PhysicsComponentMenu(1, this);
	sceneMenu   = new PhysicsSceneMenu(2, this);
	timerMenu   = new PhysicsTimerMenu(3, this);

	timerMenu->setActive(false);
	timerMenu->setRender(false);
	sceneMenu->setActive(false);
	sceneMenu->setRender(false);
	
	physicsBar->dockTo((uint32_t)Margin::TOP);

	FlowLayout* menuBar = new FlowLayout(0, nullptr, (uint32_t)Margin::LEFT, 5, 0);
	menuBar->setColor(128, 128, 128, 128);
	menuBar->setSize(width, 28);
	menuBar->setPosition(0, height - 28);

	Button* sceneButton = new Button(0, nullptr);
	sceneButton->setFont("default.ttf",10);
	sceneButton->setText("Scene");
	sceneButton->setColor(0, 0, 255, 255);
	sceneButton->setFontColor(255, 255, 255, 255);
	sceneButton->adjustToText(10, 28);
	sceneButton->vUserData = sceneMenu;
	sceneButton->setOnClickEvent(this, &PhysicsEditor::onClickMenuBar);
	menuBar->addWidget(sceneButton);
	
	Button* menuButton = new Button(1, nullptr);
	menuButton->setFont("default.ttf",10);
	menuButton->setText("Menu");
	menuButton->setColor(0, 0, 255, 255);
	menuButton->setFontColor(255, 255, 255, 255);
	menuButton->adjustToText(10, 28);
	menuButton->vUserData = physicsMenu;
	menuButton->setOnClickEvent(this, &PhysicsEditor::onClickMenuBar);
	menuBar->addWidget(menuButton);

	Button* timerButton = new Button(2, nullptr);
	timerButton->setFont("default.ttf",10);
	timerButton->setText("Timer");
	timerButton->setColor(0, 0, 255, 255);
	timerButton->setFontColor(255, 255, 255, 255);
	timerButton->adjustToText(10, 28);
	timerButton->vUserData = timerMenu;
	timerButton->setOnClickEvent(this, &PhysicsEditor::onClickMenuBar);
	menuBar->addWidget(timerButton);

	spaceBarMenu = new PhysicsSpaceBarMenu(3, nullptr);
	spaceBarMenu->hide();

	physicsBar->setSlideArea(10);
	physicsBar->setArea(28);
	physicsBar->setBox(menuBar);

	addWidget(physicsMenu);
	addWidget(sceneMenu);
	addWidget(timerMenu);
	addWidget(physicsBar);
	addWidget(spaceBarMenu);
}

void PhysicsEditor::gatherResources()
{
	sceneMenu->reloadResources();
	physicsMenu->reloadResources();
}

PhysicsEditor::~PhysicsEditor()
{
	delete selectedPhysicObjectsTemp;
}

void PhysicsEditor::init()
{
	if(singleton == nullptr)
	{
		singleton = new PhysicsEditor();
	}
}

PhysicsEditor* PhysicsEditor::getSingleton()
{
	return singleton;
}

void PhysicsEditor::cleanUp()
{
	if(singleton != nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void PhysicsEditor::setEditorMode(PhysicsEditorMode mode)
{
	if (editMode == PhysicsEditorMode::COMPONENT_EDITOR_MODE)
		currentComponentEditor->setActiveMode(false);

	editMode = mode;

	switch(editMode)
	{
	case PhysicsEditorMode::SELECT_MODE:
		hud.setFieldValue((uint32_t)PhysicsEditorHud::MODE_HUD, "Mode=Select Body");
		hud.setFieldValue((uint32_t)PhysicsEditorHud::HELPER_HUD, "(LShift) + (LCtrl) + LMB - Select\nAlt + LMB - Change gizmo position\nP - Properties\nPG_UP,PG_DOWN - Selected body\n[,] - Change shape\n,,. - Body type\nS - Scale\nR - Rotate\nD - Delete body\nCtrl + C - Copy\nCtrl + D - Duplicate\nV - LVelocity\nF - AVelocity\nESC - Cancel");
		break;
	case PhysicsEditorMode::TRANSLATE_MODE:
		hud.setFieldValue((uint32_t)PhysicsEditorHud::MODE_HUD, "Mode=Translate Body");
		hud.setFieldValue((uint32_t)PhysicsEditorHud::HELPER_HUD, "LMB - Accept\nESC - Cancel");
		break;
	case PhysicsEditorMode::ROTATE_MODE:
		hud.setFieldValue((uint32_t)PhysicsEditorHud::MODE_HUD, "Mode=Rotate Body");
		hud.setFieldValue((uint32_t)PhysicsEditorHud::HELPER_HUD, "LMB - Accept\n0 - Set 0 Angle\nESC - Cancel");
		break;
	case PhysicsEditorMode::SIZE_MODE:
		hud.setFieldValue((uint32_t)PhysicsEditorHud::MODE_HUD, "Mode=Size Body");
		hud.setFieldValue((uint32_t)PhysicsEditorHud::HELPER_HUD, "LMB - Accept\n[,] - Grid size\n+,- - Size\n1 - Size to grid\nESC - Cancel");
		break;
	}
}

void PhysicsEditor::setEditorMode( CommonWidget* commonWidget )
{
	if (editMode == PhysicsEditorMode::COMPONENT_EDITOR_MODE)
	{
		currentComponentEditor->setActiveMode(false);
		setEditorMode(PhysicsEditorMode::SELECT_MODE);
		return;
	}

	editMode = PhysicsEditorMode::COMPONENT_EDITOR_MODE;
	currentComponentEditor = commonWidget;
	currentComponentEditor->setActiveMode(true);
}

PhysicsEditorMode PhysicsEditor::getEditorMode()
{
	return this->editMode;
}

CommonWidget* PhysicsEditor::getCurrentEditor()
{
	if (editMode == PhysicsEditorMode::COMPONENT_EDITOR_MODE)
	{
		return this->currentComponentEditor;
	}

	return nullptr;
}

void PhysicsEditor::setShape(ShapeType shapeType)
{
	switch (shapeType)
	{
	case ShapeType::Box:
		physicObjectDef->copyShape(&physicObjectDef->box);
		break;
	case ShapeType::Circle:
		physicObjectDef->copyShape(&physicObjectDef->circle);
		break;
	case ShapeType::ConvexPolygon:
		physicObjectDef->copyShape(&physicObjectDef->polygon);
		break;
	case ShapeType::Point:
		physicObjectDef->copyShape(&physicObjectDef->point);
		break;
	}
}

void PhysicsEditor::nextShape()
{
	shapeTypeId = (shapeTypeId + 1) % 2;

	switch(shapeTypeId)
	{
	case (int8_t)ShapeType::Box:
		physicObjectDef->copyShape(&physicObjectDef->box);
		break;
	case(int8_t)ShapeType::Circle:
		physicObjectDef->copyShape(&physicObjectDef->circle);
		break;
	case (int8_t)ShapeType::ConvexPolygon:
		physicObjectDef->copyShape(&physicObjectDef->polygon);
		break;
	case (int8_t)ShapeType::Point:
		physicObjectDef->copyShape(&physicObjectDef->point);
		break;
	}

	applyFilterToSelection(changeShape);

	if (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 0)
	{
		PhysicObject2d* physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		ShapeEditor::getSingleton()->updateWidget(physicObject);
		MaterialEditor::getSingleton()->updateWidget(physicObject);
	}
}

void PhysicsEditor::previousShape()
{
	shapeTypeId--;
	if(shapeTypeId < 0)
		shapeTypeId = 1;

	switch(shapeTypeId)
	{
	case (int8_t)ShapeType::Box:
		physicObjectDef->copyShape(&physicObjectDef->box);
		break;
	case (int8_t)ShapeType::Circle:
		physicObjectDef->copyShape(&physicObjectDef->circle);
		break;
	case (int8_t)ShapeType::ConvexPolygon:
		physicObjectDef->copyShape(&physicObjectDef->polygon);
		break;
	case (int8_t)ShapeType::Point:
		physicObjectDef->copyShape(&physicObjectDef->point);
		break;
	}

	applyFilterToSelection(changeShape);

	if (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 0)
	{
		PhysicObject2d* physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		ShapeEditor::getSingleton()->updateWidget(physicObject);
		MaterialEditor::getSingleton()->updateWidget(physicObject);
	}
}

void PhysicsEditor::nextBodyType()
{
	int8_t bodyTypeId = (int8_t)physicObjectDef->bodyType;
	bodyTypeId = (bodyTypeId + 1) % 3;
	physicObjectDef->bodyType = (BodyType)bodyTypeId;
}

void PhysicsEditor::previousBodyType()
{
	int8_t bodyTypeId = (int8_t)physicObjectDef->bodyType;
	bodyTypeId--;
	if(bodyTypeId < 0)
		bodyTypeId = 2;
	physicObjectDef->bodyType = (BodyType)bodyTypeId;
}

jaVector2 PhysicsEditor::getGizmoPosition()
{
	return gizmo;
}

void PhysicsEditor::moveBody()
{
	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
	
	std::list<PhysicObject2d*>::iterator itPhysic = selectedPhysicObjects.begin();

	jaVector2 posDiff;
	jaVector2 newPos;

	for(itPhysic; itPhysic != selectedPhysicObjects.end(); ++itPhysic)
	{
		ObjectHandle2d* objectHandle = (*itPhysic)->getShapeHandle();
		posDiff = objectHandle->transform.position - gizmo;
		newPos = drawPoint + posDiff;
		objectHandle->transform.position = newPos;
		PhysicsManager::getSingleton()->updatePhysicObjectImmediate(*itPhysic);
	}

	gizmo = drawPoint;

	if (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 0)
	{
		PhysicObject2d* physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		ShapeEditor::getSingleton()->updateWidget(physicObject);
	}
	
	Toolset::getSingleton()->worldHud->addInfoC((uint16_t)PhysicsEditorInfo::TRANSLATE_INFO, gizmo.x, gizmo.y, 0, 255, 0, 5.0f, "(%.4f,%.4f)", gizmo.x, gizmo.y);
}

void PhysicsEditor::rotateBody()
{
	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

	jaVector2 diff1 = selectionStartPoint - gizmo;
	jaVector2 diff2 = drawPoint - gizmo;

	std::list<PhysicObject2d*>::iterator    itPhysic = selectedPhysicObjects.begin();
	std::vector<PhysicObjectState>::iterator itState = selectedPhysicObjectStates.begin();

	jaFloat   angle1 = jaVectormath2::getAngle(diff1);
	jaFloat   angle2 = jaVectormath2::getAngle(diff2);
	jaFloat	  angle = angle2 - angle1;
	jaMatrix2 rotDiff = jaMatrix2(angle2 - angle1);

	if (InputManager::isKeyModPress(JA_KMOD_LCTRL) || InputManager::isKeyModPress(JA_KMOD_LSHIFT))
	{
		if (InputManager::isKeyModPress(JA_KMOD_LCTRL)) {
			rotDiff = jaMatrix2((float)(math::PI) / 4.0f);
			angle = 45.0f;
		}
		else {
			rotDiff = jaMatrix2((float)(math::PI) / 2.0f);
			angle = 90.0f;
		}
		setEditorMode(PhysicsEditorMode::SELECT_MODE);
	}

	for(itPhysic; itPhysic != selectedPhysicObjects.end(); ++itPhysic)
	{
		ObjectHandle2d* objectHandle = (*itPhysic)->getShapeHandle();
		jaVector2 positionDiff = rotDiff * (itState->position - gizmo);
		objectHandle->transform.position = gizmo + positionDiff;
		(*itPhysic)->setOrientation(rotDiff * itState->rotMatrix);
		
		PhysicsManager::getSingleton()->updatePhysicObjectImmediate(*itPhysic);
		++itState;
	}

	if (InputManager::isKeyPressed(JA_0))
	{
		angle = 0.0f;
		std::list<PhysicObject2d*>::iterator itPhysic = selectedPhysicObjects.begin();
		for (itPhysic; itPhysic != selectedPhysicObjects.end(); ++itPhysic)
		{
			(*itPhysic)->setOrientation(jaMatrix2(0.0f));

			PhysicsManager::getSingleton()->updatePhysicObjectImmediate(*itPhysic);
		}
		setEditorMode(PhysicsEditorMode::SELECT_MODE);
	}

	if (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 0)
	{
		PhysicObject2d* physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		ShapeEditor::getSingleton()->updateWidget(physicObject);
	}

	DisplayManager* display = DisplayManager::getSingleton();
	float posX = (float)(display->getResolutionWidth(0.2f));
	float posY = (float)(display->getResolutionHeight(0.2f));
	Toolset::getSingleton()->windowHud->addInfoC((uint16_t)PhysicsEditorInfo::ROTATE_INFO, posX, posY, 224, 12, 17, 5.0f, "Angle %.4f", angle);
}

void PhysicsEditor::sizeBody()
{
	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
	ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();

	jaVector2 scale = drawPoint - gizmo;
	jaVector2 scaleStart = selectionStartPoint - gizmo;

	if((scaleStart.x == 0.0f) || (scaleStart.y == 0.0f))
		return;

	scale.x = scale.x / scaleStart.x;
	scale.y = scale.y / scaleStart.y;

	if (InputManager::isKeyPressed(JA_KP_PLUS)) {
		if (ruler->isActive()) {
			switch (ruler->alignTo)
			{
			case RulerAlign::ALIGN_X:
				scale.x *= 2.0f;
				break;
			case RulerAlign::ALIGN_Y:
				scale.y *= 2.0f;
				break;
			}
		}
		else
		{
			scale.x *= 2.0f;
			scale.y *= 2.0f;
		}
		setEditorMode(PhysicsEditorMode::SELECT_MODE);
	}

	if (InputManager::isKeyPressed(JA_KP_MINUS)) {
		if (ruler->isActive()) {
			switch (ruler->alignTo)
			{
			case RulerAlign::ALIGN_X:
				scale.x *= 0.5f;
				break;
			case RulerAlign::ALIGN_Y:
				scale.y *= 0.5f;
				break;
			}
		}
		else
		{
			scale.x *= 0.5f;
			scale.y *= 0.5f;
		}
		setEditorMode(PhysicsEditorMode::SELECT_MODE);
	}

	std::list<PhysicObject2d*>::iterator itSelection = selectedPhysicObjects.begin();
	std::vector<PhysicObjectState>::iterator itSelectionState = selectedPhysicObjectStates.begin();
	for(itSelection; itSelection != selectedPhysicObjects.end(); ++itSelection)
	{
		PhysicObject2d* physicObject = *itSelection;
		ObjectHandle2d* objectHandle = physicObject->getShapeHandle();
		Shape2d* shapeHandle = objectHandle->getShape();
		
		jaVector2 diff = itSelectionState->position - gizmo;
		diff.x = scale.x * diff.x;
		diff.y = scale.y * diff.y;

		objectHandle->transform.position = gizmo + diff;

		shapeHandle->setSizeX(itSelectionState->size.x * scale.x);
		shapeHandle->setSizeY(itSelectionState->size.y * scale.y);
		
		PhysicsManager::getSingleton()->updatePhysicObjectImmediate(*itSelection);
		
		if (physicObject->getObjectType() == (uint8_t)PhysicObjectType::RIGID_BODY)
		{
			RigidBody2d* rigidBody = (RigidBody2d*)physicObject;
			rigidBody->resetMassData();
		}

		++itSelectionState;
	}

	if (InputManager::isKeyPressed(JA_1)) {

		scale.x = 1.0f;
		scale.y = 1.0f;

		std::list<PhysicObject2d*>::iterator itSelection = selectedPhysicObjects.begin();
		for (itSelection; itSelection != selectedPhysicObjects.end(); ++itSelection)
		{
			PhysicObject2d* physicObject = *itSelection;
			physicObject->getShape()->setSizeX(1.0f);
			physicObject->getShape()->setSizeY(1.0f);

			PhysicsManager::getSingleton()->updatePhysicObjectImmediate(*itSelection);

			if (physicObject->getObjectType() == (uint8_t)PhysicObjectType::RIGID_BODY)
			{
				RigidBody2d* rigidBody = (RigidBody2d*)physicObject;
				rigidBody->resetMassData();
			}
		}
		setEditorMode(PhysicsEditorMode::SELECT_MODE);
	}

	if (PhysicsEditor::getSingleton()->selectedPhysicObjects.size() > 0)
	{
		PhysicObject2d* physicObject = PhysicsEditor::getSingleton()->selectedPhysicObjects.back();
		ShapeEditor::getSingleton()->updateWidget(physicObject);
		MaterialEditor::getSingleton()->updateWidget(physicObject);
	}

	DisplayManager* display = DisplayManager::getSingleton();
	float posX = (float)(display->getResolutionWidth(0.3f));
	float posY = (float)(display->getResolutionHeight(0.3f));
	Toolset::getSingleton()->windowHud->addInfoC((uint16_t)PhysicsEditorInfo::SCALE_INFO, posX, posY, 255, 255, 0, 5.0f, "Scale (%.4f,%.4f)", scale.x, scale.y);
}

void PhysicsEditor::setDefaultParameters()
{
	this->physicObjectDef = &PhysicsManager::getSingleton()->physicObjectDef;
	this->constraintDef   = &PhysicsManager::getSingleton()->constraintDef;

	physicObjectDef->box.setWidth(  GIL_REAL(1.0) );
	physicObjectDef->box.setHeight( GIL_REAL(1.0) );
	physicObjectDef->circle.radius  = GIL_REAL(0.5);
	
	jaVector2 verticesPolygon[3] = { jaVector2(0,0), jaVector2(2,0), jaVector2(2,2) };
	jaVector2 translateOffset;
	physicObjectDef->polygon.setVertices(verticesPolygon, 3, translateOffset);

	physicObjectDef->bodyType   = BodyType::STATIC_BODY;
	physicObjectDef->isSleeping = false;

	physicObjectDef->transform.setPosition(0.0f, 0.0f);
	physicObjectDef->transform.rotationMatrix.setDegrees(0.0f);


	physicObjectDef->linearDamping = GIL_ZERO;
	physicObjectDef->angluarDamping = GIL_ZERO;
	physicObjectDef->timeFactor = GIL_ONE;
	physicObjectDef->gravity = jaVector2(0,setup::physics::DEFAULT_GRAVITY);
	physicObjectDef->linearVelocity.setZero();
	physicObjectDef->angluarVelocity = GIL_ZERO; 
	physicObjectDef->fixedRotation = false;
	physicObjectDef->allowSleep = true;
	physicObjectDef->groupIndex = 1;
	physicObjectDef->categoryMask = 0x0001;
	physicObjectDef->maskBits = 0xFFFF;

	//Constraints
	constraintDef->softness = GIL_ZERO;
	constraintDef->relaxationBias = 0.2f;
	constraintDef->anchorPointA.setZero();
	constraintDef->anchorPointB.setZero();
}

void PhysicsEditor::createBody()
{
	selectedPhysicObjectsTemp->clear();
	selectedPhysicObjects.clear();

	RigidBody2d* body = PhysicsManager::getSingleton()->createBodyDef();

	selectedPhysicObjects.push_back(body);
	calculateSelectionMidpoint();
	updateComponentEditors(body);
}

void PhysicsEditor::calculateSelectionMidpoint()
{
	std::list<PhysicObject2d*>::iterator itPhysicObjects = selectedPhysicObjects.begin();

	int32_t selectedNum = selectedPhysicObjects.size();

	if (selectedNum > 0)
	{
		gizmo.setZero();

		for (itPhysicObjects; itPhysicObjects != selectedPhysicObjects.end(); ++itPhysicObjects)
		{
			ObjectHandle2d* objectHandle = (*itPhysicObjects)->getShapeHandle();
			gizmo += objectHandle->transform.position;
		}

		gizmo = gizmo / (jaFloat)(selectedNum);
	}
}

void PhysicsEditor::update()
{
	Toolset::getSingleton()->update();
	Interface::update();

	if (physicsBar->isSlided())
	{
		hud.setRender(false);
	}
	else
	{
		hud.setRender(this->drawHud);
	}

	bool isGuiActive = (UIManager::isMouseOverGui() || UIManager::isTextInputSelected());
		
	if(!isGuiActive)
	{
		if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
		{
			if(editMode == PhysicsEditorMode::SELECT_MODE)
			{
				if(drawBoxSelection && !InputManager::isKeyModPress(JA_KMOD_LALT))
				{
					drawBoxSelection = false;
					AABB2d selectionAABB = AABB2d::make(selectionStartPoint, Toolset::getSingleton()->getDrawPoint());
					selectedPhysicObjectsTemp->clear();
	
					jaVector2 size = selectionAABB.max - selectionAABB.min;
					jaTransform2 transform;
					transform.position = selectionAABB.min + (size / 2.0f);
					transform.rotationMatrix.setRadians(0.0f);
					Box2d boxSelection(size.x, size.y);
					PhysicsManager::getSingleton()->getPhysicObjectsAtShape(transform, &boxSelection, selectedPhysicObjectsTemp);

					if(InputManager::isKeyModPress(JA_KMOD_LSHIFT) || InputManager::isKeyModPress(JA_KMOD_LCTRL))
					{
						if(InputManager::isKeyModPress(JA_KMOD_LSHIFT))
							addToSelection(selectedPhysicObjectsTemp);
						else
							removeFromSelection(selectedPhysicObjectsTemp);
					}
					else
					{	
						selectedPhysicObjects.clear();
						addToSelection(selectedPhysicObjectsTemp);
					}

					if(selectedPhysicObjects.size() != 0)
					{
						PhysicObject2d* physicObject = selectedPhysicObjects.back();
						updateComponentEditors(physicObject);
					}
					else
					{
						updateComponentEditors(nullptr);
					}

					calculateSelectionMidpoint();
				}
			}
		}

		if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
		{
			switch(editMode)
			{
			case PhysicsEditorMode::SELECT_MODE:
				{		
					selectionStartPoint = Toolset::getSingleton()->getDrawPoint();

					if(InputManager::isKeyModPress(JA_KMOD_LALT))
					{
						gizmo = Toolset::getSingleton()->getDrawPoint();
					}
					else
					{
						drawBoxSelection = true;
					}
				}
				break;
			case PhysicsEditorMode::TRANSLATE_MODE:
					setEditorMode(PhysicsEditorMode::SELECT_MODE);
				break;
			case PhysicsEditorMode::ROTATE_MODE:
					setEditorMode(PhysicsEditorMode::SELECT_MODE);
				break;
			case PhysicsEditorMode::SIZE_MODE:
					setEditorMode(PhysicsEditorMode::SELECT_MODE);
				break;
			case PhysicsEditorMode::COMPONENT_EDITOR_MODE:
					//setEditorMode(TR_PHYSIC_SELECT_MODE);
				break;
			}	
		}

		if (InputManager::isKeyPressed(JA_o)) {
			ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
			objectSpace = !objectSpace;
			if (objectSpace) {
				if (selectedPhysicObjects.size() != 0) {
					PhysicObject2d* physicObject = selectedPhysicObjects.back();
					ruler->setAngle(physicObject->getShapeHandle()->transform.rotationMatrix.getDegrees());
				}
			}
			else
				ruler->setAngle(0);
		}

		if(InputManager::isKeyPressed(JA_l))
		{
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
			ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
			Toolset::getSingleton()->setRulerActive(!ruler->isActive());
			ruler->setPosition(drawPoint.x,drawPoint.y);

		}

		if(InputManager::isKeyPressed(JA_x))
		{
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
			ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
			ruler->setAlign(RulerAlign::ALIGN_X);
			ruler->setPosition(drawPoint.x,drawPoint.y);
		}

		if(InputManager::isKeyPressed(JA_y))
		{
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
			ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
			ruler->setAlign(RulerAlign::ALIGN_Y);
			ruler->setPosition(drawPoint.x,drawPoint.y);
		}

		if(InputManager::isKeyPressed(JA_a))
		{
			PhysicsManager* physicsManager = PhysicsManager::getSingleton();
			physicsManager->drawAABB = !physicsManager->drawAABB;
		}

		if(InputManager::isKeyPressed(JA_b))
		{
			PhysicsManager* physicsManager = PhysicsManager::getSingleton();
			physicsManager->drawShapes = !physicsManager->drawShapes;
		}

		if(InputManager::isKeyPressed(JA_e))
		{
			drawEntity = !drawEntity;
		}

		if(InputManager::isKeyPressed(JA_h))
		{
			PhysicsManager* physicsManager = PhysicsManager::getSingleton();
			physicsManager->drawHashCells = !physicsManager->drawHashCells;
		}

		if(InputManager::isKeyPressed(JA_n))
		{
			PhysicsManager* physicsManager = PhysicsManager::getSingleton();
			physicsManager->drawId = !physicsManager->drawId;
		}

		if(InputManager::isKeyPressed(JA_i))
		{
			PhysicsManager* physicsManager = PhysicsManager::getSingleton();
			physicsManager->printHashes();
		}

		if(InputManager::isKeyPressed(JA_u))
		{
			PhysicsManager* physicsManager = PhysicsManager::getSingleton();
			if (InputManager::isKeyModPress(JA_KMOD_SHIFT))
				physicsManager->printConstraints();
			else
				physicsManager->printConstraintIslands();
		}

		if(InputManager::isKeyPressed(JA_TAB))
		{
			PhysicsManager* physicsManager = PhysicsManager::getSingleton();
			physicsManager->fixConstraintPosition = !physicsManager->fixConstraintPosition;
			if(physicsManager->fixConstraintPosition)
			{
				hud.setFieldValue((uint32_t)PhysicsEditorHud::FIX_CONSTRAINTS_HUD, "Tab - Fix constraints = On");
			}
			else
			{
				hud.setFieldValue((uint32_t)PhysicsEditorHud::FIX_CONSTRAINTS_HUD, "Tab - Fix constraints = Off");
			}		
		}

		if(InputManager::isKeyPressed(JA_w))
		{
			PhysicsManager* physicsManager = PhysicsManager::getSingleton();
			physicsManager->isWarmStart = !physicsManager->isWarmStart;
			if(physicsManager->isWarmStart)
			{
				hud.setFieldValue((uint32_t)PhysicsEditorHud::WARM_START_HUD, "W - Warm start = On");
			}
			else
			{
				hud.setFieldValue((uint32_t)PhysicsEditorHud::WARM_START_HUD, "W - Warm start = Off");
			}	
		}

		if (InputManager::isKeyPressed(JA_SPACE))
		{
			UIManager* pUIManager = UIManager::getSingleton();
			spaceBarMenu->setPosition(pUIManager->mouseX, pUIManager->mouseY);
			spaceBarMenu->show();
		}

		if (editMode == PhysicsEditorMode::SELECT_MODE)
		{
			if (InputManager::isKeyPressed(JA_c)) {
				if (InputManager::isKeyModPress(JA_KMOD_CTRL))
				{
					if (selectedPhysicObjects.size() != 0)
					{
						PhysicObject2d* physicObject = selectedPhysicObjects.back();
						RigidBody2d*    rigidBody = (RigidBody2d*)(physicObject);
						rigidBody->fillRigidBodyDef(physicObjectDef);
					}
				}
				else
				{
					createBody();
					setEditorMode(PhysicsEditorMode::TRANSLATE_MODE);
				}
			}

			if (InputManager::isKeyPressed(JA_g)) {
				if (selectedPhysicObjects.size() != 0) {
					saveSelectedPhysicsObjectStates();
					setEditorMode(PhysicsEditorMode::TRANSLATE_MODE);
				}
			}

			if (InputManager::isKeyPressed(JA_r)) {
				if (selectedPhysicObjects.size() != 0)
				{
					selectionStartPoint = Toolset::getSingleton()->getDrawPoint();
					saveSelectedPhysicsObjectStates();

					setEditorMode(PhysicsEditorMode::ROTATE_MODE);
				}
			}

			if (InputManager::isKeyPressed(JA_s)) {
				if (selectedPhysicObjects.size() != 0)
				{
					selectionStartPoint = Toolset::getSingleton()->getDrawPoint();
					saveSelectedPhysicsObjectStates();

					setEditorMode(PhysicsEditorMode::SIZE_MODE);
				}
			}

			if (InputManager::isKeyPressed(JA_LEFTBRACKET)) {
				previousShape();
			}

			if (InputManager::isKeyPressed(JA_RIGHTBRACKET)) {
				nextShape();
			}

			if (InputManager::isKeyPressed(JA_d)) {
				if (InputManager::isKeyModPress(JA_KMOD_CTRL))
				{
					if (selectedPhysicObjects.size() != 0)
					{
						duplicateSelectedAtPoint(Toolset::getSingleton()->getDrawPoint());
					}
				}
				else
				{
					applyFilterToSelection(deletePhysicObject);
					selectedPhysicObjects.clear();;
				}
			}

			if (InputManager::isKeyPressed(JA_ESCAPE)) {
				MainMenu::getSingleton()->setGameState(GameState::MAINMENU_MODE);
			}

			if (InputManager::isKeyPressed(JA_n)) {
				PhysicsManager::getSingleton()->step(1.0f / 60.0f);
				timerMenu->increaseFramePassed();
			}

			if (InputManager::isKeyPress(JA_m)) {
				PhysicsManager::getSingleton()->step(1.0f / 60.0f);
				timerMenu->increaseFramePassed();
			}
		}

		if (editMode == PhysicsEditorMode::TRANSLATE_MODE) {

			if (InputManager::isKeyPressed(JA_PERIOD)) {
				nextBodyType();
			}

			if (InputManager::isKeyPressed(JA_COMMA)) {
				previousBodyType();
			}

			if (InputManager::isKeyPressed(JA_ESCAPE)) {
				setEditorMode(PhysicsEditorMode::SELECT_MODE);
			}
		}

		if (InputManager::isKeyModPress(JA_KMOD_SHIFT) && InputManager::isKeyPressed(JA_8)) {
			drawHud = !drawHud;
		}
	}
   
	if (editMode == PhysicsEditorMode::ROTATE_MODE) {
		if (InputManager::isKeyPressed(JA_ESCAPE)) {
			setEditorMode(PhysicsEditorMode::SELECT_MODE);
		}
	}

	if (editMode == PhysicsEditorMode::SIZE_MODE) {
		if (InputManager::isKeyPressed(JA_ESCAPE)) {
			setEditorMode(PhysicsEditorMode::SELECT_MODE);
		}
	}

	switch(editMode) {
	case PhysicsEditorMode::SELECT_MODE:
		{
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
			physicObjectDef->transform.position = drawPoint;
		}
		break;
	case PhysicsEditorMode::TRANSLATE_MODE:
		moveBody(); 
		break;
	case PhysicsEditorMode::ROTATE_MODE:
		rotateBody();
		break;
	case PhysicsEditorMode::SIZE_MODE:
		sizeBody();
		break;
	case PhysicsEditorMode::COMPONENT_EDITOR_MODE:
		currentComponentEditor->handleInput();
		break;
	}

	hud.update();
}

void PhysicsEditor::drawSelectedPhysicObjects()
{
	CameraInterface* activeCamera = GameManager::getSingleton()->getActiveCamera();
	jaVector2 originX(1.0f / activeCamera->scale.x, 0.0f);
	jaVector2 originY(0.0f, 1.0f / activeCamera->scale.y);

	if(selectedPhysicObjects.size() != 0) {
		std::list<PhysicObject2d*>::iterator itPhysic = selectedPhysicObjects.begin();
		DisplayManager::setColorUb(0, 255, 0, 128);
		DisplayManager::setLineWidth(3.0f);

		for(itPhysic; itPhysic != selectedPhysicObjects.end(); ++itPhysic) {
			ObjectHandle2d* objectHandle = ( *itPhysic )->getShapeHandle();
			Debug::drawObjectHandle2d( objectHandle,objectHandle->transform );
		}		

		itPhysic = selectedPhysicObjects.begin();
		DisplayManager::setColorUb(255, 255, 255);
		DisplayManager::setLineWidth(0.1f);
		for (itPhysic; itPhysic != selectedPhysicObjects.end(); ++itPhysic) {
			ObjectHandle2d* objectHandle = (*itPhysic)->getShapeHandle();

			jaVector2 transformedOriginX = objectHandle->transform.rotationMatrix * originX;
			jaVector2 transformedOriginY = objectHandle->transform.rotationMatrix * originY;

			DisplayManager::drawLine(objectHandle->transform.position.x, objectHandle->transform.position.y, objectHandle->transform.position.x + transformedOriginX.x, objectHandle->transform.position.y + transformedOriginX.y);
			DisplayManager::drawLine(objectHandle->transform.position.x, objectHandle->transform.position.y, objectHandle->transform.position.x + transformedOriginY.x, objectHandle->transform.position.y + transformedOriginY.y);

		}
	}
}

void PhysicsEditor::drawGizmo()
{
	DisplayManager::setColorUb(255, 255, 255);
	DisplayManager::setPointSize(7);
	DisplayManager::drawPoint(gizmo.x, gizmo.y);

	DisplayManager::setColorUb(255, 0, 0);
	DisplayManager::setPointSize(5);
	DisplayManager::drawPoint(gizmo.x, gizmo.y);
}

void PhysicsEditor::draw() {

	CameraInterface* camera = GameManager::getSingleton()->getActiveCamera();
	DisplayManager::pushWorldMatrix();
	DisplayManager::clearWorldMatrix();
	DisplayManager::setCameraMatrix(camera);

	DisplayManager::setColorUb( 0,255,0,128 );
	DisplayManager::setLineWidth( 3.0f );

	drawSelectedPhysicObjects();
	
	DisplayManager::setLineWidth( 1.0f );
	if(drawEntity)
		PhysicsManager::getSingleton()->drawUser = EntityEditor::drawPhysicObject;
	else
		PhysicsManager::getSingleton()->drawUser = nullptr;

	PhysicsManager::getSingleton()->drawDebug(camera);

	if(drawBoxSelection) {
		DisplayManager::setColorUb(255,0,128,255);
		DisplayManager::setLineWidth(3.0f);
		AABB2d boxSelection = AABB2d::make(selectionStartPoint,Toolset::getSingleton()->getDrawPoint());	
		Debug::drawAABB2d(boxSelection);

		DisplayManager::setColorUb(255,255,255);
		DisplayManager::setPointSize(7);
		DisplayManager::drawPoint(selectionStartPoint.x,selectionStartPoint.y);

		DisplayManager::setColorUb(255,0,0);
		DisplayManager::setPointSize(5);
		DisplayManager::drawPoint(selectionStartPoint.x,selectionStartPoint.y);
	}

	if (editMode == PhysicsEditorMode::ROTATE_MODE) {
		DisplayManager::setColorUb( 255,255,255 );
		DisplayManager::setLineWidth( 2.0f );
		jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
		DisplayManager::drawLine( gizmo.x, gizmo.y, drawPoint.x, drawPoint.y );
	}

	if (editMode == PhysicsEditorMode::COMPONENT_EDITOR_MODE) {
		currentComponentEditor->render();
	}

	drawGizmo();

	DisplayManager::popWorldMatrix();
	Toolset::getSingleton()->draw();
	Interface::draw();

	hud.draw();
}

void PhysicsEditor::addToSelection( StackAllocator<ObjectHandle2d*>* selection )
{
	ObjectHandle2d** objectHandles = selection->getFirst();

	for(uint32_t i=0; i<selection->getSize(); ++i)
	{
		PhysicObject2d* physicObject = static_cast<PhysicObject2d*>(objectHandles[i]->getUserData());
		selectedPhysicObjects.remove(physicObject);
		selectedPhysicObjects.push_back(physicObject);
	}
}

void PhysicsEditor::addToSelection( PhysicObject2d* physicObject )
{
	selectedPhysicObjects.remove(physicObject);
	selectedPhysicObjects.push_back(physicObject);
}

void PhysicsEditor::removeFromSelection( StackAllocator<ObjectHandle2d*>* selection )
{
	ObjectHandle2d** objectHandles = selection->getFirst();

	for(uint32_t i=0; i<selection->getSize(); ++i)
	{
		PhysicObject2d* physicObject = static_cast<PhysicObject2d*>(objectHandles[i]->getUserData());
		selectedPhysicObjects.remove(physicObject);
	}
}

void PhysicsEditor::removeFromSelection( PhysicObject2d* physicObject )
{
	selectedPhysicObjects.remove(physicObject);
}

void PhysicsEditor::saveSelectedPhysicsObjectStates()
{
	std::list<PhysicObject2d*>::iterator itSelection = selectedPhysicObjects.begin();
	selectedPhysicObjectStates.clear();
	for(itSelection; itSelection != selectedPhysicObjects.end(); ++itSelection)
	{
		PhysicObjectState physicObjectState;
		ObjectHandle2d* objectHandle = (*itSelection)->getShapeHandle();
		physicObjectState.position = objectHandle->transform.position;
		physicObjectState.rotMatrix = objectHandle->transform.rotationMatrix;
		physicObjectState.size.x = objectHandle->getShape()->getSizeX();
		physicObjectState.size.y = objectHandle->getShape()->getSizeY();
		selectedPhysicObjectStates.push_back(physicObjectState);
	}
}

void PhysicsEditor::applyFilterToSelection( PhysicObjectFilterPtr physicObjectFilter )
{
	std::list<PhysicObject2d*>::iterator itPhysicObject = selectedPhysicObjects.begin();

	for(itPhysicObject; itPhysicObject != selectedPhysicObjects.end(); ++itPhysicObject)
	{
		physicObjectFilter(*itPhysicObject);
	}
}

void PhysicsEditor::deletePhysicObject( PhysicObject2d* physicObject )
{
	if(physicObject->getComponentData() == nullptr)
	{
		PhysicsManager::getSingleton()->deletePhysicObjectImmediate(physicObject);
	}
}

void PhysicsEditor::changeShape( PhysicObject2d* physicObject )
{
	if ((uint8_t)PhysicObjectType::RIGID_BODY == physicObject->getObjectType())
	{
		RigidBody2d* rigidBody = (RigidBody2d*)(physicObject);
		Shape2d* shape = PhysicsEditor::getSingleton()->physicObjectDef->getShape();
		PhysicsManager::getSingleton()->changeBodyShape(rigidBody,shape);
	}
}

void PhysicsEditor::updateComponentEditors( PhysicObject2d* physicObject )
{
	if (nullptr != physicObject)
	{
		hud.setFieldValuePrintC((uint32_t)PhysicsEditorHud::BODY_ID_HUD, "Id=%hu", physicObject->getId());

		switch (physicObject->getObjectType())
		{
		case (uint8_t)PhysicObjectType::RIGID_BODY:
			hud.setFieldValue((uint32_t)PhysicsEditorHud::BODY_TYPE_HUD, "Type=Rigid body");
			break;
		case (uint8_t)PhysicObjectType::PARTICLE_GENERATOR:
			hud.setFieldValue((uint32_t)PhysicsEditorHud::BODY_TYPE_HUD, "Type=Particles");
			break;
		case (uint8_t)PhysicObjectType::DEFORMABLE_BODY:
			hud.setFieldValue((uint32_t)PhysicsEditorHud::BODY_TYPE_HUD, "Type=Deformable body");
			break;
		}
	}
	else
	{
		hud.setFieldValue((uint32_t)PhysicsEditorHud::BODY_ID_HUD, "Id=");
		hud.setFieldValue((uint32_t)PhysicsEditorHud::BODY_TYPE_HUD, "Type=");
	}

	physicsMenu->updateComponentEditors(physicObject);
}

void PhysicsEditor::duplicateSelectedAtPoint( jaVector2& point )
{
	std::list<PhysicObject2d*>::iterator itSelected = selectedPhysicObjects.begin();
	std::list<PhysicObject2d*> physicsObjectTemp;
	std::list<PhysicObject2d*>::iterator itPhysic;

	for(itSelected; itSelected != selectedPhysicObjects.end(); ++itSelected)
	{
		if ((*itSelected)->getObjectType() == (uint8_t)PhysicObjectType::RIGID_BODY)
		{
			RigidBody2d* rigidBody = (RigidBody2d*)(*itSelected);
			rigidBody->fillRigidBodyDef(physicObjectDef);
			physicObjectDef->transform.position -= gizmo - point;
			PhysicObject2d* physicObject = PhysicsManager::getSingleton()->createBodyDef();
			physicsObjectTemp.push_back(physicObject);
		}	 
	}

	selectedPhysicObjects.clear();
	itPhysic = physicsObjectTemp.begin();
	for(itPhysic; itPhysic != physicsObjectTemp.end(); ++itPhysic)
	{
		selectedPhysicObjects.push_back(*itPhysic);
	}
	calculateSelectionMidpoint();
}

void PhysicsEditor::onClickMenuBar(Widget* sender, WidgetEvent action, void* data)
{
	Button* menuButton = static_cast<Button*>(sender);

	Widget* linkedWidget = static_cast<Widget*>(menuButton->vUserData);
	linkedWidget->setRender(true);
	linkedWidget->setActive(true);
}

PhysicsEditor* PhysicsEditor::singleton = nullptr;
