#include "../../Engine/jaSetup.hpp"
#include "Toolset.hpp"
#include "WidgetId.hpp"
#include "../../Engine/Manager/GameManager.hpp"
#include "../../Engine/Manager/ResourceManager.hpp"
#include "../../Engine/Manager/UIManager.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"
#include "../../Engine/Manager/InputManager.hpp"
#include "../../Engine/Manager/SoundManager.hpp"
#include "../../Engine/Gui/TestHud.hpp"

using namespace ja;

namespace tr
{
	

Toolset* Toolset::singleton = nullptr;

ToolsetRuler::ToolsetRuler()
{
	alignTo = RulerAlign::ALIGN_X;
	angleDeg = 0;
	transform.rotationMatrix.setDegrees(0);
	activeRuler = false;
}

void ToolsetRuler::setActive(bool active)
{
	activeRuler = active;
}

bool ToolsetRuler::isActive()
{
	return activeRuler;
}

void ToolsetRuler::setAlign(RulerAlign align)
{
	this->alignTo = align;
}

void ToolsetRuler::setAngle(jaFloat angleDeg)
{
	this->angleDeg = angleDeg;
	transform.rotationMatrix.setDegrees(angleDeg);
}

void ToolsetRuler::setPosition(jaFloat x, jaFloat y)
{
	transform.setPosition(x,y);
}

void ToolsetRuler::draw(gil::AABB2d& cameraView)
{
	DisplayManager::setColorUb(255,255,0);
	DisplayManager::setLineWidth(0.1f);

	
	switch(alignTo)
	{
	case RulerAlign::ALIGN_X:
		{
			jaVector2 halfHorizontalLine(0.5f * (cameraView.max.x - cameraView.min.x), 0.0f);
			jaVector2 point0 = transform * halfHorizontalLine;
			jaVector2 point1 = transform * -halfHorizontalLine;
			DisplayManager::drawLine(point0.x,point0.y,point1.x,point1.y);
		}
		break;
	case RulerAlign::ALIGN_Y:
		{
			jaVector2 halfVerticalLine(0.0f, 0.5f * (cameraView.max.y - cameraView.min.y));
			jaVector2 point0 = transform * halfVerticalLine;
			jaVector2 point1 = transform * -halfVerticalLine;
			DisplayManager::drawLine(point0.x, point0.y, point1.x, point1.y);
		}
		break;
	}
	
}

jaVector2 ToolsetRuler::transformPoint(jaVector2& point)
{
	if(activeRuler)
	{
		jaVector2 normal;
		switch(alignTo)
		{
		case RulerAlign::ALIGN_X:
			normal.x = GIL_REAL(1.0);
			normal.y = GIL_REAL(0.0);
			break;
		case RulerAlign::ALIGN_Y:
			normal.x = GIL_REAL(0.0);
			normal.y = GIL_REAL(1.0);
			break;
		case RulerAlign::NO_ALIGN:
			return point;
			break;
		}

		normal = transform.rotationMatrix * normal;
		jaVector2 projected = point - transform.position;
		jaFloat length = jaVectormath2::projection(normal,projected);
		normal *= length;
		return (transform.position + normal);
	}
	
	return point;
}

Toolset::Toolset() : Menu((uint32_t)TR_MENU_ID::TOOLSET, nullptr)
{
	IsGridDrawed    = false;
	IsTestHudDrawed = false;
	IsGridAnchor    = false;
	gridDetail = TR_GRID_64;

	int32_t widthR, heightR;
	DisplayManager::getResolution(&widthR,&heightR);
	setText("Toolset");
	setPosition(0,0);

	SoundManager*    soundMangager   = ja::SoundManager::getSingleton();
	ResourceManager* resourceManager = ja::ResourceManager::getSingleton();

	TestHud::initTestHud("default.ttf",10);
	testHud = TestHud::getSingleton();
	testHud->setFont("default.ttf",10,jaLeftLowerCorner,255,0,0,255);
	testHud->setFont("default.ttf",10,jaRightLowerCorner,0,255,0,255);
	testHud->setFont("default.ttf",10,jaLeftUpperCorner,255,0,0,255);
	testHud->setFont("default.ttf",10,jaRightUpperCorner,0,0,255,255);
	testHud->addField(jaLeftUpperCorner,TR_HUD_CAMERANAME,"Camera name=");
	testHud->addField(jaLeftUpperCorner,TR_HUD_CAMERAPOSITION,"Camera pos:");
	testHud->addField(jaLeftUpperCorner,TR_HUD_CAMERASCALE,"Camera scale:");
	testHud->addField(jaRightUpperCorner,TR_HUD_GAMESTATE,"Game state=");
	testHud->addField(jaRightLowerCorner,TR_HUD_MOUSEWINDOW,"Mouse Window pos:");
	testHud->addField(jaRightLowerCorner, TR_HUD_MOUSEGUI, "Mouse Gui pos:");
	testHud->addField(jaRightLowerCorner,TR_HUD_MOUSEWORLD,"Mouse World pos:");

	spectrumChart = new Chart((uint32_t)TR_MENU_ID::TOOLSET, this);
	spectrumChart->setColor(255,255,0,255);
	spectrumChart->setPosition(0,0);
	spectrumChart->setSize(300,100);
	spectrumChart->setChartValues(soundMangager->fftData.normalized_spectrum, setup::sound::CAPTURE_BUFFER_SIZE / 2);
	addWidget(spectrumChart);
	spectrumChart->setRender(true);

	activeDeviceLabel = new Label((uint32_t)TR_TOOLSET_ID::ACTIVE_DEVICE_NAME_LABEL, this);
	activeDeviceLabel->setPosition(0,105);
	activeDeviceLabel->setFont("default.ttf",10);
	activeDeviceLabel->setColor(255,255,255,255);
	activeDeviceLabel->setShadowColor(255,0,0,128);
	activeDeviceLabel->setShadowOffset(-2,-2);
	activeDeviceLabel->setAlign((uint32_t)Margin::LEFT | (uint32_t)Margin::DOWN); 
	activeDeviceLabel->setText( soundMangager->deviceNames[soundMangager->activeRecordingDevice].c_str() );
	addWidget(activeDeviceLabel);

	ja::string deviceNames= "";

	for(uint32_t i=0; i< soundMangager->deviceNames.size(); ++i)
	{
		deviceNames.append(soundMangager->deviceNames[i]);
		deviceNames.append("\n");
	}

	deviceNamesLabel = new Label((uint32_t)TR_TOOLSET_ID::DEVICE_NAMES_LABEL, this);
	deviceNamesLabel->setPosition(0,0);
	deviceNamesLabel->setFont("default.ttf",10);
	deviceNamesLabel->setColor(255,255,255,255);
	deviceNamesLabel->setShadowColor(255,0,0,128);
	deviceNamesLabel->setShadowOffset(-2,-2);
	deviceNamesLabel->setAlign((uint32_t)Margin::LEFT | (uint32_t)Margin::TOP);
	deviceNamesLabel->setText( deviceNames.c_str() );
	addWidget(deviceNamesLabel);

	deviceInfoFadeTimer = new Timer((uint32_t)TR_TOOLSET_ID::DEVICE_INFO_FADE_TIMER, this);
	deviceInfoFadeTimer->setOnTimerEvent(this,&Toolset::onTimer);
	deviceInfoFadeTimer->setTimeInterval(3);
	deviceInfoFadeTimer->setAutoTimer(false);
	deviceInfoFadeTimer->setTimer(true);
	addWidget(deviceInfoFadeTimer);

	deviceInfoFadeTimer->setActive(true);
	deviceInfoFadeTimer->setRender(false);

	this->windowHud = new InfoFade((uint32_t)TR_TOOLSET_ID::WINDOW_INFO_HUD, nullptr);
	this->windowHud->setFont("lucon.ttf",12);
	this->windowHud->setShadowColor(128, 128, 128, 128);
	this->windowHud->setShadowOffset(1, -1);
	this->windowHud->isWorldMatrix = false;
	addWidget(this->windowHud);

	this->infoHud = new LogFade((uint32_t)TR_TOOLSET_ID::LOG_INFO_HUD, nullptr);
	this->infoHud->setFont("default.ttf",10);
	this->infoHud->setShadowColor(128, 128, 128, 128);
	this->infoHud->setShadowOffset(1, -1);
	this->infoHud->isWorldMatrix = false;
	this->infoHud->setFadeTime(10.0f);
	this->infoHud->setVerticalLineSpace(28);
	this->infoHud->setPosition(0 , heightR / 2);
	this->infoHud->setAlign((uint32_t)Margin::LEFT | (uint32_t)Margin::DOWN);
	addWidget(this->infoHud);

	this->worldHud = new InfoFade((uint32_t)TR_TOOLSET_ID::WORLD_INFO_HUD, nullptr);
	this->worldHud->setFont("lucon.ttf",12);
	this->worldHud->setShadowColor(128, 128, 128, 128);
	this->worldHud->setShadowOffset(1, -1);
	this->worldHud->isWorldMatrix = true;
	this->worldHud->setOnBeginDrawEvent(this, &Toolset::onBeginDrawWorldHud);
	this->worldHud->setOnEndDrawEvent(this, &Toolset::onEndDrawWorldHud);
	this->worldHud->setAlign((uint32_t)Margin::LEFT | (uint32_t)Margin::DOWN);
	addWidget(this->worldHud);

	//gui sound
	selectButtonSound = soundMangager->createSoundPlayer();
	//selectButtonSound->setSound(resourceManager->getSoundEffect(UIManager::getSoundId(JA_GUI_BUTTON_SELECT)));
	//gui sound

	this->zoomFactor = 0.2f;
	this->maxWorldScale = 4.0f;
	this->minWorldScale = 0.05f;
}

void Toolset::init()
{
	if(singleton==nullptr)
	{
		singleton = new Toolset();
	}
}

Toolset* Toolset::getSingleton()
{
	return singleton;
}

void Toolset::cleanUp()
{
	delete singleton;
}

void Toolset::update()
{
	Interface::update();

	if(UIManager::getLastSelected() == nullptr)
	{
		if(InputManager::isKeyPressed(JA_3) && InputManager::isKeyModPress(JA_KMOD_SHIFT)) // #
		{
			if(gridDetail == TR_GRID_256)
			{
				gridDetail = TR_GRID_8;
				IsGridDrawed = false;
			}
			else
			{
				IsGridDrawed = true;
				gridDetail = GridDetail(int(gridDetail) * 2);
			}
		}

		if(InputManager::isKeyPressed(JA_1) && InputManager::isKeyModPress(JA_KMOD_SHIFT)) // !
		{
			IsTestHudDrawed = !IsTestHudDrawed;
		}

		if(InputManager::isKeyPressed(JA_a) && InputManager::isKeyModPress(JA_KMOD_CTRL))
		{
			IsGridAnchor = !IsGridAnchor;
		}
	}

	if(InputManager::isKeyPressed(JA_F9))
	{
		SoundManager* soundManager = SoundManager::getSingleton();
		uint32_t recordingDevice = soundManager->activeRecordingDevice;
		recordingDevice = (recordingDevice + 1) % soundManager->deviceNames.size();
		soundManager->openCaptureDevice(recordingDevice);
		activeDeviceLabel->setText(soundManager->deviceNames[recordingDevice].c_str());
		
		deviceNamesLabel->setRender(true);
		activeDeviceLabel->setRender(true);
		spectrumChart->setRender(true);
		deviceInfoFadeTimer->restartTimer();
		
		soundManager->startCapturing();
	}

	updateDrawPoint();

	CameraInterface* camera = GameManager::getSingleton()->getActiveCamera();
	worldScaleMultiplier = DisplayManager::getHeightMultiplier() * (1.0f / camera->scale.x);

	updateCamera(camera);

	if(IsTestHudDrawed)
	{
		jaVector2 position = camera->position;
		jaVector2 scale = camera->scale;
		TestHud::getSingleton()->setFieldValuePrintC(TR_HUD_CAMERAPOSITION, "Pos x=%4.2f y=%4.2f", position.x, position.y);
		TestHud::getSingleton()->setFieldValuePrintC(TR_HUD_CAMERASCALE, "Scal x=%4.2f y=%4.2f", scale.x, scale.y);

		int32_t mouseX = InputManager::getMousePositionX();
		int32_t mouseY = InputManager::getMousePositionY();
		int32_t mouseGuiX = mouseX;
		int32_t mouseGuiY = DisplayManager::getSingleton()->getResolutionHeight() - mouseY;
		
		testHud->setFieldValuePrintC(TR_HUD_MOUSEWINDOW,"Mouse Window x=%i y=%i", mouseX, mouseY);
		testHud->setFieldValuePrintC(TR_HUD_MOUSEGUI, "Mouse Gui x=%i y=%i", mouseGuiX, mouseGuiY);
		testHud->setFieldValuePrintC(TR_HUD_MOUSEWORLD,"Mouse x=%.2f y=%.2f", drawPoint.x, drawPoint.y);
		testHud->update();
	}
}

void Toolset::updateCamera(CameraInterface* camera)
{
	mouseWorldPosition = drawPoint;
	
	float deltaTime = GameManager::getSingleton()->getDeltaTime();
	int32_t x = 0,y = 0;

	InputManager::getRelativeMousePositionXY(&x,&y);
	if(InputManager::isMouseWheelUp())
	{
		if(camera->scale.x < this->maxWorldScale && camera->scale.y < this->maxWorldScale)
		{
			camera->scale.x += zoomFactor;
			camera->scale.y += zoomFactor;
		}	
	}

	if(InputManager::isMouseWheelDown())
	{
		camera->scale.x -= zoomFactor;
		camera->scale.y -= zoomFactor;
		if (camera->scale.x < this->minWorldScale && camera->scale.y < this->minWorldScale)
		{
			camera->scale.x = GIL_REAL(0.1);
			camera->scale.y = GIL_REAL(0.1);
		}
	}

	if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_MIDDLE))
	{
		anchorPoint = mouseWorldPosition;
		anchorCamera = camera->position;
	}

	if (InputManager::isMouseButtonPress((uint8_t)MouseButton::BUTTON_MIDDLE))
	{
		jaVector2 diffrence = anchorPoint - mouseWorldPosition;
		camera->position = anchorCamera + diffrence;
		anchorPoint = mouseWorldPosition + diffrence;
		anchorCamera = camera->position;
	}
}

void Toolset::updateDrawPoint()
{
	CameraInterface* camera = GameManager::getSingleton()->getActiveCamera();
	DisplayManager::pushWorldMatrix();
	DisplayManager::clearWorldMatrix();
	DisplayManager::setCameraMatrix(camera);

	double worldX,worldY,worldZ;
	DisplayManager::getWorldPosition(InputManager::getMousePositionX(),InputManager::getMousePositionY(),&worldX,&worldY,&worldZ);

	if(IsGridAnchor)
	{
		const float pixelsPerUnit = (float)setup::graphics::PIXELS_PER_UNIT;
		float cellSize = (float)gridDetail / pixelsPerUnit;
		float invCellSize = GIL_ONE / cellSize;

		drawPoint.x = (jaFloat)(floor(worldX * invCellSize)) * cellSize; // it is better due to proper allign
		drawPoint.y = (jaFloat)(floor(worldY * invCellSize)) * cellSize;
	}
	else
	{
		drawPoint.x = (float)worldX;
		drawPoint.y = (float)worldY;
	}

	drawPoint = ruler.transformPoint(drawPoint);

	DisplayManager::popWorldMatrix();
}

jaVector2 Toolset::getDrawPoint()
{
	return drawPoint;
}

void Toolset::draw()
{
	Interface::draw();

	if(isRendered())
	{
		CameraInterface* camera = GameManager::getSingleton()->getActiveCamera();
		DisplayManager::pushWorldMatrix();
		DisplayManager::clearWorldMatrix();
		DisplayManager::setCameraMatrix(camera);
		if(isGridDrawed())
			drawGrid(camera);

		//draw Ruler
		if(ruler.isActive())
		{
			gil::AABB2d cameraAABB;
			camera->getBoundingBox(&cameraAABB);
			ruler.draw(cameraAABB);
		}
		//draw Ruler
		drawDrawPoint();

		DisplayManager::popWorldMatrix();
		if(isTestHudDrawed())
			testHud->draw();
	}
}

void Toolset::setRulerActive(bool state)
{
	ruler.setActive(state);
}

bool Toolset::isRulerActive()
{
	return ruler.isActive();
}

ToolsetRuler* Toolset::getRuler()
{
	return &ruler;
}

void Toolset::drawGrid(CameraInterface* camera)
{
	jaVector2 position = camera->position;
	jaVector2 scale    = camera->scale;
	DisplayManager* display = DisplayManager::getSingleton();
	float halfWidth, halfHeight, cellSize;
	display->getWorldResolution(&halfWidth,&halfHeight);
	halfWidth = halfWidth / scale.x;
	halfHeight = halfHeight / scale.y;

	const float pixelsPerUnit = (float)setup::graphics::PIXELS_PER_UNIT;
	cellSize = (float)gridDetail / pixelsPerUnit;
	float invCellSize = 1.0f / cellSize;
	int32_t countX = (int32_t)( halfWidth * invCellSize ) ;
	int32_t countY = (int32_t)( halfHeight * invCellSize );
	halfWidth *= GIL_REAL(0.5);
	halfHeight *= GIL_REAL(0.5);

	jaVector2 begin = jaVector2(position.x - halfWidth, position.y - halfHeight);
	jaVector2 end   = jaVector2(position.x + halfWidth, position.y + halfHeight);

	begin.x = ceil(begin.x * invCellSize) * cellSize; 
	begin.y = ceil(begin.y * invCellSize) * cellSize;

	begin.x -= cellSize;
	begin.y -= cellSize;

	countX += 2;
	countY += 2;

	float interval = begin.x;

	float f64 = ((float)TR_GRID_64   / pixelsPerUnit);
	float f128 = ((float)TR_GRID_128 / pixelsPerUnit);
	float f256 = ((float)TR_GRID_256 / pixelsPerUnit);

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_ALPHA);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	DisplayManager::setColorUb(0,255,0,90);
	for(int32_t i=0; i< countX; i++) // pionowe
	{
		if(fmodf(interval,f256) == 0) DisplayManager::setLineWidth(3.0f);
		else
		{
			if(fmodf(interval,f128) == 0) DisplayManager::setLineWidth(2.0f);
			else
			{
				if(fmodf(interval,f64) == 0) DisplayManager::setLineWidth(1.0f);
				else DisplayManager::setLineWidth(0.1f);
			}
		}
		DisplayManager::drawLine(interval, begin.y, interval, end.y);
		interval += cellSize;
	}

	interval = begin.y;
	for(int32_t i=0; i< countY; i++) // poziome
	{
		if(fmodf(interval,f256) == 0) DisplayManager::setLineWidth(3.0f);
		else
		{
			if(fmodf(interval,f128) == 0) DisplayManager::setLineWidth(2.0f);
			else
			{
				if(fmodf(interval,f64) == 0) DisplayManager::setLineWidth(1.0f);
				else DisplayManager::setLineWidth(0.1f);
			}
		}
		DisplayManager::drawLine(begin.x, interval, end.x, interval);
		interval += cellSize;
	}

	glPopAttrib();
}

void Toolset::drawDrawPoint()
{
	DisplayManager::setPointSize(3.0f);
	DisplayManager::setColorUb(255,0,0);
	DisplayManager::drawPoint(drawPoint.x,drawPoint.y);
}

Toolset::~Toolset()
{
	TestHud::cleanUp();
}

void Toolset::onTimer( Widget* sender, WidgetEvent action, void* data )
{
	if (sender->getId() == (uint32_t)TR_TOOLSET_ID::DEVICE_INFO_FADE_TIMER)
	{
		deviceNamesLabel->setRender(false);
		activeDeviceLabel->setRender(false);
		spectrumChart->setRender(false);
	}
}

void Toolset::onBeginDrawWorldHud(Widget* sender, WidgetEvent action, void* data)
{
	CameraInterface* camera = GameManager::getSingleton()->getActiveCamera();

	DisplayManager::pushProjectionMatrix();
	DisplayManager::pushModelMatrix();
	DisplayManager::setCameraMatrix(camera);

	worldHud->worldScaleMultiplier = this->worldScaleMultiplier;
}

void Toolset::onEndDrawWorldHud(Widget* sender, WidgetEvent action, void* data)
{
	DisplayManager::popModelMatrix();
	DisplayManager::popProjectionMatrix();

	DisplayManager::setModelMatrixMode();
}

}