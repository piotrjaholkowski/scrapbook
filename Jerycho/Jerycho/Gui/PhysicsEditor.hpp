#pragma once

#include "../../Engine/Gui/DebugHud.hpp"
#include "../../Engine/Gui/Menu.hpp"
#include "../../Engine/Gui/Label.hpp"
#include "../../Engine/Gui/Editbox.hpp"
#include "../../Engine/Gui/ImageButton.hpp"
#include "../../Engine/Gui/ProgressButton.hpp"
#include "../../Engine/Gui/Button.hpp"
#include "../../Engine/Gui/SlideBox.hpp"

#include "../../Engine/Math/VectorMath2d.hpp"
#include "../../Engine/Gilgamesh/Narrow/Narrow2d.hpp"
#include "../../Engine/Manager/ResourceManager.hpp"
#include "../../Engine/Physics/RigidBody2d.hpp"
#include "../../Engine/Manager/PhysicsManager.hpp"

#include "../../Engine/Physics/PhysicObjectDef2d.hpp"
#include "PhysicsComponents.hpp"

#include <vector>

namespace tr
{

	enum class PhysicsEditorMode
	{
		SELECT_MODE = 0,
		TRANSLATE_MODE = 1,
		ROTATE_MODE = 2,
		SIZE_MODE = 3,
		COMPONENT_EDITOR_MODE = 4
	};

	enum class PhysicsEditorInfo
	{
		TRANSLATE_INFO = 0,
		ROTATE_INFO = 1,
		SCALE_INFO = 2,
		TEX_SCALE_INFO = 3,
		DELETE_INFO = 4,
		COMPONENT_EDIT_INFO = 5
	};

	enum class PhysicsEditorHud
	{
		BODY_ID_HUD = 0,
		BODY_TYPE_HUD = 1,
		GRID_SIZE_HUD = 2,
		WARM_START_HUD = 3,
		FIX_CONSTRAINTS_HUD = 4,
		MODE_HUD = 5,
		HELPER_HUD = 6,
		STATIC_HELPER_HUD = 7
	};

	class PhysicObjectState
	{
	public:
		jaVector2 position;
		jaMatrix2 rotMatrix;
		jaVector2 size;
	};

	typedef void(*PhysicObjectFilterPtr)(ja::PhysicObject2d*);

	class PhysicsEditor : public ja::Menu
	{
	public:
		ja::DebugHud hud;
	private:
		static PhysicsEditor* singleton;
		PhysicsEditorMode     editMode;

		int8_t    shapeTypeId;
		jaVector2 gizmo;

		StackAllocator<gil::ObjectHandle2d*>* selectedPhysicObjectsTemp;
		std::vector<PhysicObjectState>        selectedPhysicObjectStates;

		bool    objectSpace;
		int32_t bodySelectedIt;

		ja::PhysicObjectDef2d* physicObjectDef;
		ja::ConstraintDef2d*   constraintDef;

		PhysicsComponentMenu* physicsMenu;
		PhysicsSpaceBarMenu*  spaceBarMenu;
		PhysicsSceneMenu*	  sceneMenu;
		PhysicsTimerMenu*	  timerMenu;
		ja::SlideBox*		  physicsBar;
		CommonWidget*		  currentComponentEditor;

		bool drawBoxSelection;
		bool drawEntity;
		bool drawHud;
		jaVector2 selectionStartPoint;

		PhysicsEditor();
		void createGUI();
		void calculateSelectionMidpoint();
		void addToSelection(StackAllocator<gil::ObjectHandle2d*>* selection);
		void removeFromSelection(StackAllocator<gil::ObjectHandle2d*>* selection);
	public:
		std::list<ja::PhysicObject2d*> selectedPhysicObjects;

		void gatherResources();
		void addToSelection(ja::PhysicObject2d* physicObject);
		void removeFromSelection(ja::PhysicObject2d* physicObject);

		void updateComponentEditors(ja::PhysicObject2d* physicObject);
		static void init();
		static PhysicsEditor* getSingleton();
		static void cleanUp();
		~PhysicsEditor();

		void setEditorMode(PhysicsEditorMode mode);
		void setEditorMode(CommonWidget* commonWidget);
		PhysicsEditorMode getEditorMode();
		CommonWidget*     getCurrentEditor();
		void setDefaultParameters();

		void setShape(ShapeType shapeType);
		void nextShape();
		void previousShape();
		void nextBodyType();
		void previousBodyType();
		
		jaVector2 getGizmoPosition();

		void moveBody();
		void rotateBody();
		void duplicateSelectedAtPoint(jaVector2& point);
		void saveBodySize();
		void loadBodySize();
		void sizeBody();
		void createBody();

		void update();
		void draw();
		void drawGizmo();
		void drawSelectedPhysicObjects();

		void onClickMenuBar(ja::Widget* sender, ja::WidgetEvent action, void* data);

		void saveSelectedPhysicsObjectStates();
		void applyFilterToSelection(PhysicObjectFilterPtr physicObjectFilter);
		//filters
		static void deletePhysicObject(ja::PhysicObject2d* physicObject);
		static void changeShape(ja::PhysicObject2d* physicObject);
	};

}