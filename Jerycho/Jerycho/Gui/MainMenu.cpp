#include "MainMenu.hpp"
#include "WidgetId.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"
#include "../../Engine/Manager/ScriptManager.hpp"
#include "../../Engine/Manager/LogManager.hpp"
#include "../../Engine/Gui/TestHud.hpp"
#include "../../Engine/Utility/Exception.hpp"

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

namespace tr
{
	MainMenu* MainMenu::singleton = nullptr;

MainMenu::MainMenu() : Menu((uint32_t)TR_MENU_ID::MAINMENU, nullptr)
{
	lastInterface = nullptr;
	isExit = false;

	Toolset::init();
	toolset = Toolset::getSingleton();

	ScriptConsole::init();
	scriptConsole = ScriptConsole::getSingleton();

	LayerObjectEditor::init();
	layerObjectEditor = LayerObjectEditor::getSingleton();

	AnimationEditor::init();
	animationEditor = AnimationEditor::getSingleton();

	ResourceEditor::init();
	resourceEditor = ResourceEditor::getSingleton();

	PhysicsEditor::init();
	physicsEditor = PhysicsEditor::getSingleton();

	EntityEditor::init();
	entityEditor = EntityEditor::getSingleton();

	GamePlay::init();
	gamePlay = GamePlay::getSingleton();

	Loader::init();
	loader = Loader::getSingleton();

	TestHud::getSingleton()->setFieldValuePrintC(TR_HUD_GAMESTATE,"Game state: Main menu");

	toolset->setRender(true);
	toolset->setActive(true);

	layerObjectEditor->setRender(true);
	layerObjectEditor->setActive(true);

	animationEditor->setRender(true);
	animationEditor->setActive(true);

	resourceEditor->setRender(true);
	resourceEditor->setActive(true);

	physicsEditor->setRender(true);
	physicsEditor->setActive(true);

	entityEditor->setRender(true);
	entityEditor->setActive(true);

	scriptConsole->setRender(true);
	scriptConsole->setActive(true);

	gamePlay->setRender(true);
	gamePlay->setActive(true);

	loader->setRender(true);
	loader->setActive(true);

	setRender(false);
	setActive(false);
	int32_t widthR, heightR;
	DisplayManager::getResolution(&widthR,&heightR);

	setText("Main menu");
	setPosition(widthR/2,heightR/2);

	newGameButton = new Button((uint32_t)TR_MAINMENU_ID::NEWGAME, this);
	newGameButton->setFont("lucon.ttf",12);
	newGameButton->setText("Do nothing");
	newGameButton->setColor(255,0,0);
	newGameButton->setFontColor(255,255,255,255);
	newGameButton->setPosition(-100,-25);
	newGameButton->setSize(200,25);
	newGameButton->setOnClickEvent(this,&MainMenu::onNewGame);
	newGameButton->setOnMouseOverEvent(this,&MainMenu::onMouseOver);
	newGameButton->setOnMouseLeaveEvent(this,&MainMenu::onMouseLeave);
	addWidget(newGameButton);

	exitButton = new Button((uint32_t)TR_MAINMENU_ID::EXIT, this);
	exitButton->setFont("lucon.ttf",12);
	exitButton->setText("Exit");
	exitButton->setColor(255,0,0);
	exitButton->setFontColor(255,255,255,255);
	exitButton->setPosition(-100,-55);
	exitButton->setSize(200,25);
	exitButton->setOnClickEvent(this,&MainMenu::onExit);
	exitButton->setOnMouseOverEvent(this,&MainMenu::onMouseOver);
	exitButton->setOnMouseLeaveEvent(this,&MainMenu::onMouseLeave);
	addWidget(exitButton);

	setGameState(GameState::MAINMENU_MODE);
}

MainMenu::~MainMenu()
{
	LayerObjectEditor::cleanUp();
	AnimationEditor::cleanUp();
	ResourceEditor::cleanUp();
	ScriptConsole::cleanUp();
	PhysicsEditor::cleanUp();
	EntityEditor::cleanUp();
	Toolset::cleanUp();
	GamePlay::cleanUp();
	Loader::cleanUp();
}

MainMenu* MainMenu::getSingleton()
{
	return singleton;
}

void MainMenu::cleanUp()
{
	if(singleton!=nullptr)
	{
		delete singleton;
	}
}

void MainMenu::setGameState(GameState gameState)
{
	if(this->gameState == gameState) return;
	previousGameState = this->gameState;
	this->gameState = gameState;

	scriptConsole->activeConsole(false);

	switch(gameState)
	{
	case GameState::MAINMENU_MODE:
		TestHud::getSingleton()->setFieldValue(TR_HUD_GAMESTATE, "Game state: Main menu");
		setRender(true);
		setActive(true);
		UIManager::setSelected(newGameButton);
		break;
	case GameState::LAYEROBJECTEDITOR_MODE:
		TestHud::getSingleton()->setFieldValue(TR_HUD_GAMESTATE, "Game state: Layer object editor");
		setRender(false);
		setActive(false);
		break;
	case GameState::ANIMATIONEDITOR_MODE:
		TestHud::getSingleton()->setFieldValue(TR_HUD_GAMESTATE, "Game state: Animation editor");
		setRender(false);
		setActive(false);
		break;
	case GameState::RESOURCEEDITOR_MODE:
		TestHud::getSingleton()->setFieldValue(TR_HUD_GAMESTATE, "Game state: Resource editor");
		this->resourceEditor->updateComponentEditors();
		setRender(false);
		setActive(false);
		break;
	case GameState::PHYSICSEDITOR_MODE:
		TestHud::getSingleton()->setFieldValue(TR_HUD_GAMESTATE, "Game state: Physics editor");
		setRender(false);
		setActive(false);
		break;
	case GameState::ENTITYEDITOR_MODE:
		TestHud::getSingleton()->setFieldValue(TR_HUD_GAMESTATE, "Game state: Entity editor");
		setRender(false);
		setActive(false);
		break;
	case GameState::SCRIPTCONSOLE_MODE:
		TestHud::getSingleton()->setFieldValue(TR_HUD_GAMESTATE, "Game state: Script console");
		setRender(false);
		setActive(false);
		scriptConsole->activeConsole(true);
		break;
	case GameState::GAMEPLAY_MODE:
		TestHud::getSingleton()->setFieldValue(TR_HUD_GAMESTATE, "Game state: Gameplay");
		setRender(false);
		setActive(false);
		break;
	case GameState::LOADING_MODE:
		TestHud::getSingleton()->setFieldValue(TR_HUD_GAMESTATE, "Game state: Loading");
		loader->restartLoader();
		setRender(false);
		setActive(false);
		break;
	case GameState::BLANK_MODE:
		setRender(false);
		setActive(false);
		break;
	}

	switch(previousGameState)
	{
	case GameState::MAINMENU_MODE:
		lastInterface = nullptr;
		break;
	case GameState::LAYEROBJECTEDITOR_MODE:
		lastInterface = layerObjectEditor;
		break;
	case GameState::ANIMATIONEDITOR_MODE:
		lastInterface = animationEditor;
		break;
	case GameState::RESOURCEEDITOR_MODE:
		lastInterface = resourceEditor;
		break;
	case GameState::SCRIPTCONSOLE_MODE:
		lastInterface = scriptConsole;
		break;
	case GameState::PHYSICSEDITOR_MODE:
		lastInterface = physicsEditor;
		break;
	case GameState::ENTITYEDITOR_MODE:
		lastInterface = entityEditor;
		break;
	case GameState::GAMEPLAY_MODE:
		lastInterface = gamePlay;
		break;
	case GameState::LOADING_MODE:
		lastInterface = loader;
		break;
	case GameState::BLANK_MODE:
		lastInterface = nullptr;
		break;
	}

	lua_State* L = ScriptManager::getSingleton()->getLuaState();
	lua_getglobal(L, "onGuiChangeState");

	if(lua_isfunction(L, 1))
	{
		lua_pushinteger(L,(uint32_t)(previousGameState));
		lua_pushinteger(L,(uint32_t)(gameState));
		lua_call(L, 2, 0);
	}
	else
	{
		lua_remove(L,1);
	}
}

GameState MainMenu::getGameState()
{
	return gameState;
}

void MainMenu::onDropFile(const char* filePath)
{
	std::string fileString(filePath);

	size_t dotFound = fileString.find_last_of('.');

	if (string::npos == dotFound)
	{
		Toolset::getSingleton()->infoHud->addInfo(255, 0, 0, "Can't specify file extension");
	}

	std::string fileExtension;
	fileExtension.resize(fileString.length() - dotFound);
	std::transform(fileString.begin() + dotFound, fileString.end(), fileExtension.begin(), ::toupper);

	if (fileExtension == ".LUA")
	{
		//remember last log here
		const uint32_t oldLogsNum = LogManager::getSingleton()->getLogsNum();

		Toolset::getSingleton()->infoHud->addInfo(255, 255, 0, "Recognized lua script");
		
		bool       isInScriptPath = false;
		ja::string fileInScriptPath(filePath);
		ResourceManager::getSingleton()->getInScriptPath(fileInScriptPath, isInScriptPath);

		if (!isInScriptPath)
		{
			Toolset::getSingleton()->infoHud->addInfoC(0, 0, 255, "Put file in %s first", ResourceManager::getSingleton()->getScriptsPath());
			return;
		}

		
		if (ScriptManager::getSingleton()->runScriptFile(fileInScriptPath,false))
		{
			Toolset::getSingleton()->infoHud->addInfoC(0, 255, 0, "Script %s executed", fileInScriptPath.c_str());
			EntityEditor::getSingleton()->refresh();
		}
		else
		{
			Toolset::getSingleton()->infoHud->addInfoC(255, 0, 0, "Script %s could not be compiled", fileInScriptPath.c_str());
		}

		const uint32_t newLogsNum = LogManager::getSingleton()->getLogsNum();

		if (oldLogsNum < newLogsNum)
		{
			Toolset*    toolset    = Toolset::getSingleton();
			LogManager* logManager = LogManager::getSingleton();

			for (uint32_t i = oldLogsNum; i < newLogsNum; ++i)
			{
				LogMessage* logMessage = logManager->getLogMessage(i);

				uint8_t r, g, b;

				switch (logMessage->logType)
				{
				case LogType::SUCCESS_LOG:
					r = 0; g = 255; b = 0;
					break;
				case LogType::INFO_LOG:
					r = 255; g = 255; b = 0;
					break;
				case LogType::WARNNING_LOG:
					r = 0; g = 255; b = 255;
					break;
				case LogType::ERROR_LOG:
					r = 255; g = 0; b = 0;
					break;
				}
				toolset->infoHud->addInfo(r, g, b, logMessage->message);
			}
		}
		
		EntityEditor::getSingleton()->gatherResources();

		return;
	}

	if (fileExtension == ".XML")
	{
		Toolset::getSingleton()->infoHud->addInfo(255, 255, 0, "Recognized xml file");
		return;
	}

	if (fileExtension == ".OGG")
	{
		Toolset::getSingleton()->infoHud->addInfo(255, 255, 0, "Recognized ogg file");
		return;
	}

	Toolset::getSingleton()->infoHud->addInfoC(255, 0, 0, "Loader doesn't support %s file extension", fileExtension.c_str());
}

void MainMenu::update()
{
	Interface::update();
	
	if (gameState != GameState::LOADING_MODE)
	{

		if (InputManager::isKeyPressed(JA_F1))
		{
			setGameState(GameState::RESOURCEEDITOR_MODE);
		}

		if(InputManager::isKeyPressed(JA_F2))
		{
			setGameState(GameState::SCRIPTCONSOLE_MODE);
		}

		if(InputManager::isKeyPressed(JA_F3))
		{
			setGameState(GameState::PHYSICSEDITOR_MODE);
		}

		if(InputManager::isKeyPressed(JA_F4))
		{
			setGameState(GameState::ENTITYEDITOR_MODE);
		}

		if(InputManager::isKeyPressed(JA_F6))
		{
			setGameState(GameState::ANIMATIONEDITOR_MODE);
		}

		if(InputManager::isKeyPressed(JA_F7))
		{
			setGameState(GameState::BLANK_MODE);
		}

		if (InputManager::isKeyPressed(JA_F8))
		{
			setGameState(GameState::LAYEROBJECTEDITOR_MODE);
		}

		

		if(InputManager::isKeyPressed(JA_F10))
		{
			setGameState(GameState::GAMEPLAY_MODE);
		}

	}
	
	switch(gameState)
	{
		case GameState::LAYEROBJECTEDITOR_MODE:
			layerObjectEditor->update();
			break;
		case GameState::ANIMATIONEDITOR_MODE:
			animationEditor->update();
			break;
		case GameState::RESOURCEEDITOR_MODE:
			resourceEditor->update();
			break;
		case GameState::SCRIPTCONSOLE_MODE:
			scriptConsole->update();
			break;
		case GameState::PHYSICSEDITOR_MODE:
			physicsEditor->update();
			break;	
		case GameState::ENTITYEDITOR_MODE:
			entityEditor->update();
			break;
		case GameState::GAMEPLAY_MODE:
			gamePlay->update();
			break;
		case GameState::LOADING_MODE:
			loader->update();
			break;
		case GameState::BLANK_MODE:
			toolset->update();
			break;
	}
}

void MainMenu::draw()
{
	Interface::draw();

	switch(gameState)
	{
	case GameState::GAMEPLAY_MODE:
		gamePlay->draw();
		break;
	case GameState::LAYEROBJECTEDITOR_MODE:
		layerObjectEditor->draw();
		break;
	case GameState::ANIMATIONEDITOR_MODE:
		animationEditor->draw();
		break;
	case GameState::RESOURCEEDITOR_MODE:
		resourceEditor->draw();
		break;
	case GameState::PHYSICSEDITOR_MODE:
		physicsEditor->draw();
		break;
	case GameState::ENTITYEDITOR_MODE:
		entityEditor->draw();
		break;
	case GameState::SCRIPTCONSOLE_MODE:
		if(lastInterface != nullptr)
			lastInterface->draw();
		scriptConsole->draw();
		break;
	case GameState::LOADING_MODE:
		loader->draw();
		break;
	case GameState::BLANK_MODE:
		toolset->draw();
		break;
	}

}

void MainMenu::init()
{
	if(singleton==nullptr)
	{
		singleton = new MainMenu();
	}
}

void MainMenu::onNewGame( Widget* sender, WidgetEvent action, void* data )
{

}

void MainMenu::onExit( Widget* sender, WidgetEvent action, void* data )
{
	isExit = true;
}

void MainMenu::onMouseOver(Widget* sender, WidgetEvent action, void* data)
{
	if(sender->getWidgetType() == JA_BUTTON)
	{
		Button* button = (Button*)sender;
		button->setColor(0,255,0);
		toolset->selectButtonSound->play();
	}
}

void MainMenu::onMouseLeave(Widget* sender, WidgetEvent action, void* data)
{
	if(sender->getWidgetType() == JA_BUTTON)
	{
		Button* button = (Button*)sender;
		button->setColor(255,0,0);
	}
}



}