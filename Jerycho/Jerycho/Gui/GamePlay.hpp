#pragma once

#include "../../Engine/Gui/DebugHud.hpp"
#include "../../Engine/Gui/Menu.hpp"
#include "../../Engine/Math/VectorMath2d.hpp"
#include "../../Engine/Gilgamesh/Narrow/Narrow2d.hpp"
#include "../../Engine/Manager/ResourceManager.hpp"
#include <vector>

using namespace ja;

namespace tr {

	enum GamePlayHud
	{
		TR_PLAYER_POSITION_HUD = 0,
	};

	class GamePlay : public Menu
	{
	private:
		DebugHud hud;
		static GamePlay* singleton;

		GamePlay();
	public:
		static void init();
		static GamePlay* getSingleton();
		static void cleanUp();
		~GamePlay();

		void update();
		void updatePlayerControl();
		void draw();
	};

}
