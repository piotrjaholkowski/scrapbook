#include "EntityEditor.hpp"
#include "WidgetId.hpp"

#include "MainMenu.hpp"

#include "../../Engine/Manager/EntityManager.hpp"
#include "../../Engine/Manager/ScriptManager.hpp"
#include "../../Engine/Manager/GameManager.hpp"

using namespace ja;

namespace tr
{

EntityEditor* EntityEditor::singleton = nullptr;


EntityEditor::EntityEditor() : Menu((uint32_t)TR_MENU_ID::ENTITYEDITOR, nullptr)
{
	this->singleton = this;

	fontDebugRender = ResourceManager::getSingleton()->getEternalFont("debug.ttf",12)->resource;

	hud.setFont("default.ttf", 10, jaLeftLowerCorner,255,255,255,255);
	hud.setFont("default.ttf", 10, jaRightLowerCorner,255,255,255,255);
	hud.setFont("default.ttf", 10, jaLeftUpperCorner,255,255,255,255);
	hud.setFont("default.ttf", 10, jaRightUpperCorner,255,255,255,255);

	hud.addField(jaLeftUpperCorner,(uint32_t)EntityEditorHud::ENTITY_ID_HUD,"Id=");
	hud.addField(jaRightUpperCorner,(uint32_t)EntityEditorHud::ENTITY_EDITOR_MODE_HUD,"Mode=");

	hud.addField(jaRightLowerCorner,(uint32_t)EntityEditorHud::ENTITY_EDITOR_HELPER_HUD,"");
	hud.addField(jaLeftLowerCorner,(int32_t)EntityEditorHud::ENTITY_EDITOR_STATIC_HELPER_HUD,"A - Draw Icon\nB - Draw Bound\nE - Draw description");

	setText("Entity Editor");
	setPosition(0,0);

	createGUI();

	setDefaultParameters();
	setEditorMode(EntityEditorMode::ENTITY_EDIT_MODE);
}

void EntityEditor::createGUI()
{
	this->componentMenu = new EntityComponentMenu(0, this);
	addWidget(this->componentMenu);

	this->scriptFunctionViewer = new ScriptFunctionViewer(1, this);
	scriptFunctionViewer->setRender(false);
	scriptFunctionViewer->setActive(false);
	addWidget(this->scriptFunctionViewer);

	this->dataModelViewer = new DataModelViewer(2, this);
	dataModelViewer->setRender(false);
	dataModelViewer->setActive(false);
	addWidget(this->dataModelViewer);
}


EntityEditor::~EntityEditor()
{
	
}

void EntityEditor::init()
{
	if(singleton == nullptr)
	{
		singleton = new EntityEditor();
	}
}

EntityEditor* EntityEditor::getSingleton()
{
	return singleton;
}

void EntityEditor::cleanUp()
{
	if(singleton != nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void EntityEditor::setEditorMode(EntityEditorMode mode)
{
	editMode = mode;

	switch(editMode)
	{
	case EntityEditorMode::ENTITY_EDIT_MODE:
		hud.setFieldValue((uint32_t)EntityEditorHud::ENTITY_EDITOR_MODE_HUD,"Mode=Edit Entity");
		hud.setFieldValue((uint32_t)EntityEditorHud::ENTITY_EDITOR_HELPER_HUD,"LMB - Pick entity");
		break;
	}
}

void EntityEditor::setDefaultParameters()
{
	

}

void EntityEditor::update()
{
	Toolset::getSingleton()->update();
	Interface::update();

	if(!UIManager::isMouseOverGui())
	{
		if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
		{

		}

		//A - Draw Icon\nB - Draw Bound\nE - Draw description
		if(InputManager::isKeyPressed(JA_a))
		{
			
		}

		if(InputManager::isKeyPressed(JA_e))
		{
			
		}
	}

	if(InputManager::isKeyPressed(JA_ESCAPE))
	{
		MainMenu::getSingleton()->setGameState(GameState::MAINMENU_MODE);
	}

	if (editMode == EntityEditorMode::ENTITY_EDIT_MODE)
	{
		/*if(jaInputManager::isKeyPressed(JA_d))
		{
			deleteCurrentEntity();
			setEditorMode(TR_ENTITY_PICK_MODE);
			updateEntityInfo();
		}

		if(jaInputManager::isKeyPressed(JA_ESCAPE))
		{
			unsigned short firstEntityId = static_cast<unsigned short>(floor(entityScrollbar->getValue()));

			if((entity->getId() >= firstEntityId) && (entity->getId() < (firstEntityId + TR_ENTITY_NUM_LIST))) // need to update entity list
			{
				unsigned short tableId = entity->getId() - firstEntityId;
				entitiesButtons[tableId]->setAlpha(0);
			}
			setEditorMode(TR_ENTITY_PICK_MODE);
			updateEntityInfo();
			entity = NULL;
		}*/
	}

	if(InputManager::isKeyModPress(JA_KMOD_SHIFT) && InputManager::isKeyPressed(JA_8))
	{
		hud.setRender(!hud.isRendered());
	}
	
	hud.update();
}

void EntityEditor::draw()
{
	CameraInterface* camera = GameManager::getSingleton()->getActiveCamera();
	DisplayManager::pushWorldMatrix();
	DisplayManager::clearWorldMatrix();
	DisplayManager::setCameraMatrix(camera);
	
	DisplayManager::setColorUb( 0,0,255,128 );
	DisplayManager::setLineWidth( 3.0f );
	
	DisplayManager::setColorUb( 0,255,0,128 );
	DisplayManager::setLineWidth( 3.0f );
	PhysicsEditor::getSingleton()->drawSelectedPhysicObjects();
	LayerObjectEditor::getSingleton()->drawSelectedLayerObjects();

	PhysicsManager::getSingleton()->drawUser = drawPhysicObject;
	DisplayManager::setLineWidth( 1.0f );
	
	PhysicsManager::getSingleton()->drawDebug(camera);

	DisplayManager::popWorldMatrix();

	Toolset::getSingleton()->draw();
	Interface::draw();

	hud.draw();
}

void EntityEditor::refresh()
{
	EntityListEditor::getSingleton()->refresh();
}

void EntityEditor::addToSelection( Entity* entity )
{
	selectedEntities.remove(entity);
	selectedEntities.push_back(entity);
}

bool EntityEditor::isInSelection( Entity* entity )
{
	std::list<Entity*>::iterator it = selectedEntities.begin();
	it = selectedEntities.begin();

	for(it;it != selectedEntities.end(); ++it)
	{
		if((*it) == entity)
		{
			return true;
		}
	}

	return false;
}

void EntityEditor::applyFilterToSelectedEntities( EntityFilterPtr entityFilter )
{
	std::list<Entity*>::iterator itEntity = selectedEntities.begin();

	for(itEntity; itEntity != selectedEntities.end(); ++itEntity)
	{
		entityFilter(*itEntity);
	}
}

void EntityEditor::drawPhysicObject( PhysicObject2d* physicObject )
{
	if(physicObject->getComponentData() != nullptr)
	{
		PhysicComponent* physicComponent = (PhysicComponent*)(physicObject->getComponentData());
		float worldScaleMultiplier = Toolset::getSingleton()->worldScaleMultiplier;

		if(physicComponent->getEntity()->getPhysicObject() == physicObject) // root
		{
			char entityName[setup::TAG_NAME_LENGTH + 1];
			ObjectHandle2d* object = physicObject->getShapeHandle();
			Entity* entity = physicComponent->getEntity();

	#pragma warning(disable : 4996)
			if(physicComponent->getEntity()->getHash() == 0)
			{
				sprintf(entityName, "Entity %hu", entity->getId());
			}
			else
			{
				const char* tagName = entity->getTagName();
				sprintf(entityName, "%s %u", tagName, entity->getId());
			}
	#pragma warning(default : 4996)

			DisplayManager::RenderTextWorld(singleton->fontDebugRender, object->transform.position.x, object->transform.position.y, worldScaleMultiplier, entityName, (uint32_t)Margin::LEFT, 255, 70, 0, 255);
		}
		
		if(physicComponent->getHash() != 0)
		{
			ObjectHandle2d* object = physicObject->getShapeHandle();
			const char* tagName = physicComponent->getTagName();
			DisplayManager::RenderTextWorld(singleton->fontDebugRender, object->transform.position.x, object->transform.position.y, worldScaleMultiplier, tagName, (uint32_t)Margin::LEFT | (uint32_t)Margin::TOP, 0, 255, 0, 255);
		}
	}
	
}

void EntityEditor::gatherResources()
{
	this->scriptFunctionViewer->reloadResources();
	this->dataModelViewer->reloadResources();
}

}


