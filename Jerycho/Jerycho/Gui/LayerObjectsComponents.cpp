#include "LayerObjectsComponents.hpp"
#include "WidgetId.hpp"
#include "LayerObjectEditor.hpp"
#include "Toolset.hpp"

#include "../../Engine/Manager/LayerObjectManager.hpp"
#include "../../Engine/Manager/PhysicsManager.hpp"
#include "../../Engine/Manager/LevelManager.hpp"
#include "../../Engine/Manager/EffectManager.hpp"
#include "../../Engine/Manager/GameManager.hpp"

#include "../../Engine/Gui/Margin.hpp"
#include "../../Engine/Gui/Editbox.hpp"
#include "../../Engine/Gui/ProgressButton.hpp"
#include "../../Engine/Gui/GridLayout.hpp"
#include "../../Engine/Gui/FlowLayout.hpp"
#include "../../Engine/Gui/TextField.hpp"
#include "../../Engine/Gui/BoxLayout.hpp"
#include "../../Engine/Gui/Scrollbar.hpp"
#include "../../Engine/Gui/Checkbox.hpp"
#include "../../Engine/Gui/RadioButton.hpp"
#include "../../Engine/Gui/GridViewer.hpp"
#include "../../Engine/Gui/SlideWidget.hpp"
#include "../../Engine/Gui/SequenceEditor.hpp"
#include "../../Engine/Gui/ScrollPane.hpp"
#include "../../Engine/Gui/Hider.hpp"
#include "../../Engine/Gui/Magnet.hpp"

#include <algorithm>

using namespace ja;

namespace tr
{

LayerObjectComponentMenu* LayerObjectComponentMenu::singleton = nullptr;

LayerObjectShapeEditor*  LayerObjectShapeEditor::singleton  = nullptr;
LayerObjectColorEditor*  LayerObjectColorEditor::singleton  = nullptr;
PolygonBoxEditor*        PolygonBoxEditor::singleton    = nullptr;
PolygonCircleEditor*     PolygonCircleEditor::singleton = nullptr;
LightEditor*             LightEditor::singleton = nullptr;
CameraEditor*            CameraEditor::singleton = nullptr;
PolygonEditor*           PolygonEditor::singleton = nullptr;
MorphEditor*             MorphEditor::singleton = nullptr;
LayersEditor*            LayersEditor::singleton = nullptr;
ParallaxEditor*          ParallaxEditor::singleton = nullptr;
TextEditor*              TextEditor::singleton = nullptr;

LayerObjectComponentMenu::LayerObjectComponentMenu(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Menu", parent)
{
	singleton = this;

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(150, 250);
	fillBoxLayout(boxLayout);

	Button* sceneButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::SCENE_EDITOR, nullptr);
	fillButton(sceneButton, "Scene");
	sceneButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(sceneButton);

	Button* shapeButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::LAYER_OBJECT_EDITOR, nullptr);
	fillButton(shapeButton, "Layer Object");
	shapeButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(shapeButton);

	Button* colorButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::COLOR_EDITOR, nullptr);
	fillButton(colorButton, "Color");
	colorButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(colorButton);

	Button* lightButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::LIGHT_EDITOR, nullptr);
	fillButton(lightButton, "Light");
	lightButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(lightButton);

	Button* cameraButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::CAMERA_EDITOR, nullptr);
	fillButton(cameraButton, "Camera");
	cameraButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(cameraButton);

	Button* polygonButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::POLYGON_EDITOR, nullptr);
	fillButton(polygonButton, "Polygon");
	polygonButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(polygonButton);

	Button* morphButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::MORPH_EDITOR, nullptr);
	fillButton(morphButton, "Morph");
	morphButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(morphButton);

	Button* layersButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::LAYERS_EDITOR, nullptr);
	fillButton(layersButton, "Layers");
	layersButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(layersButton);

	Button* parallaxButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::PARALLAX_EDITOR, nullptr);
	fillButton(parallaxButton, "Parallax");
	parallaxButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(parallaxButton);

	Button* materialButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::MATERIAL_EDITOR, nullptr);
	fillButton(materialButton, "Material");
	materialButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(materialButton);

	Button* textButton = new Button((int32_t)TR_LAYEROBJECTEDITOR_ID::TEXT_EDITOR, nullptr);
	fillButton(textButton, "Text");
	textButton->setOnClickEvent(this, &LayerObjectComponentMenu::onMenuClick);
	boxLayout->addWidget(textButton);

	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::SCENE_EDITOR] = new LayerObjectSceneEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::SCENE_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::SCENE_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::LAYER_OBJECT_EDITOR] = new LayerObjectShapeEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::LAYER_OBJECT_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::LAYER_OBJECT_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::COLOR_EDITOR] = new LayerObjectColorEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::COLOR_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::COLOR_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::LIGHT_EDITOR] = new LightEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::LIGHT_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::LIGHT_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::CAMERA_EDITOR] = new CameraEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::CAMERA_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::CAMERA_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::POLYGON_EDITOR] = new PolygonEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::POLYGON_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::POLYGON_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::MORPH_EDITOR] = new MorphEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::MORPH_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::MORPH_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::LAYERS_EDITOR] = new LayersEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::LAYERS_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::LAYERS_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::PARALLAX_EDITOR] = new ParallaxEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::PARALLAX_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::PARALLAX_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::MATERIAL_EDITOR] = new LayerObjectMaterialEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::MATERIAL_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::MATERIAL_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::TEXT_EDITOR] = new TextEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::TEXT_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::TEXT_EDITOR]);

	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::BOX_EDITOR] = new PolygonBoxEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::BOX_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::BOX_EDITOR]);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::CIRCLE_EDITOR] = new PolygonCircleEditor((uint32_t)TR_LAYEROBJECTEDITOR_ID::CIRCLE_EDITOR, this);
	addWidget(components[(int32_t)TR_LAYEROBJECTEDITOR_ID::CIRCLE_EDITOR]);

	addWidgetToEditor(boxLayout);
}

void LayerObjectComponentMenu::onMenuClick(Widget* sender, WidgetEvent action, void* data)
{
	uint32_t id = sender->getId();
	components[id]->setActive(true);
	components[id]->setRender(true);

	setTopWidget(components[id]);
}

void LayerObjectComponentMenu::updateComponentEditors(LayerObject* layerObject)
{
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::LAYER_OBJECT_EDITOR]->updateWidget(layerObject);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::COLOR_EDITOR]->updateWidget(layerObject);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::LIGHT_EDITOR]->updateWidget(layerObject);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::CAMERA_EDITOR]->updateWidget(layerObject);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::POLYGON_EDITOR]->updateWidget(layerObject);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::MORPH_EDITOR]->updateWidget(layerObject);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::MATERIAL_EDITOR]->updateWidget(layerObject);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::TEXT_EDITOR]->updateWidget(layerObject);

	
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::BOX_EDITOR]->updateWidget(layerObject);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::CIRCLE_EDITOR]->updateWidget(layerObject);
}

void LayerObjectComponentMenu::onCreateNotify(ja::LayerObject* layerObject)
{
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::CAMERA_EDITOR]->onCreateNotify(layerObject);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::PARALLAX_EDITOR]->onCreateNotify(layerObject);
}

void LayerObjectComponentMenu::onDeleteNotify(ja::LayerObject* layerObject)
{
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::CAMERA_EDITOR]->onDeleteNotify(layerObject);
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::PARALLAX_EDITOR]->onDeleteNotify(layerObject);
}

void LayerObjectComponentMenu::reloadResources()
{
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::SCENE_EDITOR]->reloadResources();
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::LAYERS_EDITOR]->reloadResources();
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::PARALLAX_EDITOR]->reloadResources();
	components[(int32_t)TR_LAYEROBJECTEDITOR_ID::MATERIAL_EDITOR]->reloadResources();
}

LayerObjectSceneEditor::LayerObjectSceneEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Scene", parent)
{
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 150;

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, 4 * (20 + 2));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* cellWidthFlow = new FlowLayout(0, nullptr, (uint32_t)Margin::LEFT, 2, 0);
		cellWidthFlow->setSize(widgetWidth, 20);
		fillFlowLayout(cellWidthFlow);

		Label* cellWidthLabel = new Label(0, nullptr);
		fillLabel(cellWidthLabel, "Width");
		cellWidthFlow->addWidget(cellWidthLabel);

		cellWidthTextfield = new TextField(1, nullptr);
		fillTextField(cellWidthTextfield);
		cellWidthTextfield->setAcceptOnlyNumbers(true);
		cellWidthTextfield->setAllowDynamicBuffering(true);
		cellWidthTextfield->setFieldValue("%f", 0.0f);
		cellWidthTextfield->setSize(70, 20);
		cellWidthTextfield->setOnDeselectEvent(this, &LayerObjectSceneEditor::onCellWidthDeselect);
		cellWidthFlow->addWidget(cellWidthTextfield);
		boxLayout->addWidget(cellWidthFlow);
	}

	{
		FlowLayout* cellHeightFlow = new FlowLayout(1, nullptr, (uint32_t)Margin::LEFT, 2, 0);
		cellHeightFlow->setSize(widgetWidth, 20);
		fillFlowLayout(cellHeightFlow);

		Label* cellHeightLabel = new Label(0, nullptr);
		fillLabel(cellHeightLabel, "Height");
		cellHeightFlow->addWidget(cellHeightLabel);

		cellHeightTextfield = new TextField(1, nullptr);
		fillTextField(cellHeightTextfield);
		cellHeightTextfield->setAcceptOnlyNumbers(true);
		cellHeightTextfield->setAllowDynamicBuffering(true);
		cellHeightTextfield->setFieldValue("%f", 0.0f);
		cellHeightTextfield->setSize(70, 20);
		cellHeightTextfield->setOnDeselectEvent(this, &LayerObjectSceneEditor::onCellHeightDeselect);
		cellHeightFlow->addWidget(cellHeightTextfield);
		boxLayout->addWidget(cellHeightFlow);
	}
	
	{
		FlowLayout* cellSizeFlow = new FlowLayout(2, nullptr, (uint32_t)Margin::LEFT, 2, 0);
		cellSizeFlow->setSize(widgetWidth, 20);
		fillFlowLayout(cellSizeFlow);

		Label* cellSizeLabel = new Label(0, nullptr);
		fillLabel(cellSizeLabel, "Size");
		cellSizeFlow->addWidget(cellSizeLabel);

		cellSizeTextfield = new TextField(1, nullptr);
		fillTextField(cellSizeTextfield);
		cellSizeTextfield->setAcceptOnlyNumbers(true);
		cellSizeTextfield->setAllowDynamicBuffering(true);
		cellSizeTextfield->setFieldValue("%f", 0.0f);
		cellSizeTextfield->setSize(70, 20);
		cellSizeTextfield->setOnDeselectEvent(this, &LayerObjectSceneEditor::onCellSizeDeselect);
		cellSizeFlow->addWidget(cellSizeTextfield);
		boxLayout->addWidget(cellSizeFlow);
	}
	
	{
		FlowLayout* cellFlow = new FlowLayout(3, nullptr, (uint32_t)Margin::LEFT, 2, 0);
		cellFlow->setSize(150, 20);
		fillFlowLayout(cellFlow);

		cellRebuildButton = new Button(0, nullptr);
		fillButton(cellRebuildButton, "Rebuild");
		cellRebuildButton->setOnClickEvent(this, &LayerObjectSceneEditor::onRebuildClick);
		cellFlow->addWidget(cellRebuildButton);

		cellRefreshButton = new Button(1, nullptr);
		fillButton(cellRefreshButton, "Refresh");
		cellRefreshButton->setOnClickEvent(this, &LayerObjectSceneEditor::onRefreshClick);
		cellFlow->addWidget(cellRefreshButton);

		boxLayout->addWidget(cellFlow);
	}
	
	addWidgetToEditor(boxLayout);
}

void LayerObjectSceneEditor::reloadResources()
{
	LayerObjectManager* layerObjectManager = LayerObjectManager::getSingleton();

	Scene2d*  scene = layerObjectManager->getScene();
	HashGrid* hashGrid = (HashGrid*)(scene);

	int32_t cellWidth = hashGrid->getCellWidth();
	int32_t cellHeight = hashGrid->getCellHeight();
	jaFloat cellSize = hashGrid->getCellSize();

	cellWidthTextfield->setFieldValue("%d", cellWidth);
	cellHeightTextfield->setFieldValue("%d", cellHeight);
	cellSizeTextfield->setFieldValue("%f", cellSize);
}

void LayerObjectSceneEditor::onCellWidthDeselect(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	HashGrid* hashGrid = (HashGrid*)physicsManager->getScene();

	int32_t cellWidth;
	int32_t cellHeight;

	if (false == this->cellWidthTextfield->getAsInt32(&cellWidth))
	{
		this->cellWidthTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellWidth());
	}

	if (false == this->cellHeightTextfield->getAsInt32(&cellHeight))
	{
		this->cellHeightTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellHeight());
	}

	int32_t cellsNum = cellWidth * cellHeight;

	if (cellsNum > setup::gilgamesh::MAX_BUCKET_NUM)
	{
		this->cellWidthTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellWidth());
	}
}

void LayerObjectSceneEditor::onCellHeightDeselect(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	HashGrid* hashGrid = (HashGrid*)physicsManager->getScene();

	int32_t cellWidth;
	int32_t cellHeight;

	if (false == this->cellWidthTextfield->getAsInt32(&cellWidth))
	{
		this->cellWidthTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellWidth());
	}

	if (false == this->cellHeightTextfield->getAsInt32(&cellHeight))
	{
		this->cellHeightTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellHeight());
	}

	int32_t cellsNum = cellWidth * cellHeight;

	if (cellsNum > setup::gilgamesh::MAX_BUCKET_NUM)
	{
		this->cellWidthTextfield->setFieldValue("%d", (int32_t)hashGrid->getCellWidth());
	}
}

void LayerObjectSceneEditor::onCellSizeDeselect(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();

	HashGrid* hashGrid = (HashGrid*)physicsManager->getScene();

	float cellSize;

	if (false == this->cellSizeTextfield->getAsFloat(&cellSize))
	{
		this->cellSizeTextfield->setFieldValue("%f", hashGrid->getCellSize());
	}

	if (cellSize <= 0.0f)
	{
		this->cellSizeTextfield->setFieldValue("%f", hashGrid->getCellSize());
	}
}

void LayerObjectSceneEditor::onRefreshClick(Widget* sender, WidgetEvent action, void* data)
{
	reloadResources();
}

void LayerObjectSceneEditor::onRebuildClick(Widget* sender, WidgetEvent action, void* data)
{
	LayerObjectManager* layerObjectManager = LayerObjectManager::getSingleton();

	Scene2d* scene     = layerObjectManager->getScene();
	HashGrid* hashGrid = (HashGrid*)(scene);

	int16_t cellWidth;
	int16_t cellHeight;
	jaFloat cellSize;

	if (!cellWidthTextfield->getAsInt16(&cellWidth)) {
		cellWidth = hashGrid->getCellWidth();
	}

	if (!cellHeightTextfield->getAsInt16(&cellHeight)) {
		cellHeight = hashGrid->getCellHeight();
	}

	if (!cellSizeTextfield->getAsFloat(&cellSize)) {
		cellSize = hashGrid->getCellSize();
	}

	hashGrid->rebuild(cellWidth, cellHeight, cellSize);
}

LayerObjectColorEditor::LayerObjectColorEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Color", parent)
{
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 200;
	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, (20 + 2) * 5);
	fillBoxLayout(boxLayout);

	{
		FlowLayout* rFlow = new FlowLayout(0, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		rFlow->setSize(widgetWidth, 20);
		fillFlowLayout(rFlow);

		Label* rLabel = new Label(0, nullptr);
		fillLabel(rLabel, "R");

		redTextField = new TextField(1, nullptr);
		fillTextField(redTextField);
		redTextField->setAcceptOnlyNumbers(true);
		redTextField->setTextSize(10);
		redTextField->setAllowDynamicBuffering(false);
		redTextField->setFieldValue("%f", 1.0f);
		redTextField->setSize(50, 20);
		redTextField->setOnDeselectEvent(this, &LayerObjectColorEditor::onChangeColor);

		redScrollbar = new Scrollbar(2, nullptr);
		fillScrollbar(redScrollbar, 0.0f, 1.0f);
		redScrollbar->setHorizontal();
		redScrollbar->setSize(100, 20);
		redScrollbar->setValue(1.0f);
		redScrollbar->setOnChangeEvent(this, &LayerObjectColorEditor::onChangeColor);

		rFlow->addWidget(rLabel);
		rFlow->addWidget(redTextField);
		rFlow->addWidget(redScrollbar);
		boxLayout->addWidget(rFlow);
	}

	{
		FlowLayout* gFlow = new FlowLayout(1, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		gFlow->setSize(widgetWidth, 20);
		fillFlowLayout(gFlow);

		Label* gLabel = new Label(0, nullptr);
		fillLabel(gLabel, "G");

		greenTextField = new TextField(1, nullptr);
		fillTextField(greenTextField);
		greenTextField->setAcceptOnlyNumbers(true);
		greenTextField->setTextSize(10);
		greenTextField->setAllowDynamicBuffering(false);
		greenTextField->setFieldValue("%f", 1.0f);
		greenTextField->setSize(50, 20);
		greenTextField->setOnDeselectEvent(this, &LayerObjectColorEditor::onChangeColor);

		greenScrollbar = new Scrollbar(2, nullptr);
		fillScrollbar(greenScrollbar, 0.0f, 1.0f);
		greenScrollbar->setHorizontal();
		greenScrollbar->setSize(100, 20);
		greenScrollbar->setValue(1.0f);
		greenScrollbar->setOnChangeEvent(this, &LayerObjectColorEditor::onChangeColor);

		gFlow->addWidget(gLabel);
		gFlow->addWidget(greenTextField);
		gFlow->addWidget(greenScrollbar);
		boxLayout->addWidget(gFlow);
	}

	{
		FlowLayout* bFlow = new FlowLayout(2, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		bFlow->setSize(widgetWidth, 20);
		fillFlowLayout(bFlow);

		Label* bLabel = new Label(0, nullptr);
		fillLabel(bLabel, "B");

		blueTextField = new TextField(1, nullptr);
		fillTextField(blueTextField);
		blueTextField->setAcceptOnlyNumbers(true);
		blueTextField->setTextSize(10);
		blueTextField->setAllowDynamicBuffering(false);
		blueTextField->setFieldValue("%f", 1.0f);
		blueTextField->setSize(50, 20);
		blueTextField->setOnDeselectEvent(this, &LayerObjectColorEditor::onChangeColor);

		blueScrollbar = new Scrollbar(2, nullptr);
		fillScrollbar(blueScrollbar, 0.0f, 1.0f);
		blueScrollbar->setHorizontal();
		blueScrollbar->setSize(100, 20);
		blueScrollbar->setValue(1.0f);
		blueScrollbar->setOnChangeEvent(this, &LayerObjectColorEditor::onChangeColor);

		bFlow->addWidget(bLabel);
		bFlow->addWidget(blueTextField);
		bFlow->addWidget(blueScrollbar);
		boxLayout->addWidget(bFlow);
	}
	
	{
		FlowLayout* aFlow = new FlowLayout(3, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		aFlow->setSize(widgetWidth, 20);
		fillFlowLayout(aFlow);

		Label* aLabel = new Label(0, nullptr);
		fillLabel(aLabel, "A");

		alphaTextField = new TextField(1, nullptr);
		fillTextField(alphaTextField);
		alphaTextField->setAcceptOnlyNumbers(true);
		alphaTextField->setTextSize(10);
		alphaTextField->setAllowDynamicBuffering(false);
		alphaTextField->setFieldValue("%f", 1.0f);
		alphaTextField->setSize(50, 20);
		alphaTextField->setOnDeselectEvent(this, &LayerObjectColorEditor::onChangeColor);

		alphaScrollbar = new Scrollbar(2, nullptr);
		fillScrollbar(alphaScrollbar, 0.0f, 1.0f);
		alphaScrollbar->setHorizontal();
		alphaScrollbar->setSize(100, 20);
		alphaScrollbar->setValue(1.0f);
		alphaScrollbar->setOnChangeEvent(this, &LayerObjectColorEditor::onChangeColor);

		aFlow->addWidget(aLabel);
		aFlow->addWidget(alphaTextField);
		aFlow->addWidget(alphaScrollbar);
		boxLayout->addWidget(aFlow);
	}
	
	{
		this->colorFlow = new FlowLayout(4, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		colorFlow->setSize(widgetWidth, 20);
		fillFlowLayout(colorFlow);

		colorFlow->setColor(255, 255, 255, 255);
		boxLayout->addWidget(colorFlow);
	}

	this->red   = 1.0f;
	this->green = 1.0f;
	this->blue  = 1.0f;
	this->alpha = 1.0f;

	this->singleton = this;

	addWidgetToEditor(boxLayout);
}

void LayerObjectColorEditor::onChangeColor(Widget* sender, WidgetEvent action, void* data)
{
	if ((sender == redScrollbar) || (sender == greenScrollbar) || (sender == blueScrollbar))
	{
		if (sender == redScrollbar) {
			updateColorValueScrollbar(redScrollbar, redTextField, red);
		}

		if (sender == greenScrollbar) {
			updateColorValueScrollbar(greenScrollbar, greenTextField, green);
		}

		if (sender == blueScrollbar) {
			updateColorValueScrollbar(blueScrollbar, blueTextField, blue);
		}

		if (LayerObjectEditor::getSingleton()->getCurrentEditor() == PolygonEditor::getSingleton())
		{
			PolygonEditor::getSingleton()->applyFilterToSelection(&LayerObjectColorEditor::changeColorFilter);
		}
	}
	else
	{
		bool alphaChanged = false;
		bool colorChanged = false;

		if (sender == alphaScrollbar)
		{
			updateColorValueScrollbar(alphaScrollbar, alphaTextField, alpha);
			alphaChanged = true;
		}

		if (sender == redTextField) {
			updateColorValueTextfield(redScrollbar, redTextField, red);
			colorChanged = true;
		}

		if (sender == greenTextField) {
			updateColorValueTextfield(greenScrollbar, greenTextField, green);
			colorChanged = true;
		}

		if (sender == blueTextField) {
			updateColorValueTextfield(blueScrollbar, blueTextField, blue);
			colorChanged = true;
		}

		if (sender == alphaTextField) {
			updateColorValueTextfield(alphaScrollbar, alphaTextField, alpha);
			alphaChanged = true;
		}

		if (LayerObjectEditor::getSingleton()->getCurrentEditor() == PolygonEditor::getSingleton())
		{
			if (colorChanged)
				PolygonEditor::getSingleton()->applyFilterToSelection(&LayerObjectColorEditor::changeColorFilter);
			if (alphaChanged)
				PolygonEditor::getSingleton()->applyFilterToSelection(&LayerObjectColorEditor::changeAlphaFilter);
		}
	}

	const uint8_t red8   = (uint8_t)((double)red * 255.0);
	const uint8_t green8 = (uint8_t)((double)green * 255.0);
	const uint8_t blue8  = (uint8_t)((double)blue * 255.0);
	const uint8_t alpha8 = (uint8_t)((double)alpha * 255.0);

	colorFlow->setColor(red8, green8, blue8, alpha8);
}

void LayerObjectColorEditor::updateColorValueScrollbar(Scrollbar* scrollbar, TextField* textField, float& colorValue)
{
	double newValue = scrollbar->getValue();
	colorValue = (float)(newValue);
	textField->clearText();
	textField->setFieldValue("%f", colorValue);
}

void LayerObjectColorEditor::updateColorValueTextfield(Scrollbar* scrollbar, TextField* textField, float& colorValue)
{
	textField->getAsFloat(&colorValue);
	scrollbar->setValue(colorValue);
}

void LayerObjectColorEditor::changeColorFilter(ja::Polygon* polygon)
{
	//sprite->r = singleton->red;
	//sprite->g = singleton->green;
	//sprite->b = singleton->blue;
}

void LayerObjectColorEditor::changeColorFilter(Color4f* vertexColor)
{
	vertexColor->r = singleton->red;
	vertexColor->g = singleton->green;
	vertexColor->b = singleton->blue;
}

void LayerObjectColorEditor::changeAlphaFilter(ja::Polygon* polygon)
{
	jaFloat alphaFloat = singleton->alpha;
	//sprite->setAlpha(alphaFloat);
}

void LayerObjectColorEditor::changeAlphaFilter(Color4f* vertexColor)
{
	vertexColor->a = singleton->alpha;
}

void LayerObjectColorEditor::updateWidget(void* data)
{
	if (nullptr == data)
		return;

	if (LayerObjectEditor::getSingleton()->getCurrentEditor() == PolygonEditor::getSingleton())
	{
		Color4f* vertex = (Color4f*)(data);

		red   = vertex->r;
		green = vertex->g;
		blue  = vertex->b;
		alpha = vertex->a;
	}
	
	double dR = (double)(red);
	double dG = (double)(green);
	double dB = (double)(blue);
	double dA = (double)(alpha);
	
	redScrollbar->setValue(dR);
	greenScrollbar->setValue(dG);
	blueScrollbar->setValue(dB);
	alphaScrollbar->setValue(dA);
	
	updateColorValueScrollbar(redScrollbar,   redTextField,   red);
	updateColorValueScrollbar(greenScrollbar, greenTextField, green);
	updateColorValueScrollbar(blueScrollbar,  blueTextField,  blue);
	updateColorValueScrollbar(alphaScrollbar, alphaTextField, alpha);

	const uint8_t red8   = (uint8_t)((double)red * 255.0);
	const uint8_t green8 = (uint8_t)((double)green * 255.0);
	const uint8_t blue8  = (uint8_t)((double)blue * 255.0);
	const uint8_t alpha8 = (uint8_t)((double)alpha * 255.0);

	colorFlow->setColor(red8, green8, blue8, alpha8);
}

LayerObjectShapeEditor::LayerObjectShapeEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Layer Object", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	
	const int32_t widgetWidth = 280;
	
	boxLayout->setSize(widgetWidth, 6 * (20 + 2));
	fillBoxLayout(boxLayout);

	DisplayManager* display = DisplayManager::getSingleton();
	infoPosX = (float)(display->getResolutionWidth(0.3f));
	infoPosY = (float)(display->getResolutionHeight(0.3f));

	{
		FlowLayout* posLayout = new FlowLayout(0, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		posLayout->setSize(widgetWidth, 20);
		fillFlowLayout(posLayout);

		Label* posLabel = new Label(0, nullptr);
		fillLabel(posLabel, "Transformation");

		posLayout->addWidget(posLabel);
		boxLayout->addWidget(posLayout);
	}

	{
		FlowLayout* postLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		postLayout->setSize(widgetWidth, 20);
		fillFlowLayout(postLayout);
		Label* xLabel = new Label(0, nullptr);
		fillLabel(xLabel, "X");
		xTextfield = new TextField(1, nullptr);
		fillTextField(xTextfield);
		xTextfield->setAcceptOnlyNumbers(true);
		xTextfield->setTextSize(12, 20);
		xTextfield->setOnDeselectEvent(this, &LayerObjectShapeEditor::onXDeselect);
		Label* yLabel = new Label(2, nullptr);
		fillLabel(yLabel, "Y");
		yTextfield = new TextField(3, nullptr);
		fillTextField(yTextfield);
		yTextfield->setAcceptOnlyNumbers(true);
		yTextfield->setTextSize(12, 20);
		yTextfield->setOnDeselectEvent(this, &LayerObjectShapeEditor::onYDeselect);
		postLayout->addWidget(xLabel);
		postLayout->addWidget(xTextfield);
		postLayout->addWidget(yLabel);
		postLayout->addWidget(yTextfield);
		boxLayout->addWidget(postLayout);

		this->xTextfield->setActive(false);
		this->yTextfield->setActive(false);

		this->x = 0.0f;
		this->y = 0.0f;
	}

	{
		FlowLayout* rotLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		rotLayout->setSize(widgetWidth, 20);
		fillFlowLayout(rotLayout);
		Label* angleLabel = new Label(0, nullptr);
		fillLabel(angleLabel, "Angle");
		rotLayout->addWidget(angleLabel);
		this->angleTextfield = new TextField(1, nullptr);
		fillTextField(this->angleTextfield);
		this->angleTextfield->setAcceptOnlyNumbers(true);
		this->angleTextfield->setTextSize(12, 20);
		this->angleTextfield->setOnDeselectEvent(this, &LayerObjectShapeEditor::onAngleDeselect);
		rotLayout->addWidget(this->angleTextfield);
		boxLayout->addWidget(rotLayout);

		this->angleTextfield->setActive(false);

		this->angleDeg = 0.0f;
	}

	{
		FlowLayout* scaleLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		scaleLayout->setSize(widgetWidth, 20);
		fillFlowLayout(scaleLayout);
		
		Label* scaleXLabel = new Label(0, nullptr);
		fillLabel(scaleXLabel, "Scale X");
		scaleLayout->addWidget(scaleXLabel);
		
		this->scaleXTextfield = new TextField(1, nullptr);
		fillTextField(this->scaleXTextfield);
		this->scaleXTextfield->setAcceptOnlyNumbers(true);
		this->scaleXTextfield->setTextSize(12, 20);
		this->scaleXTextfield->setOnDeselectEvent(this, &LayerObjectShapeEditor::onScaleXDeselect);
		scaleLayout->addWidget(this->scaleXTextfield);


		Label* scaleYLabel = new Label(2, nullptr);
		fillLabel(scaleYLabel, "Y");
		scaleLayout->addWidget(scaleYLabel);

		this->scaleYTextfield = new TextField(3, nullptr);
		fillTextField(this->scaleYTextfield);
		this->scaleYTextfield->setAcceptOnlyNumbers(true);
		this->scaleYTextfield->setTextSize(12, 20);
		this->scaleYTextfield->setOnDeselectEvent(this, &LayerObjectShapeEditor::onScaleYDeselect);
		scaleLayout->addWidget(this->scaleYTextfield);

		boxLayout->addWidget(scaleLayout);

		this->scaleXTextfield->setActive(false);
		this->scaleYTextfield->setActive(false);

		this->scaleX = 1.0f;
		this->scaleY = 1.0f;
	}

	{
		FlowLayout* layerLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		layerLayout->setSize(widgetWidth, 20);
		fillFlowLayout(layerLayout);

		Label* layerLabel = new Label(0, nullptr);
		fillLabel(layerLabel, "Layer");
		layerLayout->addWidget(layerLabel);

		this->layerName = new Label(1, nullptr);
		fillLabel(this->layerName, "");
		this->layerName->setSize(100, 20);
		layerLayout->addWidget(this->layerName);

		boxLayout->addWidget(layerLayout);
	}

	{
		FlowLayout* depthLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		depthLayout->setSize(widgetWidth, 20);
		fillFlowLayout(depthLayout);

		Label* depthLabel = new Label(0, nullptr);
		fillLabel(depthLabel, "Depth");
		depthLayout->addWidget(depthLabel);

		this->depthTextfield = new TextField(1, nullptr);
		fillTextField(this->depthTextfield);
		this->depthTextfield->setAcceptOnlyNumbers(true);
		this->depthTextfield->setTextSize(12, 20);
		this->depthTextfield->setOnDeselectEvent(this, &LayerObjectShapeEditor::onDepthDeselect);
		depthLayout->addWidget(this->depthTextfield);

		boxLayout->addWidget(depthLayout);

		this->depthTextfield->setActive(false);

		this->depth = 0;
	}


	addWidgetToEditor(boxLayout);
}

void LayerObjectShapeEditor::updateWidget(void* data)
{
	ja::LayerObject* layerObject = (ja::LayerObject*)data;

	if (nullptr == layerObject)
	{
		this->xTextfield->setText("");
		this->yTextfield->setText("");
		this->angleTextfield->setText("");
		this->scaleXTextfield->setText("");;
		this->scaleYTextfield->setText("");
		this->depthTextfield->setText("");
		this->layerName->setText("");

		this->xTextfield->setActive(false);
		this->yTextfield->setActive(false);
		this->angleTextfield->setActive(false);
		this->scaleXTextfield->setActive(false);
		this->scaleYTextfield->setActive(false);
		this->depthTextfield->setActive(false);

		return;
	}

	this->xTextfield->setActive(true);
	this->yTextfield->setActive(true);
	this->angleTextfield->setActive(true);
	this->scaleXTextfield->setActive(true);
	this->scaleYTextfield->setActive(true);
	this->depthTextfield->setActive(true);

	const jaVector2 position = layerObject->getPosition();
	const jaVector2 scale    = layerObject->getScale();
	const jaFloat   angle    = layerObject->getAngleDeg();
	const uint16_t  depth    = layerObject->depth;
	const Layer*    layer    = EffectManager::getSingleton()->getLayer(layerObject->layerId);

	this->xTextfield->setFieldValue("%f", position.x);
	this->yTextfield->setFieldValue("%f", position.y);
	this->angleTextfield->setFieldValue("%f", angle);
	this->scaleXTextfield->setFieldValue("%f", scale.x);
	this->scaleYTextfield->setFieldValue("%f", scale.y);
	this->depthTextfield->setFieldValue("%hu", depth);
	this->layerName->setText(layer->getName());
}

void LayerObjectShapeEditor::onXDeselect(Widget* sender, WidgetEvent action, void* data)
{
	if (true == xTextfield->getAsFloat(&x))
		LayerObjectEditor::getSingleton()->applyFilterToSelection(xFilter);
	else
		xTextfield->setFieldValue("%f", x);
}

void LayerObjectShapeEditor::onYDeselect(Widget* sender, WidgetEvent action, void* data)
{
	if (true == yTextfield->getAsFloat(&y))
		LayerObjectEditor::getSingleton()->applyFilterToSelection(yFilter);
	else
		yTextfield->setFieldValue("%f", y);
}

void LayerObjectShapeEditor::onAngleDeselect(Widget* sender, WidgetEvent action, void* data)
{
	if (true == angleTextfield->getAsFloat(&angleDeg))
		LayerObjectEditor::getSingleton()->applyFilterToSelection(angleFilter);
	else
		angleTextfield->setFieldValue("%f", angleDeg);
}

void LayerObjectShapeEditor::onScaleXDeselect(Widget* sender, WidgetEvent action, void* data)
{
	if (true == scaleXTextfield->getAsFloat(&scaleX))
		LayerObjectEditor::getSingleton()->applyFilterToSelection(scaleXFilter);
	else
		scaleXTextfield->setFieldValue("%f", scaleX);
}

void LayerObjectShapeEditor::onScaleYDeselect(Widget* sender, WidgetEvent action, void* data)
{
	if (true == scaleYTextfield->getAsFloat(&scaleY))
		LayerObjectEditor::getSingleton()->applyFilterToSelection(scaleYFilter);
	else
		scaleYTextfield->setFieldValue("%f", scaleY);
}

void LayerObjectShapeEditor::onDepthDeselect(Widget* sender, WidgetEvent action, void* data)
{
	if (true == depthTextfield->getAsUInt16(&depth))
		LayerObjectEditor::getSingleton()->applyFilterToSelection(depthFilter);
	else
		depthTextfield->setFieldValue("%hu", depth);
}

void LayerObjectShapeEditor::xFilter(LayerObject* layerObject)
{
	jaVector2 position = layerObject->getPosition();
	layerObject->setPosition(jaVector2(singleton->x, position.y));
}

void LayerObjectShapeEditor::yFilter(LayerObject* layerObject)
{
	const jaVector2 position = layerObject->getPosition();
	layerObject->setPosition(jaVector2(position.x, singleton->y));
}

void LayerObjectShapeEditor::angleFilter(LayerObject* layerObject)
{
	layerObject->setAngleDeg(singleton->angleDeg);
}

void LayerObjectShapeEditor::scaleXFilter(LayerObject* layerObject)
{
	jaVector2 scale = layerObject->getScale();

	scale.x = singleton->scaleX;
	layerObject->setScale(scale);
}

void LayerObjectShapeEditor::scaleYFilter(LayerObject* layerObject)
{
	jaVector2 scale = layerObject->getScale();

	scale.y = singleton->scaleY;
	layerObject->setScale(scale);
}

void LayerObjectShapeEditor::depthFilter(LayerObject* layerObject)
{
	layerObject->depth = singleton->depth;
}

PolygonEditor::PolygonEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Polygon", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);

	const int32_t widgetWidth      = 260;
	const int32_t scrollPaneHeight = 100;

	boxLayout->setSize(widgetWidth, (4 * (20 + 2)) + scrollPaneHeight );
	fillBoxLayout(boxLayout);

	DisplayManager* display = DisplayManager::getSingleton();
	infoPosX = (float)(display->getResolutionWidth(0.3f));
	infoPosY = (float)(display->getResolutionHeight(0.3f));

	{
		FlowLayout* shapeLayout = new FlowLayout(5, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		shapeLayout->setSize(widgetWidth, 20);
		fillFlowLayout(shapeLayout);
		Label* shapesLabel = new Label(0, nullptr);
		fillLabel(shapesLabel, "Shapes");
		shapeLayout->addWidget(shapesLabel);

		boxLayout->addWidget(shapeLayout);
	}

	{
		FlowLayout* shapesLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		shapesLayout->setSize(widgetWidth, 20);
		fillFlowLayout(shapesLayout);

		boxButton = new Button((uint32_t)TR_LAYEROBJECTEDITOR_ID::BOX_EDITOR, nullptr);
		fillButton(boxButton, "Box");
		boxButton->setOnClickEvent(LayerObjectComponentMenu::getSingleton(), &LayerObjectComponentMenu::onMenuClick);

		circleButton = new Button((uint32_t)TR_LAYEROBJECTEDITOR_ID::CIRCLE_EDITOR, nullptr);
		fillButton(circleButton, "Circle");
		circleButton->setOnClickEvent(LayerObjectComponentMenu::getSingleton(), &LayerObjectComponentMenu::onMenuClick);

		shapesLayout->addWidget(boxButton);
		shapesLayout->addWidget(circleButton);

		boxLayout->addWidget(shapesLayout);
	}

	{
		FlowLayout* editLayout = new FlowLayout(6, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		editLayout->setSize(widgetWidth, 20);
		fillFlowLayout(editLayout);

		this->editModeButton = new Button(2, nullptr);
		fillButton(this->editModeButton, "Edit mode");
		editModeButton->setOnClickEvent(this, &PolygonEditor::onEditModeClick);
		this->editModeButton->setActive(false);
		editLayout->addWidget(this->editModeButton);

		this->makeAsPhysicBodyButton = new Button(3, nullptr);
		fillButton(this->makeAsPhysicBodyButton, "Make as physic body");
		makeAsPhysicBodyButton->setOnClickEvent(this, &PolygonEditor::onMakeAsPhysicBodyClick);
		makeAsPhysicBodyButton->setActive(false);
		editLayout->addWidget(this->makeAsPhysicBodyButton);

		this->editModeButton->setActive(false);

		boxLayout->addWidget(editLayout);
	}

	{
		FlowLayout* selectTrianglesLayout = new FlowLayout(6, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		selectTrianglesLayout->setSize(widgetWidth, 20);
		fillFlowLayout(selectTrianglesLayout);

		Label* selectTrianglesLabel = new Label(0, nullptr);
		fillLabel(selectTrianglesLabel, "Select Triangles");
		selectTrianglesLayout->addWidget(selectTrianglesLabel);

		this->selectTrianglesCheckbox = new Checkbox(1, nullptr);
		fillCheckbox(this->selectTrianglesCheckbox);
		this->selectTrianglesCheckbox->setCheck(false);
		selectTrianglesLayout->addWidget(this->selectTrianglesCheckbox);

		boxLayout->addWidget(selectTrianglesLayout);
	}

	{
		this->scrollPane = new ScrollPane(4, nullptr);
		this->scrollPane->setSize(widgetWidth, scrollPaneHeight);
		fillScrollPane(this->scrollPane);

		this->scrollPane->setColor(0, 255, 0, 128);

		boxLayout->addWidget(this->scrollPane);
	}

	this->drawBoxSelection = false;

	addWidgetToEditor(boxLayout);
}

void PolygonEditor::drawGizmo()
{
	DisplayManager::setColorUb(255, 255, 255);
	DisplayManager::setPointSize(7);
	DisplayManager::drawPoint(gizmo.x, gizmo.y);

	DisplayManager::setColorUb(255, 0, 0);
	DisplayManager::setPointSize(5);
	DisplayManager::drawPoint(gizmo.x, gizmo.y);
}

void PolygonEditor::calculateSelectionPoint()
{
	int32_t    verticesNum = verticesPositions.getSize();
	jaVector2* pVertices = verticesPositions.getFirst();
	bool*      pSelected = selectedVertices.getFirst();

	jaVector2 midpoint(0.0f, 0.0f);

	uint32_t selectedNum = 0;

	for (int32_t i = 0; i < verticesNum; ++i)
	{
		if (pSelected[i])
		{
			midpoint += pVertices[i];
			++selectedNum;
		}
	}

	if (selectedNum > 0)
	{
		float fSelectedNum = (float)selectedNum;
		this->gizmo = jaVector2(midpoint.x / fSelectedNum, midpoint.y / fSelectedNum);
	}

}

void PolygonEditor::setEditorMode(PolygonEditorMode editorMode)
{
	this->editorMode = editorMode;

	switch (this->editorMode)
	{
	case PolygonEditorMode::SELECT_MODE:
		LayerObjectEditor::getSingleton()->hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=Select");
		LayerObjectEditor::getSingleton()->hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "ESC - Accept\nLMB - Select vertex\nF - Face\nD - Delete vertex\nCtrl+D - Delete face\nShift + D - Duplicate\nE - Extrude\nC - Create vertex\nG - Translate\nR - Rotate\nS - Scale");
		break;
	case PolygonEditorMode::TRANSLATE_MODE:
		LayerObjectEditor::getSingleton()->hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=Translate");
		LayerObjectEditor::getSingleton()->hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "LMB - Accepth\nESC - Cancel");
		break;
	case PolygonEditorMode::ROTATE_MODE:
		LayerObjectEditor::getSingleton()->hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=Rotate");
		LayerObjectEditor::getSingleton()->hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "LMB - Accepth\nESC - Cancel");
		break;
	case PolygonEditorMode::SCALE_MODE:
		LayerObjectEditor::getSingleton()->hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=Scale");
		LayerObjectEditor::getSingleton()->hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "LMB - Accepth\nESC - Cancel\nx - scale x\ny - scale y");
		break;
	}
}

void PolygonEditor::savePositionVerticesState()
{
	uint32_t verticesNum = verticesPositions.getSize();

	savedPositionVertices.resize(verticesNum * sizeof(jaVector2));
	savedPositionVertices.setSize(verticesNum);

	memcpy(savedPositionVertices.getFirst(), verticesPositions.getFirst(), verticesNum * sizeof(jaVector2));
}

void PolygonEditor::saveColorVerticesState()
{
	uint32_t verticesNum = verticesColors.getSize();

	savedColorVertices.resize(verticesNum * sizeof(Color4f));
	savedColorVertices.setSize(verticesNum);

	memcpy(savedColorVertices.getFirst(), verticesColors.getFirst(), verticesNum * sizeof(Color4f));
}

void PolygonEditor::assignPositionVerticesState(jaVector2* const pVertices, uint32_t verticesNum)
{
	verticesTransformedToLocal.resize(verticesNum * sizeof(jaVector2));
	verticesTransformedToLocal.setSize(verticesNum);

	jaVector2* pLocalVertices = verticesTransformedToLocal.getFirst();

	jaTransform2 transform = this->editedPolygon->getHandle()->transform;
	jaVector2    scale     = this->editedPolygon->getScale();

	jaMatrix2 invRotMat = jaVectormath2::inverse(transform.rotationMatrix);
	jaVector2 invScale(1.0f / scale.x, 1.0f / scale.y);
	jaVector2 invTranslate(-transform.position.x, -transform.position.y);

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		pLocalVertices[i] = invScale * (invRotMat * (pVertices[i] + invTranslate));
	}

	this->editedPolygon->drawElements.setVerticesPositions(pLocalVertices, verticesNum);
	this->editedPolygon->updateBoundingVolume();
}

void PolygonEditor::assignColorVerticesState(Color4f* const pColors, uint32_t verticesNum)
{
	this->editedPolygon->drawElements.setVerticesColors(pColors, verticesNum);
}

void PolygonEditor::translateMode()
{
	jaVector2* pPositionVertices = verticesPositions.getFirst();
	jaVector2* pPositionSaved    = savedPositionVertices.getFirst();
	uint32_t   verticesNum       = verticesPositions.getSize();

	bool* pSelected = selectedVertices.getFirst();

	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		if (pSelected[i])
		{
			jaVector2 diff      = pPositionSaved[i] - gizmo;
			pPositionVertices[i]= drawPoint + diff;
		}
	}

	assignPositionVerticesState(pPositionVertices, verticesNum);
}

void PolygonEditor::scaleMode()
{
	jaVector2* pVertices   = verticesPositions.getFirst();
	jaVector2* pSaved      = savedPositionVertices.getFirst();
	uint32_t   verticesNum = verticesPositions.getSize();

	bool* pSelected = selectedVertices.getFirst();

	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

	const jaVector2 startVec   = transformationStart - gizmo;
	const jaVector2 currentVec = drawPoint - gizmo;

	const jaFloat startVecLength = jaVectormath2::length(startVec);
	const jaFloat currentVecLength = jaVectormath2::length(currentVec);

	jaVector2 scale(currentVecLength / startVecLength, currentVecLength / startVecLength);

	if (InputManager::isKeyPressed(JA_x))
	{
		this->scaleAxis = ScaleMode::SCALE_X;
		ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
		ruler->setPosition(gizmo.x, gizmo.y);
		ruler->setAngle(0);
		ruler->setActive(true);
		ruler->setAlign(RulerAlign::NO_ALIGN);
	}

	if (InputManager::isKeyPressed(JA_y))
	{
		this->scaleAxis = ScaleMode::SCALE_Y;
		ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
		ruler->setPosition(gizmo.x, gizmo.y);
		ruler->setAngle(90);
		ruler->setActive(true);
		ruler->setAlign(RulerAlign::NO_ALIGN);
	}

	switch (this->scaleAxis)
	{
	case ScaleMode::SCALE_X:
		scale.y = 1.0f;
		break;
	case ScaleMode::SCALE_Y:
		scale.x = 1.0f;
		break;
	}

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		if (pSelected[i])
		{
			jaVector2 diff = pSaved[i] - gizmo;
			const jaVector2 scaleDiff = scale * diff;
			pVertices[i] = gizmo + scaleDiff;
		}
	}

	assignPositionVerticesState(pVertices, verticesNum);
}

void PolygonEditor::rotateMode()
{
	jaVector2* pVertices   = verticesPositions.getFirst();
	jaVector2* pSaved      = savedPositionVertices.getFirst();
	uint32_t   verticesNum = verticesPositions.getSize();

	bool* pSelected = selectedVertices.getFirst();

	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

	const jaVector2 startVec = transformationStart - gizmo;
	const jaVector2 currentVec = drawPoint - gizmo;

	const jaFloat   angle = jaVectormath2::getAngle(currentVec, startVec);
	const jaMatrix2 rotMat(angle);

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		if (pSelected[i])
		{
			jaVector2       diff    = pSaved[i] - gizmo;
			const jaVector2 rotDiff = rotMat * diff;
			pVertices[i] = gizmo + rotDiff;
		}
	}

	assignPositionVerticesState(pVertices, verticesNum);
}

void PolygonEditor::extrude()
{

}

void PolygonEditor::duplicate()
{
	uint32_t verticesNum = verticesPositions.getSize();
	verticesPositions.resize(2 * verticesNum * sizeof(jaVector2));
	verticesColors.resize(2 * verticesNum * sizeof(Color4f));
	selectedVertices.resize(2 * verticesNum * sizeof(bool));

	bool*      pSelected          = selectedVertices.getFirst();
	jaVector2* pVerticesPositions = verticesPositions.getFirst();
	Color4f*   pVerticesColors    = verticesColors.getFirst();

	uint32_t unselectedNum = 0;
	this->savedTriangleIndices.clear();

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		if (pSelected[i])
		{
			verticesPositions.push(pVerticesPositions[i]);
			verticesColors.push(pVerticesColors[i]);
			selectedVertices.push(true);

			this->savedTriangleIndices.push(unselectedNum);
		}
		else
		{
			this->savedTriangleIndices.push(unselectedNum++);
		}
	}

	uint32_t indicesNum = this->triangleIndices.getSize();
	triangleIndices.resize(2 * indicesNum * sizeof(uint32_t));

	uint32_t* pIndices       = triangleIndices.getFirst();
	uint32_t* pUnselectedNum = this->savedTriangleIndices.getFirst();

	for (uint32_t i = 0; i < indicesNum; i += 3)
	{
		if (pSelected[pIndices[i]] && pSelected[pIndices[i + 1]] && pSelected[pIndices[i + 2]])
		{
			uint32_t i0 = pIndices[i];
			uint32_t i1 = pIndices[i + 1];
			uint32_t i2 = pIndices[i + 2];

			i0 += verticesNum - pUnselectedNum[i0];
			i1 += verticesNum - pUnselectedNum[i1];
			i2 += verticesNum - pUnselectedNum[i2];

			triangleIndices.push(i0);
			triangleIndices.push(i1);
			triangleIndices.push(i2);
		}
	}

	assignPositionVerticesState(verticesPositions.getFirst(), verticesPositions.getSize());
	assignColorVerticesState(verticesColors.getFirst(), verticesColors.getSize());
	this->editedPolygon->drawElements.setTriangleIndices(triangleIndices.getFirst(), triangleIndices.getSize());

	memset(selectedVertices.getFirst(), 0, verticesNum * sizeof(bool)); // unselect previous indices

	savePositionVerticesState();
	saveColorVerticesState();
	setEditorMode(PolygonEditorMode::TRANSLATE_MODE);
}

bool PolygonEditor::isTriangleSelectionSelected()
{
	return this->selectTrianglesCheckbox->isChecked();
}

void PolygonEditor::setActiveMode(bool state)
{
	if (state)
	{
		editModeButton->setColor(0, 255, 0);

	}
	else
	{
		editModeButton->setColor(255, 0, 0);
	}

	setEditorMode(PolygonEditorMode::SELECT_MODE);
}

void PolygonEditor::drawOriginPoint()
{
	DisplayManager::setColorUb(255, 255, 255);
	DisplayManager::setPointSize(7);
	DisplayManager::drawPoint(originPoint.x, originPoint.y);

	DisplayManager::setColorUb(0, 0, 0);
	DisplayManager::setPointSize(5);
	DisplayManager::drawPoint(originPoint.x, originPoint.y);
}

void PolygonEditor::addToSelection(gil::AABB2d& selectionAABB)
{
	jaVector2 size = selectionAABB.max - selectionAABB.min;
	jaTransform2 transform;
	transform.position = selectionAABB.min + (size / 2.0f);
	transform.rotationMatrix.setRadians(0.0f);
	gil::Box2d boxSelection(size.x, size.y);

	uint32_t   verticesNum = verticesPositions.getSize();
	jaVector2* pVertices   = verticesPositions.getFirst();
	bool*      pSelected   = selectedVertices.getFirst();

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		bool state = boxSelection.isPointInShape(transform, pVertices[i]);

		pSelected[i] = pSelected[i] || state;
	}
}

void PolygonEditor::removeFromSelection(gil::AABB2d& selectionAABB)
{
	jaVector2 size = selectionAABB.max - selectionAABB.min;
	jaTransform2 transform;
	transform.position = selectionAABB.min + (size / 2.0f);
	transform.rotationMatrix.setRadians(0.0f);
	gil::Box2d boxSelection(size.x, size.y);

	uint32_t   verticesNum = verticesPositions.getSize();
	jaVector2* pVertices   = verticesPositions.getFirst();
	bool*      pSelected   = selectedVertices.getFirst();

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		bool state = boxSelection.isPointInShape(transform, pVertices[i]);

		if (state)
			pSelected[i] = false;
	}
}

void PolygonEditor::makeSelection(gil::AABB2d& selectionAABB)
{
	jaVector2 size = selectionAABB.max - selectionAABB.min;
	jaTransform2 transform;
	transform.position = selectionAABB.min + (size / 2.0f);
	transform.rotationMatrix.setRadians(0.0f);
	gil::Box2d boxSelection(size.x, size.y);

	uint32_t   verticesNum    = verticesPositions.getSize();
	jaVector2* pVertices      = verticesPositions.getFirst();
	Color4f*   pColorVertices = verticesColors.getFirst();
	bool*      pSelected      = selectedVertices.getFirst();

	Color4f* lastSelected = nullptr;

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		bool state = boxSelection.isPointInShape(transform, pVertices[i]);

		pSelected[i] = state;
		lastSelected = (state) ? &pColorVertices[i] : lastSelected;
	}

	if (nullptr != lastSelected)
	{
		LayerObjectColorEditor::getSingleton()->updateWidget(lastSelected);
	}
}

void PolygonEditor::createVertex()
{
	const jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

	LayerObjectColorEditor* colorEditor = LayerObjectColorEditor::getSingleton();

	const Color4f color(colorEditor->red, colorEditor->green, colorEditor->blue, colorEditor->alpha);

	jaVector2 newPositionVertex(drawPoint.x, drawPoint.y);

	this->verticesPositions.push(newPositionVertex);
	this->verticesColors.push(color);
	this->selectedVertices.push(true);

	assignPositionVerticesState(this->verticesPositions.getFirst(), this->verticesPositions.getSize());
	assignColorVerticesState(this->verticesColors.getFirst(), this->verticesColors.getSize());
}

void PolygonEditor::deleteVertex()
{
	uint32_t verticesNum = this->verticesPositions.getSize();
	bool*    pSelected   = this->selectedVertices.getFirst();

	recalculatedTriangleIndices.resize(verticesNum * sizeof(uint32_t));
	recalculatedTriangleIndices.setSize(verticesNum);
	uint32_t* pRecalculatedTriangleIndices = recalculatedTriangleIndices.getFirst();

	{
		//Recalculate triangle indices
		uint32_t subOffset = 0;
		for (uint32_t i = 0; i < verticesNum; ++i)
		{
			if (pSelected[i])
				++subOffset;

			pRecalculatedTriangleIndices[i] = (i - subOffset);
		}

		//Remove faces
		{
			uint32_t indicesNum = this->triangleIndices.getSize();

			savedTriangleIndices.resize(indicesNum * sizeof(uint32_t));
			savedTriangleIndices.setSize(indicesNum);
			memcpy(savedTriangleIndices.getFirst(), triangleIndices.getFirst(), indicesNum * sizeof(uint32_t));

			uint32_t* pSavedIndices = savedTriangleIndices.getFirst();
			triangleIndices.clear();

			for (uint32_t i = 0; i < indicesNum; i += 3)
			{
				if (!(pSelected[pSavedIndices[i]] || pSelected[pSavedIndices[i + 1]] || pSelected[pSavedIndices[i + 2]]))
				{
					const uint32_t recalculated0 = pRecalculatedTriangleIndices[pSavedIndices[i]];
					const uint32_t recalculated1 = pRecalculatedTriangleIndices[pSavedIndices[i + 1]];
					const uint32_t recalculated2 = pRecalculatedTriangleIndices[pSavedIndices[i + 2]];

					triangleIndices.push(recalculated0);
					triangleIndices.push(recalculated1);
					triangleIndices.push(recalculated2);
				}
			}

			this->editedPolygon->drawElements.setTriangleIndices(this->triangleIndices.getFirst(), this->triangleIndices.getSize());
		}
	}

	savedPositionVertices.resize(verticesNum * sizeof(jaVector2));
	savedPositionVertices.setSize(verticesNum);
	memcpy(savedPositionVertices.getFirst(), verticesPositions.getFirst(), verticesNum * sizeof(jaVector2));
	savedColorVertices.resize(verticesNum * sizeof(Color4f));
	savedColorVertices.setSize(verticesNum);
	memcpy(savedColorVertices.getFirst(), verticesColors.getFirst(), verticesNum * sizeof(Color4f));

	jaVector2* pSavedPositionVertices = savedPositionVertices.getFirst();
	Color4f*   pSavedColorVertices    = savedColorVertices.getFirst();

	verticesPositions.clear();
	verticesColors.clear();

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		if (!pSelected[i])
		{
			verticesPositions.push(pSavedPositionVertices[i]);
			verticesColors.push(pSavedColorVertices[i]);
		}
	}

	verticesNum = verticesPositions.getSize();
	memset(pSelected, 0, verticesNum *  sizeof(bool));
	selectedVertices.setSize(verticesNum);

	assignPositionVerticesState(this->verticesPositions.getFirst(), verticesNum);
	assignColorVerticesState(this->verticesColors.getFirst(), verticesNum);
}

void PolygonEditor::deleteFace()
{
	uint32_t indicesNum = this->triangleIndices.getSize();

	bool* pSelected = this->selectedVertices.getFirst();

	savedTriangleIndices.resize(indicesNum * sizeof(uint32_t));
	savedTriangleIndices.setSize(indicesNum);
	memcpy(savedTriangleIndices.getFirst(), triangleIndices.getFirst(), indicesNum * sizeof(uint32_t));

	uint32_t* pSavedIndices = savedTriangleIndices.getFirst();
	triangleIndices.clear();

	for (uint32_t i = 0; i < indicesNum; i += 3)
	{
		if (!(pSelected[pSavedIndices[i]] || pSelected[pSavedIndices[i + 1]] || pSelected[pSavedIndices[i + 2]]))
		{
			triangleIndices.push(pSavedIndices[i]);
			triangleIndices.push(pSavedIndices[i + 1]);
			triangleIndices.push(pSavedIndices[i + 2]);
		}
	}

	this->editedPolygon->drawElements.setTriangleIndices(this->triangleIndices.getFirst(), this->triangleIndices.getSize());
}

struct smallerXSort : public std::binary_function<jaVector2*, jaVector2*, bool>
{
	bool operator()(const jaVector2* left, const jaVector2* right) const
	{
		return (left->x < right->x);
	}
};

float cross(const jaVector2* O, const jaVector2* A, const jaVector2* B)
{
	return (A->x - O->x) * (B->y - O->y) - (A->y - O->y) * (B->x - O->x);
}

void buildConvexHull(StackAllocator<jaVector2*>& points, StackAllocator<jaVector2*>& convexHull)
{
	int32_t n = points.getSize();
	int32_t k = 0;

	//vector<Point> H(2 * n);
	convexHull.resize(2 * n * sizeof(jaVector2*));

	// Sort points lexicographically
	jaVector2** pPoints = points.getFirst();
	jaVector2** pConvexHull = convexHull.getFirst();

	std::sort(pPoints, pPoints + n, smallerXSort());

	// Build lower hull
	for (int32_t i = 0; i < n; ++i) {
		while (k >= 2 && cross(pConvexHull[k - 2], pConvexHull[k - 1], pPoints[i]) <= 0) k--;
		pConvexHull[k++] = pPoints[i];
	}

	// Build upper hull
	for (int32_t i = n - 2, t = k + 1; i >= 0; i--) {
		while (k >= t && cross(pConvexHull[k - 2], pConvexHull[k - 1], pPoints[i]) <= 0)
			k--;
		pConvexHull[k++] = pPoints[i];
	}

	//H.resize(k - 1);
	convexHull.setSize(k - 1);
}

void reorderCounterClockWise(uint32_t& index0, uint32_t& index1, uint32_t& index2, jaVector2* vertices)
{
	jaVector2 edge01 = vertices[index1] - vertices[index0];
	jaVector2 edge02 = vertices[index2] - vertices[index0];

	const jaVector2 edge01N = jaVectormath2::normalize(edge01);
	const jaVector2 edge02N = jaVectormath2::normalize(edge02);

	const float det = jaVectormath2::det(edge01N, edge02N);

	if (det < 0.0f) // is clockWise
	{
		uint32_t temp = index1;
		index1 = index2;
		index2 = temp;
	}
}

void PolygonEditor::updateWidget(void* data)
{
	LayerObject* layerObject = (LayerObject*)data;
	ja::Polygon* polygon     = nullptr;

	if(nullptr != layerObject)
		polygon = (layerObject->getObjectType() == (uint8_t)LayerObjectType::POLYGON) ? (ja::Polygon*)layerObject : nullptr;

	refreshDataFieldViewer(polygon);

	if (nullptr == polygon)
	{
		this->makeAsPhysicBodyButton->setActive(false);
		this->editModeButton->setActive(false);
		return;
	}

	this->makeAsPhysicBodyButton->setActive(true);
	this->editModeButton->setActive(true);
}

void PolygonEditor::refreshDataFieldViewer(ja::Polygon* polygon)
{
	this->scrollPane->clearWidgets();
	this->dataNotifiers.clear();

	if (nullptr == polygon)
		return;

	const ShaderObjectData*   pShaderObjectData = polygon->getShaderObjectData();
	const ShaderStructureDef* pShaderObjectDef  = pShaderObjectData->getShaderObjectDef();

	const StackAllocator<DataFieldInfo>* dataFieldsStack = pShaderObjectDef->getDataFields();
	const uint32_t                       dataFieldsNum   = dataFieldsStack->getSize();
	DataFieldInfo*                       pDataField      = dataFieldsStack->getFirst();

	uint32_t fieldsNum = 0;

	for (uint32_t i = 0; i < dataFieldsNum; ++i)
	{
		fieldsNum += pDataField[i].getElementsNum();
	}

	const uint32_t gridLayoutHeight = fieldsNum  * (20 + 2);
	const int32_t  scrollPaneWidth = this->scrollPane->getWidth();

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(scrollPaneWidth, gridLayoutHeight);

	boxLayout->setPosition(0, 0);
	boxLayout->setColor(0, 0, 255, 150);

	this->dataNotifiers.resize(sizeof(DataNotifier) * fieldsNum);

	DataNotifier* pDataNotifier = this->dataNotifiers.getFirst();
	void*         pData         = pShaderObjectData->getData();

	for (uint32_t i = 0; i < dataFieldsNum; ++i)
	{
		const uint32_t elementsNum = pDataField[i].getElementsNum();

		for (uint32_t z = 0; z < elementsNum; ++z)
		{
			DataNotifier* fieldNotifier = new (&pDataNotifier[i + z]) DataNotifier(pData, z, &pDataField[i]);

			fieldNotifier->setDataChangeCallback(this, &PolygonEditor::onDataChange);
			Widget* widget = fieldNotifier->createDataWidget();

			boxLayout->addWidget(widget);
		}
	}

	this->scrollPane->addWidget(boxLayout);
	//this->scrollPane->setScrollY(INT_MAX);
	this->scrollPane->setVerticalScrollbarPolicy(ScrollbarPolicy::JA_SCROLLBAR_ALWAYS);
}

void PolygonEditor::onDataChange(ja::DataNotifier* dataNotifier)
{

}

void PolygonEditor::makeFace()
{
	jaVector2* pVertices   = this->verticesPositions.getFirst();
	uint32_t   verticesNum = this->verticesPositions.getSize();
	bool*      pSelected   = this->selectedVertices.getFirst();

	this->faceVertices.resize(verticesNum * sizeof(jaVector2*));
	jaVector2** pFace = this->faceVertices.getFirst();

	uint32_t selectedNum = 0;

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		if (pSelected[i])
		{
			pFace[selectedNum++] = &pVertices[i];
		}
	}

	if (selectedNum < 3)
	{
		Toolset::getSingleton()->windowHud->addInfoC((uint16_t)LayerObjectEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "Not enough points selected to make a face");
		return;
	}

	this->faceVertices.setSize(selectedNum);

	StackAllocator<jaVector2*> convexHull;

	buildConvexHull(this->faceVertices, convexHull);

	uint32_t allocatedIndicesSize = this->triangleIndices.getSize() * sizeof(uint32_t);
	uint32_t trianglesNum = convexHull.getSize() - 2;
	triangleIndices.resize(allocatedIndicesSize + (3 * trianglesNum * sizeof(uint32_t)));

	jaVector2** pConvexHull = convexHull.getFirst();

	uint32_t commonIndex = ((uintptr_t)pConvexHull[0] - (uintptr_t)&pVertices[0]) / sizeof(jaVector2);
	uint32_t nextIndex   = ((uintptr_t)pConvexHull[1] - (uintptr_t)&pVertices[0]) / sizeof(jaVector2);

	for (uint32_t i = 0; i < trianglesNum; ++i)
	{
		uint32_t newIndex = ((uintptr_t)pConvexHull[i + 2] - (uintptr_t)&pVertices[0]) / sizeof(jaVector2);

		uint32_t index0 = commonIndex;
		uint32_t index1 = nextIndex;
		uint32_t index2 = newIndex;

		reorderCounterClockWise(index0, index1, index2, pVertices);

		triangleIndices.push(index0);
		triangleIndices.push(index1);
		triangleIndices.push(index2);

		nextIndex = newIndex;
	}

	this->editedPolygon->drawElements.setTriangleIndices(triangleIndices.getFirst(), triangleIndices.getSize());
}

void PolygonEditor::onEditModeClick(Widget* sender, WidgetEvent action, void* data)
{
	CommonWidget* pCommonWidget = LayerObjectEditor::getSingleton()->getCurrentEditor();
	if (nullptr != pCommonWidget)
	{
		if (pCommonWidget == this)
			return;
	}

	LayerObjectManager* layerObjectManager = LayerObjectManager::getSingleton();
	std::list<LayerObject*>& selectedLayerObjects = LayerObjectEditor::getSingleton()->selectedLayerObjects;

	if (selectedLayerObjects.size() == 0)
	{
		Toolset::getSingleton()->windowHud->addInfoC((uint16_t)LayerObjectEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "Select layer object first");
		return;
	}

	if (selectedLayerObjects.size() > 1)
	{
		Toolset::getSingleton()->windowHud->addInfoC((uint16_t)LayerObjectEditorInfo::COMPONENT_EDIT_INFO, infoPosX, infoPosY, 224, 12, 17, 5.0f, "You can edit only one layer object at once");
		return;
	}

	editedLayerObject = *selectedLayerObjects.begin();

	switch (editedLayerObject->getObjectType())
	{
	case (uint8_t)LayerObjectType::POLYGON:
	{
		this->editedPolygon = (ja::Polygon*)editedLayerObject;
		getPolygonInfo(editedPolygon);
		LayerObjectEditor::getSingleton()->setEditorMode(this);
	}
		break;
	}

	
}

void PolygonEditor::onMakeAsPhysicBodyClick(Widget* sender, WidgetEvent action, void* data)
{
	PhysicsManager* physicsManager = PhysicsManager::getSingleton();
	
	std::list<LayerObject*>& selectedLayerObjects = LayerObjectEditor::getSingleton()->selectedLayerObjects;
	std::list<LayerObject*>::iterator itLayerObject = selectedLayerObjects.begin();

	PhysicObjectDef2d* physicObjectDef = &physicsManager->physicObjectDef;
	physicObjectDef->setShapeType((int8_t)ShapeType::TriangleMesh);
	
	TriangleMesh2d* triangleMesh = (TriangleMesh2d*)physicObjectDef->getShape();
	StackAllocator<jaVector2> verticesCopyStack;

	for (itLayerObject; itLayerObject != selectedLayerObjects.end(); ++itLayerObject)
	{
		if ((uint8_t)LayerObjectType::POLYGON != (*itLayerObject)->getObjectType())
			continue;

		ja::Polygon* polygon = (ja::Polygon*)(*itLayerObject);

		physicObjectDef->transform.position       = polygon->getPosition();
		physicObjectDef->transform.rotationMatrix = polygon->getRotationMatrix();
		
		const jaVector2* pPositionVertices = polygon->drawElements.getVerticesPositions();
		const uint32_t*  pTriangleIndices  = polygon->drawElements.getTriangleIndices();
		jaVector2*       pVertices         = nullptr;

		uint32_t triangleIndicesNum = polygon->drawElements.getTriangleIndicesNum();
		uint32_t trianglesNum       = triangleIndicesNum / 3;
		uint32_t verticesNum        = polygon->drawElements.getVerticesNum();

		{
			//Copy vertices
			verticesCopyStack.resize(verticesNum * sizeof(jaVector2));
			verticesCopyStack.setSize(verticesNum);
			pVertices = verticesCopyStack.getFirst();
			
			for (uint32_t i = 0; i < verticesNum; ++i)
			{
				pVertices[i] = pPositionVertices[i];
			}
		}

		//triangleMesh->setTriangleMeshWithoutSettingCentroid(pVertices, verticesNum, pTriangleIndices, trianglesNum, nullptr);
		assert(false);
		//triangleMesh->setTriangleMesh(pVertices, verticesNum, pTriangleIndices, trianglesNum, nullptr);
		physicsManager->createBodyDef();
	}
}

void PolygonEditor::getPolygonInfo(ja::Polygon* polygon)
{
	jaTransform2 transform = polygon->getHandle()->transform;
	jaVector2    scale = polygon->getScale();

	uint32_t         verticesNum       = polygon->drawElements.getVerticesNum();
	const jaVector2* pItVertexPosition = polygon->drawElements.getVerticesPositions();
	const Color4f*   pItVertexColor    = polygon->drawElements.getVerticesColors();
	uint32_t         indicesNum        = polygon->drawElements.getTriangleIndicesNum();
	const uint32_t*  pItIndices        = polygon->drawElements.getTriangleIndices();

	this->verticesPositions.resize(verticesNum * sizeof(jaVector2));
	this->verticesColors.resize(verticesNum * sizeof(Color4f));
	this->selectedVertices.resize(verticesNum * sizeof(bool));
	this->verticesPositions.setSize(verticesNum);
	this->verticesColors.setSize(verticesNum);
	this->selectedVertices.setSize(verticesNum);
	this->triangleIndices.resize(indicesNum * sizeof(uint32_t));
	this->triangleIndices.setSize(indicesNum);

	memset(this->selectedVertices.getFirst(), 0, verticesNum * sizeof(bool));
	memcpy(this->triangleIndices.getFirst(), pItIndices, indicesNum * sizeof(uint32_t));

	jaVector2* pVertexPosition = this->verticesPositions.getFirst();
	Color4f*   pVertexColor    = this->verticesColors.getFirst();

	for (uint32_t i = 0; i < verticesNum; ++i)
	{
		pVertexColor[i]    = pItVertexColor[i];
		pVertexPosition[i] = transform.position + (transform.rotationMatrix * (scale * pItVertexPosition[i]));
	}

	gizmo = transform.position;
	originPoint = gizmo;
}

void PolygonEditor::handleInput()
{
	if (PolygonEditorMode::SELECT_MODE == this->editorMode)
	{
		if (InputManager::isKeyPressed(JA_ESCAPE))
		{
			this->editedLayerObject->updateTransform();
			LayerObjectEditor::getSingleton()->setEditorMode(LayerObjectEditorMode::SELECT_MODE);
		}
	}

	if (!UIManager::isMouseOverGui())
	{
		if (PolygonEditorMode::SELECT_MODE == this->editorMode)
		{
			if (!InputManager::isKeyModPress(JA_KMOD_LALT))
			{
				if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
				{
					this->drawBoxSelection = true;
					this->selectionStartPoint = Toolset::getSingleton()->getDrawPoint();
				}

				if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT) && (this->drawBoxSelection))
				{
					this->drawBoxSelection = false;

					gil::AABB2d selectionAABB = gil::AABB2d::make(selectionStartPoint, Toolset::getSingleton()->getDrawPoint());

					if (InputManager::isKeyModPress(JA_KMOD_LSHIFT) || InputManager::isKeyModPress(JA_KMOD_LCTRL))
					{
						if (InputManager::isKeyModPress(JA_KMOD_LSHIFT))
							addToSelection(selectionAABB);
						else
							removeFromSelection(selectionAABB);
					}
					else
					{
						makeSelection(selectionAABB);
					}

					calculateSelectionPoint();
				}
			}

			if (InputManager::isKeyModPress(JA_KMOD_LALT))
			{
				if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
					this->gizmo = Toolset::getSingleton()->getDrawPoint();
			}

			if (InputManager::isKeyPressed(JA_f))
			{
				makeFace();
			}

			if (InputManager::isKeyPressed(JA_g))
			{
				savePositionVerticesState();
				saveColorVerticesState();
				setEditorMode(PolygonEditorMode::TRANSLATE_MODE);
			}

			if (InputManager::isKeyPressed(JA_r))
			{
				savePositionVerticesState();
				saveColorVerticesState();
				transformationStart = Toolset::getSingleton()->getDrawPoint();
				setEditorMode(PolygonEditorMode::ROTATE_MODE);
			}

			if (InputManager::isKeyPressed(JA_s))
			{
				savePositionVerticesState();
				saveColorVerticesState();
				transformationStart = Toolset::getSingleton()->getDrawPoint();
				setEditorMode(PolygonEditorMode::SCALE_MODE);
				this->scaleAxis = ScaleMode::SCALE_XY;
			}

			if (InputManager::isKeyPressed(JA_c))
			{
				createVertex();
			}

			if (InputManager::isKeyPressed(JA_d))
			{
				if (InputManager::isKeyModPress(JA_KMOD_LCTRL))
					deleteFace();
				else
					deleteVertex();

				if (InputManager::isKeyModPress(JA_KMOD_LSHIFT))
					duplicate();
			}
		}
	}

	if (editorMode == PolygonEditorMode::TRANSLATE_MODE)
	{
		if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT)) {
			setEditorMode(PolygonEditorMode::SELECT_MODE);
			calculateSelectionPoint();
		}
	}

	if (editorMode == PolygonEditorMode::ROTATE_MODE)
	{
		if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT)) {
			setEditorMode(PolygonEditorMode::SELECT_MODE);
		}
	}

	if (editorMode == PolygonEditorMode::SCALE_MODE)
	{
		if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT)) {
			setEditorMode(PolygonEditorMode::SELECT_MODE);
		}
	}

	if (editorMode != PolygonEditorMode::SELECT_MODE)
	{
		if (InputManager::getSingleton()->isKeyPressed(JA_ESCAPE))
		{
			assignPositionVerticesState(this->savedPositionVertices.getFirst(), this->verticesPositions.getSize());
			assignColorVerticesState(this->savedColorVertices.getFirst(), this->verticesColors.getSize());

			memcpy(this->verticesPositions.getFirst(), this->savedPositionVertices.getFirst(), this->verticesPositions.getSize() * sizeof(jaVector2));
			memcpy(this->verticesColors.getFirst(), this->savedColorVertices.getFirst(), this->verticesColors.getSize() * sizeof(Color4f));

			setEditorMode(PolygonEditorMode::SELECT_MODE);
		}
	}

	switch (editorMode)
	{
	case PolygonEditorMode::TRANSLATE_MODE:
		translateMode();
		break;
	case PolygonEditorMode::SCALE_MODE:
		scaleMode();
		break;
	case PolygonEditorMode::ROTATE_MODE:
		rotateMode();
		break;
	}

}

void PolygonEditor::render()
{
	if (this->editedPolygon == nullptr)
		return;

	const jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

	{
		jaVector2* pVertices  = this->verticesPositions.getFirst();
		uint32_t*  pIndices   = this->triangleIndices.getFirst();
		uint32_t   indicesNum = this->triangleIndices.getSize();

		DisplayManager::setColorUb(235, 1, 101, 255);

		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glEnableClientState(GL_VERTEX_ARRAY);
		//glEnableClientState(GL_COLOR_ARRAY);

		glVertexPointer(2, GL_FLOAT, sizeof(jaVector2), pVertices);
		//glColorPointer(4, GL_FLOAT, sizeof(ColorVertex), ((uint8_t*)pVertices + sizeof(jaVector2)));

		glDrawElements(GL_TRIANGLES, indicesNum, GL_UNSIGNED_INT, pIndices);

		glDisableClientState(GL_VERTEX_ARRAY);
		//glDisableClientState(GL_COLOR_ARRAY);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	{
		jaVector2*     pVertex     = verticesPositions.getFirst();
		Color4f*       pColors     = verticesColors.getFirst();
		bool*          pSelected   = selectedVertices.getFirst();
		const uint32_t verticesNum = verticesPositions.getSize();

		DisplayManager::setColorF(0.0f, 0.0f, 0.0f);
		DisplayManager::setPointSize(7);
		for (uint32_t i = 0; i < verticesNum; ++i)
		{
			if (pSelected[i])
				DisplayManager::setColorF(1.0f, 1.0f, 1.0f);
			else
				DisplayManager::setColorF(0.0f, 0.0f, 0.0f);

			DisplayManager::drawPoint(pVertex[i].x, pVertex[i].y);
		}

		DisplayManager::setPointSize(5);
		for (uint32_t i = 0; i < verticesNum; ++i)
		{
			DisplayManager::setColorF(pColors[i].r, pColors[i].g, pColors[i].b);
			DisplayManager::drawPoint(pVertex[i].x, pVertex[i].y);
		}
	}

	if (drawBoxSelection)
	{
		DisplayManager::setColorUb(255, 0, 128, 255);
		DisplayManager::setLineWidth(3.0f);
		gil::AABB2d boxSelection = gil::AABB2d::make(selectionStartPoint, Toolset::getSingleton()->getDrawPoint());
		gil::Debug::drawAABB2d(boxSelection);

		DisplayManager::setColorUb(255, 255, 255);
		DisplayManager::setPointSize(7);
		DisplayManager::drawPoint(selectionStartPoint.x, selectionStartPoint.y);

		DisplayManager::setColorUb(255, 0, 0);
		DisplayManager::setPointSize(5);
		DisplayManager::drawPoint(selectionStartPoint.x, selectionStartPoint.y);
	}

	if ((this->editorMode == PolygonEditorMode::ROTATE_MODE) || (this->editorMode == PolygonEditorMode::SCALE_MODE))
	{
		DisplayManager::setLineWidth(2.0f);
		DisplayManager::setColorF(1.0f, 1.0f, 1.0f);
		DisplayManager::drawLine(drawPoint.x, drawPoint.y, gizmo.x, gizmo.y);
	}

	drawGizmo();
	drawOriginPoint();
}

MorphEditor::MorphEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Morph", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);

	const int32_t widgetWidth = 350;

	boxLayout->setSize(widgetWidth, (5 * (20 + 2)) + (2 * (60 + 2)));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* nameLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		nameLayout->setSize(widgetWidth, 20);
		fillFlowLayout(nameLayout);

		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Name");
		nameLayout->addWidget(nameLabel);

		this->newMorphNameTextfield = new TextField(1, nullptr);
		fillTextField(this->newMorphNameTextfield);
		this->newMorphNameTextfield->setTextSize(setup::TAG_NAME_LENGTH, 20);
		nameLayout->addWidget(this->newMorphNameTextfield);

		this->createMorphButton = new Button(2, nullptr);
		fillButton(this->createMorphButton, "Create");
		this->createMorphButton->setOnClickEvent(this, &MorphEditor::onCreateMorphClick);
		nameLayout->addWidget(this->createMorphButton);

		this->morphsNumLabel = new Label(3, nullptr);
		fillLabel(this->morphsNumLabel, "Morphs:");
		nameLayout->addWidget(this->morphsNumLabel);

		boxLayout->addWidget(nameLayout);
	}

	{
		this->morphGridViewer = new GridViewer(3, 2, 3, nullptr);
		this->morphGridViewer->setSize(widgetWidth, 60);
		fillGridViewer(this->morphGridViewer);
		this->morphGridViewer->setOnDrawEvent(this, &MorphEditor::onDrawMorph);
		this->morphGridViewer->setOnClickEvent(this, &MorphEditor::onPickMorph);
		this->morphGridViewer->setGridList(&morphs);

		boxLayout->addWidget(this->morphGridViewer);
	}

	{
		FlowLayout* morphLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		morphLayout->setSize(widgetWidth, 20);
		fillFlowLayout(morphLayout);

		Label* startLabel = new Label(0, nullptr);
		fillLabel(startLabel, "Start:");
		morphLayout->addWidget(startLabel);

		this->startMorphLabel = new Label(0, nullptr);
		fillLabel(this->startMorphLabel, "");
		startMorphLabel->setSize(80, 20);
		morphLayout->addWidget(this->startMorphLabel);

		Label* endLabel = new Label(0, nullptr);
		fillLabel(endLabel, "End:");
		morphLayout->addWidget(endLabel);

		this->endMorphLabel = new Label(0, nullptr);
		fillLabel(this->endMorphLabel, "");
		endMorphLabel->setSize(80, 20);
		morphLayout->addWidget(this->endMorphLabel);

		boxLayout->addWidget(morphLayout);
	}

	{
		FlowLayout* morphLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		morphLayout->setSize(widgetWidth, 20);
		fillFlowLayout(morphLayout);

		this->setStartMorphButton = new Button(3, nullptr);
		fillButton(this->setStartMorphButton, "Set start");
		this->setStartMorphButton->setOnClickEvent(this, &MorphEditor::onSetStartMorphClick);
		morphLayout->addWidget(this->setStartMorphButton);

		this->setEndMorphButton = new Button(4, nullptr);
		fillButton(this->setEndMorphButton, "Set end");
		this->setEndMorphButton->setOnClickEvent(this, &MorphEditor::onSetEndMorphClick);
		morphLayout->addWidget(this->setEndMorphButton);
		boxLayout->addWidget(morphLayout);
	}

	{
		FlowLayout* morphLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		morphLayout->setSize(widgetWidth, 20);
		fillFlowLayout(morphLayout);

		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Name");
		morphLayout->addWidget(nameLabel);

		this->morphNameTextfield = new TextField(1, nullptr);
		fillTextField(this->morphNameTextfield);
		this->morphNameTextfield->setTextSize(setup::TAG_NAME_LENGTH, 20);
		this->morphNameTextfield->setOnDeselectEvent(this, &MorphEditor::onNameDeselect);
		morphLayout->addWidget(this->morphNameTextfield);

		this->deleteMorphButton = new Button(2, nullptr);
		fillButton(this->deleteMorphButton, "Delete");
		this->deleteMorphButton->setOnClickEvent(this, &MorphEditor::onDeleteMorphClick);
		morphLayout->addWidget(this->deleteMorphButton);

		this->updateMorphButton = new Button(3, nullptr);
		fillButton(this->updateMorphButton, "Update");
		this->updateMorphButton->setOnClickEvent(this, &MorphEditor::onUpdateMorphClick);
		morphLayout->addWidget(this->updateMorphButton);


		this->morphNameTextfield->setActive(false);
		this->deleteMorphButton->setActive(false);

		boxLayout->addWidget(morphLayout);
	}

	{
		FlowLayout* morphFlow = new FlowLayout(3, this, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		morphFlow->setSize(widgetWidth, 20);
		fillFlowLayout(morphFlow);

		Label* morphLabel = new Label(0, nullptr);
		fillLabel(morphLabel, "Morph");

		morphTextField = new TextField(1, nullptr);
		fillTextField(morphTextField);
		morphTextField->setAcceptOnlyNumbers(true);
		morphTextField->setTextSize(10);
		morphTextField->setAllowDynamicBuffering(false);
		morphTextField->setFieldValue("%f", 1.0f);
		morphTextField->setSize(50, 20);
		morphTextField->setOnDeselectEvent(this, &MorphEditor::onMorphChange);

		morphScrollbar = new Scrollbar(2, nullptr);
		fillScrollbar(morphScrollbar, 0.0f, 1.0f);
		morphScrollbar->setHorizontal();
		morphScrollbar->setSize(100, 20);
		morphScrollbar->setValue(1.0f);
		morphScrollbar->setOnChangeEvent(this, &MorphEditor::onMorphChange);
	
		morphFlow->addWidget(morphLabel);
		morphFlow->addWidget(morphTextField);
		morphFlow->addWidget(morphScrollbar);

		boxLayout->addWidget(morphFlow);
	}

	{
		this->morphFeaturesGridViewer = new GridViewer(3, 2, 3, nullptr);
		this->morphFeaturesGridViewer->setSize(widgetWidth, 60);
		fillGridViewer(this->morphFeaturesGridViewer);
		this->morphFeaturesGridViewer->setOnDrawEvent(this, &MorphEditor::onDrawMorphFeature);
		this->morphFeaturesGridViewer->setOnClickEvent(this, &MorphEditor::onPickMorphFeature);
		this->morphFeaturesGridViewer->setGridList(&morphFeatures);

		boxLayout->addWidget(this->morphFeaturesGridViewer);
	}

	{
		morphLabel.setFont("default.ttf",10);
		morphLabel.setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
		morphLabel.setColor(255, 255, 255, 255);
		morphLabel.setText("");
	}

	setActiveWidgets(false);

	addWidgetToEditor(boxLayout);

	this->startMorph  = nullptr;
	this->endMorph    = nullptr;
	this->editedMorph = nullptr;
}

void MorphEditor::setActiveWidgets(bool state)
{
	this->createMorphButton->setActive(state);
	this->newMorphNameTextfield->setActive(state);

	this->morphTextField->setActive(state);
	this->morphScrollbar->setActive(state);
	this->setStartMorphButton->setActive(state);
	this->setEndMorphButton->setActive(state);

	this->morphNameTextfield->setActive(state);
	this->deleteMorphButton->setActive(state);
	this->updateMorphButton->setActive(state);
}

void MorphEditor::updateWidget(void* data)
{
	this->morphs.clear();

	ja::LayerObject* layerObject = (ja::LayerObject*)data;

	if (this->editedLayerObject != layerObject)
	{
		this->startMorph = nullptr;
		this->endMorph   = nullptr;
		this->startMorphLabel->setText("");
		this->endMorphLabel->setText("");

		setMorphData(nullptr);
	}

	this->editedLayerObject = layerObject;

	if (nullptr == layerObject)
	{
		this->morphsNumLabel->setText("Morphs:");
		setActiveWidgets(false);
		return;
	}

	MorphElements* morphElements = layerObject->getMorphElements();

	if (nullptr == morphElements)
	{
		this->morphsNumLabel->setText("Morphs:");
		setActiveWidgets(false);
		return;
	}

	setActiveWidgets(true);
	refreshMorphGridView();
}

void MorphEditor::setMorphData(LayerObjectMorph* layerObjectMorph)
{
	
	if (nullptr == layerObjectMorph)
	{
		//this->timeTextfield->setFieldValue("%f", 0.0f);

		this->morphTextField->setText("");
		this->morphTextField->setActive(false);

		this->morphScrollbar->setValue(0.0);
		this->morphScrollbar->setActive(false);

		this->deleteMorphButton->setActive(false);
		this->updateMorphButton->setActive(false);
		this->morphNameTextfield->setText("");
		this->morphNameTextfield->setActive(false);
		this->editedMorph = nullptr;
		this->morphFeatures.clear();

		return;
	}

	if (this->editedMorph != layerObjectMorph)
	{
		this->editedMorph = layerObjectMorph;

		this->morphNameTextfield->setText(layerObjectMorph->getName());
		this->morphNameTextfield->setActive(true);
		this->deleteMorphButton->setActive(true);
		this->updateMorphButton->setActive(true);

		this->morphFeatures.clear();
		uint8_t featuresNum = this->editedMorph->getFeaturesNum();
		for (uint8_t i = 1; i <= featuresNum; ++i)
		{
			this->morphFeatures.push_back((void*)i);
		}
	}

	uint32_t activeFeatures = this->editedMorph->getActiveFeatures();
	this->editedMorph->setActiveFeatures(0xffffffff);
	this->editedLayerObject->morph(this->editedMorph, this->editedMorph, 1.0f);
	this->editedMorph->setActiveFeatures(activeFeatures);
}

void MorphEditor::refreshMorphGridView()
{
	this->morphs.clear();

	if (nullptr == this->editedLayerObject)
		return;

	MorphElements* morphElements = this->editedLayerObject->getMorphElements();

	if (nullptr != morphElements)
	{
		uint16_t    morphsNum    = morphElements->getMorphsNum();
		MorphEntry* morphEntries = morphElements->getMorphEntries();

		this->morphsNumLabel->setTextPrintC("Morphs:%d", (int32_t)morphsNum);

		for (uint16_t i = 0; i < morphsNum; ++i)
		{
			this->morphs.push_back( morphEntries[i].layerObjectMorph );
		}
	}
	else
	{
		this->morphsNumLabel->setText("Morphs:");
	}

	morphGridViewer->setCellColor(255, 0, 0, 128);
}

void MorphEditor::onMorphChange(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	if (nullptr == this->startMorph)
		return;

	if (nullptr == this->endMorph)
		return;

	float morphValue = 0.0f;

	if (this->morphScrollbar == sender)
	{
		morphValue = (float)this->morphScrollbar->getValue();
		this->morphTextField->setFieldValue("%f", morphValue);
	}

	if (this->morphTextField == sender)
	{
		if (false == this->morphTextField->getAsFloat(&morphValue))
		{
			this->morphTextField->setFieldValue("%f", (float)this->morphScrollbar->getValue());
		}
		else
		{
			if ((morphValue < 0.0f) || (morphValue > 1.0f))
			{
				this->morphTextField->setFieldValue("%f", (float)this->morphScrollbar->getValue());
				morphValue = (float)this->morphScrollbar->getValue();
			}
		}	
	}

	this->editedLayerObject->morph(this->startMorph, this->endMorph, morphValue);
}

void MorphEditor::onCreateMorphClick(Widget* sender, WidgetEvent action, void* data)
{
	if (this->editedLayerObject == nullptr)
		return;

	const char* morphName = this->newMorphNameTextfield->getTextPtr();

	MorphElements* morphElements = this->editedLayerObject->getMorphElements();

	if (nullptr != morphElements)
	{
		LayerObjectMorph* morph = morphElements->getMorph(morphName);
		if (nullptr != morph)
		{
			Toolset::getSingleton()->infoHud->addInfoC(255, 0, 0, "Morph with name %s already exists", morphName);
			return;
		}
	}

	LayerObjectMorph* morphElement = morphElements->createMorph(morphName);
	morphElement->setMorph(this->editedLayerObject);

	this->morphs.push_back(morphElement);

	int32_t morphsNum = this->morphs.size();
	this->morphsNumLabel->setTextPrintC("Morphs:%d", (int32_t)morphsNum);
}

void MorphEditor::onSetStartMorphClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	if (nullptr == this->editedLayerObject)
		return;

	if (nullptr == this->editedMorph)
		return;

	this->startMorph = this->editedMorph;
	this->startMorphLabel->setText(this->startMorph->getName());
}

void MorphEditor::onSetEndMorphClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	if (nullptr == this->editedLayerObject)
		return;

	if (nullptr == this->editedMorph)
		return;

	this->endMorph = this->editedMorph;
	this->endMorphLabel->setText(this->endMorph->getName());
}

void MorphEditor::onDrawMorph(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	LayerObjectMorph* morph = (LayerObjectMorph*)data;

	int32_t x, y;

	int32_t halfWidth, halfHeight;
	float   fX, fY;

	sender->getPosition(x, y);
	fX = (float)(x);
	fY = (float)(y);

	halfWidth  = (sender->getWidth() / 2);
	halfHeight = (sender->getHeight() / 2);

	Button* button = (Button*)sender;

	if ((editedMorph == morph) && (nullptr != editedMorph))
	{
		button->setColor(0, 255, 0);
	}
	else
	{
		button->setColor(255, 0, 0);
	}

	morphLabel.x = x + halfWidth;
	morphLabel.y = y + halfHeight;
	morphLabel.setText(morph->getName());
	morphLabel.draw(0, 0);
}

void MorphEditor::onPickMorph(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	Button* button = (Button*)sender;
	LayerObjectMorph* layerObjectMorph = (LayerObjectMorph*)button->vUserData;

	setMorphData(layerObjectMorph);
}

void MorphEditor::onDrawMorphFeature(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	uint8_t featureId = (uint8_t)data;

	int32_t x, y;

	int32_t halfWidth, halfHeight;
	float   fX, fY;

	sender->getPosition(x, y);
	fX = (float)(x);
	fY = (float)(y);

	halfWidth = (sender->getWidth() / 2);
	halfHeight = (sender->getHeight() / 2);

	Button* button = (Button*)sender;

	if (this->editedMorph->isFeatureActive(featureId))
	{
		button->setColor(0, 255, 0);
	}
	else
	{
		button->setColor(255, 0, 0);
	}

	const char* morphFeatureName = this->editedMorph->getFeatureName(featureId);

	morphLabel.x = x + halfWidth;
	morphLabel.y = y + halfHeight;
	morphLabel.setText(morphFeatureName);
	morphLabel.draw(0, 0);
}

void MorphEditor::onPickMorphFeature(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	Button* button    = (Button*)sender;
	uint8_t featureId = (uint8_t)button->vUserData;

	bool isFeatureActive = this->editedMorph->isFeatureActive(featureId);
	this->editedMorph->setFeature(featureId, !isFeatureActive);
}

void MorphEditor::onDeleteMorphClick(Widget* sender, WidgetEvent action, void* data)
{
	MorphElements* morphElements = this->editedLayerObject->getMorphElements();

	if (nullptr != morphElements)
	{
		assert(morphElements->removeMorph(this->editedMorph));
	}

	if (this->startMorph == this->editedMorph)
	{
		this->startMorph = nullptr;
		this->startMorphLabel->setText("");
	}

	if (this->endMorph == this->editedMorph)
	{
		this->endMorph = nullptr;
		this->endMorphLabel->setText("");
	}

	refreshMorphGridView();

	setMorphData(nullptr);

	this->deleteMorphButton->setColor(255, 0, 0);
}

void MorphEditor::onUpdateMorphClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	if (nullptr == this->editedLayerObject)
		return;

	if (nullptr == this->editedMorph)
		return;

	this->editedMorph->setMorph(this->editedLayerObject);
}

void MorphEditor::onNameDeselect(Widget* sender, WidgetEvent action, void* data)
{
	const char* morphName = this->morphNameTextfield->getTextPtr();

	MorphElements* morphElements = this->editedLayerObject->getMorphElements();

	if (nullptr != morphElements)
	{
		LayerObjectMorph* morph = morphElements->getMorph(morphName);

		if (nullptr != morph)
		{
			this->morphNameTextfield->setText(this->editedMorph->getName());
			return;
		}
	}

	this->editedMorph->setName(morphName);
}

PolygonBoxEditor::PolygonBoxEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Box", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(350, 2 * (28 + 2));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* boxSizeLayout = new FlowLayout(5, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		boxSizeLayout->setSize(350, 28);
		fillFlowLayout(boxSizeLayout);
		Label* widthLabel = new Label(0, nullptr);
		fillLabel(widthLabel, "Width");
		this->widthTextfield = new TextField(1, nullptr);
		fillTextField(this->widthTextfield);
		this->widthTextfield->setAcceptOnlyNumbers(true);
		this->widthTextfield->setTextSize(12, 28);
		this->widthTextfield->setFieldValue("%f", 1.0f);

		Label* heightLabel = new Label(2, nullptr);
		fillLabel(heightLabel, "Height");
		this->heightTextfield = new TextField(3, nullptr);
		fillTextField(this->heightTextfield);
		this->heightTextfield->setAcceptOnlyNumbers(true);
		this->heightTextfield->setTextSize(12, 28);
		this->heightTextfield->setFieldValue("%f", 1.0f);

		this->createButton = new Button(3, nullptr);
		fillButton(this->createButton, "Create");
		this->createButton->setOnClickEvent(this, &PolygonBoxEditor::onCreateClick);

		boxSizeLayout->addWidget(widthLabel);
		boxSizeLayout->addWidget(this->widthTextfield);
		boxSizeLayout->addWidget(heightLabel);
		boxSizeLayout->addWidget(this->heightTextfield);
		boxSizeLayout->addWidget(this->createButton);

		FlowLayout* drawTypeLayout = new FlowLayout(6, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		drawTypeLayout->setSize(350, 28);
		fillFlowLayout(drawTypeLayout);

		this->facesButton = new RadioButton(0, nullptr);
		this->edgesButton = new RadioButton(1, nullptr);
		Label* facesLabel = new Label(2, nullptr);
		Label* edgesLabel = new Label(3, nullptr);
		Label* edgeWidthLabel    = new Label(4, nullptr);
		this->edgeWidthTextfield = new TextField(5, nullptr);

		fillLabel(facesLabel, "Faces");
		fillLabel(edgesLabel, "Edges");
		fillLabel(edgeWidthLabel, "Edge width");
		fillRadioButton(this->facesButton, "Faces");
		fillRadioButton(this->edgesButton, "Edges"); 
		fillTextField(this->edgeWidthTextfield);
		this->edgeWidthTextfield->setAcceptOnlyNumbers(true);
		this->edgeWidthTextfield->setTextSize(12, 28);
		this->edgeWidthTextfield->setFieldValue("%f", 0.1f);
		
		ButtonGroup* buttonGroup = new ButtonGroup(0, nullptr);
		buttonGroup->addButton(this->facesButton);
		buttonGroup->addButton(this->edgesButton);
		this->facesButton->setCheck(true);

		drawTypeLayout->addWidget(facesLabel);
		drawTypeLayout->addWidget(this->facesButton);
		drawTypeLayout->addWidget(edgesLabel);
		drawTypeLayout->addWidget(this->edgesButton);
		drawTypeLayout->addWidget(buttonGroup);
		drawTypeLayout->addWidget(edgeWidthLabel);
		drawTypeLayout->addWidget(this->edgeWidthTextfield);

		boxLayout->addWidget(boxSizeLayout);
		boxLayout->addWidget(drawTypeLayout);
	}

	addWidgetToEditor(boxLayout);
}

void PolygonBoxEditor::onCreateClick(Widget* sender, WidgetEvent action, void* data)
{
	LayerObjectEditor* layerObjectEditor = LayerObjectEditor::getSingleton();

	jaVector2 gizmo = LayerObjectEditor::getSingleton()->getGizmoPosition();

	LayerObjectDef* layerObjectDef = layerObjectEditor->getLayerObjectDef();

	layerObjectDef->transform.position = gizmo;
	layerObjectDef->transform.rotationMatrix.setDegrees(0.0f);
	layerObjectDef->layerObjectType = (uint8_t)LayerObjectType::POLYGON;

	float width  = 1.0f;
	float height = 1.0f;
	float edgeWidth = 0.1f;

	this->edgeWidthTextfield->getAsFloat(&edgeWidth);
	this->widthTextfield->getAsFloat(&width);
	this->heightTextfield->getAsFloat(&height);

	bool isFace = this->facesButton->isChecked();

	float halfWidth  = width * 0.5f;
	float halfHeight = height * 0.5f;

	LayerObjectColorEditor* colorEditor = LayerObjectColorEditor::getSingleton();

	Color4f color = Color4f(colorEditor->red, colorEditor->green, colorEditor->blue, colorEditor->alpha);

	if (isFace)
	{
		jaVector2 vertices[] = { jaVector2(-halfWidth, -halfHeight), jaVector2(halfWidth, -halfHeight), jaVector2(halfWidth, halfHeight), jaVector2(-halfWidth, halfHeight) };

		layerObjectDef->drawElements->setVerticesPositions(vertices, 4);
		layerObjectDef->drawElements->setVerticesColor(color, 4);

		const uint32_t    indices[] = { 0, 1, 2, 0, 2, 3 };		
		layerObjectDef->drawElements->setTriangleIndices(indices, 6);
	}
	else //edges
	{
		jaVector2 vertices[8];

		vertices[0] = jaVector2(-halfWidth, -halfHeight);
		vertices[1] = jaVector2(halfWidth, -halfHeight);
		vertices[2] = jaVector2(halfWidth, halfHeight);
		vertices[3] = jaVector2(-halfWidth, halfHeight);
		vertices[4] = jaVector2(-halfWidth + edgeWidth, -halfHeight + edgeWidth);
		vertices[5] = jaVector2(halfWidth - edgeWidth, -halfHeight + edgeWidth);
		vertices[6] = jaVector2(halfWidth - edgeWidth, halfHeight - edgeWidth);
		vertices[7] = jaVector2(-halfWidth + edgeWidth, halfHeight - edgeWidth);

		const uint32_t indices[] = { 0, 1, 4, 4, 1, 5, 1, 2, 5, 5, 2, 6, 2, 3, 6, 6, 3, 7, 3, 0, 7, 7, 0, 4 };

		layerObjectDef->drawElements->setVerticesPositions(vertices, 8);
		layerObjectDef->drawElements->setVerticesColor(color, 8);
		layerObjectDef->drawElements->setTriangleIndices(indices, 24);
	}
	
	if (layerObjectEditor->getEditorMode() == LayerObjectEditorMode::SELECT_MODE)
	{
		LayerObjectEditor::getSingleton()->createLayerObject();
	}
	else
	{

	}
}

PolygonCircleEditor::PolygonCircleEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Circle", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 350;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, 2 * (28 + 2));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* circleSizeLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		circleSizeLayout->setSize(widgetWidth, 28);
		fillFlowLayout(circleSizeLayout);
		
		Label* radiusLabel = new Label(0, nullptr);
		fillLabel(radiusLabel, "Radius");

		this->radiusTextfield = new TextField(1, nullptr);
		fillTextField(this->radiusTextfield);
		this->radiusTextfield->setAcceptOnlyNumbers(true);
		this->radiusTextfield->setTextSize(12, 28);
		this->radiusTextfield->setFieldValue("%f", 1.0f);

		Label* segmentsLabel = new Label(2, nullptr);
		fillLabel(segmentsLabel, "Segments");

		this->segmentsTextField = new TextField(3, nullptr);
		fillTextField(this->segmentsTextField);
		this->segmentsTextField->setAcceptOnlyNumbers(true);
		this->segmentsTextField->setTextSize(12, 28);
		this->segmentsTextField->setFieldValue("%d", 16);

		this->createButton = new Button(4, nullptr);
		fillButton(this->createButton, "Create");
		this->createButton->setOnClickEvent(this, &PolygonCircleEditor::onCreateClick);
		
		circleSizeLayout->addWidget(radiusLabel);
		circleSizeLayout->addWidget(this->radiusTextfield);
		circleSizeLayout->addWidget(segmentsLabel);
		circleSizeLayout->addWidget(this->segmentsTextField);
		circleSizeLayout->addWidget(this->createButton);

		FlowLayout* drawTypeLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		drawTypeLayout->setSize(widgetWidth, 28);
		fillFlowLayout(drawTypeLayout);

		this->facesButton = new RadioButton(0, nullptr);
		this->edgesButton = new RadioButton(1, nullptr);
		Label* facesLabel = new Label(2, nullptr);
		Label* edgesLabel = new Label(3, nullptr);
		Label* edgeWidthLabel = new Label(4, nullptr);
		this->edgeWidthTextfield = new TextField(5, nullptr);

		fillLabel(facesLabel, "Faces");
		fillLabel(edgesLabel, "Edges");
		fillRadioButton(this->facesButton, "Faces");
		fillRadioButton(this->edgesButton, "Edges");
		fillLabel(edgeWidthLabel, "Edge width");
		fillTextField(this->edgeWidthTextfield);
		this->edgeWidthTextfield->setAcceptOnlyNumbers(true);
		this->edgeWidthTextfield->setTextSize(12, 28);
		this->edgeWidthTextfield->setFieldValue("%f", 0.1f);

		ButtonGroup* buttonGroup = new ButtonGroup(0, nullptr);
		buttonGroup->addButton(this->facesButton);
		buttonGroup->addButton(this->edgesButton);

		this->facesButton->setCheck(true);

		drawTypeLayout->addWidget(facesLabel);
		drawTypeLayout->addWidget(this->facesButton);
		drawTypeLayout->addWidget(edgesLabel);
		drawTypeLayout->addWidget(this->edgesButton);
		drawTypeLayout->addWidget(buttonGroup);
		drawTypeLayout->addWidget(edgeWidthLabel);
		drawTypeLayout->addWidget(this->edgeWidthTextfield);

		boxLayout->addWidget(circleSizeLayout);
		boxLayout->addWidget(drawTypeLayout);
	}

	addWidgetToEditor(boxLayout);
}


void PolygonCircleEditor::onCreateClick(Widget* sender, WidgetEvent action, void* data)
{
	LayerObjectEditor* polygonEditor = LayerObjectEditor::getSingleton();

	jaVector2 gizmo = LayerObjectEditor::getSingleton()->getGizmoPosition();

	LayerObjectDef* layerObjectDef = polygonEditor->getLayerObjectDef();

	layerObjectDef->transform.position = gizmo;
	layerObjectDef->transform.rotationMatrix.setDegrees(0.0f);
	layerObjectDef->layerObjectType = (uint8_t)LayerObjectType::CAMERA;

	jaFloat  radius      = 1.0f;
	jaFloat  edgeWidth   = 0.1f;
	int32_t  segmentsNum = 16;
	bool     isFace      = this->facesButton->isChecked();

	this->radiusTextfield->getAsFloat(&radius);
	this->segmentsTextField->getAsInt32(&segmentsNum);

	segmentsNum = abs(segmentsNum);

	LayerObjectColorEditor* colorEditor = LayerObjectColorEditor::getSingleton();

	Color4f color = Color4f(colorEditor->red, colorEditor->green, colorEditor->blue, colorEditor->alpha);

	StackAllocator<jaVector2> vertices;
	StackAllocator<uint32_t>  indices;

	if (isFace)
	{
		vertices.resize((segmentsNum + 1) * sizeof(jaVector2));
		vertices.setSize((segmentsNum + 1));
		indices.resize((segmentsNum * 3) * sizeof(uint32_t));
		indices.setSize((segmentsNum * 3));

		jaVector2* pVertices = vertices.getFirst();
		uint32_t*  pIndices  = indices.getFirst();

		pVertices[0].x = 0.0f;
		pVertices[0].y = 0.0f;

		jaFloat   deltaAngle = (2.0f * math::PI) / (jaFloat)(segmentsNum);
		jaVector2 radiusLine(0.0f, radius);

		for (int32_t i = 0; i < segmentsNum; ++i)
		{
			jaFloat    rotAngle = i * deltaAngle;
			jaMatrix2  rotMat(rotAngle);

			pVertices[i + 1] = rotMat * radiusLine;
		}

		layerObjectDef->drawElements->setVerticesPositions(pVertices, (segmentsNum + 1));
		layerObjectDef->drawElements->setVerticesColor(color, (segmentsNum + 1));

		int32_t z = 0;
		for (int32_t i = 0; i < segmentsNum; ++i)
		{
			pIndices[z++] = 0;
			pIndices[z++] = i + 1;
			pIndices[z++] = i + 2;
		}
		pIndices[(segmentsNum * 3) - 1] = 1;

		layerObjectDef->drawElements->setTriangleIndices(pIndices, (segmentsNum * 3));
	}
	else
	{
		vertices.resize((segmentsNum * 2) * sizeof(jaVector2));
		vertices.setSize((segmentsNum * 2));
		indices.resize((segmentsNum * 3 * 2) * sizeof(uint32_t));
		indices.setSize((segmentsNum * 3 * 2));

		jaVector2* pVertices = vertices.getFirst();
		uint32_t*  pIndices  = indices.getFirst();

		jaFloat     deltaAngle = (2.0f * math::PI) / (jaFloat)(segmentsNum);
		jaVector2 radiusOutterLine(0.0f, radius);
		jaVector2 radiusInnerLine(0.0f, radius - edgeWidth);

		for (int32_t i = 0; i < segmentsNum; ++i)
		{
			jaFloat    rotAngle = i * deltaAngle;
			jaMatrix2  rotMat(rotAngle);

			pVertices[i] = rotMat * radiusOutterLine;
			pVertices[i + segmentsNum] = rotMat * radiusInnerLine;
		}

		int32_t z = 0;
		for (int32_t i = 0; i < segmentsNum; i++)
		{
			pIndices[z++] = i; //
			pIndices[z++] = (i + 1) % segmentsNum; // 
			pIndices[z++] = i + segmentsNum; //
			pIndices[z++] = i + segmentsNum; //
			pIndices[z++] = (i + 1) % segmentsNum; // 
			pIndices[z++] = segmentsNum + ((i + 1) % segmentsNum);//
		}

		layerObjectDef->drawElements->setVerticesPositions(pVertices, segmentsNum * 2);
		layerObjectDef->drawElements->setVerticesColor(color, segmentsNum * 2);
		layerObjectDef->drawElements->setTriangleIndices(pIndices, (segmentsNum * 2 * 3));
	}
	

	if (polygonEditor->getEditorMode() == LayerObjectEditorMode::SELECT_MODE)
	{
		LayerObjectEditor::getSingleton()->createLayerObject();
	}
	else
	{

	}
}

LightEditor::LightEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Light", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 350;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, 2 * (28 + 2));
	fillBoxLayout(boxLayout);

	{
		//FlowLayout* circleSizeLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		//circleSizeLayout->setSize(350, 28);
		//fillFlowLayout(circleSizeLayout);
		//
		//Label* radiusLabel = new Label(0, nullptr);
		//fillLabel(radiusLabel, "Radius");
		//
		//this->radiusTextfield = new TextField(1, nullptr);
		//fillTextField(this->radiusTextfield);
		//this->radiusTextfield->setAcceptOnlyNumbers(true);
		//this->radiusTextfield->setTextSize(12, 28);
		//this->radiusTextfield->setFieldValue("%f", 1.0f);
		//
		//Label* segmentsLabel = new Label(2, nullptr);
		//fillLabel(segmentsLabel, "Segments");
		//
		//this->segmentsTextField = new TextField(3, nullptr);
		//fillTextField(this->segmentsTextField);
		//this->segmentsTextField->setAcceptOnlyNumbers(true);
		//this->segmentsTextField->setTextSize(12, 28);
		//this->segmentsTextField->setFieldValue("%d", 16);
		//
		//this->createButton = new Button(4, nullptr);
		//fillButton(this->createButton, "Create");
		//this->createButton->setOnClickEvent(this, &PolygonCircleEditor::onCreateClick);
		//
		//circleSizeLayout->addWidget(radiusLabel);
		//circleSizeLayout->addWidget(this->radiusTextfield);
		//circleSizeLayout->addWidget(segmentsLabel);
		//circleSizeLayout->addWidget(this->segmentsTextField);
		//circleSizeLayout->addWidget(this->createButton);
		//
		//FlowLayout* drawTypeLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		//drawTypeLayout->setSize(350, 28);
		//fillFlowLayout(drawTypeLayout);
		//
		//this->facesButton = new RadioButton(0, nullptr);
		//this->edgesButton = new RadioButton(1, nullptr);
		//Label* facesLabel = new Label(2, nullptr);
		//Label* edgesLabel = new Label(3, nullptr);
		//Label* edgeWidthLabel = new Label(4, nullptr);
		//this->edgeWidthTextfield = new TextField(5, nullptr);
		//
		//fillLabel(facesLabel, "Faces");
		//fillLabel(edgesLabel, "Edges");
		//fillRadioButton(this->facesButton, "Faces");
		//fillRadioButton(this->edgesButton, "Edges");
		//fillLabel(edgeWidthLabel, "Edge width");
		//fillTextField(this->edgeWidthTextfield);
		//this->edgeWidthTextfield->setAcceptOnlyNumbers(true);
		//this->edgeWidthTextfield->setTextSize(12, 28);
		//this->edgeWidthTextfield->setFieldValue("%f", 0.1f);
		//
		//ButtonGroup* buttonGroup = new ButtonGroup(0, nullptr);
		//buttonGroup->addButton(this->facesButton);
		//buttonGroup->addButton(this->edgesButton);
		//
		//this->facesButton->setCheck(true);
		//
		//drawTypeLayout->addWidget(facesLabel);
		//drawTypeLayout->addWidget(this->facesButton);
		//drawTypeLayout->addWidget(edgesLabel);
		//drawTypeLayout->addWidget(this->edgesButton);
		//drawTypeLayout->addWidget(buttonGroup);
		//drawTypeLayout->addWidget(edgeWidthLabel);
		//drawTypeLayout->addWidget(this->edgeWidthTextfield);
		//
		//boxLayout->addWidget(circleSizeLayout);
		//boxLayout->addWidget(drawTypeLayout);
	}

	addWidgetToEditor(boxLayout);
}

CameraEditor::CameraEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Camera", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 350;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, (60 + 2) + (3 * (20 + 2)));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* nameLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		nameLayout->setSize(widgetWidth, 20);
		fillFlowLayout(nameLayout);
		
		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Name");
		nameLayout->addWidget(nameLabel);

		this->newNameTextfield = new TextField(1, nullptr);
		fillTextField(this->newNameTextfield);
		this->newNameTextfield->setTextSize(setup::TAG_NAME_LENGTH, 20);
		//this->newNameTextfield->setOnDeselectEvent(this, &LayerObjectCameraEditor::onNameDeselect);
		nameLayout->addWidget(this->newNameTextfield);

		this->createButton = new Button(2, nullptr);
		fillButton(this->createButton, "Create");
		this->createButton->setOnClickEvent(this, &CameraEditor::onCreateClick);
		nameLayout->addWidget(this->createButton);

		boxLayout->addWidget(nameLayout);
	}

	{
		this->cameraGridViewer = new GridViewer(4, 3, 3, nullptr);
		this->cameraGridViewer->setSize(widgetWidth, 60);
		fillGridViewer(this->cameraGridViewer);
		this->cameraGridViewer->setOnDrawEvent(this, &CameraEditor::onDrawCamera);
		this->cameraGridViewer->setOnClickEvent(this, &CameraEditor::onPickCamera);
		this->cameraGridViewer->setGridList(&cameras);

		boxLayout->addWidget(this->cameraGridViewer);
	}

	{
		FlowLayout* nameLayout = new FlowLayout(5, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		nameLayout->setSize(widgetWidth, 20);
		fillFlowLayout(nameLayout);

		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Name");
		nameLayout->addWidget(nameLabel);

		this->nameTextfield = new TextField(1, nullptr);
		fillTextField(this->nameTextfield);
		this->nameTextfield->setTextSize(setup::TAG_NAME_LENGTH, 20);
		this->nameTextfield->setOnDeselectEvent(this, &CameraEditor::onNameDeselect);
		nameLayout->addWidget(this->nameTextfield);

		this->deleteButton = new Button(2, nullptr);
		fillButton(this->deleteButton, "Delete");
		this->deleteButton->setOnClickEvent(this, &CameraEditor::onDeleteClick);
		nameLayout->addWidget(this->deleteButton);

		this->nameTextfield->setActive(false);
		this->deleteButton->setActive(false);

		boxLayout->addWidget(nameLayout);
	}

	{
		FlowLayout* selectedLayout = new FlowLayout(6, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		selectedLayout->setSize(widgetWidth, 20);
		fillFlowLayout(selectedLayout);
		
		Label* selectedLabel = new Label(0, nullptr);
		fillLabel(selectedLabel, "Selected as View");
		selectedLayout->addWidget(selectedLabel);

		this->selectedAsViewCheckbox = new Checkbox(1, nullptr);
		fillCheckbox(this->selectedAsViewCheckbox);
		this->selectedAsViewCheckbox->setCheck(false);
		selectedLayout->addWidget(this->selectedAsViewCheckbox);

		this->defaultButton = new Button(2, nullptr);
		fillButton(this->defaultButton, "Default");
		this->defaultButton->setOnClickEvent(this, &CameraEditor::onDefaultClick);
		selectedLayout->addWidget(this->defaultButton);

		boxLayout->addWidget(selectedLayout);
	}

	{
		cameraLabel.setFont("default.ttf",10);
		cameraLabel.setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
		cameraLabel.setColor(255, 255, 255, 255);
		cameraLabel.setText("");
	}

	addWidgetToEditor(boxLayout);

	this->editedCamera = nullptr;
}

void CameraEditor::updateWidget(void* data)
{
	LayerObject* layerObject = (LayerObject*)data;

	if (nullptr == layerObject)
	{
		return;
	}

	if (layerObject->getObjectType() == (uint8_t)LayerObjectType::CAMERA)
	{
		Camera* camera = (Camera*)layerObject;
		setCameraData(camera);
		refreshGridViewer();
	}
}

void CameraEditor::onDeleteNotify(void* data)
{
	LayerObject* layerObject = (LayerObject*)data;

	if (layerObject->getObjectType() == (uint8_t)LayerObjectType::CAMERA)
	{
		if (this->editedCamera == layerObject)
			setCameraData(nullptr);

		refreshGridViewer();
	}
}

void CameraEditor::setCameraData(void* data)
{
	this->editedCamera = (Camera*)data;

	if (nullptr == this->editedCamera)
	{
		this->nameTextfield->setText("");
		this->nameTextfield->setActive(false);
		this->deleteButton->setActive(false);
		this->deleteButton->setColor(255, 0, 0);

		return;
	}

	this->nameTextfield->setText(this->editedCamera->getCameraName());
	this->nameTextfield->setActive(true);
	this->deleteButton->setActive(true);
}

void CameraEditor::reloadResources()
{
	setCameraData(nullptr);
	refreshGridViewer();	
}

void CameraEditor::refreshGridViewer()
{
	cameras.clear();
	
	LayerObjectManager* layerObjectManager = LayerObjectManager::getSingleton();
	StackAllocator<LayerObject*> layerObjects;
	layerObjectManager->getLayerObjects(layerObjects);

	const uint32_t layerObjectsNum = layerObjects.getSize();
	LayerObject**  pLayerObjects   = layerObjects.getFirst();

	this->cameraGridViewer->setCellColor(255, 0, 0, 150);

	for (uint32_t i = 0; i < layerObjectsNum; ++i)
	{
		if (pLayerObjects[i]->getObjectType() == (uint8_t)LayerObjectType::CAMERA)
		{
			if (!pLayerObjects[i]->isDeleted())
				cameras.push_back(pLayerObjects[i]);
		}
	}
}

void CameraEditor::onNameDeselect(Widget* sender, WidgetEvent action, void* data)
{
	ja::TextField* nameTextfield = (ja::TextField*)sender;
	const char*    cameraName = nameTextfield->getTextPtr();

	this->editedCamera->setCameraName(cameraName);
}

void CameraEditor::onCreateClick(Widget* sender, WidgetEvent action, void* data)
{
	LayerObjectEditor* polygonEditor = LayerObjectEditor::getSingleton();

	jaVector2 gizmo = LayerObjectEditor::getSingleton()->getGizmoPosition();

	LayerObjectDef* layerObjectDef = polygonEditor->getLayerObjectDef();

	const char* cameraName = this->newNameTextfield->getTextPtr();

	layerObjectDef->transform.position = gizmo;
	layerObjectDef->transform.rotationMatrix.setDegrees(0.0f);
	layerObjectDef->scale = jaVector2(1.0f, 1.0f);
	layerObjectDef->layerObjectType = (uint8_t)LayerObjectType::CAMERA;
	layerObjectDef->cameraName.setName(cameraName);

	if (polygonEditor->getEditorMode() == LayerObjectEditorMode::SELECT_MODE)
	{
		LayerObjectEditor::getSingleton()->createLayerObject();
	}

	refreshGridViewer();
}

void CameraEditor::onDrawCamera(Widget* sender, WidgetEvent action, void* data)
{
	Camera* camera = (Camera*)data;
	
	int32_t x, y;
	
	int32_t halfWidth, halfHeight;
	float   fX, fY;
	
	sender->getPosition(x, y);
	fX = (float)(x);
	fY = (float)(y);
	
	halfWidth = (sender->getWidth() / 2);
	halfHeight = (sender->getHeight() / 2);
	
	Button* button = (Button*)sender;
	
	if ((editedCamera == camera) && (nullptr != editedCamera))
	{
		button->setColor(0, 255, 0);
	}
	else
	{
		button->setColor(255, 0, 0);
	}
	
	cameraLabel.x = x + halfWidth;
	cameraLabel.y = y + halfHeight;
	cameraLabel.setText(camera->getCameraName());
	cameraLabel.draw(0, 0);
}

void CameraEditor::onPickCamera(Widget* sender, WidgetEvent action, void* data)
{
	Button* button = (Button*)sender;
	Camera* camera = (Camera*)button->vUserData;
	
	setCameraData(camera);

	if (this->selectedAsViewCheckbox->isChecked())
	{
		CameraInterface* cameraInterface = GameManager::getSingleton()->getActiveCamera();
		cameraInterface->position = camera->getPosition();
		cameraInterface->scale    = camera->getCameraScale();
		cameraInterface->angleRad = camera->getAngleRad();
	}
}

void CameraEditor::onDefaultClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	CameraInterface* cameraInterface = GameManager::getSingleton()->getActiveCamera();
	//cameraInterface->position        = camera->getPosition();
	cameraInterface->scale           = jaVector2(1.0f, 1.0f);
	cameraInterface->angleRad        = 0.0f;
}

void CameraEditor::onDeleteClick(Widget* sender, WidgetEvent action, void* data)
{		
	refreshGridViewer();
	
	setCameraData(nullptr);
	
	this->deleteButton->setColor(255, 0, 0);
}

TextEditor::TextEditor(uint32_t widgetId, ja::Widget* parent) : CommonWidget(widgetId, "Text", parent)
{
	singleton = this;
	setRender(false);
	setActive(false);

	const int32_t widgetWidth   = 350;
	const int32_t editboxHeight = 100;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, (editboxHeight + 2) + (1 * (20 + 2)));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* editboxLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		editboxLayout->setSize(widgetWidth, 100);
		fillFlowLayout(editboxLayout);

		this->editbox = new Editbox(0, nullptr);
		fillEditbox(this->editbox);
		this->editbox->setSize(widgetWidth, editboxHeight);
		this->editbox->setAcceptNewLine(true);
		this->editbox->setAllowDynamicBuffering(true);
		editboxLayout->addWidget(this->editbox);

		boxLayout->addWidget(editboxLayout);
	}

	{
		FlowLayout* nameLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		nameLayout->setSize(widgetWidth, 20);
		fillFlowLayout(nameLayout);

		this->applyButton = new Button(0, nullptr);
		fillButton(this->applyButton, "Apply");
		this->applyButton->setOnClickEvent(this, &TextEditor::onApplyClick);
		nameLayout->addWidget(this->applyButton);

		boxLayout->addWidget(nameLayout);
	}

	addWidgetToEditor(boxLayout);
}

void TextEditor::updateWidget(void* data)
{
	LayerObject* layerObject = (LayerObject*)data;

	if (nullptr == layerObject)
	{
		this->applyButton->setActive(false);

		return;
	}

	if (layerObject->getObjectType() == (uint8_t)LayerObjectType::POLYGON)
	{
		this->applyButton->setActive(true);
	}
}

void TextEditor::onApplyClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	LayerObjectEditor*      pLayerObjectEditor = LayerObjectEditor::getSingleton();
	LayerObjectColorEditor* pColorEditor       = LayerObjectColorEditor::getSingleton();

	ja::Resource<FontPolygon>* fontResource = ResourceManager::getSingleton()->getEternalFontPolygon("lucon.ttf");

	if (nullptr == fontResource)
		return;

	std::string editboxText = this->editbox->getText();
	ja::Color4f textColor   = Color4f(pColorEditor->red, pColorEditor->green, pColorEditor->blue, pColorEditor->alpha);

	for (LayerObject* pLayerObjectIt : pLayerObjectEditor->selectedLayerObjects)
	{
		if (pLayerObjectIt->getObjectType() != (uint8_t)LayerObjectType::POLYGON)
			continue;
		
		ja::Polygon* pPolygon = (ja::Polygon*)pLayerObjectIt;

		pPolygon->setText(fontResource->resource, editboxText.c_str(), textColor);
	}
}

LayersEditor::LayersEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Layers", parent)
{
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 270;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, (3 * (20 + 2)) + (100 + 2));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* nameLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		nameLayout->setSize(widgetWidth, 20);
		fillFlowLayout(nameLayout);

		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Name");
		nameLayout->addWidget(nameLabel);

		this->newLayerNameTextfield = new TextField(1, nullptr);
		fillTextField(this->newLayerNameTextfield);
		this->newLayerNameTextfield->setTextSize(setup::TAG_NAME_LENGTH, 20);
		//this->newLayerNameTextfield->setOnDeselectEvent(this, &LayersEditor::onNameDeselect);
		nameLayout->addWidget(this->newLayerNameTextfield);

		this->createLayerButton = new Button(2, nullptr);
		fillButton(this->createLayerButton, "Create");
		this->createLayerButton->setOnClickEvent(this, &LayersEditor::onCreateLayerClick);
		nameLayout->addWidget(this->createLayerButton);

		boxLayout->addWidget(nameLayout);
	}

	{
		this->layersGridViewer = new GridViewer(4, 1, 5, nullptr);
		this->layersGridViewer->setSize(widgetWidth, 100);
		fillGridViewer(this->layersGridViewer);
		this->layersGridViewer->setOnDrawEvent(this, &LayersEditor::onDrawLayer);
		this->layersGridViewer->setOnClickEvent(this, &LayersEditor::onPickLayer);
		this->layersGridViewer->setGridList(&layers);

		boxLayout->addWidget(this->layersGridViewer);
	}

	{
		FlowLayout* nameLayout = new FlowLayout(5, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		nameLayout->setSize(widgetWidth, 20);
		fillFlowLayout(nameLayout);

		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Name");
		nameLayout->addWidget(nameLabel);

		this->layerNameTextfield = new TextField(1, nullptr);
		fillTextField(this->layerNameTextfield);
		this->layerNameTextfield->setTextSize(setup::TAG_NAME_LENGTH, 20);
		this->layerNameTextfield->setOnDeselectEvent(this, &LayersEditor::onLayerNameDeselect);
		nameLayout->addWidget(this->layerNameTextfield);

		this->deleteLayerButton = new Button(2, nullptr);
		fillButton(this->deleteLayerButton, "Delete");
		this->deleteLayerButton->setOnClickEvent(this, &LayersEditor::onDeleteLayerClick);
		nameLayout->addWidget(this->deleteLayerButton);

		this->layerNameTextfield->setActive(false);
		this->deleteLayerButton->setActive(false);

		boxLayout->addWidget(nameLayout);
	}

	{
		FlowLayout* isActiveLayout = new FlowLayout(5, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		isActiveLayout->setSize(widgetWidth, 20);
		fillFlowLayout(isActiveLayout);

		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Active");
		isActiveLayout->addWidget(nameLabel);

		this->isActiveCheckbox = new Checkbox(1, nullptr);
		fillCheckbox(this->isActiveCheckbox);
		isActiveCheckbox->setOnChangeEvent(this, &LayersEditor::onActiveChange);
		isActiveLayout->addWidget(this->isActiveCheckbox);

		this->moveDownButton = new Button(2, nullptr);
		fillButton(this->moveDownButton, "Move down");
		this->moveDownButton->setOnClickEvent(this, &LayersEditor::onMoveLayerDownClick);
		isActiveLayout->addWidget(this->moveDownButton);

		this->moveUpButton = new Button(3, nullptr);
		fillButton(this->moveUpButton, "Move up");
		this->moveUpButton->setOnClickEvent(this, &LayersEditor::onMoveLayerUpClick);
		isActiveLayout->addWidget(this->moveUpButton);

		boxLayout->addWidget(isActiveLayout);
	}

	{
		layerLabel.setFont("default.ttf",10);
		layerLabel.setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
		layerLabel.setColor(255, 255, 255, 255);
		layerLabel.setText("");
	}

	setLayerData(nullptr);

	addWidgetToEditor(boxLayout);
}

void LayersEditor::updateWidget(void* data)
{
	LayerObject* layerObject = (LayerObject*)data;

	if (nullptr == layerObject)
	{
		setLayerData(nullptr);
		return;
	}

	Layer* layer = EffectManager::getSingleton()->getLayer(layerObject->layerId);
	setLayerData(layer);
}

void LayersEditor::reloadResources()
{
	refreshGridViewer();

	setLayerData(nullptr);
}

void LayersEditor::refreshGridViewer()
{
	layers.clear();

	EffectManager* effectManager = EffectManager::getSingleton();
	const uint8_t layersNum      = effectManager->getLayersNum();

	this->layersGridViewer->setCellColor(255, 0, 0, 150);

	for (uint8_t i = 0; i < layersNum; ++i)
	{
		Layer* layer = effectManager->getLayer(i);
		layers.push_back(layer);
	}
}

void LayersEditor::setLayerData(Layer* layer)
{
	this->editedLayer = layer;

	if (nullptr == layer)
	{
		this->layerNameTextfield->setText("");
		this->isActiveCheckbox->setCheck(false);
		this->layerNameTextfield->setActive(false);
		this->isActiveCheckbox->setActive(false);
		this->deleteLayerButton->setActive(false);
		this->moveUpButton->setActive(false);
		this->moveDownButton->setActive(false);

		this->deleteLayerButton->setColor(255, 0, 0, 128);

		return;
	}	

	this->layerNameTextfield->setActive(true);
	this->isActiveCheckbox->setActive(true);
	this->deleteLayerButton->setActive(true);
	this->moveDownButton->setActive(true);
	this->moveUpButton->setActive(true);

	const char* layerName = layer->getName();
	this->layerNameTextfield->setText(layerName);
	this->isActiveCheckbox->setCheck(layer->isActive);
	
}

void LayersEditor::onLayerNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	ja::TextField* nameTextfield = (ja::TextField*)sender;
	const char*    layerName     = nameTextfield->getTextPtr();

	this->editedLayer->setName(layerName);
}

void LayersEditor::onCreateLayerClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	const char* layerName = newLayerNameTextfield->getTextPtr();

	EffectManager* effectManager = EffectManager::getSingleton();
	effectManager->createLayer(layerName);

	refreshGridViewer();
}

void LayersEditor::onDeleteLayerClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	StackAllocator<LayerObject*> layerObjects;

	LayerObjectManager* layerObjectManager = LayerObjectManager::getSingleton();
	EffectManager*      effectManager      = EffectManager::getSingleton();

	layerObjectManager->getLayerObjects(layerObjects);
	
	uint8_t currentLayerId = effectManager->getLayerId(this->editedLayer);

	LayerObject**  pLayerObject    = layerObjects.getFirst();
	const uint32_t layerObjectsNum = layerObjects.getSize();
	
	for (uint32_t i = 0; i < layerObjectsNum; ++i)
	{
		if (pLayerObject[i]->layerId == currentLayerId)
		{
			Toolset::getSingleton()->infoHud->addInfo(255, 0, 0, "Can't delete layer there are objects on it");
			return;
		}
	}

	for (uint32_t i = 0; i < layerObjectsNum; ++i)
	{
		if (pLayerObject[i]->layerId > currentLayerId)
		{
			--pLayerObject[i]->layerId;
		}
	}
	
	effectManager->deleteLayer(this->editedLayer);

	setLayerData(nullptr);

	refreshGridViewer();
}

void LayersEditor::onActiveChange(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	bool isChecked = this->isActiveCheckbox->isChecked();

	this->editedLayer->isActive = isChecked;
}

void LayersEditor::onMoveLayerUpClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	EffectManager* effectManager = EffectManager::getSingleton();

	uint8_t layersNum      = effectManager->getLayersNum();
	uint8_t currentLayerId = effectManager->getLayerId(this->editedLayer);

	if ((layersNum - 1) == currentLayerId)
	{
		Toolset::getSingleton()->infoHud->addInfo(255, 0, 0, "Layer on top");
		return;
	}

	LayerObjectManager* layerObjectManager = LayerObjectManager::getSingleton();
	StackAllocator<LayerObject*> layerObjects;
	layerObjectManager->getLayerObjects(layerObjects);

	const uint32_t layerObjectsNum = layerObjects.getSize();
	LayerObject**  pLayerObject    = layerObjects.getFirst();

	uint8_t upperLayerId = currentLayerId + 1;
	Layer*  upperLayer   = effectManager->getLayer(upperLayerId);

	for (uint32_t i = 0; i < layerObjectsNum; ++i)
	{
		if (currentLayerId == pLayerObject[i]->layerId)
		{
			++pLayerObject[i]->layerId;
			continue;
		}

		if (upperLayerId == pLayerObject[i]->layerId)
		{
			--pLayerObject[i]->layerId;
			continue;
		}
	}

	char copyBuffer[sizeof(Layer)];
	memcpy(copyBuffer, this->editedLayer, sizeof(Layer));
	memcpy(this->editedLayer, upperLayer, sizeof(Layer));
	memcpy(upperLayer, copyBuffer, sizeof(Layer));

	this->editedLayer = upperLayer;
	layerObjectManager->layerObjectDef.layerId = upperLayerId;

	refreshGridViewer();
}

void LayersEditor::onMoveLayerDownClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	EffectManager* effectManager = EffectManager::getSingleton();

	uint8_t currentLayerId = effectManager->getLayerId(this->editedLayer);

	if (0 == currentLayerId)
	{
		Toolset::getSingleton()->infoHud->addInfo(255, 0, 0, "Layer on bottom");
		return;
	}

	LayerObjectManager* layerObjectManager = LayerObjectManager::getSingleton();
	StackAllocator<LayerObject*> layerObjects;
	layerObjectManager->getLayerObjects(layerObjects);

	const uint32_t layerObjectsNum = layerObjects.getSize();
	LayerObject**  pLayerObject    = layerObjects.getFirst();

	uint8_t lowerLayerId = currentLayerId - 1;
	Layer*  lowerLayer   = effectManager->getLayer(lowerLayerId);

	for (uint32_t i = 0; i < layerObjectsNum; ++i)
	{
		if (currentLayerId == pLayerObject[i]->layerId)
		{
			--pLayerObject[i]->layerId;
			continue;
		}

		if (lowerLayerId == pLayerObject[i]->layerId)
		{
			++pLayerObject[i]->layerId;
			continue;
		}
	}

	const char* layerName = this->editedLayer->getName();

	char copyBuffer[sizeof(Layer)];
	memcpy(copyBuffer, this->editedLayer, sizeof(Layer));
	memcpy(this->editedLayer, lowerLayer, sizeof(Layer));
	memcpy(lowerLayer, copyBuffer, sizeof(Layer));

	this->editedLayer = lowerLayer;
	layerObjectManager->layerObjectDef.layerId = lowerLayerId;

	refreshGridViewer();
}

void LayersEditor::onDrawLayer(Widget* sender, WidgetEvent action, void* data)
{
	Layer* layer = (Layer*)data;
	
	int32_t x, y;
	
	int32_t halfWidth, halfHeight;
	float   fX, fY;
	
	sender->getPosition(x, y);
	fX = (float)(x);
	fY = (float)(y);
	
	halfWidth  = (sender->getWidth() / 2);
	halfHeight = (sender->getHeight() / 2);
	
	Button* button = (Button*)sender;
	
	if ((editedLayer == layer) && (nullptr != editedLayer))
	{
		button->setColor(0, 255, 0);
	}
	else
	{
		button->setColor(255, 0, 0);
	}
	
	layerLabel.x = x + halfWidth;
	layerLabel.y = y + halfHeight;
	layerLabel.setTextPrintC("%s active:%s", layer->getName(), layer->isActive ? "true" : "false");
	layerLabel.draw(0, 0);
}

void LayersEditor::onPickLayer(Widget* sender, WidgetEvent action, void* data)
{
	Button* button = (Button*)sender;
	Layer*  layer  = (Layer*)button->vUserData;

	Layer* pFirstLayer = EffectManager::getSingleton()->getLayer(0);
	LayerObjectManager::getSingleton()->layerObjectDef.layerId = ((uintptr_t)layer - (uintptr_t)pFirstLayer) / sizeof(Layer);
	
	setLayerData(layer);
}

ParallaxEditor::ParallaxEditor(uint32_t widgetId, ja::Widget* parent) : CommonWidget(widgetId, "Parallax", parent)
{
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 300;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, (4 * (20 + 2)) + 7 * (20 + 2));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* nameLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		nameLayout->setSize(widgetWidth, 20);
		fillFlowLayout(nameLayout);

		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Name");
		nameLayout->addWidget(nameLabel);

		this->newParallaxNameTextfield = new TextField(1, nullptr);
		fillTextField(this->newParallaxNameTextfield);
		this->newParallaxNameTextfield->setTextSize(setup::TAG_NAME_LENGTH, 20);
		nameLayout->addWidget(this->newParallaxNameTextfield);

		this->createParallaxButton = new Button(2, nullptr);
		fillButton(this->createParallaxButton, "Create");
		this->createParallaxButton->setOnClickEvent(this, &ParallaxEditor::onCreateParallaxClick);
		nameLayout->addWidget(this->createParallaxButton);

		boxLayout->addWidget(nameLayout);
	}

	{
		this->parallaxGridViewer = new GridViewer(4, 1, 5, nullptr);
		this->parallaxGridViewer->setSize(widgetWidth, 100);
		fillGridViewer(this->parallaxGridViewer);
		this->parallaxGridViewer->setOnDrawEvent(this, &ParallaxEditor::onDrawParallax);
		this->parallaxGridViewer->setOnClickEvent(this, &ParallaxEditor::onPickParallax);
		this->parallaxGridViewer->setGridList(&parallaxes);

		boxLayout->addWidget(this->parallaxGridViewer);
	}

	{
		FlowLayout* nameLayout = new FlowLayout(5, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		nameLayout->setSize(widgetWidth, 20);
		fillFlowLayout(nameLayout);

		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Name");
		nameLayout->addWidget(nameLabel);

		this->parallaxNameTextfield = new TextField(1, nullptr);
		fillTextField(this->parallaxNameTextfield);
		this->parallaxNameTextfield->setTextSize(setup::TAG_NAME_LENGTH, 20);
		this->parallaxNameTextfield->setOnDeselectEvent(this, &ParallaxEditor::onParallaxNameDeselect);
		nameLayout->addWidget(this->parallaxNameTextfield);

		this->deleteParallaxButton = new Button(2, nullptr);
		fillButton(this->deleteParallaxButton, "Delete");
		this->deleteParallaxButton->setOnClickEvent(this, &ParallaxEditor::onDeleteParallaxClick);
		nameLayout->addWidget(this->deleteParallaxButton);

		this->parallaxNameTextfield->setActive(false);
		this->deleteParallaxButton->setActive(false);

		boxLayout->addWidget(nameLayout);
	}

	{
		FlowLayout* isActiveLayout = new FlowLayout(5, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		isActiveLayout->setSize(widgetWidth, 20);
		fillFlowLayout(isActiveLayout);

		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Active");
		isActiveLayout->addWidget(nameLabel);

		this->isActiveCheckbox = new Checkbox(1, nullptr);
		fillCheckbox(this->isActiveCheckbox);
		isActiveCheckbox->setOnChangeEvent(this, &ParallaxEditor::onActiveChange);
		isActiveLayout->addWidget(this->isActiveCheckbox);

		this->moveDownButton = new Button(2, nullptr);
		fillButton(this->moveDownButton, "Move down");
		this->moveDownButton->setOnClickEvent(this, &ParallaxEditor::onMoveLayerDownClick);
		isActiveLayout->addWidget(this->moveDownButton);

		this->moveUpButton = new Button(3, nullptr);
		fillButton(this->moveUpButton, "Move up");
		this->moveUpButton->setOnClickEvent(this, &ParallaxEditor::onMoveLayerUpClick);
		isActiveLayout->addWidget(this->moveUpButton);

		boxLayout->addWidget(isActiveLayout);
	}

	{
		this->cameraGridViewer = new GridViewer(4, 3, 3, nullptr);
		this->cameraGridViewer->setSize(widgetWidth, 60);
		fillGridViewer(this->cameraGridViewer);
		this->cameraGridViewer->setOnDrawEvent(this, &ParallaxEditor::onDrawCamera);
		this->cameraGridViewer->setOnClickEvent(this, &ParallaxEditor::onPickCamera);
		this->cameraGridViewer->setGridList(&cameras);

		boxLayout->addWidget(this->cameraGridViewer);
	}

	{
		parallaxLabel.setFont("default.ttf",10);
		parallaxLabel.setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
		parallaxLabel.setColor(255, 255, 255, 255);
		parallaxLabel.setText("");
	}

	setParallaxData(nullptr);

	addWidgetToEditor(boxLayout);
}

void ParallaxEditor::reloadResources()
{
	refreshGridViewer();

	setParallaxData(nullptr);
}

void ParallaxEditor::refreshGridViewer()
{
	parallaxes.clear();

	EffectManager* effectManager = EffectManager::getSingleton();
	const uint32_t parallaxesNum = effectManager->getParallaxesNum();

	this->parallaxGridViewer->setCellColor(255, 0, 0, 150);

	for (uint8_t i = 0; i < parallaxesNum; ++i)
	{
		Parallax* parallax = effectManager->getParallax(i);
		parallaxes.push_back(parallax);
	}
}

void ParallaxEditor::refreshCameraGridViewer()
{
	cameras.clear();

	LayerObjectManager* layerObjectManager = LayerObjectManager::getSingleton();
	StackAllocator<LayerObject*> layerObjects;
	layerObjectManager->getLayerObjects(layerObjects);

	const uint32_t layerObjectsNum = layerObjects.getSize();
	LayerObject**  pLayerObjects   = layerObjects.getFirst();

	this->cameraGridViewer->setCellColor(255, 0, 0, 150);

	for (uint32_t i = 0; i < layerObjectsNum; ++i)
	{
		if (pLayerObjects[i]->getObjectType() == (uint8_t)LayerObjectType::CAMERA)
		{
			if (!pLayerObjects[i]->isDeleted())
				cameras.push_back(pLayerObjects[i]);
		}
	}
}

void ParallaxEditor::setParallaxData(Parallax* parallax)
{
	this->editedParallax = parallax;;

	if (nullptr == parallax)
	{
		this->parallaxNameTextfield->setText("");
		this->isActiveCheckbox->setCheck(false);
		this->parallaxNameTextfield->setActive(false);
		this->isActiveCheckbox->setActive(false);
		this->deleteParallaxButton->setActive(false);
		this->moveUpButton->setActive(false);
		this->moveDownButton->setActive(false);

		this->deleteParallaxButton->setColor(255, 0, 0, 128);

		return;
	}

	this->parallaxNameTextfield->setActive(true);
	this->isActiveCheckbox->setActive(true);
	this->deleteParallaxButton->setActive(true);
	this->moveDownButton->setActive(true);
	this->moveUpButton->setActive(true);

	const char* parallaxName = parallax->getName();
	this->parallaxNameTextfield->setText(parallaxName);
	this->isActiveCheckbox->setCheck(parallax->isActive);
}

void ParallaxEditor::setCameraData(void* data)
{
	this->editedParallax->camera = (Camera*)data;
}

void ParallaxEditor::onDeleteNotify(void* data)
{
	LayerObject* layerObject = (LayerObject*)data;

	if (layerObject->getObjectType() == (uint8_t)LayerObjectType::CAMERA)
	{
		refreshCameraGridViewer();

		EffectManager* effectManager = EffectManager::getSingleton();
		uint8_t parallaxesNum = effectManager->getParallaxesNum();
		
		for (uint8_t i = 0; i < parallaxesNum; ++i)
		{
			Parallax* parallax = effectManager->getParallax(i);
			
			if (parallax->camera == layerObject)
			{
				parallax->camera = nullptr;
			}
		}
	}
}

void ParallaxEditor::onCreateNotify(void* data)
{
	LayerObject* layerObject = (LayerObject*)data;

	if (layerObject->getObjectType() == (uint8_t)LayerObjectType::CAMERA)
	{
		refreshCameraGridViewer();
	}
}

void ParallaxEditor::onParallaxNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	ja::TextField* nameTextfield = (ja::TextField*)sender;
	const char*    parallaxName  = nameTextfield->getTextPtr();

	this->editedParallax->setName(parallaxName);
}

void ParallaxEditor::onCreateParallaxClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	const char* parallaxName = newParallaxNameTextfield->getTextPtr();

	EffectManager* effectManager = EffectManager::getSingleton();
	effectManager->createParallax(parallaxName,nullptr,false);

	refreshGridViewer();
}

void ParallaxEditor::onDeleteParallaxClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	StackAllocator<LayerObject*> layerObjects;

	LayerObjectManager* layerObjectManager = LayerObjectManager::getSingleton();
	EffectManager*      effectManager      = EffectManager::getSingleton();

	if (this->editedParallax->isActiveCamera)
	{
		Toolset::getSingleton()->infoHud->addInfo(255, 0, 0, "Can't delete this parallax");
		return;
	}

	effectManager->deleteParallax(this->editedParallax);

	setParallaxData(nullptr);

	refreshGridViewer();
}

void ParallaxEditor::onActiveChange(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	bool isChecked = this->isActiveCheckbox->isChecked();

	this->editedParallax->isActive = isChecked;
}

void ParallaxEditor::onMoveLayerUpClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	EffectManager* effectManager = EffectManager::getSingleton();

	uint8_t parallaxesNum     = effectManager->getParallaxesNum();
	uint8_t currentParallaxId = effectManager->getParallaxId(this->editedParallax);

	if ((parallaxesNum - 1) == currentParallaxId)
	{
		Toolset::getSingleton()->infoHud->addInfo(255, 0, 0, "Parallax on top");
		return;
	}

	uint8_t   upperParallaxId = currentParallaxId + 1;
	Parallax* upperParallax   = effectManager->getParallax(upperParallaxId);

	char copyBuffer[sizeof(Parallax)];
	memcpy(copyBuffer, this->editedParallax, sizeof(Parallax));
	memcpy(this->editedParallax, upperParallax, sizeof(Parallax));
	memcpy(upperParallax, copyBuffer, sizeof(Parallax));

	this->editedParallax = upperParallax;

	refreshGridViewer();
}

void ParallaxEditor::onMoveLayerDownClick(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	EffectManager* effectManager = EffectManager::getSingleton();

	uint8_t currentParallaxId = effectManager->getParallaxId(this->editedParallax);

	if (0 == currentParallaxId)
	{
		Toolset::getSingleton()->infoHud->addInfo(255, 0, 0, "Parallax on bottom");
		return;
	}

	uint8_t   lowerParallaxId = currentParallaxId - 1;
	Parallax* lowerParallax   = effectManager->getParallax(lowerParallaxId);

	char copyBuffer[sizeof(Parallax)];
	memcpy(copyBuffer, this->editedParallax, sizeof(Parallax));
	memcpy(this->editedParallax, lowerParallax, sizeof(Parallax));
	memcpy(lowerParallax, copyBuffer, sizeof(Parallax));

	this->editedParallax = lowerParallax;

	refreshGridViewer();
}

void ParallaxEditor::onDrawParallax(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	Parallax* parallax = (Parallax*)data;

	int32_t x, y;

	int32_t halfWidth, halfHeight;
	float   fX, fY;

	sender->getPosition(x, y);
	fX = (float)(x);
	fY = (float)(y);

	halfWidth  = (sender->getWidth() / 2);
	halfHeight = (sender->getHeight() / 2);

	Button* button = (Button*)sender;

	if ((editedParallax == parallax) && (nullptr != editedParallax))
	{
		button->setColor(0, 255, 0);
	}
	else
	{
		button->setColor(255, 0, 0);
	}

	char* cameraName = "";
	
	if (nullptr != editedParallax)
	{
		if (nullptr != editedParallax->camera)
			cameraName = (char*)editedParallax->camera->getCameraName();
	}
	
	parallaxLabel.x = x + halfWidth;
	parallaxLabel.y = y + halfHeight;
	parallaxLabel.setTextPrintC("%s active:%s Camera:%s", parallax->getName(), parallax->isActive ? "true" : "false", cameraName);
	parallaxLabel.draw(0, 0);
}

void ParallaxEditor::onPickParallax(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	Button*   button   = (Button*)sender;
	Parallax* parallax = (Parallax*)button->vUserData;

	if (parallax->isActiveCamera)
		return;

	setParallaxData(parallax);
}

void ParallaxEditor::onDrawCamera(Widget* sender, WidgetEvent action, void* data)
{
	Camera* camera = (Camera*)data;

	int32_t x, y;

	int32_t halfWidth, halfHeight;
	float   fX, fY;

	sender->getPosition(x, y);
	fX = (float)(x);
	fY = (float)(y);

	halfWidth = (sender->getWidth() / 2);
	halfHeight = (sender->getHeight() / 2);

	Button* button = (Button*)sender;

	if (nullptr == editedParallax)
	{
		button->setColor(255, 0, 0);
	}
	else
	{
		if ((editedParallax->camera == camera) && (nullptr != editedParallax->camera))
		{
			button->setColor(0, 255, 0);
		}
		else
		{
			button->setColor(255, 0, 0);
		}
	}
	
	parallaxLabel.x = x + halfWidth;
	parallaxLabel.y = y + halfHeight;
	parallaxLabel.setText(camera->getCameraName());
	parallaxLabel.draw(0, 0);
}

void ParallaxEditor::onPickCamera(Widget* sender, WidgetEvent action, void* data)
{
	Button* button = (Button*)sender;
	Camera* camera = (Camera*)button->vUserData;

	setCameraData(camera);
}

LayerObjectMaterialEditor::LayerObjectMaterialEditor(uint32_t widgetId, ja::Widget* parent) : CommonWidget(widgetId, "Material", parent)
{
	setRender(false);
	setActive(false);

	const int32_t widgetWidth      = 350;
	const int32_t labelWidth       = 100;
	const int32_t buttonWidth      = 180;
	const int32_t scrollbarWidth   = 60;
	const int32_t scrollPaneHeight = 100;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	//boxLayout->setSize(widgetWidth, (4 * (20 + 2)) + 7 * (20 + 2));
	boxLayout->setSize(widgetWidth, (4 * (20 + 2)) + 100);
	fillBoxLayout(boxLayout);

	{
		FlowLayout* nameLayout = new FlowLayout(3, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		nameLayout->setSize(widgetWidth, 20);
		fillFlowLayout(nameLayout);

		Label* nameLabel = new Label(0, nullptr);
		fillLabel(nameLabel, "Material");
		nameLabel->setSize(labelWidth,20);
		nameLayout->addWidget(nameLabel);

		this->materialButton = new ja::Button(0, nullptr);
		fillButton(this->materialButton, "");
		this->materialButton->setSize(buttonWidth, 20);
		nameLayout->addWidget(this->materialButton);

		boxLayout->addWidget(nameLayout);
	}

	{
		FlowLayout* blendFactorsLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		blendFactorsLayout->setSize(widgetWidth, 20);
		fillFlowLayout(blendFactorsLayout);

		Label* sourceFactorLabel = new Label(0, nullptr);
		fillLabel(sourceFactorLabel, "Source");
		sourceFactorLabel->setSize(labelWidth, 20);
		blendFactorsLayout->addWidget(sourceFactorLabel);

		this->sourceFactorButton = new ja::Button(0, nullptr);
		fillButton(this->sourceFactorButton, "");
		this->sourceFactorButton->setSize(buttonWidth, 20);
		blendFactorsLayout->addWidget(this->sourceFactorButton);

		const uint32_t blendFactorsNum = Material::getBlendFactorsNum();

		this->sourceFactorScrollbar = new ja::Scrollbar(1, nullptr);
		fillScrollbar(this->sourceFactorScrollbar, 0, (double)(blendFactorsNum - 1));
		this->sourceFactorScrollbar->setSize(scrollbarWidth, 20);
		this->sourceFactorScrollbar->setOnChangeEvent(this, &LayerObjectMaterialEditor::onScrollbarChange);
		blendFactorsLayout->addWidget(this->sourceFactorScrollbar);

		boxLayout->addWidget(blendFactorsLayout);
	}

	{
		FlowLayout* blendFactorsLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		blendFactorsLayout->setSize(widgetWidth, 20);
		fillFlowLayout(blendFactorsLayout);

		Label* destinationFactorLabel = new Label(0, nullptr);
		fillLabel(destinationFactorLabel, "Destination");
		destinationFactorLabel->setSize(labelWidth, 20);
		blendFactorsLayout->addWidget(destinationFactorLabel);

		this->destinationFactorButton = new ja::Button(0, nullptr);
		fillButton(this->destinationFactorButton, "");
		this->destinationFactorButton->setSize(buttonWidth, 20);
		blendFactorsLayout->addWidget(this->destinationFactorButton);

		const uint32_t blendFactorsNum = Material::getBlendFactorsNum();

		this->destinationFactorScrollbar = new ja::Scrollbar(1, nullptr);
		fillScrollbar(this->destinationFactorScrollbar, 0, (double)(blendFactorsNum - 1));
		this->destinationFactorScrollbar->setSize(scrollbarWidth, 20);
		this->destinationFactorScrollbar->setOnChangeEvent(this, &LayerObjectMaterialEditor::onScrollbarChange);
		blendFactorsLayout->addWidget(this->destinationFactorScrollbar);

		boxLayout->addWidget(blendFactorsLayout);
	}

	{
		FlowLayout* blendEquationLayout = new FlowLayout(4, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		blendEquationLayout->setSize(widgetWidth, 20);
		fillFlowLayout(blendEquationLayout);

		Label* blendEquationLabel = new Label(0, nullptr);
		fillLabel(blendEquationLabel, "Equation");
		blendEquationLabel->setSize(labelWidth, 20);
		blendEquationLayout->addWidget(blendEquationLabel);

		this->blendEquationButton = new ja::Button(0, nullptr);
		fillButton(this->blendEquationButton, "");
		this->blendEquationButton->setSize(buttonWidth, 20);
		blendEquationLayout->addWidget(this->blendEquationButton);

		const uint32_t blendEquationsNum = Material::getBlendEquationsNum();

		this->blendEquationScrollbar = new ja::Scrollbar(1, nullptr);
		fillScrollbar(this->blendEquationScrollbar, 0, (double)(blendEquationsNum - 1));
		this->blendEquationScrollbar->setSize(scrollbarWidth, 20);
		this->blendEquationScrollbar->setOnChangeEvent(this, &LayerObjectMaterialEditor::onScrollbarChange);
		blendEquationLayout->addWidget(this->blendEquationScrollbar);

		boxLayout->addWidget(blendEquationLayout);
	}

	{
		this->scrollPane = new ScrollPane(4, nullptr);
		this->scrollPane->setSize(widgetWidth, scrollPaneHeight);
		fillScrollPane(this->scrollPane);

		this->scrollPane->setColor(0, 255, 0, 128);

		boxLayout->addWidget(this->scrollPane);
	}

	setMaterialData(nullptr);

	addWidgetToEditor(boxLayout);
}

void LayerObjectMaterialEditor::updateWidget(void* data)
{
	LayerObject* layerObject = (LayerObject*)data;

	if (nullptr == layerObject)
	{
		setMaterialData(nullptr);
		return;
	}

	ja::Material* material = nullptr;

	if (layerObject->getObjectType() == (uint8_t)LayerObjectType::POLYGON)
	{
		ja::Polygon* polygon = (ja::Polygon*)layerObject;
		material = polygon->getMaterial();
	}
	
	setMaterialData(material);
}

void LayerObjectMaterialEditor::reloadResources()
{
	//materials.clear();
}

void LayerObjectMaterialEditor::setMaterialData(ja::Material* material)
{
	if (material == this->editedMaterial)
		return;

	this->editedMaterial = material;
	refreshDataFieldViewer();

	if (nullptr == this->editedMaterial)
	{
		this->materialButton->setActive(false);
		this->sourceFactorButton->setActive(false);
		this->sourceFactorScrollbar->setActive(false);
		this->destinationFactorButton->setActive(false);
		this->destinationFactorScrollbar->setActive(false);
		this->blendEquationButton->setActive(false);
		this->blendEquationScrollbar->setActive(false);

		this->materialButton->setText("");
		this->sourceFactorButton->setText("");
		this->destinationFactorButton->setText("");
		this->blendEquationButton->setText("");

		return;
	}
	
	this->materialButton->setActive(true);
	this->sourceFactorButton->setActive(true);
	this->sourceFactorScrollbar->setActive(true);
	this->destinationFactorButton->setActive(true);
	this->destinationFactorScrollbar->setActive(true);
	this->blendEquationButton->setActive(true);
	this->blendEquationScrollbar->setActive(true);

	this->materialButton->setText(editedMaterial->getMaterialName());
	this->sourceFactorButton->setText(editedMaterial->getSourceFactorName());
	this->destinationFactorButton->setText(editedMaterial->getDestinationFactorName());
	this->blendEquationButton->setText(editedMaterial->getBlendEquationName());

	this->blendEquationScrollbar->setValue(Material::getBlendEquationId(editedMaterial->blendEquation));
	this->sourceFactorScrollbar->setValue(Material::getBlendFactorId(editedMaterial->sourceFactor));
	this->destinationFactorScrollbar->setValue(Material::getBlendFactorId(editedMaterial->destinationFactor));
}

void LayerObjectMaterialEditor::refreshDataFieldViewer()
{
	this->scrollPane->clearWidgets();
	this->dataNotifiers.clear();

	if (nullptr == this->editedMaterial)
		return;

	const ShaderStructureDef* pMaterialShaderDef = this->editedMaterial->getMaterialShaderDef();
	
	const StackAllocator<DataFieldInfo>* dataFieldsStack = pMaterialShaderDef->getDataFields();
	const uint32_t                       dataFieldsNum   = dataFieldsStack->getSize();
	DataFieldInfo*                       pDataField      = dataFieldsStack->getFirst();

	uint32_t fieldsNum = 0;

	for (uint32_t i = 0; i < dataFieldsNum; ++i)
	{
		fieldsNum += pDataField[i].getElementsNum();
	}

	const uint32_t gridLayoutHeight = fieldsNum  * (20 + 2);
	const int32_t  scrollPaneWidth  = this->scrollPane->getWidth();

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(scrollPaneWidth, gridLayoutHeight);

	boxLayout->setPosition(0, 0);
	boxLayout->setColor(0, 0, 255, 150); 

	this->dataNotifiers.resize(sizeof(DataNotifier) * fieldsNum);
	DataNotifier* pDataNotifier = this->dataNotifiers.getFirst();
	void*         pUniformData  = this->editedMaterial->getMaterialData();

	for (uint32_t i = 0; i < dataFieldsNum; ++i)
	{
		const uint32_t elementsNum = pDataField[i].getElementsNum();

		for (uint32_t z = 0; z < elementsNum; ++z)
		{
			DataNotifier* fieldNotifier = new (&pDataNotifier[i + z]) DataNotifier(pUniformData, z, &pDataField[i]);

			fieldNotifier->setDataChangeCallback(this, &LayerObjectMaterialEditor::onDataChange);
			Widget* widget = fieldNotifier->createDataWidget();

			boxLayout->addWidget(widget);
		}
	}

	this->scrollPane->addWidget(boxLayout);
	//this->scrollPane->setScrollY(INT_MAX);
	this->scrollPane->setVerticalScrollbarPolicy(ScrollbarPolicy::JA_SCROLLBAR_ALWAYS);
}

void LayerObjectMaterialEditor::onScrollbarChange(ja::Widget* sender, ja::WidgetEvent action, void* data)
{
	if (sender == this->blendEquationScrollbar)
	{
		 uint32_t blendEquationId = (uint32_t)this->blendEquationScrollbar->getValue();
		 uint32_t* blendEquations = Material::getBlendEquations();

		 if (nullptr != this->editedMaterial)
		 {
			 this->editedMaterial->blendEquation = blendEquations[blendEquationId];
			 this->blendEquationButton->setText(this->editedMaterial->getBlendEquationName());
		 }
	}

	if (sender == this->sourceFactorScrollbar)
	{
		uint32_t  sourceFactorId  = (uint32_t)this->sourceFactorScrollbar->getValue();
		uint32_t* blendFactors    = Material::getBlendFactors();

		if (nullptr != this->editedMaterial)
		{
			this->editedMaterial->sourceFactor = blendFactors[sourceFactorId];
			this->sourceFactorButton->setText(this->editedMaterial->getSourceFactorName());
		}
	}

	if (sender == this->destinationFactorScrollbar)
	{
		uint32_t  destinationFactorId = (uint32_t)this->destinationFactorScrollbar->getValue();
		uint32_t* blendFactors        = Material::getBlendFactors();

		if (nullptr != this->editedMaterial)
		{
			this->editedMaterial->destinationFactor = blendFactors[destinationFactorId];
			this->destinationFactorButton->setText(this->editedMaterial->getDestinationFactorName());
		}
	}
}

void LayerObjectMaterialEditor::onDataChange(ja::DataNotifier* dataNotifier)
{

}

LayerObjectAddMenu::LayerObjectAddMenu(uint32_t widgetId, Widget* parent) : GridLayout(widgetId, parent, 1, 3, 0, 2)
{
	setSelectable(true);
	setSize(80, 3 * (20 + 2));

	polygonButton = new Button((uint32_t)LayerObjectType::POLYGON, nullptr);
	fillButton(polygonButton, "Polygon");
	polygonButton->setOnClickEvent(this, &LayerObjectAddMenu::createLayerObjectClick);
	addWidget(polygonButton, 0, 0);

	lightButton = new Button((uint32_t)LayerObjectType::LIGHT, nullptr);
	fillButton(lightButton, "Light");
	lightButton->setOnClickEvent(this, &LayerObjectAddMenu::createLayerObjectClick);
	addWidget(lightButton, 0, 1);

	cameraButton = new Button((uint32_t)LayerObjectType::CAMERA, nullptr);
	fillButton(cameraButton, "Camera");
	cameraButton->setOnClickEvent(this, &LayerObjectAddMenu::createLayerObjectClick);
	addWidget(cameraButton, 0, 2);

}

LayerObjectAddMenu::~LayerObjectAddMenu()
{

}

void LayerObjectAddMenu::createLayerObjectClick(Widget* sender, WidgetEvent action, void* data)
{
	LayerObjectEditor* pLayerObjectEditor = LayerObjectEditor::getSingleton();
	pLayerObjectEditor->setLayerObjectType((LayerObjectType)sender->getId());

	LayerObjectManager::getSingleton()->layerObjectDef.transform.position = pLayerObjectEditor->getGizmoPosition();
	pLayerObjectEditor->createLayerObject();
}

LayerObjectSpaceBarMenu::LayerObjectSpaceBarMenu(uint32_t widgetId, Widget* parent) : GridLayout(widgetId, parent, 1, 1, 0, 2)
{
	setSize(80, (20 + 2) * 1);

	addLayerObjectButton = new Button(0, nullptr);
	
	fillButton(addLayerObjectButton, "Add");
	
	LayerObjectAddMenu* addLayerObjectMenu  = new LayerObjectAddMenu(0, nullptr);
	SlideWidget*  addShapeSlide = new SlideWidget(1, addLayerObjectButton, addLayerObjectMenu, nullptr);
	addShapeSlide->setAlign((uint32_t)Margin::TOP | (uint32_t)Margin::RIGHT);
	
	addWidget(addShapeSlide, 0, 0);
}

LayerObjectSpaceBarMenu::~LayerObjectSpaceBarMenu()
{

}

void LayerObjectSpaceBarMenu::hide()
{
	this->setActive(false);
	this->setRender(false);
}

void LayerObjectSpaceBarMenu::show()
{
	this->setActive(true);
	this->setRender(true);

	UIManager::setSelected(this);
}

void LayerObjectSpaceBarMenu::update()
{
	Widget* lastSelected = UIManager::getSingleton()->getLastSelected();

	bool isSelected = false;

	if (lastSelected != nullptr)
	{
		Widget* itWidget = lastSelected;

		for (Widget* itWidget = lastSelected; itWidget != nullptr; itWidget = itWidget->getParent())
		{
			if (itWidget == this)
			{
				isSelected = true;
				break;
			}
		}
	}

	if (isSelected)
	{
		GridLayout::update();

		Widget* lastClicked = UIManager::getLastClicked();
		for (Widget* itWidget = lastClicked; itWidget != nullptr; itWidget = itWidget->getParent())
		{
			if (itWidget == this)
			{
				hide();
			}
		}
	}
	else {
		hide();
	}
}

}