#include "AnimationEditor.hpp"
#include "WidgetId.hpp"
#include "Toolset.hpp"

#include "../../Engine/Manager/GameManager.hpp"
#include "../../Engine/Gilgamesh/DebugDraw.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"
//#include "../../Engine/Manager/PolygonManager.hpp"
#include "../../Engine/Manager/AnimationManager.hpp"
#include "MainMenu.hpp"

namespace tr
{
	AnimationEditor* AnimationEditor::singleton = nullptr;

	AnimationEditor::AnimationEditor() : ja::Menu((uint32_t)TR_MENU_ID::ANIMATIONEDITOR, nullptr)
	{
		this->drawBoxSelection = false;
		this->drawHud = true;
		this->drawBoxSelection = false;
		this->drawPhysics = true;
		this->drawHashGrid = false;

		hud.setFont("default.ttf", 10, jaLeftLowerCorner, 255, 255, 255, 255);
		hud.setFont("default.ttf", 10, jaRightLowerCorner, 255, 255, 255, 255);
		hud.setFont("default.ttf", 10, jaLeftUpperCorner, 255, 255, 255, 255);
		hud.setFont("default.ttf", 10, jaRightUpperCorner, 255, 255, 255, 255);

		hud.addField(jaLeftUpperCorner, (uint32_t)AnimationEditorHud::ID_HUD, "Id=0");

		hud.addField(jaRightUpperCorner, (uint32_t)AnimationEditorHud::EDITOR_MODE_HUD, "Mode=");


		int32_t resolutionWidth, resolutionHeight;
		DisplayManager::getSingleton()->getResolution(&resolutionWidth, &resolutionHeight);
		setPosition(0, 0);
		setSize(resolutionWidth, resolutionHeight);
		setText("Animation Editor");

		this->skeletonDef = &AnimationManager::getSingleton()->skeletonDef;

		createGUI();
		setDefaultParameters();

		objectSpace = false;
		setEditorMode(AnimationEditorMode::SELECT_MODE);
	}

	AnimationEditor::~AnimationEditor() {
		//delete this->selectedPolygonsTemp;
	}

	void AnimationEditor::cleanUp()	{
		if (singleton != nullptr)
		{
			delete singleton;
			singleton = nullptr;
		}
	}

	void AnimationEditor::setDefaultParameters()
	{

		this->gizmo = jaVector2(0.0f, 0.0f);
	}

	AnimationEditor* AnimationEditor::getSingleton()
	{
		return singleton;
	}

	void AnimationEditor::init()
	{
		if (singleton == nullptr)
		{
			singleton = new AnimationEditor();
		}
	}

	void AnimationEditor::update()
	{
		Toolset::getSingleton()->update();
		Interface::update();

		if (animationBar->isSlided())
		{
			hud.setRender(false);
		}
		else
		{
			hud.setRender(this->drawHud);
		}

		bool isGuiActive = (UIManager::isMouseOverGui() || UIManager::isTextInputSelected());
		
		if (!isGuiActive)
		{
			if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
			{
				if (editMode == AnimationEditorMode::SELECT_MODE)
				{
					if (drawBoxSelection && !InputManager::isKeyModPress(JA_KMOD_LALT))
					{
						drawBoxSelection = false;
						AABB2d selectionAABB = AABB2d::make(selectionStartPoint, Toolset::getSingleton()->getDrawPoint());
						selectedSkeletonsTemp.clear();
				
						AnimationManager::getSingleton()->getSkeletonsAtAABB(selectionAABB, &selectedSkeletonsTemp);
						
						if (InputManager::isKeyModPress(JA_KMOD_LSHIFT) || InputManager::isKeyModPress(JA_KMOD_LCTRL))
						{
							if (InputManager::isKeyModPress(JA_KMOD_LSHIFT))
								addToSelection(&selectedSkeletonsTemp);
							else
								removeFromSelection(&selectedSkeletonsTemp);
						}
						else
						{
							selectedSkeletons.clear();
							addToSelection(&selectedSkeletonsTemp);
						}
						

						if (selectedSkeletons.size() != 0)
						{
							ja::Skeleton* skeleton = selectedSkeletons.back();
							updateComponentEditors(skeleton);
						}
						else
						{
							updateComponentEditors(nullptr);
						}
						
						calculateSelectionMidpoint();
					}
				}
			}

			if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
			{
				switch (editMode) {
				case AnimationEditorMode::SELECT_MODE:
				{
					selectionStartPoint = Toolset::getSingleton()->getDrawPoint();

					if (InputManager::isKeyModPress(JA_KMOD_LALT))
					{
						gizmo = Toolset::getSingleton()->getDrawPoint();
					}
					else
					{
						drawBoxSelection = true;
					}
				}
				break;
				case AnimationEditorMode::TRANSLATE_MODE:
					setEditorMode(AnimationEditorMode::SELECT_MODE);
					break;
				}
			}

		}

		if (!isGuiActive)
		{

			if (InputManager::isKeyPressed(JA_o)) {
				ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
				objectSpace = !objectSpace;
				if (objectSpace) {
					/*if (selectedPolygons.size() != 0) {
						ja::Polygon* polygon = selectedPolygons.back();
						ruler->setAngle(polygon->getAngleDeg());
						}*/
				}
				else
					ruler->setAngle(0);
			}

			if (InputManager::isKeyPressed(JA_l)) {
				jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
				ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
				Toolset::getSingleton()->setRulerActive(!Toolset::getSingleton()->isRulerActive());
				ruler->setPosition(drawPoint.x, drawPoint.y);
			}

			if (InputManager::isKeyPressed(JA_x))
			{
				jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
				ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
				ruler->setAlign(RulerAlign::ALIGN_X);
				ruler->setPosition(drawPoint.x, drawPoint.y);
			}

			if (InputManager::isKeyPressed(JA_y))
			{
				jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
				ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
				ruler->setAlign(RulerAlign::ALIGN_Y);
				ruler->setPosition(drawPoint.x, drawPoint.y);
			}

			if (InputManager::isKeyPressed(JA_a))
			{
				GameManager* gameManager = GameManager::getSingleton();
				gameManager->renderAABB = !gameManager->renderAABB;
			}

			if (InputManager::isKeyPressed(JA_b))
			{
				GameManager* gameManager = GameManager::getSingleton();
				gameManager->renderBounds = !gameManager->renderBounds;
			}

			if (InputManager::isKeyPressed(JA_p))
			{
				drawPhysics = !drawPhysics;
			}

			if (InputManager::isKeyPressed(JA_h))
			{
				this->drawHashGrid = !this->drawHashGrid;
			}

			if (InputManager::isKeyPressed(JA_SPACE))
			{
				UIManager* pUIManager = UIManager::getSingleton();
				spaceBarMenu->setPosition(pUIManager->mouseX, pUIManager->mouseY);
				spaceBarMenu->show();
			}

			if (editMode == AnimationEditorMode::SELECT_MODE)
			{
				if (InputManager::isKeyPressed(JA_c)) {
					if (InputManager::isKeyModPress(JA_KMOD_CTRL))
					{
						//if (selectedPolygons.size() != 0)
						//{
						//	ja::Polygon* polygon = selectedPolygons.back();
						//	polygon->fillPolygonDef(*polygonDef);
						//}
					}
					else
					{
						createSkeleton();
						setEditorMode(AnimationEditorMode::TRANSLATE_MODE);
					}
				}

				if (selectedSkeletons.size() != 0)
				{
					if (InputManager::isKeyPressed(JA_g))
					{
						saveSelectedSkeletonStates();
						setEditorMode(AnimationEditorMode::TRANSLATE_MODE);
					}
				}

				if (InputManager::isKeyPressed(JA_ESCAPE)) {
					Toolset::getSingleton()->setRulerActive(false);
					MainMenu::getSingleton()->setGameState(GameState::MAINMENU_MODE);
				}
			}

			if ((editMode == AnimationEditorMode::TRANSLATE_MODE) || (editMode == AnimationEditorMode::ROTATE_MODE) || (editMode == AnimationEditorMode::SCALE_MODE))
			{
				if (InputManager::isKeyPressed(JA_ESCAPE))
					setEditorMode(AnimationEditorMode::SELECT_MODE);
			}

			if (InputManager::isKeyModPress(JA_KMOD_SHIFT) && InputManager::isKeyPressed(JA_8)) {
				hud.setRender(!hud.isRendered());
			}
		}

		switch (editMode)
		{
		case AnimationEditorMode::SELECT_MODE:
		{
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
			skeletonDef->position = drawPoint;
		}
		break;
		case AnimationEditorMode::TRANSLATE_MODE:
			moveSkeleton();
			break;
		case AnimationEditorMode::COMPONENT_EDITOR_MODE:
			currentComponentEditor->handleInput();
			break;
		}

		oldDrawPointPosition = Toolset::getSingleton()->getDrawPoint();
		hud.update();
	}

	void AnimationEditor::drawSelectedSkeletons()
	{
		CameraInterface* activeCamera = GameManager::getSingleton()->getActiveCamera();
		jaVector2        originX(1.0f / activeCamera->scale.x, 0.0f);
		jaVector2        originY(0.0f, 1.0f / activeCamera->scale.y);
		
		if (selectedSkeletons.size() != 0) {

			std::list<ja::Skeleton*>::iterator itSkeleton = selectedSkeletons.begin();
	
			glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
			glPointSize(3.0f);
			
			glBegin(GL_POINTS);
			for (itSkeleton; itSkeleton != selectedSkeletons.end(); ++itSkeleton)
			{
				jaVector2 position = (*itSkeleton)->getPosition();
				glVertex2f(position.x, position.y);
			}
			glEnd();
		}
	}

	void AnimationEditor::drawGizmo()
	{
		DisplayManager::setColorUb(255, 255, 255);
		DisplayManager::setPointSize(7);
		DisplayManager::drawPoint(gizmo.x, gizmo.y);

		DisplayManager::setColorUb(255, 0, 0);
		DisplayManager::setPointSize(5);
		DisplayManager::drawPoint(gizmo.x, gizmo.y);
	}


	void AnimationEditor::draw()
	{
		CameraInterface* camera = GameManager::getSingleton()->getActiveCamera();
		DisplayManager::pushWorldMatrix();
		DisplayManager::clearWorldMatrix();
		DisplayManager::setCameraMatrix(camera);

		AnimationManager::getSingleton()->drawDebug(camera);

		if (drawBoxSelection) {
			DisplayManager::setColorUb(255, 0, 128, 255);
			DisplayManager::setLineWidth(3.0f);
			AABB2d boxSelection = AABB2d::make(selectionStartPoint, Toolset::getSingleton()->getDrawPoint());
			Debug::drawAABB2d(boxSelection);

			DisplayManager::setColorUb(255, 255, 255);
			DisplayManager::setPointSize(7);
			DisplayManager::drawPoint(selectionStartPoint.x, selectionStartPoint.y);

			DisplayManager::setColorUb(255, 0, 0);
			DisplayManager::setPointSize(5);
			DisplayManager::drawPoint(selectionStartPoint.x, selectionStartPoint.y);
		}

		drawSelectedSkeletons();

		//if (this->editMode == LayerObjectEditorMode::ROTATE_MODE) {
		//	DisplayManager::setColorUb(255, 255, 255);
		//	DisplayManager::setLineWidth(2.0f);
		//	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
		//	DisplayManager::drawLine(gizmo.x, gizmo.y, drawPoint.x, drawPoint.y);
		//}

		if (this->drawPhysics)
		{
			PhysicsManager::getSingleton()->drawUser = nullptr;
			DisplayManager::setLineWidth(1.0f);
			PhysicsManager::getSingleton()->drawDebug(camera);
		}

		//if (this->drawHashGrid)
		//{
		//	PolygonManager::getSingleton()->drawHashGrid(camera);
		//}

		drawGizmo();

		if (editMode == AnimationEditorMode::COMPONENT_EDITOR_MODE) {
			currentComponentEditor->render();
		}

		DisplayManager::popWorldMatrix();
		Toolset::getSingleton()->draw();
		Interface::draw();

		hud.draw();
	}

	void AnimationEditor::moveSkeleton()
	{
		jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

		std::list<ja::Skeleton*>::iterator itSkeleton = this->selectedSkeletons.begin();

		jaVector2 posDiff;

		for (itSkeleton; itSkeleton != this->selectedSkeletons.end(); ++itSkeleton)
		{	
			posDiff = (*itSkeleton)->getPosition() - gizmo;
			(*itSkeleton)->setPosition(drawPoint + posDiff);
			//AnimationManager::getSingleton()->updatePolygonImmediate(*itPolygon);
		}

		if (this->selectedSkeletons.size() > 0)
		{
			ja::Skeleton* skeleton = this->selectedSkeletons.back();
			//PolygonShapeEditor::getSingleton()->updateWidget(skeleton);
		}

		gizmo = drawPoint;
		Toolset::getSingleton()->worldHud->addInfoC((uint16_t)AnimationEditorInfo::TRANSLATE_INFO, gizmo.x, gizmo.y, 0, 255, 0, 5.0f, "(%.4f,%.4f)", gizmo.x, gizmo.y);
	}

	void AnimationEditor::createSkeleton()
	{
		this->selectedSkeletons.clear();

		ja::Skeleton* skeleton = ja::AnimationManager::getSingleton()->createSkeletonDef();

		selectedSkeletons.push_back(skeleton);
		calculateSelectionMidpoint();
		updateComponentEditors(skeleton);
	}

	void AnimationEditor::setEditorMode(AnimationEditorMode mode)
	{
		editMode = mode;

		switch (editMode)
		{
		case AnimationEditorMode::SELECT_MODE:
			hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_MODE_HUD, "Mode=Select Skeleton");
			hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_HELPER_HUD, "(LShift) + (LCtrl) + LMB - Select\nAlt + LMB - Change gizmo position\nC - Create polygon\nTab - Edit mode\nS - Scale\nR - Rotate\nD - Delete polygon\nCtrl + C - Copy\nCtrl + D - Duplicate\nESC - Cancel");
			break;
		case AnimationEditorMode::TRANSLATE_MODE:
			hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=Translate Skeleton");
			hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "LMB - Accept\nESC - Cancel");
			break;
		}
	}

	void AnimationEditor::setEditorMode(CommonWidget* commonWidget)
	{
		if (editMode == AnimationEditorMode::COMPONENT_EDITOR_MODE)
		{
			currentComponentEditor->setActiveMode(false);
			setEditorMode(AnimationEditorMode::SELECT_MODE);
			return;
		}

		editMode = AnimationEditorMode::COMPONENT_EDITOR_MODE;
		currentComponentEditor = commonWidget;
		currentComponentEditor->setActiveMode(true);
	}

	AnimationEditorMode AnimationEditor::getEditorMode()
	{
		return this->editMode;
	}

	CommonWidget* AnimationEditor::getCurrentEditor()
	{
		if (editMode == AnimationEditorMode::COMPONENT_EDITOR_MODE)
		{
			return this->currentComponentEditor;
		}

		return nullptr;
	}

	jaVector2 AnimationEditor::getGizmoPosition()
	{
		return gizmo;
	}

	void AnimationEditor::createGUI()
	{
		this->animationBar  = new SlideBox(0);
		this->animationMenu = new AnimationComponentMenu(1, this);

		this->animationBar->dockTo((uint32_t)Margin::TOP);

		FlowLayout* menuBar = new FlowLayout(0, nullptr, (uint32_t)Margin::LEFT, 5, 0);
		menuBar->setColor(128, 128, 128, 128);
		menuBar->setSize(width, 28);
		menuBar->setPosition(0, height - 28);

		Button* menuButton = new Button(1, nullptr);
		menuButton->setFont("default.ttf",10);
		menuButton->setText("Menu");
		menuButton->setColor(0, 0, 255, 255);
		menuButton->setFontColor(255, 255, 255, 255);
		menuButton->adjustToText(10, 28);
		menuButton->vUserData = this->animationMenu;
		menuButton->setOnClickEvent(this, &AnimationEditor::onClickMenuBar);
		menuBar->addWidget(menuButton);

		spaceBarMenu = new AnimationSpaceBarMenu(3, nullptr);
		spaceBarMenu->hide();

		this->animationBar->setSlideArea(10);
		this->animationBar->setArea(28);
		this->animationBar->setBox(menuBar);

		addWidget(this->animationMenu);
		addWidget(this->animationBar);
	}

	void AnimationEditor::calculateSelectionMidpoint()
	{
		std::list<ja::Skeleton*>::iterator itSkeleton = selectedSkeletons.begin();
		gizmo.setZero();

		for (itSkeleton; itSkeleton != selectedSkeletons.end(); ++itSkeleton)
		{
			//ObjectHandle2d* objectHandle = (*itSkeleton)->getPolygonHandle();
			gizmo += (*itSkeleton)->getPosition(); //objectHandle->transform.position;
		}

		gizmo = gizmo / (jaFloat)(selectedSkeletons.size());
	}

	void AnimationEditor::addToSelection(StackAllocator<ja::Skeleton*>* selection)
	{
		ja::Skeleton** skeletons = selection->getFirst();

		for (uint32_t i = 0; i<selection->getSize(); ++i)
		{
			selectedSkeletons.remove(skeletons[i]);
			selectedSkeletons.push_back(skeletons[i]);
		}
	}

	void AnimationEditor::updateComponentEditors(ja::Skeleton* skeleton)
	{
		if (nullptr != skeleton)
			hud.setFieldValuePrintC((uint32_t)AnimationEditorHud::ID_HUD, "Id=%hu", skeleton->getId());
		else
			hud.setFieldValue((uint32_t)AnimationEditorHud::ID_HUD, "Id=");

		animationMenu->updateComponentEditors(skeleton);
	}

	void AnimationEditor::removeFromSelection(StackAllocator<ja::Skeleton*>* selection)
	{
		ja::Skeleton** skeletons = selection->getFirst();

		for (uint32_t i = 0; i<selection->getSize(); ++i)
		{
			selectedSkeletons.remove(skeletons[i]);
		}
	}

	void AnimationEditor::saveSelectedSkeletonStates()
	{
		std::list<ja::Skeleton*>::iterator itSelection = selectedSkeletons.begin();
		selectedSkeletonStates.clear();

		for (itSelection; itSelection != selectedSkeletons.end(); ++itSelection)
		{
			SkeletonState skeletonState;
			ja::Skeleton* skeleton = *itSelection;

			skeletonState.position = skeleton->getPosition();

			selectedSkeletonStates.push_back(skeletonState);
		}
	}

	void AnimationEditor::onClickMenuBar(Widget* sender, WidgetEvent action, void* data)
	{
		Button* menuButton = (Button*)(sender);

		Widget* linkedWidget = (Widget*)(menuButton->vUserData);
		linkedWidget->setRender(true);
		linkedWidget->setActive(true);
	}

}