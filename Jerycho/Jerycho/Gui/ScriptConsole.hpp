#pragma once

#include "../../Engine/Gui/Menu.hpp"
#include "../../Engine/Gui/Editbox.hpp"
#include <sstream>
#include <string>

namespace ja
{

	class ScriptConsole : public Menu
	{
	private:
		bool isActive;
		Editbox* consoleLine;
		ja::string lastCommand;

		static ScriptConsole* singleton;
		ScriptConsole();
	public:
		static void init();
		static ScriptConsole* getSingleton();
		static void cleanUp();
		void activeConsole(bool active);
		void update();
		void draw();
		~ScriptConsole();
	};

}