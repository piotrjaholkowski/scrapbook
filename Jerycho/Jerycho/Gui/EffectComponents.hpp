#pragma once

#include "CommonWidgets.hpp"

namespace tr
{

	class EffectComponentMenu : public CommonWidget
	{
	private:
		CommonWidget* components[20];
	public:
		EffectComponentMenu(uint32_t widgetId, ja::Widget* parent);
		void onMenuClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

}