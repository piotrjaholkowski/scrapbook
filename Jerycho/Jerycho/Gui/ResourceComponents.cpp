#include "ResourceComponents.hpp"
#include "Toolset.hpp"


#include "LayerObjectEditor.hpp"
#include "PhysicsEditor.hpp"
#include "EntityEditor.hpp"
#include "ResourceEditor.hpp"

#include "../../Engine/Scripts/PhysicObject.hpp"
#include "../../Engine/Scripts/LayerObject.hpp"
#include "../../Engine/Scripts/Entity.hpp"

#include "../../Engine/Gui/Margin.hpp"
#include "../../Engine/Gui/Editbox.hpp"
#include "../../Engine/Gui/ProgressButton.hpp"
#include "../../Engine/Gui/GridLayout.hpp"
#include "../../Engine/Gui/FlowLayout.hpp"
#include "../../Engine/Gui/TextField.hpp"
#include "../../Engine/Gui/BoxLayout.hpp"
#include "../../Engine/Gui/Scrollbar.hpp"
#include "../../Engine/Gui/Checkbox.hpp"
#include "../../Engine/Gui/RadioButton.hpp"
#include "../../Engine/Gui/GridViewer.hpp"
#include "../../Engine/Gui/SlideWidget.hpp"
#include "../../Engine/Gui/SequenceEditor.hpp"
#include "../../Engine/Gui/ScrollPane.hpp"
#include "../../Engine/Gui/Hider.hpp"
#include "../../Engine/Gui/Magnet.hpp"

#include "../../Engine/Gilgamesh/DebugDraw.hpp"

using namespace ja;

namespace tr
{
	ResourceComponentMenu*  ResourceComponentMenu::singleton = nullptr;
	ResourceSceneEditor*    ResourceSceneEditor::singleton   = nullptr;
	ResourceExporterEditor* ResourceExporterEditor::singleton = nullptr;

	ResourceComponentMenu::ResourceComponentMenu(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Menu", parent)
	{
		singleton = this;

		BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 1);
		boxLayout->setSize(150, 250);
		fillBoxLayout(boxLayout);

		Button* sceneButton = new Button((int32_t)TR_RESOURCEEDITOR_ID::SCENE_EDITOR, nullptr);
		fillButton(sceneButton, "Scene");
		sceneButton->setOnClickEvent(this, &ResourceComponentMenu::onMenuClick);
		boxLayout->addWidget(sceneButton);

		Button* exporterButton = new Button((int32_t)TR_RESOURCEEDITOR_ID::EXPORTER_EDITOR, nullptr);
		fillButton(exporterButton, "Exporter");
		exporterButton->setOnClickEvent(this, &ResourceComponentMenu::onMenuClick);
		boxLayout->addWidget(exporterButton);

		components[(int32_t)TR_RESOURCEEDITOR_ID::SCENE_EDITOR] = new ResourceSceneEditor((uint32_t)TR_RESOURCEEDITOR_ID::SCENE_EDITOR, this);
		addWidget(components[(int32_t)TR_RESOURCEEDITOR_ID::SCENE_EDITOR]);

		components[(int32_t)TR_RESOURCEEDITOR_ID::EXPORTER_EDITOR] = new ResourceExporterEditor((uint32_t)TR_RESOURCEEDITOR_ID::EXPORTER_EDITOR, this);
		addWidget(components[(int32_t)TR_RESOURCEEDITOR_ID::EXPORTER_EDITOR]);

		addWidgetToEditor(boxLayout);
	}

	void ResourceComponentMenu::onMenuClick(Widget* sender, WidgetEvent action, void* data)
	{
		uint32_t id = sender->getId();
		components[id]->setActive(true);
		components[id]->setRender(true);
	}

	void ResourceComponentMenu::updateComponentEditors(void* resource)
	{
		components[(int32_t)TR_RESOURCEEDITOR_ID::EXPORTER_EDITOR]->updateWidget(nullptr);
	}

	ResourceSceneEditor::ResourceSceneEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Scene", parent)
	{
		singleton = this;
		setRender(false);
		setActive(false);

		BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);

		const uint32_t widgetWidth = 300;
		boxLayout->setSize(widgetWidth, 2 * (28 + 2));
		fillBoxLayout(boxLayout);

		{
			FlowLayout* nameLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			nameLayout->setSize(widgetWidth, 28);
			fillFlowLayout(nameLayout);

			Label* nameLabel = new Label(0, nullptr);
			fillLabel(nameLabel, "Name");
			nameLayout->addWidget(nameLabel);

			this->sceneNameTextfield = new TextField(1, nullptr);
			fillTextField(this->sceneNameTextfield);
			this->sceneNameTextfield->setTextSize(28, 28);

			this->saveButton = new Button(2, nullptr);
			fillButton(this->saveButton, "Save");
			this->saveButton->setActive(true);

			nameLayout->addWidget(this->sceneNameTextfield);
			boxLayout->addWidget(nameLayout);
		}

		addWidgetToEditor(boxLayout);
	}

	void ResourceSceneEditor::updateWidget(void* data)
	{
	
	}

	ResourceExporterEditor::ResourceExporterEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Exporter", parent)
	{
		singleton = this;
		setRender(false);
		setActive(false);

		BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);

		const uint32_t widgetWidth = 320;
		boxLayout->setSize(widgetWidth, (7 * (28 + 2)) + (3 * (28 + 2)));
		fillBoxLayout(boxLayout);

		{
			FlowLayout* nameLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			nameLayout->setSize(widgetWidth, 28);
			fillFlowLayout(nameLayout);

			Label* nameLabel = new Label(0, nullptr);
			fillLabel(nameLabel, "Filename");
			nameLayout->addWidget(nameLabel);

			this->fileNameTextfield = new TextField(1, nullptr);
			fillTextField(this->fileNameTextfield);
			this->fileNameTextfield->setTextSize(28, 28);

			this->exportButton = new Button(2, nullptr);
			fillButton(this->exportButton,"Export");
			this->exportButton->setOnClickEvent(this, &ResourceExporterEditor::onExportClick);
			this->exportButton->setActive(true);

			nameLayout->addWidget(this->fileNameTextfield);
			nameLayout->addWidget(this->exportButton);
			boxLayout->addWidget(nameLayout);
		}

		{
			FlowLayout* selectedLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			selectedLayout->setSize(widgetWidth, 28);
			fillFlowLayout(selectedLayout);

			Label* selectedLabel = new Label(0, nullptr);
			fillLabel(selectedLabel, "Selected");

			selectedLayout->addWidget(selectedLabel);
			boxLayout->addWidget(selectedLayout);
		}

		{
			this->selectedResourceGridViewer = new GridViewer(3, 1, 7, nullptr);
			this->selectedResourceGridViewer->setSize(widgetWidth, 7 * 28);
			fillGridViewer(this->selectedResourceGridViewer);

			this->selectedResourceGridViewer->setOnDrawEvent(this, &ResourceExporterEditor::onDrawResource);
			//this->selectedResourceGridViewer->setOnClickEvent(this, &AnimationSkeletonEditor::onPickBone);
			this->selectedResourceGridViewer->setGridList(&this->selectedResourcesGridList);

			this->selectedResourceGridViewer->setActive(false);

			boxLayout->addWidget(this->selectedResourceGridViewer);
		}

		{
			FlowLayout* relatedLayout = new FlowLayout(2, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
			relatedLayout->setSize(widgetWidth, 28);
			fillFlowLayout(relatedLayout);

			Label* exportRelatedLabel = new Label(0, nullptr);
			fillLabel(exportRelatedLabel, "Export related");
			relatedLayout->addWidget(exportRelatedLabel);

			this->exportRelatedCheckbox = new Checkbox(1, nullptr);
			fillCheckbox(this->exportRelatedCheckbox);
			relatedLayout->addWidget(this->exportRelatedCheckbox);

			boxLayout->addWidget(relatedLayout);
		}

		{
			resourceLabel.setFont("default.ttf",10);
			resourceLabel.setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
			resourceLabel.setColor(255, 255, 255, 255);
			resourceLabel.setText("");
		}

		addWidgetToEditor(boxLayout);
	}

	void ResourceExporterEditor::updateWidget(void* data)
	{
		this->selectedResources.clear();
		this->selectedResourcesGridList.clear();

		{
			//adding layer objects to list
			std::list<LayerObject*>& selectedLayerObjects = LayerObjectEditor::getSingleton()->selectedLayerObjects;
			std::list<LayerObject*>::iterator itLayerObject = selectedLayerObjects.begin();

			for (itLayerObject; itLayerObject != selectedLayerObjects.end(); ++itLayerObject)
			{
				this->selectedResources.push( ResourceEntry((uint8_t)ResourceType::LAYER_OBJECT, (void*)(*itLayerObject)) );
			}
		}

		{
			//adding layer objects to list
			std::list<PhysicObject2d*>& selectedPhysicObjects = PhysicsEditor::getSingleton()->selectedPhysicObjects;
			std::list<PhysicObject2d*>::iterator itPhysicObject = selectedPhysicObjects.begin();

			for (itPhysicObject; itPhysicObject != selectedPhysicObjects.end(); ++itPhysicObject)
			{
				this->selectedResources.push(ResourceEntry((uint8_t)ResourceType::PHYSIC_OBJECT, (void*)(*itPhysicObject)));
			}
		}

		{
			//adding layer objects to list
			std::list<Entity*>& selectedEntities  = EntityEditor::getSingleton()->selectedEntities;
			std::list<Entity*>::iterator itEntity = selectedEntities.begin();

			for (itEntity; itEntity != selectedEntities.end(); ++itEntity)
			{
				this->selectedResources.push(ResourceEntry((uint8_t)ResourceType::ENTITY, (void*)(*itEntity)));
			}
		}

		{
			//copy to grid list
			ResourceEntry* pSelectedResources   = selectedResources.getFirst();
			uint32_t       selectedResourcesNum = selectedResources.getSize();

			for (uint32_t i = 0; i < selectedResourcesNum; ++i)
			{
				this->selectedResourcesGridList.push_back(&pSelectedResources[i]);
			}
		}
		
	}

	void ResourceExporterEditor::onDrawResource(Widget* sender, WidgetEvent action, void* data)
	{
		ResourceEntry* resourceEntry = (ResourceEntry*)data;

		int32_t x, y;

		int32_t halfWidth, halfHeight;
		float   fX, fY;

		sender->getPosition(x, y);
		fX = (float)(x);
		fY = (float)(y);

		halfWidth = (sender->getWidth() / 2);
		halfHeight = (sender->getHeight() / 2);

		Button* button = (Button*)sender;

		resourceLabel.x = x + halfWidth;
		resourceLabel.y = y + halfHeight;

		switch (resourceEntry->resourceType)
		{
			case (uint8_t)ResourceType::LAYER_OBJECT:
			{
				LayerObject* layerObject = (LayerObject*)resourceEntry->resourceData;
				resourceLabel.setTextPrintC("Layer Object %d", (int32_t)layerObject->getId());
			}
			break;

			case (uint8_t)ResourceType::PHYSIC_OBJECT:
			{
				PhysicObject2d* physicObject = (PhysicObject2d*)resourceEntry->resourceData;
				resourceLabel.setTextPrintC("Physic Object %d", (int32_t)physicObject->getId());
			}
			break;

			case (uint8_t)ResourceType::ENTITY:
			{
				Entity* entity = (Entity*)resourceEntry->resourceData;
				resourceLabel.setTextPrintC("Entity %d",(int32_t)entity->getId());
			}
			break;
		}
		
		
		resourceLabel.draw(0, 0);
	}

	void ResourceExporterEditor::onExportClick(Widget* sender, WidgetEvent action, void* data)
	{
		const char* fileName = this->fileNameTextfield->getTextPtr();

		if ('\0' == *fileName)
		{
			Toolset::getSingleton()->infoHud->addInfo(255, 0, 0, "Filename textfield is blank");
			
			UIManager::setSelected(this->fileNameTextfield);
			return;
		}

		std::string fileNameExt(fileName);
		fileNameExt.append(".lua");

		FILE * pFile = nullptr;
		pFile = fopen(fileNameExt.c_str(), "w");

		const char* physicObjecVar = "physicObjectDef";
		const char* layerObjecVar  = "layerObjectDef";
		const char* entityVar      = "entity";

		HashMap<uint32_t> scriptResources;

		//function renderBlackWhite
		fprintf(pFile, "function init%s(x,y)\n", fileName);
		fprintf(pFile, "--Physics Objects\n");
		fprintf(pFile, "local physicObjects = {};\n");
		fprintf(pFile, "local multiShape = {};\n");
		fprintf(pFile, "local %s = PhysicsManager.getPhysicObjectDef();\n", physicObjecVar);

		//jaVector2 worldPosition(0.0f, 0.0f);
		const jaVector2 worldPosition = ResourceEditor::getSingleton()->getGizmoPosition();

		ResourceEntry* resources    = this->selectedResources.getFirst();
		uint32_t       resourcesNum = this->selectedResources.getSize();

		{
			//Save physics objects
			for (uint32_t i = 0; i < resourcesNum; ++i)
			{
				if ((uint8_t)ResourceType::PHYSIC_OBJECT != resources[i].resourceType)
					continue;

				fprintf(pFile, "--Physic object %d\n", i);
				jas::PhysicObject::save(true, pFile, physicObjecVar, worldPosition, (PhysicObject2d*)resources[i].resourceData);
			}
		}
		
		{
			//Save layer objects
			fprintf(pFile, "--Layer Objects\n");
			fprintf(pFile, "local layerObjects = {};\n");
			fprintf(pFile, "local %s = LayerObjectManager.getLayerObjectDef();\n", layerObjecVar);
			for (uint32_t i = 0; i < resourcesNum; ++i)
			{
				if ((uint8_t)ResourceType::LAYER_OBJECT != resources[i].resourceType)
					continue;

				jas::LayerObject::save(true, pFile, layerObjecVar, worldPosition, (LayerObject*)resources[i].resourceData);
			}
		}
		
		{
			//Save layer objects
			fprintf(pFile, "--Entities\n");
			for (uint32_t i = 0; i < resourcesNum; ++i)
			{
				if ((uint8_t)ResourceType::ENTITY != resources[i].resourceType)
					continue;

				Entity* entity = (Entity*)resources[i].resourceData;
				jas::Entity::save(true, pFile, entityVar, worldPosition, entity);

				{
					ja::EntityComponent* itComponent = entity->getFirstComponent();
					for (itComponent; nullptr != itComponent; itComponent = itComponent->getNextComponent())
					{
						switch (itComponent->getComponentType())
						{
							case (uint8_t)ja::EntityGroupType::SCRIPT:
							{
								ScriptComponent* scriptComponent = (ScriptComponent*)itComponent;
								uint32_t resourceId = scriptComponent->getUpdateFileHash();

								scriptResources.add(resourceId, &resourceId);
							}
							break;

						}
					}
				}
			}
		}	

		fprintf(pFile, "end\n");

		ResourceManager* resourceManager = ResourceManager::getSingleton();
		//Export resources here
		{
			HashEntry<uint32_t>* scriptHashes = scriptResources.getFirst();
			uint32_t             scriptsNum   = scriptResources.getSize();

			fprintf(pFile, "--Script files\n");
			for (uint32_t i = 0; i < scriptsNum; ++i)
			{
				Resource<Script>* resource = resourceManager->getScript(scriptHashes[i].hash);

				if (nullptr != resource)
				{
					ja::string moduleName = resource->resourceName.substr(0, resource->resourceName.length() - 4);
					fprintf(pFile, "require \"%s%s\"\n",resourceManager->getScriptsPath(), moduleName.c_str());
				}
			}
		}
		

		fprintf(pFile, "local worldPoint = Toolset.getDrawPoint();\n");
		fprintf(pFile, "init%s(Vector2.getX(worldPoint), Vector2.getY(worldPoint));\n",fileName);

		fclose(pFile);

		Toolset::getSingleton()->infoHud->addInfoC(0, 255, 0, "Exported %s file", fileNameExt.c_str());
	}

	ResourceSpaceBarMenu::ResourceSpaceBarMenu(uint32_t widgetId, Widget* parent) : GridLayout(widgetId, parent, 1, 3, 0, 2)
	{
		setSize(80, 200);

		//addShapeButton = new Button(0, nullptr);
		//
		//fillButton(addShapeButton, "Add");
		//
		//ShapeAddMenu* addShapeMenu  = new ShapeAddMenu(0, nullptr);
		//SlideWidget*  addShapeSlide = new SlideWidget(1, addShapeButton, addShapeMenu, nullptr);
		//addShapeSlide->setAlign((uint32_t)Margin::TOP | (uint32_t)Margin::RIGHT);
		//
		//addWidget(addShapeSlide, 0, 0);
	}

	ResourceSpaceBarMenu::~ResourceSpaceBarMenu()
	{

	}

	void ResourceSpaceBarMenu::hide()
	{
		this->setActive(false);
		this->setRender(false);
	}

	void ResourceSpaceBarMenu::show()
	{
		this->setActive(true);
		this->setRender(true);

		UIManager::setSelected(this);
	}

	void ResourceSpaceBarMenu::update()
	{
		Widget* lastSelected = UIManager::getSingleton()->getLastSelected();

		bool isSelected = false;

		if (lastSelected != nullptr)
		{
			Widget* itWidget = lastSelected;

			for (Widget* itWidget = lastSelected; itWidget != nullptr; itWidget = itWidget->getParent())
			{
				if (itWidget == this)
				{
					isSelected = true;
					break;
				}
			}
		}

		if (isSelected)
		{
			GridLayout::update();

			Widget* lastClicked = UIManager::getLastClicked();
			for (Widget* itWidget = lastClicked; itWidget != nullptr; itWidget = itWidget->getParent())
			{
				if (itWidget == this)
				{
					hide();
				}
			}
		}
		else {
			hide();
		}
	}
}