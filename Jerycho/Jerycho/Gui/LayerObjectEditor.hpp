#pragma once

#include "../../Engine/Gui/DebugHud.hpp"
#include "../../Engine/Gui/Menu.hpp"
#include "../../Engine/Gui/Widget.hpp"

#include "../../Engine/Gui/SlideBox.hpp"
#include "../../Engine/Math/VectorMath2d.hpp"
#include "../../Engine/Gilgamesh/ObjectHandle2d.hpp"
#include "../../Engine/Allocators/StackAllocator.hpp"
#include "../../Engine/Graphics/Polygon.hpp"
#include "../../Engine/Graphics/LayerObjectDef.hpp"
#include "LayerObjectsComponents.hpp"
#include <vector>

#include <stdint.h>

namespace tr
{

	enum class LayerObjectEditorMode
	{
		SELECT_MODE           = 0,
		TRANSLATE_MODE        = 1,
		ROTATE_MODE           = 2,
		SCALE_MODE            = 3,
		COMPONENT_EDITOR_MODE = 4
	};

	enum class LayerObjectEditorInfo
	{
		TRANSLATE_INFO      = 0,
		ROTATE_INFO         = 1,
		SCALE_INFO          = 2,
		TEX_SCALE_INFO      = 3,
		DELETE_INFO         = 4,
		LAYER_INFO          = 5,
		COMPONENT_EDIT_INFO = 6
	};

	enum class LayerObjectEditorHud
	{
		ID_HUD                   = 0,
		ANGLE_HUD                = 1,
		POSITION_HUD             = 2,
		SCALE_HUD                = 3,
		LAYER_HUD                = 4,
		EDITOR_MODE_HUD          = 5,
		EDITOR_HELPER_HUD        = 6,
		DEPTH_HUD                = 7,
		LAYER_OBJECT_TYPE_HUD    = 8,
		EDITOR_STACIC_HELPER_HUD = 9
	};

	class LayerObjectState
	{
	public:
		jaVector2 position;
		jaMatrix2 rotMatrix;
		jaVector2 scale;
	};

	typedef void(*LayerObjectFilterPtr)(ja::LayerObject*);

	class LayerObjectEditor : public ja::Menu
	{
	private:
		ja::LayerObjectDef* layerObjectDef;

		//Gui
		bool objectSpace;

		int32_t mousePosX, mousePosY;

		LayerObjectEditorMode editMode;
		jaVector2 gizmo;
		jaVector2 oldDrawPointPosition;

		bool      drawBoxSelection;
		bool      drawPhysics;
		bool      drawHud;
		bool      drawHashGrid;
		ScaleMode scaleAxis;
		jaVector2 selectionStartPoint;
		jaVector2 transformationStart;

		LayerObjectComponentMenu* layerObjectMenu;
		LayerObjectSpaceBarMenu*  spaceBarMenu;

		ja::SlideBox* layerObjectBar;
		CommonWidget* currentComponentEditor;

		StackAllocator<gil::ObjectHandle2d*>* selectedLayerObjectsTemp;
	public:
		std::list<ja::LayerObject*>   selectedLayerObjects;
		std::vector<LayerObjectState> selectedLayerObjectsStates;

		ja::DebugHud hud;
	private:

		static LayerObjectEditor* singleton;

	public:
		LayerObjectEditor();

		void gatherResources();
		void createGUI();
		void calculateSelectionMidpoint();

		void drawGizmo();
		void addToSelection(StackAllocator<gil::ObjectHandle2d*>* selection);

		void updateComponentEditors(ja::LayerObject* layerObject);
		void onCreateNotifyComponentEditors(ja::LayerObject* layerObject);
		void onDeleteNotifyComponentEditors(ja::LayerObject* layerObject);
		void removeFromSelection(StackAllocator<gil::ObjectHandle2d*>* selection);		

		void addToSelection(ja::LayerObject* layerObject);
		void removeFromSelection(ja::LayerObject* layerObject);

		static void init();
		static LayerObjectEditor* getSingleton();
		static void cleanUp();

		//Events
		//void onChangeTextures(Widget* sender, WidgetEvent action, void* data);

		void setDefaultParameters();
		void setEditorMode(LayerObjectEditorMode mode);
		void setEditorMode(CommonWidget* commonWidget);
		LayerObjectEditorMode getEditorMode();
		CommonWidget*         getCurrentEditor();

		jaVector2           getGizmoPosition();
		ja::LayerObjectDef* getLayerObjectDef();

		void moveLayerObject();
		void rotateLayerObject();
		void scaleLayerObject();
		void duplicateSelectedAtPoint(jaVector2& point);

		void setLayerObjectType(ja::LayerObjectType layerObjectType);
		void createLayerObject();
		void saveSelectedLayerObjectsStates();
		void applyFilterToSelection(LayerObjectFilterPtr polygonFilter);
		//filters
		static void deleteLayerObjectFilter(ja::LayerObject* layerObject);
		static void nextLayerFilter(ja::LayerObject* layerObject);
		static void previousLayerFilter(ja::LayerObject* layerObject);

		void update();
		void drawSelectedLayerObjects();
		void draw();

		void onClickMenuBar(ja::Widget* sender, ja::WidgetEvent action, void* data);

		//gui
		void onMouseOver(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onMouseLeave(ja::Widget* sender, ja::WidgetEvent action, void* data);
		//gui

		~LayerObjectEditor();
	};

}