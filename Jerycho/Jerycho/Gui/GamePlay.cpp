#include "GamePlay.hpp"
#include "WidgetId.hpp"

#include "MainMenu.hpp"
#include "../../Engine/Manager/InputManager.hpp"
#include "../../Engine/Manager/EntityManager.hpp"

using namespace tr;

GamePlay::GamePlay() : Menu((uint32_t)TR_MENU_ID::GAMEPLAY, nullptr)
{
	hud.setFont("lucon.ttf", 12, jaLeftLowerCorner,255,255,255,255);
	hud.setFont("lucon.ttf", 12, jaRightLowerCorner,255,255,255,255);
	hud.setFont("lucon.ttf", 12, jaLeftUpperCorner,255,255,255,255);
	hud.setFont("lucon.ttf", 12, jaRightUpperCorner,255,255,255,255);
}

GamePlay::~GamePlay()
{

}

void GamePlay::init()
{
	if(singleton == nullptr)
	{
		singleton = new GamePlay();
	}
}

GamePlay* GamePlay::getSingleton()
{
	return singleton;
}

void GamePlay::cleanUp()
{
	if(singleton != nullptr)
	{
		delete singleton;
	}
}

void GamePlay::update()
{
	Interface::update();

	updatePlayerControl();

	if(InputManager::isKeyPressed(JA_ESCAPE))
	{
		MainMenu::getSingleton()->setGameState(GameState::MAINMENU_MODE);
	}


	hud.update();
}

void GamePlay::updatePlayerControl()
{

}

void GamePlay::draw()
{
	Interface::draw();

	hud.draw();
}

GamePlay* GamePlay::singleton = nullptr;
