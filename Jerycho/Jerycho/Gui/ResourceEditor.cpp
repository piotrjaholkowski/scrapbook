#include "ResourceEditor.hpp"
#include "WidgetId.hpp"
#include "Toolset.hpp"

#include "../../Engine/Manager/GameManager.hpp"
#include "../../Engine/Gilgamesh/DebugDraw.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"
#include "../../Engine/Manager/ResourceManager.hpp"
#include "MainMenu.hpp"

namespace tr
{
	ResourceEditor* ResourceEditor::singleton = nullptr;

	ResourceEditor::ResourceEditor() : ja::Menu((uint32_t)TR_MENU_ID::RESOURCEEDITOR, nullptr)
	{
		this->drawBoxSelection = false;
		this->drawHud          = true;
		this->drawBoxSelection = false;
		this->drawPhysics      = true;
		this->drawHashGrid     = false;

		hud.setFont("default.ttf", 10, jaLeftLowerCorner, 255, 255, 255, 255);
		hud.setFont("default.ttf", 10, jaRightLowerCorner, 255, 255, 255, 255);
		hud.setFont("default.ttf", 10, jaLeftUpperCorner, 255, 255, 255, 255);
		hud.setFont("default.ttf", 10, jaRightUpperCorner, 255, 255, 255, 255);

		hud.addField(jaRightUpperCorner, (uint32_t)ResourceEditorHud::EDITOR_MODE_HUD, "Mode=");

		hud.addField(jaRightLowerCorner, (uint32_t)ResourceEditorHud::EDITOR_HELPER_HUD, "");

		int32_t resolutionWidth, resolutionHeight;
		DisplayManager::getSingleton()->getResolution(&resolutionWidth, &resolutionHeight);
		setPosition(0, 0);
		setSize(resolutionWidth, resolutionHeight);
		setText("Resource Editor");

		createGUI();
		setDefaultParameters();

		setEditorMode(ResourceEditorMode::SELECT_MODE);
	}

	ResourceEditor::~ResourceEditor() {

	}

	void ResourceEditor::cleanUp()	{
		if (singleton != nullptr)
		{
			delete singleton;
			singleton = nullptr;
		}
	}

	void ResourceEditor::setDefaultParameters()
	{
		this->gizmo = jaVector2(0.0f, 0.0f);
	}

	ResourceEditor* ResourceEditor::getSingleton()
	{
		return singleton;
	}

	void ResourceEditor::init()
	{
		if (singleton == nullptr)
		{
			singleton = new ResourceEditor();
		}
	}

	void ResourceEditor::update()
	{
		Toolset::getSingleton()->update();
		Interface::update();

		if (resourceBar->isSlided())
		{
			hud.setRender(false);
		}
		else
		{
			hud.setRender(this->drawHud);
		}

		bool isGuiActive = (UIManager::isMouseOverGui() || UIManager::isTextInputSelected());

		if (!isGuiActive)
		{
			if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
			{
				switch (editMode) {
				case ResourceEditorMode::SELECT_MODE:
				{
					selectionStartPoint = Toolset::getSingleton()->getDrawPoint();

					if (InputManager::isKeyModPress(JA_KMOD_LALT))
					{
						gizmo = Toolset::getSingleton()->getDrawPoint();
					}
					//else
					//{
					//	drawBoxSelection = true;
					//}
				}
				break;
				}
			}

		}

		if (!isGuiActive)
		{
			if (InputManager::isKeyPressed(JA_a))
			{
				GameManager* gameManager = GameManager::getSingleton();
				gameManager->renderAABB = !gameManager->renderAABB;
			}

			if (InputManager::isKeyPressed(JA_b))
			{
				GameManager* gameManager = GameManager::getSingleton();
				gameManager->renderBounds = !gameManager->renderBounds;
			}

			if (InputManager::isKeyPressed(JA_p))
			{
				drawPhysics = !drawPhysics;
			}

			if (InputManager::isKeyPressed(JA_h))
			{
				this->drawHashGrid = !this->drawHashGrid;
			}

			if (InputManager::isKeyPressed(JA_SPACE))
			{
				UIManager* pUIManager = UIManager::getSingleton();
				spaceBarMenu->setPosition(pUIManager->mouseX, pUIManager->mouseY);
				spaceBarMenu->show();
			}

			if (editMode == ResourceEditorMode::SELECT_MODE)
			{
				if (InputManager::isKeyPressed(JA_ESCAPE)) {
					Toolset::getSingleton()->setRulerActive(false);
					MainMenu::getSingleton()->setGameState(GameState::MAINMENU_MODE);
				}
			}

			if (InputManager::isKeyModPress(JA_KMOD_SHIFT) && InputManager::isKeyPressed(JA_8)) {
				hud.setRender(!hud.isRendered());
			}
		}

		switch (editMode)
		{
		case ResourceEditorMode::SELECT_MODE:
		{
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
		}
		break;
		case ResourceEditorMode::COMPONENT_EDITOR_MODE:
			currentComponentEditor->handleInput();
			break;
		}

		oldDrawPointPosition = Toolset::getSingleton()->getDrawPoint();
		hud.update();
	}

	void ResourceEditor::drawGizmo()
	{
		DisplayManager::setColorUb(255, 255, 255);
		DisplayManager::setPointSize(7);
		DisplayManager::drawPoint(gizmo.x, gizmo.y);

		DisplayManager::setColorUb(255, 0, 0);
		DisplayManager::setPointSize(5);
		DisplayManager::drawPoint(gizmo.x, gizmo.y);
	}


	void ResourceEditor::draw()
	{
		CameraInterface* camera = GameManager::getSingleton()->getActiveCamera();
		DisplayManager::pushWorldMatrix();
		DisplayManager::clearWorldMatrix();
		DisplayManager::setCameraMatrix(camera);

		if (drawBoxSelection) {
			DisplayManager::setColorUb(255, 0, 128, 255);
			DisplayManager::setLineWidth(3.0f);
			AABB2d boxSelection = AABB2d::make(selectionStartPoint, Toolset::getSingleton()->getDrawPoint());
			Debug::drawAABB2d(boxSelection);

			DisplayManager::setColorUb(255, 255, 255);
			DisplayManager::setPointSize(7);
			DisplayManager::drawPoint(selectionStartPoint.x, selectionStartPoint.y);

			DisplayManager::setColorUb(255, 0, 0);
			DisplayManager::setPointSize(5);
			DisplayManager::drawPoint(selectionStartPoint.x, selectionStartPoint.y);
		}

		if (this->drawPhysics)
		{
			PhysicsManager::getSingleton()->drawUser = nullptr;
			DisplayManager::setLineWidth(1.0f);
			PhysicsManager::getSingleton()->drawDebug(camera);
		}

		drawGizmo();

		if (editMode == ResourceEditorMode::COMPONENT_EDITOR_MODE) {
			currentComponentEditor->render();
		}

		DisplayManager::popWorldMatrix();
		Toolset::getSingleton()->draw();
		Interface::draw();

		hud.draw();
	}

	void ResourceEditor::setEditorMode(ResourceEditorMode mode)
	{
		editMode = mode;

		switch (editMode)
		{
		case ResourceEditorMode::SELECT_MODE:
			hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_MODE_HUD, "Mode=Select Resource");
			hud.setFieldValue((uint32_t)AnimationEditorHud::EDITOR_HELPER_HUD, "");
			break;
			//case PolygonEditorMode::ROTATE_MODE:
			//	hud.setFieldValue((uint32_t)PolygonEditorHud::EDITOR_MODE_HUD, "Mode=Rotate Sprite");
			//	hud.setFieldValue((uint32_t)PolygonEditorHud::EDITOR_HELPER_HUD, "LMB - Accept\n0 - Set 0 Angle\nESC - Cancel");
			//	break;
			//case PolygonEditorMode::SCALE_MODE:
			//	hud.setFieldValue((uint32_t)PolygonEditorHud::EDITOR_MODE_HUD, "Mode=Scale Sprite");
			//	hud.setFieldValue((uint32_t)PolygonEditorHud::EDITOR_HELPER_HUD, "LMB - Accept\n+,- - Size\n1 - Scale to 1\nESC - Cancel");
			//	break;
		}
	}

	void ResourceEditor::setEditorMode(CommonWidget* commonWidget)
	{
		if (editMode == ResourceEditorMode::COMPONENT_EDITOR_MODE)
		{
			currentComponentEditor->setActiveMode(false);
			setEditorMode(ResourceEditorMode::SELECT_MODE);
			return;
		}

		editMode = ResourceEditorMode::COMPONENT_EDITOR_MODE;
		currentComponentEditor = commonWidget;
		currentComponentEditor->setActiveMode(true);
	}

	ResourceEditorMode ResourceEditor::getEditorMode()
	{
		return this->editMode;
	}

	CommonWidget* ResourceEditor::getCurrentEditor()
	{
		if (editMode == ResourceEditorMode::COMPONENT_EDITOR_MODE)
		{
			return this->currentComponentEditor;
		}

		return nullptr;
	}

	jaVector2 ResourceEditor::getGizmoPosition()
	{
		return gizmo;
	}

	void ResourceEditor::createGUI()
	{
		this->resourceBar  = new SlideBox(0);
		this->resourceMenu = new ResourceComponentMenu(1, this);

		this->resourceBar->dockTo((uint32_t)Margin::TOP);

		FlowLayout* menuBar = new FlowLayout(0, nullptr, (uint32_t)Margin::LEFT, 5, 0);
		menuBar->setColor(128, 128, 128, 128);
		menuBar->setSize(width, 28);
		menuBar->setPosition(0, height - 28);

		Button* menuButton = new Button(1, nullptr);
		menuButton->setFont("default.ttf",10);
		menuButton->setText("Menu");
		menuButton->setColor(0, 0, 255, 255);
		menuButton->setFontColor(255, 255, 255, 255);
		menuButton->adjustToText(10, 28);
		menuButton->vUserData = this->resourceMenu;
		menuButton->setOnClickEvent(this, &ResourceEditor::onClickMenuBar);
		menuBar->addWidget(menuButton);

		spaceBarMenu = new ResourceSpaceBarMenu(3, nullptr);
		spaceBarMenu->hide();

		this->resourceBar->setSlideArea(10);
		this->resourceBar->setArea(28);
		this->resourceBar->setBox(menuBar);

		addWidget(this->resourceMenu);
		addWidget(this->resourceBar);
	}

	/*void ResourceEditor::calculateSelectionMidpoint()
	{
		std::list<ja::Skeleton*>::iterator itSkeleton = selectedSkeletons.begin();
		gizmo.setZero();

		for (itSkeleton; itSkeleton != selectedSkeletons.end(); ++itSkeleton)
		{
			gizmo += (*itSkeleton)->getPosition(); //objectHandle->transform.position;
		}

		gizmo = gizmo / (jaFloat)(selectedSkeletons.size());
	}*/

	void ResourceEditor::updateComponentEditors()
	{
		resourceMenu->updateComponentEditors(nullptr);
	}

	void ResourceEditor::onClickMenuBar(Widget* sender, WidgetEvent action, void* data)
	{
		Button* menuButton = (Button*)(sender);

		Widget* linkedWidget = (Widget*)(menuButton->vUserData);
		linkedWidget->setRender(true);
		linkedWidget->setActive(true);
	}

}