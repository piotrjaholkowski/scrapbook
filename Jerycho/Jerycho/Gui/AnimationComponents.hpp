#pragma once

#include "WidgetId.hpp"
#include "CommonWidgets.hpp"
#include "Toolset.hpp"

//#include "AnimationEditor.hpp"
//#include "../../Engine/Graphics/Polygon.hpp"

#include "../../Engine/Graphics/Skeleton.hpp"
#include "../../Engine/Gui/GridLayout.hpp"

#include <stdint.h>

namespace tr
{
	class AnimationComponentMenu : public CommonWidget
	{
	private:
		static AnimationComponentMenu* singleton;
		CommonWidget* components[20];
	public:
		AnimationComponentMenu(uint32_t widgetId, ja::Widget* parent);
		void onMenuClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void updateComponentEditors(void* polygon);

		static inline AnimationComponentMenu* getSingleton()
		{
			return singleton;
		}
	};

	class AnimationSequenceEditor : public CommonWidget
	{
		static AnimationSequenceEditor* singleton;

		float infoPosX;
		float infoPosY;

		ja::TextField*      newAnimationNameTextfield;
		ja::Button*         createAnimationButton;
		
		ja::TextField*      animationTextfield;
		ja::Button*         deleteAnimationButton;

		ja::GridViewer*     animationGridViewer;
		ja::TextField*      timeTextfield;
		ja::Label*          animationsNumLabel;
		ja::Label*          keyFramesLabel;
		ja::SequenceEditor* animationSequenceEditor;
		ja::Label           animationLabel;
		ja::TextField*      positionTextfield;
		ja::TextField*      elapsedTimeTextfield;
		
		ja::Button*         insertKeyFrameButton;
		ja::Button*         deleteKeyFrameButton;

		ja::Skeleton*          editedSkeleton;
		ja::SkeletonAnimation* editedAnimation;

		ja::SequenceMarker* playMarker;
		ja::SequenceMarker* insertMarker;
		const int16_t   markerSize = 5;

		std::vector<void*> animations;
	
	public:
		AnimationSequenceEditor(uint32_t widgetId, ja::Widget* parent);

		void updateWidget(void* data);
		void setActiveWidgets(bool state);

		static inline AnimationSequenceEditor* getSingleton()
		{
			return singleton;
		}

		void setAnimationData(ja::SkeletonAnimation* animation);
		void refreshAnimationGridView();
	
		void onDrawAnimation(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPickAnimation(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCreateAnimationClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDeleteAnimationClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCreateKeyFrameClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDeleteKeyFrameClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onMarkerChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onTimeDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPositionDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onElapsedTimeDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	enum class AnimationEditorMode;

	class AnimationSkeletonEditor : public CommonWidget
	{
		static AnimationSkeletonEditor* singleton;

		float infoPosX;
		float infoPosY;
		
		ja::TextField*  skeletonNameTextfield;
		ja::GridViewer* boneGridViewer;
		ja::Checkbox*   gizmoToParentCheckbox;
		ja::Button*     editModeButton;
		ja::Label*      bonesNumLabel;
		ja::Label       boneLabel;

		std::vector<void*> bones;

		ja::Skeleton* editedSkeleton;

		AnimationEditorMode editorMode;

		bool      drawBoxSelection;
		ScaleMode scaleAxis;
		jaVector2 selectionStartPoint;
		jaVector2 gizmo;
		jaVector2 transformationStart;
		StackAllocator<bool>            selectedBones;
		StackAllocator<uint16_t>        selectedBoneIds;
		StackAllocator<ja::Bone>            savedBoneStates;
		StackAllocator<ja::TransformedBone> savedTransformedBoneStates;

	public:
		AnimationSkeletonEditor(uint32_t widgetId, ja::Widget* parent);

		void updateWidget(void* skeleton);

		void setEditorMode(AnimationEditorMode editorMode);

		void translateMode();
		void scaleMode();
		void rotateMode();

		void drawGizmo();
		void drawBoneNames();
		void drawSelectedBones();
		//void drawOriginPoint();

		void render();
		void handleInput();
		void setActiveMode(bool state);

		void setSkeletonInfo(ja::Skeleton* skeleton);
		void refreshBoneGridView();

		void createBone();

		void saveBoneStates();
		void makeSelection(gil::AABB2d& selectionAABB);
		void addToSelection(gil::AABB2d& selectionAABB);
		void removeFromSelection(gil::AABB2d& selectionAABB);
		void calculateSelectionPoint();

		static inline AnimationSkeletonEditor* getSingleton()
		{
			return singleton;
		}

		inline ja::Skeleton* getEditedSkeleton()
		{
			return this->editedSkeleton;
		}

		void onEditModeClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDrawBone(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPickBone(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class AnimationBoneEditor : public CommonWidget
	{
	private:
		static AnimationBoneEditor* singleton;

		ja::TextField* boneNameTextfield;
		
		ja::TextField* boneLengthTextfield;
		ja::TextField* positionXTextfield;
		ja::TextField* positionYTextfield;
		ja::TextField* angleTextfield;

		uint16_t editedBoneId;


	public:
		AnimationBoneEditor(uint32_t widgetId, ja::Widget* parent);

		void updateWidget(void* data);
		void updateBoneId(uint16_t boneId);

		void onNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onBoneLengthDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPositionXDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPositionYDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onAngleDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);

		static inline AnimationBoneEditor* getSingleton()
		{
			return singleton;
		}
	};

	class AnimationSpaceBarMenu : public ja::GridLayout
	{
		ja::Button* addShapeButton;

	public:
		AnimationSpaceBarMenu(uint32_t widgetId, ja::Widget* parent);
		virtual ~AnimationSpaceBarMenu();

		void hide();
		void show();
		void update();
	};
}

