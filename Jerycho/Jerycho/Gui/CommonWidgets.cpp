#include "CommonWidgets.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"
#include "../../Engine/Manager/ResourceManager.hpp"
#include "../../Engine/Manager/SoundManager.hpp"
#include "../../Engine/Manager/ScriptManager.hpp"

#include "../../Engine/Gui/Margin.hpp"
#include "../../Engine/Gui/Editbox.hpp"
#include "../../Engine/Gui/ProgressButton.hpp"
#include "../../Engine/Gui/GridLayout.hpp"
#include "../../Engine/Gui/FlowLayout.hpp"
#include "../../Engine/Gui/TextField.hpp"
#include "../../Engine/Gui/BoxLayout.hpp"
#include "../../Engine/Gui/Scrollbar.hpp"
#include "../../Engine/Gui/Checkbox.hpp"
#include "../../Engine/Gui/RadioButton.hpp"
#include "../../Engine/Gui/GridViewer.hpp"
#include "../../Engine/Gui/SlideWidget.hpp"
#include "../../Engine/Gui/SequenceEditor.hpp"
#include "../../Engine/Gui/ScrollPane.hpp"
#include "../../Engine/Gui/Hider.hpp"
#include "../../Engine/Gui/Magnet.hpp"

using namespace ja;

namespace tr
{ 

ScriptFunctionViewer* ScriptFunctionViewer::singleton = nullptr;
DataModelViewer*      DataModelViewer::singleton = nullptr;

void fillButton(Button* button, const char* text)
{
	button->setFont("default.ttf",10);
	button->setColor(0, 0, 255, 128);
	button->setFontColor(255, 255, 255, 255);
	button->setText(text);
	button->adjustToText(10, 28);
}

CommonWidget::CommonWidget(uint32_t widgetId, const char* interfaceName, Widget* parent) : Interface(widgetId, parent)
{
	int32_t width, height;
	DisplayManager::getSingleton()->getResolution(&width,&height);

	setPosition(0,0);
	setSize(INT_MAX,INT_MAX);

	this->dockPositionX = width/2;
	this->dockPositionY = height/2;

	this->notifier = nullptr;

	this->editorTitle = new Label(0,this);
	this->editorTitle->setFont("lucon.ttf",12);
	this->editorTitle->setColor(255,255,255,255);
	this->editorTitle->setShadowColor(255,0,0,128);
	this->editorTitle->setShadowOffset(-2,-2);
	this->editorTitle->setAlign((uint32_t)Margin::LEFT | (uint32_t)Margin::DOWN);
	this->editorTitle->setText(interfaceName);
	this->editorTitle->setRender(true);
	this->editorTitle->setPosition(dockPositionX, dockPositionY);
	addWidget(this->editorTitle);	

 	ResourceManager* resourceManager = ResourceManager::getSingleton();

	this->hider = new Hider(1,this);
	this->hider->setPosition(dockPositionX - 18, dockPositionY);
	this->hider->setShowImage(resourceManager->getEternalTexture2d("gui/hider_show.png")->resource);
	this->hider->setHideImage(resourceManager->getEternalTexture2d("gui/hider_hide.png")->resource);
	this->hider->setSize(18,18);
	this->hider->setImageSize(18,18);
	this->hider->setColor(0,0,0,0);
	this->hider->setRender(true);
	this->hider->setActive(true);
	addWidget(this->hider);

	this->magnet = new Magnet(2,this);
	this->magnet->setPosition(dockPositionX - 36, dockPositionY);
	this->magnet->setSize(18,18);
	this->magnet->setRender(true);
	this->magnet->setActive(true);
	this->magnet->setColor(0,0,0,0);
	this->magnet->setImageTexture(resourceManager->getEternalTexture2d("gui/magnet.png")->resource);
	this->magnet->setImageSize(18,18);
	this->magnet->addLink(editorTitle);
	this->magnet->addLink(hider);
	addWidget(this->magnet);

	this->closeButton = new ImageButton(3,this);
	this->closeButton->setPosition(dockPositionX - 54, dockPositionY);
	this->closeButton->setImageTexture(resourceManager->getEternalTexture2d("gui/close.png")->resource);
	this->closeButton->setSize(18,18);
	this->closeButton->setImageSize(18,18);
	this->closeButton->setColor(0,0,0,0);
	this->closeButton->setRender(true);
	this->closeButton->setActive(true);
	this->closeButton->setOnClickEvent(this,&CommonWidget::onCloseClick);
	magnet->addLink(this->closeButton);
	addWidget(this->closeButton);
}

CommonWidget::~CommonWidget()
{

}

void CommonWidget::setNotifier( WidgetCallback* notifier )
{
	this->notifier = notifier;
}

void CommonWidget::onMouseOver(Widget* sender, WidgetEvent action, void* data)
{
	if(sender->getWidgetType() == JA_BUTTON)
	{
		Button* button = (Button*)sender;
		button->setColor(0,255,0);
	}
}

void CommonWidget::onMouseLeave(Widget* sender, WidgetEvent action, void* data)
{
	if(sender->getWidgetType() == JA_BUTTON)
	{
		Button* button = (Button*)sender;
		button->setColor(255,0,0);
	}
}

void CommonWidget::addWidgetToEditor( Widget* widget )
{
	hider->addWidget(widget);
	magnet->addLink(widget);
	addWidget(widget);
}

void CommonWidget::fillLabel( Label* label, const char* text )
{
	label->setFont("default.ttf",10);
	label->setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);

	label->setColor(255,255,255,255);
	label->setShadowColor(255,0,0,128);
	label->setShadowOffset(-2,-2);

	label->setText(text);
	label->adjustToText(10,20);
}

void CommonWidget::fillButton( Button* button, const char* text )
{
	button->setFont("default.ttf",10);
	button->setColor(255,0,0,128);
	button->setFontColor(255,255,255,255);
	button->setText(text);
	button->adjustToText(10,20);

	button->setOnMouseOverEvent(this,&CommonWidget::onMouseOver);
	button->setOnMouseLeaveEvent(this,&CommonWidget::onMouseLeave);
}

void CommonWidget::fillScrollPane(ja::ScrollPane* scrollPane)
{
	scrollPane->setScrollbarSize(10);
	scrollPane->setCorner(WidgetCorner::JA_RIGHT_LOWER_CORNER);
	scrollPane->setColor(255, 0, 0, 150);

	Scrollbar* verticalScrollbar   = scrollPane->getVerticalScrollbar();
	Scrollbar* horizontalScrollbar = scrollPane->getHorizontalScrollbar();

	verticalScrollbar->setColor(30, 30, 30, 255);
	horizontalScrollbar->setColor(30, 30, 30, 255);
}

void CommonWidget::fillProgressButton( ProgressButton* button )
{
	button->setImage("gui//progressButton.png");
	button->setColor(0,0,0,0);
	button->setSize(28,28);
	button->setImageSize(28,28);
}

void CommonWidget::fillGridLayout( GridLayout* gridLayout )
{
	int32_t widthTemp = gridLayout->getWidth();
	int32_t heightTemp = gridLayout->getHeight();
	gridLayout->setColor(0,0,0,0);
	gridLayout->setRenderBorder(false);

	gridLayout->setPosition(dockPositionX - 36, dockPositionY - heightTemp);
}

void CommonWidget::fillEditbox( Editbox* editbox )
{
	editbox->setColor(128,128,128,128);
	editbox->setFontColor(255, 255, 255, 255);
}

void CommonWidget::fillCheckbox( Checkbox* checkbox )
{
	checkbox->setColor(255,255,255,255);
	checkbox->setSize(18,18);
}

void CommonWidget::fillRadioButton( RadioButton* radiobutton, const char* text )
{
	radiobutton->setColor(255,255,255,255);
	radiobutton->setText(text);

	radiobutton->setFontColor(0,0,0,255);

	radiobutton->setSize(18,18);

	radiobutton->setCheck(false);
}

void CommonWidget::fillFlowLayout( FlowLayout* flowLayout )
{
	int32_t widthTemp = flowLayout->getWidth();
	int32_t heightTemp = flowLayout->getHeight();
	flowLayout->setColor(128, 128, 128, 128);
	flowLayout->setRenderBorder(false);

	flowLayout->setPosition(dockPositionX - 36, dockPositionY - heightTemp);
}

void CommonWidget::fillBoxLayout( BoxLayout* boxLayout )
{
	int32_t widthTemp = boxLayout->getWidth();
	int32_t heightTemp = boxLayout->getHeight();
	boxLayout->setColor(128,128,128,128);
	boxLayout->setRenderBorder(true);
	boxLayout->setSelectable(true);

	boxLayout->setPosition(dockPositionX - 36, dockPositionY - heightTemp);
}


void CommonWidget::fillGridViewer(GridViewer* gridViewer)
{
	int32_t widthTemp  = gridViewer->getWidth();
	int32_t heightTemp = gridViewer->getHeight();
	gridViewer->setColor(0, 0, 0, 0);
	gridViewer->setCellColor(255, 0, 0, 150);
	gridViewer->setRenderBorder(false);
	gridViewer->setScrollbarColor(30, 30, 30, 255);
	
	gridViewer->setPosition(dockPositionX - 36, dockPositionY - heightTemp);
}

void CommonWidget::fillSequenceEditor(SequenceEditor* sequenceEditor)
{
	int32_t widthTemp =  sequenceEditor->getWidth();
	int32_t heightTemp = sequenceEditor->getHeight();
	sequenceEditor->setColor(0, 255, 0, 255);
	sequenceEditor->setRenderBorder(false);
	sequenceEditor->setScrollbarColor(30, 30, 30, 255);
	sequenceEditor->setSelectedMarkerColor(255, 0, 0);
	sequenceEditor->setMarkerMoveSensitivity(4);

	sequenceEditor->setPosition(dockPositionX - 36, dockPositionY - heightTemp);
}

void CommonWidget::fillTextField( TextField* textField )
{
	textField->setColor(128,128,128,255);
	textField->setFontColor(255,255,255,255);
}


void CommonWidget::fillScrollbar(Scrollbar* scrollbar, double lowestValue, double biggestValue)
{
	scrollbar->setColor(30,30,30,255);
	scrollbar->setRange(lowestValue, biggestValue);
}

void CommonWidget::onCloseClick( Widget* sender, WidgetEvent action, void* data )
{
	setActive(false);
	setRender(false);
}

////////////////////////////////////////////////////////

MeshEditor::MeshEditor(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Mesh Editor", parent)
{
	setRender(false);
	setActive(false);

	const int32_t widgetWidth = 300;

	BoxLayout* boxLayout = new BoxLayout(4, this, (uint32_t)Margin::LEFT, false, 0, 2);
	boxLayout->setSize(widgetWidth, 3 * (28 + 2));
	fillBoxLayout(boxLayout);

	{
		FlowLayout* posLayout = new FlowLayout(1, parent, (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, 2, 0);
		posLayout->setSize(widgetWidth, 28);
		fillFlowLayout(posLayout);
		Label* xLabel = new Label(0, nullptr);
		fillLabel(xLabel, "X");
		this->xTextfield = new TextField(1, nullptr);
		fillTextField(this->xTextfield);
		this->xTextfield->setAcceptOnlyNumbers(true);
		this->xTextfield->setTextSize(12, 28);
		//this->xTextfield->setOnDeselectEvent(this, &MeshEditor::xDeselect);
		Label* yLabel = new Label(2, nullptr);
		fillLabel(yLabel, "Y");
		this->yTextfield = new TextField(3, nullptr);
		fillTextField(this->yTextfield);
		this->yTextfield->setAcceptOnlyNumbers(true);
		this->yTextfield->setTextSize(12, 28);
		//this->yTextfield->setOnDeselectEvent(this, &MeshEditor::yDeselect);

		this->xTextfield->setActive(false);
		this->yTextfield->setActive(false);

		posLayout->addWidget(xLabel);
		posLayout->addWidget(this->xTextfield);
		posLayout->addWidget(yLabel);
		posLayout->addWidget(this->yTextfield);
		boxLayout->addWidget(posLayout);
	}

	addWidgetToEditor(boxLayout);
}

MeshEditor::~MeshEditor()
{

}

ScriptFunctionViewer::ScriptFunctionViewer( uint32_t widgetId, Widget* parent ) : CommonWidget(widgetId,"Script Functions",parent)
{
	this->singleton = this;

	setRender(false);
	setActive(false);

	chosenFunction = nullptr;

	const int32_t widgetWidth  = 400;
	const int32_t widgetHeight = 300;

	BoxLayout* boxLayout = new BoxLayout(0,nullptr,1,0,0,0);
	boxLayout->setSize(widgetWidth,widgetHeight);
	fillBoxLayout(boxLayout);

	{
		this->functionsViewer = new GridViewer(0, 1, 9, nullptr);
		this->functionsViewer->setSize(widgetWidth, widgetHeight);
		fillGridViewer(this->functionsViewer);
		this->functionsViewer->setOnDrawEvent(this, &ScriptFunctionViewer::onFunctionDraw);
		this->functionsViewer->setOnClickEvent(this, &ScriptFunctionViewer::onFunctionClick);
		this->functionsViewer->setGridList(&this->viewedFunctions);

		boxLayout->addWidget(this->functionsViewer);
	}

	{
		functionLabel.setFont("default.ttf",10);
		functionLabel.setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
		functionLabel.setColor(255, 255, 255, 255);
		functionLabel.setText("");
	}

	this->lastFunctionType = ja::ScriptFunctionType::UPDATE_ENTITY;
	
    addWidgetToEditor(boxLayout);
	closeButton->setOnClickEvent(this,&ScriptFunctionViewer::onClose);
	setModal(true);
}

ScriptFunctionViewer::~ScriptFunctionViewer()
{

}

void ScriptFunctionViewer::onClose( Widget* sender, WidgetEvent action, void* data )
{
	this->chosenFunction = nullptr;
	setActive(false);
	setRender(false);
}

void ScriptFunctionViewer::refresh(ScriptFunctionType functionType)
{
	this->lastFunctionType = functionType;
	const StackAllocator<ScriptFunction>* scriptFunctions = ScriptManager::getSingleton()->getScriptFunctions(functionType);

	viewedFunctions.clear();

	const uint32_t  functionsNum   = scriptFunctions->getSize();
	ScriptFunction* scriptFunction = scriptFunctions->getFirst();

	for (uint32_t i = 0; i < functionsNum; ++i)
	{
		viewedFunctions.push_back(&scriptFunction[i]);
	}
}

void ScriptFunctionViewer::onFunctionDraw(Widget* sender, WidgetEvent action, void* data)
{
	ScriptFunction* function = (ScriptFunction*)data;

	int32_t x, y;

	int32_t halfWidth, halfHeight;

	sender->getPosition(x, y);

	halfWidth = (sender->getWidth() / 2);
	halfHeight = (sender->getHeight() / 2);

	Button* button = (Button*)sender;

	functionLabel.x = x + halfWidth;
	functionLabel.y = y + halfHeight;
	functionLabel.setTextPrintC("%s\n%s", function->functionName, function->fileName);
	functionLabel.draw(0, 0);
}

void  ScriptFunctionViewer::onFunctionClick(Widget* sender, WidgetEvent action, void* data)
{
	Button* button = (Button*)sender;
	ScriptFunction* function = (ScriptFunction*)button->vUserData;
	
	if (nullptr != function)
	{
		this->chosenFunction = function;

		setActive(false);
		setRender(false);
		if (notifier != nullptr)
		{
			(*notifier)(this, JA_ONCLICK, nullptr);
		}
	}
}

DataModelViewer::DataModelViewer(uint32_t widgetId, Widget* parent) : CommonWidget(widgetId, "Data Model", parent)
{
	this->singleton = this;

	setRender(false);
	setActive(false);

	this->chosenDataModel = nullptr;

	const int32_t widgetWidth  = 400;
	const int32_t widgetHeight = 300;

	BoxLayout* boxLayout = new BoxLayout(0, nullptr, 1, 0, 0, 0);
	boxLayout->setSize(widgetWidth, widgetHeight);
	fillBoxLayout(boxLayout);

	{
		this->dataModelsViewer = new GridViewer(0, 1, 9, nullptr);
		this->dataModelsViewer->setSize(widgetWidth, widgetHeight);
		fillGridViewer(this->dataModelsViewer);
		this->dataModelsViewer->setOnDrawEvent(this, &DataModelViewer::onDataModelDraw);
		this->dataModelsViewer->setOnClickEvent(this, &DataModelViewer::onDataModelClick);
		this->dataModelsViewer->setGridList(&this->dataModels);

		boxLayout->addWidget(this->dataModelsViewer);
	}

	{
		dataModelLabel.setFont("default.ttf",10);
		dataModelLabel.setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);
		dataModelLabel.setColor(255, 255, 255, 255);
		dataModelLabel.setText("");
	}

	addWidgetToEditor(boxLayout);
	closeButton->setOnClickEvent(this, &DataModelViewer::onClose);
	setModal(true);
}

DataModelViewer::~DataModelViewer()
{

}

void DataModelViewer::refresh()
{
	const HashMap<Resource<DataModel>>* dataModelsHashMap = ResourceManager::getSingleton()->getDataModels();

	dataModels.clear();

	const uint32_t                  dataModelsNum = dataModelsHashMap->getSize();
	HashEntry<Resource<DataModel>>* pDataModels   = dataModelsHashMap->getFirst();

	for (uint32_t i = 0; i < dataModelsNum; ++i)
	{
		//viewedFunctions.push_back(&scriptFunction[i]);
		dataModels.push_back(pDataModels[i].data->resource);
	}
}

void DataModelViewer::onDataModelDraw(Widget* sender, WidgetEvent action, void* data)
{
	DataModel* dataModel = (DataModel*)data;

	int32_t x, y;

	int32_t halfWidth, halfHeight;

	sender->getPosition(x, y);

	halfWidth = (sender->getWidth() / 2);
	halfHeight = (sender->getHeight() / 2);

	Button* button = (Button*)sender;

	dataModelLabel.x = x + halfWidth;
	dataModelLabel.y = y + halfHeight;
	dataModelLabel.setTextPrintC("%s\n%s", dataModel->getName(), dataModel->getFileName());
	dataModelLabel.draw(0, 0);
}

void  DataModelViewer::onDataModelClick(Widget* sender, WidgetEvent action, void* data)
{
	Button* button = (Button*)sender;
	DataModel* dataModel = (DataModel*)button->vUserData;

	if (nullptr != dataModel)
	{
		this->chosenDataModel = dataModel;

		setActive(false);
		setRender(false);
		if (notifier != nullptr)
		{
			(*notifier)(this, JA_ONCLICK, nullptr);
		}
	}
}

void DataModelViewer::onClose(Widget* sender, WidgetEvent action, void* data)
{
	this->chosenDataModel = nullptr;
	setActive(false);
	setRender(false);
}

CommonPopup::CommonPopup(uint32_t widgetId, Widget* parent ) : Interface(widgetId,parent)
{
	setPosition(0,0);
	setSize(INT_MAX,INT_MAX);
}

CommonPopup::~CommonPopup()
{

}

void CommonPopup::show()
{
	int32_t mousePosX = InputManager::getMousePositionX();
	int32_t mousePosY = -InputManager::getMousePositionY() + DisplayManager::getResolutionHeight();
	popup->setPosition(mousePosX,mousePosY);	
	
	setRender(true);
	setActive(true);
}

void CommonPopup::hide()
{
	uint32_t widgetsNum = widgets.size();
	for(uint32_t i=0; i<widgetsNum; ++i)
	{
		Widget* widget = widgets[i];
		if(widget->getWidgetType() == JA_BUTTON)
		{
			Button* button = static_cast<Button*>(widget);
			button->setColor(255,0,0);
		}
	}
	setActive(false);
	setRender(false);
}

void CommonPopup::update()
{
	Interface::update();

	if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
	{
		int32_t mouseX = InputManager::getMousePositionX();
		int32_t mouseY = InputManager::getMousePositionY();
		mouseY = DisplayManager::getSingleton()->getResolutionHeight() - mouseY;
		if(!popup->getCollisionMask().intersect(mouseX,mouseY))
		{
			hide();
		}
	}
}

void CommonPopup::setEnable( Button* button )
{
	button->onClick.setActive(true);
	button->setFontColor(255,255,255,255);
}

void CommonPopup::setDisable( Button* button )
{
	button->setColor(255,0,0);
	button->onClick.setActive(false);
	button->setFontColor(0,0,0,255);
}

void CommonPopup::fillLabel(Label* label, const char* text)
{
	label->setFont("default.ttf",10);
	label->setAlign((uint32_t)Margin::MIDDLE | (uint32_t)Margin::CENTER);

	label->setColor(255, 255, 255, 255);
	label->setShadowColor(255, 0, 0, 128);
	label->setShadowOffset(-2, -2);

	label->setText(text);
	label->adjustToText(10, 20);
}

void CommonPopup::fillButton(Button* button, const char* text)
{
	button->setFont("default.ttf",10);
	button->setColor(255, 0, 0, 128);
	button->setFontColor(255, 255, 255, 255);
	button->setText(text);
	button->adjustToText(10, 20);

	button->setOnMouseOverEvent(this, &CommonPopup::onMouseOver);
	button->setOnMouseLeaveEvent(this, &CommonPopup::onMouseLeave);
}

void CommonPopup::fillProgressButton(ProgressButton* button)
{
	button->setImage("gui//progressButton.png");
	button->setColor(0, 0, 0, 0);
	button->setSize(28, 28);
	button->setImageSize(28, 28);
}

void CommonPopup::fillGridLayout(GridLayout* gridLayout)
{
	int32_t widthTemp = gridLayout->getWidth();
	int32_t heightTemp = gridLayout->getHeight();
	gridLayout->setColor(0, 0, 0, 0);
	gridLayout->setRenderBorder(false);

	//gridLayout->setPosition(dockPositionX - 36, dockPositionY - heightTemp);
}

void CommonPopup::fillEditbox(Editbox* editbox)
{
	editbox->setColor(128, 128, 128, 128);
	editbox->setFontColor(255, 255, 255, 255);
}

void CommonPopup::fillCheckbox(Checkbox* checkbox)
{
	checkbox->setColor(255, 255, 255, 255);
	checkbox->setSize(18, 18);
}

void CommonPopup::fillRadioButton(RadioButton* radiobutton, const char* text)
{
	radiobutton->setColor(255, 255, 255, 255);
	radiobutton->setText(text);

	radiobutton->setFontColor(0, 0, 0, 255);

	radiobutton->setSize(18, 18);

	radiobutton->setCheck(false);
}

void CommonPopup::fillFlowLayout(FlowLayout* flowLayout)
{
	int32_t widthTemp = flowLayout->getWidth();
	int32_t heightTemp = flowLayout->getHeight();
	flowLayout->setColor(128, 128, 128, 128);
	flowLayout->setRenderBorder(false);

}

void CommonPopup::fillBoxLayout(BoxLayout* boxLayout)
{
	int32_t widthTemp = boxLayout->getWidth();
	int32_t heightTemp = boxLayout->getHeight();
	boxLayout->setColor(128, 128, 128, 128);
	boxLayout->setRenderBorder(true);
	boxLayout->setSelectable(true);

}

void CommonPopup::fillGridViewer(GridViewer* gridViewer)
{
	int32_t widthTemp = gridViewer->getWidth();
	int32_t heightTemp = gridViewer->getHeight();
	gridViewer->setColor(0, 0, 0, 0);
	gridViewer->setCellColor(255, 0, 0, 150);
	gridViewer->setRenderBorder(false);
	gridViewer->setScrollbarColor(30, 30, 30, 255);

}

void CommonPopup::fillSequenceEditor(SequenceEditor* sequenceEditor)
{
	int32_t widthTemp = sequenceEditor->getWidth();
	int32_t heightTemp = sequenceEditor->getHeight();
	sequenceEditor->setColor(0, 255, 0, 255);
	sequenceEditor->setRenderBorder(false);
	sequenceEditor->setScrollbarColor(30, 30, 30, 255);
	sequenceEditor->setSelectedMarkerColor(255, 0, 0);
	sequenceEditor->setMarkerMoveSensitivity(4);

}

void CommonPopup::fillTextField(TextField* textField)
{
	textField->setColor(128, 128, 128, 255);
	textField->setFontColor(255, 255, 255, 255);
}


void CommonPopup::fillScrollbar(Scrollbar* scrollbar, double lowestValue, double biggestValue)
{
	scrollbar->setColor(30, 30, 30, 255);
	scrollbar->setRange(lowestValue, biggestValue);
}

void CommonPopup::onMouseOver(Widget* sender, WidgetEvent action, void* data)
{
	if (sender->getWidgetType() == JA_BUTTON)
	{
		Button* button = (Button*)sender;
		button->setColor(0, 255, 0);
	}
}

void CommonPopup::onMouseLeave(Widget* sender, WidgetEvent action, void* data)
{
	if (sender->getWidgetType() == JA_BUTTON)
	{
		Button* button = (Button*)sender;
		button->setColor(255, 0, 0);
	}
}

void CommonPopup::onCloseClick(Widget* sender, WidgetEvent action, void* data)
{
	setActive(false);
	setRender(false);
}

SoundResources::SoundResources( uint32_t widgetId, Widget* parent ) : CommonWidget(widgetId,"Sounds",parent)
{
	setRender(false);
	setActive(false);

	chosenSound = nullptr;
	chosenStream = nullptr;

	soundPlayer = SoundManager::getSingleton()->createSoundPlayer();
	soundPlayer->setRelativeToListener(true);
	soundPlayer->setPosition(0,0,0);
	soundPlayer->setRolloffFactor(1);
	soundPlayer->setDirection(0,0,0);
	soundPlayer->setGain(1);
	soundPlayer->setPitch(1);
	soundPlayer->setMaxDistance(25.0);
	soundPlayer->setReferenceDistance(15.0);
	soundPlayer->setVelocity(0,0,0);
	
	soundViewer = new GridViewer(4, 4, 4, nullptr);
	soundViewer->setSize(400, 400);
	fillGridViewer(soundViewer);
	soundViewer->setColor(0, 0, 0, 0);
	soundViewer->setCellColor(255, 0, 0, 150);
	soundViewer->setGridList(&sounds);
	soundViewer->setOnDrawEvent(this, &SoundResources::onSoundDraw);
	soundViewer->setOnMouseOverEvent(this, &SoundResources::onSoundOver);
	soundViewer->setOnMouseLeaveEvent(this, &SoundResources::onSoundLeave);
	soundViewer->setOnClickEvent(this, &SoundResources::onSoundClick);

	//soundTypeButton = new Button(6, nullptr);
	//fillButton(soundTypeButton, "Streams");
	//soundTypeButton->setSize(100, 28);
	//soundTypeButton->bUserData = true;
	//soundTypeButton->setPosition(groupViewer->x, groupViewer->y - 28);
	//soundTypeButton->setOnClickEvent(this, &SoundResources::onSoundTypeClick);

	font = ResourceManager::getSingleton()->getEternalFont("default.ttf",10)->resource;
	soundDescriptionLabel.setFont("default.ttf",10);
	soundDescriptionLabel.setColor(255, 255, 255, 255);
	soundDescriptionLabel.setShadowColor(255, 0, 0, 128);
	soundDescriptionLabel.setShadowOffset(1, 1);
	soundDescriptionLabel.setAlign((uint32_t)Margin::CENTER | (uint32_t)Margin::MIDDLE);
	soundDescriptionLabel.setText("");

	//addWidgetToEditor(soundTypeButton);
	addWidgetToEditor(soundViewer);
	closeButton->setOnClickEvent(this,&SoundResources::onClose);
	setModal(true);
}


void SoundResources::reloadResources()
{
	if (soundTypeButton->bUserData == true) // sound
	{
		//soundGroups.clear();
		sounds.clear();
		std::vector<Resource<SoundEffect>*> levelSounds;
		//std::vector<ResourceGroupProxy*> levelGroups;
		//ResourceManager::getSingleton()->getLevelResourceGroups(&levelGroups, (uint8_t)ResourceType::SOUND_GROUP);

		//if (chosenGroup == nullptr)
		//	ResourceManager::getSingleton()->getLevelSoundEffects(&levelSounds, "");
		//else
		//	ResourceManager::getSingleton()->getLevelSoundEffects(&levelSounds, chosenGroup->resourceGroupName);

		//for (uint32_t i = 0; i < levelGroups.size(); ++i)
		{
		//	soundGroups.push_back(levelGroups[i]);
		}
		for (uint32_t i = 0; i < levelSounds.size(); ++i)
		{
			sounds.push_back(levelSounds[i]);
		}

		//groupViewer->setGridList(&soundGroups);
		//soundViewer->setGridList(&sounds);
	}
	else // stream
	{
		//streamGroups.clear();
		//streams.clear();
		std::vector<Resource<SoundStream>*> levelStreams;
		//std::vector<ResourceGroupProxy*> levelGroups;
		//ResourceManager::getSingleton()->getLevelResourceGroups(&levelGroups, (uint8_t)ResourceType::SOUND_STREAM);

		//if (chosenGroup == nullptr)
		//	ResourceManager::getSingleton()->getLevelSoundStreams(&levelStreams, "");
		//else
		//	ResourceManager::getSingleton()->getLevelSoundStreams(&levelStreams, chosenGroup->resourceGroupName);

		//for (uint32_t i = 0; i < levelGroups.size(); ++i)
		{
		//	streamGroups.push_back(levelGroups[i]);
		}
		for (uint32_t i = 0; i < levelStreams.size(); ++i)
		{
			streams.push_back(levelStreams[i]);
		}

	//	groupViewer->setGridList(&streamGroups);
		soundViewer->setGridList(&streams);
	}
}

void SoundResources::onSoundDraw(Widget* sender, WidgetEvent action, void* data)
{
	if (soundTypeButton->bUserData == true) // sound effects
	{
		Resource<SoundEffect>* sound = (Resource<SoundEffect>*)(data);

		int32_t x, y;
		int32_t rectPosX, rectPosY;
		sender->getPosition(x, y);
		rectPosX = x;
		rectPosY = y;
		x += sender->width / 2;
		y += sender->height / 2;
		if (sound->resource == soundPlayer->getSoundEffect())
		{
			float elapsedTime;
			soundPlayer->getElapsedTime(elapsedTime);
			SoundEffect* soundEffect = soundPlayer->getSoundEffect();
			int32_t rectWidth = (int)((float)(sender->getWidth()) * elapsedTime / soundEffect->getLengthInSeconds());
			DisplayManager::setColorUb(128, 50, 0, 255);
			DisplayManager::drawRectangle(rectPosX, rectPosY, rectPosX + rectWidth, rectPosY + sender->height, true);
		}

		soundDescriptionLabel.x = x;
		soundDescriptionLabel.y = y;
		soundDescriptionLabel.setTextPrintC("%s\n%d ms\nch %d\n%d Hz", sound->resourceName.c_str(), sound->resource->getLengthInMiliseconds(), sound->resource->getChannelsNum(), sound->resource->getFrequency());
		soundDescriptionLabel.draw(0, 0);
	}
	else {
		Resource<SoundStream>* stream = static_cast<Resource<SoundStream>*>(data);

		int32_t x, y;
		int32_t rectPosX, rectPosY;
		sender->getPosition(x, y);
		rectPosX = x;
		rectPosY = y;
		x += sender->width / 2;
		y += sender->height / 2;
		/*if (stream->resource == soundPlayer->getS())
		{
			float elapsedTime;
			soundPlayer->getElapsedTime(elapsedTime);
			jaSoundEffect* soundEffect = soundPlayer->getSoundEffect();
			int rectWidth = static_cast<int>(static_cast<float>(sender->getWidth()) * elapsedTime / soundEffect->getLengthInSeconds());
			jaDisplayManager::setColorUb(128, 50, 0, 255);
			jaDisplayManager::drawRectangle(rectPosX, rectPosY, rectPosX + rectWidth, rectPosY + sender->height, true);
		}*/
		int32_t seconds = stream->resource->getLengthInMiliseconds() / 1000;
		int32_t minutes = seconds / 60;
		seconds = seconds % 60;
		
		soundDescriptionLabel.x = x;
		soundDescriptionLabel.y = y;
		soundDescriptionLabel.setTextPrintC("%s\n%d:%d\nch %d\n%d Hz", stream->resourceName.c_str(), minutes, seconds, stream->resource->getChannelsNum(), stream->resource->getFrequency());
		soundDescriptionLabel.draw(0, 0);
	}
}

void SoundResources::onGroupDraw(Widget* sender, WidgetEvent action, void* data)
{
	/*ResourceGroupProxy* group = (ResourceGroupProxy*)(data);

	int32_t x, y;
	sender->getPosition(x, y);
	x += sender->width / 2;
	y += sender->height / 2;
	groupLabel.x = x;
	groupLabel.y = y;
	groupLabel.setText(group->resourceGroupName.c_str());
	groupLabel.draw(0, 0);*/
}

void SoundResources::onSoundClick( Widget* sender, WidgetEvent action, void* data )
{
	Button* button = (Button*)(sender);
	if (button->vUserData == nullptr)
		return;
	
	if (soundTypeButton->bUserData == true) {
		chosenSound = static_cast<Resource<SoundEffect>*>(button->vUserData)->resource;
		chosenStream = nullptr;
	}
	else
	{
		chosenSound = nullptr;
		chosenStream = static_cast<Resource<SoundStream>*>(button->vUserData)->resource;
	}

	soundPlayer->stop();
	setActive(false);
	setRender(false);
	if (nullptr != notifier)
	{
		(*notifier)(this, JA_ONCLICK, nullptr);
	}
}


void SoundResources::onSoundTypeClick(Widget* sender, WidgetEvent action, void* data)
{
	if (soundTypeButton->bUserData == true) {
		soundTypeButton->setText("Sounds");
		soundTypeButton->bUserData = false;
		reloadResources();
	}
	else {
		soundTypeButton->setText("Streams");
		soundTypeButton->bUserData = true;
		reloadResources();
	}

}

void SoundResources::onClose( Widget* sender, WidgetEvent action, void* data )
{
	chosenSound = nullptr;
	setActive(false);
	setRender(false);
}

void SoundResources::onSoundOver( Widget* sender, WidgetEvent action, void* data )
{
	Button* button = (Button*)(sender);
	if (button->vUserData == nullptr)
		return;

	button->setColor(0, 255, 0);
	if (soundTypeButton->bUserData == true) // sound effect
	{
		Resource<SoundEffect>* soundEffect = (Resource<SoundEffect>*)(button->vUserData);
		soundPlayer->stop();
		soundPlayer->setSound(soundEffect->resource);
		soundPlayer->play();
	}
}

void SoundResources::onSoundLeave( Widget* sender, WidgetEvent action, void* data )
{
	Button* button = (Button*)(sender);
	if (button->vUserData == nullptr)
		return;
	
	if (soundTypeButton->bUserData == true) {
		Resource<SoundEffect>* soundEffect = (Resource<SoundEffect>*)(button->vUserData);
		if (chosenSound != nullptr)
		{
			if (chosenSound == soundEffect->resource)
				button->setColor(0, 0, 255);
			else
				button->setColor(255, 0, 0);
		}
		else
		{
			button->setColor(255, 0, 0);
		}

		if (soundPlayer->getSoundEffect() == soundEffect->resource)
			soundPlayer->stop();
	}
	else
	{
		Resource<SoundStream>* soundStream = (Resource<SoundStream>*)(button->vUserData);
		if (chosenStream != nullptr)
		{
			if (chosenStream == soundStream->resource)
				button->setColor(0, 0, 255);
			else
				button->setColor(255, 0, 0);
		}
		else
		{
			button->setColor(255, 0, 0);
		}
	}
}

}


