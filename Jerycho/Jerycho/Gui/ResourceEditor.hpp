#pragma once

#include "../../Engine/Gui/DebugHud.hpp"
#include "../../Engine/Gui/Menu.hpp"
#include "../../Engine/Gui/Widget.hpp"

#include "../../Engine/Gui/SlideBox.hpp"
#include "../../Engine/Math/VectorMath2d.hpp"
#include "../../Engine/Gilgamesh/ObjectHandle2d.hpp"
#include "../../Engine/Allocators/StackAllocator.hpp"

#include "ResourceComponents.hpp"

#include <vector>
#include <stdint.h>

namespace tr
{

	enum class ResourceEditorMode
	{
		SELECT_MODE = 0,
		//TRANSLATE_MODE = 1,
		//ROTATE_MODE = 2,
		//SCALE_MODE = 3,
		COMPONENT_EDITOR_MODE = 4
	};

	enum class ResourceEditorInfo
	{
		TRANSLATE_INFO = 0,
		ROTATE_INFO = 1,
		SCALE_INFO = 2,
		TEX_SCALE_INFO = 3,
		DELETE_INFO = 4,
		LAYER_INFO = 5,
		COMPONENT_EDIT_INFO = 6
	};

	enum class ResourceEditorHud
	{
		ID_HUD = 0,
		ANGLE_HUD = 1,
		POSITION_HUD = 2,
		SCALE_HUD = 3,
		//LAYER_HUD = 4,
		EDITOR_MODE_HUD = 5,
		EDITOR_HELPER_HUD = 6,
		//DEPTH_HUD = 7,
		//POLY_TYPE_HUD = 8,
		EDITOR_STACIC_HELPER_HUD = 9
	};

	class ResourceEditor : public ja::Menu
	{
	private:
		//Gui
		//bool objectSpace;

		int32_t mousePosX, mousePosY;

		ResourceEditorMode editMode;
		jaVector2 gizmo;
		jaVector2 oldDrawPointPosition;

		bool      drawBoxSelection;
		bool      drawPhysics;
		//bool      drawEntity;
		bool      drawHud;
		bool      drawHashGrid;
		jaVector2 selectionStartPoint;

		ResourceComponentMenu* resourceMenu;
		ResourceSpaceBarMenu*  spaceBarMenu;

		CommonWidget* currentComponentEditor;
		ja::SlideBox* resourceBar;

	private:

		static ResourceEditor* singleton;
		ResourceEditor();
		void createGUI();
		//void calculateSelectionMidpoint();

		void drawGizmo();

		
	public:
		ja::DebugHud hud;


		static void init();
		static ResourceEditor* getSingleton();
		static void cleanUp();

		void setDefaultParameters();
		void setEditorMode(ResourceEditorMode mode);
		void setEditorMode(CommonWidget* commonWidget);
		ResourceEditorMode getEditorMode();
		CommonWidget*      getCurrentEditor();

		jaVector2       getGizmoPosition();

		void update();
		void draw();
		void updateComponentEditors();

		void onClickMenuBar(ja::Widget* sender, ja::WidgetEvent action, void* data);

		~ResourceEditor();
	};

}