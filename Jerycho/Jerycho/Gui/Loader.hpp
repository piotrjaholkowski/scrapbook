#pragma once

#include "../../Engine/jaSetup.hpp"
#include <mutex>
#include "../../Engine/Gui/Menu.hpp"
#include "../../Engine/Gui/Label.hpp"
#include "../../Engine/Gui/ImageButton.hpp"
#include "../../Engine/Gui/Timer.hpp"

#ifdef JA_SDL2_VERSION
#include<SDL2/SDL_Thread.h>
#else
#include<SDL/SDL_Thread.h>
#endif

namespace tr
{

	class Loader : public ja::Menu
	{
	private:

		static Loader* singleton;
		Loader();

		//SDL_sem* loaderSemaphore;
		std::mutex loaderSemaphore;

		ja::ImageButton* loadingImage;
		ja::ImageButton* gradientImage;
		ja::Timer* blinkTimer;
		ja::Label* currentTaskLabel;
		ja::Label* finishedTaskLabel;
		ja::Label* failureTaskLabel;
		ja::Label* pressSpaceLabel;

		bool loadingFinished;

		bool currentTaskChanged;
		bool finishedTaskChanged;
		bool failureTaskChanged;

		ja::string failureTaskBuffer;
		ja::string currentTaskBuffer;
		ja::string finishedTaskBuffer;

	public:
		static void init();
		static Loader* getSingleton();
		static void cleanUp();
		void setCurrentTask(ja::string currentTask);
		void addFinishedTask(ja::string finishedTask);
		void addFailureTask(ja::string failureTask);
		void reloadFinishedTask();
		void reloadFailureTask();
		void reloadCurrentTask();
		void restartLoader();
		void finishLoader();
		void update();
		void draw();
		//gui
		void onTimer(ja::Widget* sender, ja::WidgetEvent action, void* data);
		//gui
		~Loader();
	};

}