#pragma once

#include "../../Engine/Gui/Interface.hpp"
#include "../../Engine/Gui/Hider.hpp"
#include "../../Engine/Gui/Magnet.hpp"
#include "../../Engine/Gui/Label.hpp"
#include "../../Engine/Gui/Editbox.hpp"
#include "../../Engine/Gui/ProgressButton.hpp"
#include "../../Engine/Gui/ScrollPane.hpp"
#include "../../Engine/Gui/GridLayout.hpp"
#include "../../Engine/Gui/FlowLayout.hpp"
#include "../../Engine/Gui/TextField.hpp"
#include "../../Engine/Gui/BoxLayout.hpp"
#include "../../Engine/Gui/Checkbox.hpp"
#include "../../Engine/Gui/RadioButton.hpp"
#include "../../Engine/Gui/GridViewer.hpp"
#include "../../Engine/Entity/Entity.hpp"
#include "../../Engine/Allocators/StackAllocator.hpp"

#include "../../Engine/Entity/PhysicGroup.hpp"
#include "../../Engine/Entity/LayerObjectGroup.hpp"
#include "../../Engine/Entity/ScriptGroup.hpp"
#include "../../Engine/Entity/SoundGroup.hpp"

#include "CommonWidgets.hpp"

#include <stdint.h>

namespace tr
{
	class ComponentPopup : public CommonPopup
	{
	public:
		ja::Button* deleteComponent;
		ja::Button* editComponent;

		ja::EntityComponent* entityComponent;
	public:
		ComponentPopup(ja::Widget* parent);
		~ComponentPopup();
		void onDeleteComponent(ja::Widget* sender, ja::WidgetEvent widgetEvent, void* data);
		void onEditComponent(ja::Widget* sender, ja::WidgetEvent widgetEvent, void* data);
	};

	class EntityPopup : public CommonPopup
	{
	public:
		ja::Button*    deleteEntity;
		ja::Button*    createEntity;
		ja::TextField* tagTextField;
		static char    tagName[ja::setup::TAG_NAME_LENGTH];
	public:
		EntityPopup(ja::Widget* parent);
		~EntityPopup();
		void onDeleteEntityClick(ja::Widget* sender, ja::WidgetEvent widgetEvent, void* data);
		void onCreateEntityClick(ja::Widget* sender, ja::WidgetEvent widgetEvent, void* data);
		void onTagEntityDeselect(ja::Widget* sender, ja::WidgetEvent widgetEvent, void* data);

		static void changeTagFilter(ja::Entity* entity);
	};
	
	class EntityComponentMenu : public CommonWidget
	{
	private:
		static EntityComponentMenu* singleton;
		CommonWidget* components[20];
	public:
		EntityComponentMenu(uint32_t widgetId, ja::Widget* parent);
		void onMenuClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void updateComponentEditors(void* data);

		static inline EntityComponentMenu* getSingleton()
		{
			return singleton;
		}
	};

	class AddComponentEditor : public CommonWidget
	{
	private:
		ja::GridLayout* componentPickerGrid;

		
		ja::ImageButton* componentsButtons[25];
	public:
		AddComponentEditor(uint32_t widgetId, Widget* parent);
		~AddComponentEditor();

		void onComponentPickerClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class EntityListEditor : public CommonWidget
	{
	private:
		static const int32_t entityNumList    = 6;
		static const int32_t componentNumList = 5;
		static EntityListEditor* singleton;

		ja::GridLayout* entityListGrid;
		ja::GridLayout* componentListGrid;
		ja::Scrollbar*  entityScrollbar;
		ja::Scrollbar*  componentScrollbar;

		ja::Button*      entitiesButtons[entityNumList];
		ja::ImageButton* componentsList[entityNumList][componentNumList];

		ComponentPopup* componentPopup;
		EntityPopup*    entityPopup;
	public:
		EntityListEditor(uint32_t widgetId, ja::Widget* parent);
		~EntityListEditor();

		void updateEditorsData(ja::EntityComponent* entityComponent);
		bool isInSelection(ja::Entity* entity);

		void refresh();
		void refreshEntityList();
		void refreshComponentList();
		void updateComponentList(uint16_t tableId, ja::Entity* tableEntity);

		inline ja::Scrollbar* getEntityScrollbar()
		{
			return this->entityScrollbar;
		}

		static inline EntityListEditor* getSingleton()
		{
			return singleton;
		}

		void onScrollbarChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onEntityClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onComponentListClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class ScriptComponentEditor : public CommonWidget
	{
	private:
		ja::ScriptComponent* scriptComponent;

		ja::TextField*     tagTextField;
		ja::Button*        functionButton;
		ja::WidgetCallback notifierCallback;
	public:
		ScriptComponentEditor(uint32_t widgetId, ja::Widget* parent);
		void updateWidget(void* data);
		void onTagDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onScriptClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onNotify(ja::Widget* sender, ja::WidgetEvent action, void* data);
		~ScriptComponentEditor();
	};

	class DataComponentEditor : public CommonWidget
	{
	private:
		ja::DataComponent* dataComponent;

		ja::TextField*     tagTextField;
		ja::Button*        dataModelButton;
		ja::WidgetCallback notifierCallback;

		ja::ScrollPane* scrollPane;

		//int32_t scrollPaneHeight;

		ja::Label dataLabel;

		StackAllocator<ja::DataNotifier> dataNotifiers;
	public:
		DataComponentEditor(uint32_t widgetId, ja::Widget* parent);
		~DataComponentEditor();

		void updateWidget(void* data);
		void onTagDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDataModelClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onNotify(ja::Widget* sender, ja::WidgetEvent action, void* data);

		void refreshDataFieldViewer();
		void onDataChange(ja::DataNotifier* dataNotifier);
	};

	class PhysicComponentEditor : public CommonWidget
	{
	private:
		ja::PhysicComponent* physicComponent;

		ja::TextField* tagTextField;

		ja::Button* assignPhysicObjectButton;
		ja::Button* selectPhysicObjectButton;

		ja::Checkbox* rootCheckbox;
		ja::Checkbox* autoDeleteCheckbox;

		ja::Button*        onBeginContactButton;
		ja::WidgetCallback onBeginContactNotifierCallback;
		ja::Button*        onEndContactButton;
		ja::WidgetCallback onEndContactNotifierCallback;

		void setPhysicObject(ja::PhysicObject2d* physicObject);
		void pickObject();
	public:
		PhysicComponentEditor(uint32_t widgetId, ja::Widget* parent);
		void updateWidget(void* entityComponent);

		void onTagDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onAssignPhysicObjectClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onSelectPhysicObjectClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onRootChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onAutoDeleteChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onBeginContactClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onBeginContactNotify(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onEndContactClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onEndContactNotify(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class LayerObjectComponentEditor : public CommonWidget
	{
	private:
		ja::LayerObjectComponent* layerObjectComponent;

		ja::TextField* tagTextField;
		ja::Button*    setTagButton;

		ja::Button* assignLayerObjectsButton;
		ja::Button* selectAssignedLayerObjectsButton;

		ja::Button*   dropPhysicButton;
		ja::Checkbox* autoDeleteCheckbox;
		ja::Checkbox* fixedRotationCheckbox;

	public:
		LayerObjectComponentEditor(uint32_t widgetId, Widget* parent);
		void updateWidget(void* entityComponent);

		void onTagDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onAssignLayerObjectClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onSelectAssignedLayerObjectsClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onSetPhysicComponentDrop(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onAutoDeleteChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onFixedRotationChange(ja::Widget* sender, ja::WidgetEvent action, void* data);

	};

	class SoundEditor : public CommonWidget
	{
	private:
		ja::Button* pickSoundButton;
		ja::Button* dropPhysicButton;
		ja::Checkbox* loopCheckbox;
		ja::RadioButton* playRadioButton;
		ja::RadioButton* pauseRadioButton;
		ja::RadioButton* stopRadioButton;

		ja::SoundEffect* chosenSound;
		ja::SoundStream* chosenStream;
		static SoundEditor* singleton;

		ja::WidgetCallback notifierCallback;
	public:
		SoundEditor(uint32_t widgetId, ja::Widget* parent);
		void updateWidget(void* entityComponent);
		void onNotify(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onSoundClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onLoopChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPlayerStateChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onSetPhysicComponentDrop(ja::Widget* sender, ja::WidgetEvent action, void* data);
		static void soundEffectFilter(ja::EntityComponent* entityComponent);
		static void soundStreamFilter(ja::EntityComponent* entityComponent);
	};

	class SensorEditor : public CommonWidget
	{
	private:
	public:
		SensorEditor(uint32_t widgetId, ja::Widget* parent);
		void updateWidget(void* entityComponent);
		~SensorEditor();
	};

}

