#pragma once

extern "C" {
#include <lua/lua.h>
#include <lua/lauxlib.h>
#include <lua/lualib.h>
}

#include "../../Engine/Gui/Menu.hpp"
#include "../../Engine/Gui/Button.hpp"
#include "ScriptConsole.hpp"
#include "PhysicsEditor.hpp"
#include "EntityEditor.hpp"
#include "LayerObjectEditor.hpp"
#include "AnimationEditor.hpp"
#include "ResourceEditor.hpp"
#include "GamePlay.hpp"
#include "Toolset.hpp"
#include "Loader.hpp"

namespace tr
{

	enum class GameState {
		MAINMENU_MODE = 0,
		GAMEPLAY_MODE = 1,
		//SPRITEEDITOR_MODE = 2,
		BLANK_MODE = 2,
		SCRIPTCONSOLE_MODE = 3,
		PHYSICSEDITOR_MODE = 4,
		ENTITYEDITOR_MODE = 5,
		LOADING_MODE = 6,
		SCRIPTGUI_MODE = 7,
		LAYEROBJECTEDITOR_MODE = 8,
		ANIMATIONEDITOR_MODE = 9,
		RESOURCEEDITOR_MODE = 10
	};

	class MainMenu : public Menu
	{
	private:
		GameState gameState;
		GameState previousGameState;
		static MainMenu* singleton;
		//SpriteEditor*    spriteEditor;
		LayerObjectEditor* layerObjectEditor;
		AnimationEditor*   animationEditor;
		ResourceEditor*    resourceEditor;
		ScriptConsole*     scriptConsole;
		PhysicsEditor*     physicsEditor;
		EntityEditor*      entityEditor;
		GamePlay* gamePlay;
		Loader* loader;
		Toolset* toolset;
		//trScriptGui* scriptGui;
		Interface* lastInterface;
		Button* exitButton;
		Button* newGameButton;
		Button* optionButton;

		//ja::string lastDroppedFile;
	public:
		bool isExit;
		MainMenu();
		static void init();
		static MainMenu* getSingleton();
		static void cleanUp();
		void setGameState(GameState gameState);
		GameState getGameState();

		void onDropFile(const char* fileName);

		void update();
		void draw();
		void onNewGame(Widget* sender, WidgetEvent action, void* data);
		void onExit(Widget* sender, WidgetEvent action, void* data);
		void onMouseOver(Widget* sender, WidgetEvent action, void* data);
		void onMouseLeave(Widget* sender, WidgetEvent action, void* data);

		~MainMenu();
	};



}
