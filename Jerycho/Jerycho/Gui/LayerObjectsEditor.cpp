#include "LayerObjectEditor.hpp"
#include "WidgetId.hpp"
#include "Toolset.hpp"

#include "../../Engine/Manager/GameManager.hpp"
#include "../../Engine/Gilgamesh/DebugDraw.hpp"
#include "../../Engine/Manager/DisplayManager.hpp"
#include "../../Engine/Manager/LayerObjectManager.hpp"
#include "../../Engine/Manager/EffectManager.hpp"
#include "MainMenu.hpp"

namespace tr
{

LayerObjectEditor* LayerObjectEditor::singleton = nullptr;

LayerObjectEditor::LayerObjectEditor() : ja::Menu((uint32_t)TR_MENU_ID::LAYEROBJECTEDITOR, nullptr)
{
	this->drawBoxSelection = false;
	this->drawHud          = true;
	this->drawBoxSelection = false;
	this->drawPhysics      = true;
	this->drawHashGrid     = false;

	hud.setFont("default.ttf", 10, jaLeftLowerCorner, 255, 255, 255, 255);
	hud.setFont("default.ttf", 10, jaRightLowerCorner, 255, 255, 255, 255);
	hud.setFont("default.ttf", 10, jaLeftUpperCorner, 255, 255, 255, 255);
	hud.setFont("default.ttf", 10, jaRightUpperCorner, 255, 255, 255, 255);

	hud.addField(jaLeftUpperCorner, (uint32_t)LayerObjectEditorHud::ID_HUD, "Id=0");
	hud.addField(jaLeftUpperCorner, (uint32_t)LayerObjectEditorHud::LAYER_OBJECT_TYPE_HUD, "Type=");

	hud.addField(jaRightUpperCorner, (uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=");

	hud.addField(jaRightLowerCorner, (uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "LMB - Pick polygon\nC - Create polygon\nCtrl + C - Copy\nCtrl + D - Duplicate");
	hud.addField(jaLeftLowerCorner, (uint32_t)LayerObjectEditorHud::EDITOR_STACIC_HELPER_HUD, "L - Ruler\nX - X axis\nY - Y axis\nO - Object Space\nA - Draw AABB\nB - Draw bound\nP - Draw physics\nH - Draw hashgrid\nShift + R - Rotate 90\nShift + Ctrl + R - Rotate 45");

	this->selectedLayerObjectsTemp = new StackAllocator<ObjectHandle2d*>(10, 16);

	int32_t resolutionWidth, resolutionHeight;
	DisplayManager::getSingleton()->getResolution(&resolutionWidth, &resolutionHeight);
	setPosition(0, 0);
	setSize(resolutionWidth, resolutionHeight);
	setText("Layer Object Editor");

	setDefaultParameters();
	createGUI();
	gatherResources();
	
	objectSpace = false;
	setEditorMode(LayerObjectEditorMode::SELECT_MODE);
}

LayerObjectEditor::~LayerObjectEditor() {
	delete this->selectedLayerObjectsTemp;
}

void LayerObjectEditor::cleanUp()	{
	if (singleton != nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void LayerObjectEditor::setDefaultParameters()
{
	this->layerObjectDef = &LayerObjectManager::getSingleton()->layerObjectDef;

	layerObjectDef->transform.rotationMatrix.setDegrees(GIL_ZERO);
	layerObjectDef->layerId = 0;
	layerObjectDef->transform.position.setZero();
	layerObjectDef->scale = jaVector2(1.0f, 1.0f);
	layerObjectDef->depth = 0;
	layerObjectDef->polygonFlag = 0;
	
	Resource<Material>* defaultMaterial = ResourceManager::getSingleton()->getMaterial("DEFAULT");
	if (nullptr != defaultMaterial)
	{
		layerObjectDef->material = defaultMaterial->resource;
		layerObjectDef->shaderObjectData->setShaderBinder(layerObjectDef->material->getShaderBinder());
	}

	layerObjectDef->color.r = 1.0f;
	layerObjectDef->color.g = 1.0f;
	layerObjectDef->color.b = 1.0f;
	layerObjectDef->color.a = 1.0f;

	Color4f white(1.0f, 1.0f, 1.0f, 1.0f);
	jaVector2 squareVertices[] = { jaVector2(-1.0f, -1.0f), jaVector2(1.0f, -1.0f), jaVector2(1.0f, 1.0f), jaVector2(-1.0f, 1.0f) };
	uint32_t  squareIndices[] = { 0, 1, 2, 0, 2, 3 };
	layerObjectDef->drawElements->setVerticesPositions(squareVertices, 4);
	layerObjectDef->drawElements->setVerticesColor(white, 4);
	layerObjectDef->drawElements->setTriangleIndices(squareIndices, 6);
	layerObjectDef->cameraName.setName("Camera");

	this->gizmo = jaVector2(0.0f, 0.0f);
}

LayerObjectEditor* LayerObjectEditor::getSingleton()
{
	return singleton;
}

void LayerObjectEditor::init()
{
	if (singleton == nullptr)
	{
		singleton = new LayerObjectEditor();
	}
}

void LayerObjectEditor::update()
{
	Toolset::getSingleton()->update();
	Interface::update();

	if (layerObjectBar->isSlided())
	{
		hud.setRender(false);
	}
	else
	{
		hud.setRender(this->drawHud);
	}

	bool isGuiActive = (UIManager::isMouseOverGui() || UIManager::isTextInputSelected());
		
	if (!isGuiActive)
	{
		if (InputManager::isMouseButtonReleased((uint8_t)MouseButton::BUTTON_LEFT))
		{
			EffectManager* effectManager = EffectManager::getSingleton();

			if (editMode == LayerObjectEditorMode::SELECT_MODE)
			{
				if (drawBoxSelection && !InputManager::isKeyModPress(JA_KMOD_LALT))
				{
					drawBoxSelection     = false;
					AABB2d selectionAABB = AABB2d::make(selectionStartPoint, Toolset::getSingleton()->getDrawPoint());
					selectedLayerObjectsTemp->clear();
					
					float boxWidth, boxHeight;
					selectionAABB.getSize(boxWidth, boxHeight);

					jaTransform2 transform;
					transform.position = selectionAABB.getCenter();
					transform.rotationMatrix.setRadians(0.0f);
					
					Box2d boxSelection(boxWidth, boxHeight);
					LayerObjectManager::getSingleton()->getLayerObjectsAtShape(transform, &boxSelection, selectedLayerObjectsTemp);

					bool useTriangleSelection = PolygonEditor::getSingleton()->isTriangleSelectionSelected();

					if (useTriangleSelection)
					{
						StackAllocator<gil::ObjectHandle2d*> selectedIntersecting;

						gil::ObjectHandle2d** selectedLayerObjectsArray = selectedLayerObjectsTemp->getFirst();
						uint16_t              selectionNum              = selectedLayerObjectsTemp->getSize();

						LayerObject* selectedAtTop = nullptr;

						for (uint16_t i = 0; i < selectionNum; ++i)
						{
							LayerObject* selectedLayerObject = (LayerObject*)selectedLayerObjectsArray[i]->getUserData();
							Layer*       layer               = effectManager->getLayer(selectedLayerObject->layerId);

							if (!layer->isActive)
								continue;

							if (selectedLayerObject->isIntersecting(selectionAABB))
							{
								selectedIntersecting.push(selectedLayerObjectsArray[i]);

								if (nullptr == selectedAtTop)
								{
									selectedAtTop = (LayerObject*)selectedLayerObjectsArray[i]->getUserData();
								}			
								else
								{
									if (selectedAtTop->depth < selectedLayerObject->depth)
										selectedAtTop = selectedLayerObject;
								}
							}
						}

						if (selectionAABB.min == selectionAABB.max)
						{
							if (nullptr != selectedAtTop)
							{
								selectedLayerObjectsTemp->setSize(1);
								selectedLayerObjectsArray[0] = selectedAtTop->getHandle();
							}
							else
							{
								selectedLayerObjectsTemp->setSize(0);
							}
						}
						else
						{
							uint32_t selectedIntersectingNum = selectedIntersecting.getSize();
							selectedLayerObjectsTemp->setSize(selectedIntersectingNum);
							memcpy(selectedLayerObjectsArray, selectedIntersecting.getFirst(), selectedIntersectingNum * sizeof(gil::ObjectHandle2d*));
						}						
					}
					
					if (InputManager::isKeyModPress(JA_KMOD_LSHIFT) || InputManager::isKeyModPress(JA_KMOD_LCTRL))
					{
						if (InputManager::isKeyModPress(JA_KMOD_LSHIFT))
							addToSelection(selectedLayerObjectsTemp);
						else
							removeFromSelection(selectedLayerObjectsTemp);
					}
					else
					{
						selectedLayerObjects.clear();
						addToSelection(selectedLayerObjectsTemp);
					}
					
					if (selectedLayerObjects.size() != 0)
					{
						LayerObject* layerObject = selectedLayerObjects.back();
						updateComponentEditors(layerObject);
					}
					else
					{
						updateComponentEditors(nullptr);
					}
					
					calculateSelectionMidpoint();
				}
			}
		}

		if (InputManager::isMouseButtonPressed((uint8_t)MouseButton::BUTTON_LEFT))
		{
			switch (editMode) {
			case LayerObjectEditorMode::SELECT_MODE:
			{
				selectionStartPoint = Toolset::getSingleton()->getDrawPoint();

				if (InputManager::isKeyModPress(JA_KMOD_LALT))
				{
					gizmo = Toolset::getSingleton()->getDrawPoint();
				}
				else
				{
					drawBoxSelection = true;
				}
			}
			break;
			case LayerObjectEditorMode::TRANSLATE_MODE:
				setEditorMode(LayerObjectEditorMode::SELECT_MODE);
				break;
			case LayerObjectEditorMode::ROTATE_MODE:
				setEditorMode(LayerObjectEditorMode::SELECT_MODE);
				break;
			case LayerObjectEditorMode::SCALE_MODE:
				setEditorMode(LayerObjectEditorMode::SELECT_MODE);
				if (scaleAxis != ScaleMode::SCALE_XY)
					Toolset::getSingleton()->getRuler()->setActive(false);
				break;
			case LayerObjectEditorMode::COMPONENT_EDITOR_MODE:
				//setEditorMode(TR_PHYSIC_SELECT_MODE);
				break;
			}
		}

		if (InputManager::isKeyPressed(JA_o)) {
			ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
			objectSpace = !objectSpace;
			if (objectSpace) {
				if (selectedLayerObjects.size() != 0) {
					LayerObject* layerObject = selectedLayerObjects.back();
					ruler->setAngle(layerObject->getAngleDeg());
				}
			}
			else
				ruler->setAngle(0);
		}

		if (InputManager::isKeyPressed(JA_l)) {
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
			ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
			Toolset::getSingleton()->setRulerActive(!Toolset::getSingleton()->isRulerActive());
			ruler->setPosition(drawPoint.x, drawPoint.y);
		}

		if (InputManager::isKeyPressed(JA_x))
		{
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
			ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
			ruler->setAlign(RulerAlign::ALIGN_X);
			ruler->setPosition(drawPoint.x, drawPoint.y);
		}

		if (InputManager::isKeyPressed(JA_y))
		{
			jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
			ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
			ruler->setAlign(RulerAlign::ALIGN_Y);
			ruler->setPosition(drawPoint.x, drawPoint.y);
		}

		if (InputManager::isKeyPressed(JA_a))
		{
			GameManager* gameManager = GameManager::getSingleton();
			gameManager->renderAABB = !gameManager->renderAABB;
		}

		if (InputManager::isKeyPressed(JA_b))
		{
			GameManager* gameManager = GameManager::getSingleton();
			gameManager->renderBounds = !gameManager->renderBounds;
		}

		if (InputManager::isKeyPressed(JA_p))
		{
			drawPhysics = !drawPhysics;
		}

		if (InputManager::isKeyPressed(JA_h))
		{
			this->drawHashGrid = !this->drawHashGrid;
		}

		if (InputManager::isKeyPressed(JA_SPACE))
		{
			//this->gizmo = Toolset::getSingleton()->getDrawPoint();
			UIManager* pUIManager = UIManager::getSingleton();
			spaceBarMenu->setPosition(pUIManager->mouseX, pUIManager->mouseY);
			spaceBarMenu->show();
		}

		if (InputManager::isKeyModPress(JA_KMOD_SHIFT) && InputManager::isKeyPressed(JA_8)) {
			hud.setRender(!hud.isRendered());
		}

		if (editMode == LayerObjectEditorMode::SELECT_MODE)
		{
			if (InputManager::isKeyPressed(JA_c)) {
				if (InputManager::isKeyModPress(JA_KMOD_CTRL))
				{
					if (selectedLayerObjects.size() != 0)
					{
						LayerObject* layerObject = selectedLayerObjects.back();
						layerObject->fillLayerObjectDef(*layerObjectDef);
					}
				}
				else
				{
					createLayerObject();
					setEditorMode(LayerObjectEditorMode::TRANSLATE_MODE);
				}
			}

			if (InputManager::isKeyPressed(JA_KP_PLUS))
			{
				applyFilterToSelection(nextLayerFilter);
				if (this->selectedLayerObjects.size() != 0){
					LayerObject* layerObject = this->selectedLayerObjects.back();
					layerObjectMenu->onCreateNotify(layerObject);
					updateComponentEditors(layerObject);
				}
			}

			if (InputManager::isKeyPressed(JA_KP_MINUS))
			{
				applyFilterToSelection(previousLayerFilter);
				if (selectedLayerObjects.size() != 0){
					LayerObject* layerObject = this->selectedLayerObjects.back();
					updateComponentEditors(layerObject);
				}
			}

			if (selectedLayerObjects.size() != 0)
			{
				if (InputManager::isKeyPressed(JA_g))
				{
					saveSelectedLayerObjectsStates();
					setEditorMode(LayerObjectEditorMode::TRANSLATE_MODE);
				}

				if (InputManager::isKeyPressed(JA_r))
				{
					transformationStart = Toolset::getSingleton()->getDrawPoint();
					saveSelectedLayerObjectsStates();
					setEditorMode(LayerObjectEditorMode::ROTATE_MODE);
				}

				if (InputManager::isKeyPressed(JA_s))
				{
					transformationStart = Toolset::getSingleton()->getDrawPoint();
					saveSelectedLayerObjectsStates();
					setEditorMode(LayerObjectEditorMode::SCALE_MODE);
					this->scaleAxis = ScaleMode::SCALE_XY;
				}

				if (InputManager::isKeyPressed(JA_d))
				{
					if (InputManager::isKeyModPress(JA_KMOD_CTRL))
					{
						if (selectedLayerObjects.size() != 0)
						{
							duplicateSelectedAtPoint(Toolset::getSingleton()->getDrawPoint());
						}
					}
					else
					{
						applyFilterToSelection(deleteLayerObjectFilter);
						selectedLayerObjects.clear();
						updateComponentEditors(nullptr);
					}
				}
			}

			if (InputManager::isKeyPressed(JA_ESCAPE)) {
				Toolset::getSingleton()->setRulerActive(false);
				MainMenu::getSingleton()->setGameState(GameState::MAINMENU_MODE);
			}
		}
	}

	if ((editMode == LayerObjectEditorMode::TRANSLATE_MODE) || (editMode == LayerObjectEditorMode::ROTATE_MODE) || (editMode == LayerObjectEditorMode::SCALE_MODE))
	{
		if (InputManager::isKeyPressed(JA_ESCAPE))
		{
			if (editMode == LayerObjectEditorMode::SCALE_MODE)
			{
				if (scaleAxis != ScaleMode::SCALE_XY)
					Toolset::getSingleton()->getRuler()->setActive(false);
			}

			setEditorMode(LayerObjectEditorMode::SELECT_MODE);
		}		
	}

	switch (editMode)
	{
	case LayerObjectEditorMode::SELECT_MODE:
	{
		jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
		layerObjectDef->transform.position = drawPoint;
	}
	break;
	case LayerObjectEditorMode::TRANSLATE_MODE:
		moveLayerObject();
		break;
	case LayerObjectEditorMode::ROTATE_MODE:
		rotateLayerObject();
		break;
	case LayerObjectEditorMode::SCALE_MODE:
		scaleLayerObject();
		break;
	case LayerObjectEditorMode::COMPONENT_EDITOR_MODE:
		currentComponentEditor->handleInput();
		break;
	//case LayerObjectEditorMode::EDIT_MODE:
	//	editPolygon();
	//	break;
	}

	oldDrawPointPosition = Toolset::getSingleton()->getDrawPoint();
	hud.update();
}

void LayerObjectEditor::drawSelectedLayerObjects()
{
	CameraInterface* activeCamera = GameManager::getSingleton()->getActiveCamera();
	jaVector2 originX(1.0f / activeCamera->scale.x, 0.0f);
	jaVector2 originY(0.0f, 1.0f / activeCamera->scale.y);

	uint8_t currentLayerId = LayerObjectManager::getSingleton()->layerObjectDef.layerId;

	if (selectedLayerObjects.size() != 0)
	{
		std::list<LayerObject*>::iterator itLayerObject = selectedLayerObjects.begin();	

		DisplayManager::setLineWidth(3.0f);
		for (itLayerObject; itLayerObject != selectedLayerObjects.end(); ++itLayerObject)
		{
			if (currentLayerId == (*itLayerObject)->layerId)
				DisplayManager::setColorUb(0, 255, 0, 128);
			else
				DisplayManager::setColorUb(255, 255, 0, 128);

			ObjectHandle2d* objectHandle = (*itLayerObject)->getHandle();
			Debug::drawObjectHandle2d(objectHandle, objectHandle->transform);
		}

		itLayerObject = selectedLayerObjects.begin();
		DisplayManager::setColorUb(255, 255, 255);
		DisplayManager::setLineWidth(0.1f);
		for (itLayerObject; itLayerObject != selectedLayerObjects.end(); ++itLayerObject) {
			ObjectHandle2d* objectHandle = (*itLayerObject)->getHandle();

			jaVector2 transformedOriginX = objectHandle->transform.rotationMatrix * originX;
			jaVector2 transformedOriginY = objectHandle->transform.rotationMatrix * originY;

			DisplayManager::drawLine(objectHandle->transform.position.x, objectHandle->transform.position.y, objectHandle->transform.position.x + transformedOriginX.x, objectHandle->transform.position.y + transformedOriginX.y);
			DisplayManager::drawLine(objectHandle->transform.position.x, objectHandle->transform.position.y, objectHandle->transform.position.x + transformedOriginY.x, objectHandle->transform.position.y + transformedOriginY.y);
		}

		DisplayManager::setColorUb(255, 255, 255);
		DisplayManager::setPointSize(7);
		DisplayManager::drawPoint(gizmo.x, gizmo.y);

		DisplayManager::setColorUb(255, 0, 0);
		DisplayManager::setPointSize(5);
		DisplayManager::drawPoint(gizmo.x, gizmo.y);
	}
}

void LayerObjectEditor::drawGizmo()
{
	DisplayManager::setColorUb(255, 255, 255);
	DisplayManager::setPointSize(7);
	DisplayManager::drawPoint(gizmo.x, gizmo.y);

	DisplayManager::setColorUb(255, 0, 0);
	DisplayManager::setPointSize(5);
	DisplayManager::drawPoint(gizmo.x, gizmo.y);
}


void LayerObjectEditor::draw()
{
	CameraInterface* camera = GameManager::getSingleton()->getActiveCamera();
	DisplayManager::pushWorldMatrix();
	DisplayManager::clearWorldMatrix();
	DisplayManager::setCameraMatrix(camera);

	//if (this->drawEntity)
	//	PhysicsManager::getSingleton()->drawUser = EntityEditor::drawPhysicObject;
	//else
	//	PhysicsManager::getSingleton()->drawUser = nullptr;

	if (drawBoxSelection) {
		DisplayManager::setColorUb(255, 0, 128, 255);
		DisplayManager::setLineWidth(3.0f);
		AABB2d boxSelection = AABB2d::make(selectionStartPoint, Toolset::getSingleton()->getDrawPoint());
		Debug::drawAABB2d(boxSelection);

		DisplayManager::setColorUb(255, 255, 255);
		DisplayManager::setPointSize(7);
		DisplayManager::drawPoint(selectionStartPoint.x, selectionStartPoint.y);

		DisplayManager::setColorUb(255, 0, 0);
		DisplayManager::setPointSize(5);
		DisplayManager::drawPoint(selectionStartPoint.x, selectionStartPoint.y);
	}

	drawSelectedLayerObjects();

	if ((this->editMode == LayerObjectEditorMode::ROTATE_MODE) || (this->editMode == LayerObjectEditorMode::SCALE_MODE)) {
		DisplayManager::setColorUb(255, 255, 255);
		DisplayManager::setLineWidth(2.0f);
		jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
		DisplayManager::drawLine(gizmo.x, gizmo.y, drawPoint.x, drawPoint.y);
	}

	if (this->drawPhysics)
	{
		PhysicsManager::getSingleton()->drawUser = nullptr;
		DisplayManager::setLineWidth(1.0f);
		PhysicsManager::getSingleton()->drawDebug(camera);
	}

	if (this->drawHashGrid)
	{
		LayerObjectManager::getSingleton()->drawHashGrid(camera);
	}

	drawGizmo();

	if (editMode == LayerObjectEditorMode::COMPONENT_EDITOR_MODE) {
		currentComponentEditor->render();
	}

	DisplayManager::popWorldMatrix();
	Toolset::getSingleton()->draw();
	Interface::draw();

	hud.draw();
}

void LayerObjectEditor::moveLayerObject()
{
	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();
	
	std::list<LayerObject*>::iterator itLayerObject = this->selectedLayerObjects.begin();

	jaVector2 posDiff;

	for (itLayerObject; itLayerObject != this->selectedLayerObjects.end(); ++itLayerObject)
	{
		ObjectHandle2d* objectHandle = (*itLayerObject)->getHandle();
		posDiff = objectHandle->transform.position - gizmo;
		jaVector2 newPosition = drawPoint + posDiff;
		(*itLayerObject)->setPosition(newPosition);
		//LayerObjectManager::getSingleton()->updateLayerObjectImmediate(*itLayerObject);
	}

	if (LayerObjectEditor::getSingleton()->selectedLayerObjects.size() > 0)
	{
		LayerObject* layerObject = LayerObjectEditor::getSingleton()->selectedLayerObjects.back();
		LayerObjectShapeEditor::getSingleton()->updateWidget(layerObject);
	}

	gizmo = drawPoint;
	Toolset::getSingleton()->worldHud->addInfoC((uint16_t)LayerObjectEditorInfo::TRANSLATE_INFO, gizmo.x, gizmo.y, 0, 255, 0, 5.0f, "(%.4f,%.4f)", gizmo.x, gizmo.y);
}

void LayerObjectEditor::rotateLayerObject()
{
	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

	jaVector2 diff1 = transformationStart - gizmo;
	jaVector2 diff2 = drawPoint - gizmo;

	jaFloat   angle1  = jaVectormath2::getAngle(diff1);
	jaFloat   angle2  = jaVectormath2::getAngle(diff2);
	jaFloat   angle   = jaVectormath2::radiansToDegrees(angle2 - angle1);
	jaMatrix2 rotDiff = jaMatrix2(angle2 - angle1);

	if (InputManager::isKeyPressed(JA_0))
	{
		std::list<LayerObject*>::iterator itLayerObject = selectedLayerObjects.begin();
		for (itLayerObject; itLayerObject != selectedLayerObjects.end(); ++itLayerObject)
		{
			ObjectHandle2d* objectHandle = (*itLayerObject)->getHandle();
			//objectHandle->transform.rotationMatrix = jaMatrix2(0.0f);
			(*itLayerObject)->setAngleDeg(0.0f);

			//LayerObjectManager::getSingleton()->updateLayerObjectImmediate(*itLayerObject);
		}
		setEditorMode(LayerObjectEditorMode::SELECT_MODE);
		return;
	}

	if (InputManager::isKeyModPress(JA_KMOD_LCTRL) || InputManager::isKeyModPress(JA_KMOD_LSHIFT))
	{
		if (InputManager::isKeyModPress(JA_KMOD_LCTRL)) {
			rotDiff = jaMatrix2((float)(math::PI) / 4.0f);
			angle = 45.0f;
		}
		else {
			rotDiff = jaMatrix2((float)(math::PI) / 2.0f);
			angle = 90.0f;
		}
		setEditorMode(LayerObjectEditorMode::SELECT_MODE);
	}

	std::list<LayerObject*>::iterator       itLayerObject = selectedLayerObjects.begin();
	std::vector<LayerObjectState>::iterator itState       = selectedLayerObjectsStates.begin();

	for (itLayerObject; itLayerObject != selectedLayerObjects.end(); ++itLayerObject)
	{
		ObjectHandle2d* objectHandle = (*itLayerObject)->getHandle();
		jaVector2 positionDiff = rotDiff * (itState->position - gizmo);
		objectHandle->transform.position       = gizmo + positionDiff;
		objectHandle->transform.rotationMatrix = rotDiff * itState->rotMatrix;
		(*itLayerObject)->updateTransform();

		++itState;
	}

	if (LayerObjectEditor::getSingleton()->selectedLayerObjects.size() > 0)
	{
		LayerObject* layerObject = LayerObjectEditor::getSingleton()->selectedLayerObjects.back();
		LayerObjectShapeEditor::getSingleton()->updateWidget(layerObject);
	}

	DisplayManager* display = DisplayManager::getSingleton();
	float posX = (float)(display->getResolutionWidth(0.2f));
	float posY = (float)(display->getResolutionHeight(0.2f));
	Toolset::getSingleton()->windowHud->addInfoC((uint16_t)LayerObjectEditorInfo::ROTATE_INFO, posX, posY, 224, 12, 17, 5.0f, "Angle %.4f", angle);
}

void LayerObjectEditor::scaleLayerObject()
{
	ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();

	jaVector2 drawPoint = Toolset::getSingleton()->getDrawPoint();

	const jaVector2 startVec   = transformationStart - gizmo;
	const jaVector2 currentVec = drawPoint - gizmo;

	const jaFloat startVecLength = jaVectormath2::length(startVec);
	const jaFloat currentVecLength = jaVectormath2::length(currentVec);

	jaVector2 scale(currentVecLength / startVecLength, currentVecLength / startVecLength);

	if (InputManager::isKeyPressed(JA_x))
	{
		this->scaleAxis = ScaleMode::SCALE_X;
		ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
		ruler->setPosition(gizmo.x, gizmo.y);
		ruler->setAngle(0);
		ruler->setActive(true);
		ruler->setAlign(RulerAlign::NO_ALIGN);
	}

	if (InputManager::isKeyPressed(JA_y))
	{
		this->scaleAxis = ScaleMode::SCALE_Y;
		ToolsetRuler* ruler = Toolset::getSingleton()->getRuler();
		ruler->setPosition(gizmo.x, gizmo.y);
		ruler->setAngle(90);
		ruler->setActive(true);
		ruler->setAlign(RulerAlign::NO_ALIGN);
	}

	switch (this->scaleAxis)
	{
	case ScaleMode::SCALE_X:
		scale.y = 1.0f;
		break;
	case ScaleMode::SCALE_Y:
		scale.x = 1.0f;
		break;
	}

	if (InputManager::isKeyPressed(JA_1)) {
		std::list<LayerObject*>::iterator itSelection = selectedLayerObjects.begin();
		for (itSelection; itSelection != selectedLayerObjects.end(); ++itSelection)
		{
			LayerObject* layerObject = *itSelection;
			layerObject->setScale(jaVector2(1.0f, 1.0f));
			
			//LayerObjectManager::getSingleton()->updateLayerObjectImmediate(layerObject);
		}
		setEditorMode(LayerObjectEditorMode::SELECT_MODE);
		return;
	}

	bool endScaling = false;

	if (InputManager::isKeyPressed(JA_KP_PLUS))
	{
		if (ruler->isActive()) {
			switch (ruler->alignTo)
			{
			case RulerAlign::ALIGN_X:
				scale.x = 2.0f;
				scale.y = 1.0f;
				break;
			case RulerAlign::ALIGN_Y:
				scale.x = 1.0f;
				scale.y = 2.0f;
				break;
			}
		}
		else
		{
			scale.x = 2.0f;
			scale.y = 2.0f;
		}
		endScaling = true;
	}

	if (InputManager::isKeyPressed(JA_KP_MINUS))
	{
		if (ruler->isActive()) {
			switch (ruler->alignTo)
			{
			case RulerAlign::ALIGN_X:
				scale.x = 0.5f;
				scale.y = 1.0f;
				break;
			case RulerAlign::ALIGN_Y:
				scale.x = 1.0f;
				scale.y = 0.5f;
				break;
			}
		}
		else
		{
			scale.x = 0.5f;
			scale.y = 0.5f;
		}
		endScaling = true;
	}

	std::list<LayerObject*>::iterator   itSelection = selectedLayerObjects.begin();
	std::vector<LayerObjectState>::iterator itSelectionState = selectedLayerObjectsStates.begin();
	for (itSelection; itSelection != selectedLayerObjects.end(); ++itSelection)
	{
		LayerObject*    layerObject  = *itSelection;
		ObjectHandle2d* objectHandle = layerObject->getHandle();

		jaVector2 diff = itSelectionState->position - gizmo;
		diff.x = scale.x * diff.x;
		diff.y = scale.y * diff.y;

		objectHandle->transform.position = gizmo + diff;
		
		jaVector2 scaledScale(scale.x * itSelectionState->scale.x, scale.y * itSelectionState->scale.y);

		layerObject->setScale(scaledScale);

		++itSelectionState;
	}

	if (LayerObjectEditor::getSingleton()->selectedLayerObjects.size() > 0)
	{
		LayerObject* layerObject = LayerObjectEditor::getSingleton()->selectedLayerObjects.back();
		LayerObjectShapeEditor::getSingleton()->updateWidget(layerObject);
	}

	DisplayManager* display = DisplayManager::getSingleton();
	float posX = (float)(display->getResolutionWidth(0.3f));
	float posY = (float)(display->getResolutionHeight(0.3f));
	Toolset::getSingleton()->windowHud->addInfoC((uint16_t)LayerObjectEditorInfo::SCALE_INFO, posX, posY, 255, 255, 0, 5.0f, "Scale (%.4f,%.4f)", scale.x, scale.y);

	if (endScaling)
		setEditorMode(LayerObjectEditorMode::SELECT_MODE);
}

void LayerObjectEditor::setLayerObjectType(LayerObjectType layerObjectType)
{
	this->layerObjectDef->layerObjectType = (uint8_t)layerObjectType;
}

void LayerObjectEditor::createLayerObject()
{
	selectedLayerObjectsTemp->clear();
	selectedLayerObjects.clear();

	ja::LayerObject* layerObject = (ja::Polygon*)LayerObjectManager::getSingleton()->createLayerObjectDef();
	layerObject->depth    = layerObject->getId();
	layerObjectDef->depth = layerObject->getId();

	selectedLayerObjects.push_back(layerObject);
	calculateSelectionMidpoint();

	onCreateNotifyComponentEditors(layerObject);
	updateComponentEditors(layerObject);
}

void LayerObjectEditor::setEditorMode(LayerObjectEditorMode mode)
{
	if (editMode == LayerObjectEditorMode::COMPONENT_EDITOR_MODE)
		currentComponentEditor->setActiveMode(false);

	editMode = mode;

	switch (editMode)
	{
	case LayerObjectEditorMode::SELECT_MODE:
		hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=Select Layer Object");
		hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "(LShift) + (LCtrl) + LMB - Select\nAlt + LMB - Change gizmo position\nC - Create polygon\nTab - Edit mode\nS - Scale\nR - Rotate\nD - Delete polygon\nCtrl + C - Copy\nCtrl + D - Duplicate\nESC - Cancel");
		break;
	case LayerObjectEditorMode::TRANSLATE_MODE:
		hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=Translate Layer Object");
		hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "LMB - Accept\nESC - Cancel");
		break;
	case LayerObjectEditorMode::ROTATE_MODE:
		hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=Rotate Layer Object");
		hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "LMB - Accept\n0 - Set 0 Angle\nESC - Cancel");
		break;
	case LayerObjectEditorMode::SCALE_MODE:
		hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=Scale Layer Object");
		hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "LMB - Accept\n+,- - Size\n1 - Scale to 1\nESC - Cancel");
		break;
	//case LayerObjectEditorMode::EDIT_MODE:
	//	hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_MODE_HUD, "Mode=Edit");
	//	hud.setFieldValue((uint32_t)LayerObjectEditorHud::EDITOR_HELPER_HUD, "ESC - Accept");
		break;
	}
}

void LayerObjectEditor::setEditorMode(CommonWidget* commonWidget)
{
	if (editMode == LayerObjectEditorMode::COMPONENT_EDITOR_MODE)
	{
		currentComponentEditor->setActiveMode(false);
		setEditorMode(LayerObjectEditorMode::SELECT_MODE);
		return;
	}

	editMode = LayerObjectEditorMode::COMPONENT_EDITOR_MODE;
	currentComponentEditor = commonWidget;
	currentComponentEditor->setActiveMode(true);
}

LayerObjectEditorMode LayerObjectEditor::getEditorMode()
{
	return this->editMode;
}

CommonWidget* LayerObjectEditor::getCurrentEditor()
{
	if (editMode == LayerObjectEditorMode::COMPONENT_EDITOR_MODE)
	{
		return this->currentComponentEditor;
	}

	return nullptr;
}

void LayerObjectEditor::onMouseOver(Widget* sender, WidgetEvent action, void* data)
{
	if (sender->getWidgetType() == JA_BUTTON)
	{
		Button* button = static_cast<Button*>(sender);
		button->setColor(0, 255, 0);
	}
}

void LayerObjectEditor::onMouseLeave(Widget* sender, WidgetEvent action, void* data)
{
	if (sender->getWidgetType() == JA_BUTTON)
	{
		Button* button = static_cast<Button*>(sender);
		button->setColor(255, 0, 0);
	}
}

jaVector2 LayerObjectEditor::getGizmoPosition()
{
	return gizmo;
}

LayerObjectDef* LayerObjectEditor::getLayerObjectDef()
{
	return this->layerObjectDef;
}

void LayerObjectEditor::gatherResources()
{
	this->layerObjectMenu->reloadResources();
}

void LayerObjectEditor::createGUI()
{
	this->layerObjectBar  = new SlideBox(0);
	this->layerObjectMenu = new LayerObjectComponentMenu(1, this);

	this->layerObjectBar->dockTo((uint32_t)Margin::TOP);

	FlowLayout* menuBar = new FlowLayout(0, nullptr, (uint32_t)Margin::LEFT, 5, 0);
	menuBar->setColor(128, 128, 128, 128);
	menuBar->setSize(width, 28);
	menuBar->setPosition(0, height - 28);

	Button* menuButton = new Button(1, nullptr);
	menuButton->setFont("default.ttf",10);
	menuButton->setText("Menu");
	menuButton->setColor(0, 0, 255, 255);
	menuButton->setFontColor(255, 255, 255, 255);
	menuButton->adjustToText(10, 28);
	menuButton->vUserData = this->layerObjectMenu;
	menuButton->setOnClickEvent(this, &LayerObjectEditor::onClickMenuBar);
	menuBar->addWidget(menuButton);

	spaceBarMenu = new LayerObjectSpaceBarMenu(3, nullptr);
	spaceBarMenu->hide();

	this->layerObjectBar->setSlideArea(10);
	this->layerObjectBar->setArea(28);
	this->layerObjectBar->setBox(menuBar);

	addWidget(this->layerObjectMenu);
	addWidget(this->layerObjectBar);
	addWidget(this->spaceBarMenu);
}

void LayerObjectEditor::calculateSelectionMidpoint()
{
	std::list<LayerObject*>::iterator itLayerObject = selectedLayerObjects.begin();
	gizmo.setZero();

	for (itLayerObject; itLayerObject != selectedLayerObjects.end(); ++itLayerObject)
	{
		ObjectHandle2d* objectHandle = (*itLayerObject)->getHandle();
		gizmo += objectHandle->transform.position;
	}

	gizmo = gizmo / (jaFloat)(selectedLayerObjects.size());
}

void LayerObjectEditor::addToSelection(StackAllocator<ObjectHandle2d*>* selection)
{
	ObjectHandle2d** objectHandles = selection->getFirst();
	EffectManager*   effectManager = EffectManager::getSingleton();

	for (uint32_t i = 0; i<selection->getSize(); ++i)
	{
		LayerObject* layerObject = (LayerObject*)(objectHandles[i]->getUserData());
		Layer*       layer       = effectManager->getLayer(layerObject->layerId);

		if (!layer->isActive)
			continue;

		selectedLayerObjects.remove(layerObject);
		selectedLayerObjects.push_back(layerObject);
	}
}

void LayerObjectEditor::addToSelection(LayerObject* layerObject)
{
	Layer* layer = EffectManager::getSingleton()->getLayer(layerObject->layerId);

	if (!layer->isActive)
		return;

	selectedLayerObjects.remove(layerObject);
	selectedLayerObjects.push_back(layerObject);
}

void LayerObjectEditor::updateComponentEditors(LayerObject* layerObject)
{
	if (layerObject != nullptr)
		hud.setFieldValuePrintC((uint32_t)LayerObjectEditorHud::ID_HUD, "Id=%hu", layerObject->getId());
	else
		hud.setFieldValue((uint32_t)LayerObjectEditorHud::ID_HUD, "Id=");

	layerObjectMenu->updateComponentEditors(layerObject);
}

void LayerObjectEditor::onCreateNotifyComponentEditors(ja::LayerObject* layerObject)
{
	layerObjectMenu->onCreateNotify(layerObject);
}

void LayerObjectEditor::onDeleteNotifyComponentEditors(ja::LayerObject* layerObject)
{
	layerObjectMenu->onDeleteNotify(layerObject);
}

void LayerObjectEditor::removeFromSelection(StackAllocator<ObjectHandle2d*>* selection)
{
	ObjectHandle2d** objectHandles = selection->getFirst();
	EffectManager*   effectManager = EffectManager::getSingleton();

	for (uint32_t i = 0; i<selection->getSize(); ++i)
	{
		Layer* layer = effectManager->getLayer(i);

		if (!layer->isActive)
			return;

		LayerObject* layerObject = (LayerObject*)(objectHandles[i]->getUserData());
		selectedLayerObjects.remove(layerObject);
	}
}

void LayerObjectEditor::removeFromSelection(LayerObject* layerObject)
{
	Layer* layer = EffectManager::getSingleton()->getLayer(layerObject->layerId);

	if (!layer->isActive)
		return;

	selectedLayerObjects.remove(layerObject);
}

void LayerObjectEditor::saveSelectedLayerObjectsStates()
{
	std::list<LayerObject*>::iterator itSelection = selectedLayerObjects.begin();
	selectedLayerObjectsStates.clear();

	for (itSelection; itSelection != selectedLayerObjects.end(); ++itSelection)
	{
		LayerObjectState layerObjectState;
		LayerObject*     layerObject = *itSelection;

		ObjectHandle2d* objectHandle = layerObject->getHandle();
		layerObjectState.position  = objectHandle->transform.position;
		layerObjectState.rotMatrix = objectHandle->transform.rotationMatrix;
		layerObjectState.scale     = layerObject->getScale();

		selectedLayerObjectsStates.push_back(layerObjectState);
	}
}

void LayerObjectEditor::applyFilterToSelection(LayerObjectFilterPtr polygonFilter)
{
	std::list<LayerObject*>::iterator itLayerObject = selectedLayerObjects.begin();

	for (itLayerObject; itLayerObject != selectedLayerObjects.end(); ++itLayerObject)
	{
		polygonFilter(*itLayerObject);
	}
}

void LayerObjectEditor::deleteLayerObjectFilter(LayerObject* layerObject)
{
	jaVector2 position = layerObject->getPosition();

	if (layerObject->getComponentData() == nullptr)
	{
		layerObject->deleteLayerObject();
		LayerObjectEditor::getSingleton()->onDeleteNotifyComponentEditors(layerObject);
		Toolset::getSingleton()->worldHud->addInfo((uint16_t)LayerObjectEditorInfo::DELETE_INFO * setup::graphics::MAX_LAYER_OBJECTS + layerObject->getId(), position.x, position.y, 255, 0, 0, 5.0f, "Deleted");
	}
	else
	{
		Toolset::getSingleton()->worldHud->addInfo((uint16_t)LayerObjectEditorInfo::DELETE_INFO * setup::graphics::MAX_LAYER_OBJECTS + layerObject->getId(), position.x, position.y, 255, 0, 0, 5.0f, "Can't delete");
	}
}

void LayerObjectEditor::nextLayerFilter(LayerObject* layerObject)
{
	EffectManager* effectManager = EffectManager::getSingleton();
	uint8_t        layersNum     = effectManager->getLayersNum();

	layerObject->layerId++;
	layerObject->layerId %= EffectManager::getSingleton()->getLayersNum();

	LayerObjectEditor::getSingleton()->layerObjectDef->layerId = layerObject->layerId;

	Layer* layer = effectManager->getLayer(layerObject->layerId);

	jaVector2 position = layerObject->getPosition(); 
	Toolset::getSingleton()->worldHud->addInfoC((uint16_t)LayerObjectEditorInfo::LAYER_INFO * setup::graphics::MAX_LAYER_OBJECTS + layerObject->getId(), position.x, position.y, 0, 255, 0, 5.0f, "Layer %s", layer->getName());
}

void LayerObjectEditor::previousLayerFilter(LayerObject* layerObject)
{
	EffectManager* effectManager = EffectManager::getSingleton();
	uint8_t        layersNum     = effectManager->getLayersNum();

	if (0 == layersNum)
		return;

	if (0 == layerObject->layerId)
	{
		layerObject->layerId = layersNum - 1;
	}
	else
	{
		layerObject->layerId = --layerObject->layerId;
	}	 

	LayerObjectEditor::getSingleton()->layerObjectDef->layerId = layerObject->layerId;

	Layer* layer = effectManager->getLayer(layerObject->layerId);

	jaVector2 position = layerObject->getPosition();
	Toolset::getSingleton()->worldHud->addInfoC((uint16_t)LayerObjectEditorInfo::LAYER_INFO * setup::graphics::MAX_LAYER_OBJECTS + layerObject->getId(), position.x, position.y, 0, 255, 0, 5.0f, "Layer %s", layer->getName());
}

void LayerObjectEditor::duplicateSelectedAtPoint(jaVector2& point)
{
	std::list<LayerObject*>::iterator itSelected = selectedLayerObjects.begin();
	std::list<LayerObject*> layerObjectsTemp;
	std::list<LayerObject*>::iterator itLayerObject;

	for (itSelected; itSelected != selectedLayerObjects.end(); ++itSelected)
	{
		(*itSelected)->fillLayerObjectDef(*layerObjectDef);
		layerObjectDef->transform.position -= gizmo - point;
		ja::Polygon* polygon = (ja::Polygon*)LayerObjectManager::getSingleton()->createLayerObjectDef();
		layerObjectsTemp.push_back(polygon);
	}

	selectedLayerObjects.clear();
	itLayerObject = layerObjectsTemp.begin();
	for (itLayerObject; itLayerObject != layerObjectsTemp.end(); ++itLayerObject)
	{
		selectedLayerObjects.push_back(*itLayerObject);
	}
	calculateSelectionMidpoint();
}


void LayerObjectEditor::onClickMenuBar(Widget* sender, WidgetEvent action, void* data)
{
	Button* menuButton = static_cast<Button*>(sender);

	Widget* linkedWidget = static_cast<Widget*>(menuButton->vUserData);
	linkedWidget->setRender(true);
	linkedWidget->setActive(true);
}


}