#pragma once

#include "../../Engine/Gui/DebugHud.hpp"
#include "../../Engine/Gui/Menu.hpp"
#include "../../Engine/Gui/Widget.hpp"

#include "../../Engine/Gui/SlideBox.hpp"
#include "../../Engine/Math/VectorMath2d.hpp"
#include "../../Engine/Gilgamesh/ObjectHandle2d.hpp"
#include "../../Engine/Allocators/StackAllocator.hpp"

#include "../../Engine/Graphics/Skeleton.hpp"

#include "AnimationComponents.hpp"

#include <vector>

#include <stdint.h>

namespace tr
{

	enum class AnimationEditorMode
	{
		SELECT_MODE = 0,
		TRANSLATE_MODE = 1,
		ROTATE_MODE = 2,
		SCALE_MODE = 3,
		COMPONENT_EDITOR_MODE = 4
	};

	enum class AnimationEditorInfo
	{
		TRANSLATE_INFO = 0,
		ROTATE_INFO = 1,
		SCALE_INFO = 2,
		TEX_SCALE_INFO = 3,
		DELETE_INFO = 4,
		LAYER_INFO = 5,
		COMPONENT_EDIT_INFO = 6
	};

	enum class AnimationEditorHud
	{
		ID_HUD = 0,
		ANGLE_HUD = 1,
		POSITION_HUD = 2,
		SCALE_HUD = 3,
		//LAYER_HUD = 4,
		EDITOR_MODE_HUD = 5,
		EDITOR_HELPER_HUD = 6,
		//DEPTH_HUD = 7,
		//POLY_TYPE_HUD = 8,
		EDITOR_STACIC_HELPER_HUD = 9
	};

	class SkeletonState
	{
	public:
		jaVector2 position;
		//jaMatrix2 rotMatrix;
		//jaVector2 scale;
	};

	//typedef void(*trPolygonFilterPtr)(ja::Polygon*);

	class AnimationEditor : public ja::Menu
	{
	private:
		ja::SkeletonDef* skeletonDef;

		//Gui
		bool objectSpace;

		int32_t mousePosX, mousePosY;

		AnimationEditorMode editMode;
		jaVector2 gizmo;
		jaVector2 oldDrawPointPosition;

		bool      drawBoxSelection;
		bool      drawPhysics;
		//bool      drawEntity;
		bool      drawHud;
		bool      drawHashGrid;
		jaVector2 selectionStartPoint;

		AnimationComponentMenu* animationMenu;
		AnimationSpaceBarMenu*  spaceBarMenu;

		ja::SlideBox*           animationBar;
		CommonWidget*		    currentComponentEditor;

		StackAllocator<ja::Skeleton*> selectedSkeletonsTemp;
	public:
		std::list<ja::Skeleton*>       selectedSkeletons;
		std::vector<SkeletonState> selectedSkeletonStates;
	private:

		static AnimationEditor* singleton;
		AnimationEditor();
		void createGUI();
		void calculateSelectionMidpoint();

		void drawGizmo();
		void addToSelection(StackAllocator<ja::Skeleton*>* selection);

		void updateComponentEditors(ja::Skeleton* skeleton);
		void removeFromSelection(StackAllocator<ja::Skeleton*>* selection);
	public:
		ja::DebugHud hud;

		//void addToSelection(ja::Skeleton* skeleton);
		//void removeFromSelection(ja::Skeleton* skeleton);

		static void init();
		static AnimationEditor* getSingleton();
		static void cleanUp();

		void setDefaultParameters();
		void setEditorMode(AnimationEditorMode mode);
		void setEditorMode(CommonWidget* commonWidget);
		AnimationEditorMode getEditorMode();
		CommonWidget*       getCurrentEditor();

		jaVector2       getGizmoPosition();

		void moveSkeleton();

		void createSkeleton();
		void saveSelectedSkeletonStates();

		void update();
		void drawSelectedSkeletons();
		void draw();

		void onClickMenuBar(ja::Widget* sender, ja::WidgetEvent action, void* data);

		~AnimationEditor();
	};

}