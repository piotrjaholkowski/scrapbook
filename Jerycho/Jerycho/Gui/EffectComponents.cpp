#include "EffectComponents.hpp"
#include "WidgetId.hpp"
#include "../../Engine/Gui/BoxLayout.hpp"
#include "../../Engine/Gui/Margin.hpp"
#include "../../Engine/Gui/Button.hpp"

using namespace ja;

namespace tr
{ 

EffectComponentMenu::EffectComponentMenu( unsigned int widgetId, Widget* parent ) : CommonWidget(widgetId,"Menu",parent)
{
	BoxLayout* boxLayout = new BoxLayout(0, nullptr, (uint32_t)Margin::LEFT, false, 0, 1);
	boxLayout->setSize(150,200);
	fillBoxLayout(boxLayout);

	Button* layerButton = new Button((uint32_t)TR_EFFECTEDITOR_ID::LAYER_EDITOR, nullptr);
	fillButton(layerButton,"Layers");
	layerButton->setOnClickEvent(this, &EffectComponentMenu::onMenuClick);
	boxLayout->addWidget(layerButton);

	addWidgetToEditor(boxLayout);
}

void EffectComponentMenu::onMenuClick( Widget* sender, WidgetEvent action, void* data )
{
	uint32_t id = sender->getId();
	components[id]->setActive(true);
	components[id]->setRender(true);
}



}