#pragma once

#include "WidgetId.hpp"
#include "CommonWidgets.hpp"
#include "Toolset.hpp"

#include "../../Engine/Gui/GridLayout.hpp"

#include <stdint.h>

namespace tr
{

class ResourceComponentMenu : public CommonWidget
{
private:
	static ResourceComponentMenu* singleton;
	CommonWidget* components[20];
public:
	ResourceComponentMenu(uint32_t widgetId, ja::Widget* parent);
	void onMenuClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	void updateComponentEditors(void* data);

	static inline ResourceComponentMenu* getSingleton()
	{
		return singleton;
	}
};

class ResourceSceneEditor : public CommonWidget
{
private:
	static ResourceSceneEditor* singleton;

	ja::TextField* sceneNameTextfield;
	ja::Button*    saveButton;
public:
	ResourceSceneEditor(uint32_t widgetId, ja::Widget* parent);

	void updateWidget(void* data);

	void onNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);

	static inline ResourceSceneEditor* getSingleton()
	{
		return singleton;
	}
};

enum class ResourceType
{
	LAYER_OBJECT  = 0,
	PHYSIC_OBJECT = 1,
	ENTITY = 2
};

class ResourceEntry
{
public:
	uint8_t resourceType;
	void*   resourceData;

	ResourceEntry(uint8_t resourceType, void* resourceData) : resourceType(resourceType), resourceData(resourceData)
	{

	}
};

class ResourceExporterEditor : public CommonWidget
{
private:
	static ResourceExporterEditor* singleton;

	ja::TextField* fileNameTextfield;
	ja::Button*    exportButton;

	ja::GridViewer* selectedResourceGridViewer;
	ja::Checkbox*   exportRelatedCheckbox;
	ja::Label       resourceLabel;

	std::vector<void*>            selectedResourcesGridList;
	StackAllocator<ResourceEntry> selectedResources;
public:
	ResourceExporterEditor(uint32_t widgetId, ja::Widget* parent);

	void updateWidget(void* data);

	void onNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
	void onDrawResource(ja::Widget* sender, ja::WidgetEvent action, void* data);
	void onExportClick(ja::Widget* sender, ja::WidgetEvent action, void* data);

	static inline ResourceExporterEditor* getSingleton()
	{
		return singleton;
	}
};

class ResourceSpaceBarMenu : public ja::GridLayout
{
	ja::Button* addShapeButton;

public:
	ResourceSpaceBarMenu(uint32_t widgetId, ja::Widget* parent);
	virtual ~ResourceSpaceBarMenu();

	void hide();
	void show();
	void update();
};

}

