#pragma once

#include "CommonWidgets.hpp"
#include "../../Engine/Gui/GridLayout.hpp"
#include "../../Engine/Entity/Entity.hpp"
#include "../../Engine/Allocators/StackAllocator.hpp"
#include "../../Engine/Allocators/PoolAllocator.hpp"
#include "../../Engine/Utility/ListElement.hpp"
#include <list>

namespace tr
{

	class PhysicsSceneMenu : public CommonWidget
	{
	private:
		ja::TextField* cellWidthTextfield;
		ja::TextField* cellHeightTextfield;
		ja::TextField* cellSizeTextfield;
		ja::Button*    cellRebuildButton;
		ja::Button*    cellRefreshButton;
	public:
		PhysicsSceneMenu(uint32_t widgetId, ja::Widget* parent);
		void reloadResources();
		void onCellWidthDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCellHeightDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCellSizeDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onRefreshClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onRebuildClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class PhysicsTimerMenu : public CommonWidget
	{
	private:
		ja::Button* resetButton;
		ja::Label*  frameElapsedLabel;
		uint32_t    framesPassed;
	public:
		PhysicsTimerMenu(uint32_t widgetId, ja::Widget* parent);
		void increaseFramePassed();
		void onClickReset(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class PhysicsComponentMenu : public CommonWidget
	{
	private:
		static PhysicsComponentMenu* singleton;
		CommonWidget* components[20];
	public:
		PhysicsComponentMenu(uint32_t widgetId, ja::Widget* parent);
		void onMenuClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void updateComponentEditors(ja::PhysicObject2d* physicObject);

		static inline PhysicsComponentMenu* getSingleton()
		{
			return singleton;
		}
	};

	class MaterialEditor : public CommonWidget
	{
	private:
		static MaterialEditor* singleton;
		ja::TextField* restitutionTextField;
		ja::TextField* frictionTextField;
		ja::TextField* densityTextField;
		ja::Label*     massValueLabel;

		ja::Scrollbar* restitutionScrollbar;
		ja::Scrollbar* frictionScrollbar;
		jaFloat density;
		jaFloat restitution;
		jaFloat friction;

	public:
		MaterialEditor(uint32_t widgetId, ja::Widget* parent);
		void updateWidget(void* data);
		void onChangeDensity(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onChangeFriction(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onChangeRestitution(ja::Widget* sender, ja::WidgetEvent action, void* data);
		static void changeDensityFilter(ja::PhysicObject2d* physicObject);
		static void changeFrictionFilter(ja::PhysicObject2d* physicObject);
		static void changeResitutionFilter(ja::PhysicObject2d* physicObject);

		inline static MaterialEditor* getSingleton()
		{
			return singleton;
		}
	};

	class BodyTypeEditor : public CommonWidget
	{
	private:
		static BodyTypeEditor* singleton;
		ja::RadioButton* staticButton;
		ja::RadioButton* dynamicButton;
		ja::RadioButton* kinematicButton;
		ja::Checkbox*    isSleepingCheckbox;
		ja::Checkbox*    isSleepingAllowedCheckbox;
		ja::BodyType     bodyType;
		bool isSleeping;
		bool isSleepingAllowed;
	public:
		BodyTypeEditor(uint32_t widgetId, ja::Widget* parent);
		void onClickBodyType(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onChangeSleep(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onChangeAllowSleep(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void updateWidget(void* data);
		static void bodyTypeFilter(ja::PhysicObject2d* physicObject);
		static void sleepFilter(ja::PhysicObject2d* physicObject);
		static void allowSleepFilter(ja::PhysicObject2d* physicObject);
	};

	class BoxEditor : public CommonWidget
	{
	private:
		static BoxEditor* singleton;

		ja::TextField* widthTextfield;
		ja::TextField* heightTextfield;
		ja::Button*    createButton;

		jaFloat width;
		jaFloat height;
	public:
		BoxEditor(uint32_t widgetId, ja::Widget* parent);

		void updateWidget(void* data);
		void onWidthDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onHeightDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCreateClick(ja::Widget* sender, ja::WidgetEvent action, void* data);

		static void widthFilter(ja::PhysicObject2d* physicObject);
		static void heightFilter(ja::PhysicObject2d* physicObject);

		static inline BoxEditor* getSingleton()
		{
			return singleton;
		}
	};

	class CircleEditor : public CommonWidget
	{
	private:
		static CircleEditor* singleton;

		ja::TextField* radiusTextfield;
		ja::Button*    createButton;
		jaFloat        radius;
	public:
		CircleEditor(uint32_t widgetId, ja::Widget* parent);

		void updateWidget(void* data);
		void onRadiusDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCreateClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		static void radiusFilter(ja::PhysicObject2d* physicObject);

		static inline CircleEditor* getSingleton()
		{
			return singleton;
		}
	};

	class ConvexPolygonEditor : public CommonWidget
	{
	private:
		static ConvexPolygonEditor* singleton;

		float infoPosX;
		float infoPosY;

		ja::Label*  verticesNumLabel;
		ja::Button* editModeButton;
		ja::Button* setAsTriangleButton;
		ja::Button* setAsBoxButton;
	
		ja::PhysicObject2d* editedPhysicObject;

		PoolAllocator<ja::ListElement<jaVector2>>* verticesAllocator;
		uint32_t verticesNum;
		ja::ListElement<jaVector2>* firstVertex;
		ja::ListElement<jaVector2>* editedVertex;
		ja::ListElement<jaVector2>* nearestVertex;
		ja::ListElement<jaVector2>* nearestConnected;;
		
		jaVector2 castedPoint;

	public:
		ConvexPolygonEditor(uint32_t widgetId, ja::Widget* parent);
		~ConvexPolygonEditor();

		void render();
		void handleInput();
		void setActiveMode(bool state);

		void updateWidget(void* data);
		void editModeClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void setAsTriangleClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void setAsBoxClick(ja::Widget* sender, ja::WidgetEvent action, void* data);

		jaVector2                   findCastedPoint(const jaVector2& point, ja::ListElement<jaVector2>* closestVertex, ja::ListElement<jaVector2>** insertVertexIt);
		ja::ListElement<jaVector2>* findNearestVertex(jaVector2& point);

		bool isEdgeIntersectingWithPoly(jaVector2& lineA0, jaVector2& lineA1);
		bool canMakeTriangle(jaVector2& p0, jaVector2& p1, jaVector2& p2);
		void getShapeInfo(ja::PhysicObject2d* physicObject);
		void setShapeInfo(ja::PhysicObject2d* physicObject);
		void updateVerticesNumLabel();

		static inline ConvexPolygonEditor* getSingleton()
		{
			return singleton;
		}

		static ja::ListElement<jaVector2>* fillWithLocalCoordinates(ja::ListElement<jaVector2>* firstConvexVertex, uint32_t convexVerticesNum, const jaVector2& shapeCenter, jaVector2* vertices);
		static jaVector2                   getShapeCenter(ja::ListElement<jaVector2>* firstVertex, int32_t verticesNum);
	private:

		void  reconnectVertices(ja::ListElement<jaVector2>* firstConvexVertex, ja::ListElement<jaVector2>* lastConvexVertex);
	};

	class TriangleMeshEditor : public CommonWidget
	{
	private:
		static TriangleMeshEditor* singleton;

		float infoPosX;
		float infoPosY;

		MeshEditor* meshEditor;

		ja::Label*  verticesNumLabel;
		ja::Label*  trianglesNumLabel;
		ja::Button* editButton;
		ja::Button* makeAsPolygonButton;

		ja::PhysicObject2d* editedPhysicObject;

		uint32_t verticesNum;

	public:
		TriangleMeshEditor(uint32_t widgetId, ja::Widget* parent);
		~TriangleMeshEditor();

		void updateWidget(void* data);
		void onMakeAsPolygonClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onEditClick(ja::Widget* sender, ja::WidgetEvent action, void* data);

		static inline TriangleMeshEditor* getSingleton()
		{
			return singleton;
		}
	};

	class PointEditor : public CommonWidget
	{
	private:
		static PointEditor* singleton;

		float infoPosX;
		float infoPosY;

		ja::Button* createButton;
	public:
		PointEditor(uint32_t widgetId, ja::Widget* parent);
		~PointEditor();

		void updateWidget(void* data);

		static inline PointEditor* getSingleton()
		{
			return singleton;
		}

		void onCreateClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class ShapeEditor : public CommonWidget
	{
	private:
		static ShapeEditor* singleton;
		
		ja::TextField* xTextfield;
		ja::TextField* yTextfield;
		ja::TextField* xComTextfield;
		ja::TextField* yComTextfield;
		ja::TextField* angleTextfield;
		ja::TextField* massOfInteriaTextfield;

		ja::Button* boxButton;
		ja::Button* circleButton;
		ja::Button* convexPolygonButton;
		ja::Button* triangleMeshButton;
		ja::Button* pointButton;

		ja::Button* combineButton;
		ja::Button* decomposeButton;
		ja::Button* editModeButton;

		ja::Button* applyCofButton;
		ja::Button* editCofButton;
		ja::Button* resetCofButton;

		jaFloat x;
		jaFloat y;
		jaFloat angleDeg;
	public:
		ShapeEditor(uint32_t widgetId, ja::Widget* parent);
		void updateWidget(void* data);
		void resetClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void combineClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void decomposeClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void xDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void yDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void angleDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		static void xFilter(ja::PhysicObject2d* physicObject);
		static void yFilter(ja::PhysicObject2d* physicObject);
		static void angleFilter(ja::PhysicObject2d* physicObject);

		static inline ShapeEditor* getSingleton()
		{
			return singleton;
		}
		// dodac nowe opcje do rozwijanego menu z prawego przycisku myszy
	};

	class VelocityEditor : public CommonWidget
	{
	private:
		static VelocityEditor* singleton;
		ja::TextField* linXTextfield;
		ja::TextField* linYTextfield;
		ja::TextField* angularTextfield;
		jaVector2      linearVelocity;
		jaFloat        angularVelocity;
	public:
		VelocityEditor(uint32_t widgetId, Widget* parent);
		void updateWidget(void* data);
		void linXVelDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void linYVelDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void angVelDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		static void linVelocityXFilter(ja::PhysicObject2d* physicObject);
		static void linVelocityYFilter(ja::PhysicObject2d* physicObject);
		static void angVelocityFilter(ja::PhysicObject2d* physicObject);
	};

	class DampingEditor : public CommonWidget
	{
	private:
		static DampingEditor* singleton;
		ja::TextField* linDampingTextfield;
		ja::TextField* angDampingTextfield;
		jaFloat        linearDamping;
		jaFloat        angularDamping;
	public:
		DampingEditor(uint32_t widgetId, Widget* parent);
		void updateWidget(void* data);
		void linDampingDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void angDampingDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		static void linDampingFilter(ja::PhysicObject2d* physicObject);
		static void angDampingFilter(ja::PhysicObject2d* physicObject);
	};

	class LinearMotorEditor : public CommonWidget
	{
	private:
		static LinearMotorEditor* singleton;

		ja::TextField* posXTextfield;
		ja::TextField* posYTextfield;
		ja::TextField* norXTextfield;
		ja::TextField* norYTextfield;
		ja::TextField* maxImpulseTextfield;
		ja::TextField* accelerationTextfield;
		ja::Button*    editModeButton;
		ja::Button*    createMotorButton;
		ja::Button*    deleteMotorButton;
	public:
		LinearMotorEditor(uint32_t widgetId, ja::Widget* parent);
		void render();
		void handleInput();
		void setActiveMode(bool state);

		void editModeClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void createMotorClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void deleteMotorClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class DistanceJointEditor : public CommonWidget
	{
	private:
		static DistanceJointEditor* singleton;
		ja::Button* editModeButton;
		ja::Button* createMotorButton;

		float infoPosX;
		float infoPosY;
		ja::DistanceJoint2d* activeDistanceJoint = nullptr;
		bool anchorPointEdited;
		jaVector2 anchorPointA;
		jaVector2 anchorPointB;
		int32_t editedAnchorPointId = 0;
		std::list<ja::DistanceJoint2d*> distanceJoints;
	public:
		DistanceJointEditor(uint32_t widgetId, ja::Widget* parent);
		void render();
		void handleInput();
		void setActiveMode(bool state);

		void editModeClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void createJointClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		//void deleteJointClick(Widget* sender, WidgetEvent action, void* data);
	};

	class CollisionMaskEditor : public CommonWidget
	{
	private:
		static CollisionMaskEditor* singleton;

		float infoPosX;
		float infoPosY;
	public:
		CollisionMaskEditor(uint32_t widgetId, ja::Widget* parent);
		void render();
		void handleInput();
		void setActiveMode(bool state);
	};

	class ShapeAddMenu : public ja::GridLayout
	{
		ja::Button* circleButton;
		ja::Button* boxButton;
		ja::Button* polygonButton;
		ja::Button* pointButton;
	public:
		ShapeAddMenu(uint32_t widgetId, ja::Widget* parent);
		virtual ~ShapeAddMenu();
		
		void createShapeClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class PhysicsSpaceBarMenu : public ja::GridLayout
	{
		ja::Button* addShapeButton;

	public:
		PhysicsSpaceBarMenu(uint32_t widgetId, ja::Widget* parent);
		virtual ~PhysicsSpaceBarMenu();

		void hide();
		void show();
		void update();
	};
}
