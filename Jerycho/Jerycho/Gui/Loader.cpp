#include "Loader.hpp"

#include "../../Engine/Manager/ScriptManager.hpp"
#include "MainMenu.hpp"
#include "WidgetId.hpp"

namespace tr
{

Loader::Loader() : Menu((uint32_t)TR_MENU_ID::LOADER, nullptr)
{
	int32_t widthR, heightR;
	DisplayManager::getResolution(&widthR, &heightR);
	setText("Loader");
	setPosition(widthR / 2, heightR / 2);

	finishedTaskLabel = new Label((uint32_t)TR_LOADER_ID::FINISHED_TASK_LABEL, this);
	finishedTaskLabel->setPosition((-widthR / 2) + 5, -(heightR / 2) + 5);
	finishedTaskLabel->setFont("default.ttf",10);
	finishedTaskLabel->setColor(255, 255, 255, 255);
	finishedTaskLabel->setShadowColor(255, 0, 0, 128);
	finishedTaskLabel->setShadowOffset(-2, -2);
	finishedTaskLabel->setAlign((uint32_t)Margin::LEFT | (uint32_t)Margin::DOWN);
	finishedTaskLabel->setText("");
	addWidget(finishedTaskLabel);

	gradientImage = new ImageButton((uint32_t)TR_LOADER_ID::GRADIENT_IMAGEBUTTON, this);
	gradientImage->setPosition((-widthR / 2), -(heightR / 2) + 5);
	gradientImage->setImage("gui/gradient.png");
	gradientImage->setSize(widthR, heightR);
	gradientImage->setImageSize(widthR, heightR);
	gradientImage->setColor(0, 0, 0, 0);
	addWidget(gradientImage);

	failureTaskLabel = new Label((uint32_t)TR_LOADER_ID::FAILURE_TASK_LABEL, this);
	failureTaskLabel->setPosition((widthR / 2) - 5, -(heightR / 2) + 5);
	failureTaskLabel->setFont("default.ttf",10);
	failureTaskLabel->setColor(255, 0, 0, 255);
	failureTaskLabel->setShadowColor(255, 0, 0, 128);
	failureTaskLabel->setShadowOffset(-2, -2);
	failureTaskLabel->setAlign((uint32_t)Margin::RIGHT | (uint32_t)Margin::DOWN);
	failureTaskLabel->setText("");
	addWidget(failureTaskLabel);

	currentTaskLabel = new Label((uint32_t)TR_LOADER_ID::CURRENT_TASK_LABEL, this);
	currentTaskLabel->setPosition(0, -100);
	currentTaskLabel->setFont("lucon.ttf",12);
	currentTaskLabel->setColor(255, 255, 255, 255);
	currentTaskLabel->setShadowColor(255, 0, 0, 128);
	currentTaskLabel->setShadowOffset(-2, -2);
	currentTaskLabel->setAlign((uint32_t)Margin::CENTER | (uint32_t)Margin::DOWN);
	currentTaskLabel->setText("Restarting loader ...");
	addWidget(currentTaskLabel);

	blinkTimer = new Timer((uint32_t)TR_LOADER_ID::BLINK_TIMER, this);
	blinkTimer->setOnTimerEvent(this, &Loader::onTimer);
	blinkTimer->setTimeInterval(0.500);
	blinkTimer->setAutoTimer(true);
	blinkTimer->setTimer(true);
	addWidget(blinkTimer);

	pressSpaceLabel = new Label((uint32_t)TR_LOADER_ID::PRESS_SPACE_LABEL, this);
	pressSpaceLabel->setPosition(0, -150);
	pressSpaceLabel->setFont("lucon.ttf",12);
	pressSpaceLabel->setColor(255, 255, 255, 255);
	pressSpaceLabel->setShadowColor(255, 0, 0, 128);
	pressSpaceLabel->setShadowOffset(-2, -2);
	pressSpaceLabel->setAlign((uint32_t)Margin::CENTER | (uint32_t)Margin::DOWN);
	pressSpaceLabel->setText("Press Space");
	addWidget(pressSpaceLabel);

	loadingImage = new ImageButton((uint32_t)TR_LOADER_ID::LOADING_IMAGEBUTTON, this);
	loadingImage->setPosition(-512 / 2, 0);
	loadingImage->setImage("gui/loading.png");
	loadingImage->setSize(512, 128);
	loadingImage->setColor(0, 0, 0, 0);

	addWidget(loadingImage);

	gradientImage->setActive(false);
	gradientImage->setRender(true);

	currentTaskLabel->setRender(true);
	currentTaskLabel->setActive(false);

	finishedTaskLabel->setRender(true);
	finishedTaskLabel->setActive(false);

	failureTaskLabel->setRender(true);
	failureTaskLabel->setActive(false);

	pressSpaceLabel->setRender(false);
	pressSpaceLabel->setActive(false);

	blinkTimer->setActive(true);
	blinkTimer->setRender(false);
}

Loader::~Loader()
{
	
}

void Loader::init()
{
	if (singleton == nullptr)
	{
		singleton = new Loader();
	}
}

Loader* Loader::getSingleton()
{
	return singleton;
}

void Loader::cleanUp()
{
	if (singleton != nullptr)
	{
		delete singleton;
		singleton = nullptr;
	}
}

void Loader::update()
{
	loaderSemaphore.lock();
	if (loadingFinished)
	{
		if (InputManager::isKeyPressed(JA_SPACE))
		{
			MainMenu::getSingleton()->setGameState(GameState::LAYEROBJECTEDITOR_MODE);
		}
	}
	loaderSemaphore.unlock();

	reloadCurrentTask();
	reloadFinishedTask();
	reloadFailureTask();

	Interface::update();
}

void Loader::draw()
{
	Interface::draw();
}

void Loader::restartLoader()
{
	loaderSemaphore.lock();
	loadingFinished = false;
	currentTaskChanged = false;
	failureTaskChanged = false;
	finishedTaskChanged = false;

	pressSpaceLabel->setRender(false);
	currentTaskBuffer.clear();
	failureTaskBuffer.clear();
	finishedTaskBuffer.clear();

	currentTaskLabel->setText("Restarting loader ...");
	failureTaskLabel->setText("");
	finishedTaskLabel->setText("");

	loaderSemaphore.unlock();
}

void Loader::reloadFinishedTask()
{
	loaderSemaphore.lock();
	if (finishedTaskChanged)
	{
		finishedTaskLabel->appendText(finishedTaskBuffer);
		finishedTaskBuffer.clear();
		finishedTaskChanged = false;
	}
	loaderSemaphore.unlock();
}

void Loader::reloadFailureTask()
{
	loaderSemaphore.lock();
	if (failureTaskChanged)
	{
		failureTaskLabel->appendText(failureTaskBuffer);
		failureTaskBuffer.clear();
		failureTaskChanged = false;
	}
	loaderSemaphore.unlock();
}

void Loader::reloadCurrentTask()
{
	loaderSemaphore.lock();
	if (currentTaskChanged)
	{
		currentTaskLabel->setText(currentTaskBuffer.c_str());
		currentTaskChanged = false;
	}
	loaderSemaphore.unlock();
}

void Loader::finishLoader()
{
	loaderSemaphore.lock();
	loadingFinished = true;
	loaderSemaphore.unlock();
}

void Loader::setCurrentTask(ja::string currentTask)
{
	loaderSemaphore.lock();
	currentTaskBuffer = currentTask;
	currentTaskChanged = true;
	loaderSemaphore.unlock();
}

void Loader::addFinishedTask(ja::string finishedTask)
{
	loaderSemaphore.lock();
	finishedTaskBuffer.append(finishedTask);
	finishedTaskBuffer.append("\n");
	finishedTaskChanged = true;
	loaderSemaphore.unlock();
}

void Loader::addFailureTask(ja::string failureTask)
{
	loaderSemaphore.lock();
	failureTaskBuffer.append(failureTask);
	failureTaskBuffer.append("\n");
	failureTaskChanged = true;
	loaderSemaphore.unlock();
}

void Loader::onTimer(Widget* sender, WidgetEvent action, void* data)
{
	if (sender->getId() == (uint32_t)TR_LOADER_ID::BLINK_TIMER)
	{
		if (loadingFinished)
		{
			pressSpaceLabel->setRender(!pressSpaceLabel->isRendered());
		}
	}
}

Loader* Loader::singleton = nullptr;

}



