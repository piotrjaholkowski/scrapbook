#pragma once

#include "CommonWidgets.hpp"
#include "Toolset.hpp"

#include "../../Engine/Graphics/Polygon.hpp"
#include "../../Engine/Graphics/Color4f.hpp"

#include "../../Engine/Gui/GridLayout.hpp"

#include <stdint.h>

namespace ja
{
	class Layer;
	class Camera;
	class Parallax;
}

namespace tr
{
	
	class LayerObjectComponentMenu : public CommonWidget
	{
	private:
		static LayerObjectComponentMenu* singleton;
		CommonWidget* components[20];
	public:
		LayerObjectComponentMenu(uint32_t widgetId, ja::Widget* parent);
		void onMenuClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void updateComponentEditors(ja::LayerObject* layerObject);
		void onCreateNotify(ja::LayerObject* layerObject);
		void onDeleteNotify(ja::LayerObject* layerObject);

		static inline LayerObjectComponentMenu* getSingleton()
		{
			return singleton;
		}

		void reloadResources();
	};

	typedef void(*VertexFilterPtr)(ja::Color4f*);

	class LayerObjectSceneEditor : public CommonWidget
	{
	private:
		ja::TextField* cellWidthTextfield;
		ja::TextField* cellHeightTextfield;
		ja::TextField* cellSizeTextfield;
		ja::Button*    cellRebuildButton;
		ja::Button*    cellRefreshButton;
	public:
		LayerObjectSceneEditor(uint32_t widgetId, ja::Widget* parent);
		void reloadResources();
		void onCellWidthDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCellHeightDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCellSizeDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onRefreshClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onRebuildClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class LayerObjectShapeEditor : public CommonWidget
	{
	private:
		static LayerObjectShapeEditor* singleton;

		float infoPosX;
		float infoPosY;

		ja::TextField* xTextfield;
		ja::TextField* yTextfield;
		ja::TextField* angleTextfield;
		ja::TextField* scaleXTextfield;
		ja::TextField* scaleYTextfield;
		ja::TextField* depthTextfield;
		ja::Label*     layerName;

		jaFloat  x;
		jaFloat  y;
		jaFloat  angleDeg;
		jaFloat  scaleX;
		jaFloat  scaleY;
		uint16_t depth;
	public:
		LayerObjectShapeEditor(uint32_t widgetId, ja::Widget* parent);

		void updateWidget(void* data);

		void onXDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onYDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onScaleXDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onScaleYDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onAngleDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDepthDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		static void xFilter(ja::LayerObject* layerObject);
		static void yFilter(ja::LayerObject* layerObject);
		static void angleFilter(ja::LayerObject* layerObject);
		static void scaleXFilter(ja::LayerObject* layerObject);
		static void scaleYFilter(ja::LayerObject* layerObject);
		static void depthFilter(ja::LayerObject* layerObject);

		static inline LayerObjectShapeEditor* getSingleton()
		{
			return singleton;
		}
		// dodac nowe opcje do rozwijanego menu z prawego przycisku myszy
	};

	enum class PolygonEditorMode
	{
		SELECT_MODE = 0,
		TRANSLATE_MODE = 1,
		ROTATE_MODE = 2,
		SCALE_MODE = 3,
		COMPONENT_EDITOR_MODE = 4
	};

	class PolygonEditor : public CommonWidget
	{
	private:
		static PolygonEditor* singleton;

		float infoPosX;
		float infoPosY;

		ja::Button* boxButton;
		ja::Button* circleButton;

		ja::LayerObject* editedLayerObject;
		ja::Polygon*     editedPolygon;
		ja::Button*      editModeButton;
		ja::Button*      makeAsPhysicBodyButton;

		ja::Checkbox* selectTrianglesCheckbox;

		ja::ScrollPane*                  scrollPane;
		StackAllocator<ja::DataNotifier> dataNotifiers;

		bool      drawBoxSelection;
		ScaleMode scaleAxis;
		jaVector2 selectionStartPoint;
		jaVector2 gizmo;
		jaVector2 originPoint;
		jaVector2 transformationStart;

		PolygonEditorMode           editorMode;
		StackAllocator<ja::Color4f> verticesColors;
		StackAllocator<jaVector2>   verticesPositions;
		StackAllocator<jaVector2>   verticesTransformedToLocal;
		StackAllocator<jaVector2>   savedPositionVertices;
		StackAllocator<ja::Color4f> savedColorVertices;
		StackAllocator<jaVector2*>  faceVertices;
		StackAllocator<bool>        selectedVertices;
		StackAllocator<uint32_t>    savedTriangleIndices;
		StackAllocator<uint32_t>    triangleIndices;
		StackAllocator<uint32_t>    recalculatedTriangleIndices;

	public:
		PolygonEditor(uint32_t widgetId, ja::Widget* parent);

		void drawGizmo();
		void drawOriginPoint();

		void createVertex();
		void deleteVertex();
		void deleteFace();
		void extrude();
		void duplicate();
		void makeFace();

		void render();
		void handleInput();
		void setActiveMode(bool state);

		void updateWidget(void* data);
		void refreshDataFieldViewer(ja::Polygon* polygon);
		void onDataChange(ja::DataNotifier* dataNotifier);

		void makeSelection(gil::AABB2d& selectionAABB);
		void addToSelection(gil::AABB2d& selectionAABB);
		void removeFromSelection(gil::AABB2d& selectionAABB);
		void calculateSelectionPoint();

		void onEditModeClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onMakeAsPhysicBodyClick(ja::Widget* sender, ja::WidgetEvent action, void* data);

		void setEditorMode(PolygonEditorMode editorMode);
		void savePositionVerticesState();
		void saveColorVerticesState();
		void assignPositionVerticesState(jaVector2* const pVertices, uint32_t verticesNum);
		void assignColorVerticesState(ja::Color4f* const pColors, uint32_t verticesNum);
		void translateMode();
		void scaleMode();
		void rotateMode();

		bool isTriangleSelectionSelected();

		void getPolygonInfo(ja::Polygon* polygon);

		void applyFilterToSelection(VertexFilterPtr vertexFilter)
		{
			ja::Color4f* pColors = verticesColors.getFirst();
			uint32_t verticesNum = verticesColors.getSize();

			bool* pSelected = selectedVertices.getFirst();

			for (uint32_t i = 0; i < verticesNum; ++i)
			{
				if (pSelected[i])
					vertexFilter(&pColors[i]);
			}

			assignColorVerticesState(pColors, verticesNum);
		}

		static inline PolygonEditor* getSingleton()
		{
			return singleton;
		}
	};

	class MorphEditor : public CommonWidget
	{
	private:
		static MorphEditor* singleton;

		ja::TextField*      newMorphNameTextfield;
		ja::Button*         createMorphButton;

		ja::Label*          morphsNumLabel;

		ja::GridViewer*     morphGridViewer;

		ja::TextField*      morphNameTextfield;
		ja::Button*         deleteMorphButton;
		ja::Button*         updateMorphButton;

		ja::TextField*      morphTextField;
		ja::Scrollbar*      morphScrollbar;

		ja::Label           morphLabel;

		std::vector<void*>  morphs;

		ja::Label* startMorphLabel;
		ja::Label* endMorphLabel;

		ja::Button*         setStartMorphButton;
		ja::Button*         setEndMorphButton;

		std::vector<void*> morphFeatures;
		ja::GridViewer*    morphFeaturesGridViewer;

		ja::LayerObject*      editedLayerObject;
		ja::LayerObjectMorph* editedMorph;
		ja::LayerObjectMorph* startMorph;
		ja::LayerObjectMorph* endMorph;

	public:
		MorphEditor(uint32_t widgetId, ja::Widget* parent);

		void updateWidget(void* data);
		void setActiveWidgets(bool state);

		void setMorphData(ja::LayerObjectMorph* layerObjectMorph);
		void refreshMorphGridView();

		void onMorphChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCreateMorphClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onSetStartMorphClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onSetEndMorphClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDrawMorph(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPickMorph(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDrawMorphFeature(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPickMorphFeature(ja::Widget* sender, ja::WidgetEvent action, void* data);

		void onNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDeleteMorphClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onUpdateMorphClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class PolygonBoxEditor : public CommonWidget
	{
	private:
		static PolygonBoxEditor* singleton;

		ja::TextField* widthTextfield;
		ja::TextField* heightTextfield;
		ja::Button*    createButton;

		ja::RadioButton* facesButton;
		ja::RadioButton* edgesButton;
		ja::TextField*   edgeWidthTextfield;

	public:
		PolygonBoxEditor(uint32_t widgetId, Widget* parent);

		//void updateWidget(void* data);

		void onCreateClick(Widget* sender, ja::WidgetEvent action, void* data);

		static inline PolygonBoxEditor* getSingleton()
		{
			return singleton;
		}
	};

	class PolygonCircleEditor : public CommonWidget
	{
	private:
		static PolygonCircleEditor* singleton;

		ja::TextField* radiusTextfield;
		ja::TextField* segmentsTextField;
		ja::Button*    createButton;

		ja::RadioButton* facesButton;
		ja::RadioButton* edgesButton;
		ja::TextField*   edgeWidthTextfield;

	public:
		PolygonCircleEditor(uint32_t widgetId, ja::Widget* parent);

		//void updateWidget(void* data);

		void onCreateClick(ja::Widget* sender, ja::WidgetEvent action, void* data);

		static inline PolygonCircleEditor* getSingleton()
		{
			return singleton;
		}
	};

	class LayerObjectColorEditor : public CommonWidget
	{
	private:
		static LayerObjectColorEditor* singleton;
		ja::TextField*  redTextField;
		ja::TextField*  greenTextField;
		ja::TextField*  blueTextField;
		ja::TextField*  alphaTextField;
		ja::Scrollbar*  redScrollbar;
		ja::Scrollbar*  greenScrollbar;
		ja::Scrollbar*  blueScrollbar;
		ja::Scrollbar*  alphaScrollbar;
		ja::FlowLayout* colorFlow;

	public:
		float alpha = 1.0f;
		float blue  = 1.0f;
		float red   = 1.0f;
		float green = 1.0f;

	private:
		void updateColorValueScrollbar(ja::Scrollbar* scrollbar, ja::TextField* textField, float& colorValue);
		void updateColorValueTextfield(ja::Scrollbar* scrollbar, ja::TextField* textField, float& colorValue);
	public:
		LayerObjectColorEditor(uint32_t widgetId, Widget* parent);
		void updateWidget(void* data);
		void onChangeColor(ja::Widget* sender, ja::WidgetEvent action, void* data);

		static void changeColorFilter(ja::Polygon* polygon);
		static void changeColorFilter(ja::Color4f* vertex);
		static void changeAlphaFilter(ja::Polygon* polygon);
		static void changeAlphaFilter(ja::Color4f* vertexColor);

		static inline LayerObjectColorEditor* getSingleton()
		{
			return singleton;
		}
	};

	class LightEditor : public CommonWidget
	{
	private:

		static LightEditor* singleton;
	public:
		LightEditor(uint32_t widgetId, ja::Widget* parent);

		static inline LightEditor* getSingleton()
		{
			return singleton;
		}
	};

	class CameraEditor : public CommonWidget
	{
	private:
		ja::TextField* newNameTextfield;
		ja::Button*    createButton;

		ja::GridViewer* cameraGridViewer;

		ja::TextField* nameTextfield;
		ja::Button*    deleteButton;

		ja::Checkbox* selectedAsViewCheckbox;
		ja::Button*   defaultButton;

		std::vector<void*> cameras;

		ja::Label cameraLabel;

		ja::Camera* editedCamera;

		static CameraEditor* singleton;
	public:
		CameraEditor(uint32_t widgetId, ja::Widget* parent);

		static inline CameraEditor* getSingleton()
		{
			return singleton;
		}

		void setCameraData(void* data);

		void reloadResources();
		void refreshGridViewer();

		void updateWidget(void* data);
		void onDeleteNotify(void* data);

		void onNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCreateClick(ja::Widget* sender, ja::WidgetEvent action, void* data);	

		void onDrawCamera(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPickCamera(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDefaultClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDeleteClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class TextEditor : public CommonWidget
	{
	private:
		ja::Button*  applyButton;
		ja::Editbox* editbox;

		static TextEditor* singleton;
	public:
		TextEditor(uint32_t widgetId, ja::Widget* parent);

		static inline TextEditor* getSingleton()
		{
			return singleton;
		}

		void updateWidget(void* data);

		void onApplyClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class LayersEditor : public CommonWidget
	{
	private:

		ja::TextField* newLayerNameTextfield;
		ja::Button*    createLayerButton;

		ja::GridViewer*    layersGridViewer;
		std::vector<void*> layers;

		ja::Layer* editedLayer;

		ja::TextField* layerNameTextfield;
		ja::Button*    deleteLayerButton;

		ja::Checkbox*  isActiveCheckbox;
		ja::Button*    moveUpButton;
		ja::Button*    moveDownButton;

		ja::Label layerLabel;

		static LayersEditor* singleton;
	public:
		LayersEditor(uint32_t widgetId, ja::Widget* parent);
		void updateWidget(void* data);

		void reloadResources();
		void refreshGridViewer();

		void setLayerData(ja::Layer* layer);

		void onLayerNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCreateLayerClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDeleteLayerClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onActiveChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onMoveLayerUpClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onMoveLayerDownClick(ja::Widget* sender, ja::WidgetEvent action, void* data);

		void onDrawLayer(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPickLayer(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class ParallaxEditor : public CommonWidget
	{
	private:

		ja::TextField* newParallaxNameTextfield;
		ja::Button*    createParallaxButton;

		ja::GridViewer*    parallaxGridViewer;
		std::vector<void*> parallaxes;

		ja::Parallax* editedParallax;

		ja::TextField* parallaxNameTextfield;
		ja::Button*    deleteParallaxButton;

		ja::Checkbox*  isActiveCheckbox;
		ja::Button*    moveUpButton;
		ja::Button*    moveDownButton;

		ja::GridViewer*    cameraGridViewer;
		std::vector<void*> cameras;

		//ja::Label* cameraNameLabel;

		ja::Label parallaxLabel;

		static ParallaxEditor* singleton;
	public:
		ParallaxEditor(uint32_t widgetId, ja::Widget* parent);
		
		void reloadResources();
		void refreshGridViewer();
		void refreshCameraGridViewer();

		void setParallaxData(ja::Parallax* parallax);
		void setCameraData(void* data);

		void onCreateNotify(void* data);
		void onDeleteNotify(void* data);
		void onParallaxNameDeselect(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onCreateParallaxClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDeleteParallaxClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onActiveChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onMoveLayerUpClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onMoveLayerDownClick(ja::Widget* sender, ja::WidgetEvent action, void* data);

		void onDrawParallax(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPickParallax(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDrawCamera(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onPickCamera(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class LayerObjectMaterialEditor : public CommonWidget
	{
	private:

		ja::Button*    materialButton;
		ja::Button*    sourceFactorButton;
		ja::Scrollbar* sourceFactorScrollbar;
		ja::Button*    destinationFactorButton;
		ja::Scrollbar* destinationFactorScrollbar;
		ja::Button*    blendEquationButton;
		ja::Scrollbar* blendEquationScrollbar;
	
		ja::ScrollPane* scrollPane;
		StackAllocator<ja::DataNotifier> dataNotifiers;

		ja::Material* editedMaterial;
	public:
		LayerObjectMaterialEditor(uint32_t widgetId, ja::Widget* parent);
		void updateWidget(void* data);

		void reloadResources();
		
		void setMaterialData(ja::Material* material);
		void refreshDataFieldViewer();

		void onScrollbarChange(ja::Widget* sender, ja::WidgetEvent action, void* data);
		void onDataChange(ja::DataNotifier* dataNotifier);
	};

	class LayerObjectAddMenu : public ja::GridLayout
	{
		ja::Button* polygonButton;
		ja::Button* cameraButton;
		ja::Button* lightButton;
	public:
		LayerObjectAddMenu(uint32_t widgetId, ja::Widget* parent);
		virtual ~LayerObjectAddMenu();

		void createLayerObjectClick(ja::Widget* sender, ja::WidgetEvent action, void* data);
	};

	class LayerObjectSpaceBarMenu : public ja::GridLayout
	{
		ja::Button* addLayerObjectButton;

	public:
		LayerObjectSpaceBarMenu(uint32_t widgetId, ja::Widget* parent);
		virtual ~LayerObjectSpaceBarMenu();

		void hide();
		void show();
		void update();
	};
}
