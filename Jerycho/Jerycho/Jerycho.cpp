#include "Jerycho.hpp"

#ifdef JA_SDL2_VERSION
	#include<SDL2/SDL.h>
#else
	#include<SDL/SDL.h>
#endif

#include "../Engine/Manager/LogManager.hpp"
#include "../Engine/Manager/GameManager.hpp"
#include "../Engine/Manager/DisplayManager.hpp"
#include "../Engine/Manager/ResourceManager.hpp"
#include "../Engine/Manager/InputManager.hpp"
#include "../Engine/Manager/UIManager.hpp"
#include "../Engine/Manager/LayerObjectManager.hpp"

#include "../Engine/Manager/ScriptManager.hpp"
#include "../Engine/Manager/PhysicsManager.hpp"
#include "../Engine/Manager/LevelManager.hpp"
#include "../Engine/Manager/SoundManager.hpp"
#include "../Engine/Manager/EntityManager.hpp"
#include "../Engine/Manager/EffectManager.hpp"
#include "../Engine/Manager/ThreadManager.hpp"
#include "../Engine/Graphics/Texture2dLoader.hpp"
#include "../Engine/Graphics/ShaderLoader.hpp"
#include "../Engine/Tests/TimeStamp.hpp"

#include "../Engine/Math/VectorMath2d.hpp"
#include "../Engine/Gilgamesh/Narrow/Sat2d.hpp"
#include "../Engine/Allocators/PoolAllocator.hpp"
#include "../Engine/Gilgamesh/Mid/AABB2d.hpp"
#include "../Engine/Utility/CpuInfo.hpp"
#include "../Engine/Gilgamesh/ObjectHandle2d.hpp"
#include "../Engine/Gilgamesh/Shapes/Box2d.hpp"
#include "../Engine/Gilgamesh/Shapes/Circle2d.hpp"
#include "../Engine/Gilgamesh/DebugDraw.hpp"
#include "../Engine/Gilgamesh/Narrow2dConfiguration.hpp"

#include "../Engine/Utility/Exception.hpp"

#include "Gui\MainMenu.hpp"
#include "Callbacks\Callbacks.hpp"

//#include "../Engine/Math/vectormath/cpp/vectormath_aos.h"
using namespace tr;



void printProfileInfo(const ProfileOperationInfo& profileInfo, std::string& profileInfoText, uint32_t spaceNum)
{
	profileInfoText.append(spaceNum, ' ');
	profileInfoText.append(profileInfo.operationTag.getName());
	profileInfoText.append(" time: ");

	char deltaTimeStr[20];
	sprintf(deltaTimeStr, "%f", profileInfo.deltaTime);
	profileInfoText.append(deltaTimeStr);
	profileInfoText.append("\n");

	const uint32_t childNum = profileInfo.childOperations.getSize();
	const ProfileOperationInfo* const pChildProfile = profileInfo.childOperations.getFirst();

	for (uint32_t i = 0; i < childNum; ++i)
	{
		printProfileInfo(pChildProfile[i], profileInfoText, spaceNum + 1);
	}
}

void renderProfileInfo(const ProfileOperationInfo& profileInfo, RenderBuffer& renderBuffer)
{
	GameState gameState = MainMenu::getSingleton()->getGameState();

	if (gameState == GameState::LOADING_MODE)
		return;

	float widthUnits;
	float heightUnits;

	DisplayManager*  displayManager  = DisplayManager::getSingleton();
	ResourceManager* resourceManager = ResourceManager::getSingleton();
	displayManager->getWorldResolution(&widthUnits, &heightUnits);

	jaMatrix44 projectionMatrix;
	jaVectormath2::setOrthoFrustumMatrix44(-widthUnits * 0.5f, widthUnits * 0.5f, -heightUnits * 0.5f, heightUnits * 0.5f, -1.0f, 1.0f, projectionMatrix);

	Material*    defaultMaterial = resourceManager->getMaterial("DEFAULT")->resource;
	FontPolygon* font            = resourceManager->getEternalFontPolygon("lucon.ttf")->resource;

	renderBuffer.addRenderCommand(projectionMatrix, defaultMaterial);

	jaMatrix44 textMatrix;
	//jaVectormath2::setTranslateMatrix44(jaVector2(-widthUnits * 0.5f, 0.0f), textMatrix);
	jaVectormath2::setTranslateRotScaleMatrix44(jaVector2(-widthUnits * 0.5f, 0.0f), jaMatrix2(0.0f), jaVector2(0.7f, 0.7f), textMatrix);

	uint8_t shaderObjectData[256];
	memset(shaderObjectData, 0, sizeof(shaderObjectData));

	std::string profileInfoText;
	printProfileInfo(profileInfo, profileInfoText, 0);

	displayManager->RenderText(renderBuffer, defaultMaterial, shaderObjectData, textMatrix, font, profileInfoText.c_str(), (uint32_t)Margin::LEFT | (uint32_t)Margin::MIDDLE, Color4f(1.0f, 1.0f, 1.0f, 1.0f));
}




Jerycho::Jerycho(int argc, char* args[])
{
	Profiler::reset();
	Profiler::clearRegisteredOperationNames();

	this->frameRate = 60;
	this->timeStepMiliseconds = 1000 / this->frameRate;
	this->timeStepSeconds     = 1.0f / (float)this->frameRate;
	
 	SDL_Init(SDL_INIT_NOPARACHUTE);
	GameManager::initGameManager("Jerycho");
	LogManager::initLogManager();

	CpuInfo* cpuInfo = CpuInfo::getSingleton();
	cpuInfo->logInfo();

	DisplayManager::initDisplayManager();
	//DisplayManager::getSingleton()->setResolution(res1280x720,8,8,8,8,false,false,4);
	//DisplayManager::getSingleton()->setResolution(res1280x720, 8, 8, 8, 8, false, true, 0);
	DisplayManager::getSingleton()->setResolution(res1280x720, 8, 8, 8, 8, false, true, 4);
	//jaDisplayManager::getSingleton()->setResolution(res1366x768,8,8,8,8,true,false);
	DisplayManager::getSingleton()->setWindowTitle("Scrapbook");
	DisplayManager::getSingleton()->initExtensions();

	DisplayManager::getSingleton()->setBackgroundColor(0,0,0);

	ResourceManager::init();
	ThreadManager::init();
	InputManager::init();
	UIManager::init();
	LayerObjectManager::init(LayerObjectDefaultConfiguration());
	AnimationManager::init(AnimationDefaultConfiguration());
	EffectManager::init();	
	PhysicsManager::init(PhysicsDefaultConfiguration()); // uses cacheManager
	LevelManager::init();
	SoundManager::init();
	ScriptManager::init();
	EntityManager::init(); // uses cacheManager if Init

	logManager         = LogManager::getSingleton();
	gameManager        = GameManager::getSingleton();
	displayManager     = DisplayManager::getSingleton();
	resourceManager    = ResourceManager::getSingleton();
	inputManager       = InputManager::getSingleton();
	uiManager          = UIManager::getSingleton();
	layerObjectManager = LayerObjectManager::getSingleton();
	animationManager   = AnimationManager::getSingleton();
	scriptManager      = ScriptManager::getSingleton();
	physicsManager     = PhysicsManager::getSingleton();
	levelManager       = LevelManager::getSingleton();
	soundManager       = SoundManager::getSingleton();
	entityManager      = EntityManager::getSingleton();
	effectManager      = EffectManager::getSingleton();
	threadManager      = ThreadManager::getSingleton();

	logManager->saveLog("Config.log");

	//threadManager->setThreadsNum(cpuInfo.getLogicalCoresNum());
	//threadManager->setThreadsNum(1);
	//ThreadManager::displayOpenClInfo();
	threadManager->setWarmUpMode(true);
	//threadManager->setThreadsNum(3);
	threadManager->setThreadsNum(1);

	gameManager->update(timeStepSeconds); // to send camera event

	//game
	resourceManager->loadEternalFontPolygon("debug.ttf", FontEncode::ASCII_STANDARD);
	resourceManager->loadEternalFontPolygon("lucon.ttf", FontEncode::ASCII_STANDARD);

	resourceManager->loadEternalFont("lucon.ttf",12,FontEncode::ASCII_STANDARD);

	//resourceManager->loadFont("lucon.ttf", 9, FontEncode::ASCII_STANDARD, "default");
	//resourceManager->loadFont("lucon.ttf", 8, FontEncode::ASCII_STANDARD, "default");
	resourceManager->loadEternalFont("default.ttf", 10, FontEncode::ASCII_STANDARD);
	resourceManager->loadEternalFont("debug.ttf", 12, FontEncode::ASCII_STANDARD);
	resourceManager->loadEternalTexture2d("gui/left_arrow.png");
	resourceManager->loadEternalTexture2d("gui/right_arrow.png");
	resourceManager->loadEternalTexture2d("gui/up_arrow.png");
	resourceManager->loadEternalTexture2d("gui/down_arrow.png");
	resourceManager->loadEternalTexture2d("gui/checked.png");
	resourceManager->loadEternalTexture2d("gui/unchecked.png");
	resourceManager->loadEternalTexture2d("gui/progressButton.png");
	resourceManager->loadEternalTexture2d("gui/loading.png");
	resourceManager->loadEternalTexture2d("gui/gradient.png");
	resourceManager->loadEternalTexture2d("gui/hider_hide.png");
	resourceManager->loadEternalTexture2d("gui/hider_show.png");
	resourceManager->loadEternalTexture2d("gui/magnet.png");
	resourceManager->loadEternalTexture2d("gui/close.png");

	resourceManager->loadEternalTexture2d("entity_icons/player.png");
	resourceManager->loadEternalTexture2d("entity_icons/timer.png");
	resourceManager->loadEternalTexture2d("entity_icons/sound.png");
	resourceManager->loadEternalTexture2d("entity_icons/data.png");
	//resourceManager->loadEternalTexture2d("entity_icons/script.png");
	resourceManager->loadEternalTexture2d("entity_icons/particles.png");
	//resourceManager->loadEternalTexture2d("entity_icons/physic_object.png");
	resourceManager->loadEternalTexture2d("entity_icons/layer_object.png");

	//resourceManager->loadVideoStream("drop.avi", 0, "teledysk");

	MainMenu::init();
	uiManager->addInterface("MAIN MENU",MainMenu::getSingleton());

	Callbacks::registerCallbacks();

	levelManager->loadLevelSeparateThread("levels/default.lua");
	
	gameManager->renderDebug = true;	
}

Jerycho::~Jerycho()
{
	GameManager::cleanUpAll();
	SDL_Quit();
}

void Jerycho::run()
{
	gameManager->blockMouseEvents(true);
	gameManager->showMouse(true);
	gameManager->grabInput(false);
	gameManager->allowDropEvent(true);

	RenderBuffer       debugRenderBuffer(64);
	CacheLineAllocator profileAllocator(sizeof(ProfileOperationInfo) * 512, 32);

	SDL_Event event;
	//this->lastTimeMiliseconds = SDL_GetTicks();
	while(!MainMenu::getSingleton()->isExit)
	{
		uint32_t startTimeLoop = SDL_GetTicks();

		Profiler::reset();

		while(SDL_PollEvent(&event))
		{
			switch (event.type)
			{
#ifndef JA_SDL2_VERSION
			case SDL_MOUSEBUTTONDOWN:
				switch(event.button.button)
				{
				case SDL_BUTTON_WHEELUP:
					inputManager->setMouseWheelUp();
					break;
				case SDL_BUTTON_WHEELDOWN:
					inputManager->setMouseWheelDown();
					break;
				}
				break;
#endif
#ifdef JA_SDL2_VERSION
			case SDL_MOUSEWHEEL:
				if (event.wheel.y > 0)
					inputManager->setMouseWheelUp();
				else
					inputManager->setMouseWheelDown();
				break;
#endif
#ifndef JA_SDL2_VERSION
			case SDL_KEYDOWN:
				inputManager->addInputEvent(JA_KEYPRESSED, event.key.keysym.sym, event.key.keysym.unicode);
				break;
			case SDL_KEYUP:
				inputManager->addInputEvent(JA_KEYRELEASED, event.key.keysym.sym, event.key.keysym.unicode);
				break;
#endif
#ifdef JA_SDL2_VERSION
			case SDL_TEXTINPUT:
				inputManager->addInputEvent(JA_KEYPRESSED, event.text.text);
				break;
			case SDL_WINDOWEVENT:
			{
				switch (event.window.event) {
				case SDL_WINDOWEVENT_ENTER:
					//std::cout << "Mouse entered window" << std::endl;
					break;
				case SDL_WINDOWEVENT_LEAVE:
					//std::cout << "Mouse left window" << std::endl;
					break;
				}
			}
			break;

			//case SDL_DROPBEGIN:


			case SDL_DROPFILE:
			{
				char* droppedFileName = event.drop.file; 
		
				MainMenu::getSingleton()->onDropFile(droppedFileName);
				
				SDL_free(droppedFileName);    // Free dropped_filedir memory
			}
				break;
#endif
			case SDL_QUIT:
				if (MainMenu::getSingleton()->getGameState() != GameState::LOADING_MODE)
					MainMenu::getSingleton()->isExit = true;
				break;
			}
		}

		SDL_GL_SetSwapInterval(0); // Fixes stalling problem while calling glUniformLocation
		
		update();
		inputManager->clearInputEvents();
		draw();

		ProfileOperationInfo profileInfo(&profileAllocator);
		Profiler::getProfileTimes(profileInfo);
		
		debugRenderBuffer.clear();
		renderProfileInfo(profileInfo, debugRenderBuffer);
		debugRenderBuffer.applyViewMatricesToMvpMatrices();
		debugRenderBuffer.sendDataToGPU();
		debugRenderBuffer.render();
		
		displayManager->flushRenderingCommands();

		SDL_GL_SetSwapInterval(-1); // Fixes stalling problem while calling glUniformLocation

		// when make some rest
		uint32_t endTimeLoop           = SDL_GetTicks();
		uint32_t deltaTimeMiliseconds  = endTimeLoop - startTimeLoop;
		
		if (deltaTimeMiliseconds < timeStepMiliseconds)
		{
			const uint32_t marginSleepError = 3;
		
			uint32_t timeToWait = timeStepMiliseconds - deltaTimeMiliseconds;
		
			if (timeToWait > marginSleepError)
			{
				uint32_t timeToSleep = timeToWait - marginSleepError;
		
				SDL_Delay(timeToSleep);
			}
					
			while (1 == 1)
			{
				uint32_t lastTime  = SDL_GetTicks();
				uint32_t spendTime = lastTime - startTimeLoop;
		
				if (spendTime >= timeStepMiliseconds)
				{
					break;
				}
			}
		
			uint32_t afterWait     = SDL_GetTicks();
			uint32_t realTimeSleep = afterWait - endTimeLoop;			
			//std::cout << "TimeToWait: " << timeToWait << " RealWaitTime: "  << realTimeSleep << std::endl;
		}

		displayManager->swapBuffers();
	}
}

void Jerycho::update()
{
	PROFILER_FUN;
	

	inputManager->updateInput();

	GameState gameState = MainMenu::getSingleton()->getGameState();

	switch(gameState)
	{
	case GameState::GAMEPLAY_MODE:
		Texture2dLoader::queryScheduleLoading();
		ShaderLoader::queryScheduleLoading();
		uiManager->update(timeStepSeconds);	
		physicsManager->step(timeStepSeconds);
		entityManager->update(timeStepSeconds);
		animationManager->update(timeStepSeconds);
		layerObjectManager->update(timeStepSeconds);
		soundManager->update(timeStepSeconds);
		gameManager->update(timeStepSeconds);
	
		break;
	case GameState::LOADING_MODE:
		Texture2dLoader::queryScheduleLoading(); // not in gameplay mode
		ShaderLoader::queryScheduleLoading();
		uiManager->updateSafe(timeStepSeconds);
		break;
	default:
		Texture2dLoader::queryScheduleLoading(); // not in gameplay mode
		ShaderLoader::queryScheduleLoading(); // put that in one method later
		uiManager->update(timeStepSeconds);
		physicsManager->debugStep(timeStepSeconds); // Generate contact points
		entityManager->update(timeStepSeconds);
		animationManager->update(timeStepSeconds);
		layerObjectManager->update(timeStepSeconds);
		soundManager->update(timeStepSeconds);
		gameManager->update(timeStepSeconds);
		break;
	}
}

void Jerycho::draw()
{
	PROFILER_FUN;

	displayManager->clearDisplay();
	GameState gameState = MainMenu::getSingleton()->getGameState();
	
	if (gameState != GameState::LOADING_MODE)
	{
		
		gameManager->renderScene();	
		
		displayManager->setPointSize(3);
		displayManager->setLineWidth(0.1f);	
		
		if ((gameState == GameState::GAMEPLAY_MODE) && gameManager->renderDebug)
		{
			physicsManager->drawDebug(gameManager->getActiveCamera());
		}

		uiManager->draw();
	}
	else
	{
		uiManager->drawSafe();
	}

}