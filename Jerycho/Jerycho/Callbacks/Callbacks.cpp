#include "Callbacks.hpp"

#include "../../Engine/Manager/LevelManager.hpp"
#include "../../Engine/Manager/LayerObjectManager.hpp"
#include "../../Engine/Manager/EffectManager.hpp"
#include "../../Engine/Manager/PhysicsManager.hpp"
#include "../../Engine/Manager/GameManager.hpp"
#include "../../Engine/Manager/EntityManager.hpp"
#include "../../Engine/Manager/LogManager.hpp"
#include "../Gui/LayerObjectEditor.hpp"
#include "../Gui/MainMenu.hpp"
#include "../Gui/Loader.hpp"
#include "../../Engine/Utility/String.hpp"

#include <iostream>

using namespace tr;

void Callbacks::registerCallbacks()
{
	LevelManager* levelManager = LevelManager::getSingleton();
	
	levelManager->setSaveLevelCallback(beforeSaveLevel);
	levelManager->setSavedLevelCallback(afterSaveLevel);
	levelManager->setLoadLevelCallback(beforeLoadLevel);
	levelManager->setLoadedLevelCallback(afterLoadLevel);
	levelManager->setReloadLevelCallback(beforeReloadLevel);
	levelManager->setReloadedLevelCallback(afterReloadLevel);
	levelManager->setPrintCurrentTaskCallback(printCurrentTaskLevel);
	levelManager->setPrintFinishedTaskCallback(printFinishedTaskLavel);
	levelManager->setPrintFailureTaskCallback(printFailureTaskLevel);
}

void Callbacks::beforeSaveLevel()
{
	//std::cout << "before save";
	//trSpriteEditor::getSingleton()->deleteCurrentSprite(); // Usuni�cie sprite'a kt�ry jest w trybie edycji
}

void Callbacks::afterSaveLevel()
{
	//std::cout << "after save";
	//trSpriteEditor::getSingleton()->createSprite();
}

void Callbacks::beforeLoadLevel()
{
	EntityManager::getSingleton()->clearEntities(); // clear sprites,bodies etc
	LayerObjectManager::getSingleton()->clearLayerObjects(); // clearSpriteEntities
	PhysicsManager::getSingleton()->clearWorld();
	EffectManager::getSingleton()->clearEffects();
	//jaSoundManager::getSingleton()->clearSounds(); it is cleared by clearEntities
	ResourceManager::getSingleton()->clearResources(); // clearLevelResources
	LogManager::getSingleton()->clearLogs();
}

void Callbacks::afterLoadLevel()
{
	LayerObjectEditor::getSingleton()->gatherResources();
	LayerObjectEditor::getSingleton()->setDefaultParameters();

	PhysicsEditor::getSingleton()->gatherResources();
	PhysicsEditor::getSingleton()->setDefaultParameters();

	EntityEditor::getSingleton()->gatherResources();
	EntityEditor::getSingleton()->setDefaultParameters();

	Loader::getSingleton()->finishLoader();
}

void Callbacks::beforeReloadLevel()
{
	EntityManager::getSingleton()->clearEntities(); // clear sprites,bodies etc
	LayerObjectManager::getSingleton()->clearLayerObjects(); 
	PhysicsManager::getSingleton()->clearWorld();
	EffectManager::getSingleton()->clearEffects();
	LogManager::getSingleton()->clearLogs();
	//jaSoundManager::getSingleton()->clearSounds(); it is cleaned by clearEntitities
}

void Callbacks::afterReloadLevel()
{
	Loader::getSingleton()->finishLoader();
}

void Callbacks::printCurrentTaskLevel(const char* currentTask)
{
	Loader::getSingleton()->setCurrentTask(currentTask);
}

void Callbacks::printFinishedTaskLavel(const char* finishedTask)
{
	Loader::getSingleton()->addFinishedTask(finishedTask);
}

void Callbacks::printFailureTaskLevel(const char* failureTask)
{
	Loader::getSingleton()->addFailureTask(failureTask);
}

