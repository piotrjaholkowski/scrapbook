#pragma once

#include "../../Engine/Utility/String.hpp"

namespace Callbacks
{
	void registerCallbacks();

	void beforeSaveLevel();
	void afterSaveLevel();
	void beforeLoadLevel();
	void afterLoadLevel();
	void beforeReloadLevel();
	void afterReloadLevel();
	void printCurrentTaskLevel(const char* currentTask);
	void printFinishedTaskLavel(const char* finishedTask);
	void printFailureTaskLevel(const char* failureTask);
};
