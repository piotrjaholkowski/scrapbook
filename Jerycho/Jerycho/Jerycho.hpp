#pragma once

#include "../Engine/Manager/LogManager.hpp"
#include "../Engine/Manager/GameManager.hpp"
#include "../Engine/Manager/DisplayManager.hpp"
#include "../Engine/Manager/ResourceManager.hpp"
#include "../Engine/Manager/InputManager.hpp"
#include "../Engine/Manager/UIManager.hpp"
#include "../Engine/Manager/AnimationManager.hpp"
#include "../Engine/Manager/LayerObjectManager.hpp"
#include "../Engine/Manager/ScriptManager.hpp"
#include "../Engine/Manager/PhysicsManager.hpp"
#include "../Engine/Manager/LevelManager.hpp"
#include "../Engine/Manager/SoundManager.hpp"
#include "../Engine/Manager/EntityManager.hpp"
#include "../Engine/Manager/EffectManager.hpp"
#include "../Engine/Manager/ThreadManager.hpp"

#include <stdint.h>

namespace ja
{

	class Jerycho
	{
	private:
		uint32_t frameRate;
		float    timeStepSeconds;
		uint32_t timeStepMiliseconds;


		GameManager*        gameManager;
		LogManager*         logManager;
		DisplayManager*     displayManager;
		ResourceManager*    resourceManager;
		InputManager*       inputManager;
		UIManager*          uiManager;
		LayerObjectManager* layerObjectManager;
		AnimationManager*   animationManager;
		ScriptManager*      scriptManager;
		PhysicsManager*     physicsManager;
		LevelManager*       levelManager;
		SoundManager*       soundManager;
		EntityManager*      entityManager;
		EffectManager*      effectManager;
		ThreadManager*      threadManager;
	public:
		Jerycho(int argc, char* args[]);
		~Jerycho();
		void run();
		void draw();
		void update();
	};

}
