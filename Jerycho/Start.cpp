#ifndef _JA_TEST
#include "Jerycho/Jerycho.hpp"
#endif

#include "Jerycho/../Engine/Tests/Test.hpp"
#include "Jerycho/../Engine/Tests/MathTests.hpp"
#include "Jerycho/../Engine/Tests/AllocatorTests.hpp"
#include "Jerycho/../Engine/Tests/PhysicsTests.hpp"
#include "Jerycho/../Engine/Tests/ThreadTests.hpp"
#include "Jerycho/../Engine/Tests/UtilityTests.hpp"


int main(int argc, char* args[])
{
	if(argc > 1)
	{
		//args[0] - Called program directory
		std::cout << args[0] << std::endl;
		std::cout << args[1] << std::endl;

		Test::init(args[1]);
		Test::registerSuite<MathTests>("Math Test");
		Test::registerSuite<AllocatorTests>("Allocator Test");
		Test::registerSuite<PhysicsTests>("Physics Test");
		Test::registerSuite<ThreadTests>("Thread Test");
		Test::registerSuite<UtilityTests>("Utility Test");

		if (argc > 2) {
			if (strcmp(args[2], "UNIT_TESTS") == 0) {
				Test::run(JATEST_RUN_UNIT_TESTS);
			}
			else if (strcmp(args[2], "PERFORMANCE_TESTS") == 0) {
				Test::run(JATEST_RUN_PERFORMANCE_TESTS);
			}
			else {
				Test::run(JATEST_RUN_ALL);
			}
		} else {
			Test::run(JATEST_RUN_ALL);
		}
	}
	else
	{
#ifndef _JA_TEST
		Jerycho game = Jerycho(argc, args);
		game.run();
#endif
	}
	
	return 0;
}
