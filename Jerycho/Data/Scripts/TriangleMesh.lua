function initTriangleMesh(x,y)
--Physics Objects
local physicObjects = {};
local multiShape = {};
local physicObjectDef = PhysicsManager.getPhysicObjectDef();
--Physic object 1
PhysicObjectDef.setPosition(physicObjectDef,Vector2.create(x + -16.000000,y + -2.187500));
PhysicObjectDef.setRotation(physicObjectDef,0.000000);
PhysicObjectDef.setBodyType(physicObjectDef,0);
PhysicObjectDef.setTimeFactor(physicObjectDef,1.000000);
PhysicObjectDef.setLinearVelocity(physicObjectDef,Vector2.create(0.000000,0.000000));
PhysicObjectDef.setAngluarVelocity(physicObjectDef,0.000000);
PhysicObjectDef.setLinearDamping(physicObjectDef,0.000000);
PhysicObjectDef.setAngluarDamping(physicObjectDef,0.000000);
PhysicObjectDef.setFixedRotation(physicObjectDef,0);
PhysicObjectDef.setAllowSleep(physicObjectDef,0);
PhysicObjectDef.setSleep(physicObjectDef,0);
PhysicObjectDef.setGravity(physicObjectDef,Vector2.create(0.000000,-0.060000));
PhysicObjectDef.setGroupIndex(physicObjectDef,1);
PhysicObjectDef.setCategoryMask(physicObjectDef,1);
PhysicObjectDef.setMaskBits(physicObjectDef,65535);
PhysicObjectDef.setShapeType(physicObjectDef,ShapeType.TriangleMesh);
local shape = PhysicObjectDef.getShape(physicObjectDef);
Shape.setFriction(shape,0.200000);
Shape.setDensity(shape,1.000000);
TriangleMesh.setTriangleMesh(shape,{-1.000000, -1.000000,1.000000, -1.000000,1.000000, 1.000000,-1.000000, 1.000000,-0.312500, 4.437500,3.000000, 3.812500,2.125000, 2.750000,-0.625000, 2.625000,-2.812500, -2.312500,1.125000, -3.062500,0.562500, -4.500000,-3.562500, -3.375000,-2.437500, 1.312500,-3.312500, -0.625000,-5.187500, 0.812500,-4.250000, 2.312500,2.875000, 0.250000,3.000000, -2.750000,5.625000, -1.187500,5.750000, 1.500000,3.437500,2.312500},{0,1,2,0,2,3,7,6,5,7,5,4,11,10,9,11,9,8,14,13,12,14,12,15,16,17,18,16,18,19,16,19,20});
physicObjects[0] = PhysicsManager.createBodyDef();
--Layer Objects
local layerObjects = {};
local layerObjectDef = LayerObjectManager.getLayerObjectDef();
LayerObjectDef.setPosition(layerObjectDef,Vector2.create(x + -15.989582,y + -2.187500));
LayerObjectDef.setRotation(layerObjectDef,0.000000);
LayerObjectDef.setScale(layerObjectDef,Vector2.create(1.000000,1.000000));
LayerObjectDef.setLayerId(layerObjectDef,7);
LayerObjectDef.setDepth(layerObjectDef,0.000000);
LayerObjectDef.setLayerObjectType(layerObjectDef,0);
local drawElements = LayerObjectDef.getDrawElements(layerObjectDef);
DrawElements.setColorVertices(drawElements, {-1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
-1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
-0.312500, 4.437500, 0.000000, 1.000000, 0.000000, 1.000000,
3.000000, 3.812500, 0.000000, 1.000000, 0.000000, 1.000000,
2.125000, 2.750000, 0.000000, 1.000000, 0.000000, 1.000000,
-0.625000, 2.625000, 0.000000, 1.000000, 0.000000, 1.000000,
-2.812500, -2.312500, 1.000000, 1.000000, 1.000000, 1.000000,
1.125000, -3.062500, 1.000000, 0.000000, 1.000000, 1.000000,
0.562500, -4.500000, 1.000000, 0.000000, 1.000000, 1.000000,
-3.562500, -3.375000, 1.000000, 0.000000, 1.000000, 1.000000,
-2.437500, 1.312500, 0.000000, 0.000000, 1.000000, 1.000000,
-3.312500, -0.625000, 0.000000, 0.000000, 1.000000, 1.000000,
-5.187500, 0.812500, 0.000000, 0.000000, 1.000000, 1.000000,
-4.250000, 2.312500, 0.000000, 0.000000, 1.000000, 1.000000,
2.875000, 0.250000, 1.000000, 0.000000, 0.000000, 1.000000,
3.000000, -2.750000, 1.000000, 0.000000, 0.000000, 1.000000,
5.625000, -1.187500, 1.000000, 0.000000, 0.000000, 1.000000,
5.750000, 1.500000, 1.000000, 0.000000, 0.000000, 1.000000,
3.437500, 2.312500, 1.000000, 0.000000, 0.000000, 1.000000});
DrawElements.setTriangleIndices(drawElements,{0,1,2,0,2,3,7,6,5,7,5,4,11,10,9,11,9,8,14,13,12,14,12,15,16,17,18,16,18,19,16,19,20});
layerObjects[0] = LayerObjectManager.createLayerObjectDef();
--Entities
end
local worldPoint = Toolset.getDrawPoint();
initTriangleMesh(Vector2.getX(worldPoint), Vector2.getY(worldPoint));
