function renderBlackWhite(layerId,layerRenderer)
	
	local gray = EffectManager.getShaderBinder(GRAY);
	if(gray ~= nil) then
		local x,y = InputManager.getMousePositionXY();
		local darkValue = x / 1280;
		ShaderBinder.setFloat(gray,darkness,darkValue);
		ShaderBinder.bind(gray);
	end
	
	local other = EffectManager.getLayerBlender(OTHER);
	if(other ~= nil) then
		LayerBlender.setBlending(other);
	end
	
	LayerRenderer.renderLayer(layerRenderer, layerId);
	
	EffectManager.setAlphaBlendingFunc();
	ShaderBinder.unbind();
	
	local fbo = EffectManager.getFbo(FIRST);
	if(fbo ~= nil) then
		--Fbo.bind(fbo);
		--LayerRenderer.renderLayer(layerRenderer, layerId);
		--EffectManager.bindEffectFbo();
		--EffectManager.renderFbo(fbo);	
	end
end