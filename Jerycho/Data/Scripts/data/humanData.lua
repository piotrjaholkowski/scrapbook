function onHumanDataAccess(dataComponent, dataField, elementId)
	print('Data access');
end

function onLifeAccess(dataComponent, dataField, elementId)
	print('Life access');
end

function onPositionAccess(dataComponent, dataField, elementId)
	print('Position access');
end

function onInitHumanData(dataComponent)
	print('Init human data');
	--DataComponent.setFloat(dataComponent, HumanDataModel.life, 666);
	DataComponent.setFloat(dataComponent, HumanDataModel.life, 666);
	
	DataComponent.setInt32(dataComponent, HumanDataModel.position, 0);
	DataComponent.setInt32Element(dataComponent, HumanDataModel.position, 1, 1);
	DataComponent.setInt32Element(dataComponent, HumanDataModel.position, 2, 2);
	
	--DataComponent.setElement(dataComponent, HumanDataModel.position, 1, 1);
	--DataComponent.set(dataComponent, HumanDataModel.position, {666,666,666});
	--DataComponent.setFloatArray(dataComponent, HumanDataModel.life, {666,777});
end

ScriptManager.addFunction(ScriptFunctionType.InitData,   'onInitHumanData',   onInitHumanData);
ScriptManager.addFunction(ScriptFunctionType.AccessData, 'onHumanDataAccess', onHumanDataAccess);
ScriptManager.addFunction(ScriptFunctionType.AccessData, 'onLifeAccess',      onLifeAccess);
ScriptManager.addFunction(ScriptFunctionType.AccessData, 'onPositionAccess',  onPositionAccess);

local lifeData = DataFieldInfo.create();
DataFieldInfo.setDataType(lifeData, DataType.float);
DataFieldInfo.setDataWidget(lifeData, DataWidget.Scrollbar);
DataFieldInfo.setRange(lifeData, 0, 100);
DataFieldInfo.setDataAccess(lifeData, {'onLifeAccess', 'dataModels/humanData.lua'});

local positionData = DataFieldInfo.create();
DataFieldInfo.setDataType(positionData, DataType.int32);
DataFieldInfo.setDataWidget(positionData, DataWidget.Scrollbar);
DataFieldInfo.setRange(positionData, 0, 5);
DataFieldInfo.setDataAccess(positionData, {'onPositionAccess', 'dataModels/humanData.lua'})

local HumanData    = {}
HumanData.life     = {1, lifeData };
HumanData.position = {3, positionData};





--HumanData.position = {DataType.int32, 2, DataGui.create(DataGui.Label)  };


--criptManager.addFunction(ScriptFunctionType.AccesData, )

--ResourceManager.addDataModel('HumanDataModel', HumanData, {'onInitHumanData', 'dataModels/humanData.lua'}, {'onHumanDataAccess', 'dataModels/humanData.lua'});

ResourceManager.addDataModel('HumanDataModel', HumanData, {'onInitHumanData', 'dataModels/humanData.lua'});




