function initConvexPolygon(x,y)
--Physics Objects
local physicObjects = {};
local multiShape = {};
local physicObjectDef = PhysicsManager.getPhysicObjectDef();
--Physic object 0
PhysicObjectDef.setPosition(physicObjectDef,Vector2.create(x + -11.375000,y + 7.312500));
PhysicObjectDef.setRotation(physicObjectDef,0.000000);
PhysicObjectDef.setBodyType(physicObjectDef,0);
PhysicObjectDef.setTimeFactor(physicObjectDef,1.000000);
PhysicObjectDef.setLinearVelocity(physicObjectDef,Vector2.create(0.000000,0.000000));
PhysicObjectDef.setAngluarVelocity(physicObjectDef,0.000000);
PhysicObjectDef.setLinearDamping(physicObjectDef,0.000000);
PhysicObjectDef.setAngluarDamping(physicObjectDef,0.000000);
PhysicObjectDef.setFixedRotation(physicObjectDef,0);
PhysicObjectDef.setAllowSleep(physicObjectDef,0);
PhysicObjectDef.setSleep(physicObjectDef,0);
PhysicObjectDef.setGravity(physicObjectDef,Vector2.create(0.000000,-0.060000));
PhysicObjectDef.setGroupIndex(physicObjectDef,1);
PhysicObjectDef.setCategoryMask(physicObjectDef,1);
PhysicObjectDef.setMaskBits(physicObjectDef,65535);
PhysicObjectDef.setShapeType(physicObjectDef,ShapeType.ConvexPolygon);
local shape = PhysicObjectDef.getShape(physicObjectDef);
Shape.setFriction(shape,-431602080.000000);
Shape.setDensity(shape,-431602080.000000);
ConvexPolygon.setVertices(shape,{-1.333333, -0.666667,
0.666667, -0.666667,
0.666667,1.333333});
physicObjects[0] = PhysicsManager.createBodyDef();
--Layer Objects
local layerObjects = {};
local layerObjectDef = LayerObjectManager.getLayerObjectDef();
--Entities
end
local worldPoint = Toolset.getDrawPoint();
initConvexPolygon(Vector2.getX(worldPoint), Vector2.getY(worldPoint));
