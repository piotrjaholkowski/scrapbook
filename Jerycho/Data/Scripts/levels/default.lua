function initLevel() 

LevelManager.printCurrentTask('Creating layers');
EffectManager.createLayer('Layer 0');
EffectManager.createLayer('Layer 1');
EffectManager.createLayer('Layer 2');
EffectManager.createLayer('Layer 3');
EffectManager.createLayer('Layer 4');
EffectManager.createLayer('Layer 5');
EffectManager.createLayer('Layer 6');
LevelManager.printFinishedTask('Layers created');

LevelManager.printCurrentTask('Creating parallaxes');
EffectManager.createParallax("Active camera", nil, true);
LevelManager.printFinishedTask('Parallaxes created');

LevelManager.printCurrentTask('Loading Complete');
--print('Init level');
end

LevelManager.printCurrentTask('Loading resources');

LevelManager.printCurrentTask('Loading shaders');
LevelManager.loadShader('default.vert', ShaderType.VertexShader);
LevelManager.loadShader('default.frag', ShaderType.FragmentShader);
LevelManager.loadShader('default.geom', ShaderType.GeometryShader);
LevelManager.loadShader('fboRender.vert', ShaderType.VertexShader);
LevelManager.loadShader('fboRender.frag', ShaderType.FragmentShader);
LevelManager.loadShader('fboRenderDebug.frag', ShaderType.FragmentShader);
LevelManager.loadShader('gaussianBlur.frag', ShaderType.FragmentShader);
LevelManager.printFinishedTask('Shaders loaded');

LevelManager.printCurrentTask('Loading shader binders');
LevelManager.createShaderBinder('DEFAULT','default.vert','default.frag');
--LevelManager.createShaderBinder('DEFAULT','default.vert','default.frag', 'default.geom');
LevelManager.createShaderBinder('FBO','fboRender.vert','fboRender.frag');
LevelManager.createShaderBinder('FBO_DEBUG','fboRender.vert','fboRenderDebug.frag');
LevelManager.createShaderBinder('GAUSSIAN_BLUR','fboRender.vert','gaussianBlur.frag');
LevelManager.printFinishedTask('Shader binders loaded');

LevelManager.printCurrentTask('Loading material shader definitions');
require "Data/Scripts/materials/materialDef"
LevelManager.printFinishedTask('Material shader definitions loaded');

LevelManager.printCurrentTask('Loading materials');
-- Add materials here
local defaultMaterial = LevelManager.createMaterial('DEFAULT', 'DEFAULT');
Material.setFloat(defaultMaterial, MaterialDef.DEFAULT.darkness, 0.5);
Material.setBool(defaultMaterial, MaterialDef.DEFAULT.isGlowing, false);
Material.setBool(defaultMaterial, MaterialDef.DEFAULT.linearGradient, true);
Material.setBool(defaultMaterial, MaterialDef.DEFAULT.radialGradient, false);
-- Set material settings
LevelManager.printFinishedTask('Materials loaded');

LevelManager.printCurrentTask('Loading shader object definitions');
require "Data/Scripts/shaderObjects/shaderObjectDef"
LevelManager.printFinishedTask('Shader object definitions loaded');

LevelManager.printFinishedTask('Resources loaded');


print('Script EXECUTED');