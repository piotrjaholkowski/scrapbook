--print('Adding materials');



local darknessField = DataFieldInfo.create();
DataFieldInfo.setDataType(darknessField, DataType.float);
DataFieldInfo.setDataWidget(darknessField, DataWidget.Scrollbar);
DataFieldInfo.setRange(darknessField, 0, 1);

local isGlowingField = DataFieldInfo.create();
DataFieldInfo.setDataType(isGlowingField, DataType.bool);
DataFieldInfo.setDataWidget(isGlowingField, DataWidget.Checkbox);

local linearGradientField = DataFieldInfo.create();
DataFieldInfo.setDataType(linearGradientField, DataType.bool);
DataFieldInfo.setDataWidget(linearGradientField, DataWidget.Checkbox);

local radialGradientField = DataFieldInfo.create();
DataFieldInfo.setDataType(radialGradientField, DataType.bool);
DataFieldInfo.setDataWidget(radialGradientField, DataWidget.Checkbox);

local materialProperties = {}
materialProperties.darkness  = {1, darknessField};
materialProperties.isGlowing = {1, isGlowingField}
materialProperties.linearGradient = {1, linearGradientField};
materialProperties.radialGradient = {1, radialGradientField};

local defaultCore = ResourceManager.addMaterialShaderDef('DEFAULT','DEFAULT',materialProperties);
--MaterialCore.setBlendEquation(materialCore, GLenum.ADD);
--MaterialCore.setSourceFactor(materialCore, GLenum.GL_ONE);
--MaterialCore.setDestinationFactor(materialCore, GLenum.GL_ONE_MINUS_SRC_ALPHA);
--MaterialCore.setShaderBinder(defaultCore, 'DEFAULT');
--MaterialCore.setProperties(defaultCore, materialProperties);

--ResourceManager.addMaterialCore('DEFAULT',defaultCore);
--MaterialCores.DEFAULT.darkness;
--Material.setFloat(newMaterial, MaterialCores.DEFAULT.darkness, 666);

--ResourceManager.addMaterial('DEFAULT', materialCore, materialProperties);






