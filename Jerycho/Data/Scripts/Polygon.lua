function initPolygon(x,y)
--Physics Objects
local physicObjects = {};
local multiShape = {};
local physicObjectDef = PhysicsManager.getPhysicObjectDef();
--Layer Objects
local layerObjects = {};
local layerObjectDef = LayerObjectManager.getLayerObjectDef();
LayerObjectDef.setPosition(layerObjectDef,Vector2.create(x + -17.062500,y + 1.187500));
LayerObjectDef.setRotation(layerObjectDef,0.000000);
LayerObjectDef.setScale(layerObjectDef,Vector2.create(1.000000,1.000000));
LayerObjectDef.setLayerId(layerObjectDef,7);
LayerObjectDef.setDepth(layerObjectDef,0.000000);
LayerObjectDef.setLayerObjectType(layerObjectDef,0);
local drawElements = LayerObjectDef.getDrawElements(layerObjectDef);
DrawElements.setColorVertices(drawElements, {-1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
4.714952, 4.250000, 0.233333, 0.000000, 0.444444, 0.544444,
-4.714951, 4.250000, 0.233333, 0.000000, 0.444444, 0.544444});
DrawElements.setTriangleIndices(drawElements,{0,1,2,0,2,3});
layerObjects[0] = LayerObjectManager.createLayerObjectDef();
--Entities
end
local worldPoint = Toolset.getDrawPoint();
initPolygon(Vector2.getX(worldPoint), Vector2.getY(worldPoint));
