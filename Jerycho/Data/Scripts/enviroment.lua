print "Setting script enviroment constants"
--JA_LOC

--GUI STATES
TR_MAINMENU_MODE = 0
TR_GAMEPLAY_MODE = 1
TR_SPRITEEDITOR_MODE = 2
TR_BLANK_MODE = 3
TR_SCRIPTCONSOLE_MODE = 4
TR_PHYSICSEDITOR_MODE = 5
TR_ENTITYEDITOR_MODE = 6
TR_EFFECTEDITOR_MODE = 7
TR_LOADING_MODE = 8
TR_BONEEDITOR_MODE = 9
TR_SCRIPTGUI_MODE = 10

--GUI
JA_LABEL = 1
JA_INTERFACE = 2
JA_BUTTON = 3
JA_MENU = 4
JA_HUD = 5
JA_IMAGE_BUTTON = 6
JA_SWITCH_BUTTON = 7
JA_EDITBOX = 8
JA_SCROLLBAR = 9
JA_PROGRESS_BUTTON = 10
JA_TIMER = 11
JA_CHART = 12
JA_GRID_LAYOUT = 13
JA_CHECKBOX = 14

--CONSTRAINT
JA_RIGID_BODY_CONTACT2D = 0
JA_REVOLUTE_JOINT2D = 1
JA_SPRING_JOINT2D = 2
JA_LINEAR_MOTOR2D = 3

--SHAPE TYPES
JA_BOX            = 0
JA_CIRCLE         = 1
JA_CONVEX_POLYGON = 2
JA_PARTICLES      = 3
JA_MULTI_SHAPE    = 4
JA_TRIANGLE_MESH  = 5
JA_POINT          = 6

--COMPONENTS
JA_PHYSIC = 0
JA_PLAYER = 1
JA_PARTICLES = 2
JA_TIMER = 3
JA_SENSOR = 4
JA_CAMERA = 5
JA_SOUND = 6
JA_SCRIPT = 7
JA_TEXT = 8
JA_SPRITE = 9


--LOGIC_GATE_TYPES
JA_BOOL = 0
JA_INT = 1
JA_FLOAT = 2
JA_STRING = 4

--print(arg[0]);
local info = debug.getinfo(1,'S');
print(info.source);