

function onUpdateTest(deltaTime,entity,scriptComponent)
	if(InputManager.isKeyPressed(97)) then
		print("State 1")
		ScriptComponent.setFunction(scriptComponent, 'onUpdateTest2');
	end
	--print(val)
end
ScriptManager.addFunction(ScriptFunctionType.UpdateEntity, 'onUpdateTest', onUpdateTest);

function onUpdateTest2(deltaTime,entity,scriptComponent)
	if(InputManager.isKeyPressed(97)) then
		print("State 2")
		ScriptComponent.setFunction(scriptComponent, 'onUpdateTest');
	end
	--print(val)
end
ScriptManager.addFunction(ScriptFunctionType.UpdateEntity, 'onUpdateTest2', onUpdateTest2);

function onBeginContact(component,constraint)
	print('Begin contact');
end
ScriptManager.addFunction(ScriptFunctionType.ConstraintNotifier, 'onBeginContact', onBeginContact);

function onEndContact(component,constraint)
	print('End contact');
end
ScriptManager.addFunction(ScriptFunctionType.ConstraintNotifier, 'onEndContact', onEndContact);

print('ScriptLoadTest executed');
