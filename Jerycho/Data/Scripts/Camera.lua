function initCamera(x,y)
--Physics Objects
local physicObjects = {};
local multiShape = {};
local physicObjectDef = PhysicsManager.getPhysicObjectDef();
--Layer Objects
local layerObjects = {};
local layerObjectDef = LayerObjectManager.getLayerObjectDef();
LayerObjectDef.setPosition(layerObjectDef,Vector2.create(x + -13.250000,y + 1.125000));
LayerObjectDef.setRotation(layerObjectDef,0.000000);
LayerObjectDef.setScale(layerObjectDef,Vector2.create(1.000000,1.000000));
LayerObjectDef.setLayerId(layerObjectDef,0);
LayerObjectDef.setDepth(layerObjectDef,0.000000);
LayerObjectDef.setLayerObjectType(layerObjectDef,LayerObjectType.Camera);
LayerObjectDef.setCameraName(layerObjectDef,'Camera');
local morphElements = LayerObjectDef.getCameraMorphElements(layerObjectDef);
MorphElements.clear(morphElements);
local cameraMorph = MorphElements.createMorph(morphElements,'big');
CameraMorph.setPosition(cameraMorph,Vector2.create(-13.250000,1.125000));
CameraMorph.setRotation(cameraMorph, 0.000000);
CameraMorph.setScale(cameraMorph, Vector2.create(1.000000,1.000000));
CameraMorph.setActiveFeatures(cameraMorph, 8)local cameraMorph = MorphElements.createMorph(morphElements,'small');
CameraMorph.setPosition(cameraMorph,Vector2.create(-13.250000,1.125000));
CameraMorph.setRotation(cameraMorph, 0.000000);
CameraMorph.setScale(cameraMorph, Vector2.create(0.351960,0.351960));
CameraMorph.setActiveFeatures(cameraMorph, 8)layerObjects[0] = LayerObjectManager.createLayerObjectDef();
--Entities
end
--Script files
local worldPoint = Toolset.getDrawPoint();
initCamera(Vector2.getX(worldPoint), Vector2.getY(worldPoint));
