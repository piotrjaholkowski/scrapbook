local driveForce = Vector2.create(0.0,0.1);
local angVelocity = 2.0;

function onFly(deltaTime,entity,component)
	local key_w = 119;
	local key_a = 97;
	local key_d = 100;
	
	local drifterBody = Entity.getPhysicObject(entity);
	
	if(drifterBody ~= nil) then
	
		if(InputManager.isKeyPress(key_w)) then
			--print("key w press");
			local rotMatrix     = RigidBody.getRotationMatrix(drifterBody);
			local directedForce = Matrix2.rotateVector(rotMatrix,driveForce);
			RigidBody.applyForceToCenter(drifterBody,directedForce);
			--spawnDelay = ParticleGenerator.getSpawnDelay(particles);
		else
			--ParticleGenerator.setSpawnDelay(particles,20000);
		end
		
		if(InputManager.isKeyPress(key_a)) then
			--print("key a press");
			RigidBody.setAngularVelocity(drifterBody,angVelocity);
		end
				
		if(InputManager.isKeyReleased(key_a)) then
			--print("key a released");
			RigidBody.setAngularVelocity(drifterBody,0);
		end
				
		if(InputManager.isKeyPress(key_d)) then
			--print("key d press");
			RigidBody.setAngularVelocity(drifterBody,-angVelocity);
		end
				
		if(InputManager.isKeyReleased(key_d)) then
			--print("key a released");
			RigidBody.setAngularVelocity(drifterBody,0);
		end
	
	end
end

--function onUpdateTest(deltaTime,entity,scriptComponent)
--	if(InputManager.isKeyPressed(97)) then
--		print("State 1")
--		ScriptComponent.setFunction(scriptComponent, 'onUpdateTest2');
--	end
--	--print(val)
--end


ScriptManager.addFunction(ScriptFunctionType.UpdateEntity, 'onFly', onFly);




