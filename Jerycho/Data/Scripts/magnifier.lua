
function onUpdateMagnifier(deltaTime,entity)
	
	local physicObject = Entity2d.getPhysicObject(entity);
	
	if(physicObject ~= nil) then
			local position = RigidBody.getPosition(physicObject);
			local camera = GameManager.getActiveCamera();
	
			if(InputManager.isKeyPress(119)) then
				--print("key w press");
				local rotMatrix = RigidBody.getRotationMatrix(physicObject);
				local tForce = Matrix2.rotateVector(rotMatrix,force);
				RigidBody.applyForceToCenter(physicObject,tForce);
			end
			
			if(InputManager.isKeyPress(97)) then
				--print("key a press");
				RigidBody.setAngularVelocity(physicObject,angVelocity);
			end
			
			if(InputManager.isKeyReleased(97)) then
				--print("key a released");
				RigidBody.setAngularVelocity(physicObject,0);
			end
			
			if(InputManager.isKeyPress(100)) then
				--print("key d press");
				RigidBody.setAngularVelocity(physicObject,-angVelocity);
			end
			
			if(InputManager.isKeyReleased(100)) then
				--print("key a released");
				RigidBody.setAngularVelocity(physicObject,0);
			end
			
			local linVel = RigidBody.getLinearVelocity(physicObject);
			local vel = linVel:length();
			if (vel > maxVelocity) then
				vel = maxVelocity;
			end
	end
	
end