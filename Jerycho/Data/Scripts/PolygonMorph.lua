function initPolygonMorph(x,y)
--Physics Objects
local physicObjects = {};
local multiShape = {};
local physicObjectDef = PhysicsManager.getPhysicObjectDef();
--Layer Objects
local layerObjects = {};
local layerObjectDef = LayerObjectManager.getLayerObjectDef();
LayerObjectDef.setPosition(layerObjectDef,Vector2.create(x + -13.062500,y + 3.437500));
LayerObjectDef.setRotation(layerObjectDef,0.000000);
LayerObjectDef.setScale(layerObjectDef,Vector2.create(1.000000,1.000000));
LayerObjectDef.setLayerId(layerObjectDef,0);
LayerObjectDef.setDepth(layerObjectDef,0.000000);
LayerObjectDef.setLayerObjectType(layerObjectDef,LayerObjectType.Polygon);
local drawElements = LayerObjectDef.getDrawElements(layerObjectDef);
DrawElements.setColorVertices(drawElements, {-1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
-1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000});
DrawElements.setTriangleIndices(drawElements,{0,1,2,0,2,3});
local morphElements = LayerObjectDef.getPolygonMorphElements(layerObjectDef);
MorphElements.clear(morphElements);
local polygonMorph = MorphElements.createMorph(morphElements,'small');
local drawElements = PolygonMorph.getDrawElements(polygonMorph);
DrawElements.setColorVertices(drawElements, {-1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, -1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000,
-1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000});
DrawElements.setTriangleIndices(drawElements,{0,1,2,0,2,3});
PolygonMorph.setPosition(polygonMorph,Vector2.create(-13.062500,3.437500));
PolygonMorph.setRotation(polygonMorph, 0.000000);
PolygonMorph.setScale(polygonMorph, Vector2.create(1.000000,1.000000));
PolygonMorph.setActiveFeatures(polygonMorph, 0);
layerObjects[0] = LayerObjectManager.createLayerObjectDef();
--Entities
end
--Script files
local worldPoint = Toolset.getDrawPoint();
initPolygonMorph(Vector2.getX(worldPoint), Vector2.getY(worldPoint));
