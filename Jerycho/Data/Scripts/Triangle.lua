function initTriangle(x,y)
--Physics Objects
local physicObjectDef = PhysicsManager.getPhysicObjectDef();
--Physic object 0
PhysicObjectDef.setPosition(physicObjectDef,Vector2.create(x + 0.062500,y + -3.062500));
PhysicObjectDef.setRotation(physicObjectDef,0.000000);
PhysicObjectDef.setBodyType(physicObjectDef,0);
PhysicObjectDef.setTimeFactor(physicObjectDef,1.000000);
PhysicObjectDef.setLinearVelocity(physicObjectDef,Vector2.create(0.000000,0.000000));
PhysicObjectDef.setAngluarVelocity(physicObjectDef,0.000000);
PhysicObjectDef.setLinearDamping(physicObjectDef,0.000000);
PhysicObjectDef.setAngluarDamping(physicObjectDef,0.000000);
PhysicObjectDef.setFixedRotation(physicObjectDef,0);
PhysicObjectDef.setAllowSleep(physicObjectDef,0);
PhysicObjectDef.setSleep(physicObjectDef,0);
PhysicObjectDef.setGravity(physicObjectDef,Vector2.create(0.000000,-0.060000));
PhysicObjectDef.setGroupIndex(physicObjectDef,1);
PhysicObjectDef.setCategoryMask(physicObjectDef,1);
PhysicObjectDef.setMaskBits(physicObjectDef,65535);
PhysicObjectDef.setShapeType(physicObjectDef,ShapeType.TriangleMesh);
local shape = PhysicObjectDef.getShape(physicObjectDef);
Shape.setFriction(shape,0.200000);
Shape.setDensity(shape,1.000000);
TriangleMesh.setTriangleMesh(shape,{-1.000000, -1.000000,1.000000, -1.000000,1.000000, 1.000000,-1.000000,1.000000},{0,1,2,0,2,3});
PhysicsManager.createBodyDef();
--Layer Objects
local layerObjectDef = LayerObjectManager.getLayerObjectDef();
end
local worldPoint = Toolset.getDrawPoint();
initTriangle(Vector2.getX(worldPoint), Vector2.getY(worldPoint));
