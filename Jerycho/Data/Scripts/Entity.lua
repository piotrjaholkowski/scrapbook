function onUpdateDupa(delta,entity,component)
--print('dupa');
end

function initEntity(x,y)
--Physics Objects
local physicObjects = {};
local physicObjectDef = PhysicsManager.getPhysicObjectDef();

--Physic object 0
PhysicObjectDef.setPosition(physicObjectDef,Vector2.create(x + 0.062500,y + -3.062500));
PhysicObjectDef.setRotation(physicObjectDef,0.000000);
PhysicObjectDef.setBodyType(physicObjectDef,0);
PhysicObjectDef.setTimeFactor(physicObjectDef,1.000000);
PhysicObjectDef.setLinearVelocity(physicObjectDef,Vector2.create(0.000000,0.000000));
PhysicObjectDef.setAngluarVelocity(physicObjectDef,0.000000);
PhysicObjectDef.setLinearDamping(physicObjectDef,0.000000);
PhysicObjectDef.setAngluarDamping(physicObjectDef,0.000000);
PhysicObjectDef.setFixedRotation(physicObjectDef,0);
PhysicObjectDef.setAllowSleep(physicObjectDef,0);
PhysicObjectDef.setSleep(physicObjectDef,0);
PhysicObjectDef.setGravity(physicObjectDef,Vector2.create(0.000000,-0.060000));
PhysicObjectDef.setGroupIndex(physicObjectDef,1);
PhysicObjectDef.setCategoryMask(physicObjectDef,1);
PhysicObjectDef.setMaskBits(physicObjectDef,65535);

PhysicObjectDef.setShapeType(physicObjectDef,JA_BOX);
local shape = PhysicObjectDef.getShape(physicObjectDef); --Box
Box.setWidth(shape,1.000000);
Box.setHeight(shape,1.000000);
physicObjects[0] = PhysicsManager.createBodyDef();
--Layer Objects
local layerObjectDef = LayerObjectManager.getLayerObjectDef();
--Entities
local entity = EntityManager.createEntity();
Entity.setTagName(entity,'spaceship');
local physicComponent = PhysicComponent.create(entity);
Component.setTagName(physicComponent, 'monkey');
PhysicComponent.setPhysicObject(physicComponent, physicObjects[0]);
PhysicComponent.setAutoDelete(physicComponent,true);
PhysicComponent.setRoot(physicComponent);
local scriptComponent = ScriptComponent.create(entity);
Component.setTagName(scriptComponent, 'dupa');
ScriptComponent.setFunction(scriptComponent, 'onUpdateTest', 'ScriptLoadTest.lua');

end

require "/Data/Scripts/ScriptLoadTest"
--ScriptManager.addFunction(ScriptFunctionType.UpdateEntity, 'onUpdateDupa', onUpdateDupa);
local worldPoint = Toolset.getDrawPoint();
initEntity(Vector2.getX(worldPoint), Vector2.getY(worldPoint));
