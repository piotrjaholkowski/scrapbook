
function onUpdateFrequency(deltaTime,entity)	
	local frequency = SoundManager.getBiggestIndex();
	local effect = EffectManager.getEffect(7);
	local shaderBinder = Effect.getShaderBinder(effect,JA_NO_EFFECT,1);
	local uvDelta = (frequency / 128) * 0.1; 
	ShaderBinder.setUniformVec3(shaderBinder,"uvDelta",uvDelta,uvDelta,0);
	frequency = frequency * 2;
	--DisplayManager.setBackgroundColor(frequency, frequency, frequency);
end
