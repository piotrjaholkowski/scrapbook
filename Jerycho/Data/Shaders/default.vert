#version 330
//All above matches openGL version
//#version 150 - 3.2
//#version 140 - 3.1
//#version 130 - 3.0
//#version 120 - 2.1
//#version 110 - 1.1
//#extension extension_name? : behavior?

//built in types
//attribute vec4 gl_Color;
//attribute vec4 gl_SecondaryColor;
//attribute vec3 gl_Normal;
//attribute vec4 gl_Vertex;
//attribute vec4 gl_MultiTexCoord0;
//attribute vec4 gl_MultiTexCoord1;
//attribute vec4 gl_MultiTexCoord2;
//attribute vec4 gl_MultiTexCoord3;
//attribute vec4 gl_MultiTexCoord4;
//attribute vec4 gl_MultiTexCoord5;
//attribute vec4 gl_MultiTexCoord6;
//attribute vec4 gl_MultiTexCoord7;
//attribute float gl_FogCoord;
//varying vec2 texcoord; // przekazywane do fragment shadera

struct Material
{
	bool isGlowing;
	bool linearGradient;
	bool radialGradient;
	//bool isLight;
	//bool isTransparent;
	float darkness;
};

struct Object
{
	bool testValue;
};

uniform mat4 mvpMatrix;

uniform mat4 mvpMatrices[64];
uniform mat4 rotMatrices[64];

uniform Settings
{
	vec2 resolution;
	//put a layer settings here like darkness and blurr values
	//put a global frame settings here like lights
};

//layout (std140) uniform Materials // shared by default
uniform Materials
{
   Material materials[32]; 
};

uniform Objects
{
	Object objects[64];
};

layout(location = 0) in vec2 vertexPosition;
layout(location = 1) in vec4 vertexColor;
layout(location = 2) in vec4 vertexNormal;
layout(location = 3) in vec4 vertexColor2;
layout(location = 4) in int materialId;
layout(location = 5) in int objectId;

out vec4 fragmentColor;

/*
in int gl_VertexID; // adds base vertex
in int gl_InstanceID; // 0 when not using instancing
*/

/* Predefined outputs
out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};
*/

out VS_OUT {
    //vec3 color;
	flat int objectId;
	flat int materialId;
} vs_out;


void main(void)
{
   //texcoord = gl_MultiTexCoord0; bo nie wie jak rzutowa� trzeba st
   //texcoord = gl_MultiTexCoord0.st; //to jest i tak przekazywane
   //vec4 v = vec4(gl_Vertex); chyba nie trzeba dodatkowej zmienej   
   //gl_Position = gl_ModelViewProjectionMatrix * v;
   //gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
   
   //gl_Position = ftransform();
   //gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
   //gl_FrontColor = gl_Color;
   //gl_TexCoord[0] = gl_MultiTexCoord0;
   //gl_Position = vec4(1.0, 1.0, 1.0, 1.0);
   
   // Output position of the vertex, in clip space : MVP * position
  vs_out.objectId   = objectId;
  vs_out.materialId = materialId;
  //gl_Position   = mvpMatrix * vec4(vertexPosition, 1, 1);
  gl_Position   = mvpMatrices[objectId] * vec4(vertexPosition, 1, 1);
  fragmentColor = vertexColor;
 
  //if(objectId == 0)
	//  fragmentColor = vec4(1,0,0,1);
  //
  //if(objectId == 1)
	//  fragmentColor = vec4(0,1,0,1);
  //
  //if(objectId == 2)
	//  fragmentColor = vec4(0,0,1,1);
  //fragmentColor = vec4(0,1,0,1);
  //vs_out.color = color;
}