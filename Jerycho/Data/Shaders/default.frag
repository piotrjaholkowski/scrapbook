#version 330
//All above matches openGL version
//#version 150 - 3.2
//#version 140 - 3.1
//#version 130 - 3.0
//#version 120 - 2.1
//#version 110 - 1.1
//#extension extension_name​ : behavior​

//layout(early_fragment_tests) in;

//built in types 3.0
/*
in vec4 gl_FragCoord; // window relative coordinates
in bool gl_FrontFacing; // front or back facing primitive
in vec2 gl_PointCoord;
*/

//built in types 4.0
/*
in int gl_SampleID;
in vec2 gl_SamplePosition;
in int gl_SampleMaskIn[];
*/

/*
in float gl_ClipDistance[];
in int gl_PrimitiveID;
*/

//built in types 4.3
/*
in int gl_Layer;
in int gl_ViewportIndex;
*/

//out float gl_FragDepth;

//out vec4 colorOut;
in vec4 fragmentColor;

//layout(location = 0) out vec4 diffuseColor; //color attachment at 0 when using fbo
layout(location = 0) out vec4 color;
layout(location = 1) out vec4 glow;


struct Material
{
	bool isGlowing;
	bool linearGradient;
	bool radialGradient;
	//bool isLight;
	//bool isTransparent;
	float darkness;
};

struct Object
{
	bool testValue;
};

uniform Settings
{
	vec2 resolution;
};

//layout (std140) uniform Materials // shared by default
uniform Materials
{
	Material materials[32];
};

uniform Objects
{
	Object objects[64];
};

in VS_OUT {
    //vec3 color;
	flat int objectId;
	flat int materialId;
} vs_out;

uniform sampler2D texture;


void main(void)
{
	color = fragmentColor;
	
	vec2 windowPos = (gl_FragCoord.xy / resolution) - vec2(0.5,0.5);
	
	vec2 colorGradientPos0 = vec2(0.0,0.0);
	vec2 colorGradientPos1 = vec2(0.0,0.5);
	
	vec4 colorGradient0 = vec4(1.0,0.0,0.0,1.0);
	vec4 colorGradient1 = vec4(0.0,1.0,0.0,0.0);
	vec4 colorGradientDiff = colorGradient1 - colorGradient0;
	
	if(materials[vs_out.materialId].linearGradient)
	{		
		vec2 colorGradientPosDiff0 = windowPos   - colorGradientPos0;
		vec2 colorGradientPosDiff1 = colorGradientPos1 - colorGradientPos0;
		vec2 normColorGradientDir  = normalize(colorGradientPosDiff1);
		
		float gradVal0 = dot(normColorGradientDir, colorGradientPosDiff0);
		float gradVal1 = dot(normColorGradientDir, colorGradientPosDiff1);
		
		float gradVal = gradVal0 / gradVal1;
		gradVal = clamp(gradVal,0.0,1.0);
		
		color.rgb = colorGradient0.rgb + (gradVal * colorGradientDiff.rgb);
	}
	
	if(materials[vs_out.materialId].radialGradient)
	{
		float gradientDist = distance(colorGradientPos0, colorGradientPos1);
		float pointDist    = distance(colorGradientPos0, windowPos);
		
		float gradVal = pointDist / gradientDist;
		gradVal = clamp(gradVal,0.0,1.0);
    
		color.rgb = colorGradient0.rgb + (gradVal * colorGradientDiff.rgb);
	}
	
	color.rgb *= materials[vs_out.materialId].darkness;
	
	if(materials[vs_out.materialId].isGlowing == true)
	{
		//finalColor.rgb *= materials[vs_out.materialId].darkness;
		//color = materials[vs_out.materialId].darkness * vec4(1.0,0.0,0.0,1.0);	
		glow  = vec4(0.0,0.0,1.0,1.0);
	}
	else
	{
		//color = materials[vs_out.materialId].darkness * vec4(0.0,1.0,0.0,1.0);
		//color.rgb *= materials[vs_out.materialId].darkness;
		glow  = vec4(1.0,0.0,0.0,1.0);
	}
	
	if(objects[vs_out.objectId].testValue == true)
	{
		color.rgb = vec3(0.0,0.0,1.0);
	}
	
	color.a = fragmentColor.a;
}