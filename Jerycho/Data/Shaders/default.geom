#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

//in gl_PerVertex
//{
//  vec4 gl_Position;
//  float gl_PointSize;
//  float gl_ClipDistance[];
//} gl_in[];

//in int gl_PrimitiveIDIn;
//in int gl_InvocationID;  //Requires GLSL 4.0 or ARB_gpu_shader5

//out int gl_Layer;
//out int gl_ViewportIndex; //Requires GL 4.1 or ARB_viewport_array.

void main()
{
    gl_Position = gl_in[0].gl_Position + vec4(-0.5, 0.0, 0.0, 0.0);
    EmitVertex();

    gl_Position = gl_in[1].gl_Position + vec4(0.0, 0.0, 0.0, 0.0);
    EmitVertex();
	
	gl_Position = gl_in[2].gl_Position + vec4(0.0, 0.0, 0.0, 0.0);
    EmitVertex();

    EndPrimitive();
}