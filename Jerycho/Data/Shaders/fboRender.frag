#version 330 core

in vec2 UV;

out vec4 color;

uniform sampler2D colorTexture;
uniform sampler2D glowTexture;
//uniform sampler2DMS colorTexture;

void main(){
	color.rgb = texture( colorTexture, UV).xyz;
	//color.rgb = texelFetch( colorTexture, UV).xyz;
	color.a = 1.0;
	//color = vec4(0.0,1.0,0.0,1.0);
}