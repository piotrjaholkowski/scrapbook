#version 120

uniform sampler2D texture;

void main(void)
{
    // Convert to greyscale using NTSC weightings
	vec4 decal = texture2D(texture, gl_TexCoord[0].st).rgba;
	decal *= gl_Color;
	float grey = dot(decal.rgb, vec3(0.299, 0.587, 0.114));
	
	// Play with these rgb weightings to get different tones.
	// (As long as all rgb weightings add up to 1.0 you won't lighten or darken the image)
	gl_FragColor = vec4(grey * vec3(1.2, 1.0, 0.8), decal.a);
}