#version 120

uniform sampler2D texture;
float vecSkill[4] = float[4](1.0, 4.0, 0.01, 0.5); //blur, sharpness, speed, range
float time = float(1.0);

void main(void)
{
	vec2 texCoord = gl_TexCoord[0].st;
	
    texCoord.xy -= 0.5;
	texCoord.xy *= 1-(sin(time*vecSkill[2])*vecSkill[3]+vecSkill[3])*0.5;
	texCoord.xy += 0.5;
	
	vec4 decal = texture2D( texture, texCoord.xy);

	decal += texture2D( texture, texCoord.xy+0.001*vecSkill[0]);
	decal += texture2D( texture, texCoord.xy+0.003*vecSkill[0]);
	decal += texture2D( texture, texCoord.xy+0.005*vecSkill[0]);
	decal += texture2D( texture, texCoord.xy+0.007*vecSkill[0]);
	decal += texture2D( texture, texCoord.xy+0.009*vecSkill[0]);
	decal += texture2D( texture, texCoord.xy+0.011*vecSkill[0]);

	decal += texture2D( texture, texCoord.xy-0.001*vecSkill[0]);
	decal += texture2D( texture, texCoord.xy-0.003*vecSkill[0]);
	decal += texture2D( texture, texCoord.xy-0.005*vecSkill[0]);
	decal += texture2D( texture, texCoord.xy-0.007*vecSkill[0]);
	decal += texture2D( texture, texCoord.xy-0.009*vecSkill[0]);
	decal += texture2D( texture, texCoord.xy-0.011*vecSkill[0]);

	decal.rgb = vec3((decal.r+decal.g+decal.b) / 3.0);
	decal /= vecSkill[1];
	gl_FragColor.rgba = decal.rgba;
}