float4x4 World;
 
float4x4 View;
 
// The projection transformation
float4x4 Projection;
 
// The transpose of the inverse of the world transformation,
// used for transforming the vertex's normal
float4x4 WorldInverseTranspose;
 
float3 LightPosition = float3(1, 0, 0);
 
float4 DiffuseColor = float4(1, 1, 1, 1);

float LightRange = 10;
 
float4 LineColor = float4(0, 0, 0, 1);
 
// Zmienia� w zale�no�ci od skali obiektu
float4 LineThickness = .03;
 
texture Texture;
 
sampler2D textureSampler = sampler_state 
{
    Texture = (Texture);
    MinFilter = Linear;
    MagFilter = Linear;
    MipFilter = Linear;
    AddressU = Clamp;
    AddressV = Clamp;
};
 
struct AppToVertex
{
    float4 Position : POSITION0;            // The position of the vertex
    float3 Normal : NORMAL0;                // The vertex's normal
    float2 TextureCoordinate : TEXCOORD0;    // The texture coordinate of the vertex
};
 
struct VertexToPixel
{
    float4 Position : POSITION0;
    float2 TextureCoordinate : TEXCOORD0;
    float3 Normal : TEXCOORD1;
    //dodane
    float3 Pixel : TEXCOORD2;
};
 
VertexToPixel CelVertexShader(AppToVertex input)
{
    VertexToPixel output;
 
    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
 
    output.Normal = normalize(mul(input.Normal, WorldInverseTranspose));
    
    output.TextureCoordinate = input.TextureCoordinate;
 
	//dodane
	output.Pixel = worldPosition.xyz;
	
    return output;
}
 
float4 CelPixelShader(VertexToPixel input) : COLOR0
{
	float4 color = tex2D(textureSampler, input.TextureCoordinate);
    float actualRange = length(input.Pixel - LightPosition);
    float intensity = 0;
    
    if(actualRange > LightRange)
    {
		intensity = 0;
	}
	else
	{
		intensity = dot(normalize(LightPosition - input.Pixel), input.Normal);
		//float rangeIntensity = -(actualRange/LightRange) + 1;
		//color *= (-(actualRange/LightRange) + 1) * DiffuseColor;
	}
 
    color.a = 1;
    
    /*if (intensity > 0.95)
       color = float4(1.0,1,1,1.0) * color;
    else if (intensity > 0.5)
       color = float4(0.7,0.7,0.7,1.0) * color;
    else if (intensity > 0.05)
       color = float4(0.35,0.35,0.35,1.0) * color;
    else
       color = float4(0.1,0.1,0.1,1.0) * color;
   */
    
       
 	color = round(intensity * 5) / 5 * color;
    return color;
}
 
VertexToPixel OutlineVertexShader(AppToVertex input)
{
    VertexToPixel output = (VertexToPixel)0;
 
    // Calculate where the vertex ought to be.  This line is equivalent
    // to the transformations in the CelVertexShader.
    float4 original = mul(mul(mul(input.Position, World), View), Projection);
 
    // Calculates the normal of the vertex like it ought to be.
    float4 normal = mul(mul(mul(input.Normal, World), View), Projection);
 
    // Take the correct "original" location and translate the vertex a little
    // bit in the direction of the normal to draw a slightly expanded object.
    // Later, we will draw over most of this with the right color, except the expanded
    // part, which will leave the outline that we want.
    output.Position    = original + (mul(LineThickness, normal));
 
    return output;
}
 
float4 OutlinePixelShader(VertexToPixel input) : COLOR0
{
    return LineColor;
}
 
technique Toon
{
    pass Pass1
    {
        VertexShader = compile vs_1_1 OutlineVertexShader();
        PixelShader = compile ps_2_0 OutlinePixelShader();
        CullMode = CW;
    }
 
    pass Pass2
    {
        VertexShader = compile vs_1_1 CelVertexShader();
        PixelShader = compile ps_2_0 CelPixelShader();
        CullMode = CCW;
    }
}