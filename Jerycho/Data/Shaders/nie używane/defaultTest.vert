#version 330

uniform mat4 mvpMatrices[2];

layout(location = 0) in vec2 vertexPosition;
layout(location = 1) in vec4 vertexColor;
layout(location = 2) in int objectId;


out vec4 fragmentColor;


void main(void)
{
  gl_Position   = mvpMatrices[objectId] * vec4(vertexPosition, 1, 1);
  fragmentColor = vertexColor;
 
  if(objectId == 0)
	  fragmentColor = vec4(1,0,0,1);
  
  if(objectId == 1)
	  fragmentColor = vec4(0,1,0,1);
}