#version 120

uniform sampler2D texture;
//varying vec2 texcoord;
uniform float darkness; // value from 0 to 1

void main(void)
{
    gl_FragColor = texture2D(texture, gl_TexCoord[0].st).rgba;
	gl_FragColor *= gl_Color;
	gl_FragColor.rgb *= darkness;
}