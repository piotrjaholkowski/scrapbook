//////////////////////////////////////
///////simple dream like effect///////
//////////////////////////////////////

float4 vecSkill1;
float4 vecTime;

texture entSkin1;

sampler postTex = sampler_state
{
   texture = (entSkin1);
   MinFilter = linear;
   MagFilter = linear;
   MipFilter = linear;
   AddressU = Clamp;
   AddressV = Clamp;
};

float4 Dream_PS( float2 Tex : TEXCOORD0 ) : COLOR0
{
	float4 Color;
	
	Tex.xy -= 0.5;
	Tex.xy *= 1-(sin(vecTime.w*vecSkill1[2])*vecSkill1[3]+vecSkill1[3])*0.5;
	Tex.xy += 0.5;
	
	Color = tex2D( postTex, Tex.xy);

	Color += tex2D( postTex, Tex.xy+0.001*vecSkill1[0]);
	Color += tex2D( postTex, Tex.xy+0.003*vecSkill1[0]);
	Color += tex2D( postTex, Tex.xy+0.005*vecSkill1[0]);
	Color += tex2D( postTex, Tex.xy+0.007*vecSkill1[0]);
	Color += tex2D( postTex, Tex.xy+0.009*vecSkill1[0]);
	Color += tex2D( postTex, Tex.xy+0.011*vecSkill1[0]);

	Color += tex2D( postTex, Tex.xy-0.001*vecSkill1[0]);
	Color += tex2D( postTex, Tex.xy-0.003*vecSkill1[0]);
	Color += tex2D( postTex, Tex.xy-0.005*vecSkill1[0]);
	Color += tex2D( postTex, Tex.xy-0.007*vecSkill1[0]);
	Color += tex2D( postTex, Tex.xy-0.009*vecSkill1[0]);
	Color += tex2D( postTex, Tex.xy-0.011*vecSkill1[0]);

	Color.rgb = (Color.r+Color.g+Color.b)/3.0f;

	Color /= vecSkill1[1];
	return Color;
}

technique Dream
{
	pass pass_00
	{
		PixelShader = compile ps_2_0 Dream_PS();
	}
}