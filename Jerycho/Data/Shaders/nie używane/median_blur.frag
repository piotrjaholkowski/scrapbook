#version 120

uniform sampler2D texture;

void main(void)
{
    gl_FragColor = texture2D(texture, gl_TexCoord[0].st).rgba;
	gl_FragColor *= gl_Color;
	float luminance = 0.3 * gl_FragColor.r + 0.59 * gl_FragColor.g + 0.11 * gl_FragColor.b;
	gl_FragColor.rgb = vec3(luminance);
}