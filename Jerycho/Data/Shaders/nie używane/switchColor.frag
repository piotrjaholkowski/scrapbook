#version 120

uniform sampler2D texture;
varying vec2 texcoord; // przekazywane przez vertex shader

void main(void)
{
    gl_FragColor = texture2D(texture, texcoord).bgra;
}