#version 330
//All above matches openGL version
//#version 150 - 3.2
//#version 140 - 3.1
//#version 130 - 3.0
//#version 120 - 2.1
//#version 110 - 1.1
//#extension extension_name​ : behavior​

//layout(early_fragment_tests) in;

//built in types 3.0
/*
in vec4 gl_FragCoord;
in bool gl_FrontFacing;
in vec2 gl_PointCoord;
*/

//built in types 4.0
/*
in int gl_SampleID;
in vec2 gl_SamplePosition;
in int gl_SampleMaskIn[];
*/

/*
in float gl_ClipDistance[];
in int gl_PrimitiveID;
*/

//built in types 4.3
/*
in int gl_Layer;
in int gl_ViewportIndex;
*/

//out float gl_FragDepth;

//out vec4 colorOut;
in vec4 fragmentColor;

//layout(location = 0) out vec4 diffuseColor; //color attachment at 0 when using fbo
layout(location = 0) out vec4 color;
layout(location = 1) out vec4 glow;

/*
const GLenum buffers[] = {GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT2, GL_NONE, GL_COLOR_ATTACHMENT0};
glDrawBuffers(4, buffers);
*/


//uniform worldMatrix

layout (std140) uniform World
{
  float worldDarkness;
} world;

layout (std140) uniform Material
{
  float materialDarkness;
} material;

layout (std140) uniform Object
{
  float objectDarkness;
} object;

uniform sampler2D texture;
//varying vec2 texcoord; // przekazywane przez vertex shader
//varying vec4 color; // przekazywane przez vertex shader

void main(void)
{
    //gl_FragColor = texture2D(texture, gl_TexCoord[0].st).rgba;
	//gl_FragColor *= gl_Color;
	//gl_FragColor = texture2D(texture, texcoord).rgba;
	
	//gl_FragColor = gl_Color;
	//discard​;
	
	//vec4 transformedVector = myMatrix * myVector; // Yeah, it's pretty much the same than GLM
	
	//diffuseColor = vec4(1.0, 1.0, 1.0, 1.0);
	//diffuseColor = fragmentColor; when using fbo;
	//color = vec4(1,1,0,1);
	color = fragmentColor;
}