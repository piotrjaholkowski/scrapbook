#version 120

uniform sampler2D texture;

void main(void)
{
    // Convert to greyscale using NTSC weightings
	vec4 decal = texture2D(texture, gl_TexCoord[0].st).rgba;
	decal *= gl_Color;
	gl_FragColor = vec4(1.0 - decal.rgb, decal.a);
}