#version 120

uniform sampler2D texture;
uniform vec2 sampleDivisor; // (pixel size)/(screen size)

void main()
{
    vec2 samplePos = gl_TexCoord[0].st - mod( gl_TexCoord[0].st, sampleDivisor );
    gl_FragColor = texture2D( texture, samplePos );
}