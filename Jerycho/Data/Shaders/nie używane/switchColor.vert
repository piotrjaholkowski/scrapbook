#version 120

varying vec2 texcoord; // przekazywane do fragment shadera

void main(void)
{
   //texcoord = gl_MultiTexCoord0; bo nie wie jak rzutowa� trzeba st
   texcoord = gl_MultiTexCoord0.st;
   
   vec4 v = vec4(gl_Vertex);
   //v.x = v.x * 0.5;
   //v.y = v.y * 0.5;
   gl_Position = gl_ModelViewProjectionMatrix * v;
}