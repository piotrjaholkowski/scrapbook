#version 120

uniform sampler2D texture;
uniform sampler2D normalmap;
uniform vec3 light_pos = vec3(0.0,0.0,1.0);

void main(void)
{
	// Extract the normal from the normal map  
	vec3 normal = normalize(texture2D(normalmap, gl_TexCoord[0].st).rgb * 2.0 - 1.0);   
	
	// Determine where the light is positioned (this can be set however you like)  
	vec3 light_posn = normalize(light_pos);  
	
	// Calculate the lighting diffuse value  
	float diffuse = max(dot(normal, light_posn), 0.0);  
	
	vec4 decal = texture2D(texture, gl_TexCoord[0].st).rgba;
	decal *= gl_Color;
	decal.rgb *= diffuse;

	gl_FragColor = decal.rgba;
}