#version 120

//varying vec2 texcoord;

void main(void)
{
   vec4 v = gl_Vertex;
   v.xy = v.xy * 2.0;
   gl_Position = gl_ModelViewProjectionMatrix * v;
   gl_FrontColor = gl_Color;
   gl_TexCoord[0] = gl_MultiTexCoord0;
}