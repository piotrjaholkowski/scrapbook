#version 120

//built in types
//attribute vec4 gl_Color;
//attribute vec4 gl_SecondaryColor;
//attribute vec3 gl_Normal;
//attribute vec4 gl_Vertex;
//attribute vec4 gl_MultiTexCoord0;
//attribute float gl_FogCoord;
uniform vec3 light_pos = vec3(0.0,0.0,1.0);

void main(void)
{
   //vec3 vertex = ftransform();
   vec4 vertex = gl_ModelView * gl_Vertex;
   //uniform vec3 light_pos = vec3(0.0,0.0,1.0);
   vec3 light_diff.xyz = light_pos.xyz - vertex.xyz; 
  
  
   gl_FrontColor = gl_Color;
   gl_TexCoord[0] = gl_MultiTexCoord0;
}