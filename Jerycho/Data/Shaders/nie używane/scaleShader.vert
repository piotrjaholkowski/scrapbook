void main() {            
     // Set the position of the current vertex 
	 vec4 v = vec4(gl_Vertex);
	 v.x = v.x * 1.0;
	 v.y = v.y * 1.0;
     gl_Position = gl_ModelViewProjectionMatrix * v;
}