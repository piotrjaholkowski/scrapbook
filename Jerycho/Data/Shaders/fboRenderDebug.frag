#version 330 core

in vec2 UV;

out vec4 color;

uniform sampler2D debugTexture;
//uniform sampler2DMS debugTexture;

void main()
{
	color.rgb = texture( debugTexture, UV).xyz;
	color.a   = 1.0;
}